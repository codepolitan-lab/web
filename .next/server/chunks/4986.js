"use strict";
exports.id = 4986;
exports.ids = [4986];
exports.modules = {

/***/ 4986:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "tc": () => (/* binding */ removeTag),
/* harmony export */   "tj": () => (/* binding */ getLessons),
/* harmony export */   "k7": () => (/* binding */ countRates),
/* harmony export */   "T4": () => (/* binding */ formatPrice),
/* harmony export */   "uf": () => (/* binding */ formatNumber),
/* harmony export */   "Yw": () => (/* binding */ formatOnlyDate),
/* harmony export */   "wh": () => (/* binding */ formatDateFromNow),
/* harmony export */   "DU": () => (/* binding */ getPercentage),
/* harmony export */   "Nz": () => (/* binding */ getPercentageTestimony),
/* harmony export */   "Sy": () => (/* binding */ shuffleArray),
/* harmony export */   "g5": () => (/* binding */ getTotalModule),
/* harmony export */   "AE": () => (/* binding */ getTotalTime),
/* harmony export */   "fm": () => (/* binding */ capitalizeFirstLetter)
/* harmony export */ });
/* unused harmony export formatDate */
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2245);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var moment_locale_id__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5518);
/* harmony import */ var moment_locale_id__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(moment_locale_id__WEBPACK_IMPORTED_MODULE_1__);


// Remove Tag
const removeTag = (tag)=>{
    return tag.replace(/(<([^>]+)>)/ig, "");
};
// Get Lesson
const getLessons = (lessons)=>{
    const lessonTopic = [];
    const lessonContent = [];
    /* Loop for tab lessons */ for(const lessonKey in lessons){
        lessonTopic.push({
            topic_slug: lessonKey,
            topic_title: lessons[lessonKey]['topic_title']
        });
    }
    lessonTopic.map((lessonTopicItem)=>{
        let tempEachContent = {};
        let topicSlug = lessonTopicItem.topic_slug;
        tempEachContent.topic_slug = topicSlug;
        tempEachContent.contents = [];
        let lessonContentKey = Object.keys(lessons[topicSlug].lessons);
        for(const contentKey in lessonContentKey){
            let tempContent = {};
            tempContent.lesson_slug = lessons[topicSlug].lessons[lessonContentKey[contentKey]].lesson_slug;
            tempContent.lesson_title = lessons[topicSlug].lessons[lessonContentKey[contentKey]].lesson_title;
            tempContent.duration = lessons[topicSlug].lessons[lessonContentKey[contentKey]].duration;
            tempEachContent.contents.push(tempContent);
        }
        lessonContent.push(tempEachContent);
    });
    return {
        lessonTopic,
        lessonContent
    };
};
// Hitung rata-rata rating
const countRates = (rate)=>{
    const rates = [];
    if (rate.status === 'failed') {
        console.log('there is no testimony');
    } else {
        rate?.map((item)=>{
            rates.push(parseInt(item.rate));
        });
    }
    const sumRate = rates.reduce((a, b)=>a + b
    , 0);
    const totalRate = rates.length;
    const ratingValue = sumRate / totalRate;
    return ratingValue;
};
// Format Price
const formatPrice = (price)=>{
    return price?.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
};
// Format Number
const formatNumber = (value)=>{
    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
};
// Format Only Date
const formatOnlyDate = (value)=>{
    return moment__WEBPACK_IMPORTED_MODULE_0___default()(value).format('LL');
};
// Format Date
const formatDate = (value)=>{
    return moment(value).format('LLLL');
};
// Format From Now
const formatDateFromNow = (value)=>{
    return moment__WEBPACK_IMPORTED_MODULE_0___default()(value).fromNow();
};
// Get Percentage From Price
const getPercentage = (retailPrice, normalPrice)=>{
    const decreaseValue = normalPrice - retailPrice;
    const result = decreaseValue / normalPrice * 100;
    return Math.round(result);
};
// Get Percentage From Testimony
const getPercentageTestimony = (testimony, count)=>{
    const total = testimony.length > 1 && testimony.map((item)=>item.rate
    ).filter((item)=>item == count
    );
    let result = total.length / testimony.length * 100;
    return Math.round(result);
};
// Suffle Array
const shuffleArray = (array)=>{
    array?.sort(()=>Math.random() - 0.5
    );
    return array;
};
// Get total module
const getTotalModule = (courses)=>{
    let totalModule = 0;
    courses.map((course)=>totalModule += course.total_module
    );
    return totalModule;
};
// Get total time
const getTotalTime = (courses)=>{
    let totalTime = 0;
    courses.map((course)=>totalTime += course.total_time
    );
    return totalTime;
};
// Capitalize first leter
const capitalizeFirstLetter = (text)=>{
    return text.charAt(0).toUpperCase() + text.slice(1);
};


/***/ })

};
;