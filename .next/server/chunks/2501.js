exports.id = 2501;
exports.ids = [2501];
exports.modules = {

/***/ 1116:
/***/ ((module) => {

// Exports
module.exports = {
	"card": "SkeletonCardRoadmap_card___ByNr",
	"gradient": "SkeletonCardRoadmap_gradient___2x8_",
	"card_img": "SkeletonCardRoadmap_card_img__z_FRK",
	"overlay": "SkeletonCardRoadmap_overlay__Nb_lW",
	"title": "SkeletonCardRoadmap_title__OfTG1"
};


/***/ }),

/***/ 2501:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _SkeletonCardRoadmap_module_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1116);
/* harmony import */ var _SkeletonCardRoadmap_module_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_SkeletonCardRoadmap_module_scss__WEBPACK_IMPORTED_MODULE_1__);


const SkeletonCardRoadmap = ()=>{
    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
        className: `${(_SkeletonCardRoadmap_module_scss__WEBPACK_IMPORTED_MODULE_1___default().card)} card border-0 text-white`,
        children: [
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                className: (_SkeletonCardRoadmap_module_scss__WEBPACK_IMPORTED_MODULE_1___default().card_img)
            }),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                className: `${(_SkeletonCardRoadmap_module_scss__WEBPACK_IMPORTED_MODULE_1___default().overlay)} card-img-overlay bg-secondary d-flex flex-column justify-content-end`,
                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                    className: "my-1",
                    style: {
                        width: '80%'
                    }
                })
            })
        ]
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (SkeletonCardRoadmap);


/***/ })

};
;