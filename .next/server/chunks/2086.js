exports.id = 2086;
exports.ids = [2086];
exports.modules = {

/***/ 7941:
/***/ ((module) => {

// Exports
module.exports = {
	"card": "CardRoadmap_card__0qP65",
	"card_img": "CardRoadmap_card_img__J3UWl",
	"overlay": "CardRoadmap_overlay__PzjNE",
	"title": "CardRoadmap_title__xHV3a",
	"overlay_hover": "CardRoadmap_overlay_hover__ls5JB",
	"rate": "CardRoadmap_rate__y4Te6",
	"roadmap_icon": "CardRoadmap_roadmap_icon__PdK36",
	"roadmap_title": "CardRoadmap_roadmap_title__p5Us3",
	"roadmap_info": "CardRoadmap_roadmap_info__BPYq9"
};


/***/ }),

/***/ 2086:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6466);
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(7197);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_image__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(5675);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _utils_helper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(4986);
/* harmony import */ var _CardRoadmap_module_scss__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(7941);
/* harmony import */ var _CardRoadmap_module_scss__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_CardRoadmap_module_scss__WEBPACK_IMPORTED_MODULE_6__);







const CardRoadmap = ({ title , thumbnail , rate , level , totalCourse , totalStudent , icon , author  })=>{
    const { 0: hover , 1: setHover  } = (0,react__WEBPACK_IMPORTED_MODULE_4__.useState)(false);
    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
        className: `${(_CardRoadmap_module_scss__WEBPACK_IMPORTED_MODULE_6___default().card)} card border-0 text-white`,
        onMouseEnter: ()=>setHover(true)
        ,
        onMouseLeave: ()=>setHover(false)
        ,
        children: [
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_image__WEBPACK_IMPORTED_MODULE_3__["default"], {
                className: (_CardRoadmap_module_scss__WEBPACK_IMPORTED_MODULE_6___default().card_img),
                src: thumbnail || "/assets/img/placeholder.jpg",
                placeholder: "blur",
                blurDataURL: "/assets/img/placeholder.jpg",
                alt: title,
                width: 100,
                height: 200,
                layout: "responsive"
            }),
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                className: `${(_CardRoadmap_module_scss__WEBPACK_IMPORTED_MODULE_6___default().overlay_hover)} card-img-overlay d-flex flex-column justify-content-end`,
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                        className: (_CardRoadmap_module_scss__WEBPACK_IMPORTED_MODULE_6___default().roadmap_icon),
                        loading: "lazy",
                        src: icon,
                        alt: "Icon"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                        className: (_CardRoadmap_module_scss__WEBPACK_IMPORTED_MODULE_6___default().roadmap_title),
                        children: author
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h5", {
                        className: (_CardRoadmap_module_scss__WEBPACK_IMPORTED_MODULE_6___default().title),
                        children: title
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: `${(_CardRoadmap_module_scss__WEBPACK_IMPORTED_MODULE_6___default().roadmap_info)} d-flex align-items-start`,
                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("small", {
                            children: [
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__.FontAwesomeIcon, {
                                    className: "my-auto",
                                    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_1__.faBook
                                }),
                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                                    className: "ms-1",
                                    children: [
                                        totalCourse,
                                        " Kelas"
                                    ]
                                }),
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__.FontAwesomeIcon, {
                                    className: "my-auto ms-2",
                                    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_1__.faUsers
                                }),
                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                                    className: "ms-1",
                                    children: [
                                        totalStudent,
                                        " Siswa"
                                    ]
                                }),
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__.FontAwesomeIcon, {
                                    className: "my-auto ms-2",
                                    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_1__.faSignal
                                }),
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                    className: "ms-1",
                                    children: (0,_utils_helper__WEBPACK_IMPORTED_MODULE_5__/* .capitalizeFirstLetter */ .fm)(level)
                                })
                            ]
                        })
                    }),
                    rate && /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: `${(_CardRoadmap_module_scss__WEBPACK_IMPORTED_MODULE_6___default().rate)} text-end text-warning`,
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__.FontAwesomeIcon, {
                                className: "my-auto",
                                icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_1__.faStar
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                className: "ms-1",
                                children: rate
                            })
                        ]
                    })
                ]
            })
        ]
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (CardRoadmap);


/***/ })

};
;