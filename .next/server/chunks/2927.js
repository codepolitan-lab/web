exports.id = 2927;
exports.ids = [2927];
exports.modules = {

/***/ 5880:
/***/ ((module) => {

// Exports
module.exports = {
	"card_course": "SkeletonCardCourse_card_course__s4Y6V",
	"card_img_top": "SkeletonCardCourse_card_img_top__ZRfsu",
	"gradient": "SkeletonCardCourse_gradient__TZrPY",
	"course_author": "SkeletonCardCourse_course_author__aVv70",
	"course_title": "SkeletonCardCourse_course_title__tsNTK",
	"course_level": "SkeletonCardCourse_course_level___dHR2",
	"course_info": "SkeletonCardCourse_course_info__fGhEi",
	"rate_and_price": "SkeletonCardCourse_rate_and_price__b7EH_"
};


/***/ }),

/***/ 2927:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _SkeletonCardCourse_module_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5880);
/* harmony import */ var _SkeletonCardCourse_module_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_SkeletonCardCourse_module_scss__WEBPACK_IMPORTED_MODULE_1__);


const SkeletonCardCourse = ()=>{
    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
        className: `${(_SkeletonCardCourse_module_scss__WEBPACK_IMPORTED_MODULE_1___default().card_course)} card border-0 shadow-sm`,
        children: [
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                className: `${(_SkeletonCardCourse_module_scss__WEBPACK_IMPORTED_MODULE_1___default().card_img_top)} card-img-top`
            }),
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                className: "card-body",
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                        className: "mb-3",
                        style: {
                            width: '50%'
                        }
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                        className: "my-2",
                        style: {
                            width: '100%'
                        }
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                        className: "d-block",
                        style: {
                            width: '80%'
                        }
                    }),
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: "row my-2",
                        children: [
                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                className: "col-6",
                                children: [
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                        className: "d-inline-block",
                                        style: {
                                            width: '70%'
                                        }
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                        className: "d-inline-block",
                                        style: {
                                            width: '70%'
                                        }
                                    })
                                ]
                            }),
                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                className: "col-6",
                                children: [
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                        className: "d-inline-block",
                                        style: {
                                            width: '70%'
                                        }
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                        className: "d-inline-block",
                                        style: {
                                            width: '70%'
                                        }
                                    })
                                ]
                            })
                        ]
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                        className: "d-block",
                        style: {
                            width: '65%'
                        }
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("hr", {
                        className: "text-muted"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: (_SkeletonCardCourse_module_scss__WEBPACK_IMPORTED_MODULE_1___default().rate_and_price),
                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                            className: "d-flex align-items-start",
                            children: [
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                    className: "d-inline-block",
                                    style: {
                                        width: '55%'
                                    }
                                }),
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                    className: "d-inline-block ms-auto",
                                    style: {
                                        width: '30%'
                                    }
                                })
                            ]
                        })
                    })
                ]
            })
        ]
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (SkeletonCardCourse);


/***/ })

};
;