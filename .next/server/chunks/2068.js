exports.id = 2068;
exports.ids = [2068];
exports.modules = {

/***/ 2250:
/***/ ((module) => {

// Exports
module.exports = {
	"section": "CertificateSection_section__5QQJl",
	"title": "CertificateSection_title__QV86r"
};


/***/ }),

/***/ 2068:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _CertificateSection_module_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2250);
/* harmony import */ var _CertificateSection_module_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_CertificateSection_module_scss__WEBPACK_IMPORTED_MODULE_1__);


const CertificateSection = ()=>{
    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("section", {
        className: `${(_CertificateSection_module_scss__WEBPACK_IMPORTED_MODULE_1___default().section)}`,
        children: [
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                className: "container p-4 p-lg-5",
                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: "row mt-5 mt-lg-0 my-lg-5 justify-content-lg-end",
                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: "col-lg-4 my-lg-5 py-lg-5",
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
                                className: (_CertificateSection_module_scss__WEBPACK_IMPORTED_MODULE_1___default().title),
                                children: "Sertifikat"
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                className: "my-3 text-muted",
                                children: "Kamu akan mendapatkan sertifikat digital setelah menyelesaikan kelas ini"
                            })
                        ]
                    })
                })
            }),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                className: "px-auto d-lg-none",
                style: {
                    overflow: 'hidden'
                },
                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: "row",
                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: "col",
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                            className: "img-fluid",
                            src: "/assets/img/program/certificate-img-small.png",
                            alt: "Forum Tanya Jawab"
                        })
                    })
                })
            })
        ]
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (CertificateSection);


/***/ })

};
;