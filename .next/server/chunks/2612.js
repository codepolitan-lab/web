exports.id = 2612;
exports.ids = [2612];
exports.modules = {

/***/ 5999:
/***/ ((module) => {

// Exports
module.exports = {
	"card_img": "CardMentor_card_img__VEOAv",
	"jobs": "CardMentor_jobs__yTdD_"
};


/***/ }),

/***/ 2612:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _CardMentor_module_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5999);
/* harmony import */ var _CardMentor_module_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_CardMentor_module_scss__WEBPACK_IMPORTED_MODULE_1__);


const CardMentor = ({ thumbnail , name , jobs , brandImage , fromBrand  })=>{
    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
        className: "card bg-white border-0 h-100",
        children: [
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                className: (_CardMentor_module_scss__WEBPACK_IMPORTED_MODULE_1___default().card_img),
                src: thumbnail || "/assets/img/placeholder.jpg",
                alt: name || "Belum ada nama"
            }),
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                className: "card-body text-center px-0 pb-2",
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h5", {
                        className: "card-text h6 mb-3",
                        children: name || "Belum ada nama"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: "my-auto h-50",
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                            className: `${(_CardMentor_module_scss__WEBPACK_IMPORTED_MODULE_1___default().jobs)} text-muted`,
                            title: jobs,
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("small", {
                                children: jobs || "Job belum terisi"
                            })
                        })
                    })
                ]
            }),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                className: "card-footer bg-white border-0",
                children: !fromBrand && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                    className: "img-fluid w-25 rounded-circle d-block mx-auto",
                    src: brandImage || "/assets/img/placeholder.jpg",
                    alt: "Brand"
                })
            })
        ]
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (CardMentor);


/***/ })

};
;