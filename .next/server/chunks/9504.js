exports.id = 9504;
exports.ids = [9504];
exports.modules = {

/***/ 6713:
/***/ ((module) => {

// Exports
module.exports = {
	"card": "StudycaseSection_card__OZHKr",
	"card_title": "StudycaseSection_card_title__wk6ym",
	"card_img": "StudycaseSection_card_img__s6DKX",
	"text_background": "StudycaseSection_text_background__sK0mo",
	"overlay": "StudycaseSection_overlay__kALBZ"
};


/***/ }),

/***/ 3680:
/***/ ((module) => {

// Exports
module.exports = {
	"discord_icon": "Program_discord_icon__Pet9f",
	"headline": "Program_headline__St2c_",
	"count": "Program_count__NY1NI",
	"card_label_materi": "Program_card_label_materi__l8Bzs"
};


/***/ }),

/***/ 6244:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.a(module, async (__webpack_handle_async_dependencies__) => {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _StudycaseSection_module_scss__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(6713);
/* harmony import */ var _StudycaseSection_module_scss__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_StudycaseSection_module_scss__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var swiper_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3015);
/* harmony import */ var swiper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3877);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([swiper_react__WEBPACK_IMPORTED_MODULE_1__, swiper__WEBPACK_IMPORTED_MODULE_2__]);
([swiper_react__WEBPACK_IMPORTED_MODULE_1__, swiper__WEBPACK_IMPORTED_MODULE_2__] = __webpack_async_dependencies__.then ? await __webpack_async_dependencies__ : __webpack_async_dependencies__);



// import Swiper core and required modules

// install Swiper modules
swiper__WEBPACK_IMPORTED_MODULE_2__["default"].use([
    swiper__WEBPACK_IMPORTED_MODULE_2__.Navigation
]);
const StudycaseSection = ({ data  })=>{
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("section", {
        className: "section bg-light",
        id: "swiperCarouselStudyCase",
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
            className: "container p-4 p-lg-5",
            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                className: "row",
                children: [
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: "col-lg-4 my-auto",
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
                                className: "section-title",
                                children: "Belajar Dengan Studi Kasus"
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                className: "text-muted my-3",
                                children: "Nggak cuman teori doang, di kelas ini kamu juga bisa langsung praktek mengikuti studi kasus yang telah disediakan. kami telah menyediakan contoh studi kasus pembuatan aplikasi webstite dan kamu juga bisa mengikuti step by step sampai kamu bisa membuatnya sendiri."
                            })
                        ]
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: "col-lg-7 ms-auto",
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(swiper_react__WEBPACK_IMPORTED_MODULE_1__.Swiper, {
                            spaceBetween: 20,
                            grabCursor: true,
                            slidesPerView: 2,
                            pagination: true,
                            navigation: true,
                            breakpoints: {
                                // when window width is >= 414px
                                414: {
                                    slidesPerView: 2
                                },
                                // when window width is >= 768px
                                768: {
                                    slidesPerView: 3
                                },
                                1024: {
                                    slidesPerView: 3
                                }
                            },
                            style: {
                                paddingTop: "60px"
                            },
                            children: data.map((item, index)=>{
                                return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(swiper_react__WEBPACK_IMPORTED_MODULE_1__.SwiperSlide, {
                                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        className: `${(_StudycaseSection_module_scss__WEBPACK_IMPORTED_MODULE_3___default().card)} card text-white`,
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                src: item.thumbnail,
                                                className: `${(_StudycaseSection_module_scss__WEBPACK_IMPORTED_MODULE_3___default().card_img)} card-img`,
                                                alt: item.title
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                className: (_StudycaseSection_module_scss__WEBPACK_IMPORTED_MODULE_3___default().text_background)
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                className: `${(_StudycaseSection_module_scss__WEBPACK_IMPORTED_MODULE_3___default().overlay)} card-img-overlay d-flex align-items-end`,
                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h5", {
                                                    className: (_StudycaseSection_module_scss__WEBPACK_IMPORTED_MODULE_3___default().card_title),
                                                    children: item.title
                                                })
                                            })
                                        ]
                                    })
                                }, index));
                            })
                        })
                    })
                ]
            })
        })
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (StudycaseSection);

});

/***/ })

};
;