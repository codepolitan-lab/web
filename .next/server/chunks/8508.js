"use strict";
exports.id = 8508;
exports.ids = [8508];
exports.modules = {

/***/ 122:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var mixpanel_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5804);
/* harmony import */ var mixpanel_browser__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(mixpanel_browser__WEBPACK_IMPORTED_MODULE_1__);


const SectionCTA = ({ caption , link , btnTitle , background  })=>{
    mixpanel_browser__WEBPACK_IMPORTED_MODULE_1___default().init('608746391f30c018d759b8c2c1ecb097', {
        debug: false
    });
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("section", {
        className: `section ${background}`,
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
            className: "container p-4 p-lg-5",
            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                className: "row",
                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: "col",
                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: "card bg-codepolitan border-0",
                        style: {
                            borderRadius: '25px'
                        },
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "card-body bg-codepolitan text-center",
                            style: {
                                borderRadius: '25px'
                            },
                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                className: "py-4",
                                children: [
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
                                        className: "section-title text-white mx-auto mb-3 h1 w-75",
                                        children: caption
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("a", {
                                        onClick: ()=>mixpanel_browser__WEBPACK_IMPORTED_MODULE_1___default().track('Isi formulir karir', {
                                                'source': 'Landing'
                                            })
                                        ,
                                        style: {
                                            borderRadius: '15px',
                                            background: '#FF8B99'
                                        },
                                        className: "btn btn-lg text-white my-3",
                                        href: link,
                                        target: "_blank",
                                        rel: "noreferrer",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("strong", {
                                            children: btnTitle
                                        })
                                    })
                                ]
                            })
                        })
                    })
                })
            })
        })
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (SectionCTA);


/***/ }),

/***/ 3858:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);


const SectionPartners = ({ backgroundColor , sectionTitleTextStyle , customTItle  })=>{
    const { 0: partners  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)([
        {
            name: 'Kemnaker',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/KEMNAKER_MGfr7cHn61.webp'
        },
        {
            name: 'Kemenkeu',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/kemenkeu_p9YQYkw-AI.webp'
        },
        {
            name: 'Here',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/here_THgKeV7WUz.webp'
        },
        {
            name: 'Udemy',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/udemy_DVYNj94zCK14x.webp'
        },
        {
            name: 'Samsung',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/samsung_Insf8kassvK.webp'
        },
        {
            name: 'Lenovo',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/Lenovo_co1GDjBpxV.webp'
        },
        {
            name: 'Alibaba Cloud',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/Alibaba_kBCmKi7O7.webp'
        },
        {
            name: 'Intel',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/Intel_T_cVZHo81M_t.webp'
        },
        {
            name: 'Dicoding',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/dicoding-header-logo_-yUghUNkx7.webp'
        },
        {
            name: 'IBM',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/IBM_GXVYl92inl905.webp'
        },
        {
            name: 'Pixel',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/pixel_YtPl8tvzu.webp'
        },
        {
            name: 'XL',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/xl_t5bLnDwJn.webp'
        },
        {
            name: 'Hactive8',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/hacktiv8_Y3L2HUj4K4X.webp'
        },
        {
            name: 'Kudo',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/Kudo_oGHxpphEw.webp'
        },
        {
            name: 'Refactory',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/Refactory_VO89Fn5BX.webp'
        },
        {
            name: 'Ajita',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/ajita_Za3H1pSLYI.webp'
        },
        {
            name: 'Geometri',
            thumbnail: 'https://i.ibb.co/86yCrCP/Geometri.png'
        }, 
    ]);
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("section", {
        className: `section ${backgroundColor}`,
        id: "partners",
        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
            className: "container p-4 p-lg-5",
            children: [
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: "row mb-4",
                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: `col ${sectionTitleTextStyle}`,
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
                            className: "section-title h3",
                            children: customTItle || 'Telah Dipercaya oleh Perusahaan Besar'
                        })
                    })
                }),
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: "row justify-content-center",
                    children: partners.map((item, index)=>{
                        return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "col-4 col-md-2 my-2",
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: "card border-0",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: `card-body p-2 p-lg-3 ${backgroundColor}`,
                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                        className: "img-fluid d-block mx-auto",
                                        src: item.thumbnail,
                                        alt: item.name
                                    })
                                })
                            })
                        }, index));
                    })
                })
            ]
        })
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (SectionPartners);


/***/ })

};
;