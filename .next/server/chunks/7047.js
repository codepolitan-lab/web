exports.id = 7047;
exports.ids = [7047];
exports.modules = {

/***/ 626:
/***/ ((module) => {

// Exports
module.exports = {
	"avatar_img": "CardForum_avatar_img__k0Gvz",
	"user_info": "CardForum_user_info__DuNdX"
};


/***/ }),

/***/ 7047:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.a(module, async (__webpack_handle_async_dependencies__) => {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2245);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _CardForum_module_scss__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(626);
/* harmony import */ var _CardForum_module_scss__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_CardForum_module_scss__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var react_markdown_lib_react_markdown__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(7433);
/* harmony import */ var rehype_raw__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1871);
/* harmony import */ var remark_gfm__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(6809);
/* harmony import */ var _Codeblock__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(4074);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([rehype_raw__WEBPACK_IMPORTED_MODULE_2__, remark_gfm__WEBPACK_IMPORTED_MODULE_3__, react_markdown_lib_react_markdown__WEBPACK_IMPORTED_MODULE_6__]);
([rehype_raw__WEBPACK_IMPORTED_MODULE_2__, remark_gfm__WEBPACK_IMPORTED_MODULE_3__, react_markdown_lib_react_markdown__WEBPACK_IMPORTED_MODULE_6__] = __webpack_async_dependencies__.then ? await __webpack_async_dependencies__ : __webpack_async_dependencies__);







const CardForum = ({ avatar , name , date , totalAnswer , lessonTitle , subject , mark , content  })=>{
    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
        className: "card shadow-sm rounded my-3",
        children: [
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                className: "card-header bg-white border-0",
                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                    className: "row justify-content-between",
                    children: [
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "col-9 col-lg-auto",
                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("d", {
                                className: "d-flex align-items-start mt-2",
                                children: [
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                        className: `${(_CardForum_module_scss__WEBPACK_IMPORTED_MODULE_5___default().avatar_img)} rounded-circle`,
                                        src: avatar || "/assets/img/icons/icon-avatar.png",
                                        alt: name
                                    }),
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        className: `${(_CardForum_module_scss__WEBPACK_IMPORTED_MODULE_5___default().user_info)} ms-3 ms-lg-4 mt-1`,
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h5", {
                                                className: "text-secondary m-0",
                                                children: name
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                className: "text-muted m-0",
                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("small", {
                                                    children: moment__WEBPACK_IMPORTED_MODULE_1___default()(date).fromNow()
                                                })
                                            })
                                        ]
                                    })
                                ]
                            })
                        }),
                        content || !totalAnswer ? '' : /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                            className: "col-3 col-lg-auto mt-2",
                            children: [
                                totalAnswer === '0' && /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                                    className: `float-end badge ${mark === 'solved' ? 'bg-codepolitan' : 'bg-danger'}`,
                                    children: [
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                            className: "h4 mb-0",
                                            children: totalAnswer
                                        }),
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                            className: "mb-0",
                                            children: "Jawaban"
                                        })
                                    ]
                                }),
                                totalAnswer !== '0' && /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                                    className: `float-end badge ${mark === 'solved' ? 'bg-codepolitan' : 'bg-info'}`,
                                    children: [
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                            className: "h4 mb-0",
                                            children: totalAnswer
                                        }),
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                            className: "mb-0",
                                            children: "Jawaban"
                                        })
                                    ]
                                })
                            ]
                        })
                    ]
                })
            }),
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                className: "card-body",
                children: [
                    lessonTitle && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                        className: "text-muted",
                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("small", {
                            children: [
                                "Ditanyakan pada: ",
                                lessonTitle
                            ]
                        })
                    }),
                    subject && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                        className: "text-muted h5",
                        children: subject
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_markdown_lib_react_markdown__WEBPACK_IMPORTED_MODULE_6__/* .ReactMarkdown */ .D, {
                        remarkPlugins: [
                            remark_gfm__WEBPACK_IMPORTED_MODULE_3__["default"]
                        ],
                        rehypePlugins: [
                            rehype_raw__WEBPACK_IMPORTED_MODULE_2__["default"]
                        ],
                        components: _Codeblock__WEBPACK_IMPORTED_MODULE_4__/* .CodeBlock */ .d,
                        children: content
                    })
                ]
            })
        ]
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (CardForum);

});

/***/ }),

/***/ 4074:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "d": () => (/* binding */ CodeBlock)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_syntax_highlighter__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(727);
/* harmony import */ var react_syntax_highlighter__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_syntax_highlighter__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_syntax_highlighter_dist_cjs_styles_prism__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(4794);
/* harmony import */ var react_syntax_highlighter_dist_cjs_styles_prism__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_syntax_highlighter_dist_cjs_styles_prism__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_modal_image__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(4781);
/* harmony import */ var react_modal_image__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_modal_image__WEBPACK_IMPORTED_MODULE_3__);




const CodeBlock = {
    p: ({ node , children  })=>{
        if (node?.children[0]?.tagName === "img") {
            const image = node.children[0];
            return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((react_modal_image__WEBPACK_IMPORTED_MODULE_3___default()), {
                className: "img-fluid d-block mx-auto my-4",
                hideDownload: true,
                hideZoom: true,
                small: image.properties.src,
                large: image.properties.src,
                alt: image.properties.alt || "Image"
            }));
        }
        return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
            children: children
        }));
    },
    code ({ node , inline , className , children , ...props }) {
        const match = /language-(\w+)/.exec(className || '');
        return !inline && match ? /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_syntax_highlighter__WEBPACK_IMPORTED_MODULE_1__.Prism, {
            style: react_syntax_highlighter_dist_cjs_styles_prism__WEBPACK_IMPORTED_MODULE_2__.vscDarkPlus,
            language: match[1],
            PreTag: "div",
            ...props,
            children: String(children).replace(/\n$/, '')
        }) : /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("code", {
            className: className,
            ...props,
            children: children
        });
    }
};


/***/ })

};
;