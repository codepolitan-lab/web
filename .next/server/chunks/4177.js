exports.id = 4177;
exports.ids = [4177];
exports.modules = {

/***/ 3860:
/***/ ((module) => {

// Exports
module.exports = {
	"card": "CourseCard_card__YuiMH",
	"pill_index": "CourseCard_pill_index__94Jsf",
	"title": "CourseCard_title__97DeG",
	"total_module": "CourseCard_total_module__HSvRw"
};


/***/ }),

/***/ 1430:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1664);
/* harmony import */ var _CourseCard_module_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3860);
/* harmony import */ var _CourseCard_module_scss__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_CourseCard_module_scss__WEBPACK_IMPORTED_MODULE_2__);



const CourseCard = ({ index , thumbnail , title , totalModule , link  })=>{
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_link__WEBPACK_IMPORTED_MODULE_1__["default"], {
        href: link,
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("a", {
            className: "link",
            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                className: `${(_CourseCard_module_scss__WEBPACK_IMPORTED_MODULE_2___default().card)} card border-0 shadow-sm`,
                children: [
                    index && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: `${(_CourseCard_module_scss__WEBPACK_IMPORTED_MODULE_2___default().pill_index)} d-flex align-items-center`,
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                            className: "text-white mx-auto mb-0",
                            children: index
                        })
                    }),
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: "card-body",
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                className: "card-img rounded-3 img-fluid",
                                loading: "lazy",
                                src: thumbnail || '/assets/img/placeholder.jpg',
                                alt: title
                            }),
                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                className: "my-3",
                                children: [
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h5", {
                                        className: (_CourseCard_module_scss__WEBPACK_IMPORTED_MODULE_2___default().title),
                                        children: title || 'Unamed'
                                    }),
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("p", {
                                        className: `${(_CourseCard_module_scss__WEBPACK_IMPORTED_MODULE_2___default().total_module)} text-muted`,
                                        children: [
                                            totalModule,
                                            " modul"
                                        ]
                                    })
                                ]
                            })
                        ]
                    })
                ]
            })
        })
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (CourseCard);


/***/ }),

/***/ 4079:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1664);
/* harmony import */ var _RoadmapCard_RoadmapCard__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5997);



const RoadmapSection = ()=>{
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("section", {
        className: "section bg-light",
        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
            className: "container p-5",
            children: [
                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                    className: "row",
                    children: [
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "col-md-6 text-center text-md-start",
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
                                className: "section-title",
                                children: "Roadmap Belajar"
                            })
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "col-md-6 text-center text-md-end",
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_link__WEBPACK_IMPORTED_MODULE_1__["default"], {
                                href: "/programmerzamannow/roadmap",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("a", {
                                    className: "btn text-primary rounded-pill mt-3 mt-md-0",
                                    style: {
                                        backgroundColor: '#eee',
                                        fontWeight: 'bold'
                                    },
                                    children: "Lihat Selengkapnya"
                                })
                            })
                        })
                    ]
                }),
                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                    className: "row mt-5",
                    children: [
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "col-md-6 col-lg-3 mb-3",
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_RoadmapCard_RoadmapCard__WEBPACK_IMPORTED_MODULE_2__/* ["default"] */ .Z, {
                                thumbnail: "/assets/img/programmerzamannow/logos/logo-golang.png",
                                title: "Jagoan Golang",
                                description: "Pemrograman Golang dari pemula sampai mahir",
                                link: "/programmerzamannow/roadmap/12"
                            })
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "col-md-6 col-lg-3 mb-3",
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_RoadmapCard_RoadmapCard__WEBPACK_IMPORTED_MODULE_2__/* ["default"] */ .Z, {
                                thumbnail: "/assets/img/programmerzamannow/logos/logo-js.png",
                                title: "Jagoan JavaScript",
                                description: "Pemrograman JavaScript dari pemula sampai mahir",
                                link: "/programmerzamannow/roadmap/13"
                            })
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "col-md-6 col-lg-3 mb-3",
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_RoadmapCard_RoadmapCard__WEBPACK_IMPORTED_MODULE_2__/* ["default"] */ .Z, {
                                thumbnail: "/assets/img/programmerzamannow/logos/logo-php.png",
                                title: "Jagoan PHP",
                                description: "Pemrograman PHP dari pemulai sampai mahir",
                                link: "/programmerzamannow/roadmap/15"
                            })
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "col-md-6 col-lg-3 mb-3",
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_RoadmapCard_RoadmapCard__WEBPACK_IMPORTED_MODULE_2__/* ["default"] */ .Z, {
                                thumbnail: "/assets/img/programmerzamannow/logos/logo-kotlin.png",
                                title: "Jagoan Kotlin",
                                description: "Pemrograman Kotlin dari pemula sampai mahir",
                                link: "/programmerzamannow/roadmap/14"
                            })
                        })
                    ]
                })
            ]
        })
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (RoadmapSection);


/***/ })

};
;