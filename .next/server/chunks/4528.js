exports.id = 4528;
exports.ids = [4528];
exports.modules = {

/***/ 7308:
/***/ ((module) => {

// Exports
module.exports = {
	"nav_link": "Navbar_nav_link__0F9k8",
	"megamenu": "Navbar_megamenu__eZl_d",
	"menu_title": "Navbar_menu_title__iUsul",
	"has_megamenu": "Navbar_has_megamenu__3SU7r",
	"dropdown_menu": "Navbar_dropdown_menu__9N_2f",
	"dropdown_menu_courses": "Navbar_dropdown_menu_courses__1Jnh_"
};


/***/ }),

/***/ 4528:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ Layout_Layout)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: ./src/components/global/Navbar/Navbar.module.scss
var Navbar_module = __webpack_require__(7308);
var Navbar_module_default = /*#__PURE__*/__webpack_require__.n(Navbar_module);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: ./node_modules/next/link.js
var next_link = __webpack_require__(1664);
// EXTERNAL MODULE: external "@fortawesome/react-fontawesome"
var react_fontawesome_ = __webpack_require__(7197);
// EXTERNAL MODULE: external "@fortawesome/free-solid-svg-icons"
var free_solid_svg_icons_ = __webpack_require__(6466);
;// CONCATENATED MODULE: ./src/components/global/Navbar/MegamenuItem/MegamenuItem.js




const MegamenuItem = ({ link , icon , title , subtitle , newBadge , isActive , courses , targetBlank  })=>{
    const { 0: hover , 1: setHover  } = (0,external_react_.useState)(false);
    return(/*#__PURE__*/ jsx_runtime_.jsx(jsx_runtime_.Fragment, {
        children: targetBlank ? /*#__PURE__*/ jsx_runtime_.jsx("a", {
            href: link,
            target: "_blank",
            rel: "noopener noreferrer",
            className: "dropdown-item px-0 py-2",
            onMouseEnter: ()=>setHover(!hover)
            ,
            onMouseLeave: ()=>setHover(!hover)
            ,
            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                className: "d-flex align-items-start",
                children: [
                    icon && /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        className: "my-auto ps-2",
                        children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: "rounded",
                            style: {
                                backgroundColor: hover ? '#14a7a0' : '#eee'
                            },
                            children: /*#__PURE__*/ jsx_runtime_.jsx(react_fontawesome_.FontAwesomeIcon, {
                                className: hover ? "text-light p-2" : "text-primary p-2",
                                size: "lg",
                                fixedWidth: true,
                                icon: icon
                            })
                        })
                    }),
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                        className: "ms-2 my-auto",
                        children: [
                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("h6", {
                                className: isActive ? "text-primary" : undefined,
                                style: {
                                    fontWeight: courses ? 'normal' : 'bold',
                                    fontSize: 'smaller',
                                    marginBottom: 0
                                },
                                children: [
                                    title,
                                    newBadge && /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                        className: "badge rounded-pill bg-danger ms-1",
                                        children: "NEW"
                                    })
                                ]
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("small", {
                                className: "text-muted",
                                style: {
                                    fontSize: 'small'
                                },
                                children: subtitle
                            })
                        ]
                    })
                ]
            })
        }) : /*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
            href: link,
            children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                className: "dropdown-item px-0 py-2",
                onMouseEnter: ()=>setHover(!hover)
                ,
                onMouseLeave: ()=>setHover(!hover)
                ,
                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                    className: "d-flex align-items-start",
                    children: [
                        icon && /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: "my-auto ps-2",
                            children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                className: "rounded",
                                style: {
                                    backgroundColor: hover ? '#14a7a0' : '#eee'
                                },
                                children: /*#__PURE__*/ jsx_runtime_.jsx(react_fontawesome_.FontAwesomeIcon, {
                                    className: hover ? "text-light p-2" : "text-primary p-2",
                                    size: "lg",
                                    fixedWidth: true,
                                    icon: icon
                                })
                            })
                        }),
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: "ms-2 my-auto",
                            children: [
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("h6", {
                                    className: isActive ? "text-primary" : undefined,
                                    style: {
                                        fontWeight: courses ? 'normal' : 'bold',
                                        fontSize: 'smaller',
                                        marginBottom: 0
                                    },
                                    children: [
                                        title,
                                        newBadge && /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                            className: "badge rounded-pill bg-danger ms-1",
                                            children: "NEW"
                                        })
                                    ]
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("small", {
                                    className: "text-muted",
                                    style: {
                                        fontSize: 'small'
                                    },
                                    children: subtitle
                                })
                            ]
                        })
                    ]
                })
            })
        })
    }));
};
/* harmony default export */ const MegamenuItem_MegamenuItem = (MegamenuItem);

// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__(1853);
// EXTERNAL MODULE: external "axios"
var external_axios_ = __webpack_require__(2167);
var external_axios_default = /*#__PURE__*/__webpack_require__.n(external_axios_);
// EXTERNAL MODULE: external "fuse.js"
var external_fuse_js_ = __webpack_require__(2733);
var external_fuse_js_default = /*#__PURE__*/__webpack_require__.n(external_fuse_js_);
;// CONCATENATED MODULE: ./src/components/global/SearchModal/SearchModal.js





const SearchModal = ()=>{
    const { 0: lists , 1: setLists  } = (0,external_react_.useState)([]);
    const { 0: search , 1: setSearch  } = (0,external_react_.useState)('');
    (0,external_react_.useEffect)(()=>{
        const getLists = async ()=>{
            try {
                let response = await external_axios_default().get(`${"https://api.codepolitan.com"}/v1/search`);
                if (response.data.error === "No Content") {
                    setLists(null);
                } else {
                    setLists(response.data);
                }
            } catch (err) {
                return err.message;
            }
        };
        getLists();
    }, []);
    const fuse = new (external_fuse_js_default())(lists, {
        keys: [
            'title', 
        ]
    });
    const results = fuse.search(search);
    const listResults = results.map((result)=>result.item
    );
    const handleSearch = ({ currentTarget ={}  })=>{
        const { value  } = currentTarget;
        setSearch(value);
    };
    // console.log(search);
    return(/*#__PURE__*/ jsx_runtime_.jsx("div", {
        className: "modal fade",
        id: "searchModal",
        tabIndex: -1,
        "aria-labelledby": "searchModalLabel",
        "aria-hidden": "true",
        children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
            className: "modal-dialog modal-xl",
            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                className: "modal-content",
                children: [
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                        className: "modal-header d-flex align-content-start",
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                type: "text",
                                className: "form-control",
                                value: search,
                                onChange: handleSearch,
                                placeholder: "Cari sesuatu disini..."
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                onClick: ()=>setSearch('')
                                ,
                                type: "button",
                                className: "btn btn-light text-muted btn-sm ms-2 border",
                                "data-bs-dismiss": "modal",
                                children: /*#__PURE__*/ jsx_runtime_.jsx("strong", {
                                    children: "Close"
                                })
                            })
                        ]
                    }),
                    search != '' && /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                        className: "modal-body",
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx("h6", {
                                children: "Hasil pencarian :"
                            }),
                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                className: "list-group",
                                children: [
                                    search != '' && listResults.length < 1 && /*#__PURE__*/ (0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                children: "Hasil tidak ditemukan. Silahkan klik link dibawah ini untuk request materi"
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                    href: "https://docs.google.com/forms/d/e/1FAIpQLSd3jkYL123X5LYWlKo7PNNRvYiAu28y9KqJzLA09d4jiVqFig/viewform",
                                                    target: "_blank",
                                                    rel: "noopener noreferrer",
                                                    className: "btn btn-primary btn-sm",
                                                    children: "Request Materi"
                                                })
                                            })
                                        ]
                                    }),
                                    listResults && listResults.filter((item)=>!item.title.includes('KelasFullstack.id')
                                    ).map((item, index)=>{
                                        let prefix = '';
                                        if (item?.title?.includes('Course')) {
                                            prefix = '/course/intro/';
                                        } else if (item?.title?.includes('Roadmap')) {
                                            prefix = '/roadmap/';
                                        } else if (item?.title?.includes('Article')) {
                                            prefix = '/blog/';
                                        }
                                        ;
                                        return(/*#__PURE__*/ jsx_runtime_.jsx("div", {
                                            onClick: ()=>wa.track({
                                                    name: 'Search Klik'
                                                })
                                            ,
                                            "data-bs-dismiss": "modal",
                                            children: /*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                                                href: prefix + item.slug,
                                                children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                    className: "list-group-item list-group-item-action border-0 rounded-pill text-muted",
                                                    "aria-current": "true",
                                                    children: item.title
                                                }, index)
                                            })
                                        }, index));
                                    }).slice(0, 10)
                                ]
                            })
                        ]
                    })
                ]
            })
        })
    }));
};
/* harmony default export */ const SearchModal_SearchModal = (SearchModal);

// EXTERNAL MODULE: external "@fortawesome/free-brands-svg-icons"
var free_brands_svg_icons_ = __webpack_require__(5368);
// EXTERNAL MODULE: external "@fortawesome/free-regular-svg-icons"
var free_regular_svg_icons_ = __webpack_require__(197);
;// CONCATENATED MODULE: ./src/components/global/Navbar/navmenus.js



const courseMenus = {
    categories: [
        {
            title: 'Web Development',
            queryType: 'web'
        },
        {
            title: 'Mobile Development',
            queryType: 'mobile'
        },
        {
            title: 'Studi Kasus',
            queryType: 'project'
        },
        {
            title: 'Fundamental',
            queryType: 'fundamental'
        },
        {
            title: 'Pemula',
            queryType: 'beginner'
        },
        {
            title: 'Menengah',
            queryType: 'intermediate'
        },
        {
            title: 'Framework',
            queryType: 'framework'
        },
        {
            title: 'Front End',
            queryType: 'frontend'
        },
        {
            title: 'Back End',
            queryType: 'backend'
        }, 
    ],
    popularTechnologies: [
        {
            title: 'Laravel',
            queryType: 'laravel'
        },
        {
            title: 'PHP',
            queryType: 'php'
        },
        {
            title: 'Kotlin',
            queryType: 'kotlin'
        },
        {
            title: 'Android',
            queryType: 'android'
        },
        {
            title: 'Javascript',
            queryType: 'javascript'
        },
        {
            title: 'Wordpress',
            queryType: 'wordpress'
        },
        {
            title: 'Database',
            queryType: 'database'
        },
        {
            title: 'Java',
            queryType: 'java'
        },
        {
            title: 'Go-Lang',
            queryType: 'golang'
        }
    ],
    others: [
        {
            title: 'Kelas Terbaru',
            subtitle: 'Kelas Online Terbaru',
            icon: free_solid_svg_icons_.faCircleCheck,
            link: '/library'
        },
        {
            title: 'Kelas Gratis',
            subtitle: 'Kelas Online Gratis',
            icon: free_solid_svg_icons_.faTag,
            link: '/library?type=free'
        },
        // {
        //     title: 'Flash Sale',
        //     subtitle: 'Kelas Online Promo',
        //     icon: faBoltLightning,
        //     link: '/flashsale'
        // },
        {
            title: 'Popular',
            subtitle: 'Kelas Online Popular',
            icon: free_regular_svg_icons_.faBookmark,
            link: '/library?type=popular'
        },
        {
            title: 'Mentor',
            subtitle: 'Daftar Mentor Kelas',
            icon: free_solid_svg_icons_.faChalkboardTeacher,
            link: '/mentor'
        },
        {
            title: 'Roadmap',
            subtitle: 'Daftar Roadmap Belajar',
            icon: free_solid_svg_icons_.faMap,
            link: '/roadmap'
        }, 
    ]
};
const exploreMenus = [
    // {
    //     title: 'Karier',
    //     subtitle: 'Temukan Kariermu',
    //     icon: faRoad,
    //     link: '/karier'
    // },
    {
        title: 'Tutorial & Artikel',
        subtitle: 'Temukan Artikel Menarik',
        icon: free_solid_svg_icons_.faNewspaper,
        link: '/tutorials'
    },
    // {
    //     title: 'Podcast',
    //     subtitle: 'Podcast seputar pemrograman',
    //     icon: faMicrophone,
    //     link: '/podcasts'
    // },
    {
        title: 'Events',
        subtitle: 'Ikuti Berbagai Event',
        icon: free_solid_svg_icons_.faCalendar,
        link: '/events'
    },
    // {
    //     title: 'Event',
    //     subtitle: 'Temukan Event Menarik',
    //     icon: faCalendarAlt,
    //     link: '/events-and-info'
    // },
    // {
    //     title: 'Beasiswa',
    //     subtitle: 'Program Beasiswa',
    //     icon: faGraduationCap,
    //     link: '/beasiswa'
    // },
    // {
    //     title: 'Discord',
    //     subtitle: 'Komunitas Discord',
    //     icon: faDiscord,
    //     link: '/program/join-discord'
    // },
    {
        title: 'Discussions',
        subtitle: 'Diskusi antar Programmer',
        icon: free_solid_svg_icons_.faComments,
        link: '/forum'
    },
    // {
    //     title: 'Exercise',
    //     subtitle: 'Asah Pengtahuanmu di Exercise',
    //     icon: faEdit,
    //     link: '/exercise'
    // },
    {
        title: 'Leaderboard',
        subtitle: 'Ranking siswa Codepolitan',
        icon: free_solid_svg_icons_.faTrophy,
        link: '/leaderboard'
    }, 
];
const programMenus = [
    {
        title: 'KelasFullstack.id',
        subtitle: 'Belajar FullStack Web Development from A to Z',
        icon: free_solid_svg_icons_.faLayerGroup,
        link: 'https://kelasfullstack.id/',
        targetBlank: true
    },
    {
        title: 'PZN EXPERT',
        subtitle: 'Kuasai Skill Coding ala Startup Unicorn',
        icon: free_solid_svg_icons_.faGlasses,
        link: 'https://pzn.codepolitan.com/',
        targetBlank: true
    }, 
];
const partnershipMenus = [
    {
        title: 'For Company',
        subtitle: 'Solusi tepat untuk perusahaan',
        icon: free_solid_svg_icons_.faBuilding,
        link: '/for-company'
    },
    {
        title: 'For Mentor',
        subtitle: 'Peluang penghasilan untuk mentor',
        icon: free_solid_svg_icons_.faChalkboardTeacher,
        link: '/for-mentor'
    }, 
];


;// CONCATENATED MODULE: ./src/components/global/Navbar/Navbar.js




 // Import the FontAwesomeIcon component
 // import the icons you need




const Navbar = ()=>{
    const router = (0,router_.useRouter)();
    const { 0: icon1 , 1: setIcon  } = (0,external_react_.useState)(false);
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx("nav", {
                id: "Navbar",
                className: "navbar navbar-expand-xl navbar-light bg-white shadow fixed-top px-lg-5",
                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                    className: "container px-lg-5",
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                            href: "/",
                            children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                className: "navbar-brand",
                                children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                    height: "45",
                                    width: "auto",
                                    src: "/assets/img/codepolitan-logo.png",
                                    alt: "Codepolitan"
                                })
                            })
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx("button", {
                            onClick: ()=>setIcon((icon)=>!icon
                                )
                            ,
                            className: "navbar-toggler shadow-none border-0",
                            type: "button",
                            "data-bs-toggle": "collapse",
                            "data-bs-target": "#navbarSupportedContent",
                            "aria-controls": "navbarSupportedContent",
                            "aria-expanded": "false",
                            "aria-label": "Toggle navigation",
                            children: icon1 ? icon1 && /*#__PURE__*/ jsx_runtime_.jsx(react_fontawesome_.FontAwesomeIcon, {
                                icon: free_solid_svg_icons_.faTimes
                            }) : /*#__PURE__*/ jsx_runtime_.jsx(react_fontawesome_.FontAwesomeIcon, {
                                icon: free_solid_svg_icons_.faBars
                            })
                        }),
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: `collapse navbar-collapse ${(Navbar_module_default()).navbar_collapse}`,
                            id: "navbarSupportedContent",
                            children: [
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                    className: "navbar-nav mb-2 mb-lg-0 ms-auto",
                                    children: [
                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                            className: "nav-item input-group",
                                            style: {
                                                minWidth: '15em'
                                            },
                                            children: [
                                                /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                    className: "input-group-text bg-white",
                                                    children: /*#__PURE__*/ jsx_runtime_.jsx(react_fontawesome_.FontAwesomeIcon, {
                                                        className: "text-muted",
                                                        icon: free_solid_svg_icons_.faSearch
                                                    })
                                                }),
                                                /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                    className: "form-control shadow-none border-start-0 text-muted text-start",
                                                    role: "button",
                                                    "data-bs-toggle": "modal",
                                                    "data-bs-target": "#searchModal",
                                                    children: "Cari disini..."
                                                })
                                            ]
                                        }),
                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                            className: "nav-item dropdown mx-2",
                                            children: [
                                                /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                    className: `nav-link dropdown-toggle ${(Navbar_module_default()).nav_link}`,
                                                    href: "#",
                                                    id: "navbarDropdown",
                                                    role: "button",
                                                    "data-bs-toggle": "dropdown",
                                                    "aria-expanded": "false",
                                                    children: "Courses"
                                                }),
                                                /*#__PURE__*/ jsx_runtime_.jsx("ul", {
                                                    className: `dropdown-menu ${(Navbar_module_default()).dropdown_menu_courses} dropdown-content shadow`,
                                                    "aria-labelledby": "navbarDropdown",
                                                    children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                        className: "row mx-3 my-2",
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                className: "col-lg-4",
                                                                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                                    className: "row",
                                                                    children: [
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                            className: "col-12 mb-3",
                                                                            children: /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                                children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                                    className: "py-1 border-bottom small fw-bold",
                                                                                    children: "Kategori"
                                                                                })
                                                                            })
                                                                        }),
                                                                        courseMenus.categories.map((menu, index)=>/*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                                className: "col-12 p-0 mb-2",
                                                                                children: /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                                    children: /*#__PURE__*/ jsx_runtime_.jsx(MegamenuItem_MegamenuItem, {
                                                                                        title: menu.title,
                                                                                        link: `/library?type=${menu.queryType}`,
                                                                                        isActive: router.pathname === '/library' && router.query.type === menu.queryType,
                                                                                        courses: true
                                                                                    })
                                                                                })
                                                                            }, index)
                                                                        )
                                                                    ]
                                                                })
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                className: "col-lg-4",
                                                                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                                    className: "row",
                                                                    children: [
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                            className: "col-12 mb-3",
                                                                            children: /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                                children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                                    className: "py-1 border-bottom small fw-bold",
                                                                                    children: "Teknologi Populer"
                                                                                })
                                                                            })
                                                                        }),
                                                                        courseMenus.popularTechnologies.map((menu, index)=>/*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                                className: "col-12 p-0 mb-2",
                                                                                children: /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                                    children: /*#__PURE__*/ jsx_runtime_.jsx(MegamenuItem_MegamenuItem, {
                                                                                        title: menu.title,
                                                                                        link: `/library?type=${menu.queryType}`,
                                                                                        isActive: router.pathname === '/library' && router.query.type === menu.queryType,
                                                                                        courses: true
                                                                                    })
                                                                                })
                                                                            }, index)
                                                                        )
                                                                    ]
                                                                })
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                className: "col-lg-4 border-lg-start",
                                                                children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                    className: "row",
                                                                    children: courseMenus.others.map((menu, index)=>/*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                            className: "col-12 p-0",
                                                                            children: /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                                children: /*#__PURE__*/ jsx_runtime_.jsx(MegamenuItem_MegamenuItem, {
                                                                                    icon: menu.icon,
                                                                                    title: menu.title,
                                                                                    subtitle: menu.subtitle,
                                                                                    link: menu.link,
                                                                                    isActive: router.pathname === menu.link
                                                                                })
                                                                            })
                                                                        }, index)
                                                                    )
                                                                })
                                                            })
                                                        ]
                                                    })
                                                })
                                            ]
                                        }),
                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                            className: "nav-item dropdown mx-2",
                                            children: [
                                                /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                    className: `nav-link dropdown-toggle ${(Navbar_module_default()).nav_link}`,
                                                    href: "#",
                                                    id: "navbarDropdown",
                                                    role: "button",
                                                    "data-bs-toggle": "dropdown",
                                                    "aria-expanded": "false",
                                                    children: "Explore"
                                                }),
                                                /*#__PURE__*/ jsx_runtime_.jsx("ul", {
                                                    className: `dropdown-menu ${(Navbar_module_default()).dropdown_menu} dropdown-content shadow`,
                                                    style: {
                                                        left: '-120%'
                                                    },
                                                    "aria-labelledby": "navbarDropdown",
                                                    children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                        className: "row m-3",
                                                        children: exploreMenus.map((menu, index)=>/*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                className: "col-12 col-lg-6 p-0",
                                                                children: /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                    children: /*#__PURE__*/ jsx_runtime_.jsx(MegamenuItem_MegamenuItem, {
                                                                        icon: menu.icon,
                                                                        title: menu.title,
                                                                        subtitle: menu.subtitle,
                                                                        link: menu.link,
                                                                        isActive: router.pathname === menu.link
                                                                    })
                                                                })
                                                            }, index)
                                                        )
                                                    })
                                                })
                                            ]
                                        }),
                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                            className: "nav-item dropdown mx-2 program",
                                            children: [
                                                /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                    className: `nav-link dropdown-toggle ${(Navbar_module_default()).nav_link}`,
                                                    href: "#",
                                                    id: "navbarDropdown",
                                                    role: "button",
                                                    "data-bs-toggle": "dropdown",
                                                    "aria-expanded": "false",
                                                    children: "Program"
                                                }),
                                                /*#__PURE__*/ jsx_runtime_.jsx("ul", {
                                                    className: `dropdown-menu ${(Navbar_module_default()).dropdown_menu} dropdown-content shadow`,
                                                    style: {
                                                        minWidth: '300px'
                                                    },
                                                    "aria-labelledby": "navbarDropdown",
                                                    children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                        className: "row m-3",
                                                        children: programMenus.map((menu, index)=>/*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                className: "col-12 p-0",
                                                                children: /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                    children: /*#__PURE__*/ jsx_runtime_.jsx(MegamenuItem_MegamenuItem, {
                                                                        icon: menu.icon,
                                                                        title: menu.title,
                                                                        subtitle: menu.subtitle,
                                                                        link: menu.link,
                                                                        isActive: router.pathname === menu.link,
                                                                        targetBlank: menu.targetBlank
                                                                    })
                                                                })
                                                            }, index)
                                                        )
                                                    })
                                                })
                                            ]
                                        }),
                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                            className: "nav-item dropdown mx-2 partnership",
                                            children: [
                                                /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                    className: `nav-link dropdown-toggle ${(Navbar_module_default()).nav_link}`,
                                                    href: "#",
                                                    id: "navbarDropdown",
                                                    role: "button",
                                                    "data-bs-toggle": "dropdown",
                                                    "aria-expanded": "false",
                                                    children: "Partnership"
                                                }),
                                                /*#__PURE__*/ jsx_runtime_.jsx("ul", {
                                                    className: `dropdown-menu ${(Navbar_module_default()).dropdown_menu} dropdown-content shadow`,
                                                    style: {
                                                        minWidth: '300px'
                                                    },
                                                    "aria-labelledby": "navbarDropdown",
                                                    children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                        className: "row m-3",
                                                        children: partnershipMenus.map((menu, index)=>/*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                className: "col-12 p-0",
                                                                children: /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                    children: /*#__PURE__*/ jsx_runtime_.jsx(MegamenuItem_MegamenuItem, {
                                                                        icon: menu.icon,
                                                                        title: menu.title,
                                                                        subtitle: menu.subtitle,
                                                                        link: menu.link,
                                                                        isActive: router.pathname === menu.link,
                                                                        targetBlank: menu.targetBlank
                                                                    })
                                                                })
                                                            }, index)
                                                        )
                                                    })
                                                })
                                            ]
                                        })
                                    ]
                                }),
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                    className: "d-grid gap-2 ms-lg-2 d-lg-block",
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                            className: `${(Navbar_module_default()).nav_link} btn btn-light px-3 mx-2 m-lg-1`,
                                            href: "https://dashboard.codepolitan.com/login/",
                                            children: "Login"
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                            className: `${(Navbar_module_default()).nav_link} btn btn-primary px-3 mx-2 m-lg-1`,
                                            href: "https://dashboard.codepolitan.com/register/",
                                            children: "Register"
                                        })
                                    ]
                                })
                            ]
                        })
                    ]
                })
            }),
            /*#__PURE__*/ jsx_runtime_.jsx(SearchModal_SearchModal, {})
        ]
    }));
};
/* harmony default export */ const Navbar_Navbar = (Navbar);

;// CONCATENATED MODULE: ./src/components/global/Footer/Footer.js

 // Import the FontAwesomeIcon component
 // import the icons you need

const Footer = ()=>{
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)("footer", {
        id: "Footer",
        style: {
            borderTop: '1px solid #ddd'
        },
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                className: "container px-5 mt-5",
                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                    className: "row justify-content-between",
                    children: [
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: "col-12 col-lg-4",
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                    height: "45",
                                    width: "auto",
                                    className: "mb-4",
                                    src: "/assets/img/codepolitan-logo.png",
                                    alt: "Codepolitan"
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("h6", {
                                    className: "fw-bolder",
                                    children: "ADDRESS"
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                    className: "text-muted",
                                    children: "Komp. Permata, Jl. Permata Raya I No.3, Tanimulya, Ngamprah, Kabupaten Bandung Barat, Jawa Barat - 40552"
                                })
                            ]
                        }),
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: "col-12 col-lg-2",
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("h6", {
                                    className: "fw-bolder",
                                    children: "JOIN US"
                                }),
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("nav", {
                                    className: "nav flex-column my-3",
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                                            href: "/about",
                                            children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                className: "link",
                                                children: "About Us"
                                            })
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                                            href: "/how-to-learn",
                                            children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                className: "link",
                                                children: "How to Learn"
                                            })
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                                            href: "/faq",
                                            children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                className: "link",
                                                children: "FAQ"
                                            })
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                                            href: "/terms-and-condition",
                                            children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                className: "link",
                                                children: "Terms & Conditions"
                                            })
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                                            href: "/privacy-policy",
                                            children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                className: "link",
                                                children: "Privacy Policy"
                                            })
                                        })
                                    ]
                                })
                            ]
                        }),
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: "col-12 col-lg-2",
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("h6", {
                                    className: "fw-bolder",
                                    children: "COMMUNITY"
                                }),
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("nav", {
                                    className: "nav flex-column my-3",
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                                            href: "/events",
                                            children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                className: "link",
                                                children: "Events"
                                            })
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                                            href: "/tutorials",
                                            children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                className: "link",
                                                children: "Blog"
                                            })
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                                            href: "/leaderboard",
                                            children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                className: "link",
                                                children: "Leaderboard"
                                            })
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                                            href: "/forum",
                                            children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                className: "link",
                                                children: "Questions"
                                            })
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                                            href: "/hall-of-fame",
                                            children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                className: "link",
                                                children: "Hall of Fame"
                                            })
                                        })
                                    ]
                                })
                            ]
                        }),
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: "col-12 col-lg-2",
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("h6", {
                                    className: "fw-bolder",
                                    children: "PARTNERSHIP"
                                }),
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("nav", {
                                    className: "nav flex-column my-3",
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                                            href: "/for-company",
                                            children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                className: "link",
                                                children: "For Company"
                                            })
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                                            href: "/for-mentor",
                                            children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                className: "link",
                                                children: "For Mentor"
                                            })
                                        })
                                    ]
                                })
                            ]
                        })
                    ]
                })
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("hr", {
                style: {
                    height: '1px',
                    color: '#ccc',
                    backgroundColor: '#ccc'
                }
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                className: "container px-5",
                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                    className: "row",
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: " text-center text-md-start",
                            children: /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                className: "text-muted",
                                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("small", {
                                    children: [
                                        "\xa9 ",
                                        new Date().getFullYear(),
                                        " CodePolitan. All rights reserved. Made with ❤️ in Indonesia."
                                    ]
                                })
                            })
                        }),
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: " text-center text-md-end mb-2",
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                    className: "footer-socmed-link",
                                    title: "Tiktok",
                                    href: "https://www.tiktok.com/@codepolitan.com",
                                    children: /*#__PURE__*/ jsx_runtime_.jsx(react_fontawesome_.FontAwesomeIcon, {
                                        icon: free_brands_svg_icons_.faTiktok
                                    })
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                    className: "footer-socmed-link",
                                    title: "Facebook",
                                    href: "https://www.facebook.com/codepolitan/",
                                    children: /*#__PURE__*/ jsx_runtime_.jsx(react_fontawesome_.FontAwesomeIcon, {
                                        icon: free_brands_svg_icons_.faFacebook
                                    })
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                    className: "footer-socmed-link",
                                    title: "Instagram",
                                    href: "https://www.instagram.com/codepolitan/",
                                    children: /*#__PURE__*/ jsx_runtime_.jsx(react_fontawesome_.FontAwesomeIcon, {
                                        icon: free_brands_svg_icons_.faInstagram
                                    })
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                    className: "footer-socmed-link",
                                    title: "Twitter",
                                    href: "https://twitter.com/codepolitan",
                                    children: /*#__PURE__*/ jsx_runtime_.jsx(react_fontawesome_.FontAwesomeIcon, {
                                        icon: free_brands_svg_icons_.faTwitter
                                    })
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                    className: "footer-socmed-link",
                                    title: "Linkedin",
                                    href: "https://www.linkedin.com/company/codepolitan",
                                    children: /*#__PURE__*/ jsx_runtime_.jsx(react_fontawesome_.FontAwesomeIcon, {
                                        icon: free_brands_svg_icons_.faLinkedin
                                    })
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                    className: "footer-socmed-link",
                                    title: "Youtube",
                                    href: "https://www.youtube.com/c/codepolitan",
                                    children: /*#__PURE__*/ jsx_runtime_.jsx(react_fontawesome_.FontAwesomeIcon, {
                                        icon: free_brands_svg_icons_.faYoutube
                                    })
                                })
                            ]
                        })
                    ]
                })
            })
        ]
    }));
};
/* harmony default export */ const Footer_Footer = (Footer);

;// CONCATENATED MODULE: ./src/components/global/Layout/Layout.js



const Layout = ({ children  })=>{
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx(Navbar_Navbar, {}),
            /*#__PURE__*/ jsx_runtime_.jsx("main", {
                id: "Main",
                style: {
                    minHeight: '100vh'
                },
                children: children
            }),
            /*#__PURE__*/ jsx_runtime_.jsx(Footer_Footer, {})
        ]
    }));
};
/* harmony default export */ const Layout_Layout = (Layout);


/***/ })

};
;