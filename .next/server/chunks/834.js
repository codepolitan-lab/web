"use strict";
exports.id = 834;
exports.ids = [834];
exports.modules = {

/***/ 834:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);

const SectionRoadmapList = ({ title , subtitle  })=>{
    const roadmaps = [
        {
            thumbnail: '/assets/img/forschool/icon-db.png',
            title: 'Database Engineer',
            description: 'Siswa dapat menjadi database enginer dengan mempelajari Query RDBMS MySQL, MongoDB dan lain lain.'
        },
        {
            thumbnail: '/assets/img/forschool/icon-web.png',
            title: 'Fullstack Programmer',
            description: 'Siswa dapat menjadi fullstack programmer dengan belajar bahasa pemrograman untuk Frontend, Backend dan Database.'
        },
        {
            thumbnail: '/assets/img/forschool/icon-android.png',
            title: 'Android Developer',
            description: 'Siswa dapat menjadi Android developer dengan mempelajari bahasa pemrograman Kotlin, React Native, Fluter dan lain lain.'
        },
        {
            thumbnail: '/assets/img/forschool/icon-frontend.png',
            title: 'Frontend Developer',
            description: 'Siswa dapat menjadi Frontend Developer dengan mempelajari bahasa pemrograman, HTML, CSS, dan Javascript'
        },
        {
            thumbnail: '/assets/img/forschool/icon-backend.png',
            title: 'Backend Developer',
            description: 'Siswa dapat menjadi Backend Developer dengan mempelajari PHP, Javascript dan lain lain.'
        },
        {
            thumbnail: '/assets/img/forschool/icon-uiux.png',
            title: 'UI/UX Designer',
            description: 'Siswa dapat menjadi UI/UX designer dengan mempelajari tools Figma atau Adobe XD.'
        },
        {
            thumbnail: '/assets/img/forschool/icon-cloud.png',
            title: 'Cloud Computing',
            description: 'Siswa dapat mempelajari Cloud Computing dengan mempelajari layanan hosting seperti Firebase.'
        },
        {
            thumbnail: '/assets/img/forschool/icon-security.png',
            title: 'Cyber Security',
            description: 'Siswa dapat mempelajari Cyber Security dengan mempelajari Ethical Hacking'
        }, 
    ];
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("section", {
        className: "section",
        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
            className: "container p-4 p-lg-5",
            children: [
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: "row mb-5",
                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: "col text-center",
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
                                className: "section-title",
                                children: title
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                className: "text-muted",
                                children: subtitle
                            })
                        ]
                    })
                }),
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: "row",
                    children: roadmaps.map((roadmap, index)=>{
                        return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                            className: "col-md-6 col-lg-3 my-3 text-center",
                            children: [
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                    className: "img-fluid",
                                    src: roadmap.thumbnail,
                                    alt: roadmap.title
                                }),
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h5", {
                                    className: "section-title my-3",
                                    children: roadmap.title
                                }),
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                    className: "text-muted",
                                    children: roadmap.description
                                })
                            ]
                        }, index));
                    })
                })
            ]
        })
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (SectionRoadmapList);


/***/ })

};
;