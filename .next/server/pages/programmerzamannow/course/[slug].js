(() => {
var exports = {};
exports.id = 7644;
exports.ids = [7644];
exports.modules = {

/***/ 9308:
/***/ ((module) => {

// Exports
module.exports = {
	"del": "SidebarCard_del__xJ17M",
	"rp": "SidebarCard_rp__i5G7U",
	"price": "SidebarCard_price__dXO_R"
};


/***/ }),

/***/ 4639:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ _slug_),
  "getServerSideProps": () => (/* binding */ getServerSideProps)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: external "@fortawesome/free-solid-svg-icons"
var free_solid_svg_icons_ = __webpack_require__(6466);
// EXTERNAL MODULE: external "@fortawesome/react-fontawesome"
var react_fontawesome_ = __webpack_require__(7197);
// EXTERNAL MODULE: external "axios"
var external_axios_ = __webpack_require__(2167);
var external_axios_default = /*#__PURE__*/__webpack_require__.n(external_axios_);
// EXTERNAL MODULE: external "next/head"
var head_ = __webpack_require__(968);
var head_default = /*#__PURE__*/__webpack_require__.n(head_);
// EXTERNAL MODULE: ./node_modules/next/link.js
var next_link = __webpack_require__(1664);
// EXTERNAL MODULE: external "react-simple-star-rating"
var external_react_simple_star_rating_ = __webpack_require__(7386);
// EXTERNAL MODULE: ./src/components/global/Layout/Layout.js + 5 modules
var Layout = __webpack_require__(4528);
// EXTERNAL MODULE: ./src/utils/helper/index.js
var helper = __webpack_require__(4986);
// EXTERNAL MODULE: ./src/components/programmerzamannow/SidebarCard/SidebarCard.module.scss
var SidebarCard_module = __webpack_require__(9308);
var SidebarCard_module_default = /*#__PURE__*/__webpack_require__.n(SidebarCard_module);
;// CONCATENATED MODULE: ./src/components/programmerzamannow/SidebarCard/TabSidebarContent/index.js




const TabSidebarContent = ({ normalPrice , retailPrice , discountPrice , link  })=>{
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
        children: [
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                className: "p-2 text-muted text-center",
                children: [
                    normalPrice && /*#__PURE__*/ (0,jsx_runtime_.jsxs)("del", {
                        className: (SidebarCard_module_default()).del,
                        children: [
                            "Rp. ",
                            normalPrice
                        ]
                    }),
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("span", {
                        className: (SidebarCard_module_default()).price,
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                className: (SidebarCard_module_default()).rp,
                                children: "Rp."
                            }),
                            retailPrice
                        ]
                    })
                ]
            }),
            discountPrice && /*#__PURE__*/ (0,jsx_runtime_.jsxs)("span", {
                className: "badge bg-pink",
                style: {
                    borderTopLeftRadius: 0,
                    borderBottomLeftRadius: 0
                },
                children: [
                    "Hemat Rp. ",
                    discountPrice
                ]
            }),
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                className: "p-2 text-muted",
                style: {
                    fontSize: 'smaller'
                },
                children: [
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                        className: "d-flex align-items-start",
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx(react_fontawesome_.FontAwesomeIcon, {
                                className: "my-auto me-2 text-primary",
                                icon: free_solid_svg_icons_.faCheckCircle
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                children: "Klaim Sertifikat Digital"
                            })
                        ]
                    }),
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                        className: "d-flex align-items-start",
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx(react_fontawesome_.FontAwesomeIcon, {
                                className: "my-auto me-2 text-primary",
                                icon: free_solid_svg_icons_.faCheckCircle
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                children: "Forum Diskusi Tanya Jawab"
                            })
                        ]
                    }),
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                        className: "d-flex align-items-start",
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx(react_fontawesome_.FontAwesomeIcon, {
                                className: "my-auto me-2 text-primary",
                                icon: free_solid_svg_icons_.faCheckCircle
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                children: "Fitur Lowongan Kerja Talent Hub"
                            })
                        ]
                    })
                ]
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                className: "d-grid gap-2 p-2",
                children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                    className: "btn btn-primary",
                    href: `https://pay.codepolitan.com/?slug=${link}`,
                    children: "Beli Sekarang"
                })
            })
        ]
    }));
};
/* harmony default export */ const SidebarCard_TabSidebarContent = (TabSidebarContent);

;// CONCATENATED MODULE: ./src/components/programmerzamannow/SidebarCard/SidebarCard.js


const SidebarCard = ()=>{
    return(/*#__PURE__*/ jsx_runtime_.jsx("div", {
        className: "card border-0 sticky-top shadow-sm",
        style: {
            top: '100px',
            borderRadius: '10px'
        },
        children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
            className: "card-body p-0",
            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                id: "sidebarPzn",
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx("ul", {
                        className: "nav nav-tabs nav-justified bg-light mb-1",
                        id: "pills-tab",
                        role: "tablist",
                        children: /*#__PURE__*/ jsx_runtime_.jsx("li", {
                            className: "nav-item",
                            role: "presentation",
                            children: /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                className: "nav-link active",
                                id: "pills-bulanan-tab",
                                "data-bs-toggle": "pill",
                                "data-bs-target": "#pills-bulanan",
                                type: "button",
                                role: "tab",
                                "aria-controls": "pills-bulanan",
                                "aria-selected": "true",
                                children: "Pilih Paket"
                            })
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        className: "tab-content",
                        id: "pills-tabContent",
                        children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: "tab-pane show active",
                            id: "pills-bulanan",
                            role: "tabpanel",
                            "aria-labelledby": "pills-bulanan-tab",
                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                id: "tabSidebarPzn",
                                className: "py-2",
                                children: [
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                        className: "nav nav-pills nav-justified mx-2",
                                        id: "pills-tab",
                                        role: "tablist",
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                className: "nav-item",
                                                role: "presentation",
                                                children: /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                                    className: "nav-link rounded-pill active",
                                                    id: "pills-satubulan-tab",
                                                    "data-bs-toggle": "pill",
                                                    "data-bs-target": "#pills-satubulan",
                                                    type: "button",
                                                    role: "tab",
                                                    "aria-controls": "pills-satubulan",
                                                    "aria-selected": "true",
                                                    children: "1 Bulan"
                                                })
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                className: "nav-item",
                                                role: "presentation",
                                                children: /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                                    className: "nav-link rounded-pill",
                                                    id: "pills-tigabulan-tab",
                                                    "data-bs-toggle": "pill",
                                                    "data-bs-target": "#pills-tigabulan",
                                                    type: "button",
                                                    role: "tab",
                                                    "aria-controls": "pills-tigabulan",
                                                    "aria-selected": "false",
                                                    children: "4 Bulan"
                                                })
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                className: "nav-item",
                                                role: "presentation",
                                                children: /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                                    className: "nav-link rounded-pill",
                                                    id: "pills-enambulan-tab",
                                                    "data-bs-toggle": "pill",
                                                    "data-bs-target": "#pills-enambulan",
                                                    type: "button",
                                                    role: "tab",
                                                    "aria-controls": "pills-enambulan",
                                                    "aria-selected": "false",
                                                    children: "6 Bulan"
                                                })
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: "tab-content",
                                        id: "pills-tabContent",
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                className: "tab-pane show active",
                                                id: "pills-satubulan",
                                                role: "tabpanel",
                                                "aria-labelledby": "pills-satubulan-tab",
                                                children: /*#__PURE__*/ jsx_runtime_.jsx(SidebarCard_TabSidebarContent, {
                                                    retailPrice: "129.000",
                                                    link: "programmer-zaman-now-cicilan"
                                                })
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                className: "tab-pane",
                                                id: "pills-tigabulan",
                                                role: "tabpanel",
                                                "aria-labelledby": "pills-tigabulan-tab",
                                                children: /*#__PURE__*/ jsx_runtime_.jsx(SidebarCard_TabSidebarContent, {
                                                    normalPrice: "516.000",
                                                    retailPrice: "349.000",
                                                    discountPrice: "167.000",
                                                    link: "programmer-zaman-now-ngebut"
                                                })
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                className: "tab-pane",
                                                id: "pills-enambulan",
                                                role: "tabpanel",
                                                "aria-labelledby": "pills-enambulan-tab",
                                                children: /*#__PURE__*/ jsx_runtime_.jsx(SidebarCard_TabSidebarContent, {
                                                    normalPrice: "774.000",
                                                    retailPrice: "499.000",
                                                    discountPrice: "275.000",
                                                    link: "programmer-zaman-now-reguler"
                                                })
                                            })
                                        ]
                                    })
                                ]
                            })
                        })
                    })
                ]
            })
        })
    }));
};
/* harmony default export */ const SidebarCard_SidebarCard = (SidebarCard);

;// CONCATENATED MODULE: ./src/pages/programmerzamannow/course/[slug].js











const getServerSideProps = async (context)=>{
    try {
        const { slug  } = context.query;
        const courseDetailRes = await external_axios_default().get(`${"https://api.codepolitan.com"}/course/detail/${slug}`);
        const testimonyRes = await external_axios_default().get(`${"https://apps.codepolitan.com"}/api/feedback/course/${slug}`);
        const [courseDetail, testimony] = await Promise.all([
            courseDetailRes.data,
            testimonyRes.data
        ]);
        return {
            props: {
                courseDetail,
                testimony
            }
        };
    } catch (err) {
        console.log(err);
        return {
            notFound: true
        };
    }
};
const CourseDetail = ({ courseDetail , testimony  })=>{
    const { 0: limit , 1: setLimit  } = (0,external_react_.useState)(5);
    let courses = courseDetail.course;
    let authors = courseDetail.authors;
    let lessons = courseDetail.lessons;
    const ratingValue = (0,helper/* countRates */.k7)(testimony);
    const lessonList = (0,helper/* getLessons */.tj)(lessons);
    const lessonTopic = lessonList.lessonTopic;
    const lessonContent = lessonList.lessonContent;
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
        children: [
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)((head_default()), {
                children: [
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("title", {
                        children: [
                            courses.title,
                            " Programmer Zaman Now - Codepolitan"
                        ]
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        name: "title",
                        content: "Kursus Coding Online Programmer Zaman Now Selama 1 Semester"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        name: "description",
                        content: "Saya Eko Kurniawan Khannedy, Technical Architect di Blibli.com. Dengan pengalaman saya lebih dari 15 tahun berkarya dan 11 tahun berkarir di bidang programming akan membantu kamu belajar coding lebih cepat dan terarah untuk menjadi Programmer Zaman Now"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "og:type",
                        content: "website"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "og:url",
                        content: "https://codepolitan.com/program/mobirise"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "og:title",
                        content: "Kursus Coding Online Programmer Zaman Now Selama 1 Semester"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "og:description",
                        content: "Saya Eko Kurniawan Khannedy, Technical Architect di Blibli.com. Dengan pengalaman saya lebih dari 15 tahun berkarya dan 11 tahun berkarir di bidang programming akan membantu kamu belajar coding lebih cepat dan terarah untuk menjadi Programmer Zaman Now"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "og:image",
                        content: "/assets/img/programmerzamannow/og-image-pzn.jpg"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "twitter:card",
                        content: "summary_large_image"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "twitter:url",
                        content: "https://codepolitan.com/program/mobirise"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "twitter:title",
                        content: "Kursus Coding Online Programmer Zaman Now Selama 1 Semester"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "twitter:description",
                        content: "Saya Eko Kurniawan Khannedy, Technical Architect di Blibli.com. Dengan pengalaman saya lebih dari 15 tahun berkarya dan 11 tahun berkarir di bidang programming akan membantu kamu belajar coding lebih cepat dan terarah untuk menjadi Programmer Zaman Now"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "twitter:image",
                        content: "/assets/img/programmerzamannow/og-image-pzn.jpg"
                    })
                ]
            }),
            /*#__PURE__*/ jsx_runtime_.jsx(Layout/* default */.Z, {
                children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                    className: "container pt-5 p-3 p-md-5",
                    children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                        className: "row mt-5",
                        children: [
                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                className: "col-lg-7",
                                children: [
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("section", {
                                        className: "section",
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                className: "row mt-3 mb-4",
                                                children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                    className: "col",
                                                    children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("span", {
                                                        className: "text-muted",
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                                                                href: "/programmerzamannow",
                                                                children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                    className: "link text-primary",
                                                                    children: "Home"
                                                                })
                                                            }),
                                                            " > ",
                                                            courses.title,
                                                            " "
                                                        ]
                                                    })
                                                })
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: "d-flex align-items-start",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                        height: "65",
                                                        className: "rounded",
                                                        src: courses.thumbnail,
                                                        alt: courses.title
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("h1", {
                                                        style: {
                                                            fontSize: '1.7rem'
                                                        },
                                                        className: "section-title ms-3 my-auto",
                                                        children: courses.title
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                className: "text-muted my-3",
                                                children: courses.description
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: "d-flex align-items-start",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                        className: "badge bg-codepolitan px-md-4 py-md-2",
                                                        children: courses.level.toUpperCase()
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx(external_react_simple_star_rating_.RatingView, {
                                                        className: "ms-2 ms-md-4 mt-md-1",
                                                        size: 20,
                                                        ratingValue: Math.floor(ratingValue)
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("span", {
                                                        className: "mt-md-1 text-muted ms-auto ms-md-3",
                                                        children: [
                                                            testimony.length || 0,
                                                            " penilaian"
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("span", {
                                                        className: "mt-md-1 text-muted ms-auto ms-md-3",
                                                        children: [
                                                            courseDetail.total_student,
                                                            " peserta"
                                                        ]
                                                    })
                                                ]
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx("section", {
                                        className: "ratio ratio-16x9 mt-4 mb-5",
                                        children: /*#__PURE__*/ jsx_runtime_.jsx("iframe", {
                                            style: {
                                                borderRadius: '25px'
                                            },
                                            src: `https://www.youtube.com/embed/${courses.preview_video}`,
                                            title: "Course Preview",
                                            allowFullScreen: true
                                        })
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("section", {
                                        className: "section mb-4",
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("h3", {
                                                className: "section-title",
                                                children: "Tentang Kelas"
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                className: "text-muted",
                                                children: (0,helper/* removeTag */.tc)(courses.long_description)
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("section", {
                                        className: "section mb-5",
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("h3", {
                                                className: "section-title mb-3",
                                                children: "Materi Belajar"
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: "card",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                        className: "card-header bg-white",
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("h4", {
                                                            className: "text-muted",
                                                            children: lessonTopic[0].topic_title
                                                        })
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                        className: "card-body p-0",
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("table", {
                                                            className: "table table-striped m-auto",
                                                            children: /*#__PURE__*/ jsx_runtime_.jsx("tbody", {
                                                                children: lessonContent[0].contents.map((lesson, index)=>{
                                                                    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)("tr", {
                                                                        children: [
                                                                            /*#__PURE__*/ jsx_runtime_.jsx("td", {
                                                                                className: "col-1 text-muted text-center",
                                                                                children: index + 1
                                                                            }),
                                                                            /*#__PURE__*/ jsx_runtime_.jsx("td", {
                                                                                className: "col-1",
                                                                                children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                                                    height: "30",
                                                                                    src: "/assets/img/program/play-icon.png",
                                                                                    alt: "Icon"
                                                                                })
                                                                            }),
                                                                            /*#__PURE__*/ jsx_runtime_.jsx("td", {
                                                                                className: "text-muted",
                                                                                children: lesson.lesson_title
                                                                            }),
                                                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("td", {
                                                                                className: "text-end text-muted",
                                                                                children: [
                                                                                    /*#__PURE__*/ jsx_runtime_.jsx(react_fontawesome_.FontAwesomeIcon, {
                                                                                        icon: free_solid_svg_icons_.faLock,
                                                                                        className: "me-2"
                                                                                    }),
                                                                                    lesson.duration || '00:00'
                                                                                ]
                                                                            })
                                                                        ]
                                                                    }, index));
                                                                })
                                                            })
                                                        })
                                                    })
                                                ]
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("section", {
                                        className: "section mb-5",
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("h3", {
                                                className: "section-title mb-4",
                                                children: "Penyusun Materi"
                                            }),
                                            authors.map((author, index)=>{
                                                return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: "row",
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                            className: "col-3",
                                                            children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                                className: "img-fluid rounded-circle",
                                                                src: author.avatar,
                                                                alt: author.name
                                                            })
                                                        }),
                                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                            className: "col mt-2 mt-md-4",
                                                            children: [
                                                                /*#__PURE__*/ jsx_runtime_.jsx("h5", {
                                                                    children: author.name
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                                    className: "text-muted",
                                                                    children: author.short_description
                                                                })
                                                            ]
                                                        })
                                                    ]
                                                }, index));
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("section", {
                                        className: "section",
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("h3", {
                                                className: "section-title mb-4",
                                                children: "Testimoni oleh Siswa"
                                            }),
                                            testimony.status === 'failed' ? /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                className: "text-muted my-5",
                                                children: "Belum ada testimoni"
                                            }) : testimony.map((item, index)=>{
                                                return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: "card mb-2",
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                            className: "card-body",
                                                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                                className: "d-flex align-items-start",
                                                                children: [
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                                        height: "70",
                                                                        className: "rounded-circle me-3",
                                                                        src: "/assets/img/program/icons/icon-avatar.png",
                                                                        alt: ""
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                                        className: "text-muted mt-2",
                                                                        children: item.comment !== "" || item.comment !== null ? item.comment : /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                            children: "Tidak ada ulasan"
                                                                        })
                                                                    })
                                                                ]
                                                            })
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                            className: "card-footer bg-white border-0",
                                                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                                className: "row",
                                                                children: [
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                        className: "col-auto",
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("h6", {
                                                                            children: /*#__PURE__*/ jsx_runtime_.jsx("strong", {
                                                                                children: item.name
                                                                            })
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                        className: "col text-end",
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx(external_react_simple_star_rating_.RatingView, {
                                                                            className: "ms-2 ms-md-4 mt-md-1",
                                                                            size: 20,
                                                                            ratingValue: item.rate
                                                                        })
                                                                    })
                                                                ]
                                                            })
                                                        })
                                                    ]
                                                }, index));
                                            }).slice(0, limit),
                                            testimony.length <= limit || testimony.status === 'failed' ? '' : /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                className: "d-grid gap-2",
                                                children: /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                                    onClick: ()=>setLimit(limit + 5)
                                                    ,
                                                    className: "btn btn-outline-primary shadow-none",
                                                    children: "Tampilkan lainnya"
                                                })
                                            })
                                        ]
                                    })
                                ]
                            }),
                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                className: "col-lg-4 offset-lg-1",
                                children: [
                                    /*#__PURE__*/ jsx_runtime_.jsx("h2", {
                                        className: "d-lg-none mt-5 mb-4",
                                        children: "Mulai Berlangganan"
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx(SidebarCard_SidebarCard, {})
                                ]
                            })
                        ]
                    })
                })
            })
        ]
    }));
};
/* harmony default export */ const _slug_ = (CourseDetail);


/***/ }),

/***/ 5368:
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/free-brands-svg-icons");

/***/ }),

/***/ 197:
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/free-regular-svg-icons");

/***/ }),

/***/ 6466:
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/free-solid-svg-icons");

/***/ }),

/***/ 7197:
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/react-fontawesome");

/***/ }),

/***/ 2167:
/***/ ((module) => {

"use strict";
module.exports = require("axios");

/***/ }),

/***/ 2733:
/***/ ((module) => {

"use strict";
module.exports = require("fuse.js");

/***/ }),

/***/ 2245:
/***/ ((module) => {

"use strict";
module.exports = require("moment");

/***/ }),

/***/ 5518:
/***/ ((module) => {

"use strict";
module.exports = require("moment/locale/id");

/***/ }),

/***/ 562:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/server/denormalize-page-path.js");

/***/ }),

/***/ 4014:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/i18n/normalize-locale-path.js");

/***/ }),

/***/ 8524:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/is-plain-object.js");

/***/ }),

/***/ 8020:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/mitt.js");

/***/ }),

/***/ 4964:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router-context.js");

/***/ }),

/***/ 9565:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/get-asset-path-from-route.js");

/***/ }),

/***/ 4365:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/get-middleware-regex.js");

/***/ }),

/***/ 1428:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/is-dynamic.js");

/***/ }),

/***/ 1292:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/parse-relative-url.js");

/***/ }),

/***/ 979:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/querystring.js");

/***/ }),

/***/ 6052:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/resolve-rewrites.js");

/***/ }),

/***/ 4226:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/route-matcher.js");

/***/ }),

/***/ 5052:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/route-regex.js");

/***/ }),

/***/ 9232:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/utils.js");

/***/ }),

/***/ 968:
/***/ ((module) => {

"use strict";
module.exports = require("next/head");

/***/ }),

/***/ 1853:
/***/ ((module) => {

"use strict";
module.exports = require("next/router");

/***/ }),

/***/ 6689:
/***/ ((module) => {

"use strict";
module.exports = require("react");

/***/ }),

/***/ 7386:
/***/ ((module) => {

"use strict";
module.exports = require("react-simple-star-rating");

/***/ }),

/***/ 997:
/***/ ((module) => {

"use strict";
module.exports = require("react/jsx-runtime");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [7730,9285,1664,4528,4986], () => (__webpack_exec__(4639)));
module.exports = __webpack_exports__;

})();