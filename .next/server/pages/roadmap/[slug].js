(() => {
var exports = {};
exports.id = 4427;
exports.ids = [4427];
exports.modules = {

/***/ 9693:
/***/ ((module) => {

// Exports
module.exports = {
	"card": "CardRoadmapDetail_card__1erzS",
	"card-roadmap": "CardRoadmapDetail_card-roadmap__6PAZk"
};


/***/ }),

/***/ 2227:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6466);
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(7197);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _CardRoadmapDetail_module_scss__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(9693);
/* harmony import */ var _CardRoadmapDetail_module_scss__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_CardRoadmapDetail_module_scss__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(1664);





const CardRoadmapDetail = ({ index , cover , courseSlug , courseTitle , totalModule , totalTime  })=>{
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        className: "col-lg-4 p-2",
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_link__WEBPACK_IMPORTED_MODULE_3__["default"], {
            href: `/course/intro/${courseSlug}`,
            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("a", {
                className: "text-decoration-none text-muted",
                id: "cardRoadmapDetail",
                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                    className: `card ${(_CardRoadmapDetail_module_scss__WEBPACK_IMPORTED_MODULE_4___default().card)}`,
                    children: [
                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                            className: "position-relative",
                            children: [
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: (_CardRoadmapDetail_module_scss__WEBPACK_IMPORTED_MODULE_4___default()["card-roadmap"]),
                                    style: {
                                        backgroundImage: `url(${cover || '/assets/img/placeholder.jpg'})`
                                    }
                                }),
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "position-absolute",
                                    style: {
                                        bottom: "-9%",
                                        right: '10%'
                                    },
                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "fw-bold bg-white rounded-circle text-center shadow",
                                        style: {
                                            width: '50px',
                                            height: '50px',
                                            lineHeight: '50px',
                                            fontSize: '1.9rem'
                                        },
                                        children: index + 1
                                    })
                                })
                            ]
                        }),
                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                            className: "card-body",
                            children: [
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "card-title fw-bold mb-2",
                                    children: courseTitle || 'Tidak ada judul'
                                }),
                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                    className: "card-text d-flex gap-2 fs-6 text-muted",
                                    children: [
                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                            className: "d-flex gap-1",
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__.FontAwesomeIcon, {
                                                    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_1__.faSwatchbook
                                                }),
                                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("small", {
                                                    children: [
                                                        totalModule || 0,
                                                        " Module"
                                                    ]
                                                })
                                            ]
                                        }),
                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                            className: "d-flex gap-1",
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__.FontAwesomeIcon, {
                                                    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_1__.faClock
                                                }),
                                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("small", {
                                                    children: [
                                                        totalTime || 0,
                                                        " Jam"
                                                    ]
                                                })
                                            ]
                                        })
                                    ]
                                })
                            ]
                        })
                    ]
                })
            })
        })
    }, index));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (CardRoadmapDetail);


/***/ }),

/***/ 4906:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.a(module, async (__webpack_handle_async_dependencies__) => {
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "getServerSideProps": () => (/* binding */ getServerSideProps),
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6466);
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(7197);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _components_global_Sections_SectionRelatedCourse_SectionRelatedCourse__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(4190);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(2167);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _components_global_Layout_Layout__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(4528);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(968);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _components_global_Cards_CardDetail_CardDetail__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(1180);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(1664);
/* harmony import */ var _components_global_Buttons_ButtonShareSocmed_ButtonShareSocmed__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(1943);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var swiper_react__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(3015);
/* harmony import */ var _components_global_Cards_CardRoadmapDetail_CardRoadmapDetail__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(2227);
/* harmony import */ var _components_global_Cards_CardCourse_CardCourse__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(9722);
/* harmony import */ var _components_global_Cards_CardRoadmap_CardRoadmap__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(2086);
/* harmony import */ var _components_skeletons_SkeletonCardRoadmap_SkeletonCardRoadmap__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(2501);
/* harmony import */ var _components_skeletons_SkeletonCardCourse_SkeletonCardCourse__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(2927);
/* harmony import */ var _utils_helper__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(4986);
/* harmony import */ var react_markdown__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(3135);
/* harmony import */ var remark_gfm__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(6809);
/* harmony import */ var rehype_raw__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(1871);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([swiper_react__WEBPACK_IMPORTED_MODULE_11__, _components_global_Sections_SectionRelatedCourse_SectionRelatedCourse__WEBPACK_IMPORTED_MODULE_3__, rehype_raw__WEBPACK_IMPORTED_MODULE_20__, remark_gfm__WEBPACK_IMPORTED_MODULE_19__, react_markdown__WEBPACK_IMPORTED_MODULE_18__]);
([swiper_react__WEBPACK_IMPORTED_MODULE_11__, _components_global_Sections_SectionRelatedCourse_SectionRelatedCourse__WEBPACK_IMPORTED_MODULE_3__, rehype_raw__WEBPACK_IMPORTED_MODULE_20__, remark_gfm__WEBPACK_IMPORTED_MODULE_19__, react_markdown__WEBPACK_IMPORTED_MODULE_18__] = __webpack_async_dependencies__.then ? await __webpack_async_dependencies__ : __webpack_async_dependencies__);


















// React Markdown



const getServerSideProps = async (context)=>{
    const { slug  } = context.query;
    const detailRes = await axios__WEBPACK_IMPORTED_MODULE_4___default().get(`${"https://api.codepolitan.com"}/v1/roadmap/${slug}`);
    const [detail] = await Promise.all([
        detailRes.data, 
    ]);
    return {
        props: {
            detail
        }
    };
};
const RoadmapDetail = ({ detail  })=>{
    const { 0: show , 1: setShow  } = (0,react__WEBPACK_IMPORTED_MODULE_10__.useState)(false);
    const { 0: relatedRoadmaps , 1: setRelatedRoadmaps  } = (0,react__WEBPACK_IMPORTED_MODULE_10__.useState)([]);
    const { 0: popularCourses , 1: setPopularCourses  } = (0,react__WEBPACK_IMPORTED_MODULE_10__.useState)([]);
    const totalModule = (0,_utils_helper__WEBPACK_IMPORTED_MODULE_17__/* .getTotalModule */ .g5)(detail.courses);
    const totalTime = (0,_utils_helper__WEBPACK_IMPORTED_MODULE_17__/* .getTotalTime */ .AE)(detail.courses);
    (0,react__WEBPACK_IMPORTED_MODULE_10__.useEffect)(()=>{
        const getRelatedRoadmaps = async ()=>{
            try {
                let response = await axios__WEBPACK_IMPORTED_MODULE_4___default().get(`${"https://api.codepolitan.com"}/v1/roadmap?page=1&limit=10&filter=mentor_username[eq]${detail.username}`);
                if (response.data.error === "No Content") {
                    setRelatedRoadmaps(null);
                } else {
                    setRelatedRoadmaps(response.data);
                }
            } catch (err) {
                return err.message;
            }
        };
        getRelatedRoadmaps();
        const getPopularCourses = async ()=>{
            try {
                let response = await axios__WEBPACK_IMPORTED_MODULE_4___default().get(`${"https://api.codepolitan.com"}/course/popular`);
                if (response.data.error === "No Content") {
                    setPopularCourses(null);
                } else {
                    setPopularCourses(response.data);
                }
            } catch (err) {
                return err.message;
            }
        };
        getPopularCourses();
    }, [
        detail.username
    ]);
    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
        children: [
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)((next_head__WEBPACK_IMPORTED_MODULE_6___default()), {
                children: [
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("title", {
                        children: [
                            detail.name,
                            " - Codepolitan"
                        ]
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        name: "title",
                        content: detail.name
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        name: "description",
                        content: detail.description
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:type",
                        content: "website"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:url",
                        content: `https://codepolitan.com/roadmap/${detail.slug}`
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:title",
                        content: detail.name
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:description",
                        content: detail.description
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:image",
                        content: detail.image
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:card",
                        content: "summary_large_image"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:url",
                        content: `https://codepolitan.com/roadmap/${detail.slug}`
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:title",
                        content: detail.name
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:description",
                        content: detail.description
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:image",
                        content: detail.image
                    })
                ]
            }),
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_components_global_Layout_Layout__WEBPACK_IMPORTED_MODULE_5__/* ["default"] */ .Z, {
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("section", {
                        className: "section mt-5",
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "container p-3 p-lg-5",
                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                className: "row mt-4",
                                children: [
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        className: "col-lg-8 text-muted",
                                        children: [
                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("section", {
                                                className: "section",
                                                children: [
                                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                        className: "d-flex align-items-center",
                                                        children: [
                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                                height: "65",
                                                                className: "rounded",
                                                                src: detail.image || '/assets/img/placeholder.jpg',
                                                                alt: detail.name
                                                            }),
                                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                                className: "d-flex flex-column",
                                                                children: [
                                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h1", {
                                                                        style: {
                                                                            fontSize: '1.5rem'
                                                                        },
                                                                        className: "section-title ms-3 my-auto",
                                                                        children: detail.name || 'Belum ada judul'
                                                                    }),
                                                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                                        className: "d-flex flex-column flex-md-row gap-1 gap-md-4 ms-3",
                                                                        children: [
                                                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                                                className: "d-flex gap-2 align-items-center",
                                                                                children: [
                                                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__.FontAwesomeIcon, {
                                                                                        icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_1__.faBuilding
                                                                                    }),
                                                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                                                        children: detail.brand_name || 'Belum ada brand'
                                                                                    })
                                                                                ]
                                                                            }),
                                                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                                                className: "d-flex gap-2 align-items-center",
                                                                                children: [
                                                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__.FontAwesomeIcon, {
                                                                                        icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_1__.faUserSecret
                                                                                    }),
                                                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                                                        children: detail.brand_author || 'Belum ada author'
                                                                                    })
                                                                                ]
                                                                            })
                                                                        ]
                                                                    })
                                                                ]
                                                            })
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                        className: "text-muted my-3",
                                                        children: detail.description
                                                    }),
                                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                        className: "d-flex align-items-start",
                                                        children: [
                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                                className: "badge border text-primary py-md-2",
                                                                children: detail.level.toUpperCase()
                                                            }),
                                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("button", {
                                                                onClick: ()=>setShow(!show)
                                                                ,
                                                                className: "btn text-muted btn-sm btn-transparent ms-auto",
                                                                children: [
                                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__.FontAwesomeIcon, {
                                                                        icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_1__.faShareAlt
                                                                    }),
                                                                    " Share"
                                                                ]
                                                            })
                                                        ]
                                                    }),
                                                    show && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "row my-3",
                                                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                            className: "col text-center",
                                                            children: [
                                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_global_Buttons_ButtonShareSocmed_ButtonShareSocmed__WEBPACK_IMPORTED_MODULE_9__/* ["default"] */ .Z, {
                                                                    type: "facebook",
                                                                    link: `https://codepolitan.com/roadmap/${detail.slug}`
                                                                }),
                                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_global_Buttons_ButtonShareSocmed_ButtonShareSocmed__WEBPACK_IMPORTED_MODULE_9__/* ["default"] */ .Z, {
                                                                    type: "twitter",
                                                                    link: `https://codepolitan.com/roadmap/${detail.slug}`
                                                                }),
                                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_global_Buttons_ButtonShareSocmed_ButtonShareSocmed__WEBPACK_IMPORTED_MODULE_9__/* ["default"] */ .Z, {
                                                                    type: "linkedin",
                                                                    link: `https://codepolitan.com/roadmap/${detail.slug}`
                                                                })
                                                            ]
                                                        })
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("section", {
                                                className: "section mt-5",
                                                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                    className: "row",
                                                    children: [
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h3", {
                                                            className: "mb-4",
                                                            children: "Materi Yang Akan Dipelajari"
                                                        }),
                                                        "                                    ",
                                                        detail.courses.map((course, index)=>{
                                                            return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_global_Cards_CardRoadmapDetail_CardRoadmapDetail__WEBPACK_IMPORTED_MODULE_12__/* ["default"] */ .Z, {
                                                                index: index,
                                                                cover: course.cover,
                                                                courseSlug: course.slug,
                                                                courseTitle: course.course_title,
                                                                totalModule: course.total_module,
                                                                totalTime: course.total_time
                                                            }, index));
                                                        })
                                                    ]
                                                })
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                className: "row mt-5",
                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                    className: "col",
                                                    children: detail.long_description != "" && /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                        className: "my-5",
                                                        children: [
                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h3", {
                                                                children: "Tentang Kelas"
                                                            }),
                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_markdown__WEBPACK_IMPORTED_MODULE_18__["default"], {
                                                                className: "markdown",
                                                                remarkPlugins: [
                                                                    remark_gfm__WEBPACK_IMPORTED_MODULE_19__["default"]
                                                                ],
                                                                rehypePlugins: [
                                                                    rehype_raw__WEBPACK_IMPORTED_MODULE_20__["default"]
                                                                ],
                                                                components: {
                                                                    img: ({ node , ...props })=>/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                                            className: "text-center py-5",
                                                                            children: [
                                                                                " ",
                                                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                                                    alt: "Logo image",
                                                                                    style: {
                                                                                        width: '35%'
                                                                                    },
                                                                                    ...props
                                                                                }),
                                                                                " "
                                                                            ]
                                                                        })
                                                                },
                                                                children: detail.long_description
                                                            })
                                                        ]
                                                    })
                                                })
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "col-lg-4",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: "sticky-top",
                                            style: {
                                                top: '80px'
                                            },
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_global_Cards_CardDetail_CardDetail__WEBPACK_IMPORTED_MODULE_7__/* ["default"] */ .Z, {
                                                image: detail.image,
                                                roadmap: true,
                                                detailProduct: detail.products,
                                                slug: detail.slug,
                                                totalModule: totalModule,
                                                totalTime: totalTime
                                            })
                                        })
                                    })
                                ]
                            })
                        })
                    }),
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_components_global_Sections_SectionRelatedCourse_SectionRelatedCourse__WEBPACK_IMPORTED_MODULE_3__/* ["default"] */ .Z, {
                        titleSection: `Roadmap Lainnya dari ${detail.brand_name}`,
                        children: [
                            !relatedRoadmaps && [
                                1,
                                2,
                                3,
                                4,
                                5
                            ].map((index)=>{
                                return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(swiper_react__WEBPACK_IMPORTED_MODULE_11__.SwiperSlide, {
                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_skeletons_SkeletonCardRoadmap_SkeletonCardRoadmap__WEBPACK_IMPORTED_MODULE_15__/* ["default"] */ .Z, {})
                                }, index));
                            }),
                            relatedRoadmaps !== null && relatedRoadmaps.map((roadmap, index)=>{
                                return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(swiper_react__WEBPACK_IMPORTED_MODULE_11__.SwiperSlide, {
                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_link__WEBPACK_IMPORTED_MODULE_8__["default"], {
                                        href: `/roadmap/${roadmap.slug}`,
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("a", {
                                            className: "link",
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_global_Cards_CardRoadmap_CardRoadmap__WEBPACK_IMPORTED_MODULE_14__/* ["default"] */ .Z, {
                                                thumbnail: roadmap.image,
                                                icon: roadmap.small_icon,
                                                author: roadmap.author,
                                                title: roadmap.name,
                                                level: roadmap.level,
                                                totalCourse: roadmap.total_course,
                                                totalStudent: roadmap.total_student,
                                                rate: roadmap.rate
                                            })
                                        })
                                    })
                                }, index));
                            }).slice(0, 6)
                        ]
                    }),
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_components_global_Sections_SectionRelatedCourse_SectionRelatedCourse__WEBPACK_IMPORTED_MODULE_3__/* ["default"] */ .Z, {
                        titleSection: `Kelas Populer Lainnya`,
                        children: [
                            !popularCourses && [
                                1,
                                2,
                                3,
                                4,
                                5
                            ].map((index)=>{
                                return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(swiper_react__WEBPACK_IMPORTED_MODULE_11__.SwiperSlide, {
                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_skeletons_SkeletonCardCourse_SkeletonCardCourse__WEBPACK_IMPORTED_MODULE_16__/* ["default"] */ .Z, {})
                                }, index));
                            }),
                            popularCourses !== null && (0,_utils_helper__WEBPACK_IMPORTED_MODULE_17__/* .shuffleArray */ .Sy)(popularCourses).map((course, index)=>{
                                return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(swiper_react__WEBPACK_IMPORTED_MODULE_11__.SwiperSlide, {
                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_link__WEBPACK_IMPORTED_MODULE_8__["default"], {
                                        href: `/course/intro/${course.slug}`,
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("a", {
                                            className: "link",
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_global_Cards_CardCourse_CardCourse__WEBPACK_IMPORTED_MODULE_13__/* ["default"] */ .Z, {
                                                thumbnail: course.thumbnail,
                                                author: course.author,
                                                title: course.title,
                                                level: course.level,
                                                totalStudents: course.total_student,
                                                totalModules: course.total_module,
                                                totalTimes: course.total_time,
                                                totalRating: course.total_rating,
                                                totalFeedback: course.total_feedback,
                                                normalBuyPrice: course.buy?.normal_price || course.normal_price,
                                                retailBuyPrice: course.buy?.retail_price || course.retail_price,
                                                normalRentPrice: course.rent?.normal_price,
                                                retailRentPrice: course.rent?.retail_price
                                            })
                                        })
                                    })
                                }, index));
                            }).slice(0, 10)
                        ]
                    })
                ]
            })
        ]
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (RoadmapDetail);

});

/***/ }),

/***/ 5368:
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/free-brands-svg-icons");

/***/ }),

/***/ 197:
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/free-regular-svg-icons");

/***/ }),

/***/ 6466:
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/free-solid-svg-icons");

/***/ }),

/***/ 7197:
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/react-fontawesome");

/***/ }),

/***/ 2167:
/***/ ((module) => {

"use strict";
module.exports = require("axios");

/***/ }),

/***/ 2733:
/***/ ((module) => {

"use strict";
module.exports = require("fuse.js");

/***/ }),

/***/ 2245:
/***/ ((module) => {

"use strict";
module.exports = require("moment");

/***/ }),

/***/ 5518:
/***/ ((module) => {

"use strict";
module.exports = require("moment/locale/id");

/***/ }),

/***/ 562:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/server/denormalize-page-path.js");

/***/ }),

/***/ 8028:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/server/image-config.js");

/***/ }),

/***/ 4957:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/head.js");

/***/ }),

/***/ 4014:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/i18n/normalize-locale-path.js");

/***/ }),

/***/ 8524:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/is-plain-object.js");

/***/ }),

/***/ 8020:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/mitt.js");

/***/ }),

/***/ 4964:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router-context.js");

/***/ }),

/***/ 9565:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/get-asset-path-from-route.js");

/***/ }),

/***/ 4365:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/get-middleware-regex.js");

/***/ }),

/***/ 1428:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/is-dynamic.js");

/***/ }),

/***/ 1292:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/parse-relative-url.js");

/***/ }),

/***/ 979:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/querystring.js");

/***/ }),

/***/ 6052:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/resolve-rewrites.js");

/***/ }),

/***/ 4226:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/route-matcher.js");

/***/ }),

/***/ 5052:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/route-regex.js");

/***/ }),

/***/ 9232:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/utils.js");

/***/ }),

/***/ 968:
/***/ ((module) => {

"use strict";
module.exports = require("next/head");

/***/ }),

/***/ 1853:
/***/ ((module) => {

"use strict";
module.exports = require("next/router");

/***/ }),

/***/ 6689:
/***/ ((module) => {

"use strict";
module.exports = require("react");

/***/ }),

/***/ 997:
/***/ ((module) => {

"use strict";
module.exports = require("react/jsx-runtime");

/***/ }),

/***/ 3135:
/***/ ((module) => {

"use strict";
module.exports = import("react-markdown");;

/***/ }),

/***/ 1871:
/***/ ((module) => {

"use strict";
module.exports = import("rehype-raw");;

/***/ }),

/***/ 6809:
/***/ ((module) => {

"use strict";
module.exports = import("remark-gfm");;

/***/ }),

/***/ 3877:
/***/ ((module) => {

"use strict";
module.exports = import("swiper");;

/***/ }),

/***/ 3015:
/***/ ((module) => {

"use strict";
module.exports = import("swiper/react");;

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [7730,9285,1664,5675,4528,4986,9722,2086,2927,4722,2501], () => (__webpack_exec__(4906)));
module.exports = __webpack_exports__;

})();