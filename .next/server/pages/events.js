"use strict";
(() => {
var exports = {};
exports.id = 7695;
exports.ids = [7695];
exports.modules = {

/***/ 7341:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ events),
  "getServerSideProps": () => (/* binding */ getServerSideProps)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: external "axios"
var external_axios_ = __webpack_require__(2167);
var external_axios_default = /*#__PURE__*/__webpack_require__.n(external_axios_);
// EXTERNAL MODULE: external "next/head"
var head_ = __webpack_require__(968);
var head_default = /*#__PURE__*/__webpack_require__.n(head_);
// EXTERNAL MODULE: ./src/components/global/Layout/Layout.js + 5 modules
var Layout = __webpack_require__(4528);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: ./src/components/events/Cards/CardEvent/CardEvent.js
var CardEvent = __webpack_require__(6441);
;// CONCATENATED MODULE: external "react-loading-skeleton"
const external_react_loading_skeleton_namespaceObject = require("react-loading-skeleton");
var external_react_loading_skeleton_default = /*#__PURE__*/__webpack_require__.n(external_react_loading_skeleton_namespaceObject);
;// CONCATENATED MODULE: ./src/components/events/Cards/CardSkeleton/CardSkeleton.js



const CardSkeleton = ()=>{
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
        className: "card border-0 shadow-sm",
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx((external_react_loading_skeleton_default()), {
                height: 230
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                className: "card-body px-3 py-2",
                children: /*#__PURE__*/ jsx_runtime_.jsx((external_react_loading_skeleton_default()), {
                    count: 3
                })
            })
        ]
    }));
};
/* harmony default export */ const CardSkeleton_CardSkeleton = (CardSkeleton);

// EXTERNAL MODULE: ./node_modules/next/link.js
var next_link = __webpack_require__(1664);
;// CONCATENATED MODULE: ./src/components/events/Section/SectionEvent/SectionEvent.js





const SectionEvent = ({ data , loading  })=>{
    const { 0: limit , 1: setLimit  } = (0,external_react_.useState)(12);
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx("h3", {
                className: "section-title",
                children: "Events"
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                className: "text-muted mb-4",
                children: "Perluas wawasan dan tingkatkan kemampuanmu dengan mengikuti event CODEPOLITAN"
            }),
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                className: "row",
                children: [
                    loading && [
                        ...Array(3)
                    ].map((item, index)=>{
                        return(/*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: "col-lg-3 p-3",
                            children: /*#__PURE__*/ jsx_runtime_.jsx(CardSkeleton_CardSkeleton, {})
                        }, index));
                    }),
                    !loading && data?.map((event, index)=>{
                        return(/*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: "col-lg-3 p-3",
                            children: /*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                                href: `/events/${event.slug}`,
                                className: "link",
                                children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                    className: "link",
                                    children: /*#__PURE__*/ jsx_runtime_.jsx(CardEvent/* default */.Z, {
                                        type: event.webinar_type,
                                        thumbnail: event.featured_image,
                                        title: event.title,
                                        schedule: event.scheduled_at,
                                        location: event.location
                                    })
                                })
                            })
                        }, index));
                    }).slice(0, limit)
                ]
            }),
            data?.length > limit && /*#__PURE__*/ jsx_runtime_.jsx("div", {
                className: "text-center mt-3",
                children: /*#__PURE__*/ jsx_runtime_.jsx("button", {
                    onClick: ()=>setLimit(limit + 8)
                    ,
                    type: "button",
                    className: "btn btn-outline-secondary",
                    children: "Load more"
                })
            })
        ]
    }));
};
/* harmony default export */ const SectionEvent_SectionEvent = (SectionEvent);

;// CONCATENATED MODULE: ./src/pages/events/index.js





const getServerSideProps = async ()=>{
    const eventsRes = await external_axios_default().get(`${"https://api.codepolitan.com"}/v1/webinar/latest?page=1&limit=100`);
    const events = eventsRes.data;
    return {
        props: {
            events
        }
    };
};
const Events = ({ events  })=>{
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
        children: [
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)((head_default()), {
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx("title", {
                        children: "Events - Codepolitan"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        name: "title",
                        content: "Website Belajar Coding Bahasa Indonesia - Codepolitan"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        name: "description",
                        content: "Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "og:type",
                        content: "website"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "og:url",
                        content: "https://codepolitan.com"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "og:title",
                        content: "Website Belajar Coding Bahasa Indonesia - Codepolitan"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "og:description",
                        content: "Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "og:image",
                        content: "https://i.ibb.co/SXP8fGc/OG-image.jpg"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "twitter:card",
                        content: "summary_large_image"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "twitter:url",
                        content: "https://codepolitan.com"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "twitter:title",
                        content: "Website Belajar Coding Bahasa Indonesia - Codepolitan"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "twitter:description",
                        content: "Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "twitter:image",
                        content: "https://i.ibb.co/SXP8fGc/OG-image.jpg"
                    })
                ]
            }),
            /*#__PURE__*/ jsx_runtime_.jsx(Layout/* default */.Z, {
                children: /*#__PURE__*/ jsx_runtime_.jsx("section", {
                    className: "section bg-light",
                    style: {
                        paddingTop: '100px'
                    },
                    children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        className: "container p-4 p-lg-5",
                        children: /*#__PURE__*/ jsx_runtime_.jsx(SectionEvent_SectionEvent, {
                            loading: false,
                            data: events
                        })
                    })
                })
            })
        ]
    }));
};
/* harmony default export */ const events = (Events);


/***/ }),

/***/ 5368:
/***/ ((module) => {

module.exports = require("@fortawesome/free-brands-svg-icons");

/***/ }),

/***/ 197:
/***/ ((module) => {

module.exports = require("@fortawesome/free-regular-svg-icons");

/***/ }),

/***/ 6466:
/***/ ((module) => {

module.exports = require("@fortawesome/free-solid-svg-icons");

/***/ }),

/***/ 7197:
/***/ ((module) => {

module.exports = require("@fortawesome/react-fontawesome");

/***/ }),

/***/ 2167:
/***/ ((module) => {

module.exports = require("axios");

/***/ }),

/***/ 2733:
/***/ ((module) => {

module.exports = require("fuse.js");

/***/ }),

/***/ 2245:
/***/ ((module) => {

module.exports = require("moment");

/***/ }),

/***/ 5518:
/***/ ((module) => {

module.exports = require("moment/locale/id");

/***/ }),

/***/ 562:
/***/ ((module) => {

module.exports = require("next/dist/server/denormalize-page-path.js");

/***/ }),

/***/ 4014:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/i18n/normalize-locale-path.js");

/***/ }),

/***/ 8524:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/is-plain-object.js");

/***/ }),

/***/ 8020:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/mitt.js");

/***/ }),

/***/ 4964:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router-context.js");

/***/ }),

/***/ 9565:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/get-asset-path-from-route.js");

/***/ }),

/***/ 4365:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/get-middleware-regex.js");

/***/ }),

/***/ 1428:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/is-dynamic.js");

/***/ }),

/***/ 1292:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/parse-relative-url.js");

/***/ }),

/***/ 979:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/querystring.js");

/***/ }),

/***/ 6052:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/resolve-rewrites.js");

/***/ }),

/***/ 4226:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/route-matcher.js");

/***/ }),

/***/ 5052:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/route-regex.js");

/***/ }),

/***/ 9232:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/utils.js");

/***/ }),

/***/ 968:
/***/ ((module) => {

module.exports = require("next/head");

/***/ }),

/***/ 1853:
/***/ ((module) => {

module.exports = require("next/router");

/***/ }),

/***/ 6689:
/***/ ((module) => {

module.exports = require("react");

/***/ }),

/***/ 997:
/***/ ((module) => {

module.exports = require("react/jsx-runtime");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [7730,9285,1664,4528,4986,6441], () => (__webpack_exec__(7341)));
module.exports = __webpack_exports__;

})();