(() => {
var exports = {};
exports.id = 2362;
exports.ids = [2362];
exports.modules = {

/***/ 9731:
/***/ ((module) => {

// Exports
module.exports = {
	"section": "CtaSection_section__ZqRBn",
	"title": "CtaSection_title__rwPvV"
};


/***/ }),

/***/ 5250:
/***/ ((module) => {

// Exports
module.exports = {
	"hero": "Hero_hero__V5GMW",
	"hero_title": "Hero_hero_title__RhJzt"
};


/***/ }),

/***/ 7207:
/***/ ((module) => {

// Exports
module.exports = {
	"count": "StatisticSection_count__vPy0F"
};


/***/ }),

/***/ 7857:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ programmerzamannow),
  "getServerSideProps": () => (/* binding */ getServerSideProps)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: external "@fortawesome/free-solid-svg-icons"
var free_solid_svg_icons_ = __webpack_require__(6466);
// EXTERNAL MODULE: external "@fortawesome/react-fontawesome"
var react_fontawesome_ = __webpack_require__(7197);
// EXTERNAL MODULE: external "axios"
var external_axios_ = __webpack_require__(2167);
var external_axios_default = /*#__PURE__*/__webpack_require__.n(external_axios_);
// EXTERNAL MODULE: external "next/head"
var head_ = __webpack_require__(968);
var head_default = /*#__PURE__*/__webpack_require__.n(head_);
// EXTERNAL MODULE: ./node_modules/next/link.js
var next_link = __webpack_require__(1664);
// EXTERNAL MODULE: ./src/components/global/Sections/BenefitSection/BenefitSection.js
var BenefitSection = __webpack_require__(9725);
// EXTERNAL MODULE: ./src/components/global/Layout/Layout.js + 5 modules
var Layout = __webpack_require__(4528);
// EXTERNAL MODULE: ./src/components/programmerzamannow/CourseCard/CourseCard.js
var CourseCard = __webpack_require__(1430);
// EXTERNAL MODULE: ./src/components/programmerzamannow/Hero/Hero.module.scss
var Hero_module = __webpack_require__(5250);
var Hero_module_default = /*#__PURE__*/__webpack_require__.n(Hero_module);
;// CONCATENATED MODULE: ./src/components/programmerzamannow/Hero/Hero.js


const Hero = ()=>{
    return(/*#__PURE__*/ jsx_runtime_.jsx("section", {
        className: `${(Hero_module_default()).hero} bg-light`,
        children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
            className: "container p-5",
            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                className: "row mt-5 text-center text-md-start",
                children: [
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                        className: "col-lg-7 mt-5 text-muted",
                        style: {
                            zIndex: 1
                        },
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx("h1", {
                                className: (Hero_module_default()).hero_title,
                                children: "Kursus Coding Online Programmer Zaman Now Selama 1 Semester"
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                className: "lead my-3",
                                children: "Saya Eko Kurniawan Khannedy, Technical Architect di Blibli.com. Dengan pengalaman saya lebih dari 15 tahun berkarya dan 11 tahun berkarir di bidang programming akan membantu kamu belajar coding lebih cepat dan terarah untuk menjadi Programmer Zaman Now"
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                className: "btn btn-outline-secondary btn-lg mt-3",
                                href: "#about",
                                children: "Lihat Selengkapnya"
                            })
                        ]
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        className: "col-lg-5 mt-5 mt-md-0",
                        children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                            className: "img-fluid",
                            src: "/assets/img/programmerzamannow/eko.png",
                            alt: "Kursus Coding Online Programmer Zaman Now Selama 1 Semester"
                        })
                    })
                ]
            })
        })
    }));
};
/* harmony default export */ const Hero_Hero = (Hero);

// EXTERNAL MODULE: ./src/components/programmerzamannow/RoadmapSection/RoadmapSection.js
var RoadmapSection = __webpack_require__(4079);
// EXTERNAL MODULE: ./src/components/programmerzamannow/CtaSection/CtaSection.module.scss
var CtaSection_module = __webpack_require__(9731);
var CtaSection_module_default = /*#__PURE__*/__webpack_require__.n(CtaSection_module);
;// CONCATENATED MODULE: ./src/components/programmerzamannow/CtaSection/CtaSection.js


const CtaSection = ({ children  })=>{
    return(/*#__PURE__*/ jsx_runtime_.jsx("section", {
        className: `${(CtaSection_module_default()).section} section`,
        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
            className: "container p-5",
            children: [
                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                    className: "row justify-content-center",
                    children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        className: "col-md-8",
                        children: /*#__PURE__*/ jsx_runtime_.jsx("h3", {
                            className: `${(CtaSection_module_default()).title}`,
                            children: "Ingin Yang Lebih Irit ? Cukup Gabung Membership Kamu Dapat Mengakses Semua Roadmap Belajar Programmer Zaman Now!"
                        })
                    })
                }),
                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                    className: "row",
                    children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        className: "col text-center",
                        children: children
                    })
                })
            ]
        })
    }));
};
/* harmony default export */ const CtaSection_CtaSection = (CtaSection);

// EXTERNAL MODULE: ./src/components/global/Sections/WarrantySection/WarrantySection.js
var WarrantySection = __webpack_require__(4272);
// EXTERNAL MODULE: external "@fortawesome/free-brands-svg-icons"
var free_brands_svg_icons_ = __webpack_require__(5368);
;// CONCATENATED MODULE: ./src/components/programmerzamannow/FaqSection/FaqSection.js




const FaqSection = ()=>{
    const { 0: data  } = (0,external_react_.useState)([
        {
            id: 'flush-heading1',
            target: 'flush-collapse1',
            title: 'Bagaimana cara belajarnya?',
            content: 'Cara belajar di program ini bersifat online. Kamu akan mendapatkan materi belajar berupa video pembelajaran yang bisa kamu pelajari dan praktekan. Materi tersebut bisa kamu pelajari kapan pun, sesuai dengan waktu yang kamu miliki. Kamu cukup meluangkan waktu 2 jam sehari untuk bisa mendapatkan hasil maksimal dari program ini.'
        },
        {
            id: 'flush-heading2',
            target: 'flush-collapse2',
            title: 'Bagaimana cara pembayarannya?',
            content: '<p>Ada 2 metode pembayaran yang bisa kamu pilih yaitu:</p> <ol><li>Manual, pada metode ini kamu akan dikirimkan nomor rekening dan juga biaya yang harus kamu transfer pada nomor rekening tersebut. Setelah itu kamu harus mengirimkan bukti transfer pada link yang tertera. Kamu akan menerima notifikasi bila pembayaran kamu sudah diterima.</li><li>Otomatis, pada metode ini kamu akan dikirimkan nomor virtual account (VA), lalu kamu harus mentransfer pada nomor VA tersebut. Kamu akan menerima notifikasi bila pembayaran kamu sudah diterima</li></ol>'
        },
        {
            id: 'flush-heading3',
            target: 'flush-collapse3',
            title: 'Kalau saya ada pertanyaan, kemana saya harus bertanya?',
            content: 'Bila kamu memiliki pertanyaan terkait materi yang kamu pelajari, kamu bisa menanyakannya pada forum CodePolitan yang tertera di bawah materi yang sedang kamu pelajari'
        },
        {
            id: 'flush-heading4',
            target: 'flush-collapse4',
            title: 'Apa yang dimaksud dengan garansi?',
            content: 'Bila pada 3 hari pertama setelah pembayaran kamu diterima dan mencoba kelas yang ada di CodePolitan kamu merasa tidak cocok dengan materi yang dimiliki, kamu bisa klaim pengembalian uang pendaftaran'
        },
        {
            id: 'flush-heading5',
            target: 'flush-collapse5',
            title: 'Bagaimana cara klaim garansinya?',
            content: 'Kamu bisa menghubungi nomor CS CodePolitan, lalu laporkan kalau kamu ingin mengajukan garansi'
        },
        {
            id: 'flush-heading6',
            target: 'flush-collapse6',
            title: 'Apakah video bisa ditonton ulang setelah habis masa subscribe',
            content: 'Tidak bisa. Setelah masa membership kamu habis kelas akan otomatis terkunci kembali.'
        },
        {
            id: 'flush-heading7',
            target: 'flush-collapse7',
            title: 'Apakah akan ada pembaharuan kelas?',
            content: 'Iya, kelas akan terus di update. Harga membership juga akan disesuaikan setiap ada pembaharuan materi'
        },
        {
            id: 'flush-heading8',
            target: 'flush-collapse8',
            title: 'Bagaimana jika kelas belum selesai tapi masa subscribe sudah berakhir?',
            content: 'Kamu tidak bisa mengakses kelas lagi. Tetapi, jangan khawatir, learning progress kamu akan tetap tersimpan'
        },
        {
            id: 'flush-heading9',
            target: 'flush-collapse9',
            title: 'Apakah akan disalurkan kerja',
            content: 'Untuk saat ini belum, tapi melalui program Talent Hub Codepolitan kamu akan berpotensi terhubung dengan partner Codepolitan yang membutuhkan talent. Jika profile dan skill kamu sesuai dengan kebutuhan mereka, maka kamu akan punya kesempatan direkrut. Untuk itu, pastikan kamu bergabung dalam program Talent Hub Codepolitan segera setelah bergabung dalam program ini.'
        },
        {
            id: 'flush-heading10',
            target: 'flush-collapse10',
            title: 'Saya masih terdaftar sebagai member premium di paket reguler, apakah saya bisa akses ke kelas PZN juga?',
            content: 'Tidak bisa, karena membership reguler dan membership kelas PZN berbeda'
        },
        {
            id: 'flush-heading11',
            target: 'flush-collapse11',
            title: 'Apa perbedaan membership pada kelas PZN dan reguler?',
            content: 'Pada membership reguler , kamu bisa akses semua kelas yang disediakan oleh CodePolitan kecuali yang dibuat oleh Eko Khannedy. Sedangkan pada membership PZN, kamu bisa akses seluruh kelas PZN buatan Eko Khannedy, tetapi tidak bisa akses kelas CodePolitan yang lain.'
        },
        {
            id: 'flush-heading12',
            target: 'flush-collapse12',
            title: 'Apakah saya bisa terdaftar pada dua membership tersebut secara bersamaan?',
            content: 'Bisa, kamu bisa terdaftar pada membership reguler sekaligus membership PZN dalam waktu yang sama :)'
        },
        {
            id: 'flush-heading13',
            target: 'flush-collapse13',
            title: 'Materi apa saja yang akan saya pelajari pada program ini?',
            content: 'Untuk saat ini terdapat terdapat 6 roadmap yang didalamnya 41 kelas yang bisa kamu pelajari, 6 roadmap tersebut adalah: Roadmap Java, JavaScript, Go-Lang, PHP, MySQL, dan Kotlin'
        },
        {
            id: 'flush-heading14',
            target: 'flush-collapse14',
            title: 'Berapa biaya yang harus saya bayar untuk mengikuti program ini?',
            content: 'Harganya beragam, sesuai dengan lama membership yang kamu butuhkan. Harganya bisa kamu lihat di bagian harga pada halaman ini.'
        },
        {
            id: 'flush-heading15',
            target: 'flush-collapse15',
            title: 'Apakah saya akan mendapatkan sertifikat setelah menyelesaikan materi belajar?',
            content: 'Iya, setelah beres mempelajari setiap kelasnya kamu akan mendapatkan sertifikat'
        },
        {
            id: 'flush-heading16',
            target: 'flush-collapse16',
            title: 'Apakah video tutorial bisa di download?',
            content: 'Seluruh video di CodePolitan tidak bisa di download, hanya bisa ditonton secara streaming :)'
        }, 
    ]);
    return(/*#__PURE__*/ jsx_runtime_.jsx("section", {
        className: "section",
        children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
            className: "container p-4 p-lg-5 my-5",
            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                className: "row flex-lg-row-reverse",
                children: [
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                        className: "col-lg-8 mb-5",
                        children: [
                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("h2", {
                                className: "section-title d-lg-none mb-5",
                                style: {
                                    color: "#14a7a0",
                                    fontSize: "2rem"
                                },
                                children: [
                                    "Frequently ",
                                    /*#__PURE__*/ jsx_runtime_.jsx("br", {}),
                                    " Asked Questions"
                                ]
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                className: "accordion accordion-flush",
                                id: "accordionFlushPZN",
                                children: data.map((item, index)=>{
                                    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: "accordion-item",
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("h3", {
                                                className: "accordion-header",
                                                id: item.id,
                                                children: /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                                    className: "accordion-button collapsed",
                                                    type: "button",
                                                    "data-bs-toggle": "collapse",
                                                    "data-bs-target": `#${item.target}`,
                                                    "aria-expanded": "false",
                                                    "aria-controls": item.target,
                                                    children: /*#__PURE__*/ jsx_runtime_.jsx("strong", {
                                                        className: "text-muted",
                                                        children: item.title
                                                    })
                                                })
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                id: item.target,
                                                className: "accordion-collapse collapse",
                                                "aria-labelledby": item.id,
                                                "data-bs-parent": "#accordionFlushPZN",
                                                children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                    className: "accordion-body text-muted",
                                                    children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                        dangerouslySetInnerHTML: {
                                                            __html: item.content
                                                        }
                                                    })
                                                })
                                            })
                                        ]
                                    }, index));
                                })
                            })
                        ]
                    }),
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                        className: "col-lg-4 d-none d-lg-block border-end border-2 text-center text-lg-start",
                        children: [
                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("h2", {
                                className: "section-title",
                                children: [
                                    "Masih Memiliki Pertanyaan?",
                                    /*#__PURE__*/ jsx_runtime_.jsx("br", {}),
                                    /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                        style: {
                                            color: "#14a7a0"
                                        },
                                        children: "Lihat disini"
                                    })
                                ]
                            }),
                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("a", {
                                className: "btn btn-primary px-4 py-2 mt-3",
                                href: "https://wa.me/628999488990",
                                target: "_blank",
                                rel: "noopener noreferrer",
                                children: [
                                    /*#__PURE__*/ jsx_runtime_.jsx(react_fontawesome_.FontAwesomeIcon, {
                                        className: "me-2",
                                        icon: free_brands_svg_icons_.faWhatsapp
                                    }),
                                    "Tanyakan disini"
                                ]
                            })
                        ]
                    }),
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                        className: "col-lg-4 d-lg-none text-center text-lg-start",
                        children: [
                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("h2", {
                                className: "section-title",
                                children: [
                                    "Masih Memiliki Pertanyaan?",
                                    /*#__PURE__*/ jsx_runtime_.jsx("br", {}),
                                    /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                        style: {
                                            color: "#14a7a0"
                                        },
                                        children: "Lihat disini"
                                    })
                                ]
                            }),
                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("a", {
                                className: "btn btn-primary px-4 py-2 mt-3",
                                href: "https://wa.me/628999488990",
                                target: "_blank",
                                rel: "noopener noreferrer",
                                children: [
                                    /*#__PURE__*/ jsx_runtime_.jsx(react_fontawesome_.FontAwesomeIcon, {
                                        className: "me-2",
                                        icon: free_brands_svg_icons_.faWhatsapp
                                    }),
                                    "Tanyakan disini"
                                ]
                            })
                        ]
                    })
                ]
            })
        })
    }));
};
/* harmony default export */ const FaqSection_FaqSection = (FaqSection);

// EXTERNAL MODULE: external "react-countup"
var external_react_countup_ = __webpack_require__(609);
var external_react_countup_default = /*#__PURE__*/__webpack_require__.n(external_react_countup_);
// EXTERNAL MODULE: ./src/components/programmerzamannow/StatisticSection/StatisticSection.module.scss
var StatisticSection_module = __webpack_require__(7207);
var StatisticSection_module_default = /*#__PURE__*/__webpack_require__.n(StatisticSection_module);
;// CONCATENATED MODULE: ./src/components/programmerzamannow/StatisticSection/StatisticSection.js



const StatisticSection = ()=>{
    return(/*#__PURE__*/ jsx_runtime_.jsx("section", {
        className: "bg-light",
        children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
            className: "container px-5 pb-3",
            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                className: "row justify-content-around text-center",
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        className: "col-6 col-lg-3",
                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("p", {
                            className: `${(StatisticSection_module_default()).count} text-muted`,
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                    className: "text-primary me-1",
                                    children: /*#__PURE__*/ jsx_runtime_.jsx((external_react_countup_default()), {
                                        end: 136,
                                        duration: 5
                                    })
                                }),
                                "Kelas Online"
                            ]
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        className: "col-6 col-lg-3",
                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("p", {
                            className: `${(StatisticSection_module_default()).count} text-muted`,
                            children: [
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("span", {
                                    className: "text-primary me-1",
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx((external_react_countup_default()), {
                                            end: 1000,
                                            duration: 5
                                        }),
                                        "+"
                                    ]
                                }),
                                "Materi Belajar"
                            ]
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        className: "col-6 col-lg-3",
                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("p", {
                            className: `${(StatisticSection_module_default()).count} text-muted`,
                            children: [
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("span", {
                                    className: "text-primary me-1",
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx((external_react_countup_default()), {
                                            end: 100,
                                            duration: 5
                                        }),
                                        "+"
                                    ]
                                }),
                                "Jam Belajar"
                            ]
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        className: "col-6 col-lg-3",
                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("p", {
                            className: `${(StatisticSection_module_default()).count} text-muted`,
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                    className: "text-primary me-1",
                                    children: /*#__PURE__*/ jsx_runtime_.jsx((external_react_countup_default()), {
                                        end: 6,
                                        duration: 5
                                    })
                                }),
                                "Jalur Belajar"
                            ]
                        })
                    })
                ]
            })
        })
    }));
};
/* harmony default export */ const StatisticSection_StatisticSection = (StatisticSection);

;// CONCATENATED MODULE: ./src/pages/programmerzamannow/index.js
















const getServerSideProps = async ()=>{
    const { data  } = await external_axios_default().get(`${"https://api.codepolitan.com"}/course/tag/pzn`);
    const response = {
        data
    };
    const course = response.data;
    return {
        props: {
            course
        }
    };
};
const PznLanding = ({ course  })=>{
    const { 0: limit , 1: setLimit  } = (0,external_react_.useState)(8);
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
        children: [
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)((head_default()), {
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx("title", {
                        children: "Programmer Zaman Now - Codepolitan"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        name: "title",
                        content: "Kursus Coding Online Programmer Zaman Now Selama 1 Semester"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        name: "description",
                        content: "Saya Eko Kurniawan Khannedy, Technical Architect di Blibli.com. Dengan pengalaman saya lebih dari 15 tahun berkarya dan 11 tahun berkarir di bidang programming akan membantu kamu belajar coding lebih cepat dan terarah untuk menjadi Programmer Zaman Now"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "og:type",
                        content: "website"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "og:url",
                        content: "https://codepolitan.com/program/mobirise"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "og:title",
                        content: "Kursus Coding Online Programmer Zaman Now Selama 1 Semester"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "og:description",
                        content: "Saya Eko Kurniawan Khannedy, Technical Architect di Blibli.com. Dengan pengalaman saya lebih dari 15 tahun berkarya dan 11 tahun berkarir di bidang programming akan membantu kamu belajar coding lebih cepat dan terarah untuk menjadi Programmer Zaman Now"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "og:image",
                        content: "/assets/img/programmerzamannow/og-image-pzn.jpg"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "twitter:card",
                        content: "summary_large_image"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "twitter:url",
                        content: "https://codepolitan.com/program/mobirise"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "twitter:title",
                        content: "Kursus Coding Online Programmer Zaman Now Selama 1 Semester"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "twitter:description",
                        content: "Saya Eko Kurniawan Khannedy, Technical Architect di Blibli.com. Dengan pengalaman saya lebih dari 15 tahun berkarya dan 11 tahun berkarir di bidang programming akan membantu kamu belajar coding lebih cepat dan terarah untuk menjadi Programmer Zaman Now"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "twitter:image",
                        content: "/assets/img/programmerzamannow/og-image-pzn.jpg"
                    })
                ]
            }),
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)(Layout/* default */.Z, {
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx(Hero_Hero, {}),
                    /*#__PURE__*/ jsx_runtime_.jsx(StatisticSection_StatisticSection, {}),
                    /*#__PURE__*/ jsx_runtime_.jsx("section", {
                        className: "section",
                        id: "about",
                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: "container p-5",
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                    className: "row mb-5",
                                    children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                        className: "col text-center",
                                        children: /*#__PURE__*/ jsx_runtime_.jsx("h2", {
                                            className: "section-title",
                                            children: "Bagaimana Cara Belajarnya?"
                                        })
                                    })
                                }),
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                    className: "row justify-content-center",
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                            className: "col-md-6 col-lg-4 mb-3",
                                            children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                className: "card rounded bg-light-orange border-0",
                                                style: {
                                                    height: '25rem'
                                                },
                                                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: "card-body p-5 text-muted text-center",
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx(react_fontawesome_.FontAwesomeIcon, {
                                                            style: {
                                                                fontSize: '3em'
                                                            },
                                                            icon: free_solid_svg_icons_.faFileVideo
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx("h5", {
                                                            className: "mt-4",
                                                            children: "Step by Step Video Tutorial Premium"
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                            children: "Terdapat ratusan materi belajar dalam format video yang telah saya susun secara komprehensif untuk membantumu belajar lebih mudah"
                                                        })
                                                    ]
                                                })
                                            })
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                            className: "col-md-6 col-lg-4 mb-3",
                                            children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                className: "card rounded bg-light-green border-0",
                                                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: "card-body p-5 text-muted text-center",
                                                    style: {
                                                        height: '25rem'
                                                    },
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx(react_fontawesome_.FontAwesomeIcon, {
                                                            style: {
                                                                fontSize: '3em'
                                                            },
                                                            icon: free_solid_svg_icons_.faComments
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx("h5", {
                                                            className: "mt-4",
                                                            children: "Forum Tanya Jawab Khusus Anggota"
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                            children: "Kamu bisa bertanya seputar permasalahan belajarmu di forum tanya jawab yang telah disediakan, saya sendiri yang akan langsung menjawabnya"
                                                        })
                                                    ]
                                                })
                                            })
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                            className: "col-md-6 col-lg-4 mb-3",
                                            children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                className: "card rounded bg-light-blue border-0",
                                                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: "card-body p-5 text-muted text-center",
                                                    style: {
                                                        height: '25rem'
                                                    },
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx(react_fontawesome_.FontAwesomeIcon, {
                                                            style: {
                                                                fontSize: '3em'
                                                            },
                                                            icon: free_solid_svg_icons_.faUserCheck
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx("h5", {
                                                            className: "mt-4",
                                                            children: "Mentoring Tatap Muka Secara Daring"
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                            children: "Saya akan mengadakan mentoring tatap muka secara daring 1 kali setiap bulan, kamu bisa berkonsultasi langsung pada sesi tersebut"
                                                        })
                                                    ]
                                                })
                                            })
                                        })
                                    ]
                                })
                            ]
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx(BenefitSection/* default */.Z, {
                        title: "Mengapa Belajar di Codepolitan?"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("section", {
                        className: "section bg-light",
                        children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: "container p-5",
                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                className: "row justify-content-around",
                                children: [
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: "col-lg-6 mt-3",
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("h2", {
                                                className: "section-title mb-4",
                                                children: "Cukup luangkan waktu 2 jam sehari. Bisa?"
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                className: "text-muted",
                                                children: "Dengan bergabung dalam program ini, selama 1 semester saya akan membimbingmu belajar coding secara online melalui rangkaian kelas online yang telah saya susun."
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                className: "text-muted",
                                                children: "Kamu cukup meluangkan waktu minimal 2 jam per hari, untuk mempelajari materi dan praktek. Percayalah, dalam waktu 6 bulan kamu akan merasakan manfaatnya."
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                className: "text-muted",
                                                children: "Ayo akselerasi kecepatan belajar codingmu. Saya tunggu di dalam kelas."
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                children: /*#__PURE__*/ jsx_runtime_.jsx("strong", {
                                                    children: "Eko Kurniawan Khannedy"
                                                })
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                        className: "col-lg-6 order-first order-lg-last my-5",
                                        children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                            className: "ratio ratio-16x9",
                                            children: /*#__PURE__*/ jsx_runtime_.jsx("iframe", {
                                                style: {
                                                    borderRadius: '25px'
                                                },
                                                src: "https://www.youtube.com/embed/1h50IfH2zdU",
                                                title: "YouTube video player",
                                                frameBorder: "0",
                                                allow: "accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture",
                                                allowFullScreen: true
                                            })
                                        })
                                    })
                                ]
                            })
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("section", {
                        className: "section bg-light-blue",
                        children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: "container p-5",
                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                className: "row",
                                children: [
                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                        className: "col-lg-6 mt-5 mb-lg-5",
                                        children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                            className: "img-fluid",
                                            src: "/assets/img/programmerzamannow/logo-talent-hub.png",
                                            alt: "Talent Hub"
                                        })
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                        className: "col-lg-6 my-5",
                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                            className: "py-lg-5",
                                            children: [
                                                /*#__PURE__*/ jsx_runtime_.jsx("h2", {
                                                    className: "section-title",
                                                    children: "Terhubung dengan Industri Melalui Talent Hub Codepolitan"
                                                }),
                                                /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                    className: "my-3 text-muted",
                                                    children: "Belajar dan tingkatkan kemampuan codingmu, kemudian sempurnakan profile dan portfoliomu di Talent Hub Codepolitan untuk membuka berbagai peluang terhubung dengan industri. Mulai bangun karirmu sebagai programmer zaman now."
                                                })
                                            ]
                                        })
                                    })
                                ]
                            })
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx(RoadmapSection/* default */.Z, {}),
                    /*#__PURE__*/ jsx_runtime_.jsx("section", {
                        className: "section",
                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: "container p-5",
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                    className: "row",
                                    children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: "col text-center",
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("h2", {
                                                className: "section-title mb-3",
                                                children: "Kelas-kelas Terbaru"
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                className: "text-muted w-75 mx-auto",
                                                children: "Materi belajar akan terus bertambah seiring waktu, dan kamu tidak perlu membelinya satu per satu. Cukup gabung program semuanya bisa kamu akses."
                                            })
                                        ]
                                    })
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                    className: "row my-4",
                                    children: course?.map((item, index)=>{
                                        return(/*#__PURE__*/ jsx_runtime_.jsx("div", {
                                            className: "col-md-6 col-lg-3 mb-3",
                                            children: /*#__PURE__*/ jsx_runtime_.jsx(CourseCard/* default */.Z, {
                                                thumbnail: item.thumbnail,
                                                title: item.title,
                                                totalModule: item.total_module,
                                                link: `/programmerzamannow/course/${item.slug}`
                                            })
                                        }, index));
                                    }).reverse().slice(0, limit)
                                }),
                                course.length <= limit ? '' : /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                    className: "row",
                                    children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                        className: "col text-center",
                                        children: /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                            onClick: ()=>setLimit(limit + 4)
                                            ,
                                            className: "btn btn-primary px-4 py-2",
                                            children: "Lihat Lainnya"
                                        })
                                    })
                                })
                            ]
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("section", {
                        className: "section bg-light",
                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: "container p-5",
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                    className: "row text-center",
                                    children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: "col",
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("h2", {
                                                className: "section-title",
                                                children: "Langkah Sukses Menjadi Mahir"
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                className: "text-muted",
                                                children: "Belajar lebih terarah dan hasil maksimal dengan 4 langkah berikut:"
                                            })
                                        ]
                                    })
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                    className: "row mt-4",
                                    children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: "col px-5",
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                className: "d-none d-md-block",
                                                children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                    className: "img-fluid",
                                                    src: "/assets/img/program/cara-belajar.png",
                                                    alt: "Cara Belajar"
                                                })
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                className: "d-md-none",
                                                children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                    className: "img-fluid",
                                                    src: "/assets/img/program/cara-belajar-small.png",
                                                    alt: "Cara Belajar"
                                                })
                                            })
                                        ]
                                    })
                                })
                            ]
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("section", {
                        className: "section",
                        style: {
                            backgroundColor: '#eee'
                        },
                        children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: "container px-5 pt-5 pb-5 pb-lg-0",
                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                className: "row",
                                children: [
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: "col-lg-6 mt-lg-5 text-muted",
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("h2", {
                                                className: "section-title",
                                                children: "Manfaat Gabung Program"
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                className: "mt-3",
                                                children: "Banyak peluang yang akan terbuka jika kamu memiliki skill coding."
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: "d-flex align-items-start",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx(react_fontawesome_.FontAwesomeIcon, {
                                                        style: {
                                                            fontSize: '2em',
                                                            opacity: 0.5
                                                        },
                                                        className: "me-3 mt-1",
                                                        icon: free_solid_svg_icons_.faCheckCircle
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                        children: "Bekerja pada sebuah perusahaan atau startup digital menjadi seorang programmer profesional"
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: "d-flex align-items-start",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx(react_fontawesome_.FontAwesomeIcon, {
                                                        style: {
                                                            fontSize: '2em',
                                                            opacity: 0.5
                                                        },
                                                        className: "me-3 mt-1",
                                                        icon: free_solid_svg_icons_.faCheckCircle
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                        children: "Menjadi seorang freelancer yang bekerja secara remote dari rumah yang bekerja untuk klien baik dalam maupun luar negeri."
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: "d-flex align-items-start",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx(react_fontawesome_.FontAwesomeIcon, {
                                                        style: {
                                                            fontSize: '2em',
                                                            opacity: 0.5
                                                        },
                                                        className: "me-3 mt-1",
                                                        icon: free_solid_svg_icons_.faCheckCircle
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                        children: "Membangun bisnismu sendiri dengan mengembangkan aplikasi atau web yang kamu ciptakan."
                                                    })
                                                ]
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                        className: "col d-none d-lg-block",
                                        children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                            className: "img-fluid",
                                            src: "/assets/img/programmerzamannow/gabung-program.png",
                                            alt: "Manfaat Gabung Program"
                                        })
                                    })
                                ]
                            })
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx(CtaSection_CtaSection, {
                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: "d-grid d-md-inline-block gap-2 mt-5",
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                    className: "btn btn-primary px-3 py-2 mx-2",
                                    href: "#membership",
                                    children: "Gabung Member"
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                                    href: "/programmerzamannow/roadmap",
                                    children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                        className: "btn btn-outline-light px-3 py-2 mx-2",
                                        children: "Lihat Roadmap"
                                    })
                                })
                            ]
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("section", {
                        className: "section bg-light-blue",
                        id: "membership",
                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: "container p-5",
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                    className: "row",
                                    children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: "col",
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("h2", {
                                                className: "section-title",
                                                children: "Siap untuk Memulai?"
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                className: "text-muted my-3",
                                                children: "Pilih salah satu dari paket belajar berikut untuk bergabung dalam program"
                                            })
                                        ]
                                    })
                                }),
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                    className: "row justify-content-center my-5",
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                            className: "col-md-6 col-xl-4 mb-3",
                                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: "card border-0",
                                                style: {
                                                    borderRadius: '10px'
                                                },
                                                children: [
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                        className: "card-body text-center text-md-start",
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("h5", {
                                                                children: "Member 6 Bulan"
                                                            }),
                                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("span", {
                                                                className: "text-muted",
                                                                children: [
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("del", {
                                                                        children: " Rp. 774.000"
                                                                    }),
                                                                    " "
                                                                ]
                                                            }),
                                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("p", {
                                                                className: "my-3 text-center",
                                                                children: [
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                                        style: {
                                                                            position: 'relative',
                                                                            top: '-1.5em',
                                                                            fontWeight: 'bold'
                                                                        },
                                                                        children: "Rp"
                                                                    }),
                                                                    " ",
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                                        style: {
                                                                            fontSize: '2.5em',
                                                                            fontWeight: 'bold'
                                                                        },
                                                                        children: "499.000"
                                                                    })
                                                                ]
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                className: "text-center mb-2",
                                                                children: /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                                    className: "badge bg-pink",
                                                                    children: "Hemat Rp. 275.000"
                                                                })
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                                className: "text-center text-muted",
                                                                children: /*#__PURE__*/ jsx_runtime_.jsx("small", {
                                                                    children: "Akses belajar 56 kelas selama 180 hari"
                                                                })
                                                            })
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                                                        href: "https://pay.codepolitan.com/?slug=programmer-zaman-now-reguler",
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                            className: "link",
                                                            children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                className: "card-footer bg-codepolitan",
                                                                style: {
                                                                    borderBottomRightRadius: '10px',
                                                                    borderBottomLeftRadius: '10px'
                                                                },
                                                                children: /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                                    className: "text-center text-white my-auto",
                                                                    children: "BELI ROADMAP"
                                                                })
                                                            })
                                                        })
                                                    })
                                                ]
                                            })
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                            className: "col-md-6 col-xl-4 mb-3",
                                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: "card border-0",
                                                style: {
                                                    borderRadius: '10px'
                                                },
                                                children: [
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                        className: "card-body text-center text-md-start",
                                                        children: [
                                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                                className: "row",
                                                                children: [
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                        className: "col-auto",
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("h5", {
                                                                            children: "Member 4 Bulan"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                        className: "col text-end",
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                                            className: "badge bg-secondary",
                                                                            children: "Best seller"
                                                                        })
                                                                    })
                                                                ]
                                                            }),
                                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("span", {
                                                                className: "text-muted",
                                                                children: [
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("del", {
                                                                        children: " Rp. 516.000"
                                                                    }),
                                                                    " "
                                                                ]
                                                            }),
                                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("p", {
                                                                className: "my-3 text-center",
                                                                children: [
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                                        style: {
                                                                            position: 'relative',
                                                                            top: '-1.5em',
                                                                            fontWeight: 'bold'
                                                                        },
                                                                        children: "Rp"
                                                                    }),
                                                                    " ",
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                                        style: {
                                                                            fontSize: '2.5em',
                                                                            fontWeight: 'bold'
                                                                        },
                                                                        children: "349.000"
                                                                    })
                                                                ]
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                className: "text-center mb-2",
                                                                children: /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                                    className: "badge bg-pink",
                                                                    children: "Hemat Rp. 167.000"
                                                                })
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                                className: "text-center text-muted",
                                                                children: /*#__PURE__*/ jsx_runtime_.jsx("small", {
                                                                    children: "Akses belajar 56 kelas selama 120 hari"
                                                                })
                                                            })
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                                                        href: "https://pay.codepolitan.com/?slug=programmer-zaman-now-ngebut",
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                            className: "link",
                                                            children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                className: "card-footer bg-codepolitan",
                                                                style: {
                                                                    borderBottomRightRadius: '10px',
                                                                    borderBottomLeftRadius: '10px'
                                                                },
                                                                children: /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                                    className: "text-center text-white my-auto",
                                                                    children: "BELI ROADMAP"
                                                                })
                                                            })
                                                        })
                                                    })
                                                ]
                                            })
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                            className: "col-md-6 col-xl-4 mb-3",
                                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: "card border-0",
                                                style: {
                                                    borderRadius: '10px',
                                                    height: '100%'
                                                },
                                                children: [
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                        className: "card-body text-center text-md-start",
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("h5", {
                                                                children: "Member 1 Bulan"
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("br", {}),
                                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("p", {
                                                                className: "my-3 text-center",
                                                                children: [
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                                        style: {
                                                                            position: 'relative',
                                                                            top: '-1.5em',
                                                                            fontWeight: 'bold'
                                                                        },
                                                                        children: "Rp"
                                                                    }),
                                                                    " ",
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                                        style: {
                                                                            fontSize: '2.5em',
                                                                            fontWeight: 'bold'
                                                                        },
                                                                        children: "129.000"
                                                                    })
                                                                ]
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                                className: "text-center text-muted",
                                                                children: /*#__PURE__*/ jsx_runtime_.jsx("small", {
                                                                    children: "Akses belajar 56 kelas selama 30 hari"
                                                                })
                                                            })
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                                                        href: "https://pay.codepolitan.com/?slug=programmer-zaman-now-cicilan",
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                            className: "link",
                                                            children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                className: "card-footer bg-codepolitan",
                                                                style: {
                                                                    borderBottomRightRadius: '10px',
                                                                    borderBottomLeftRadius: '10px'
                                                                },
                                                                children: /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                                    className: "text-center text-white my-auto",
                                                                    children: "BELI ROADMAP"
                                                                })
                                                            })
                                                        })
                                                    })
                                                ]
                                            })
                                        })
                                    ]
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                    className: "row mb-3",
                                    children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                        className: "col-12 text-center",
                                        children: /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                            className: "text-muted",
                                            children: "Benefit yang akan kamu dapatkan"
                                        })
                                    })
                                }),
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                    className: "row d-flex justify-content-center px-lg-5",
                                    children: [
                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                            className: "col-md-6",
                                            children: [
                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: "d-flex align-items-start",
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx(react_fontawesome_.FontAwesomeIcon, {
                                                            style: {
                                                                fontSize: '1.5em'
                                                            },
                                                            className: "me-3 mt-1 text-primary",
                                                            icon: free_solid_svg_icons_.faCheckSquare
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                            className: "text-muted",
                                                            children: "Kelas online premium yang tidak ada di Youtube gratis Programmer Zaman Now"
                                                        })
                                                    ]
                                                }),
                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: "d-flex align-items-start",
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx(react_fontawesome_.FontAwesomeIcon, {
                                                            style: {
                                                                fontSize: '1.5em'
                                                            },
                                                            className: "me-3 mt-1 text-primary",
                                                            icon: free_solid_svg_icons_.faCheckSquare
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                            className: "text-muted",
                                                            children: "Peluang terhubung dengan industri melalui fitur Talent Hub Codepolitan"
                                                        })
                                                    ]
                                                }),
                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: "d-flex align-items-start",
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx(react_fontawesome_.FontAwesomeIcon, {
                                                            style: {
                                                                fontSize: '1.5em'
                                                            },
                                                            className: "me-3 mt-1 text-primary",
                                                            icon: free_solid_svg_icons_.faCheckSquare
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                            className: "text-muted",
                                                            children: "Semua materi belajar dan semua roadmap Belajar (tidak hanya 1 kelas)"
                                                        })
                                                    ]
                                                }),
                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: "d-flex align-items-start",
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx(react_fontawesome_.FontAwesomeIcon, {
                                                            style: {
                                                                fontSize: '1.5em'
                                                            },
                                                            className: "me-3 mt-1 text-primary",
                                                            icon: free_solid_svg_icons_.faCheckSquare
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                            className: "text-muted",
                                                            children: "Forum diskusi tanya jawab yang dijawab langsung oleh mentor"
                                                        })
                                                    ]
                                                })
                                            ]
                                        }),
                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                            className: "col-md-6",
                                            children: [
                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: "d-flex align-items-start",
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx(react_fontawesome_.FontAwesomeIcon, {
                                                            style: {
                                                                fontSize: '1.5em'
                                                            },
                                                            className: "me-3 mt-1 text-primary",
                                                            icon: free_solid_svg_icons_.faCheckSquare
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                            className: "text-muted",
                                                            children: "Mentoring tatap muka secara daring 1 bulan sekali"
                                                        })
                                                    ]
                                                }),
                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: "d-flex align-items-start",
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx(react_fontawesome_.FontAwesomeIcon, {
                                                            style: {
                                                                fontSize: '1.5em'
                                                            },
                                                            className: "me-3 mt-lg-2 text-primary",
                                                            icon: free_solid_svg_icons_.faCheckSquare
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                            className: "text-muted",
                                                            children: "Akses materi belajar sesuai dengan paket waktu belajar yang kamu pilih"
                                                        })
                                                    ]
                                                }),
                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: "d-flex align-items-start",
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx(react_fontawesome_.FontAwesomeIcon, {
                                                            style: {
                                                                fontSize: '1.5em'
                                                            },
                                                            className: "me-3 mt-1 text-primary",
                                                            icon: free_solid_svg_icons_.faCheckSquare
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                            className: "text-muted",
                                                            children: "Garansi 100% uang kembali jika dalam 3 hari tidak puas"
                                                        })
                                                    ]
                                                })
                                            ]
                                        })
                                    ]
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                    className: "row my-4",
                                    children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: "col text-center",
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                className: "text-muted",
                                                children: "Hanya Membutuhkan Sebagian Roadmap Atau Hanya Satuan ?"
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                                                href: "/programmerzamannow/roadmap",
                                                children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                    className: "btn btn-outline-primary px-3",
                                                    children: "Lihat disini"
                                                })
                                            })
                                        ]
                                    })
                                })
                            ]
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx(WarrantySection/* default */.Z, {
                        backgroundColor: "bg-light"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx(FaqSection_FaqSection, {})
                ]
            })
        ]
    }));
};
/* harmony default export */ const programmerzamannow = (PznLanding);


/***/ }),

/***/ 5368:
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/free-brands-svg-icons");

/***/ }),

/***/ 197:
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/free-regular-svg-icons");

/***/ }),

/***/ 6466:
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/free-solid-svg-icons");

/***/ }),

/***/ 7197:
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/react-fontawesome");

/***/ }),

/***/ 2167:
/***/ ((module) => {

"use strict";
module.exports = require("axios");

/***/ }),

/***/ 2733:
/***/ ((module) => {

"use strict";
module.exports = require("fuse.js");

/***/ }),

/***/ 562:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/server/denormalize-page-path.js");

/***/ }),

/***/ 4014:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/i18n/normalize-locale-path.js");

/***/ }),

/***/ 8524:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/is-plain-object.js");

/***/ }),

/***/ 8020:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/mitt.js");

/***/ }),

/***/ 4964:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router-context.js");

/***/ }),

/***/ 9565:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/get-asset-path-from-route.js");

/***/ }),

/***/ 4365:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/get-middleware-regex.js");

/***/ }),

/***/ 1428:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/is-dynamic.js");

/***/ }),

/***/ 1292:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/parse-relative-url.js");

/***/ }),

/***/ 979:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/querystring.js");

/***/ }),

/***/ 6052:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/resolve-rewrites.js");

/***/ }),

/***/ 4226:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/route-matcher.js");

/***/ }),

/***/ 5052:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/route-regex.js");

/***/ }),

/***/ 9232:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/utils.js");

/***/ }),

/***/ 968:
/***/ ((module) => {

"use strict";
module.exports = require("next/head");

/***/ }),

/***/ 1853:
/***/ ((module) => {

"use strict";
module.exports = require("next/router");

/***/ }),

/***/ 6689:
/***/ ((module) => {

"use strict";
module.exports = require("react");

/***/ }),

/***/ 609:
/***/ ((module) => {

"use strict";
module.exports = require("react-countup");

/***/ }),

/***/ 997:
/***/ ((module) => {

"use strict";
module.exports = require("react/jsx-runtime");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [7730,9285,1664,4528,4272,5997,9725,4177], () => (__webpack_exec__(7857)));
module.exports = __webpack_exports__;

})();