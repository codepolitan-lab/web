(() => {
var exports = {};
exports.id = 1793;
exports.ids = [1793];
exports.modules = {

/***/ 9625:
/***/ ((module) => {

// Exports
module.exports = {
	"card_beasiswa": "CardBeasiswa_card_beasiswa__VSMw6",
	"card_img_top": "CardBeasiswa_card_img_top__oAWMw"
};


/***/ }),

/***/ 3472:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ beasiswa),
  "getServerSideProps": () => (/* binding */ getServerSideProps)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: external "axios"
var external_axios_ = __webpack_require__(2167);
var external_axios_default = /*#__PURE__*/__webpack_require__.n(external_axios_);
// EXTERNAL MODULE: external "next/head"
var head_ = __webpack_require__(968);
var head_default = /*#__PURE__*/__webpack_require__.n(head_);
// EXTERNAL MODULE: ./src/components/global/Banner/Banner.js
var Banner = __webpack_require__(9154);
// EXTERNAL MODULE: ./src/components/global/Cards/CardBeasiswa/CardBeasiswa.module.scss
var CardBeasiswa_module = __webpack_require__(9625);
var CardBeasiswa_module_default = /*#__PURE__*/__webpack_require__.n(CardBeasiswa_module);
// EXTERNAL MODULE: external "@fortawesome/free-solid-svg-icons"
var free_solid_svg_icons_ = __webpack_require__(6466);
// EXTERNAL MODULE: external "@fortawesome/react-fontawesome"
var react_fontawesome_ = __webpack_require__(7197);
;// CONCATENATED MODULE: ./src/components/global/Cards/CardBeasiswa/CardBeasiswa.js




const CardBeasiswa = ({ thumbnail , organizer , title , description , closedAt , participats , link  })=>{
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
        className: `${(CardBeasiswa_module_default()).card_beasiswa} card h-100`,
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx("img", {
                src: thumbnail || "/assets/img/placeholder.jpg",
                className: `${(CardBeasiswa_module_default()).card_img_top} card-img-top`,
                alt: title
            }),
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                className: "card-body",
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx("span", {
                        className: "text-primary",
                        children: /*#__PURE__*/ jsx_runtime_.jsx("small", {
                            children: /*#__PURE__*/ jsx_runtime_.jsx("strong", {
                                children: organizer
                            })
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("h5", {
                        className: "card-title",
                        children: title
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("p", {
                        className: "card-text text-muted",
                        children: /*#__PURE__*/ jsx_runtime_.jsx("small", {
                            children: description.slice(0, 100) + '...'
                        })
                    }),
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("small", {
                        className: "text-muted",
                        style: {
                            fontSize: 'smaller'
                        },
                        children: [
                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("p", {
                                className: "mb-1",
                                children: [
                                    /*#__PURE__*/ jsx_runtime_.jsx(react_fontawesome_.FontAwesomeIcon, {
                                        className: "text-primary me-1",
                                        fixedWidth: true,
                                        icon: free_solid_svg_icons_.faCalendar
                                    }),
                                    closedAt
                                ]
                            }),
                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("p", {
                                className: "m-0",
                                children: [
                                    /*#__PURE__*/ jsx_runtime_.jsx(react_fontawesome_.FontAwesomeIcon, {
                                        className: "text-primary me-1",
                                        fixedWidth: true,
                                        icon: free_solid_svg_icons_.faUsers
                                    }),
                                    participats
                                ]
                            })
                        ]
                    }),
                    link && /*#__PURE__*/ (0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx("hr", {
                                style: {
                                    height: '1px',
                                    color: '#ccc'
                                }
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                style: {
                                    borderRadius: '10px'
                                },
                                href: link,
                                className: "btn btn-primary float-end",
                                children: "Daftar Sekarang"
                            })
                        ]
                    })
                ]
            })
        ]
    }));
};
/* harmony default export */ const CardBeasiswa_CardBeasiswa = (CardBeasiswa);

// EXTERNAL MODULE: ./src/components/global/Layout/Layout.js + 5 modules
var Layout = __webpack_require__(4528);
;// CONCATENATED MODULE: ./src/pages/beasiswa/index.js






const getServerSideProps = async ()=>{
    const { data  } = await external_axios_default().get(`${"https://apps.codepolitan.com"}/api/entry/index/beasiswa?perpage=100`);
    const response = {
        data
    };
    const scholarships = response.data.results;
    return {
        props: {
            scholarships
        }
    };
};
const Beasiswa = ({ scholarships  })=>{
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
        children: [
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)((head_default()), {
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx("title", {
                        children: "Beasiswa - Codepolitan"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        name: "title",
                        content: "Website Belajar Coding Bahasa Indonesia - Codepolitan"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        name: "description",
                        content: "Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "og:type",
                        content: "website"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "og:url",
                        content: "https://codepolitan.com"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "og:title",
                        content: "Website Belajar Coding Bahasa Indonesia - Codepolitan"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "og:description",
                        content: "Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "og:image",
                        content: "https://i.ibb.co/SXP8fGc/OG-image.jpg"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "twitter:card",
                        content: "summary_large_image"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "twitter:url",
                        content: "https://codepolitan.com"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "twitter:title",
                        content: "Website Belajar Coding Bahasa Indonesia - Codepolitan"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "twitter:description",
                        content: "Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "twitter:image",
                        content: "https://i.ibb.co/SXP8fGc/OG-image.jpg"
                    })
                ]
            }),
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)(Layout/* default */.Z, {
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx(Banner/* default */.Z, {
                        background: "/assets/img/backgrounds/banner-beasiswa.png",
                        title: "Beasiswa Belajar Coding",
                        subtitle: "Dapatkan Kesempatan Belajar Coding Gratis Melalui Program Beasiswa CODEPOLITAN"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("section", {
                        className: "section",
                        id: "beasiswa",
                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: "container p-4 p-lg-5",
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                    className: "row mb-4",
                                    children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: "col",
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("h2", {
                                                className: "section-title",
                                                children: "Program Beasiswa"
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                className: "text-muted",
                                                children: "Program kerjasama codepolitan dan partner bisnis yang tersedia"
                                            })
                                        ]
                                    })
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                    className: "row",
                                    children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                        className: "col",
                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                            children: [
                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                                    className: "nav nav-tabs nav-justified d-grid d-md-flex d-lg-none",
                                                    id: "BeasiswaTab",
                                                    role: "tablist",
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                            className: "nav-item",
                                                            role: "presentation",
                                                            children: /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                                                className: "nav-link active",
                                                                id: "all-tab",
                                                                "data-bs-toggle": "tab",
                                                                "data-bs-target": "#all",
                                                                type: "button",
                                                                role: "tab",
                                                                "aria-controls": "all",
                                                                "aria-selected": "true",
                                                                children: "Masih Berlangsung"
                                                            })
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                            className: "nav-item",
                                                            role: "presentation",
                                                            children: /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                                                className: "nav-link",
                                                                id: "solved-tab",
                                                                "data-bs-toggle": "tab",
                                                                "data-bs-target": "#solved",
                                                                type: "button",
                                                                role: "tab",
                                                                "aria-controls": "solved",
                                                                "aria-selected": "false",
                                                                children: "Sudah Berakhir"
                                                            })
                                                        })
                                                    ]
                                                }),
                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                                    className: "nav nav-tabs d-none d-lg-flex",
                                                    id: "BeasiswaTab",
                                                    role: "tablist",
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                            className: "nav-item",
                                                            role: "presentation",
                                                            children: /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                                                className: "nav-link active",
                                                                id: "all-tab",
                                                                "data-bs-toggle": "tab",
                                                                "data-bs-target": "#all",
                                                                type: "button",
                                                                role: "tab",
                                                                "aria-controls": "all",
                                                                "aria-selected": "true",
                                                                children: "Masih Berlangsung"
                                                            })
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                            className: "nav-item",
                                                            role: "presentation",
                                                            children: /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                                                className: "nav-link",
                                                                id: "solved-tab",
                                                                "data-bs-toggle": "tab",
                                                                "data-bs-target": "#solved",
                                                                type: "button",
                                                                role: "tab",
                                                                "aria-controls": "solved",
                                                                "aria-selected": "false",
                                                                children: "Sudah Berakhir"
                                                            })
                                                        })
                                                    ]
                                                }),
                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: "tab-content",
                                                    id: "ForumTabContent",
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                            className: "tab-pane fade show active",
                                                            id: "all",
                                                            role: "tabpanel",
                                                            "aria-labelledby": "all-tab",
                                                            children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                className: "row my-3",
                                                                children: scholarships.filter((scholarship)=>scholarship.status !== 'close'
                                                                ).map((scholarship, index)=>{
                                                                    return(/*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                        className: "col-md-6 col-lg-4 mb-3",
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx(CardBeasiswa_CardBeasiswa, {
                                                                            thumbnail: scholarship.image,
                                                                            organizer: scholarship.provider,
                                                                            title: scholarship.title,
                                                                            description: scholarship.description,
                                                                            closedAt: scholarship.date,
                                                                            participats: scholarship.participant,
                                                                            link: scholarship.status !== 'close' && scholarship.link
                                                                        })
                                                                    }, index));
                                                                }).reverse()
                                                            })
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                            className: "tab-pane fade",
                                                            id: "solved",
                                                            role: "tabpanel",
                                                            "aria-labelledby": "solved-tab",
                                                            children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                className: "row my-3",
                                                                children: scholarships.filter((scholarship)=>scholarship.status === 'close'
                                                                ).map((scholarship, index)=>{
                                                                    return(/*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                        className: "col-md-6 col-lg-4 mb-3",
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx(CardBeasiswa_CardBeasiswa, {
                                                                            thumbnail: scholarship.image,
                                                                            organizer: scholarship.provider,
                                                                            title: scholarship.title,
                                                                            description: scholarship.description,
                                                                            closedAt: scholarship.date,
                                                                            participats: scholarship.participant,
                                                                            link: scholarship.status !== 'close' && scholarship.link
                                                                        })
                                                                    }, index));
                                                                }).reverse()
                                                            })
                                                        })
                                                    ]
                                                })
                                            ]
                                        })
                                    })
                                })
                            ]
                        })
                    })
                ]
            })
        ]
    }));
};
/* harmony default export */ const beasiswa = (Beasiswa);


/***/ }),

/***/ 5368:
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/free-brands-svg-icons");

/***/ }),

/***/ 197:
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/free-regular-svg-icons");

/***/ }),

/***/ 6466:
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/free-solid-svg-icons");

/***/ }),

/***/ 7197:
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/react-fontawesome");

/***/ }),

/***/ 2167:
/***/ ((module) => {

"use strict";
module.exports = require("axios");

/***/ }),

/***/ 2733:
/***/ ((module) => {

"use strict";
module.exports = require("fuse.js");

/***/ }),

/***/ 562:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/server/denormalize-page-path.js");

/***/ }),

/***/ 4014:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/i18n/normalize-locale-path.js");

/***/ }),

/***/ 8524:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/is-plain-object.js");

/***/ }),

/***/ 8020:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/mitt.js");

/***/ }),

/***/ 4964:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router-context.js");

/***/ }),

/***/ 9565:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/get-asset-path-from-route.js");

/***/ }),

/***/ 4365:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/get-middleware-regex.js");

/***/ }),

/***/ 1428:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/is-dynamic.js");

/***/ }),

/***/ 1292:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/parse-relative-url.js");

/***/ }),

/***/ 979:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/querystring.js");

/***/ }),

/***/ 6052:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/resolve-rewrites.js");

/***/ }),

/***/ 4226:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/route-matcher.js");

/***/ }),

/***/ 5052:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/route-regex.js");

/***/ }),

/***/ 9232:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/utils.js");

/***/ }),

/***/ 968:
/***/ ((module) => {

"use strict";
module.exports = require("next/head");

/***/ }),

/***/ 1853:
/***/ ((module) => {

"use strict";
module.exports = require("next/router");

/***/ }),

/***/ 6689:
/***/ ((module) => {

"use strict";
module.exports = require("react");

/***/ }),

/***/ 997:
/***/ ((module) => {

"use strict";
module.exports = require("react/jsx-runtime");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [7730,9285,1664,4528,9154], () => (__webpack_exec__(3472)));
module.exports = __webpack_exports__;

})();