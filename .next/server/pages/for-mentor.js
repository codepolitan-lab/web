(() => {
var exports = {};
exports.id = 125;
exports.ids = [125];
exports.modules = {

/***/ 6841:
/***/ ((module) => {

// Exports
module.exports = {
	"hero": "Hero_hero__bMslr",
	"hero_title": "Hero_hero_title__nCOws",
	"lead": "Hero_lead__76wcM"
};


/***/ }),

/***/ 9111:
/***/ ((module) => {

// Exports
module.exports = {
	"background": "SectionKeuntungan_background__kSq_n"
};


/***/ }),

/***/ 1136:
/***/ ((module) => {

// Exports
module.exports = {
	"vector-position": "SectionMentorBekerja_vector-position__wcLpI",
	"vector-responsive": "SectionMentorBekerja_vector-responsive__GVlGp",
	"vector-profit": "SectionMentorBekerja_vector-profit__7kpbT",
	"wrap": "SectionMentorBekerja_wrap__lRxTp"
};


/***/ }),

/***/ 8315:
/***/ ((module) => {

// Exports
module.exports = {
	"text": "SectionPeringkatPenghasilan_text__HcmQs"
};


/***/ }),

/***/ 2190:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ for_mentor),
  "getServerSideProps": () => (/* binding */ getServerSideProps)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: external "next/head"
var head_ = __webpack_require__(968);
var head_default = /*#__PURE__*/__webpack_require__.n(head_);
// EXTERNAL MODULE: ./src/components/forMentor/Hero/Hero.module.scss
var Hero_module = __webpack_require__(6841);
var Hero_module_default = /*#__PURE__*/__webpack_require__.n(Hero_module);
;// CONCATENATED MODULE: ./src/components/forMentor/Hero/Hero.js


const Hero = ()=>{
    return(/*#__PURE__*/ jsx_runtime_.jsx(jsx_runtime_.Fragment, {
        children: /*#__PURE__*/ jsx_runtime_.jsx("section", {
            className: `${(Hero_module_default()).hero} mt-md-5 py-5`,
            children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                className: "container p-4 p-lg-5",
                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                    className: "row",
                    children: [
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: "col-lg-7 text-white text-center text-lg-start my-auto order-last order-md-first",
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("h1", {
                                    className: (Hero_module_default()).hero_title,
                                    children: "Jadilah Partner Pengajar Bersama Codepolitan"
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                    className: `lead mt-4 ${(Hero_module_default()).lead}`,
                                    children: "Melalui program ini Codepolitan ingin menciptakan sebuah peluang dimana para developer bisa mendapatkan penghasilan dengan cara berbagi pengetahuan yang dimilikinya dalam bidang teknologi."
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                    className: "text-center text-md-start mt-4",
                                    children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                        href: "#keuntunganBergabung",
                                        className: "btn btn-primary btn-lg btn-rounded text-white px-5 py-2",
                                        children: "Mulai Sekarang"
                                    })
                                })
                            ]
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: "col-lg-5",
                            children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                className: "img-fluid mt-4 mt-md-0",
                                src: "/assets/img/formentor/hero.png",
                                alt: "Hero"
                            })
                        })
                    ]
                })
            })
        })
    }));
};
/* harmony default export */ const Hero_Hero = (Hero);

// EXTERNAL MODULE: ./src/components/forMentor/Sections/SectionKeuntungan/SectionKeuntungan.module.scss
var SectionKeuntungan_module = __webpack_require__(9111);
var SectionKeuntungan_module_default = /*#__PURE__*/__webpack_require__.n(SectionKeuntungan_module);
;// CONCATENATED MODULE: ./src/components/forMentor/Sections/SectionKeuntungan/SectionKeuntungan.js


const SectionKeuntungan = ()=>{
    return(/*#__PURE__*/ jsx_runtime_.jsx("section", {
        className: `${(SectionKeuntungan_module_default()).background}`,
        id: "keuntunganBergabung",
        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
            className: "container p-4 p-lg-5 text-center",
            children: [
                /*#__PURE__*/ jsx_runtime_.jsx("h4", {
                    className: "text-muted mb-5",
                    children: "Keuntungan Bergabung Program Partner Mentor"
                }),
                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                    className: "row",
                    children: [
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: "col-lg-3 my-2 my-md-0",
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                    src: "/assets/img/formentor/vector/video.svg",
                                    width: 90,
                                    alt: "Image Secure Video"
                                }),
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                    className: "text-muted text-center",
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx("h6", {
                                            className: "my-2 fw-bold",
                                            children: "Secure Video"
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("small", {
                                            className: "text-muted",
                                            children: "Video kamu tidak bisa didownload sehingga dapat melindungi hak kekayaan intelektual mentor dari pembajakan."
                                        })
                                    ]
                                })
                            ]
                        }),
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: "col-lg-3 my-2 my-md-0",
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                    src: "/assets/img/formentor/vector/50.svg",
                                    width: 90,
                                    alt: "Image Profit Sharing"
                                }),
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                    className: "text-muted text-center",
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx("h6", {
                                            className: "my-2 fw-bold",
                                            children: "Profit Sharing"
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("small", {
                                            className: "text-muted",
                                            children: "Bagi hasil akan ditransfer langsung ke rekening mentor setiap awal bulan paling lambat tanggal 5 pada hari kerja."
                                        })
                                    ]
                                })
                            ]
                        }),
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: "col-lg-3 my-2 my-md-0",
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                    src: "/assets/img/formentor/vector/traffic.svg",
                                    width: 90,
                                    alt: "Image Traffic"
                                }),
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                    className: "text-muted text-center",
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx("h6", {
                                            className: "my-2 fw-bold",
                                            children: "Traffic"
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("small", {
                                            className: "text-muted",
                                            children: "Rata-rata traffic harian kami dikunjungi oleh pemula/praktisi teknologi mencapai 17k perhari atau sekitar 500k perbulannya."
                                        })
                                    ]
                                })
                            ]
                        }),
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: "col-lg-3 my-2 my-md-0",
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                    src: "/assets/img/formentor/vector/paper.svg",
                                    width: 90,
                                    alt: "Image Transparansi"
                                }),
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                    className: "text-muted text-center",
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx("h6", {
                                            className: "my-2 fw-bold",
                                            children: "Transparansi"
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("small", {
                                            className: "text-muted",
                                            children: "Catatan keuntungan kamu akan sangat transparan, disini kamu juga dapat memantaunya melalui platform CodePolitan."
                                        })
                                    ]
                                })
                            ]
                        })
                    ]
                }),
                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                    className: "row my-2 my-md-5",
                    children: [
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: "col-lg-3 my-2 my-md-0",
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                    src: "/assets/img/formentor/vector/target.svg",
                                    width: 90,
                                    alt: "Image Brand Terjaga"
                                }),
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                    className: "text-muted text-center",
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx("h6", {
                                            className: "my-2 fw-bold",
                                            children: "Brand Terjaga"
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("small", {
                                            className: "text-muted",
                                            children: "Brand konten kamu akan terjaga karena team CodePolitan akan menampilkan brand kamu."
                                        })
                                    ]
                                })
                            ]
                        }),
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: "col-lg-3 my-2 my-md-0",
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                    src: "/assets/img/formentor/vector/www.svg",
                                    width: 90,
                                    alt: "Image Custom Domain"
                                }),
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                    className: "text-muted text-center",
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx("h6", {
                                            className: "my-2 fw-bold",
                                            children: "Custom Domain"
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("small", {
                                            className: "text-muted",
                                            children: "Custom domain akan diberikan jika mentor memiliki lebih dari 1 kelas terpublikasi."
                                        })
                                    ]
                                })
                            ]
                        }),
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: "col-lg-3 my-2 my-md-0",
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                    src: "/assets/img/formentor/vector/list.svg",
                                    width: 90,
                                    alt: "Image Platform Teruji"
                                }),
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                    className: "text-muted text-center",
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx("h6", {
                                            className: "my-2 fw-bold",
                                            children: "Platform Teruji"
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("small", {
                                            className: "text-muted",
                                            children: "Kami telah berdiri sejak tahun 2017 hingga saat ini sehingga mendapatkan banyak kepercayaan."
                                        })
                                    ]
                                })
                            ]
                        }),
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: "col-lg-3 my-2 my-md-0",
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                    src: "/assets/img/formentor/vector/monitoring.svg",
                                    width: 90,
                                    alt: "Image Monitoring Siswa"
                                }),
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                    className: "text-muted text-center",
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx("h6", {
                                            className: "my-2 fw-bold",
                                            children: "Monitoring Siswa"
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("small", {
                                            className: "text-muted",
                                            children: "Kamu dapat memantau siswa yang belajar bersama kamu melalui platform CodePolitan."
                                        })
                                    ]
                                })
                            ]
                        })
                    ]
                })
            ]
        })
    }));
};
/* harmony default export */ const SectionKeuntungan_SectionKeuntungan = (SectionKeuntungan);

// EXTERNAL MODULE: ./src/components/forMentor/Sections/SectionPeringkatPenghasilan/SectionPeringkatPenghasilan.module.scss
var SectionPeringkatPenghasilan_module = __webpack_require__(8315);
var SectionPeringkatPenghasilan_module_default = /*#__PURE__*/__webpack_require__.n(SectionPeringkatPenghasilan_module);
;// CONCATENATED MODULE: ./src/components/forMentor/Sections/SectionPeringkatPenghasilan/SectionPeringkatPenghasilan.js


const SectionPeringkatPenghasilan = ()=>{
    return(/*#__PURE__*/ jsx_runtime_.jsx("section", {
        id: "profitTotal",
        children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
            className: "container p-4 p-lg-5",
            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                className: "row",
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        className: "col-lg-6",
                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("table", {
                            className: "table table-striped table-bordered shadow",
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("thead", {
                                    className: "bg-codepolitan",
                                    children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("tr", {
                                        className: "text-center text-white",
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("th", {
                                                children: "Mentor Partner"
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("th", {
                                                children: "Total Purchase"
                                            })
                                        ]
                                    })
                                }),
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("tbody", {
                                    className: `${(SectionPeringkatPenghasilan_module_default()).text}`,
                                    children: [
                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("tr", {
                                            className: "position-relative",
                                            children: [
                                                /*#__PURE__*/ jsx_runtime_.jsx("td", {
                                                    children: /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                        children: "Eko Kurniawan khanedy"
                                                    })
                                                }),
                                                /*#__PURE__*/ jsx_runtime_.jsx("td", {
                                                    align: "center",
                                                    children: /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                        children: "1.500 Purchase"
                                                    })
                                                })
                                            ]
                                        }),
                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("tr", {
                                            children: [
                                                /*#__PURE__*/ jsx_runtime_.jsx("td", {
                                                    children: /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                        children: "Angga Risky Setiawan"
                                                    })
                                                }),
                                                /*#__PURE__*/ jsx_runtime_.jsx("td", {
                                                    align: "right",
                                                    children: /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                        children: "750 Purchase"
                                                    })
                                                })
                                            ]
                                        }),
                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("tr", {
                                            children: [
                                                /*#__PURE__*/ jsx_runtime_.jsx("td", {
                                                    children: /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                        children: "Muhammad Amirul Ihsan"
                                                    })
                                                }),
                                                /*#__PURE__*/ jsx_runtime_.jsx("td", {
                                                    align: "right",
                                                    children: /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                        children: "699 Purchase"
                                                    })
                                                })
                                            ]
                                        }),
                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("tr", {
                                            children: [
                                                /*#__PURE__*/ jsx_runtime_.jsx("td", {
                                                    children: /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                        children: "Bagus Budi Cahyono"
                                                    })
                                                }),
                                                /*#__PURE__*/ jsx_runtime_.jsx("td", {
                                                    align: "right",
                                                    children: /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                        children: "645 Purchase"
                                                    })
                                                })
                                            ]
                                        }),
                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("tr", {
                                            children: [
                                                /*#__PURE__*/ jsx_runtime_.jsx("td", {
                                                    children: /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                        children: "Nuris Akbar"
                                                    })
                                                }),
                                                /*#__PURE__*/ jsx_runtime_.jsx("td", {
                                                    align: "right",
                                                    children: /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                        children: "638 Purchase"
                                                    })
                                                })
                                            ]
                                        })
                                    ]
                                })
                            ]
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        className: "col-lg-6 d-flex align-items-center justify-content-center",
                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: "container mt-4 mt-md-0",
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                    className: "h4 lh-base text-muted",
                                    children: "Peringkat Penghasilan Partner Mentor Codepolitan"
                                }),
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("p", {
                                    className: "fs-6 mt-4 lead lh-lg text-muted",
                                    children: [
                                        "Hingga hari ini, pencairan dana yang telah dicarikan oleh partner mentor Codepolitan sebesar ",
                                        /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                            className: "fw-bold",
                                            children: "Rp. 202.680.194"
                                        }),
                                        ". Mereka telah memulainya, sekarang giliran kamu !"
                                    ]
                                })
                            ]
                        })
                    })
                ]
            })
        })
    }));
};
/* harmony default export */ const SectionPeringkatPenghasilan_SectionPeringkatPenghasilan = (SectionPeringkatPenghasilan);

// EXTERNAL MODULE: ./src/components/global/Layout/Layout.js + 5 modules
var Layout = __webpack_require__(4528);
;// CONCATENATED MODULE: ./src/components/forMentor/Sections/SectionCodepolitanSelf/SectionCodepolitanSelf.js

const SectionCodepolitanSelf = ()=>{
    return(/*#__PURE__*/ jsx_runtime_.jsx("section", {
        id: "codepolitanSelf",
        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
            className: "container p-4 p-lg-5",
            children: [
                /*#__PURE__*/ jsx_runtime_.jsx("h4", {
                    className: "text-muted my-4",
                    children: "Codepolitan  VS Self-Publishing Course"
                }),
                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("table", {
                    className: "table shadow overflow-hidden",
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx("thead", {
                            className: "bg-codepolitan",
                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("tr", {
                                className: "text-white",
                                children: [
                                    /*#__PURE__*/ jsx_runtime_.jsx("th", {
                                        className: "ps-md-4 py-3",
                                        children: "Codepolitan"
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx("th", {
                                        className: "ps-md-4 py-md-3",
                                        children: "Self-Publishing Course"
                                    })
                                ]
                            })
                        }),
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("tbody", {
                            children: [
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("tr", {
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx("td", {
                                            children: "Profit sharing dari penjualan kelas antara Codepolitan dengan Mentor"
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("td", {
                                            children: "Seluruh hasil penjualan kelas untuk sendiri"
                                        })
                                    ]
                                }),
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("tr", {
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx("td", {
                                            children: "Bersama CodePolitan meriset tema terbaik"
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("td", {
                                            children: "Riset Tema Kelas Sendiri"
                                        })
                                    ]
                                }),
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("tr", {
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx("td", {
                                            children: "Tim Kurikulum CodePolitan akan membantu mentor dalam menyusun dan mereview silabus"
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("td", {
                                            children: "Silabus disusun sendiri"
                                        })
                                    ]
                                }),
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("tr", {
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx("td", {
                                            children: "Tim Development CodePolitan akan memastikan platform siap untuk kebutuhan kelas mentor"
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("td", {
                                            children: "Membangun dan memelihara platform sendiri"
                                        })
                                    ]
                                }),
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("tr", {
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx("td", {
                                            children: "Tim Produk CodePolitan akan membantu menyiapkan landing page untuk penjualan kelas"
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("td", {
                                            children: "Membuat landing page penjualan sendiri"
                                        })
                                    ]
                                }),
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("tr", {
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx("td", {
                                            children: "Tim Content Codepolitan akan memberikan rekomendasi agar mentor bisa menghasilkan kelas yang berkualitas sesuai standar Codepolitan"
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("td", {
                                            children: "Mencari-cari standar sendiri untuk pembuatan materi kelas"
                                        })
                                    ]
                                }),
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("tr", {
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx("td", {
                                            children: "Tim Marketing Codepolitan akan membantu mengalirkan traffic dan calon pembeli ke kelas mentor"
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("td", {
                                            children: "Promosi kelas online sendiri"
                                        })
                                    ]
                                }),
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("tr", {
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx("td", {
                                            children: "Video editing dibantu oleh tim editor Codepolitan, studio rekaman disediakan (bila perlu)"
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("td", {
                                            children: "Video rekam sendiri dan edit sendiri"
                                        })
                                    ]
                                })
                            ]
                        })
                    ]
                })
            ]
        })
    }));
};
/* harmony default export */ const SectionCodepolitanSelf_SectionCodepolitanSelf = (SectionCodepolitanSelf);

;// CONCATENATED MODULE: ./src/components/forMentor/Sections/SectionQuotes/SectionQuotes.js

const SectionQuotes = ()=>{
    return(/*#__PURE__*/ jsx_runtime_.jsx("section", {
        className: "bg-codepolitan",
        children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
            className: "container p-4 p-lg-5",
            children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                className: "row justify-content-center",
                children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                    className: "col-lg-10",
                    children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        className: "text-center text-white my-3",
                        children: /*#__PURE__*/ jsx_runtime_.jsx("h2", {
                            children: "“Mari menjadi bagian dalam membangun ekosistem yang nyaman untuk belajar pemrograman dan teknologi di Indonesia“"
                        })
                    })
                })
            })
        })
    }));
};
/* harmony default export */ const SectionQuotes_SectionQuotes = (SectionQuotes);

;// CONCATENATED MODULE: ./src/components/forMentor/Sections/SectionLangkah/SectionLangkah.js

const SectionLangkah = ()=>{
    return(/*#__PURE__*/ jsx_runtime_.jsx("section", {
        style: {
            backgroundColor: '#f8f8f8;'
        },
        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
            className: "container p-4 p-lg-5 text-muted",
            children: [
                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                    className: "text-center",
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx("h3", {
                            className: "mt-5 mb-3",
                            children: "Mulai Dengan 3 Langkah Ini"
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx("p", {
                            children: "Dengan memulai langkah berikut kamu akan menjadi partner mentor kami"
                        })
                    ]
                }),
                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                    className: "row mt-5 align-items-center",
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: "col-lg-6",
                            children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                src: "/assets/img/formentor/image-1.png",
                                className: "w-100",
                                alt: "Image Tentukan Materi"
                            })
                        }),
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: "col-lg-6",
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("h5", {
                                    className: "mb-4",
                                    children: "Tentukan materi yang akan dibuat"
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                    children: "Pilih salah satu skill yang kamu kuasai, lalu tentukan topik pembahasan yang tepat sesuai dengan kemampuan yang kamu miliki."
                                })
                            ]
                        })
                    ]
                }),
                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                    className: "row align-items-center",
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: "col-lg-6 order-first order-md-last",
                            children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                src: "/assets/img/formentor/image-2.png",
                                className: "w-100",
                                alt: "Image Buat Video Materi"
                            })
                        }),
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: "col-lg-6",
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("h5", {
                                    className: "mb-4",
                                    children: "Buat Video Materi"
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                    children: "Buat video materi kamu menggunakan alat yang kamu miliki seperti kamera DSLR atau handphone. Buatlah dalam materi yang padat, mudah dipahami dan sesuai dengan silabus yang sudah kamu rancang/"
                                })
                            ]
                        })
                    ]
                }),
                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                    className: "row align-items-center",
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: "col-lg-6",
                            children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                src: "/assets/img/formentor/image-3.png",
                                className: "w-100",
                                alt: "Image Publish Video"
                            })
                        }),
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: "col-lg-6",
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("h5", {
                                    className: "mb-4",
                                    children: "Publish Video"
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                    children: "Kami akan mempersiapkan video yang telah kamu buat diplatform Codepolitan dan kami akan membantu mempromosikan kelas yang kamu kirimkan ke Codepolitan dengan sistem bagi hasil."
                                })
                            ]
                        })
                    ]
                })
            ]
        })
    }));
};
/* harmony default export */ const SectionLangkah_SectionLangkah = (SectionLangkah);

// EXTERNAL MODULE: ./src/components/forMentor/Sections/SectionMentorBekerja/SectionMentorBekerja.module.scss
var SectionMentorBekerja_module = __webpack_require__(1136);
var SectionMentorBekerja_module_default = /*#__PURE__*/__webpack_require__.n(SectionMentorBekerja_module);
;// CONCATENATED MODULE: ./src/components/forMentor/Sections/SectionMentorBekerja/SectionMentorBekerja.js


const SectionMentorBekerja = ()=>{
    return(/*#__PURE__*/ jsx_runtime_.jsx("section", {
        id: "mentorBekerja",
        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
            className: "container p-4 p-lg-5 text-muted",
            children: [
                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                    className: "text-center",
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx("h3", {
                            className: "mt-5 mb-3",
                            children: "Bagaimana Mentor Bekerja ?"
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx("p", {
                            children: "Tentukan konten yang akan kamu publish berdasarkan dibawah ini"
                        })
                    ]
                }),
                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                    className: "row mt-5 d-none d-md-flex",
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: "col-12 p-0 col-lg-3 d-flex align-items-start",
                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                className: "nav flex-column nav-pills shadow w-100 sticky-top",
                                style: {
                                    top: '130px'
                                },
                                id: "v-pills-tab",
                                role: "tablist",
                                "aria-orientation": "vertical",
                                children: [
                                    /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                        className: "nav-link text-start active",
                                        id: "v-pills-alur-kerjasama-tab",
                                        "data-bs-toggle": "pill",
                                        "data-bs-target": "#v-pills-alur-kerjasama",
                                        type: "button",
                                        role: "tab",
                                        "aria-controls": "v-pills-alur-kerjasama",
                                        "aria-selected": "false",
                                        children: "Alur Kerjasama"
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                        className: "nav-link text-start",
                                        id: "v-pills-kategori-konten-tab",
                                        "data-bs-toggle": "pill",
                                        "data-bs-target": "#v-pills-kategori-konten",
                                        type: "button",
                                        role: "tab",
                                        "aria-controls": "v-pills-kategori-konten",
                                        "aria-selected": "false",
                                        children: "Kategori Konten"
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                        className: "nav-link text-start",
                                        id: "v-pills-model-bisnis-tab",
                                        "data-bs-toggle": "pill",
                                        "data-bs-target": "#v-pills-model-bisnis",
                                        type: "button",
                                        role: "tab",
                                        "aria-controls": "v-pills-model-bisnis",
                                        "aria-selected": "false",
                                        children: "Model Bisnis"
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                        className: "nav-link text-start",
                                        id: "v-pills-kewajiban-partner-tab",
                                        "data-bs-toggle": "pill",
                                        "data-bs-target": "#v-pills-kewajiban-partner",
                                        type: "button",
                                        role: "tab",
                                        "aria-controls": "v-pills-kewajiban-partner",
                                        "aria-selected": "true",
                                        children: "Kewajiban Partner"
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                        className: "nav-link text-start",
                                        id: "v-pills-kewajiban-codepolitan-tab",
                                        "data-bs-toggle": "pill",
                                        "data-bs-target": "#v-pills-kewajiban-codepolitan",
                                        type: "button",
                                        role: "tab",
                                        "aria-controls": "v-pills-kewajiban-codepolitan",
                                        "aria-selected": "false",
                                        children: "Kewajiban Codepolitan"
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                        className: "nav-link text-start",
                                        id: "v-pills-profit-sharing-tab",
                                        "data-bs-toggle": "pill",
                                        "data-bs-target": "#v-pills-profit-sharing",
                                        type: "button",
                                        role: "tab",
                                        "aria-controls": "v-pills-profit-sharing",
                                        "aria-selected": "false",
                                        children: "Profit Sharing"
                                    })
                                ]
                            })
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: "col-12 col-lg-8 px-0",
                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                className: "tab-content ps-3 mt-5 mt-md-0",
                                id: "v-pills-tabContent",
                                children: [
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: "tab-pane fade",
                                        id: "v-pills-kewajiban-partner",
                                        role: "tabpanel",
                                        "aria-labelledby": "v-pills-kewajiban-partner-tab",
                                        children: [
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: "row align-items-center mb-4",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                        className: "col-lg-2 text-center",
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                            src: "/assets/img/formentor/vector/Group 2256.svg",
                                                            width: 120,
                                                            alt: "Image Menyusun Silabus"
                                                        })
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                        className: "col-lg-10",
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                className: "fw-bold mb-2 text-center text-md-start",
                                                                children: "Menyusun Silabus"
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                                children: "Kamu dapat memulainya dari menyusun silabus materi yang akan kamu buat, supaya calon siswa memiliki gambaran terkait apa yang akan dipelajari di video konten yang kamu buat. "
                                                            })
                                                        ]
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: "row align-items-center mb-4 ",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                        className: "col-lg-2 text-center",
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                            src: "/assets/img/formentor/vector/Group 2257.svg",
                                                            width: 120,
                                                            alt: "Image Membuat Materi Belajar"
                                                        })
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                        className: "col-lg-10",
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                className: "fw-bold text-center text-md-start mb-2",
                                                                children: "Membuat Materi Belajar"
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                                children: "Pilih materi belajar sesuai dengan apa yang kamu kuasai, buatlah sesuai semenarik mungkin supaya calon pembeli memberikan feedback positif. Karena feedback siswa dapat mempengaruhi penghasilan kamu nantinya."
                                                            })
                                                        ]
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: "row align-items-center mb-4 ",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                        className: "col-lg-2 text-center",
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                            src: "/assets/img/formentor/vector/Group 2258.svg",
                                                            width: 120,
                                                            alt: "Image Memperbarui Materi Belajar"
                                                        })
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                        className: "col-lg-10",
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                className: "fw-bold text-center text-md-start mb-2",
                                                                children: "Memperbarui Materi Belajar"
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                                children: "Perbarui materi belajar yang sudah kamu buat, supaya siswa lebih tertarik untuk membeli kelas yang kamu buat karena mendapatkan banyak benefit dari teknologi terbaru."
                                                            })
                                                        ]
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: "row align-items-center",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                        className: "col-lg-2 text-center",
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                            src: "/assets/img/formentor/vector/Group 2259.svg",
                                                            width: 120,
                                                            alt: "Image Maintance Forum Tanya Jawab"
                                                        })
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                        className: "col-lg-10",
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                className: "fw-bold text-center text-md-start mb-2",
                                                                children: "Maintenance Forum Tanya Jawab"
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                                children: "Disini kamu dapat aktif memantau perkembangan siswa kelas yang kamu buat, semakin ramai kelas yang kamu buat semakin besar juga peluang penjualan kelas yang kamu buat."
                                                            })
                                                        ]
                                                    })
                                                ]
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: "tab-pane fade mt-5 mt-md-0",
                                        id: "v-pills-kategori-konten",
                                        role: "tabpanel",
                                        "aria-labelledby": "v-pills-kategori-konten-tab",
                                        children: [
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: "row align-items-center mb-4",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                        className: "col-lg-2 text-center",
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                            src: "/assets/img/formentor/vector/Group 2267.svg",
                                                            width: 120,
                                                            alt: "Image Programming"
                                                        })
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                        className: "col-lg-10",
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                className: "fw-bold text-center text-md-start mb-2",
                                                                children: "Programming"
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                                children: "Kategori ini kamu dapat membuat konten tentang frontend, backend, android developer hingga fullstack developer. Pilih bahasa pemrograman yang sesuai dengan keahlian kamu."
                                                            })
                                                        ]
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: "row align-items-center mb-4 ",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                        className: "col-lg-2 text-center",
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                            src: "/assets/img/formentor/vector/Group 2361.svg",
                                                            width: 120,
                                                            alt: "Image Product Management"
                                                        })
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                        className: "col-lg-10",
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                className: "fw-bold text-center text-md-start mb-2",
                                                                children: "Product Management"
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                                children: "Kamu dapat membuat konten tentang UI/UX, baik hanya UI Design, UI Interaction, UI Animation, UX Research, UX Design, UI Writter hingga bagaimana cara memanagement produk baru yang akan dibuat."
                                                            })
                                                        ]
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: "row align-items-center mb-4 ",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                        className: "col-lg-2 text-center",
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                            src: "/assets/img/formentor/vector/Group 2362.svg",
                                                            width: 120,
                                                            alt: "Image Cloud Computing"
                                                        })
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                        className: "col-lg-10",
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                className: "fw-bold text-center text-md-start mb-2",
                                                                children: "Cloud Computing"
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                                children: "Pilih materi belajar sesuai dengan apa yang kamu kuasai, buatlah sesuai semenarik mungkin supaya calon pembeli memberikan feedback positif. karena feedback siswa dapat mempengaruhi penghasilan kamu nantinya"
                                                            })
                                                        ]
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: "row align-items-center",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                        className: "col-lg-2 text-center",
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                            src: "/assets/img/formentor/vector/Group 2270.svg",
                                                            width: 120,
                                                            alt: "Image Cyber Security"
                                                        })
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                        className: "col-lg-10",
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                className: "fw-bold text-center text-md-start mb-2",
                                                                children: "Cyber Security"
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                                children: "Pilih materi belajar sesuai dengan apa yang kamu kuasai, buatlah sesuai semenarik mungkin supaya calon pembeli memberikan feedback positif. karena feedback siswa dapat mempengaruhi penghasilan kamu nantinya."
                                                            })
                                                        ]
                                                    })
                                                ]
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                        className: "tab-pane fade",
                                        id: "v-pills-model-bisnis",
                                        role: "tabpanel",
                                        "aria-labelledby": "v-pills-model-bisnis-tab",
                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                            className: `ms-2 ${(SectionMentorBekerja_module_default()).wrap} mt-5`,
                                            children: [
                                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                    className: "text-center",
                                                    children: "Sewa Kelas Satuan"
                                                }),
                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: "position-relative d-flex justify-content-center mb-n3",
                                                    children: [
                                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                            className: `position-absolute d-flex align-items-center ${(SectionMentorBekerja_module_default())["vector-position"]}`,
                                                            children: [
                                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                                    className: "d-flex flex-column ms-3",
                                                                    children: [
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                                            children: "Sewa Semua"
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                                            children: "Kelas Mentor"
                                                                        })
                                                                    ]
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                                    src: "/assets/img/formentor/vector/Group 2347.svg",
                                                                    className: `${(SectionMentorBekerja_module_default())["vector-responsive"]}`,
                                                                    alt: "Image Horizontal"
                                                                }),
                                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                                    className: "d-flex flex-column",
                                                                    children: [
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                                            children: "Sewa Roadmap"
                                                                        }),
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                                            children: "Belajar"
                                                                        })
                                                                    ]
                                                                })
                                                            ]
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                            className: "position-relative",
                                                            children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                                src: "/assets/img/formentor/vector/Group 2348.svg",
                                                                className: `${(SectionMentorBekerja_module_default())["vector-responsive"]}`,
                                                                alt: "Image Vertical"
                                                            })
                                                        })
                                                    ]
                                                }),
                                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                    className: "text-center",
                                                    children: "Beli Kelas"
                                                })
                                            ]
                                        })
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: "tab-pane fade mt-5 mt-md-0 px-0 px-md-4",
                                        id: "v-pills-profit-sharing",
                                        role: "tabpanel",
                                        "aria-labelledby": "v-pills-profit-sharing-tab",
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("h4", {
                                                children: "Simulasi Profit Sharing"
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: "row mt-5 text-center",
                                                children: [
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                        className: "col-lg-6 mb-4 mb-md-0",
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                                src: "/assets/img/formentor/vector/Group 2364.svg",
                                                                className: `img-fluid ${(SectionMentorBekerja_module_default())["vector-profit"]}`,
                                                                alt: "Image Regular Partner"
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                                className: "lead mt-n3",
                                                                children: "Regular Partner"
                                                            })
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                        className: "col-lg-6",
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                                src: "/assets/img/formentor/vector/Group 2349.svg",
                                                                className: `img-fluid ${(SectionMentorBekerja_module_default())["vector-profit"]}`,
                                                                alt: "Image Exclusive Partner"
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                                className: "lead mt-n3",
                                                                children: "Exclusive Partner"
                                                            })
                                                        ]
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("hr", {}),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("p", {
                                                children: [
                                                    "Regular Partner adalah partner CodePolitan yang memiliki platform penjualan sendiri atau menjual konten kelas di platform lain di luar CodePolitan. ",
                                                    /*#__PURE__*/ jsx_runtime_.jsx("br", {}),
                                                    " Exclusive Partner adalah partner CodePolitan yang membuat konten kelas dan hanya mensubmit kelasnya di platform CodePolitan."
                                                ]
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: "mt-4",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                        className: "fw-bold",
                                                        children: "Harga Jual Kelas : Rp. 300.000"
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                        className: "fw-bold mt-4 mb-0",
                                                        children: "Penjualan 3 kelas per hari dalam 1 bulan :"
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("p", {
                                                        children: [
                                                            "@ Rp. 300.000 x 3 kelas x 30 hari = ",
                                                            /*#__PURE__*/ jsx_runtime_.jsx("strong", {
                                                                children: "Rp. 27.000.000"
                                                            })
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                        className: "mt-4 fw-bold mb-n1",
                                                        children: "Bagi Hasil Skema 50 : 50"
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("p", {
                                                        children: [
                                                            "Rp. 27.000.000 / 2 = ",
                                                            /*#__PURE__*/ jsx_runtime_.jsx("strong", {
                                                                children: "Rp. 13.500.000"
                                                            })
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                        className: "mt-4 fw-bold text-primary fst-italic",
                                                        children: "Partner dapat membuat lebih dari 1 kelas untuk melipatgandakan keuntungan"
                                                    })
                                                ]
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: "tab-pane fade mt-5 mt-md-0 container-lg",
                                        id: "v-pills-kewajiban-codepolitan",
                                        role: "tabpanel",
                                        "aria-labelledby": "v-pills-kewajiban-codepolitan-tab",
                                        children: [
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: "row align-items-center mb-4",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                        className: "col-lg-2 text-center",
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                            src: "/assets/img/formentor/vector/Group 2292.svg",
                                                            width: 100,
                                                            alt: "Image Mempersiapkan Platform Belajar"
                                                        })
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                        className: "col-lg-10",
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                className: "fw-bold text-center text-md-start mb-2",
                                                                children: "Mempersiapkan Platform Belajar"
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                                children: "Tim Development CodePolitan akan memastikan platform siap untuk kebutuhan kelas mentor"
                                                            })
                                                        ]
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: "row align-items-center mb-4",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                        className: "col-lg-2 text-center order-first order-md-last",
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                            src: "/assets/img/formentor/vector/Group 2297.svg",
                                                            width: 100,
                                                            alt: "Image Mengatur Publikasi Kelas"
                                                        })
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                        className: "col-lg-10",
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                className: "fw-bold text-center text-md-start mb-2",
                                                                children: "Mengatur Publikasi Kelas"
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                                children: "Tim Kurikulum CodePolitan akan membantu mentor dalam menyusun dan mereview silabus"
                                                            })
                                                        ]
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: "row align-items-center mb-4",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                        className: "col-lg-2 text-center",
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                            src: "/assets/img/formentor/vector/Group 2298.svg",
                                                            width: 100,
                                                            alt: "Image Mempromosikan Kelas"
                                                        })
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                        className: "col-lg-10",
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                className: "fw-bold text-center text-md-start mb-2",
                                                                children: "Mempromosikan Kelas"
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                                children: "Tim Marketing CodePolitan akan membantu mengalirkan traffic dan calon pembeli ke kelas mentor"
                                                            })
                                                        ]
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: "row align-items-center mb-4",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                        className: "col-lg-2 text-center order-first order-md-last",
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                            src: "/assets/img/formentor/vector/Group 2299.svg",
                                                            width: 100,
                                                            alt: "Image Laporan Keuangan"
                                                        })
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                        className: "col-lg-10",
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                className: "fw-bold text-center text-md-start mb-2",
                                                                children: "Memberikan Laporan Keuangan Penjualan Kelas"
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                                children: "Profit Sharing dari penjualan kelas antara CodePolitan dengan mentor akan diberikan oleh tim CodePolitan"
                                                            })
                                                        ]
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: "row align-items-center mb-4",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                        className: "col-lg-2 text-center",
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                            src: "/assets/img/formentor/vector/Group 2300.svg",
                                                            width: 100,
                                                            alt: "Image Transfer Bagi Hasil"
                                                        })
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                        className: "col-lg-10",
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                className: "fw-bold text-center text-md-start mb-2",
                                                                children: "Transfer Bagi Hasil Kelas"
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                                children: "Bagi hasil akan ditransfer langsung ke rekening mentor setiap awal bulan, paling lambat setiap tanggal 5 pada hari kerja."
                                                            })
                                                        ]
                                                    })
                                                ]
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                        className: "tab-pane fade show active",
                                        id: "v-pills-alur-kerjasama",
                                        role: "tabpanel",
                                        "aria-labelledby": "v-pills-alur-kerjasama-tab",
                                        children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                            className: "row justify-content-center",
                                            children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                className: "col-lg-10",
                                                children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                    src: "/assets/img/formentor/alur-kerja-sama.png",
                                                    className: "w-100 ms-0 ms-md-5",
                                                    alt: "Image Alur Kerjasama"
                                                })
                                            })
                                        })
                                    })
                                ]
                            })
                        })
                    ]
                }),
                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                    className: "mt-5 d-block d-md-none",
                    children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                        className: "accordion accordion-flush",
                        id: "accordion-mentor",
                        children: [
                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                className: "accordion-item",
                                children: [
                                    /*#__PURE__*/ jsx_runtime_.jsx("h2", {
                                        className: "accordion-header",
                                        id: "flush-alur-kerjasama",
                                        children: /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                            className: "accordion-button collapsed",
                                            type: "button",
                                            "data-bs-toggle": "collapse",
                                            "data-bs-target": "#flush-collapse-alur-kerjasama",
                                            "aria-expanded": "false",
                                            "aria-controls": "flush-collapse-alur-kerjasama",
                                            children: "Alur Kerjasama"
                                        })
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                        id: "flush-collapse-alur-kerjasama",
                                        className: "accordion-collapse collapse show",
                                        "aria-labelledby": "flush-alur-kerjasama",
                                        "data-bs-parent": "#accordion-mentor",
                                        children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                            className: "accordion-body",
                                            children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                className: "row justify-content-center",
                                                children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                    className: "col-lg-10",
                                                    children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                        src: "/assets/img/formentor/vector/Group 2365.svg",
                                                        className: "w-100 ms-3",
                                                        alt: "Image Alur Kerjasama"
                                                    })
                                                })
                                            })
                                        })
                                    })
                                ]
                            }),
                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                className: "accordion-item",
                                children: [
                                    /*#__PURE__*/ jsx_runtime_.jsx("h2", {
                                        className: "accordion-header",
                                        id: "flush-kategori-konten",
                                        children: /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                            className: "accordion-button collapsed",
                                            type: "button",
                                            "data-bs-toggle": "collapse",
                                            "data-bs-target": "#flush-collapse-kategori-konten",
                                            "aria-expanded": "false",
                                            "aria-controls": "flush-collapse-kategori-konten",
                                            children: "Kategori Konten"
                                        })
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                        id: "flush-collapse-kategori-konten",
                                        className: "accordion-collapse collapse",
                                        "aria-labelledby": "flush-kategori-konten",
                                        "data-bs-parent": "#accordion-mentor",
                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                            className: "accordion-body",
                                            children: [
                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: "row align-items-center mb-4",
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                            className: "col-lg-2 text-center",
                                                            children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                                src: "/assets/img/formentor/vector/Group 2267.svg",
                                                                width: 120,
                                                                alt: "Image Programming"
                                                            })
                                                        }),
                                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                            className: "col-lg-10",
                                                            children: [
                                                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                    className: "fw-bold text-center text-md-start mb-2",
                                                                    children: "Programming"
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                                    children: "Kategori ini kamu dapat membuat konten tentang frontend, backend, android developer hingga fullstack developer. Pilih bahasa pemrograman yang sesuai dengan keahlian kamu."
                                                                })
                                                            ]
                                                        })
                                                    ]
                                                }),
                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: "row align-items-center mb-4 ",
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                            className: "col-lg-2 text-center",
                                                            children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                                src: "/assets/img/formentor/vector/Group 2361.svg",
                                                                width: 120,
                                                                alt: "Image Product Management"
                                                            })
                                                        }),
                                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                            className: "col-lg-10",
                                                            children: [
                                                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                    className: "fw-bold text-center text-md-start mb-2",
                                                                    children: "Product Management"
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                                    children: "Kamu dapat membuat konten tentang UI/UX, baik hanya UI Design, UI Interaction, UI Animation, UX Research, UX Design, UI Writter hingga bagaimana cara memanagement produk baru yang akan dibuat."
                                                                })
                                                            ]
                                                        })
                                                    ]
                                                }),
                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: "row align-items-center mb-4 ",
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                            className: "col-lg-2 text-center",
                                                            children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                                src: "/assets/img/formentor/vector/Group 2362.svg",
                                                                width: 120,
                                                                alt: "Image Cloud Computing"
                                                            })
                                                        }),
                                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                            className: "col-lg-10",
                                                            children: [
                                                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                    className: "fw-bold text-center text-md-start mb-2",
                                                                    children: "Cloud Computing"
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                                    children: "Pilih materi belajar sesuai dengan apa yang kamu kuasai, buatlah sesuai semenarik mungkin supaya calon pembeli memberikan feedback positif. karena feedback siswa dapat mempengaruhi penghasilan kamu nantinya"
                                                                })
                                                            ]
                                                        })
                                                    ]
                                                }),
                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: "row align-items-center",
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                            className: "col-lg-2 text-center",
                                                            children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                                src: "/assets/img/formentor/vector/Group 2270.svg",
                                                                width: 120,
                                                                alt: "Image Cyber Security"
                                                            })
                                                        }),
                                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                            className: "col-lg-10",
                                                            children: [
                                                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                    className: "fw-bold text-center text-md-start mb-2",
                                                                    children: "Cyber Security"
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                                    children: "Pilih materi belajar sesuai dengan apa yang kamu kuasai, buatlah sesuai semenarik mungkin supaya calon pembeli memberikan feedback positif. karena feedback siswa dapat mempengaruhi penghasilan kamu nantinya."
                                                                })
                                                            ]
                                                        })
                                                    ]
                                                })
                                            ]
                                        })
                                    })
                                ]
                            }),
                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                className: "accordion-item",
                                children: [
                                    /*#__PURE__*/ jsx_runtime_.jsx("h2", {
                                        className: "accordion-header",
                                        id: "flush-model-bisnis",
                                        children: /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                            className: "accordion-button collapsed",
                                            type: "button",
                                            "data-bs-toggle": "collapse",
                                            "data-bs-target": "#flush-collapse-model-bisnis",
                                            "aria-expanded": "false",
                                            "aria-controls": "flush-collapse-model-bisnis",
                                            children: "Model Bisnis"
                                        })
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                        id: "flush-collapse-model-bisnis",
                                        className: "accordion-collapse collapse",
                                        "aria-labelledby": "flush-model-bisnis",
                                        "data-bs-parent": "#accordion-mentor",
                                        children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                            className: "accordion-body ps-5 pb-0",
                                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: `${(SectionMentorBekerja_module_default()).wrap}`,
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                        className: "text-center",
                                                        children: "Sewa Kelas Satuan"
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                        className: "position-relative d-flex justify-content-center mb-n3",
                                                        children: [
                                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                                className: `position-absolute d-flex align-items-center ${(SectionMentorBekerja_module_default())["vector-position"]}`,
                                                                children: [
                                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                                        className: "d-flex flex-column ms-3",
                                                                        children: [
                                                                            /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                                                children: "Sewa Semua"
                                                                            }),
                                                                            /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                                                children: "Kelas Mentor"
                                                                            })
                                                                        ]
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                                        src: "/assets/img/formentor/vector/Group 2347.svg",
                                                                        className: `${(SectionMentorBekerja_module_default())["vector-responsive"]}`,
                                                                        alt: "Image Horizontal"
                                                                    }),
                                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                                        className: "d-flex flex-column",
                                                                        children: [
                                                                            /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                                                children: "Sewa Roadmap"
                                                                            }),
                                                                            /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                                                children: "Belajar"
                                                                            })
                                                                        ]
                                                                    })
                                                                ]
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                className: "position-relative",
                                                                children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                                    src: "/assets/img/formentor/vector/Group 2348.svg",
                                                                    className: `${(SectionMentorBekerja_module_default())["vector-responsive"]}`,
                                                                    alt: "Image Vertical"
                                                                })
                                                            })
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                        className: "text-center",
                                                        children: "Beli Kelas"
                                                    })
                                                ]
                                            })
                                        })
                                    })
                                ]
                            }),
                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                className: "accordion-item",
                                children: [
                                    /*#__PURE__*/ jsx_runtime_.jsx("h2", {
                                        className: "accordion-header",
                                        id: "flush-kewajiban-partner",
                                        children: /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                            className: "accordion-button collapsed",
                                            type: "button",
                                            "data-bs-toggle": "collapse",
                                            "data-bs-target": "#flush-collapse-kewajiban-partner",
                                            "aria-expanded": "false",
                                            "aria-controls": "flush-collapse-kewajiban-partner",
                                            children: "Kewajiban Partner"
                                        })
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                        id: "flush-collapse-kewajiban-partner",
                                        className: "accordion-collapse collapse",
                                        "aria-labelledby": "flush-kewajiban-partner",
                                        "data-bs-parent": "#accordion-mentor",
                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                            className: "accordion-body",
                                            children: [
                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: "row align-items-center mb-4",
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                            className: "col-lg-2 text-center",
                                                            children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                                src: "/assets/img/formentor/vector/Group 2256.svg",
                                                                width: 120,
                                                                alt: "Image Menyusun Silabus"
                                                            })
                                                        }),
                                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                            className: "col-lg-10",
                                                            children: [
                                                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                    className: "fw-bold mb-2 text-center text-md-start",
                                                                    children: "Menyusun Silabus"
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                                    children: "Kamu dapat memulainya dari menyusun silabus materi yang akan kamu buat, supaya calon siswa memiliki gambaran terkait apa yang akan dipelajari di video konten yang kamu buat. "
                                                                })
                                                            ]
                                                        })
                                                    ]
                                                }),
                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: "row align-items-center mb-4 ",
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                            className: "col-lg-2 text-center",
                                                            children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                                src: "/assets/img/formentor/vector/Group 2257.svg",
                                                                width: 120,
                                                                alt: "Image Membuat Materi Belajar"
                                                            })
                                                        }),
                                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                            className: "col-lg-10",
                                                            children: [
                                                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                    className: "fw-bold text-center text-md-start mb-2",
                                                                    children: "Membuat Materi Belajar"
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                                    children: "Pilih materi belajar sesuai dengan apa yang kamu kuasai, buatlah sesuai semenarik mungkin supaya calon pembeli memberikan feedback positif. Karena feedback siswa dapat mempengaruhi penghasilan kamu nantinya."
                                                                })
                                                            ]
                                                        })
                                                    ]
                                                }),
                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: "row align-items-center mb-4 ",
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                            className: "col-lg-2 text-center",
                                                            children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                                src: "/assets/img/formentor/vector/Group 2258.svg",
                                                                width: 120,
                                                                alt: "Image Memperbarui Materi Belajar"
                                                            })
                                                        }),
                                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                            className: "col-lg-10",
                                                            children: [
                                                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                    className: "fw-bold text-center text-md-start mb-2",
                                                                    children: "Memperbarui Materi Belajar"
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                                    children: "Perbarui materi belajar yang sudah kamu buat, supaya siswa lebih tertarik untuk membeli kelas yang kamu buat karena mendapatkan banyak benefit dari teknologi terbaru."
                                                                })
                                                            ]
                                                        })
                                                    ]
                                                }),
                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: "row align-items-center",
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                            className: "col-lg-2 text-center",
                                                            children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                                src: "/assets/img/formentor/vector/Group 2259.svg",
                                                                width: 120,
                                                                alt: "Image Maintance Forum Tanya Jawab"
                                                            })
                                                        }),
                                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                            className: "col-lg-10",
                                                            children: [
                                                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                    className: "fw-bold text-center text-md-start mb-2",
                                                                    children: "Maintenance Forum Tanya Jawab"
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                                    children: "Disini kamu dapat aktif memantau perkembangan siswa kelas yang kamu buat, semakin ramai kelas yang kamu buat semakin besar juga peluang penjualan kelas yang kamu buat."
                                                                })
                                                            ]
                                                        })
                                                    ]
                                                })
                                            ]
                                        })
                                    })
                                ]
                            }),
                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                className: "accordion-item",
                                children: [
                                    /*#__PURE__*/ jsx_runtime_.jsx("h2", {
                                        className: "accordion-header",
                                        id: "flush-kewajiban-codepolitan",
                                        children: /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                            className: "accordion-button collapsed",
                                            type: "button",
                                            "data-bs-toggle": "collapse",
                                            "data-bs-target": "#flush-collapse-kewajiban-codepolitan",
                                            "aria-expanded": "false",
                                            "aria-controls": "flush-collapse-kewajiban-codepolitan",
                                            children: "Kewajiban CodePolitan"
                                        })
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                        id: "flush-collapse-kewajiban-codepolitan",
                                        className: "accordion-collapse collapse",
                                        "aria-labelledby": "flush-kewajiban-codepolitan",
                                        "data-bs-parent": "#accordion-mentor",
                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                            className: "accordion-body",
                                            children: [
                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: "row align-items-center mb-4",
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                            className: "col-lg-2 text-center",
                                                            children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                                src: "/assets/img/formentor/vector/Group 2292.svg",
                                                                width: 100,
                                                                alt: "Image Mempersiapkan Platform Belajar"
                                                            })
                                                        }),
                                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                            className: "col-lg-10",
                                                            children: [
                                                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                    className: "fw-bold text-center text-md-start mb-2",
                                                                    children: "Mempersiapkan Platform Belajar"
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                                    children: "Tim Development CodePolitan akan memastikan platform siap untuk kebutuhan kelas mentor"
                                                                })
                                                            ]
                                                        })
                                                    ]
                                                }),
                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: "row align-items-center mb-4",
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                            className: "col-lg-2 text-center order-first order-md-last",
                                                            children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                                src: "/assets/img/formentor/vector/Group 2297.svg",
                                                                width: 100,
                                                                alt: "Image Mengatur Publikasi Kelas"
                                                            })
                                                        }),
                                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                            className: "col-lg-10",
                                                            children: [
                                                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                    className: "fw-bold text-center text-md-start mb-2",
                                                                    children: "Mengatur Publikasi Kelas"
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                                    children: "Tim Kurikulum CodePolitan akan membantu mentor dalam menyusun dan mereview silabus"
                                                                })
                                                            ]
                                                        })
                                                    ]
                                                }),
                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: "row align-items-center mb-4",
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                            className: "col-lg-2 text-center",
                                                            children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                                src: "/assets/img/formentor/vector/Group 2298.svg",
                                                                width: 100,
                                                                alt: "Image Mempromosikan Kelas"
                                                            })
                                                        }),
                                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                            className: "col-lg-10",
                                                            children: [
                                                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                    className: "fw-bold text-center text-md-start mb-2",
                                                                    children: "Mempromosikan Kelas"
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                                    children: "Tim Marketing CodePolitan akan membantu mengalirkan traffic dan calon pembeli ke kelas mentor"
                                                                })
                                                            ]
                                                        })
                                                    ]
                                                }),
                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: "row align-items-center mb-4",
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                            className: "col-lg-2 text-center order-first order-md-last",
                                                            children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                                src: "/assets/img/formentor/vector/Group 2299.svg",
                                                                width: 100,
                                                                alt: "Image Laporan Keuangan"
                                                            })
                                                        }),
                                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                            className: "col-lg-10",
                                                            children: [
                                                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                    className: "fw-bold text-center text-md-start mb-2",
                                                                    children: "Memberikan Laporan Keuangan Penjualan Kelas"
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                                    children: "Profit Sharing dari penjualan kelas antara CodePolitan dengan mentor akan diberikan oleh tim CodePolitan"
                                                                })
                                                            ]
                                                        })
                                                    ]
                                                }),
                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: "row align-items-center mb-4",
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                            className: "col-lg-2 text-center",
                                                            children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                                src: "/assets/img/formentor/vector/Group 2300.svg",
                                                                width: 100,
                                                                alt: "Image Transfer Bagi Hasil"
                                                            })
                                                        }),
                                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                            className: "col-lg-10",
                                                            children: [
                                                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                                    className: "fw-bold text-center text-md-start mb-2",
                                                                    children: "Transfer Bagi Hasil Kelas"
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                                    children: "Bagi hasil akan ditransfer langsung ke rekening mentor setiap awal bulan, paling lambat setiap tanggal 5 pada hari kerja."
                                                                })
                                                            ]
                                                        })
                                                    ]
                                                })
                                            ]
                                        })
                                    })
                                ]
                            }),
                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                className: "accordion-item",
                                children: [
                                    /*#__PURE__*/ jsx_runtime_.jsx("h2", {
                                        className: "accordion-header",
                                        id: "flush-profit-sharing",
                                        children: /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                            className: "accordion-button collapsed",
                                            type: "button",
                                            "data-bs-toggle": "collapse",
                                            "data-bs-target": "#flush-collapse-profit-sharing",
                                            "aria-expanded": "false",
                                            "aria-controls": "flush-collapse-profit-sharing",
                                            children: "Profit Sharing"
                                        })
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                        id: "flush-collapse-profit-sharing",
                                        className: "accordion-collapse collapse",
                                        "aria-labelledby": "flush-profit-sharing",
                                        "data-bs-parent": "#accordion-mentor",
                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                            className: "accordion-body",
                                            children: [
                                                /*#__PURE__*/ jsx_runtime_.jsx("h4", {
                                                    className: "text-center",
                                                    children: "Simulasi Profit Sharing"
                                                }),
                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: "row mt-5 text-center",
                                                    children: [
                                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                            className: "col-lg-6 mb-4 mb-md-0",
                                                            children: [
                                                                /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                                    src: "/assets/img/formentor/vector/Group 2364.svg",
                                                                    className: `img-fluid ${(SectionMentorBekerja_module_default())["vector-profit"]}`,
                                                                    alt: "Image Regular Partner"
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                                    className: "lead mt-n3",
                                                                    children: "Regular Partner"
                                                                })
                                                            ]
                                                        }),
                                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                            className: "col-lg-6",
                                                            children: [
                                                                /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                                    src: "/assets/img/formentor/vector/Group 2349.svg",
                                                                    className: `img-fluid ${(SectionMentorBekerja_module_default())["vector-profit"]}`,
                                                                    alt: "Image Exclusive Partner"
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                                    className: "lead mt-n3",
                                                                    children: "Exclusive Partner"
                                                                })
                                                            ]
                                                        })
                                                    ]
                                                }),
                                                /*#__PURE__*/ jsx_runtime_.jsx("hr", {}),
                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("p", {
                                                    children: [
                                                        "Regular Partner adalah partner CodePolitan yang memiliki platform penjualan sendiri atau menjual konten kelas di platform lain di luar CodePolitan. ",
                                                        /*#__PURE__*/ jsx_runtime_.jsx("br", {}),
                                                        " Exclusive Partner adalah partner CodePolitan yang membuat konten kelas dan hanya mensubmit kelasnya di platform CodePolitan."
                                                    ]
                                                }),
                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: "mt-4",
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                            className: "fw-bold",
                                                            children: "Harga Jual Kelas : Rp. 300.000"
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                            className: "fw-bold mt-4 mb-0",
                                                            children: "Penjualan 3 kelas per hari dalam 1 bulan :"
                                                        }),
                                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("p", {
                                                            children: [
                                                                "@ Rp. 300.000 x 3 kelas x 30 hari = ",
                                                                /*#__PURE__*/ jsx_runtime_.jsx("strong", {
                                                                    children: "Rp. 27.000.000"
                                                                })
                                                            ]
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                            className: "mt-4 fw-bold mb-n1",
                                                            children: "Bagi Hasil Skema 50 : 50"
                                                        }),
                                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("p", {
                                                            children: [
                                                                "Rp. 27.000.000 / 2 = ",
                                                                /*#__PURE__*/ jsx_runtime_.jsx("strong", {
                                                                    children: "Rp. 13.500.000"
                                                                })
                                                            ]
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                            className: "mt-4 fw-bold text-primary fst-italic",
                                                            children: "Partner bisa memilih lebih dari 1 kelas untuk melipatgandakan keuntungan"
                                                        })
                                                    ]
                                                })
                                            ]
                                        })
                                    })
                                ]
                            })
                        ]
                    })
                })
            ]
        })
    }));
};
/* harmony default export */ const SectionMentorBekerja_SectionMentorBekerja = (SectionMentorBekerja);

// EXTERNAL MODULE: external "mixpanel-browser"
var external_mixpanel_browser_ = __webpack_require__(5804);
var external_mixpanel_browser_default = /*#__PURE__*/__webpack_require__.n(external_mixpanel_browser_);
;// CONCATENATED MODULE: ./src/components/forMentor/Sections/SectionInvitation/SectionInvitation.js


const SectionInvitation = ()=>{
    external_mixpanel_browser_default().init('608746391f30c018d759b8c2c1ecb097', {
        debug: false
    });
    return(/*#__PURE__*/ jsx_runtime_.jsx("section", {
        className: "bg-codepolitan",
        children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
            className: "container p-4 p-lg-5",
            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                className: "text-center text-white",
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx("h2", {
                        className: "mb-4",
                        children: "Mari menjadi bagian dalam membangun ekosistem yang nyaman untuk belajar pemrograman dan teknologi di Indonesia"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("a", {
                        onClick: ()=>external_mixpanel_browser_default().track('Daftar mentor', {
                                'source': 'Landing'
                            })
                        ,
                        href: "https://airtable.com/shr7AyfCEMNuY8Hzc",
                        target: "_blank",
                        className: "btn btn-pink btn-rounded btn-lg",
                        rel: "noreferrer",
                        children: "Daftar Sebagai Mentor"
                    })
                ]
            })
        })
    }));
};
/* harmony default export */ const SectionInvitation_SectionInvitation = (SectionInvitation);

// EXTERNAL MODULE: external "axios"
var external_axios_ = __webpack_require__(2167);
var external_axios_default = /*#__PURE__*/__webpack_require__.n(external_axios_);
// EXTERNAL MODULE: ./src/components/global/Sections/SectionCounter/SectionCounter.js
var SectionCounter = __webpack_require__(5919);
;// CONCATENATED MODULE: ./src/pages/for-mentor/index.js













const getServerSideProps = async ()=>{
    const statsRes = await external_axios_default().get(`${"https://api.codepolitan.com"}/v1/stats`);
    const stats = statsRes.data;
    return {
        props: {
            stats
        }
    };
};
const ForMentor = ({ stats  })=>{
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
        children: [
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)((head_default()), {
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx("title", {
                        children: "Codepolitan for Mentor - Codepolitan"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        name: "title",
                        content: "Website Belajar Coding Bahasa Indonesia - Codepolitan"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        name: "description",
                        content: "Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "og:type",
                        content: "website"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "og:url",
                        content: "https://codepolitan.com"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "og:title",
                        content: "Website Belajar Coding Bahasa Indonesia - Codepolitan"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "og:description",
                        content: "Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "og:image",
                        content: "https://i.ibb.co/8NQZp16/og-image-for-mentor.webp"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "twitter:card",
                        content: "summary_large_image"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "twitter:url",
                        content: "https://codepolitan.com"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "twitter:title",
                        content: "Website Belajar Coding Bahasa Indonesia - Codepolitan"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "twitter:description",
                        content: "Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "twitter:image",
                        content: "https://i.ibb.co/8NQZp16/og-image-for-mentor.webp"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("script", {
                        src: "https://cdn.onesignal.com/sdks/OneSignalSDK.js",
                        async: ""
                    })
                ]
            }),
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)(Layout/* default */.Z, {
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx(Hero_Hero, {}),
                    /*#__PURE__*/ jsx_runtime_.jsx(SectionPeringkatPenghasilan_SectionPeringkatPenghasilan, {}),
                    /*#__PURE__*/ jsx_runtime_.jsx(SectionKeuntungan_SectionKeuntungan, {}),
                    /*#__PURE__*/ jsx_runtime_.jsx(SectionCounter/* default */.Z, {
                        data: stats
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx(SectionCodepolitanSelf_SectionCodepolitanSelf, {}),
                    /*#__PURE__*/ jsx_runtime_.jsx(SectionQuotes_SectionQuotes, {}),
                    /*#__PURE__*/ jsx_runtime_.jsx(SectionLangkah_SectionLangkah, {}),
                    /*#__PURE__*/ jsx_runtime_.jsx(SectionMentorBekerja_SectionMentorBekerja, {}),
                    /*#__PURE__*/ jsx_runtime_.jsx(SectionInvitation_SectionInvitation, {})
                ]
            })
        ]
    }));
};
/* harmony default export */ const for_mentor = (ForMentor);


/***/ }),

/***/ 5368:
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/free-brands-svg-icons");

/***/ }),

/***/ 197:
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/free-regular-svg-icons");

/***/ }),

/***/ 6466:
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/free-solid-svg-icons");

/***/ }),

/***/ 7197:
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/react-fontawesome");

/***/ }),

/***/ 2167:
/***/ ((module) => {

"use strict";
module.exports = require("axios");

/***/ }),

/***/ 2733:
/***/ ((module) => {

"use strict";
module.exports = require("fuse.js");

/***/ }),

/***/ 5804:
/***/ ((module) => {

"use strict";
module.exports = require("mixpanel-browser");

/***/ }),

/***/ 2245:
/***/ ((module) => {

"use strict";
module.exports = require("moment");

/***/ }),

/***/ 5518:
/***/ ((module) => {

"use strict";
module.exports = require("moment/locale/id");

/***/ }),

/***/ 562:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/server/denormalize-page-path.js");

/***/ }),

/***/ 4014:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/i18n/normalize-locale-path.js");

/***/ }),

/***/ 8524:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/is-plain-object.js");

/***/ }),

/***/ 8020:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/mitt.js");

/***/ }),

/***/ 4964:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router-context.js");

/***/ }),

/***/ 9565:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/get-asset-path-from-route.js");

/***/ }),

/***/ 4365:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/get-middleware-regex.js");

/***/ }),

/***/ 1428:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/is-dynamic.js");

/***/ }),

/***/ 1292:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/parse-relative-url.js");

/***/ }),

/***/ 979:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/querystring.js");

/***/ }),

/***/ 6052:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/resolve-rewrites.js");

/***/ }),

/***/ 4226:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/route-matcher.js");

/***/ }),

/***/ 5052:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/route-regex.js");

/***/ }),

/***/ 9232:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/utils.js");

/***/ }),

/***/ 968:
/***/ ((module) => {

"use strict";
module.exports = require("next/head");

/***/ }),

/***/ 1853:
/***/ ((module) => {

"use strict";
module.exports = require("next/router");

/***/ }),

/***/ 6689:
/***/ ((module) => {

"use strict";
module.exports = require("react");

/***/ }),

/***/ 609:
/***/ ((module) => {

"use strict";
module.exports = require("react-countup");

/***/ }),

/***/ 997:
/***/ ((module) => {

"use strict";
module.exports = require("react/jsx-runtime");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [7730,9285,1664,4528,4986,5919], () => (__webpack_exec__(2190)));
module.exports = __webpack_exports__;

})();