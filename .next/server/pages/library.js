"use strict";
(() => {
var exports = {};
exports.id = 5175;
exports.ids = [5175];
exports.modules = {

/***/ 6105:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "getStaticProps": () => (/* binding */ getStaticProps),
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(6466);
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(7197);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(968);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _components_global_Layout_Layout__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(4528);
/* harmony import */ var _components_global_Cards_CardCourse_CardCourse__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(9722);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(1664);
/* harmony import */ var _components_global_Modal_Modal__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(2541);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(2167);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _components_skeletons_SkeletonCardCourse_SkeletonCardCourse__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(2927);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(1853);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_11__);












async function getStaticProps() {
    const response = await axios__WEBPACK_IMPORTED_MODULE_9___default().get(`${"https://api.codepolitan.com"}/course/labels`);
    const allCourse = {
        term: 'Semua Kelas',
        term_slug: '',
        thumbnail: ''
    };
    const labels = [
        allCourse
    ].concat(response.data);
    return {
        props: {
            labels
        }
    };
}
const Library = ({ labels  })=>{
    const { 0: courses , 1: setCourses  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)([]);
    const { 0: filter , 1: setFilter  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)('');
    const { 0: courseLabel , 1: setCourseLabel  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)('');
    const { 0: sort , 1: setSort  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)('created_at[desc]');
    const { 0: loading , 1: setLoading  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);
    const { 0: search , 1: setSearch  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)('');
    const { 0: searchValue , 1: setSearchValue  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)('');
    const { 0: limit , 1: setLimit  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(12);
    const { 0: limitMenu , 1: setLimitMenu  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(10);
    const video = "lVzBOGons6U?rel=0";
    const { 0: videoId , 1: setVideoId  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(video);
    const { query  } = (0,next_router__WEBPACK_IMPORTED_MODULE_11__.useRouter)();
    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{
        const getPopularCourses = async ()=>{
            setLoading(true);
            try {
                let response = await axios__WEBPACK_IMPORTED_MODULE_9___default().get(`${"https://api.codepolitan.com"}/course/popular`); // Ganti sama popular-weekly kalau APInya udah fix
                if (response.data.error === "No Content") {
                    setCourses(null);
                    setLoading(false);
                } else {
                    setLoading(false);
                    setCourses(response.data);
                // if (response.data.length < 5) {
                //     setPopularEndpoint('popular')
                // };
                }
            } catch (err) {
                setLoading(true);
                return err.message;
            }
        };
        const getCourses = async ()=>{
            setLoading(true);
            try {
                let response = await axios__WEBPACK_IMPORTED_MODULE_9___default().get(`${"https://api.codepolitan.com"}/course?page=1&limit=200&search=${search}&course_label=${courseLabel}&filter=${filter}&sort=${sort}`);
                if (response.data.error === "No Content") {
                    setCourses(null);
                    setLoading(false);
                } else {
                    setCourses(response.data);
                    setLoading(false);
                    query.type && setCourseLabel(query.type);
                    query.type && document.getElementById('courses').scrollIntoView({
                        behavior: 'smooth'
                    });
                    // Push first course id to local storage
                    if (sort === 'created_at[desc]' && filter === '' && courseLabel === '' && search === '') {
                        localStorage.setItem('latestCourseId', response.data[0].id);
                    }
                }
            } catch (err) {
                setLoading(true);
                return err.message;
            }
        };
        if (query.type == 'popular') {
            getPopularCourses();
            document.getElementById('courses').scrollIntoView({
                behavior: 'smooth'
            });
            return;
        } else {
            getCourses();
            document.getElementById('courses').scrollIntoView({
                behavior: 'smooth'
            });
        }
    }, [
        search,
        filter,
        sort,
        courseLabel,
        query.type
    ]);
    const submitSearch = (e)=>{
        e.preventDefault();
        setSearch('~' + searchValue + '~');
        setCourseLabel('');
        setFilter('');
        setSort('created_at[desc]');
    };
    const scrollToTopCourses = ()=>{
        setTimeout(()=>{
            document.getElementById('filterCourses').scrollIntoView();
        }, 300);
    };
    const courseLevel = [
        {
            term: 'Semua Level',
            label: ''
        },
        {
            term: 'Pemula',
            label: 'level[eq]beginner'
        },
        {
            term: 'Menengah',
            label: 'level[eq]intermediate'
        },
        {
            term: 'Mahir',
            label: 'level[eq]advance'
        }, 
    ];
    const courseSort = [
        {
            term: 'Kelas Terbaru',
            label: 'created_at[desc]'
        },
        {
            term: 'Kelas Terpopuler',
            label: 'popular[desc]'
        },
        {
            term: 'Harga Tertinggi',
            label: 'retail_price[desc]'
        },
        {
            term: 'Harga Terendah',
            label: 'retail_price[asc]'
        }, 
    ];
    const showCourses = [
        {
            term: 'Semua Kelas',
            label: ''
        },
        {
            term: 'Kelas Gratis',
            label: 'free'
        }, 
    ];
    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
        children: [
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)((next_head__WEBPACK_IMPORTED_MODULE_4___default()), {
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("title", {
                        children: "Library - Codepolitan"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        name: "title",
                        content: "Kelas Online Skill Digital - CodePolitan"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        name: "description",
                        content: "Akses lebih dari 170 kelas online di bidang pemrograman dan skill digital lainnya dari para expert"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:type",
                        content: "website"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:url",
                        content: "https://codepolitan.com"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:title",
                        content: "Kelas Online Skill Digital - CodePolitan"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:description",
                        content: "Akses lebih dari 170 kelas online di bidang pemrograman dan skill digital lainnya dari para expert"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:image",
                        content: "https://i.ibb.co/znsT2qH/OG-library.webp"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:card",
                        content: "summary_large_image"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:url",
                        content: "https://codepolitan.com"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:title",
                        content: "Kelas Online Skill Digital - CodePolitan"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:description",
                        content: "Akses lebih dari 170 kelas online di bidang pemrograman dan skill digital lainnya dari para expert"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:image",
                        content: "https://i.ibb.co/znsT2qH/OG-library.webp"
                    })
                ]
            }),
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_components_global_Layout_Layout__WEBPACK_IMPORTED_MODULE_5__/* ["default"] */ .Z, {
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("section", {
                        className: "section",
                        id: "courses",
                        style: {
                            marginTop: '70px'
                        },
                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                            className: "container p-4 p-lg-5",
                            children: [
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "row mb-4",
                                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        className: "col",
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
                                                className: "section-title h3",
                                                children: "Pilihan Kelas Online Pemrograman"
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                className: "text-muted",
                                                children: "Pilih dan jadilah professional!"
                                            })
                                        ]
                                    })
                                }),
                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                    className: "row",
                                    id: "filterCourses",
                                    children: [
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: "col-lg-3 d-none d-lg-block",
                                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                className: "sticky-top",
                                                style: {
                                                    top: '100px'
                                                },
                                                children: [
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h3", {
                                                        className: "section-title h5 mb-3",
                                                        children: "Filter Kelas"
                                                    }),
                                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("ul", {
                                                        className: "list-group",
                                                        children: [
                                                            labels.map((item, index)=>{
                                                                const handleClick = ()=>{
                                                                    setCourseLabel(item.term_slug);
                                                                    setFilter('');
                                                                    setSort('created_at[desc]');
                                                                    setSearch('');
                                                                    query.type = '';
                                                                    window.history.replaceState(null, '', '/library/');
                                                                };
                                                                return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                                                                    className: courseLabel === item.term_slug ? "list-group-item border-0 text-primary px-0" : "list-group-item border-0 text-muted px-0",
                                                                    role: "button",
                                                                    onClick: handleClick,
                                                                    style: {
                                                                        fontWeight: courseLabel === item.term_slug && 'bold'
                                                                    },
                                                                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                                        onClick: ()=>scrollToTopCourses()
                                                                        ,
                                                                        className: "d-flex align-items-center",
                                                                        children: [
                                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                                                src: item.thumbnail || '/assets/img/placeholder.jpg',
                                                                                width: 30,
                                                                                className: "img-fluid me-2",
                                                                                alt: item.term_slug
                                                                            }),
                                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                                                className: "mt-1",
                                                                                children: item.term
                                                                            })
                                                                        ]
                                                                    })
                                                                }, index));
                                                            }).slice(0, limitMenu),
                                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("li", {
                                                                className: "list-group-item border-0 text-primary px-0",
                                                                children: [
                                                                    labels.length > limitMenu && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                                        onClick: ()=>setLimitMenu(limitMenu + 10)
                                                                        ,
                                                                        className: "link",
                                                                        style: {
                                                                            cursor: 'pointer'
                                                                        },
                                                                        children: "Show more"
                                                                    }),
                                                                    labels.length < limitMenu && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                                        onClick: ()=>{
                                                                            setLimitMenu(10);
                                                                            scrollToTopCourses();
                                                                        },
                                                                        className: "link",
                                                                        style: {
                                                                            cursor: 'pointer'
                                                                        },
                                                                        children: "Reset"
                                                                    })
                                                                ]
                                                            })
                                                        ]
                                                    })
                                                ]
                                            })
                                        }),
                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                            className: "col col-lg-9",
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                    className: "sticky-top",
                                                    style: {
                                                        top: '70px'
                                                    },
                                                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                        className: "row mb-3 bg-white py-3",
                                                        children: [
                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                                className: "col-md-6 mb-3 mb-lg-0 my-auto",
                                                                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                                    className: "d-flex align-items-lg-start",
                                                                    children: [
                                                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                                            className: "dropdown d-lg-none",
                                                                            children: [
                                                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                                                                    className: "btn btn-outline-secondary btn-sm dropdown-toggle",
                                                                                    type: "button",
                                                                                    id: "dropdownMenuButton1",
                                                                                    "data-bs-toggle": "dropdown",
                                                                                    "aria-expanded": "false",
                                                                                    children: "Filter"
                                                                                }),
                                                                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("ul", {
                                                                                    className: "dropdown-menu overflow-scroll",
                                                                                    style: {
                                                                                        height: '200px!important'
                                                                                    },
                                                                                    "aria-labelledby": "dropdownMenuButton1",
                                                                                    children: [
                                                                                        labels.map((item, index)=>{
                                                                                            const handleClick = ()=>{
                                                                                                setCourseLabel(item.term_slug);
                                                                                                setFilter('');
                                                                                                setSort('created_at[desc]');
                                                                                                setSearch('');
                                                                                            };
                                                                                            return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("li", {
                                                                                                onClick: handleClick,
                                                                                                className: "dropdown-item",
                                                                                                role: "button",
                                                                                                children: [
                                                                                                    item.term,
                                                                                                    item.term_slug === courseLabel && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_3__.FontAwesomeIcon, {
                                                                                                        className: "text-primary ms-1",
                                                                                                        icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_2__.faCheckCircle
                                                                                                    })
                                                                                                ]
                                                                                            }, index));
                                                                                        }).slice(0, limitMenu),
                                                                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("li", {
                                                                                            className: "list-group-item border-0 text-primary px-0 text-center",
                                                                                            children: [
                                                                                                labels.length > limitMenu && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                                                                    onClick: ()=>setLimitMenu(limitMenu + 10)
                                                                                                    ,
                                                                                                    className: "link",
                                                                                                    style: {
                                                                                                        cursor: 'pointer'
                                                                                                    },
                                                                                                    children: "Show more"
                                                                                                }),
                                                                                                labels.length < limitMenu && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                                                                    onClick: ()=>setLimitMenu(10)
                                                                                                    ,
                                                                                                    className: "link",
                                                                                                    style: {
                                                                                                        cursor: 'pointer'
                                                                                                    },
                                                                                                    children: "Reset"
                                                                                                })
                                                                                            ]
                                                                                        })
                                                                                    ]
                                                                                })
                                                                            ]
                                                                        }),
                                                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                                            className: "dropdown ms-2 ms-lg-0",
                                                                            children: [
                                                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                                                                    className: "btn btn-outline-secondary btn-sm dropdown-toggle",
                                                                                    type: "button",
                                                                                    id: "dropdownMenuButton1",
                                                                                    "data-bs-toggle": "dropdown",
                                                                                    "aria-expanded": "false",
                                                                                    children: "Level"
                                                                                }),
                                                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("ul", {
                                                                                    className: "dropdown-menu",
                                                                                    "aria-labelledby": "dropdownMenuButton1",
                                                                                    children: courseLevel.map((item, index)=>{
                                                                                        return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("li", {
                                                                                            onClick: ()=>{
                                                                                                setFilter(item.label);
                                                                                                query.type = '';
                                                                                                window.history.replaceState(null, '', '/library/');
                                                                                            },
                                                                                            className: "dropdown-item",
                                                                                            role: "button",
                                                                                            children: [
                                                                                                item.term,
                                                                                                item.label === filter && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_3__.FontAwesomeIcon, {
                                                                                                    className: "text-primary ms-1",
                                                                                                    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_2__.faCheckCircle
                                                                                                })
                                                                                            ]
                                                                                        }, index));
                                                                                    })
                                                                                })
                                                                            ]
                                                                        }),
                                                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                                            className: "dropdown ms-2",
                                                                            children: [
                                                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                                                                    className: "btn btn-outline-secondary btn-sm dropdown-toggle",
                                                                                    type: "button",
                                                                                    id: "dropdownMenuButton1",
                                                                                    "data-bs-toggle": "dropdown",
                                                                                    "aria-expanded": "false",
                                                                                    children: "Urutkan"
                                                                                }),
                                                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("ul", {
                                                                                    className: "dropdown-menu",
                                                                                    "aria-labelledby": "dropdownMenuButton1",
                                                                                    children: courseSort.map((item, index)=>{
                                                                                        return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("li", {
                                                                                            onClick: ()=>{
                                                                                                setSort(item.label);
                                                                                                query.type = '';
                                                                                                window.history.replaceState(null, '', '/library/');
                                                                                            },
                                                                                            className: "dropdown-item",
                                                                                            role: "button",
                                                                                            children: [
                                                                                                item.term,
                                                                                                item.label === sort && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_3__.FontAwesomeIcon, {
                                                                                                    className: "text-primary ms-1",
                                                                                                    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_2__.faCheckCircle
                                                                                                })
                                                                                            ]
                                                                                        }, index));
                                                                                    })
                                                                                })
                                                                            ]
                                                                        }),
                                                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                                            className: "dropdown ms-2",
                                                                            children: [
                                                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                                                                    className: "btn btn-outline-secondary btn-sm dropdown-toggle",
                                                                                    type: "button",
                                                                                    id: "dropdownMenuButton1",
                                                                                    "data-bs-toggle": "dropdown",
                                                                                    "aria-expanded": "false",
                                                                                    children: "Tampilkan"
                                                                                }),
                                                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("ul", {
                                                                                    className: "dropdown-menu",
                                                                                    "aria-labelledby": "dropdownMenuButton1",
                                                                                    children: showCourses.map((item, index)=>{
                                                                                        return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("li", {
                                                                                            onClick: ()=>{
                                                                                                setCourseLabel(item.label);
                                                                                                query.type = '';
                                                                                                window.history.replaceState(null, '', '/library/');
                                                                                            },
                                                                                            className: "dropdown-item",
                                                                                            role: "button",
                                                                                            children: [
                                                                                                item.term,
                                                                                                item.label === courseLabel && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_3__.FontAwesomeIcon, {
                                                                                                    className: "text-primary ms-1",
                                                                                                    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_2__.faCheckCircle
                                                                                                })
                                                                                            ]
                                                                                        }, index));
                                                                                    })
                                                                                })
                                                                            ]
                                                                        })
                                                                    ]
                                                                })
                                                            }),
                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                                className: "col-md-5 col-lg-4 ms-auto",
                                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("form", {
                                                                    onSubmit: submitSearch,
                                                                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                                        className: "input-group",
                                                                        children: [
                                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("input", {
                                                                                onChange: (e)=>setSearchValue(e.target.value)
                                                                                ,
                                                                                type: "search",
                                                                                className: "form-control shadow-none",
                                                                                placeholder: "cari kelas dan enter disini..."
                                                                            }),
                                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                                                                className: "input-group-text bg-white text-muted",
                                                                                role: "button",
                                                                                type: "submit",
                                                                                title: "Cari",
                                                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_3__.FontAwesomeIcon, {
                                                                                    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_2__.faSearch
                                                                                })
                                                                            })
                                                                        ]
                                                                    })
                                                                })
                                                            })
                                                        ]
                                                    })
                                                }),
                                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                    className: "row",
                                                    children: [
                                                        !loading && courses === null && /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                            className: "col text-center my-5",
                                                            children: [
                                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("object", {
                                                                    width: '35%',
                                                                    type: "image/svg+xml",
                                                                    data: "/assets/img/icon_404.svg",
                                                                    children: "Ups"
                                                                }),
                                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                                    className: "text-muted",
                                                                    children: "Ups, kelas tidak ditemukan, coba cari dengan kata kunci lain"
                                                                })
                                                            ]
                                                        }),
                                                        loading && [
                                                            1,
                                                            2,
                                                            3,
                                                            4,
                                                            5,
                                                            6
                                                        ].map((index)=>/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                                className: "col-md-6 col-xl-4 p-2",
                                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_skeletons_SkeletonCardCourse_SkeletonCardCourse__WEBPACK_IMPORTED_MODULE_10__/* ["default"] */ .Z, {})
                                                            }, index)
                                                        ),
                                                        !loading && courses !== null && courses?.map((course, index)=>{
                                                            return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                                className: "col-md-6 col-xl-4 px-2 py-3",
                                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_link__WEBPACK_IMPORTED_MODULE_7__["default"], {
                                                                    href: `/course/intro/${course.slug}`,
                                                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("a", {
                                                                        className: "link",
                                                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_global_Cards_CardCourse_CardCourse__WEBPACK_IMPORTED_MODULE_6__/* ["default"] */ .Z, {
                                                                            thumbnail: course.thumbnail,
                                                                            author: course.author,
                                                                            title: course.title,
                                                                            level: course.level,
                                                                            totalStudents: course.total_student,
                                                                            totalModules: course.total_module,
                                                                            totalTimes: course.total_time,
                                                                            totalRating: course.total_rating,
                                                                            totalFeedback: course.total_feedback,
                                                                            normalBuyPrice: course.buy?.normal_price || course.normal_price,
                                                                            retailBuyPrice: course.buy?.retail_price || course.retail_price,
                                                                            normalRentPrice: course.rent?.normal_price,
                                                                            retailRentPrice: course.rent?.retail_price
                                                                        })
                                                                    })
                                                                })
                                                            }, index));
                                                        }).slice(0, limit)
                                                    ]
                                                }),
                                                !loading && courses?.length > limit && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                    className: "row my-3",
                                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "col text-center",
                                                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("button", {
                                                            onClick: ()=>setLimit(limit + 9)
                                                            ,
                                                            className: "btn btn-light",
                                                            children: [
                                                                "Show more",
                                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_3__.FontAwesomeIcon, {
                                                                    size: "sm",
                                                                    className: "ms-2",
                                                                    icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_2__.faRedo
                                                                })
                                                            ]
                                                        })
                                                    })
                                                })
                                            ]
                                        })
                                    ]
                                })
                            ]
                        })
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_global_Modal_Modal__WEBPACK_IMPORTED_MODULE_8__/* ["default"] */ .Z, {
                        title: "Cara Belajar",
                        setVideo: ()=>setVideoId('')
                        ,
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "ratio ratio-16x9",
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("iframe", {
                                src: `https://www.youtube.com/embed/${videoId}`,
                                title: "YouTube video",
                                allowFullScreen: true
                            })
                        })
                    })
                ]
            })
        ]
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Library);


/***/ }),

/***/ 5368:
/***/ ((module) => {

module.exports = require("@fortawesome/free-brands-svg-icons");

/***/ }),

/***/ 197:
/***/ ((module) => {

module.exports = require("@fortawesome/free-regular-svg-icons");

/***/ }),

/***/ 6466:
/***/ ((module) => {

module.exports = require("@fortawesome/free-solid-svg-icons");

/***/ }),

/***/ 7197:
/***/ ((module) => {

module.exports = require("@fortawesome/react-fontawesome");

/***/ }),

/***/ 2167:
/***/ ((module) => {

module.exports = require("axios");

/***/ }),

/***/ 2733:
/***/ ((module) => {

module.exports = require("fuse.js");

/***/ }),

/***/ 2245:
/***/ ((module) => {

module.exports = require("moment");

/***/ }),

/***/ 5518:
/***/ ((module) => {

module.exports = require("moment/locale/id");

/***/ }),

/***/ 562:
/***/ ((module) => {

module.exports = require("next/dist/server/denormalize-page-path.js");

/***/ }),

/***/ 8028:
/***/ ((module) => {

module.exports = require("next/dist/server/image-config.js");

/***/ }),

/***/ 4957:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/head.js");

/***/ }),

/***/ 4014:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/i18n/normalize-locale-path.js");

/***/ }),

/***/ 8524:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/is-plain-object.js");

/***/ }),

/***/ 8020:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/mitt.js");

/***/ }),

/***/ 4964:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router-context.js");

/***/ }),

/***/ 9565:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/get-asset-path-from-route.js");

/***/ }),

/***/ 4365:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/get-middleware-regex.js");

/***/ }),

/***/ 1428:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/is-dynamic.js");

/***/ }),

/***/ 1292:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/parse-relative-url.js");

/***/ }),

/***/ 979:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/querystring.js");

/***/ }),

/***/ 6052:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/resolve-rewrites.js");

/***/ }),

/***/ 4226:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/route-matcher.js");

/***/ }),

/***/ 5052:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/route-regex.js");

/***/ }),

/***/ 9232:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/utils.js");

/***/ }),

/***/ 968:
/***/ ((module) => {

module.exports = require("next/head");

/***/ }),

/***/ 1853:
/***/ ((module) => {

module.exports = require("next/router");

/***/ }),

/***/ 6689:
/***/ ((module) => {

module.exports = require("react");

/***/ }),

/***/ 997:
/***/ ((module) => {

module.exports = require("react/jsx-runtime");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [7730,9285,1664,5675,4528,4986,9722,2927,2541], () => (__webpack_exec__(6105)));
module.exports = __webpack_exports__;

})();