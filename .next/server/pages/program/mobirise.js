(() => {
var exports = {};
exports.id = 6815;
exports.ids = [6815];
exports.modules = {

/***/ 1042:
/***/ ((module) => {

// Exports
module.exports = {
	"hero": "HeroMobirise_hero__YQVXm"
};


/***/ }),

/***/ 8052:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);

const CourselistSection = ({ children  })=>{
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("section", {
        className: "section",
        id: "CampaignCourseList",
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
            className: "container px-4 px-lg-5 pb-5",
            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                className: "row",
                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: "col",
                    children: children
                })
            })
        })
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (CourselistSection);


/***/ }),

/***/ 6720:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _HeroMobirise_module_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1042);
/* harmony import */ var _HeroMobirise_module_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_HeroMobirise_module_scss__WEBPACK_IMPORTED_MODULE_1__);


const HeroMobirise = ()=>{
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("section", {
        className: `${(_HeroMobirise_module_scss__WEBPACK_IMPORTED_MODULE_1___default().hero)} bg-codepolitan`,
        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
            className: "container p-4 p-lg-5",
            children: [
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: "row text-center",
                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: "col",
                        children: [
                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("h1", {
                                className: "section-title text-white",
                                children: [
                                    "3 Hari Membuat Website Sendiri,",
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                    "Tanpa Ngoding Tanpa Budget",
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                    "dan Langsung Online!"
                                ]
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                className: "mt-4 text-white w-75 mx-auto",
                                children: "Cukup satu kelas online yang Kamu butuhkan untuk membuat website Company Profile lengkap dengan pemaparan teknis, studi kasus, dan bahan-bahan buat praktek. Tidak perlu skill ngoding, install server ataupun keahlian teknis lainnya."
                            })
                        ]
                    })
                }),
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: "row",
                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: "col",
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                            className: "img-fluid d-block mx-auto",
                            src: "/assets/img/program/mobirise-placeholder.png",
                            alt: "Mobirise"
                        })
                    })
                })
            ]
        })
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (HeroMobirise);


/***/ }),

/***/ 3935:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.a(module, async (__webpack_handle_async_dependencies__) => {
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "getServerSideProps": () => (/* binding */ getServerSideProps),
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(2167);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(968);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _components_global_Layout_Layout__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(4528);
/* harmony import */ var _components_program_CourselistSection_CourselistSection__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(8052);
/* harmony import */ var _components_global_Sections_WarrantySection_WarrantySection__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(4272);
/* harmony import */ var _components_program_CertificateSection_CertificateSection__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(2068);
/* harmony import */ var _components_program_MentorSection_MentorSection__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(5341);
/* harmony import */ var _components_program_HowToLearnSection_HowToLearnSection__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(3656);
/* harmony import */ var _components_program_TestimonySection_TestimonySection__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(1533);
/* harmony import */ var _components_program_CtaSection_CtaSection__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(6916);
/* harmony import */ var _components_program_FaqSection_FaqSection__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(9982);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(7197);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(6466);
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(_fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var _utils_helper__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(4986);
/* harmony import */ var _components_program_Hero_HeroMobirise_HeroMobirise__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(6720);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_components_program_TestimonySection_TestimonySection__WEBPACK_IMPORTED_MODULE_10__]);
_components_program_TestimonySection_TestimonySection__WEBPACK_IMPORTED_MODULE_10__ = (__webpack_async_dependencies__.then ? await __webpack_async_dependencies__ : __webpack_async_dependencies__)[0];

















const getServerSideProps = async ()=>{
    const courseDetailRes = await axios__WEBPACK_IMPORTED_MODULE_2___default().get(`https://api.codepolitan.com/course/detail/membuat-website-profile-perusahaan-tanpa-coding-dengan-mobirise`);
    const testimonyRes = await axios__WEBPACK_IMPORTED_MODULE_2___default().get(`https://apps.codepolitan.com/api/feedback/course/membuat-website-profile-perusahaan-tanpa-coding-dengan-mobirise`);
    const statsRes = await axios__WEBPACK_IMPORTED_MODULE_2___default().get(`https://api.codepolitan.com/v1/stats`);
    const [courseDetail, testimony, stats] = await Promise.all([
        courseDetailRes.data,
        testimonyRes.data,
        statsRes.data
    ]);
    return {
        props: {
            courseDetail,
            testimony,
            stats
        }
    };
};
const MobiriseLanding = ({ courseDetail , testimony , stats  })=>{
    const lessons = courseDetail.lessons;
    const lessonList = (0,_utils_helper__WEBPACK_IMPORTED_MODULE_15__/* .getLessons */ .tj)(lessons);
    const lessonTopic = lessonList.lessonTopic;
    const lessonContent = lessonList.lessonContent;
    const { 0: faqData  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)([
        {
            id: 'flush-heading1',
            target: 'flush-collapse1',
            title: 'Mengapa menggunakan Mobirise?',
            content: `<p>CodePolitan sudah memiliki lebih dari ${stats.total_course} kelas online dan ${stats.total_study} modul belajar terkait pemrograman. Kami juga sudah bertemu dengan banyak sekali siswa yang belajar pemrograman hanya untuk sekedar membuat sebuah website untuk perusahaan atau pemasaran produk mereka. Banyak diantara mereka akhirnya kelelahan karena belajar pemrograman tidak sesederhana yang mereka bayangkan sebelumnya.</p><p>Kami akhirnya mencari solusi atas kebutuhan ini dan menemukan banyak sekali tools yang dapat digunakan untuk mendukung kebutuhan tersebut. Kami mencobanya satu per satu dan akhirnya menemukan kalau Mobirise-lah yang paling pas untuk kebutuhan ini.</p><p>Mobirise adalah website builder yang dapat diinstal di komputer dan digunakan secara offline. Mobirise menyediakan template gratis dengan block-block template yang banyak dan variatif dan dapat dimodifikasi sesuai kebutuhan. Selain itu kami juga menemukan cara yang paling efektif dan hemat untuk mempublikasikan website tanpa harus menyewa server atau menginstal CMS terlebih dahulu.</p><p>Di kelas online ini mentor kami sudah mengupas semua fitur Mobirise dan meramu sedemikian rupa sehingga Anda dapat membuat dan mengonlinekan website Anda nantinya tanpa budget sepeserpun. Meski demikian, Anda tetap dapat memindahkan website Anda ke server sendiri dan mengembangkannya lebih lanjut bila Anda memiliki keahlian ngoding atau memiliki tim programmer kedepannya.</p>`
        },
        {
            id: 'flush-heading2',
            target: 'flush-collapse2',
            title: 'Kapan pertemuan materi disampaikan oleh mentor?',
            content: 'Mentor kami telah membuat rangkaian video yang berisi pembahasan materi. Artinya Anda dapat mempelajari kapanpun video tersebut tanpa batas waktu.'
        },
        {
            id: 'flush-heading3',
            target: 'flush-collapse3',
            title: 'Alat apa saja yang diperlukan untuk belajar?',
            content: 'Anda akan menggunakan laptop atau komputer yang terhubung ke internet. Setelah mendaftar program Anda akan langsung mendapat akses kelas untuk mulai belajar. Silakan ikuti materi dan praktekkan studi kasus yang disampaikan oleh mentor di video pembelajaran tersebut.'
        },
        {
            id: 'flush-heading4',
            target: 'flush-collapse4',
            title: 'Kalau ada materi yang membingungkan bagaimana solusinya?',
            content: 'Anda dapat langsung bertanya pada bagian diskusi di halaman materi yang sedang Anda pelajari. Tanyakan bagian mana yang membuat Anda kurang paham. Mentor kami akan membantu menjawab persoalan Anda melalui kanal diskusi tersebut.'
        },
        {
            id: 'flush-heading5',
            target: 'flush-collapse5',
            title: 'Bisakah saya bertanya langsung via tatap muka dengan mentor?',
            content: 'Untuk saat ini belum. In syaa Allah kedepannya kami akan menyediakan layanan bimbingan privat untuk belajar pemrograman secara tatap muka online dan realtime bersama mentor.'
        }, 
    ]);
    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
        children: [
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)((next_head__WEBPACK_IMPORTED_MODULE_3___default()), {
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("title", {
                        children: "Mobirise - Codepolitan"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        name: "title",
                        content: "Bisa Bikin Web Perusahaan Tanpa Ngoding?"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        name: "description",
                        content: "Apapun latar belakang kamu, melalui kelas online ini kamu akan bisa membuat website perusahaan yang super keren dan memiliki nilai jual tanpa harus jadi jago coding."
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:type",
                        content: "website"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:url",
                        content: "https://codepolitan.com/program/mobirise"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:title",
                        content: "Bisa Bikin Web Perusahaan Tanpa Ngoding?"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:description",
                        content: "Apapun latar belakang kamu, melalui kelas online ini kamu akan bisa membuat website perusahaan yang super keren dan memiliki nilai jual tanpa harus jadi jago coding."
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:image",
                        content: "/assets/img/program/thumbnail/thumbnail-mobirise.png"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:card",
                        content: "summary_large_image"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:url",
                        content: "https://codepolitan.com/program/mobirise"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:title",
                        content: "Bisa Bikin Web Perusahaan Tanpa Ngoding?"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:description",
                        content: "Apapun latar belakang kamu, melalui kelas online ini kamu akan bisa membuat website perusahaan yang super keren dan memiliki nilai jual tanpa harus jadi jago coding."
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:image",
                        content: "/assets/img/program/thumbnail/thumbnail-mobirise.png"
                    })
                ]
            }),
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_components_global_Layout_Layout__WEBPACK_IMPORTED_MODULE_4__/* ["default"] */ .Z, {
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_program_Hero_HeroMobirise_HeroMobirise__WEBPACK_IMPORTED_MODULE_16__/* ["default"] */ .Z, {}),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("section", {
                        className: "section",
                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                            className: "container p-4 p-lg-5",
                            children: [
                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                    className: "row justify-content-between mb-5",
                                    children: [
                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                            className: "col-lg-5 order-lg-last mb-5",
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                    className: "img-fluid d-none d-lg-block mx-auto",
                                                    src: "/assets/img/program/img-mobirise-2.png",
                                                    alt: "Mobirise 1"
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                    className: "img-fluid d-block d-lg-none mx-auto",
                                                    src: "/assets/img/program/img-mobirise-2-mobile.png",
                                                    alt: "Mobirise 1"
                                                })
                                            ]
                                        }),
                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                            className: "col-lg-6 text-muted my-auto",
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
                                                    className: "section-title mb-3",
                                                    children: "Apakah Kamu Termasuk?"
                                                }),
                                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                    className: "d-flex align-items-start",
                                                    children: [
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_13__.FontAwesomeIcon, {
                                                            size: "lg",
                                                            className: "me-2 mt-1",
                                                            icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_14__.faCheckCircle
                                                        }),
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                            children: "Pemilik usaha yang pengen bikin landing page bisnis sendiri dengan budget minimum"
                                                        })
                                                    ]
                                                }),
                                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                    className: "d-flex align-items-start",
                                                    children: [
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_13__.FontAwesomeIcon, {
                                                            size: "lg",
                                                            className: "me-2 mt-1",
                                                            icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_14__.faCheckCircle
                                                        }),
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                            children: "Marketer yang ingin jualan produk online menggunakan landing page"
                                                        })
                                                    ]
                                                }),
                                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                    className: "d-flex align-items-start",
                                                    children: [
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_13__.FontAwesomeIcon, {
                                                            size: "lg",
                                                            className: "me-2 mt-1",
                                                            icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_14__.faCheckCircle
                                                        }),
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                            children: "Programmer yang ingin meminimalisir effort saat ngerjain project kecil"
                                                        })
                                                    ]
                                                }),
                                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                    className: "d-flex align-items-start",
                                                    children: [
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_13__.FontAwesomeIcon, {
                                                            size: "lg",
                                                            className: "me-2 mt-1",
                                                            icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_14__.faCheckCircle
                                                        }),
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                            children: "Desainer yang pengen nambah penghasilan dari pengembangan web company profile"
                                                        })
                                                    ]
                                                }),
                                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                    className: "d-flex align-items-start",
                                                    children: [
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_13__.FontAwesomeIcon, {
                                                            size: "lg",
                                                            className: "me-2 mt-1",
                                                            icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_14__.faCheckCircle
                                                        }),
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                            children: "Pelajar atau mahasiswa yang ingin belajar membuat website cepat dan hemat"
                                                        })
                                                    ]
                                                }),
                                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                    className: "d-flex align-items-start",
                                                    children: [
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_13__.FontAwesomeIcon, {
                                                            size: "lg",
                                                            className: "me-2 mt-1",
                                                            icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_14__.faCheckCircle
                                                        }),
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                            children: "Siapapun yang ingin dapat penghasilan tambahan dari membuat website"
                                                        })
                                                    ]
                                                })
                                            ]
                                        })
                                    ]
                                }),
                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                    className: "row justify-content-between mb-5",
                                    children: [
                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                            className: "col-lg-5 my-auto mb-5",
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                    className: "img-fluid d-none d-lg-block mx-auto",
                                                    src: "/assets/img/program/img-mobirise-3.png",
                                                    alt: ""
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                    className: "img-fluid d-block d-lg-none mx-auto",
                                                    src: "/assets/img/program/img-mobirise-3-mobile.png",
                                                    alt: ""
                                                })
                                            ]
                                        }),
                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                            className: "col-lg-6 my-auto text-muted",
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
                                                    className: "section-title mb-3",
                                                    children: "Mungkin Anda pernah mengalami masalah seperti ini:"
                                                }),
                                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                    className: "d-flex align-items-start",
                                                    children: [
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_13__.FontAwesomeIcon, {
                                                            size: "lg",
                                                            className: "me-2 mt-1",
                                                            icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_14__.faCheckCircle
                                                        }),
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                            children: "Mau bikin landing page perusahaan tapi punya budget terbatas"
                                                        })
                                                    ]
                                                }),
                                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                    className: "d-flex align-items-start",
                                                    children: [
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_13__.FontAwesomeIcon, {
                                                            size: "lg",
                                                            className: "me-2 mt-1",
                                                            icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_14__.faCheckCircle
                                                        }),
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                            children: "Sudah belajar internet marketing tapi kebingungan di tahapan membuat landing page"
                                                        })
                                                    ]
                                                }),
                                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                    className: "d-flex align-items-start",
                                                    children: [
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_13__.FontAwesomeIcon, {
                                                            size: "lg",
                                                            className: "me-2 mt-1",
                                                            icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_14__.faCheckCircle
                                                        }),
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                            children: "Mau punya landing page promosi produk dengan budget superhemat"
                                                        })
                                                    ]
                                                }),
                                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                    className: "d-flex align-items-start",
                                                    children: [
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_13__.FontAwesomeIcon, {
                                                            size: "lg",
                                                            className: "me-2 mt-1",
                                                            icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_14__.faCheckCircle
                                                        }),
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                            children: "Pengen fokus jualan dan ga mau diribetin sama belajar ngoding dulu"
                                                        })
                                                    ]
                                                })
                                            ]
                                        })
                                    ]
                                }),
                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                    className: "row justify-content-between mb-5",
                                    children: [
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: "col-lg-5 order-lg-last mb-5",
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                className: "img-fluid d-block mx-auto",
                                                src: "/assets/img/program/img-mobirise-4.png",
                                                alt: "Mobirise 1"
                                            })
                                        }),
                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                            className: "col-lg-6 text-muted my-auto",
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
                                                    className: "section-title mb-3",
                                                    children: "Atau Anda Seorang Programmer Yang"
                                                }),
                                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                    className: "d-flex align-items-start",
                                                    children: [
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_13__.FontAwesomeIcon, {
                                                            size: "lg",
                                                            className: "me-2 mt-1",
                                                            icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_14__.faCheckCircle
                                                        }),
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                            children: "Baru belajar ngoding tapi pengen cepet-cepet ambil proyek"
                                                        })
                                                    ]
                                                }),
                                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                    className: "d-flex align-items-start",
                                                    children: [
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_13__.FontAwesomeIcon, {
                                                            size: "lg",
                                                            className: "me-2 mt-1",
                                                            icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_14__.faCheckCircle
                                                        }),
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                            children: "Sering diminta bikin landing page dengan budget cuma ratusan ribu"
                                                        })
                                                    ]
                                                }),
                                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                    className: "d-flex align-items-start",
                                                    children: [
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_13__.FontAwesomeIcon, {
                                                            size: "lg",
                                                            className: "me-2 mt-1",
                                                            icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_14__.faCheckCircle
                                                        }),
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                            children: "Mau bikin website tapi kepentok di skill desain web"
                                                        })
                                                    ]
                                                })
                                            ]
                                        })
                                    ]
                                })
                            ]
                        })
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("section", {
                        className: "section bg-codepolitan",
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "container p-4 p-lg-5",
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: "row",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "col text-center",
                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
                                        className: "section-title w-75 mx-auto text-white",
                                        children: "Dengan Kelas Ini Kamu Dapat Mengatasi Masalah Tersebut Dengan Mudah Loh"
                                    })
                                })
                            })
                        })
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("section", {
                        className: "section",
                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                            className: "container p-4 p-lg-5",
                            children: [
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "row my-5",
                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "col text-center",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
                                            className: "section-title w-75 mx-auto",
                                            children: "Membuat Website Profile Perusahaan Tanpa Coding dengan Mobirise"
                                        })
                                    })
                                }),
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "row justify-content-center",
                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "col-lg-7",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: "ratio ratio-16x9",
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("iframe", {
                                                style: {
                                                    borderRadius: '25px'
                                                },
                                                src: "https://www.youtube.com/embed/HaLxOUsZHaI",
                                                title: "YouTube video player",
                                                frameBorder: "0",
                                                allow: "accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture",
                                                allowFullScreen: true
                                            })
                                        })
                                    })
                                }),
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "row my-5",
                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "col text-center text-muted",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                            className: "w-75 mx-auto",
                                            children: "Mendesain dan mengonlinekan website tidak pernah semudah ini sebelumnya! Hanya dengan berbekal laptop/komputer dan koneksi internet, kelas online ini akan memandu Anda langkah demi langkah cara praktis membuat website perusahaan dengan tampilan yang modern dan keren. Tidak diperlukan keahlian ngoding sama sekali!"
                                        })
                                    })
                                })
                            ]
                        })
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("section", {
                        className: "section bg-light",
                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                            className: "container p-4 p-lg-5",
                            children: [
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "row my-5",
                                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        className: "col text-center text-muted",
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
                                                className: "section-title",
                                                children: "Ngga Harus Keluar Banyak Biaya untuk Hal yang Dapat Dikerjakan dengan Sangat Mudah"
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                className: "my-3 w-75 mx-auto",
                                                children: "Anda bahkan nyaris tidak perlu mengeluarkan uang sama sekali untuk membeli software dan juga server untuk mengonlinekan website Anda nanti."
                                            })
                                        ]
                                    })
                                }),
                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                    className: "row justify-content-center text-center mb-5",
                                    children: [
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: "col-md-6 col-lg-4 mb-3",
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                className: "card border-0 shadow-sm rounded",
                                                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                    className: "card-body",
                                                    children: [
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_13__.FontAwesomeIcon, {
                                                            className: "text-primary",
                                                            size: "5x",
                                                            icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_14__.faLaptopCode
                                                        }),
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h5", {
                                                            className: "my-3",
                                                            children: "Belajar Studi Kasus"
                                                        }),
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                            className: "text-muted",
                                                            children: "Praktek dengan studi kasus tidak hanya memudahkan pemahaman dari sisi teori, tapi juga memberikan gambaran praktis sesuai dengan kebutuhan di dunia kerja."
                                                        })
                                                    ]
                                                })
                                            })
                                        }),
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: "col-md-6 col-lg-4 mb-3",
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                className: "card border-0 shadow-sm rounded",
                                                style: {
                                                    height: '100%'
                                                },
                                                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                    className: "card-body",
                                                    children: [
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_13__.FontAwesomeIcon, {
                                                            className: "text-primary",
                                                            size: "5x",
                                                            icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_14__.faClock
                                                        }),
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h5", {
                                                            className: "my-3",
                                                            children: "Waktu Belajar Fleksibel"
                                                        }),
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                            className: "text-muted",
                                                            children: "Tidak perlu takut ketinggalan pembahasan di kelas. Anda dapat mempelajari materi sesuai dengan jadwal yang Anda atur sendiri."
                                                        })
                                                    ]
                                                })
                                            })
                                        }),
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: "col-md-6 col-lg-4 mb-3",
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                className: "card border-0 shadow-sm rounded",
                                                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                    className: "card-body",
                                                    children: [
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_13__.FontAwesomeIcon, {
                                                            className: "text-primary",
                                                            size: "5x",
                                                            icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_14__.faComments
                                                        }),
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h5", {
                                                            className: "my-3",
                                                            children: "Fitur Tanya Jawab"
                                                        }),
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                            className: "text-muted",
                                                            children: "Jika kamu memiliki kendala saat belajar, kamu bisa langsung bertanya di kanal tanya jawab yang telah disediakan. Tim Codepolitan akan senang hati membantumu."
                                                        })
                                                    ]
                                                })
                                            })
                                        })
                                    ]
                                })
                            ]
                        })
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_program_CertificateSection_CertificateSection__WEBPACK_IMPORTED_MODULE_7__/* ["default"] */ .Z, {}),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_program_MentorSection_MentorSection__WEBPACK_IMPORTED_MODULE_8__/* ["default"] */ .Z, {
                        name: "Yusuf Fazeri",
                        title: "Mentor kelas Mobirise",
                        description: "Saya Yusuf Fazeri, seorang Full Stack Developer dan Senior Programmer di Codepolitan. Dengan pengalaman saya lebih dari 6 tahun berkarya dan berkarir di bidang programming akan membantu kamu belajar dalam kelas ini. Apapun latar belakang kamu, melalui kelas ini kamu akan belajar membuat website perusahaan sendiri yang super keren. Saya tunggu di kelas!",
                        img: "/assets/img/program/yusuf.png"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_program_HowToLearnSection_HowToLearnSection__WEBPACK_IMPORTED_MODULE_9__/* ["default"] */ .Z, {}),
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_components_program_CourselistSection_CourselistSection__WEBPACK_IMPORTED_MODULE_5__/* ["default"] */ .Z, {
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
                                className: "text-center text-muted my-5",
                                children: "Apa Saja Materi yang akan Dipelajari?"
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("ul", {
                                className: "nav nav-pills d-grid d-md-flex nav-justified bg-light mb-3",
                                id: "pills-tab",
                                role: "tablist",
                                children: lessonTopic.map((item, index)=>{
                                    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                                        className: "nav-item",
                                        role: "presentation",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                            className: `nav-link py-3 ${index < 1 && 'active'}`,
                                            id: item.topic_slug.slice(3) + '-tab',
                                            "data-bs-toggle": "pill",
                                            "data-bs-target": '#' + item.topic_slug.slice(3),
                                            type: "button",
                                            role: "tab",
                                            "aria-controls": item.topic_slug.slice(3),
                                            "aria-selected": "true",
                                            title: item.topic_title,
                                            children: item.topic_title.length > 20 ? item.topic_title.slice(0, 20) + '...' : item.topic_title
                                        })
                                    }, index));
                                })
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: "tab-content",
                                id: "pills-tabContent",
                                children: lessonContent.map((content, index1)=>{
                                    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: `tab-pane fade show ${index1 < 1 && 'active'}`,
                                        id: content.topic_slug.slice(3),
                                        role: "tabpanel",
                                        "aria-labelledby": content.topic_slug.slice(3) + '-tab',
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("table", {
                                            className: "table table-striped",
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("tbody", {
                                                children: content.contents?.map((item, index)=>{
                                                    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("tr", {
                                                        children: [
                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("td", {
                                                                style: {
                                                                    width: '50px'
                                                                },
                                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                                    height: "30",
                                                                    src: "/assets/img/program/play-icon.png",
                                                                    alt: "Icon"
                                                                })
                                                            }),
                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("td", {
                                                                className: "text-muted",
                                                                children: item.lesson_title
                                                            }),
                                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("td", {
                                                                className: "text-end text-muted",
                                                                children: [
                                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_13__.FontAwesomeIcon, {
                                                                        icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_14__.faLock,
                                                                        className: "me-2"
                                                                    }),
                                                                    item.duration || '15:00'
                                                                ]
                                                            })
                                                        ]
                                                    }, index));
                                                })
                                            })
                                        })
                                    }, index1));
                                })
                            })
                        ]
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_program_TestimonySection_TestimonySection__WEBPACK_IMPORTED_MODULE_10__/* ["default"] */ .Z, {
                        data: testimony
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("section", {
                        className: "section d-none",
                        style: {
                            backgroundColor: '#FDFDFD'
                        },
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "container p-4 p-lg-5",
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: "row",
                                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                    className: "col text-center text-muted",
                                    children: [
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
                                            className: "section-title mb-5",
                                            children: "Tapi, sebelum Anda memutuskan untuk bergabung, pahami problem krusial ini terlebih dahulu!"
                                        }),
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                            children: "Anda perlu memahami bahwa dengan membuat website atau landing page saja tidak lantas membuat penjualan produk Anda meningkat, atau brand bisnis Anda serta merta langsung jadi terkenal."
                                        }),
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                            children: "Membuat website ibarat membuka toko. Anda harus benar-benar menata toko Anda sedemikian rupa supaya pengunjung paham penawaran apa yang Anda sajikan di toko Anda. Membuat website tanpa konsep yang jelas tidak hanya membuat pengunjung website Anda jadi gagal paham, tapi juga pergi meninggalkan website Anda tanpa membawa kesan dan pesan apapun di benak mereka."
                                        }),
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                            children: "Ingat, di dunia marketing, penjualan terjadi sejak Anda berhasil menanamkan brand dan penawaran Anda ke dalam benak pengunjung Anda!"
                                        }),
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                            children: "Oleh karena itu, kami bekali Anda dengan dua buah Ebook eksklusif ini:"
                                        })
                                    ]
                                })
                            })
                        })
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("section", {
                        className: "section d-none",
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "container p-4 p-lg-5",
                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                className: "row text-center text-muted",
                                children: [
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        className: "col-12 mb-5",
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
                                                className: "section-title",
                                                children: "17 Kunci Rahasia Landing Page yang Berhasil Menjual dalam Sekali Baca"
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                className: "img-fluid d-block mx-auto",
                                                src: "/assets/img/program/ebook-1.png",
                                                alt: "17 Kunci Rahasia Landing Page yang Berhasil Menjual dalam Sekali Baca"
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                children: "Dalam ebook ini dibahas tuntas 17 poin kunci yang wajib Anda sertakan di dalam Halaman website Anda agar pengunjung website Anda buru-buru untuk membeli produk/layanan Anda."
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        className: "col-12",
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
                                                className: "section-title",
                                                children: "Branding Acceleration"
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                className: "img-fluid d-block mx-auto",
                                                src: "/assets/img/program/ebook-2.png",
                                                alt: "Branding Acceleration"
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                children: "Dalam ebook ini dibahas teknik praktis dan selalu ampuh dalam membenamkan brand Anda ke benak calon konsumen dalam waktu 21 hari bahkn kurang!"
                                            })
                                        ]
                                    })
                                ]
                            })
                        })
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("section", {
                        className: "section bg-codepolitan d-none",
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "container p-4 p-lg-5",
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: "row",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "col text-center",
                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
                                        className: "section-title text-white",
                                        children: "Dua ebook powerful senilai Rp 850.000 ini akan kami sertakan dalam paket pembelajaran kelas online tanpa biaya tambahan lain bila Anda bergabung dalam program terbatas ini sebelum tanggal 21 November 2021."
                                    })
                                })
                            })
                        })
                    }),
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: "bg-light",
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
                                className: "text-center pt-5",
                                children: "Masih Ragu?"
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_global_Sections_WarrantySection_WarrantySection__WEBPACK_IMPORTED_MODULE_6__/* ["default"] */ .Z, {})
                        ]
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_program_CtaSection_CtaSection__WEBPACK_IMPORTED_MODULE_11__/* ["default"] */ .Z, {
                        title: "Tunggu Apalagi ? Ayo Gabung Kelas Bikin Web Tanpa Koding Sekarang !",
                        priceImg: "/assets/img/program/price-mobirise.png",
                        actionLink: "https://pay.codepolitan.com/?slug=membuat-website-profile-perusahaan-tanpa-coding-dengan-mobirise"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_program_FaqSection_FaqSection__WEBPACK_IMPORTED_MODULE_12__/* ["default"] */ .Z, {
                        data: faqData
                    })
                ]
            })
        ]
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (MobiriseLanding);

});

/***/ }),

/***/ 5368:
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/free-brands-svg-icons");

/***/ }),

/***/ 197:
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/free-regular-svg-icons");

/***/ }),

/***/ 6466:
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/free-solid-svg-icons");

/***/ }),

/***/ 7197:
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/react-fontawesome");

/***/ }),

/***/ 2167:
/***/ ((module) => {

"use strict";
module.exports = require("axios");

/***/ }),

/***/ 2733:
/***/ ((module) => {

"use strict";
module.exports = require("fuse.js");

/***/ }),

/***/ 2245:
/***/ ((module) => {

"use strict";
module.exports = require("moment");

/***/ }),

/***/ 5518:
/***/ ((module) => {

"use strict";
module.exports = require("moment/locale/id");

/***/ }),

/***/ 562:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/server/denormalize-page-path.js");

/***/ }),

/***/ 8028:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/server/image-config.js");

/***/ }),

/***/ 4957:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/head.js");

/***/ }),

/***/ 4014:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/i18n/normalize-locale-path.js");

/***/ }),

/***/ 8524:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/is-plain-object.js");

/***/ }),

/***/ 8020:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/mitt.js");

/***/ }),

/***/ 4964:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router-context.js");

/***/ }),

/***/ 9565:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/get-asset-path-from-route.js");

/***/ }),

/***/ 4365:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/get-middleware-regex.js");

/***/ }),

/***/ 1428:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/is-dynamic.js");

/***/ }),

/***/ 1292:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/parse-relative-url.js");

/***/ }),

/***/ 979:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/querystring.js");

/***/ }),

/***/ 6052:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/resolve-rewrites.js");

/***/ }),

/***/ 4226:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/route-matcher.js");

/***/ }),

/***/ 5052:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/route-regex.js");

/***/ }),

/***/ 9232:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/utils.js");

/***/ }),

/***/ 968:
/***/ ((module) => {

"use strict";
module.exports = require("next/head");

/***/ }),

/***/ 1853:
/***/ ((module) => {

"use strict";
module.exports = require("next/router");

/***/ }),

/***/ 6689:
/***/ ((module) => {

"use strict";
module.exports = require("react");

/***/ }),

/***/ 7386:
/***/ ((module) => {

"use strict";
module.exports = require("react-simple-star-rating");

/***/ }),

/***/ 997:
/***/ ((module) => {

"use strict";
module.exports = require("react/jsx-runtime");

/***/ }),

/***/ 3877:
/***/ ((module) => {

"use strict";
module.exports = import("swiper");;

/***/ }),

/***/ 3015:
/***/ ((module) => {

"use strict";
module.exports = import("swiper/react");;

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [7730,9285,1664,5675,4528,4986,2068,4373,4272,5341], () => (__webpack_exec__(3935)));
module.exports = __webpack_exports__;

})();