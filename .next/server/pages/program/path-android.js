"use strict";
(() => {
var exports = {};
exports.id = 5564;
exports.ids = [5564];
exports.modules = {

/***/ 5956:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__) => {
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "getServerSideProps": () => (/* binding */ getServerSideProps),
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2167);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(968);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _components_global_Layout_Layout__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(4528);
/* harmony import */ var _components_program_CertificateSection_CertificateSection__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(2068);
/* harmony import */ var _components_program_FaqSection_FaqSection__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(9982);
/* harmony import */ var _components_program_ForumSection_ForumSection__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(4579);
/* harmony import */ var _components_program_Hero_Hero__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(1653);
/* harmony import */ var _components_program_HowToLearnSection_HowToLearnSection__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(3656);
/* harmony import */ var _components_program_StudycaseSection_StudycaseSection__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(6244);
/* harmony import */ var _components_program_TestimonySection_TestimonySection__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(1533);
/* harmony import */ var _components_program_WaktuBelajarSection_WaktuBelajarSection__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(420);
/* harmony import */ var _components_global_Sections_WarrantySection_WarrantySection__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(4272);
/* harmony import */ var react_countup__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(609);
/* harmony import */ var react_countup__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(react_countup__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var _styles_Program_module_scss__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(3680);
/* harmony import */ var _styles_Program_module_scss__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(_styles_Program_module_scss__WEBPACK_IMPORTED_MODULE_16__);
/* harmony import */ var _components_program_CtaSection_CtaSection__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(6916);
/* harmony import */ var _components_program_AboutSection_AboutSection__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(6410);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_components_program_TestimonySection_TestimonySection__WEBPACK_IMPORTED_MODULE_10__, _components_program_StudycaseSection_StudycaseSection__WEBPACK_IMPORTED_MODULE_9__]);
([_components_program_TestimonySection_TestimonySection__WEBPACK_IMPORTED_MODULE_10__, _components_program_StudycaseSection_StudycaseSection__WEBPACK_IMPORTED_MODULE_9__] = __webpack_async_dependencies__.then ? await __webpack_async_dependencies__ : __webpack_async_dependencies__);

















const getServerSideProps = async ()=>{
    const testimonyRes = await axios__WEBPACK_IMPORTED_MODULE_1___default().get(`https://apps.codepolitan.com/api/feedback/course/membuat-aplikasi-elearning-di-android-menggunakan-realtime-firebase-kotlin`);
    const [testimony] = await Promise.all([
        testimonyRes.data, 
    ]);
    return {
        props: {
            testimony
        }
    };
};
const PathAndroidLanding = ({ testimony  })=>{
    // console.log(testimony);
    const data = [
        {
            case_study: [
                {
                    id: 1,
                    thumbnail: 'https://i.ibb.co/GnLdypF/fullstack-1.png',
                    title: 'Aplikasi Management Tugas'
                },
                {
                    id: 2,
                    thumbnail: 'https://i.ibb.co/6yYcsdc/android-1.png',
                    title: 'Aplikasi E-Learning'
                },
                {
                    id: 3,
                    thumbnail: 'https://i.ibb.co/gvLcgLD/android-2.png',
                    title: 'Aplikasi Kuis'
                },
                {
                    id: 4,
                    thumbnail: 'https://i.ibb.co/jkqYBFF/android-3.png',
                    title: 'Aplikasi Musik Player'
                }, 
            ],
            courses: [
                {
                    id: 7,
                    thumbnail: 'https://i.ibb.co/7S3qM9j/icon-android.png',
                    title: 'Kelas Konsep',
                    description: 'Android Development Starter Pack',
                    number_module: '57'
                },
                {
                    id: 1,
                    thumbnail: 'https://i.ibb.co/7S3qM9j/icon-android.png',
                    title: 'Studi Kasus Android',
                    description: 'Membuat Aplikasi Presensi Android Berbasis Local',
                    number_module: '20'
                },
                {
                    id: 2,
                    thumbnail: 'https://i.ibb.co/7S3qM9j/icon-android.png',
                    title: 'Studi Kasus Android',
                    description: 'Implementasi Firebase Auth di Android',
                    number_module: '20'
                },
                {
                    id: 3,
                    thumbnail: 'https://i.ibb.co/7S3qM9j/icon-android.png',
                    title: 'Studi Kasus Android',
                    description: 'Implementasi ViewModel Pada Aplikasi Android',
                    number_module: '20'
                },
                {
                    id: 4,
                    thumbnail: 'https://i.ibb.co/7S3qM9j/icon-android.png',
                    title: 'Studi Kasus Android',
                    description: 'Memahami Cara Kerja Tim IT Perusahaan Yang WFH',
                    number_module: '19'
                },
                {
                    id: 5,
                    thumbnail: 'https://i.ibb.co/7S3qM9j/icon-android.png',
                    title: 'Studi Kasus Android',
                    description: 'Membuat Aplikasi Presensi Online Berbasis Web Dan Android',
                    number_module: '48'
                },
                {
                    id: 6,
                    thumbnail: 'https://i.ibb.co/7S3qM9j/icon-android.png',
                    title: 'Studi Kasus Android',
                    description: 'Membuat Infinite Scroll Time Seperti Instagram Di Android',
                    number_module: '14'
                },
                {
                    id: 8,
                    thumbnail: 'https://i.ibb.co/7S3qM9j/icon-android.png',
                    title: 'Studi Kasus Android',
                    description: 'Membuat Aplikasi Android Management Tugas',
                    number_module: '21'
                },
                {
                    id: 9,
                    thumbnail: 'https://i.ibb.co/7S3qM9j/icon-android.png',
                    title: 'Studi Kasus Android',
                    description: 'Membuat Aplikasi E-Learning di Android Dengan Realtime FIrebase',
                    number_module: '38'
                },
                {
                    id: 10,
                    thumbnail: 'https://i.ibb.co/7S3qM9j/icon-android.png',
                    title: 'Studi Kasus Android',
                    description: 'Membuat Aplikasi Quiz Dengan RecyclerView',
                    number_module: '21'
                },
                {
                    id: 11,
                    thumbnail: 'https://i.ibb.co/7S3qM9j/icon-android.png',
                    title: 'Studi Kasus Android',
                    description: 'Membuat Aplikasi Music Player di Android Dengan Kotln',
                    number_module: '59'
                }, 
            ],
            testimony: [
                {
                    name: 'Test',
                    comment: 'Lorem ipsum',
                    rate: 4
                },
                {
                    name: 'Test',
                    comment: 'Lorem ipsum',
                    rate: 4
                },
                {
                    name: 'Test',
                    comment: 'Lorem ipsum',
                    rate: 4
                },
                {
                    name: 'Test',
                    comment: 'Lorem ipsum',
                    rate: 4
                },
                {
                    name: 'Test',
                    comment: 'Lorem ipsum',
                    rate: 4
                }, 
            ],
            faq: [
                {
                    id: 'flush-heading1',
                    target: 'flush-collapse1',
                    title: 'Alat apa saja yang diperlukan untuk belajar?',
                    content: 'Anda akan menggunakan laptop atau komputer yang terinstall aplikasi Android Studio dan terhubung ke internet untuk menyimak materi. Akan lebih baik bila kamu juga menyiapkan smartphone Android untuk kebutuhan testing di device.'
                },
                {
                    id: 'flush-heading2',
                    target: 'flush-collapse2',
                    title: 'Saya baru di pemrograman, apakah saya dapat belajar membuat aplikasi Android?',
                    content: 'Kamu akan perlu mempelajari dulu dasar-dasar pemrograman dan bahasa pemrograman Java. Dari situ kamu bisa mulai belajar dari kelas konsep dasar Android Development Starter Pack dan lanjut ke kelas berikutnya sesuai urutan belajar yang telah kami siapkan.'
                },
                {
                    id: 'flush-heading3',
                    target: 'flush-collapse3',
                    title: 'Kalau ada materi yang membingungkan bagaimana solusinya?',
                    content: 'Anda dapat langsung bertanya pada bagian diskusi di halaman materi yang sedang Anda pelajari. Tanyakan bagian mana yang membuat Anda kurang paham. Mentor kami akan membantu menjawab persoalan Anda melalui kanal diskusi tersebut.'
                },
                {
                    id: 'flush-heading4',
                    target: 'flush-collapse4',
                    title: 'Bisakah saya bertanya langsung via tatap muka dengan mentor?',
                    content: 'Untuk saat ini belum. In syaa Allah kedepannya kami akan menyediakan layanan bimbingan privat untuk belajar pemrograman secara tatap muka online dan realtime bersama mentor.'
                }, 
            ]
        }
    ];
    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
        children: [
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)((next_head__WEBPACK_IMPORTED_MODULE_2___default()), {
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("title", {
                        children: "Path Android Developer - Codepolitan"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        name: "title",
                        content: "Raih Karir Android Developer Melalui Kelas Android Codepolitan"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        name: "description",
                        content: "Apapun latar belakang kamu, melalui kelas online ini kamu akan belajar terarah dari nol hingga menjadi mahir pemrograman android dengan standar kompetensi kerja"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:type",
                        content: "website"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:url",
                        content: "https://codepolitan.com/program/path-android"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:title",
                        content: "Raih Karir Android Developer Melalui Kelas Android Codepolitan"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:description",
                        content: "Apapun latar belakang kamu, melalui kelas online ini kamu akan belajar terarah dari nol hingga menjadi mahir pemrograman android dengan standar kompetensi kerja"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:image",
                        content: "/assets/img/program/hero-path-android.webp"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:card",
                        content: "summary_large_image"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:url",
                        content: "https://codepolitan.com/program/path-android"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:title",
                        content: "Raih Karir Android Developer Melalui Kelas Android Codepolitan"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:description",
                        content: "Apapun latar belakang kamu, melalui kelas online ini kamu akan belajar terarah dari nol hingga menjadi mahir pemrograman android dengan standar kompetensi kerja"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:image",
                        content: "/assets/img/program/hero-path-android.webp"
                    })
                ]
            }),
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_components_global_Layout_Layout__WEBPACK_IMPORTED_MODULE_3__/* ["default"] */ .Z, {
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_program_Hero_Hero__WEBPACK_IMPORTED_MODULE_7__/* ["default"] */ .Z, {
                        heroTitle: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                            children: [
                                "Raih Karir ",
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                    className: "text-primary",
                                    children: "Android Developer"
                                }),
                                " Melalui Kelas Android Codepolitan"
                            ]
                        }),
                        heroSubtitle: "Apapun latar belakang kamu, melalui kelas online ini kamu akan belajar terarah dari nol hingga menjadi mahir pemrograman android dengan standar kompetensi kerja",
                        heroImg: "/assets/img/program/hero-path-android.webp",
                        heroBgColor: "bg-light",
                        heroFontColor: "text-dark",
                        heroBtnColor: "btn-outline-secondary"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_program_AboutSection_AboutSection__WEBPACK_IMPORTED_MODULE_15__/* ["default"] */ .Z, {
                        thumbnail: "/assets/img/program/android-img.png",
                        title: "Android App Development",
                        description: "Dalam kelas ini kamu akan belajar dari awal hingga menjadi mahir pemrograman Android. melalui kelas ini, kamu yang belum mahir sekalipun dalam bidang pemrograman akan bisa membuat aplikasi Android yang super keren dan dapat bersaing di dunia industri untuk mendapatkan karir Impianmu"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_program_StudycaseSection_StudycaseSection__WEBPACK_IMPORTED_MODULE_9__/* ["default"] */ .Z, {
                        data: data[0].case_study
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("section", {
                        className: "section",
                        style: {
                            backgroundColor: '#eee'
                        },
                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                            className: "container p-4 p-lg-5",
                            children: [
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "row",
                                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        className: "col text-center",
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h3", {
                                                className: "section-title",
                                                children: "Apa yang Akan Kamu Pelajari"
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                className: "text-muted",
                                                children: "Yang akan kamu pelajari dalam kelas ini"
                                            })
                                        ]
                                    })
                                }),
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "row mt-3",
                                    children: data[0].courses.map((item, index)=>{
                                        return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: "col-md-6 col-lg-4 col-xl-3 mb-4",
                                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                className: "card border-0 shadow-sm",
                                                style: {
                                                    borderRadius: '15px',
                                                    height: '100%'
                                                },
                                                children: [
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "card-header bg-white border-0",
                                                        style: {
                                                            borderRadius: '15px'
                                                        },
                                                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                            className: "row mt-2",
                                                            children: [
                                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                                    className: "col-8",
                                                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                                        height: "50",
                                                                        src: item.thumbnail,
                                                                        alt: item.title
                                                                    })
                                                                }),
                                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                                    className: "col-4 text-center",
                                                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                                        className: (_styles_Program_module_scss__WEBPACK_IMPORTED_MODULE_16___default().card_label_materi),
                                                                        children: index + 1
                                                                    })
                                                                })
                                                            ]
                                                        })
                                                    }),
                                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                        className: "card-body text-muted pb-0",
                                                        children: [
                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h6", {
                                                                style: {
                                                                    fontSize: 'smaller'
                                                                },
                                                                children: item.title
                                                            }),
                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("strong", {
                                                                    children: item.description
                                                                })
                                                            })
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "row mb-3",
                                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                            className: "col text-end",
                                                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                                                                className: "badge bg-codepolitan px-3",
                                                                style: {
                                                                    borderRadius: 0,
                                                                    borderTopLeftRadius: '25px',
                                                                    borderBottomLeftRadius: '25px'
                                                                },
                                                                children: [
                                                                    item.number_module,
                                                                    " modul"
                                                                ]
                                                            })
                                                        })
                                                    })
                                                ]
                                            })
                                        }, index));
                                    })
                                })
                            ]
                        })
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("section", {
                        className: "section bg-light",
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "container p-4 p-lg-5",
                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                className: "row justify-content-center text-center",
                                children: [
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "col-6 col-md-4 my-2",
                                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("p", {
                                            className: "text-secondary mb-0",
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                    className: `${(_styles_Program_module_scss__WEBPACK_IMPORTED_MODULE_16___default().count)} text-secondary`,
                                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((react_countup__WEBPACK_IMPORTED_MODULE_13___default()), {
                                                        end: 11,
                                                        duration: 5
                                                    })
                                                }),
                                                " Kelas Android"
                                            ]
                                        })
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "col-6 col-md-4 my-2",
                                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("p", {
                                            className: "text-secondary mb-0",
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                    className: `${(_styles_Program_module_scss__WEBPACK_IMPORTED_MODULE_16___default().count)} text-secondary`,
                                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((react_countup__WEBPACK_IMPORTED_MODULE_13___default()), {
                                                        end: 140191,
                                                        duration: 5
                                                    })
                                                }),
                                                " Member Aktif"
                                            ]
                                        })
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "col-6 col-md-4 my-2",
                                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("p", {
                                            className: "text-secondary mb-0",
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                    className: `${(_styles_Program_module_scss__WEBPACK_IMPORTED_MODULE_16___default().count)} text-secondary`,
                                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((react_countup__WEBPACK_IMPORTED_MODULE_13___default()), {
                                                        end: 367,
                                                        duration: 5
                                                    })
                                                }),
                                                " Modul Belajar"
                                            ]
                                        })
                                    })
                                ]
                            })
                        })
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_program_WaktuBelajarSection_WaktuBelajarSection__WEBPACK_IMPORTED_MODULE_11__/* ["default"] */ .Z, {}),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_program_ForumSection_ForumSection__WEBPACK_IMPORTED_MODULE_6__/* ["default"] */ .Z, {}),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("section", {
                        className: "section",
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "container p-4 p-lg-5",
                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                className: "row",
                                children: [
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        className: "col-lg-5 offset-lg-1 my-auto",
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
                                                className: "section-title",
                                                children: "Online Mentoring"
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                className: "text-muted my-3",
                                                children: "Dapatkan kesempatan 12x berdiskusi dengan mentor kelas fullstack developer dalam sesi mentoring sebanyak 2x dalam 1 bulan, supaya kamu dapat belajar dengan terarah."
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "col-lg-6 order-lg-first",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                            className: "img-fluid",
                                            src: "/assets/img/program/discord-img-3.png",
                                            alt: "Online Mentoring"
                                        })
                                    })
                                ]
                            })
                        })
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_program_CertificateSection_CertificateSection__WEBPACK_IMPORTED_MODULE_4__/* ["default"] */ .Z, {}),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("section", {
                        className: "section bg-light",
                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                            className: "container text-center p-4 p-lg-5",
                            children: [
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "row my-3",
                                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        className: "col",
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
                                                className: "section-title",
                                                children: "Mentor Belajar"
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                className: "text-muted",
                                                children: "Dalam program ini kamu akan dibimbing oleh para mentor profesional yang berpengalaman"
                                            })
                                        ]
                                    })
                                }),
                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                    className: "row justify-content-center",
                                    children: [
                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                            className: "col-6 col-lg-3 mb-3",
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                    className: "img-fluid",
                                                    src: "/assets/img/program/mentor/aulia-rahman.png",
                                                    alt: "Aulia Rahman"
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h5", {
                                                    className: "mt-4",
                                                    children: "Aulia Rahman"
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                    className: "text-muted",
                                                    children: "Android Developer"
                                                })
                                            ]
                                        }),
                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                            className: "col-6 col-lg-3 mb-3",
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                    className: "img-fluid",
                                                    src: "/assets/img/program/mentor/muhammad-singgih.png",
                                                    alt: "Muhammad Singgih Z.A"
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h5", {
                                                    className: "mt-4",
                                                    children: "Muhammad Singgih Z.A"
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                    className: "text-muted",
                                                    children: "CMO at Codepolitan"
                                                })
                                            ]
                                        }),
                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                            className: "col-6 col-lg-3 mb-3",
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                    className: "img-fluid",
                                                    src: "/assets/img/program/mentor/hakim-sembiring.png",
                                                    alt: "Hakim Sembiring"
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h5", {
                                                    className: "mt-4",
                                                    children: "Hakim Sembiring"
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                    className: "text-muted",
                                                    children: "Fullstack Developer"
                                                })
                                            ]
                                        })
                                    ]
                                })
                            ]
                        })
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_program_HowToLearnSection_HowToLearnSection__WEBPACK_IMPORTED_MODULE_8__/* ["default"] */ .Z, {}),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_program_TestimonySection_TestimonySection__WEBPACK_IMPORTED_MODULE_10__/* ["default"] */ .Z, {
                        data: testimony
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_program_CtaSection_CtaSection__WEBPACK_IMPORTED_MODULE_14__/* ["default"] */ .Z, {
                        title: "Tunggu Apalagi ? Ayo Gabung Program Belajar Android App Development Selama 6 Bulan!",
                        priceImg: "/assets/img/program/price-android.png",
                        actionLink: "https://pay.codepolitan.com/?slug=program-android-app-development"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: "bg-light",
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_global_Sections_WarrantySection_WarrantySection__WEBPACK_IMPORTED_MODULE_12__/* ["default"] */ .Z, {})
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_program_FaqSection_FaqSection__WEBPACK_IMPORTED_MODULE_5__/* ["default"] */ .Z, {
                        data: data[0].faq
                    })
                ]
            })
        ]
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (PathAndroidLanding);

});

/***/ }),

/***/ 5368:
/***/ ((module) => {

module.exports = require("@fortawesome/free-brands-svg-icons");

/***/ }),

/***/ 197:
/***/ ((module) => {

module.exports = require("@fortawesome/free-regular-svg-icons");

/***/ }),

/***/ 6466:
/***/ ((module) => {

module.exports = require("@fortawesome/free-solid-svg-icons");

/***/ }),

/***/ 7197:
/***/ ((module) => {

module.exports = require("@fortawesome/react-fontawesome");

/***/ }),

/***/ 2167:
/***/ ((module) => {

module.exports = require("axios");

/***/ }),

/***/ 2733:
/***/ ((module) => {

module.exports = require("fuse.js");

/***/ }),

/***/ 562:
/***/ ((module) => {

module.exports = require("next/dist/server/denormalize-page-path.js");

/***/ }),

/***/ 8028:
/***/ ((module) => {

module.exports = require("next/dist/server/image-config.js");

/***/ }),

/***/ 4957:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/head.js");

/***/ }),

/***/ 4014:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/i18n/normalize-locale-path.js");

/***/ }),

/***/ 8524:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/is-plain-object.js");

/***/ }),

/***/ 8020:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/mitt.js");

/***/ }),

/***/ 4964:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router-context.js");

/***/ }),

/***/ 9565:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/get-asset-path-from-route.js");

/***/ }),

/***/ 4365:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/get-middleware-regex.js");

/***/ }),

/***/ 1428:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/is-dynamic.js");

/***/ }),

/***/ 1292:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/parse-relative-url.js");

/***/ }),

/***/ 979:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/querystring.js");

/***/ }),

/***/ 6052:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/resolve-rewrites.js");

/***/ }),

/***/ 4226:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/route-matcher.js");

/***/ }),

/***/ 5052:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/route-regex.js");

/***/ }),

/***/ 9232:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/utils.js");

/***/ }),

/***/ 968:
/***/ ((module) => {

module.exports = require("next/head");

/***/ }),

/***/ 1853:
/***/ ((module) => {

module.exports = require("next/router");

/***/ }),

/***/ 6689:
/***/ ((module) => {

module.exports = require("react");

/***/ }),

/***/ 609:
/***/ ((module) => {

module.exports = require("react-countup");

/***/ }),

/***/ 7386:
/***/ ((module) => {

module.exports = require("react-simple-star-rating");

/***/ }),

/***/ 997:
/***/ ((module) => {

module.exports = require("react/jsx-runtime");

/***/ }),

/***/ 3877:
/***/ ((module) => {

module.exports = import("swiper");;

/***/ }),

/***/ 3015:
/***/ ((module) => {

module.exports = import("swiper/react");;

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [7730,9285,1664,5675,4528,2068,4373,7838,1653,4272,6410,9504], () => (__webpack_exec__(5956)));
module.exports = __webpack_exports__;

})();