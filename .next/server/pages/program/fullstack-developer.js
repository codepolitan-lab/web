"use strict";
(() => {
var exports = {};
exports.id = 3576;
exports.ids = [3576];
exports.modules = {

/***/ 7638:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__) => {
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "getServerSideProps": () => (/* binding */ getServerSideProps),
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2167);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(968);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _components_global_Layout_Layout__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(4528);
/* harmony import */ var _components_program_CertificateSection_CertificateSection__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(2068);
/* harmony import */ var _components_program_CtaSection_CtaSection__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(6916);
/* harmony import */ var _components_program_FaqSection_FaqSection__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(9982);
/* harmony import */ var _components_program_ForumSection_ForumSection__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(4579);
/* harmony import */ var _components_program_Hero_Hero__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(1653);
/* harmony import */ var _components_program_HowToLearnSection_HowToLearnSection__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(3656);
/* harmony import */ var _components_program_StudycaseSection_StudycaseSection__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(6244);
/* harmony import */ var _components_program_TestimonySection_TestimonySection__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(1533);
/* harmony import */ var _components_program_WaktuBelajarSection_WaktuBelajarSection__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(420);
/* harmony import */ var _components_global_Sections_WarrantySection_WarrantySection__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(4272);
/* harmony import */ var react_countup__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(609);
/* harmony import */ var react_countup__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(react_countup__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var _styles_Program_module_scss__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(3680);
/* harmony import */ var _styles_Program_module_scss__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(_styles_Program_module_scss__WEBPACK_IMPORTED_MODULE_16__);
/* harmony import */ var _components_program_AboutSection_AboutSection__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(6410);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_components_program_TestimonySection_TestimonySection__WEBPACK_IMPORTED_MODULE_11__, _components_program_StudycaseSection_StudycaseSection__WEBPACK_IMPORTED_MODULE_10__]);
([_components_program_TestimonySection_TestimonySection__WEBPACK_IMPORTED_MODULE_11__, _components_program_StudycaseSection_StudycaseSection__WEBPACK_IMPORTED_MODULE_10__] = __webpack_async_dependencies__.then ? await __webpack_async_dependencies__ : __webpack_async_dependencies__);

















const getServerSideProps = async ()=>{
    const testimonyRes = await axios__WEBPACK_IMPORTED_MODULE_1___default().get(`https://apps.codepolitan.com/api/feedback/course/laravel-8x-fundamental`);
    const [testimony] = await Promise.all([
        testimonyRes.data, 
    ]);
    return {
        props: {
            testimony
        }
    };
};
const FullstackLanding = ({ testimony  })=>{
    const data = [
        {
            case_study: [
                {
                    id: 1,
                    thumbnail: 'https://i.ibb.co/GnLdypF/fullstack-1.png',
                    title: 'Aplikasi Todo Management'
                },
                {
                    id: 2,
                    thumbnail: 'https://i.ibb.co/7yTMGVh/fullstack-2.png',
                    title: 'Website Blog Artikel'
                },
                {
                    id: 3,
                    thumbnail: 'https://i.ibb.co/TwPtCj4/fullstack-3.png',
                    title: 'Website Toko Online'
                },
                {
                    id: 4,
                    thumbnail: 'https://i.ibb.co/XCt04nV/fullstack-4.png',
                    title: 'Sistem Perpustakaan'
                }, 
            ],
            technology: [
                {
                    id: 1,
                    thumbnail: '/assets/img/program/icons/box-icons/icon-html5.png',
                    title: 'HTML & CSS',
                    description: 'Belajar dari dasar hingga mahir'
                },
                {
                    id: 2,
                    thumbnail: '/assets/img/program/icons/box-icons/icon-bootstrap.png',
                    title: 'Bootstrap',
                    description: 'Belajar dari dasar hingga mahir'
                },
                {
                    id: 3,
                    thumbnail: '/assets/img/program/icons/box-icons/icon-mysql.png',
                    title: 'MySQL',
                    description: 'Belajar dari dasar hingga mahir'
                },
                {
                    id: 4,
                    thumbnail: '/assets/img/program/icons/box-icons/icon-php.png',
                    title: 'PHP',
                    description: 'Belajar dari dasar hingga mahir'
                },
                {
                    id: 5,
                    thumbnail: '/assets/img/program/icons/box-icons/icon-codeigniter.png',
                    title: 'Codeigniter',
                    description: 'Belajar dari dasar hingga mahir'
                },
                {
                    id: 6,
                    thumbnail: '/assets/img/program/icons/box-icons/icon-laravel.png',
                    title: 'Laravel',
                    description: 'Belajar dari dasar hingga mahir'
                },
                {
                    id: 9,
                    thumbnail: '/assets/img/program/icons/box-icons/icon-jquery.png',
                    title: 'JQuery',
                    description: 'Belajar dari dasar hingga mahir'
                },
                {
                    id: 10,
                    thumbnail: '/assets/img/program/icons/box-icons/icon-js.png',
                    title: 'JavaScript',
                    description: 'Belajar dari dasar hingga mahir'
                },
                {
                    id: 12,
                    thumbnail: '/assets/img/program/icons/box-icons/icon-vue.png',
                    title: 'Vue',
                    description: 'Belajar dari dasar hingga mahir'
                }, 
            ],
            faq: [
                {
                    id: 'flush-heading1',
                    target: 'flush-collapse1',
                    title: 'Apa yang dimaksud dengan fullstack web development?',
                    content: 'Fullstack web development berarti pengembangan aplikasi web yang mencakup pemrograman sisi server dan sisi client. Artinya Kamu akan belajar mulai dari dasar logika pemrograman untuk dijalankan di sisi server, managemen database, sampai penampilan antarmuka aplikasi yang berjalan di browser pengguna.'
                },
                {
                    id: 'flush-heading2',
                    target: 'flush-collapse2',
                    title: 'Bahasa pemrograman apa saja yang dipelajari?',
                    content: 'Untuk pemrograman sisi server Kamu akan belajar bahasa pemrograman PHP. Untuk pemrograman di sisi client Kamu akan belajar bahasa pemrograman JavaScript, dan juga HTML CSS. Kamu juga akan belajar dasar-dasar MySQL untuk managemen database.'
                },
                {
                    id: 'flush-heading3',
                    target: 'flush-collapse3',
                    title: 'Framework PHP apa yang digunakan dalam pembelajaran?',
                    content: 'Kami membahas mulai dari PHP native dan juga framework Laravel versi 8 dan CodeIgniter versi 3. Di sisi client kami membahas dasar-dasar javascript, library jQuery, dan juga framework VueJS.'
                },
                {
                    id: 'flush-heading4',
                    target: 'flush-collapse4',
                    title: 'Apakah saya dapat menjadi fullstack programmer dalam waktu 6 bulan?',
                    content: 'Kamu harus mengikuti semua materi teori yang disajikan agar mendapatkan konsep dan fundamental pemrograman. Kamu juga harus mencoba mempraktekkan setiap kelas studi kasus untuk mendapatkan *best-practice* di dunia pemrograman. Dengan demikian in syaa Allah Kamu akan menjadi fullstack web programmer dalam waktu 4-6 bulan bahkan lebih cepat tergantung kecepatan belajarmu.'
                },
                {
                    id: 'flush-heading5',
                    target: 'flush-collapse5',
                    title: 'Kapan pertemuan materi disampaikan oleh mentor?',
                    content: 'Mentor kami telah membuat rangkaian video yang berisi pembahasan materi. Artinya Anda dapat mempelajari kapanpun video tersebut tanpa batas waktu.'
                },
                {
                    id: 'flush-heading6',
                    target: 'flush-collapse6',
                    title: 'Alat apa saja yang diperlukan untuk belajar?',
                    content: 'Anda akan menggunakan laptop atau komputer yang terhubung ke internet. Setelah mendaftar program Anda akan langsung mendapat akses kelas untuk mulai belajar. Silakan ikuti materi dan praktekkan studi kasus yang disampaikan oleh mentor di video pembelajaran tersebut.'
                },
                {
                    id: 'flush-heading7',
                    target: 'flush-collapse7',
                    title: 'Kalau ada materi yang membingungkan bagaimana solusinya?',
                    content: 'Anda dapat langsung bertanya pada bagian diskusi di halaman materi yang sedang Anda pelajari. Tanyakan bagian mana yang membuat Anda kurang paham. Mentor kami akan membantu menjawab persoalan Anda melalui kanal diskusi tersebut.'
                },
                {
                    id: 'flush-heading8',
                    target: 'flush-collapse8',
                    title: 'Bisakah saya bertanya langsung via tatap muka dengan mentor?',
                    content: 'Untuk saat ini belum. In syaa Allah kedepannya kami akan menyediakan layanan bimbingan privat untuk belajar pemrograman secara tatap muka online dan realtime bersama mentor.'
                }, 
            ]
        }
    ];
    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
        children: [
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)((next_head__WEBPACK_IMPORTED_MODULE_2___default()), {
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("title", {
                        children: "Fullstack Developer - Codepolitan"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        name: "title",
                        content: "Bingung Cari Kelas Online Fullstack Developer ?"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        name: "description",
                        content: "Apapun latar belakang kamu, melalui kelas online ini kamu akan belajar terarah dari nol hingga menjadi professional fullstack developer bisa meraih Impianmu menjadi fullstack developer dalam 6 bulan"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:type",
                        content: "website"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:url",
                        content: "https://codepolitan.com/program/fullstack-developer"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:title",
                        content: "Bingung Cari Kelas Online Fullstack Developer ?"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:description",
                        content: "Apapun latar belakang kamu, melalui kelas online ini kamu akan belajar terarah dari nol hingga menjadi professional fullstack developer bisa meraih Impianmu menjadi fullstack developer dalam 6 bulan"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:image",
                        content: "/assets/img/program/hero-fullstack.png"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:card",
                        content: "summary_large_image"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:url",
                        content: "https://codepolitan.com/program/fullstack-developer"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:title",
                        content: "Bingung Cari Kelas Online Fullstack Developer ?"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:description",
                        content: "Apapun latar belakang kamu, melalui kelas online ini kamu akan belajar terarah dari nol hingga menjadi professional fullstack developer bisa meraih Impianmu menjadi fullstack developer dalam 6 bulan"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:image",
                        content: "/assets/img/program/hero-fullstack.png"
                    })
                ]
            }),
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_components_global_Layout_Layout__WEBPACK_IMPORTED_MODULE_3__/* ["default"] */ .Z, {
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_program_Hero_Hero__WEBPACK_IMPORTED_MODULE_8__/* ["default"] */ .Z, {
                        heroTitle: "Bingung Cari Kelas Online Fullstack Developer ?",
                        heroSubtitle: "Apapun latar belakang kamu, melalui kelas online ini kamu akan belajar terarah dari nol hingga menjadi professional fullstack developer bisa meraih Impianmu menjadi fullstack developer dalam 6 bulan",
                        heroImg: "/assets/img/program/hero-fullstack.png",
                        heroBgColor: "bg-light",
                        heroFontColor: "text-dark",
                        heroBtnColor: "btn-outline-secondary"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_program_AboutSection_AboutSection__WEBPACK_IMPORTED_MODULE_15__/* ["default"] */ .Z, {
                        thumbnail: "/assets/img/program/fullstack-img.png",
                        title: "Fullstack Web Development",
                        description: "Dalam kelas ini kamu akan belajar dari awal hingga menjadi professional fullstack developer selama 1 semester. Melalui kelas ini, kamu yang belum mahir sekalipun dalam bidang pemrograman akan bisa membuat aplikasi berbasis web yang super keren dan memiliki nilai jual yang tinggi loh."
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_program_StudycaseSection_StudycaseSection__WEBPACK_IMPORTED_MODULE_10__/* ["default"] */ .Z, {
                        data: data[0].case_study
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("section", {
                        className: "section",
                        style: {
                            backgroundColor: '#eee'
                        },
                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                            className: "container p-4 p-lg-5",
                            children: [
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "row",
                                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        className: "col text-center",
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h3", {
                                                className: "section-title",
                                                children: "Apa yang Akan Kamu Pelajari"
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                className: "text-muted",
                                                children: "Yang akan kamu pelajari dalam kelas ini"
                                            })
                                        ]
                                    })
                                }),
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "row mt-3",
                                    children: data[0].technology.map((item)=>{
                                        return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: "col-md-4 mb-3",
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                className: "card border-0 shadow-sm",
                                                style: {
                                                    borderRadius: '15px'
                                                },
                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                    className: "card-body p-3",
                                                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                        className: "row text-muted",
                                                        children: [
                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                                className: "col-4 my-auto",
                                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                                    className: "img-fluid rounded",
                                                                    src: item.thumbnail,
                                                                    alt: item.title
                                                                })
                                                            }),
                                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                                className: "col-8 mt-2",
                                                                children: [
                                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h5", {
                                                                        style: {
                                                                            fontSize: 'medium'
                                                                        },
                                                                        children: item.title
                                                                    }),
                                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                                        style: {
                                                                            fontSize: 'small'
                                                                        },
                                                                        children: item.description
                                                                    })
                                                                ]
                                                            })
                                                        ]
                                                    })
                                                })
                                            })
                                        }, item.id));
                                    })
                                })
                            ]
                        })
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("section", {
                        className: "section bg-codepolitan text-white",
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "container p-4 p-lg-5",
                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                className: "row justify-content-center text-center",
                                children: [
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "col-6 col-md-4 my-2",
                                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("p", {
                                            className: "text-white mb-0",
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                    className: `${(_styles_Program_module_scss__WEBPACK_IMPORTED_MODULE_16___default().count)} text-white`,
                                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((react_countup__WEBPACK_IMPORTED_MODULE_14___default()), {
                                                        end: 64,
                                                        duration: 5
                                                    })
                                                }),
                                                " Kelas Online"
                                            ]
                                        })
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "col-6 col-md-4 my-2",
                                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("p", {
                                            className: "text-white mb-0",
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                    className: `${(_styles_Program_module_scss__WEBPACK_IMPORTED_MODULE_16___default().count)} text-white`,
                                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((react_countup__WEBPACK_IMPORTED_MODULE_14___default()), {
                                                        end: 140191,
                                                        duration: 5
                                                    })
                                                }),
                                                " Member Aktif"
                                            ]
                                        })
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "col-6 col-md-4 my-2",
                                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("p", {
                                            className: "text-white mb-0",
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                    className: `${(_styles_Program_module_scss__WEBPACK_IMPORTED_MODULE_16___default().count)} text-white`,
                                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((react_countup__WEBPACK_IMPORTED_MODULE_14___default()), {
                                                        end: 1820,
                                                        duration: 5
                                                    })
                                                }),
                                                " Materi Belajar"
                                            ]
                                        })
                                    })
                                ]
                            })
                        })
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_program_WaktuBelajarSection_WaktuBelajarSection__WEBPACK_IMPORTED_MODULE_12__/* ["default"] */ .Z, {}),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_program_ForumSection_ForumSection__WEBPACK_IMPORTED_MODULE_7__/* ["default"] */ .Z, {}),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("section", {
                        className: "section",
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "container p-4 p-lg-5",
                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                className: "row",
                                children: [
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        className: "col-lg-5 offset-lg-1 my-auto",
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
                                                className: "section-title",
                                                children: "Online Mentoring"
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                className: "text-muted my-3",
                                                children: "Dapatkan kesempatan 12x berdiskusi dengan mentor kelas fullstack developer dalam sesi mentoring sebanyak 2x dalam 1 bulan, supaya kamu dapat belajar dengan terarah."
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "col-lg-6 order-lg-first",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                            className: "img-fluid",
                                            src: "/assets/img/program/discord-img-3.png",
                                            alt: "Online Mentoring"
                                        })
                                    })
                                ]
                            })
                        })
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_program_CertificateSection_CertificateSection__WEBPACK_IMPORTED_MODULE_4__/* ["default"] */ .Z, {}),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("section", {
                        className: "section bg-light",
                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                            className: "container text-center p-4 p-lg-5",
                            children: [
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "row my-3",
                                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        className: "col",
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
                                                className: "section-title",
                                                children: "Mentor Belajar"
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                className: "text-muted",
                                                children: "Dalam program ini kamu akan dibimbing oleh para mentor profesional yang berpengalaman"
                                            })
                                        ]
                                    })
                                }),
                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                    className: "row",
                                    children: [
                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                            className: "col-6 col-lg-3 mb-3",
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                    className: "img-fluid",
                                                    src: "/assets/img/program/mentor/ahmad-oriza.png",
                                                    alt: "Ahmad Oriza"
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h5", {
                                                    className: "mt-4",
                                                    children: "Ahmad Oriza"
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                    className: "text-muted",
                                                    children: "Fullstack Developer"
                                                })
                                            ]
                                        }),
                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                            className: "col-6 col-lg-3 mb-3",
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                    className: "img-fluid",
                                                    src: "/assets/img/program/mentor/galih-pratama.png",
                                                    alt: "Galih Pratama"
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h5", {
                                                    className: "mt-4",
                                                    children: "Galih Pratama"
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                    className: "text-muted",
                                                    children: "Fullstack Developer"
                                                })
                                            ]
                                        }),
                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                            className: "col-6 col-lg-3 mb-3",
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                    className: "img-fluid",
                                                    src: "/assets/img/program/mentor/hakim-sembiring.png",
                                                    alt: "Hakim Sembiring"
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h5", {
                                                    className: "mt-4",
                                                    children: "Hakim Sembiring"
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                    className: "text-muted",
                                                    children: "Fullstack Developer"
                                                })
                                            ]
                                        }),
                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                            className: "col-6 col-lg-3 mb-3",
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                    className: "img-fluid",
                                                    src: "/assets/img/program/mentor/tony-haryanto.png",
                                                    alt: "Tony Haryanto"
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h5", {
                                                    className: "mt-4",
                                                    children: "Tony Haryanto"
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                    className: "text-muted",
                                                    children: "Fullstack Developer"
                                                })
                                            ]
                                        })
                                    ]
                                })
                            ]
                        })
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_program_HowToLearnSection_HowToLearnSection__WEBPACK_IMPORTED_MODULE_9__/* ["default"] */ .Z, {}),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_program_TestimonySection_TestimonySection__WEBPACK_IMPORTED_MODULE_11__/* ["default"] */ .Z, {
                        data: testimony
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_program_CtaSection_CtaSection__WEBPACK_IMPORTED_MODULE_5__/* ["default"] */ .Z, {
                        title: "Tunggu Apalagi ? Ayo Gabung Program Belajar Fullstack Developer 6 Bulan Untuk Meraih Karirmu !",
                        priceImg: "/assets/img/program/price-fullstack.png",
                        actionLink: "https://pay.codepolitan.com/?slug=program-fullstack-web-development"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: "bg-light",
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_global_Sections_WarrantySection_WarrantySection__WEBPACK_IMPORTED_MODULE_13__/* ["default"] */ .Z, {})
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_program_FaqSection_FaqSection__WEBPACK_IMPORTED_MODULE_6__/* ["default"] */ .Z, {
                        data: data[0].faq
                    })
                ]
            })
        ]
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (FullstackLanding);

});

/***/ }),

/***/ 5368:
/***/ ((module) => {

module.exports = require("@fortawesome/free-brands-svg-icons");

/***/ }),

/***/ 197:
/***/ ((module) => {

module.exports = require("@fortawesome/free-regular-svg-icons");

/***/ }),

/***/ 6466:
/***/ ((module) => {

module.exports = require("@fortawesome/free-solid-svg-icons");

/***/ }),

/***/ 7197:
/***/ ((module) => {

module.exports = require("@fortawesome/react-fontawesome");

/***/ }),

/***/ 2167:
/***/ ((module) => {

module.exports = require("axios");

/***/ }),

/***/ 2733:
/***/ ((module) => {

module.exports = require("fuse.js");

/***/ }),

/***/ 562:
/***/ ((module) => {

module.exports = require("next/dist/server/denormalize-page-path.js");

/***/ }),

/***/ 8028:
/***/ ((module) => {

module.exports = require("next/dist/server/image-config.js");

/***/ }),

/***/ 4957:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/head.js");

/***/ }),

/***/ 4014:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/i18n/normalize-locale-path.js");

/***/ }),

/***/ 8524:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/is-plain-object.js");

/***/ }),

/***/ 8020:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/mitt.js");

/***/ }),

/***/ 4964:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router-context.js");

/***/ }),

/***/ 9565:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/get-asset-path-from-route.js");

/***/ }),

/***/ 4365:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/get-middleware-regex.js");

/***/ }),

/***/ 1428:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/is-dynamic.js");

/***/ }),

/***/ 1292:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/parse-relative-url.js");

/***/ }),

/***/ 979:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/querystring.js");

/***/ }),

/***/ 6052:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/resolve-rewrites.js");

/***/ }),

/***/ 4226:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/route-matcher.js");

/***/ }),

/***/ 5052:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/route-regex.js");

/***/ }),

/***/ 9232:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/utils.js");

/***/ }),

/***/ 968:
/***/ ((module) => {

module.exports = require("next/head");

/***/ }),

/***/ 1853:
/***/ ((module) => {

module.exports = require("next/router");

/***/ }),

/***/ 6689:
/***/ ((module) => {

module.exports = require("react");

/***/ }),

/***/ 609:
/***/ ((module) => {

module.exports = require("react-countup");

/***/ }),

/***/ 7386:
/***/ ((module) => {

module.exports = require("react-simple-star-rating");

/***/ }),

/***/ 997:
/***/ ((module) => {

module.exports = require("react/jsx-runtime");

/***/ }),

/***/ 3877:
/***/ ((module) => {

module.exports = import("swiper");;

/***/ }),

/***/ 3015:
/***/ ((module) => {

module.exports = import("swiper/react");;

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [7730,9285,1664,5675,4528,2068,4373,7838,1653,4272,6410,9504], () => (__webpack_exec__(7638)));
module.exports = __webpack_exports__;

})();