"use strict";
(() => {
var exports = {};
exports.id = 4223;
exports.ids = [4223];
exports.modules = {

/***/ 3627:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__) => {
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "getServerSideProps": () => (/* binding */ getServerSideProps),
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(2167);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(968);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _components_program_Hero_Hero__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(1653);
/* harmony import */ var _components_global_Layout_Layout__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(4528);
/* harmony import */ var _components_program_WaktuBelajarSection_WaktuBelajarSection__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(420);
/* harmony import */ var _components_program_VideoSection_VideoSection__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(2258);
/* harmony import */ var _components_program_CourselistSection_CourselistSection__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(8052);
/* harmony import */ var _components_program_ForumSection_ForumSection__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(4579);
/* harmony import */ var _components_program_CertificateSection_CertificateSection__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(2068);
/* harmony import */ var _components_program_MentorSection_MentorSection__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(5341);
/* harmony import */ var _components_program_HowToLearnSection_HowToLearnSection__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(3656);
/* harmony import */ var _components_program_TestimonySection_TestimonySection__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(1533);
/* harmony import */ var _components_program_CtaSection_CtaSection__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(6916);
/* harmony import */ var _components_program_FaqSection_FaqSection__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(9982);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(7197);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_16__);
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(6466);
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(_fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_17__);
/* harmony import */ var _utils_helper__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(4986);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_components_program_TestimonySection_TestimonySection__WEBPACK_IMPORTED_MODULE_13__]);
_components_program_TestimonySection_TestimonySection__WEBPACK_IMPORTED_MODULE_13__ = (__webpack_async_dependencies__.then ? await __webpack_async_dependencies__ : __webpack_async_dependencies__)[0];



















const getServerSideProps = async ()=>{
    const courseDetailRes = await axios__WEBPACK_IMPORTED_MODULE_2___default().get(`https://api.codepolitan.com/course/detail/membuat-game-tanpa-coding-dengan-construct-3`);
    const testimonyRes = await axios__WEBPACK_IMPORTED_MODULE_2___default().get(`https://apps.codepolitan.com/api/feedback/course/membuat-game-tanpa-coding-dengan-construct-3`);
    const [courseDetail, testimony] = await Promise.all([
        courseDetailRes.data,
        testimonyRes.data
    ]);
    return {
        props: {
            courseDetail,
            testimony
        }
    };
};
const ConstructLanding = ({ courseDetail , testimony  })=>{
    const lessons = courseDetail.lessons;
    const lessonList = (0,_utils_helper__WEBPACK_IMPORTED_MODULE_18__/* .getLessons */ .tj)(lessons);
    const lessonTopic = lessonList.lessonTopic;
    const lessonContent = lessonList.lessonContent;
    const { 0: dataFaq  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)([
        {
            id: 'flush-heading1',
            target: 'flush-collapse1',
            title: 'Alat apa saja yang diperlukan untuk belajar?',
            content: 'Anda akan menggunakan laptop atau komputer yang terhubung ke internet. Setelah mendaftar program Anda akan langsung mendapat akses kelas untuk mulai belajar. Silakan ikuti materi dan praktekkan studi kasus yang disampaikan oleh mentor di video pembelajaran tersebut.'
        }, 
    ]);
    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
        children: [
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)((next_head__WEBPACK_IMPORTED_MODULE_3___default()), {
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("title", {
                        children: "Construct 3 - Codepolitan"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        name: "title",
                        content: "Bikin Game 1 Jam ? Mudah dan Gak Harus Ngoding"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        name: "description",
                        content: "Melalui kelas ini, bukan hanya jadi gamer kamu juga bisa jadi game developer yang dapat membuat game super keren dan memiliki nilai jual tanpa harus jago coding !"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:type",
                        content: "website"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:url",
                        content: "https://codepolitan.com/program/construct-3"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:title",
                        content: "Bikin Game 1 Jam ? Mudah dan Gak Harus Ngoding"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:description",
                        content: "Melalui kelas ini, bukan hanya jadi gamer kamu juga bisa jadi game developer yang dapat membuat game super keren dan memiliki nilai jual tanpa harus jago coding !"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:image",
                        content: "/assets/img/program/thumbnail/thumbnail-construct-3.png"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:card",
                        content: "summary_large_image"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:url",
                        content: "https://codepolitan.com/program/construct-3"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:title",
                        content: "Bikin Game 1 Jam ? Mudah dan Gak Harus Ngoding"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:description",
                        content: "Melalui kelas ini, bukan hanya jadi gamer kamu juga bisa jadi game developer yang dapat membuat game super keren dan memiliki nilai jual tanpa harus jago coding !"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:image",
                        content: "/assets/img/program/thumbnail/thumbnail-construct-3.png"
                    })
                ]
            }),
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_components_global_Layout_Layout__WEBPACK_IMPORTED_MODULE_5__/* ["default"] */ .Z, {
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_program_Hero_Hero__WEBPACK_IMPORTED_MODULE_4__/* ["default"] */ .Z, {
                        heroTitle: "Bikin Game 1 Jam ? Mudah dan Gak Harus Ngoding",
                        heroSubtitle: "Melalui kelas ini, bukan hanya jadi gamer kamu juga bisa jadi game developer yang dapat membuat game super keren dan memiliki nilai jual tanpa harus jago coding !",
                        heroImg: "/assets/img/program/hero-construct-3.webp",
                        heroBgColor: "bg-light",
                        heroFontColor: "text-dark",
                        heroBtnColor: "btn-outline-secondary"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("section", {
                        id: "about",
                        className: "section",
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "container p-5",
                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                className: "row justify-content-between",
                                children: [
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        className: "col-lg-6 my-5",
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
                                                className: "section-title",
                                                children: "Tentang Kelas"
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                className: "text-muted my-3",
                                                children: "Kelas ditujukan bagi siapa saja yang ingin menjadi game developer dengan menggunakan tools construct 3. kamu bisa mendesign dan membuat game puzzle, Tembak-Tembakan, balapan , hingga RPG dan layak untuk dimainkan oleh siapapun, sehingga game yang kamu buat dapat diupload ke apps store atau playstore dengan nilai jual yang tinggi."
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                className: "text-muted",
                                                children: "Tools yang digunakan:"
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                height: "45",
                                                src: "https://cursos.dankicode.com/app/Views/public/mkt/curso-construct-3/images/logo-hero.png",
                                                alt: ""
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "col-lg-5 my-3",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                            className: "img-fluid d-block mx-auto",
                                            src: "/assets/img/program/mascot.png",
                                            alt: "Construct Codepolitan"
                                        })
                                    })
                                ]
                            })
                        })
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("section", {
                        className: "section",
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "container-fluid",
                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                className: "row",
                                children: [
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "col-md-6 bg-dark",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: "py-5",
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                className: "img-fluid",
                                                src: "/assets/img/program/construct.png",
                                                alt: "Mockup"
                                            })
                                        })
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "col-md-6 p-5 bg-light",
                                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                            className: "py-lg-5",
                                            children: [
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
                                                    className: "section-title",
                                                    children: "Belajar Dengan Studi Kasus"
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                    className: "text-muted my-3",
                                                    children: "Disini kami membuat game shooter yang bisa dimainkan pada perangkat mobile dan bisa diakses secara online, dengan mengikuti kelas ini kamu juga bisa bikin game shooter seperti kami atau bahkan lebih keren lagi. kamu bisa lihat hasilnya disini."
                                                }),
                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("a", {
                                                    className: "btn btn-primary btn-lg mt-3",
                                                    href: "http://zombierun.surge.sh/",
                                                    target: "_blank",
                                                    rel: "noopener noreferrer",
                                                    children: "Lihat demo"
                                                })
                                            ]
                                        })
                                    })
                                ]
                            })
                        })
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_program_WaktuBelajarSection_WaktuBelajarSection__WEBPACK_IMPORTED_MODULE_6__/* ["default"] */ .Z, {
                        order: "order-lg-last"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_program_VideoSection_VideoSection__WEBPACK_IMPORTED_MODULE_7__/* ["default"] */ .Z, {
                        videoSrc: "https://www.youtube.com/embed/iZTVB-xHlgU"
                    }),
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_components_program_CourselistSection_CourselistSection__WEBPACK_IMPORTED_MODULE_8__/* ["default"] */ .Z, {
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("ul", {
                                className: "nav nav-pills d-grid d-md-flex nav-justified bg-light mb-3",
                                id: "pills-tab",
                                role: "tablist",
                                children: lessonTopic.map((item, index)=>{
                                    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                                        className: "nav-item",
                                        role: "presentation",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                            className: `nav-link py-3 ${index < 1 && 'active'}`,
                                            id: item.topic_slug.slice(3) + '-tab',
                                            "data-bs-toggle": "pill",
                                            "data-bs-target": '#' + item.topic_slug.slice(3),
                                            type: "button",
                                            role: "tab",
                                            "aria-controls": item.topic_slug.slice(3),
                                            "aria-selected": "true",
                                            title: item.topic_title,
                                            children: item.topic_title.slice(0, 20) + '...'
                                        })
                                    }, index));
                                })
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: "tab-content",
                                id: "pills-tabContent",
                                children: lessonContent.map((content, index1)=>{
                                    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: `tab-pane fade show ${index1 < 1 && 'active'}`,
                                        id: content.topic_slug.slice(3),
                                        role: "tabpanel",
                                        "aria-labelledby": content.topic_slug.slice(3) + '-tab',
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("table", {
                                            className: "table table-striped",
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("tbody", {
                                                children: content.contents?.map((item, index)=>{
                                                    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("tr", {
                                                        children: [
                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("td", {
                                                                style: {
                                                                    width: '50px'
                                                                },
                                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                                    height: "30",
                                                                    src: "/assets/img/program/play-icon.png",
                                                                    alt: "Icon"
                                                                })
                                                            }),
                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("td", {
                                                                className: "text-muted",
                                                                children: item.lesson_title
                                                            }),
                                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("td", {
                                                                className: "text-end text-muted",
                                                                children: [
                                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_16__.FontAwesomeIcon, {
                                                                        icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_17__.faLock,
                                                                        className: "me-2"
                                                                    }),
                                                                    item.duration
                                                                ]
                                                            })
                                                        ]
                                                    }, index));
                                                })
                                            })
                                        })
                                    }, index1));
                                })
                            })
                        ]
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_program_ForumSection_ForumSection__WEBPACK_IMPORTED_MODULE_9__/* ["default"] */ .Z, {}),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_program_CertificateSection_CertificateSection__WEBPACK_IMPORTED_MODULE_10__/* ["default"] */ .Z, {}),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_program_MentorSection_MentorSection__WEBPACK_IMPORTED_MODULE_11__/* ["default"] */ .Z, {
                        name: "Ahmad Hakim",
                        title: "Mentor kelas Construct 3",
                        description: "Saya Ahmad Hakim, seorang Full Stack Developer dan Senior Programmer di Codepolitan. Dengan pengalaman saya lebih dari 6 tahun berkarya dan berkarir di bidang programming akan membantu kamu belajar dalam kelas ini. Apapun latar belakang kamu, melalui kelas ini kamu akan belajar membuat website perusahaan sendiri yang super keren. Saya tunggu di kelas!",
                        img: "/assets/img/program/hakim.png"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_program_HowToLearnSection_HowToLearnSection__WEBPACK_IMPORTED_MODULE_12__/* ["default"] */ .Z, {}),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_program_TestimonySection_TestimonySection__WEBPACK_IMPORTED_MODULE_13__/* ["default"] */ .Z, {
                        data: testimony
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_program_CtaSection_CtaSection__WEBPACK_IMPORTED_MODULE_14__/* ["default"] */ .Z, {
                        title: "Tunggu Apalagi ? Ayo Gabung Kelas Bikin Web Tanpa Koding Sekarang !",
                        priceImg: "/assets/img/program/price-construct-3.png",
                        actionLink: "https://pay.codepolitan.com/?slug=membuat-game-tanpa-coding-dengan-construct-3"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_program_FaqSection_FaqSection__WEBPACK_IMPORTED_MODULE_15__/* ["default"] */ .Z, {
                        data: dataFaq
                    })
                ]
            })
        ]
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ConstructLanding);

});

/***/ }),

/***/ 5368:
/***/ ((module) => {

module.exports = require("@fortawesome/free-brands-svg-icons");

/***/ }),

/***/ 197:
/***/ ((module) => {

module.exports = require("@fortawesome/free-regular-svg-icons");

/***/ }),

/***/ 6466:
/***/ ((module) => {

module.exports = require("@fortawesome/free-solid-svg-icons");

/***/ }),

/***/ 7197:
/***/ ((module) => {

module.exports = require("@fortawesome/react-fontawesome");

/***/ }),

/***/ 2167:
/***/ ((module) => {

module.exports = require("axios");

/***/ }),

/***/ 2733:
/***/ ((module) => {

module.exports = require("fuse.js");

/***/ }),

/***/ 2245:
/***/ ((module) => {

module.exports = require("moment");

/***/ }),

/***/ 5518:
/***/ ((module) => {

module.exports = require("moment/locale/id");

/***/ }),

/***/ 562:
/***/ ((module) => {

module.exports = require("next/dist/server/denormalize-page-path.js");

/***/ }),

/***/ 8028:
/***/ ((module) => {

module.exports = require("next/dist/server/image-config.js");

/***/ }),

/***/ 4957:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/head.js");

/***/ }),

/***/ 4014:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/i18n/normalize-locale-path.js");

/***/ }),

/***/ 8524:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/is-plain-object.js");

/***/ }),

/***/ 8020:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/mitt.js");

/***/ }),

/***/ 4964:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router-context.js");

/***/ }),

/***/ 9565:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/get-asset-path-from-route.js");

/***/ }),

/***/ 4365:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/get-middleware-regex.js");

/***/ }),

/***/ 1428:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/is-dynamic.js");

/***/ }),

/***/ 1292:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/parse-relative-url.js");

/***/ }),

/***/ 979:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/querystring.js");

/***/ }),

/***/ 6052:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/resolve-rewrites.js");

/***/ }),

/***/ 4226:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/route-matcher.js");

/***/ }),

/***/ 5052:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/route-regex.js");

/***/ }),

/***/ 9232:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/utils.js");

/***/ }),

/***/ 968:
/***/ ((module) => {

module.exports = require("next/head");

/***/ }),

/***/ 1853:
/***/ ((module) => {

module.exports = require("next/router");

/***/ }),

/***/ 6689:
/***/ ((module) => {

module.exports = require("react");

/***/ }),

/***/ 7386:
/***/ ((module) => {

module.exports = require("react-simple-star-rating");

/***/ }),

/***/ 997:
/***/ ((module) => {

module.exports = require("react/jsx-runtime");

/***/ }),

/***/ 3877:
/***/ ((module) => {

module.exports = import("swiper");;

/***/ }),

/***/ 3015:
/***/ ((module) => {

module.exports = import("swiper/react");;

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [7730,9285,1664,5675,4528,4986,2068,4373,7838,1653,5341,156], () => (__webpack_exec__(3627)));
module.exports = __webpack_exports__;

})();