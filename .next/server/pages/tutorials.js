"use strict";
(() => {
var exports = {};
exports.id = 8204;
exports.ids = [8204];
exports.modules = {

/***/ 1207:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ tutorials),
  "getStaticProps": () => (/* binding */ getStaticProps)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: external "axios"
var external_axios_ = __webpack_require__(2167);
var external_axios_default = /*#__PURE__*/__webpack_require__.n(external_axios_);
// EXTERNAL MODULE: external "next/head"
var head_ = __webpack_require__(968);
var head_default = /*#__PURE__*/__webpack_require__.n(head_);
// EXTERNAL MODULE: ./src/components/global/Layout/Layout.js + 5 modules
var Layout = __webpack_require__(4528);
// EXTERNAL MODULE: ./node_modules/next/link.js
var next_link = __webpack_require__(1664);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: external "@fortawesome/free-solid-svg-icons"
var free_solid_svg_icons_ = __webpack_require__(6466);
// EXTERNAL MODULE: external "@fortawesome/react-fontawesome"
var react_fontawesome_ = __webpack_require__(7197);
// EXTERNAL MODULE: ./src/utils/helper/index.js
var helper = __webpack_require__(4986);
;// CONCATENATED MODULE: ./src/components/global/Cards/CardFeaturedArticle/CardFeaturedArticle.js





const CardFeaturedArticle = ({ title , writer , createdAt , type , index  })=>{
    return(/*#__PURE__*/ jsx_runtime_.jsx("div", {
        className: "card border-0 p-1 h-100",
        children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
            className: "card-body",
            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                className: "d-flex gap-3",
                children: [
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                        className: "d-flex flex-column",
                        children: [
                            type === 'post' && /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                className: "badge bg-primary mb-2",
                                children: "Article"
                            }),
                            type === 'tutorial' && /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                className: "badge bg-danger mb-2",
                                children: "Tutorial"
                            }),
                            type === 'event' && /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                className: "badge bg-info mb-2",
                                children: "Event"
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                src: `/assets/img/article/0${index}.png`,
                                className: "mt-2",
                                alt: "Article"
                            })
                        ]
                    }),
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                        className: "d-flex flex-column",
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                className: "lead mb-0",
                                style: {
                                    fontSize: '1.1em'
                                },
                                children: title
                            }),
                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("small", {
                                className: "mt-2",
                                children: [
                                    /*#__PURE__*/ jsx_runtime_.jsx(react_fontawesome_.FontAwesomeIcon, {
                                        icon: free_solid_svg_icons_.faUser,
                                        className: "me-2"
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                        children: writer
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                        className: "mx-2",
                                        children: "-"
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                        children: (0,helper/* formatOnlyDate */.Yw)(createdAt)
                                    })
                                ]
                            })
                        ]
                    })
                ]
            })
        })
    }));
};
/* harmony default export */ const CardFeaturedArticle_CardFeaturedArticle = (CardFeaturedArticle);

;// CONCATENATED MODULE: ./src/components/global/Sections/SectionFeaturedArticle/SectionFeaturedArticle.js




const SectionFeaturedArticle = ({ data  })=>{
    const { 0: limit  } = (0,external_react_.useState)(6);
    return(/*#__PURE__*/ jsx_runtime_.jsx("section", {
        className: "bg-light",
        id: "featuredArticle",
        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
            className: "container p-4 p-lg-5 text-muted",
            children: [
                /*#__PURE__*/ jsx_runtime_.jsx("h4", {
                    className: "section-title mb-4",
                    children: "Artikel Pilihan"
                }),
                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                    className: "row",
                    children: [
                        data.error && /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: "col",
                            children: /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                children: "Belum ada artikel pilihan"
                            })
                        }),
                        !data.error && data?.map((item, index)=>{
                            return(/*#__PURE__*/ jsx_runtime_.jsx("div", {
                                className: "col-lg-4 mb-3",
                                children: /*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                                    href: `https://codepolitan.com/blog/${item.slug}`,
                                    children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                        className: "link",
                                        children: /*#__PURE__*/ jsx_runtime_.jsx(CardFeaturedArticle_CardFeaturedArticle, {
                                            index: index + 1,
                                            title: item.title,
                                            writer: item.writer,
                                            createdAt: item.created_at,
                                            type: item.type
                                        })
                                    })
                                })
                            }, index));
                        }).slice(0, limit)
                    ]
                })
            ]
        })
    }));
};
/* harmony default export */ const SectionFeaturedArticle_SectionFeaturedArticle = (SectionFeaturedArticle);

;// CONCATENATED MODULE: ./src/components/global/Sections/SectionBannerArticle/SectionBannerArticle.js


const SectionBannerArticle = ()=>{
    return(/*#__PURE__*/ jsx_runtime_.jsx("section", {
        className: "section",
        style: {
            marginTop: '70px'
        },
        id: "hero",
        children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
            className: "container p-4 p-lg-5 text-muted",
            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                className: "row justify-content-center align-items-center",
                children: [
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                        className: "col-lg-7 px-lg-5 order-last order-lg-first",
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx("h1", {
                                className: "mb-4 display-5",
                                children: "Tutorial dan Artikel"
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                className: "lead mb-5",
                                children: "Temukan tutorial pemrograman praktis dan tepat guna, serta informasi terbaru seputar pemrograman disini."
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                href: "#featuredArticle",
                                className: "btn btn-primary btn-rounded px-3 py-2",
                                children: "Mulai Membaca"
                            })
                        ]
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        className: "col-lg-4 text-center",
                        children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                            src: "/assets/img/article/banner-2.png",
                            className: "w-100",
                            alt: ""
                        })
                    })
                ]
            })
        })
    }));
};
/* harmony default export */ const SectionBannerArticle_SectionBannerArticle = (SectionBannerArticle);

;// CONCATENATED MODULE: ./src/components/global/Sections/SectionListArticle/SectionListArticle.js






const SectionListArticle = ({ data , labels  })=>{
    const { 0: limit , 1: setLimit  } = (0,external_react_.useState)(8);
    const { 0: searchValue , 1: setSearchValue  } = (0,external_react_.useState)("");
    return(/*#__PURE__*/ jsx_runtime_.jsx("section", {
        children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
            className: "container p-4 p-lg-5 text-muted",
            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                className: "row justify-content-between",
                children: [
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                        className: "col-lg-7 order-last order-lg-first mt-3 mt-lg-0",
                        children: [
                            data.filter((value)=>{
                                if (value.title.toLowerCase().includes(searchValue.toLowerCase())) {
                                    return value;
                                }
                            }).length > 0 ? data.filter((value)=>{
                                if (value.title.toLowerCase().includes(searchValue.toLowerCase())) {
                                    return value;
                                }
                            }).map((item, index)=>{
                                return(/*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                                    href: `https://codepolitan.com/blog/${item.slug}`,
                                    children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                        className: "link",
                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                            className: "row border-bottom mb-3 pb-3 align-items-center",
                                            children: [
                                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                    className: "col-lg-4",
                                                    children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                        src: item.featured_image || "/assets/img/placeholder.jpg",
                                                        className: "w-100",
                                                        alt: ""
                                                    })
                                                }),
                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: "col-lg-8",
                                                    children: [
                                                        item.type === 'post' && /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                            className: "badge bg-primary my-3",
                                                            children: "Article"
                                                        }),
                                                        item.type === 'tutorial' && /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                            className: "badge bg-danger my-3",
                                                            children: "Tutorial"
                                                        }),
                                                        item.type === 'event' && /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                            className: "badge bg-info my-3",
                                                            children: "Event"
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                            className: "lead mb-0",
                                                            children: item.title
                                                        }),
                                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("small", {
                                                            className: "my-3 d-block",
                                                            children: [
                                                                /*#__PURE__*/ jsx_runtime_.jsx(react_fontawesome_.FontAwesomeIcon, {
                                                                    icon: free_solid_svg_icons_.faUser,
                                                                    className: "me-2"
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                                    children: item.writer
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                                    className: "mx-2",
                                                                    children: "-"
                                                                }),
                                                                /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                                    children: (0,helper/* formatOnlyDate */.Yw)(item.created_at)
                                                                })
                                                            ]
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                            children: item.excerpt
                                                        })
                                                    ]
                                                })
                                            ]
                                        })
                                    })
                                }, index));
                            }).slice(0, limit) : /*#__PURE__*/ jsx_runtime_.jsx("h3", {
                                className: "text-center text-muted",
                                children: "Hasil pencarian tidak ditemukan.."
                            }),
                            data.filter((value)=>{
                                if (value.title.toLowerCase().includes(searchValue.toLowerCase())) {
                                    return value;
                                }
                            }).length > 0 && /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                className: "text-center",
                                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("button", {
                                    onClick: ()=>setLimit(limit + 8)
                                    ,
                                    className: "btn btn-sm btn-outline-secondary",
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx(react_fontawesome_.FontAwesomeIcon, {
                                            icon: free_solid_svg_icons_.faArrowRotateRight,
                                            className: "me-1"
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                            children: "Lihat Lainnya"
                                        })
                                    ]
                                })
                            })
                        ]
                    }),
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                        className: "col-lg-4",
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx("h4", {
                                className: "section-title mb-4",
                                children: "Temukan apa yang kamu cari disini"
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                children: labels.map((item, index)=>{
                                    return(/*#__PURE__*/ jsx_runtime_.jsx("span", {
                                        onClick: ()=>setSearchValue(item.term)
                                        ,
                                        style: {
                                            cursor: 'pointer'
                                        },
                                        className: "badge bg-light overflow-auto p-2 text-center text-muted text-dark me-2",
                                        children: item.term
                                    }, index));
                                }).slice(0, 12)
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                className: "mt-3",
                                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                    className: "input-group mb-3",
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                            onChange: (e)=>setSearchValue(e.target.value)
                                            ,
                                            value: searchValue,
                                            type: "text",
                                            className: "form-control border-0 border-bottom",
                                            placeholder: "Cari disini"
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                            className: "input-group-text bg-white border-0 border-bottom text-muted",
                                            id: "basic-addon1",
                                            children: /*#__PURE__*/ jsx_runtime_.jsx(react_fontawesome_.FontAwesomeIcon, {
                                                icon: free_solid_svg_icons_.faSearch
                                            })
                                        })
                                    ]
                                })
                            })
                        ]
                    })
                ]
            })
        })
    }));
};
/* harmony default export */ const SectionListArticle_SectionListArticle = (SectionListArticle);

;// CONCATENATED MODULE: ./src/pages/tutorials/index.js








const getStaticProps = async ()=>{
    const tutorialsFeaturedRes = await external_axios_default().get(`${"https://api.staging.codepolitan.com"}/v1/posts/popularWeekly?limit=10`);
    const tutorialsAllRes = await external_axios_default().get(`${"https://api.codepolitan.com"}/v1/posts/latest/post?page=1&limit=1000`);
    const labelsRes = await external_axios_default().get(`${"https://api.codepolitan.com"}/course/popular-labels`);
    const [tutorialsFeatured, tutorialsAll, labels] = await Promise.all([
        tutorialsFeaturedRes.data,
        tutorialsAllRes.data,
        labelsRes.data
    ]);
    return {
        props: {
            tutorialsFeatured,
            tutorialsAll,
            labels
        },
        revalidate: 10
    };
};
const Tutorials = ({ tutorialsFeatured , tutorialsAll , labels  })=>{
    (0,external_react_.useEffect)(()=>{
        document.getElementById('hero').scrollIntoView();
    }, []);
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
        children: [
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)((head_default()), {
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx("title", {
                        children: "Tutorial - Codepolitan"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        name: "title",
                        content: "Website Belajar Coding Bahasa Indonesia - Codepolitan"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        name: "description",
                        content: "Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "og:type",
                        content: "website"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "og:url",
                        content: "https://codepolitan.com"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "og:title",
                        content: "Website Belajar Coding Bahasa Indonesia - Codepolitan"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "og:description",
                        content: "Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "og:image",
                        content: "https://i.ibb.co/SXP8fGc/OG-image.jpg"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "twitter:card",
                        content: "summary_large_image"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "twitter:url",
                        content: "https://codepolitan.com"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "twitter:title",
                        content: "Website Belajar Coding Bahasa Indonesia - Codepolitan"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "twitter:description",
                        content: "Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "twitter:image",
                        content: "https://i.ibb.co/SXP8fGc/OG-image.jpg"
                    })
                ]
            }),
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)(Layout/* default */.Z, {
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx(SectionBannerArticle_SectionBannerArticle, {}),
                    /*#__PURE__*/ jsx_runtime_.jsx(SectionFeaturedArticle_SectionFeaturedArticle, {
                        data: tutorialsFeatured
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx(SectionListArticle_SectionListArticle, {
                        data: tutorialsAll,
                        labels: labels
                    })
                ]
            })
        ]
    }));
};
/* harmony default export */ const tutorials = (Tutorials);


/***/ }),

/***/ 5368:
/***/ ((module) => {

module.exports = require("@fortawesome/free-brands-svg-icons");

/***/ }),

/***/ 197:
/***/ ((module) => {

module.exports = require("@fortawesome/free-regular-svg-icons");

/***/ }),

/***/ 6466:
/***/ ((module) => {

module.exports = require("@fortawesome/free-solid-svg-icons");

/***/ }),

/***/ 7197:
/***/ ((module) => {

module.exports = require("@fortawesome/react-fontawesome");

/***/ }),

/***/ 2167:
/***/ ((module) => {

module.exports = require("axios");

/***/ }),

/***/ 2733:
/***/ ((module) => {

module.exports = require("fuse.js");

/***/ }),

/***/ 2245:
/***/ ((module) => {

module.exports = require("moment");

/***/ }),

/***/ 5518:
/***/ ((module) => {

module.exports = require("moment/locale/id");

/***/ }),

/***/ 562:
/***/ ((module) => {

module.exports = require("next/dist/server/denormalize-page-path.js");

/***/ }),

/***/ 4014:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/i18n/normalize-locale-path.js");

/***/ }),

/***/ 8524:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/is-plain-object.js");

/***/ }),

/***/ 8020:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/mitt.js");

/***/ }),

/***/ 4964:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router-context.js");

/***/ }),

/***/ 9565:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/get-asset-path-from-route.js");

/***/ }),

/***/ 4365:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/get-middleware-regex.js");

/***/ }),

/***/ 1428:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/is-dynamic.js");

/***/ }),

/***/ 1292:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/parse-relative-url.js");

/***/ }),

/***/ 979:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/querystring.js");

/***/ }),

/***/ 6052:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/resolve-rewrites.js");

/***/ }),

/***/ 4226:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/route-matcher.js");

/***/ }),

/***/ 5052:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/route-regex.js");

/***/ }),

/***/ 9232:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/utils.js");

/***/ }),

/***/ 968:
/***/ ((module) => {

module.exports = require("next/head");

/***/ }),

/***/ 1853:
/***/ ((module) => {

module.exports = require("next/router");

/***/ }),

/***/ 6689:
/***/ ((module) => {

module.exports = require("react");

/***/ }),

/***/ 997:
/***/ ((module) => {

module.exports = require("react/jsx-runtime");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [7730,9285,1664,4528,4986], () => (__webpack_exec__(1207)));
module.exports = __webpack_exports__;

})();