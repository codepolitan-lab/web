(() => {
var exports = {};
exports.id = 2268;
exports.ids = [2268];
exports.modules = {

/***/ 3097:
/***/ ((module) => {

// Exports
module.exports = {
	"card_leaderboard": "CardLeaderboard_card_leaderboard__GycJK",
	"card_body": "CardLeaderboard_card_body__W_6jJ",
	"card_img": "CardLeaderboard_card_img__JWlh2",
	"name": "CardLeaderboard_name__WqUph",
	"point": "CardLeaderboard_point__RMPA2",
	"point_caption": "CardLeaderboard_point_caption__vvPfA",
	"animated_background": "CardLeaderboard_animated_background__Qy7c8",
	"gradient": "CardLeaderboard_gradient__uwWEs"
};


/***/ }),

/***/ 2874:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ leaderboard),
  "getStaticProps": () => (/* binding */ getStaticProps)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: external "axios"
var external_axios_ = __webpack_require__(2167);
var external_axios_default = /*#__PURE__*/__webpack_require__.n(external_axios_);
// EXTERNAL MODULE: external "next/head"
var head_ = __webpack_require__(968);
var head_default = /*#__PURE__*/__webpack_require__.n(head_);
// EXTERNAL MODULE: ./src/components/global/Cards/CardLeaderboard/CardLeaderboard.module.scss
var CardLeaderboard_module = __webpack_require__(3097);
var CardLeaderboard_module_default = /*#__PURE__*/__webpack_require__.n(CardLeaderboard_module);
;// CONCATENATED MODULE: ./src/components/global/Cards/CardLeaderboard/CardLeaderboard.js


const CardLeaderboard = ({ index , thumbnail , name , rank , rankPicture , point  })=>{
    return(/*#__PURE__*/ jsx_runtime_.jsx("div", {
        className: `${(CardLeaderboard_module_default()).card_leaderboard} ${index === 0 && (CardLeaderboard_module_default()).animated_background} card border-0 shadow-sm my-3`,
        children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
            className: `card-body ${(CardLeaderboard_module_default()).card_body}`,
            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                className: "row",
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        className: "col-2 col-lg-1 text-center my-auto",
                        children: /*#__PURE__*/ jsx_runtime_.jsx("span", {
                            children: /*#__PURE__*/ jsx_runtime_.jsx("strong", {
                                children: index + 1
                            })
                        })
                    }),
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                        className: "col-1 text-center my-auto d-none d-lg-block",
                        children: [
                            index === 0 && /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                className: "img-fluid",
                                src: "https://i.ibb.co/P9q9Lbs/crown.png",
                                alt: "1st place"
                            }),
                            index === 1 && /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                className: "img-fluid",
                                src: "https://i.ibb.co/LptzNwd/crown-silver.png",
                                alt: "2nd place"
                            }),
                            index === 2 && /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                className: "img-fluid",
                                src: "https://i.ibb.co/HxK1hhV/crown-gold.png",
                                alt: "3rd place"
                            })
                        ]
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        className: "col-6",
                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: "d-flex align-items-start",
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                    className: `${(CardLeaderboard_module_default()).card_img} d-none d-lg-block`,
                                    src: thumbnail || '/assets/img/icons/icon-avatar.png',
                                    alt: name
                                }),
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                    className: "my-auto",
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx("h5", {
                                            className: (CardLeaderboard_module_default()).name,
                                            children: name
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                            className: "text-muted",
                                            children: rank
                                        })
                                    ]
                                })
                            ]
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        className: "col-1 text-center ms-auto d-none d-lg-block",
                        children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                            className: (CardLeaderboard_module_default()).card_img + ' p-2',
                            src: rankPicture || '/assets/img/placeholder.jpg',
                            alt: name
                        })
                    }),
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                        className: "col-3 col-lg-2 text-center my-auto ms-auto",
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                className: `${(CardLeaderboard_module_default()).point} ${index === 0 ? 'text-white' : 'text-primary'}`,
                                children: point
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                className: (CardLeaderboard_module_default()).point_caption,
                                children: "POINTS"
                            })
                        ]
                    })
                ]
            })
        })
    }));
};
/* harmony default export */ const CardLeaderboard_CardLeaderboard = (CardLeaderboard);

// EXTERNAL MODULE: ./src/components/global/Layout/Layout.js + 5 modules
var Layout = __webpack_require__(4528);
;// CONCATENATED MODULE: ./src/pages/leaderboard/index.js





const getStaticProps = async ()=>{
    const weeklyRes = await external_axios_default().get(`${"https://api.codepolitan.com"}/v1/points/leaderboard?page=1&period=week`);
    const monthlyRes = await external_axios_default().get(`${"https://api.codepolitan.com"}/v1/points/leaderboard?page=1&period=month`);
    const overallRes = await external_axios_default().get(`${"https://api.codepolitan.com"}/v1/points/leaderboard?page=1`);
    const [weekly, monthly, overall] = await Promise.all([
        weeklyRes.data,
        monthlyRes.data,
        overallRes.data
    ]);
    return {
        props: {
            weekly,
            monthly,
            overall
        },
        revalidate: 1000
    };
};
const Leaderboard = ({ weekly , monthly , overall  })=>{
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
        children: [
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)((head_default()), {
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx("title", {
                        children: "Leaderboard - Codepolitan"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        name: "title",
                        content: "Website Belajar Coding Bahasa Indonesia - Codepolitan"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        name: "description",
                        content: "Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "og:type",
                        content: "website"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "og:url",
                        content: "https://codepolitan.com"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "og:title",
                        content: "Website Belajar Coding Bahasa Indonesia - Codepolitan"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "og:description",
                        content: "Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "og:image",
                        content: "https://i.ibb.co/SXP8fGc/OG-image.jpg"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "twitter:card",
                        content: "summary_large_image"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "twitter:url",
                        content: "https://codepolitan.com"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "twitter:title",
                        content: "Website Belajar Coding Bahasa Indonesia - Codepolitan"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "twitter:description",
                        content: "Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan"
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("meta", {
                        property: "twitter:image",
                        content: "https://i.ibb.co/SXP8fGc/OG-image.jpg"
                    })
                ]
            }),
            /*#__PURE__*/ jsx_runtime_.jsx(Layout/* default */.Z, {
                children: /*#__PURE__*/ jsx_runtime_.jsx("section", {
                    className: "section",
                    style: {
                        margin: '65px 0'
                    },
                    children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                        className: "container p-4 p-lg-5",
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                className: "row mb-4",
                                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                    className: "col text-center",
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx("h2", {
                                            className: "section-title",
                                            children: "Top Learner Leaderboard"
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                            className: "text-muted",
                                            children: "Tingkatkan rankingmu dengan belajar di Kelas Online dan berkontribusi di Forum Tanya Jawab"
                                        })
                                    ]
                                })
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                className: "row",
                                children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                    className: "col",
                                    children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        children: [
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                                className: "nav nav-tabs nav-justified d-grid d-lg-flex",
                                                id: "BeasiswaTab",
                                                role: "tablist",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                        className: "nav-item",
                                                        role: "presentation",
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                                            className: "nav-link active",
                                                            id: "weekly-tab",
                                                            "data-bs-toggle": "tab",
                                                            "data-bs-target": "#weekly",
                                                            type: "button",
                                                            role: "tab",
                                                            "aria-controls": "weekly",
                                                            "aria-selected": "true",
                                                            children: "Weekly Rank"
                                                        })
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                        className: "nav-item",
                                                        role: "presentation",
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                                            className: "nav-link",
                                                            id: "monthly-tab",
                                                            "data-bs-toggle": "tab",
                                                            "data-bs-target": "#monthly",
                                                            type: "button",
                                                            role: "tab",
                                                            "aria-controls": "monthly",
                                                            "aria-selected": "false",
                                                            children: "Monthly Rank"
                                                        })
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                        className: "nav-item",
                                                        role: "presentation",
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                                            className: "nav-link",
                                                            id: "overall-tab",
                                                            "data-bs-toggle": "tab",
                                                            "data-bs-target": "#overall",
                                                            type: "button",
                                                            role: "tab",
                                                            "aria-controls": "overall",
                                                            "aria-selected": "false",
                                                            children: "Overall Rank"
                                                        })
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: "tab-content",
                                                id: "ForumTabContent",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                        className: "tab-pane fade show active",
                                                        id: "weekly",
                                                        role: "tabpanel",
                                                        "aria-labelledby": "weekly-tab",
                                                        children: weekly?.map((item, index)=>{
                                                            return(/*#__PURE__*/ jsx_runtime_.jsx(CardLeaderboard_CardLeaderboard, {
                                                                index: index,
                                                                thumbnail: item.avatar,
                                                                name: item.name,
                                                                rank: item.rank,
                                                                rankPicture: item.rank_picture,
                                                                point: item.total_point
                                                            }, index));
                                                        })
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                        className: "tab-pane fade",
                                                        id: "monthly",
                                                        role: "tabpanel",
                                                        "aria-labelledby": "monthly-tab",
                                                        children: monthly?.map((item, index)=>{
                                                            return(/*#__PURE__*/ jsx_runtime_.jsx(CardLeaderboard_CardLeaderboard, {
                                                                index: index,
                                                                thumbnail: item.avatar,
                                                                name: item.name,
                                                                rank: item.rank,
                                                                rankPicture: item.rank_picture,
                                                                point: item.total_point
                                                            }, index));
                                                        })
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                        className: "tab-pane fade",
                                                        id: "overall",
                                                        role: "tabpanel",
                                                        "aria-labelledby": "overall-tab",
                                                        children: overall?.map((item, index)=>{
                                                            return(/*#__PURE__*/ jsx_runtime_.jsx(CardLeaderboard_CardLeaderboard, {
                                                                index: index,
                                                                thumbnail: item.avatar,
                                                                name: item.name,
                                                                rank: item.rank,
                                                                rankPicture: item.rank_picture,
                                                                point: item.total_point
                                                            }, index));
                                                        })
                                                    })
                                                ]
                                            })
                                        ]
                                    })
                                })
                            })
                        ]
                    })
                })
            })
        ]
    }));
};
/* harmony default export */ const leaderboard = (Leaderboard);


/***/ }),

/***/ 5368:
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/free-brands-svg-icons");

/***/ }),

/***/ 197:
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/free-regular-svg-icons");

/***/ }),

/***/ 6466:
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/free-solid-svg-icons");

/***/ }),

/***/ 7197:
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/react-fontawesome");

/***/ }),

/***/ 2167:
/***/ ((module) => {

"use strict";
module.exports = require("axios");

/***/ }),

/***/ 2733:
/***/ ((module) => {

"use strict";
module.exports = require("fuse.js");

/***/ }),

/***/ 562:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/server/denormalize-page-path.js");

/***/ }),

/***/ 4014:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/i18n/normalize-locale-path.js");

/***/ }),

/***/ 8524:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/is-plain-object.js");

/***/ }),

/***/ 8020:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/mitt.js");

/***/ }),

/***/ 4964:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router-context.js");

/***/ }),

/***/ 9565:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/get-asset-path-from-route.js");

/***/ }),

/***/ 4365:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/get-middleware-regex.js");

/***/ }),

/***/ 1428:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/is-dynamic.js");

/***/ }),

/***/ 1292:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/parse-relative-url.js");

/***/ }),

/***/ 979:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/querystring.js");

/***/ }),

/***/ 6052:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/resolve-rewrites.js");

/***/ }),

/***/ 4226:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/route-matcher.js");

/***/ }),

/***/ 5052:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/route-regex.js");

/***/ }),

/***/ 9232:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/utils.js");

/***/ }),

/***/ 968:
/***/ ((module) => {

"use strict";
module.exports = require("next/head");

/***/ }),

/***/ 1853:
/***/ ((module) => {

"use strict";
module.exports = require("next/router");

/***/ }),

/***/ 6689:
/***/ ((module) => {

"use strict";
module.exports = require("react");

/***/ }),

/***/ 997:
/***/ ((module) => {

"use strict";
module.exports = require("react/jsx-runtime");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [7730,9285,1664,4528], () => (__webpack_exec__(2874)));
module.exports = __webpack_exports__;

})();