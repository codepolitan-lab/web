(() => {
var exports = {};
exports.id = 507;
exports.ids = [507];
exports.modules = {

/***/ 8247:
/***/ ((module) => {

// Exports
module.exports = {
	"hero": "Hero_hero__qikqd",
	"hero_title": "Hero_hero_title__Eg5XA"
};


/***/ }),

/***/ 116:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);

const CardTestimony = ({ name , role , thumbnail , organization , content  })=>{
    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
        className: "card border-0 shadow-sm h-100",
        children: [
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                className: "card-body p-4 p-lg-5",
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h5", {
                        children: organization
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                        className: "text-muted",
                        children: content
                    })
                ]
            }),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                className: "card-footer bg-white px-4 px-lg-5",
                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                    className: "row",
                    children: [
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "col-3",
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                className: "img-fluid rounded-circle",
                                src: thumbnail || '/assets/img/placeholder.jpg',
                                alt: role
                            })
                        }),
                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                            className: "col-auto my-auto",
                            children: [
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("strong", {
                                    children: name || 'Belum ada nama'
                                }),
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                    className: "text-muted",
                                    children: role || 'Belum ada role'
                                })
                            ]
                        })
                    ]
                })
            })
        ]
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (CardTestimony);


/***/ }),

/***/ 7212:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Hero_module_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(8247);
/* harmony import */ var _Hero_module_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_Hero_module_scss__WEBPACK_IMPORTED_MODULE_1__);


const Hero = ()=>{
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("section", {
        className: `${(_Hero_module_scss__WEBPACK_IMPORTED_MODULE_1___default().hero)}`,
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
            className: "container p-4 p-lg-5",
            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                className: "row",
                children: [
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: "col-lg-7 text-white text-center text-lg-start my-auto",
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h1", {
                                className: (_Hero_module_scss__WEBPACK_IMPORTED_MODULE_1___default().hero_title),
                                children: "Persiapkan Siswa dengan Keterampilan Industri 4.0"
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                className: "lead text-muted",
                                children: "Program yang dikembangkan khusus untuk membantu generasi muda belajar dan berlatih pemrograman agar siswa siap terjun dan bersaing di dunia kerja dalam industri 4.0"
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("a", {
                                className: "btn btn-primary p-3 my-3",
                                href: "#counter",
                                children: "Pelajari Selengkapnya"
                            })
                        ]
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: "col-lg-5",
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                            className: "img-fluid",
                            src: "/assets/img/forschool/hero-forschool.png",
                            alt: "Hero"
                        })
                    })
                ]
            })
        })
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Hero);


/***/ }),

/***/ 8799:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);


const SectionFormatBelajar = ()=>{
    const { 0: image , 1: setImage  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)('/assets/img/forschool/img-forschool-1.png');
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("section", {
        className: "section bg-light",
        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
            className: "container p-4 p-lg-5",
            children: [
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: "row mb-5",
                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: "col text-center",
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
                                className: "section-title",
                                children: "Tingkatkan Kualitas Lulusan IT Anda"
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                className: "text-muted",
                                children: "Lengkapi skill dan pengetahuan lulusan sekolah/kampus Anda melalui program komprehensif dari kami"
                            })
                        ]
                    })
                }),
                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                    className: "row",
                    children: [
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "col-lg-6 order-last order-lg-first mt-3 mt-lg-0",
                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                className: "accordion accordion-flush",
                                id: "FormatBelajarAccordion",
                                children: [
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        className: "accordion-item bg-light",
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
                                                className: "accordion-header",
                                                id: "flush-headingOne",
                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                                    onClick: ()=>setImage('/assets/img/forschool/img-forschool-1.png')
                                                    ,
                                                    className: "accordion-button px-0",
                                                    type: "button",
                                                    "data-bs-toggle": "collapse",
                                                    "data-bs-target": "#flush-collapseOne",
                                                    "aria-expanded": "true",
                                                    "aria-controls": "flush-collapseOne",
                                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("strong", {
                                                        children: "Kurikulum Berstandar SKKNI"
                                                    })
                                                })
                                            }),
                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                id: "flush-collapseOne",
                                                className: "accordion-collapse collapse show",
                                                "aria-labelledby": "flush-headingOne",
                                                "data-bs-parent": "#FormatBelajarAccordion",
                                                children: [
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "accordion-body px-0 text-muted",
                                                        children: "Siswa akan belajar secara online dengan kurikulum yang mengacu pada Standar Kompetensi Kerja Nasional Indonesia (SKKNI) berdasarkan jalur profesi Junior Web Developer."
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                        className: "img-fluid d-block d-lg-none mx-auto",
                                                        src: "/assets/img/forschool/img-forschool-1.png",
                                                        alt: "Codepolitan"
                                                    })
                                                ]
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        className: "accordion-item bg-light",
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
                                                className: "accordion-header",
                                                id: "flush-headingTwo",
                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                                    onClick: ()=>setImage('/assets/img/forschool/img-forschool-2.png')
                                                    ,
                                                    className: "accordion-button px-0 collapsed",
                                                    type: "button",
                                                    "data-bs-toggle": "collapse",
                                                    "data-bs-target": "#flush-collapseTwo",
                                                    "aria-expanded": "false",
                                                    "aria-controls": "flush-collapseTwo",
                                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("strong", {
                                                        children: "Webinar Session Bersama Pakar"
                                                    })
                                                })
                                            }),
                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                id: "flush-collapseTwo",
                                                className: "accordion-collapse collapse",
                                                "aria-labelledby": "flush-headingTwo",
                                                "data-bs-parent": "#FormatBelajarAccordion",
                                                children: [
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "accordion-body px-0 text-muted",
                                                        children: "Siswa bisa mengikuti program webinar yang diselenggarakan setiap bulan untuk mendapatkan informasi terkait dunia teknologi dan industri dalam bidang pemrograman dan teknologi"
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                        className: "img-fluid d-block d-lg-none mx-auto",
                                                        src: "/assets/img/forschool/img-forschool-2.png",
                                                        alt: "Codepolitan"
                                                    })
                                                ]
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        className: "accordion-item bg-light",
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
                                                className: "accordion-header",
                                                id: "flush-headingThree",
                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                                    onClick: ()=>setImage('/assets/img/forschool/img-forschool-3.png')
                                                    ,
                                                    className: "accordion-button px-0 collapsed",
                                                    type: "button",
                                                    "data-bs-toggle": "collapse",
                                                    "data-bs-target": "#flush-collapseThree",
                                                    "aria-expanded": "false",
                                                    "aria-controls": "flush-collapseThree",
                                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("strong", {
                                                        children: "Tingkatkan Kapasitas Siswa Dengan Studi Kasus"
                                                    })
                                                })
                                            }),
                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                id: "flush-collapseThree",
                                                className: "accordion-collapse collapse",
                                                "aria-labelledby": "flush-headingThree",
                                                "data-bs-parent": "#FormatBelajarAccordion",
                                                children: [
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "accordion-body px-0 text-muted",
                                                        children: "Tidak hanya membahas teori, disini siswa juga akan belajar bagaimana caranya menggunakan teknologi yang sedang dipelajarinya dalam sebuah proyek nyata."
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                        className: "img-fluid d-block d-lg-none mx-auto",
                                                        src: "/assets/img/forschool/img-forschool-3.png",
                                                        alt: "Codepolitan"
                                                    })
                                                ]
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        className: "accordion-item bg-light",
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
                                                className: "accordion-header",
                                                id: "flush-headingFour",
                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                                    onClick: ()=>setImage('/assets/img/forschool/img-forschool-4.png')
                                                    ,
                                                    className: "accordion-button px-0 collapsed",
                                                    type: "button",
                                                    "data-bs-toggle": "collapse",
                                                    "data-bs-target": "#flush-collapseFour",
                                                    "aria-expanded": "false",
                                                    "aria-controls": "flush-collapseFour",
                                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("strong", {
                                                        children: "Perkaya Pengalaman Siswa dengan Magang"
                                                    })
                                                })
                                            }),
                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                id: "flush-collapseFour",
                                                className: "accordion-collapse collapse",
                                                "aria-labelledby": "flush-headingFour",
                                                "data-bs-parent": "#FormatBelajarAccordion",
                                                children: [
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "accordion-body px-0 text-muted",
                                                        children: "Bagi siswa yang mampu menyelesaikan program dengan baik dan berhasil lulus seleksi akan berkesempaan mengikuti program magang Super Bootcamp dan bimbingan kari di CodePolitan selama 2 bulan. GRATIS"
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                        className: "img-fluid d-block d-lg-none mx-auto",
                                                        src: "/assets/img/forschool/img-forschool-4.png",
                                                        alt: "Codepolitan"
                                                    })
                                                ]
                                            })
                                        ]
                                    })
                                ]
                            })
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "col-lg-5 offset-lg-1 my-auto",
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                className: "img-fluid d-none d-lg-block",
                                src: image,
                                alt: "Codepolitan"
                            })
                        })
                    ]
                })
            ]
        })
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (SectionFormatBelajar);


/***/ }),

/***/ 2029:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);

const SectionPartner = ()=>{
    const partners = [
        {
            name: 'SMKN 4 Kendal',
            thumbnail: 'https://i.ibb.co/VTkGSGv/smkn4.webp'
        },
        {
            name: 'Wikrama',
            thumbnail: 'https://i.ibb.co/2qdszLv/wikrama.webp'
        },
        {
            name: 'AMIK Bumi Nusantara Cirebon',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/MitraKampus_WEBP/logo-amik-bumi-nusantara_NcvdNtyCx.webp'
        },
        {
            name: 'UPI',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/MitraKampus_WEBP/logo-upi_Za_sjehLv.webp'
        },
        {
            name: 'USTJ',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/MitraKampus_WEBP/logo-ustj_nxp-XxsM-.webp'
        },
        {
            name: 'UII',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/MitraKampus_WEBP/Logo-UII-Asli_PhHNmKFQx_GBW.webp'
        }
    ];
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("section", {
        className: "section",
        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
            className: "container p-4 p-lg-5",
            children: [
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: "row mb-5",
                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: "col text-center",
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
                            className: "section-title",
                            children: "Telah Dipercaya Oleh"
                        })
                    })
                }),
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: "row justify-content-center my-3",
                    children: partners.map((partner, index)=>{
                        return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "col-4 col-md-2 my-2",
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: "card bg-transparent border-0",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "card-body p-0",
                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                        className: "img-fluid d-block mx-auto",
                                        src: partner.thumbnail,
                                        alt: partner.name
                                    })
                                })
                            })
                        }, index));
                    })
                })
            ]
        })
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (SectionPartner);


/***/ }),

/***/ 9332:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);

const SectionTechnology = ()=>{
    const technologies = [
        {
            id: 1,
            thumbnail: '/assets/img/program/icons/box-icons/icon-html5.png',
            title: 'HTML & CSS',
            description: 'Belajar dari dasar hingga mahir'
        },
        {
            id: 2,
            thumbnail: '/assets/img/program/icons/box-icons/icon-bootstrap.png',
            title: 'Bootstrap',
            description: 'Belajar dari dasar hingga mahir'
        },
        {
            id: 3,
            thumbnail: '/assets/img/program/icons/box-icons/icon-mysql.png',
            title: 'MySQL',
            description: 'Belajar dari dasar hingga mahir'
        },
        {
            id: 4,
            thumbnail: '/assets/img/program/icons/box-icons/icon-php.png',
            title: 'PHP',
            description: 'Belajar dari dasar hingga mahir'
        },
        {
            id: 5,
            thumbnail: '/assets/img/program/icons/box-icons/icon-codeigniter.png',
            title: 'Codeigniter',
            description: 'Belajar dari dasar hingga mahir'
        },
        {
            id: 6,
            thumbnail: '/assets/img/program/icons/box-icons/icon-laravel.png',
            title: 'Laravel',
            description: 'Belajar dari dasar hingga mahir'
        },
        {
            id: 9,
            thumbnail: '/assets/img/program/icons/box-icons/icon-jquery.png',
            title: 'JQuery',
            description: 'Belajar dari dasar hingga mahir'
        },
        {
            id: 10,
            thumbnail: '/assets/img/program/icons/box-icons/icon-js.png',
            title: 'JavaScript',
            description: 'Belajar dari dasar hingga mahir'
        },
        {
            id: 12,
            thumbnail: '/assets/img/program/icons/box-icons/icon-vue.png',
            title: 'Vue',
            description: 'Belajar dari dasar hingga mahir'
        }, 
    ];
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("section", {
        className: "section bg-light",
        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
            className: "container p-4 p-lg-5",
            children: [
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: "row my-3",
                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: "col text-center",
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
                                className: "section-title",
                                children: "Teknologi Yang Dapat Dipelajari"
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                className: "text-muted",
                                children: "Ikuti Jalur belajar menjadi proffesional di dunia kerja seperti"
                            })
                        ]
                    })
                }),
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: "row",
                    children: technologies.map((tech)=>{
                        return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "col-md-4 mb-3",
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: "card border-0 shadow-sm",
                                style: {
                                    borderRadius: '15px'
                                },
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "card-body p-3",
                                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        className: "row text-muted",
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                className: "col-4 my-auto",
                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                    className: "img-fluid rounded",
                                                    src: tech.thumbnail,
                                                    alt: tech.title
                                                })
                                            }),
                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                className: "col-8 mt-2",
                                                children: [
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h5", {
                                                        style: {
                                                            fontSize: 'medium'
                                                        },
                                                        children: tech.title
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                        style: {
                                                            fontSize: 'small'
                                                        },
                                                        children: tech.description
                                                    })
                                                ]
                                            })
                                        ]
                                    })
                                })
                            })
                        }, tech.id));
                    })
                })
            ]
        })
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (SectionTechnology);


/***/ }),

/***/ 2630:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.a(module, async (__webpack_handle_async_dependencies__) => {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var swiper_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3015);
/* harmony import */ var _forschool_CardTestimony_CardTestimony__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(116);
/* harmony import */ var swiper__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3877);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([swiper_react__WEBPACK_IMPORTED_MODULE_1__, swiper__WEBPACK_IMPORTED_MODULE_3__]);
([swiper_react__WEBPACK_IMPORTED_MODULE_1__, swiper__WEBPACK_IMPORTED_MODULE_3__] = __webpack_async_dependencies__.then ? await __webpack_async_dependencies__ : __webpack_async_dependencies__);



// import Swiper core and required modules

// install Swiper modules
swiper__WEBPACK_IMPORTED_MODULE_3__["default"].use([
    swiper__WEBPACK_IMPORTED_MODULE_3__.Pagination,
    swiper__WEBPACK_IMPORTED_MODULE_3__.Navigation,
    swiper__WEBPACK_IMPORTED_MODULE_3__.Autoplay
]);
const SectionTestimony = ({ title , data  })=>{
    const testimonials = data;
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("section", {
        className: "section bg-light",
        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
            className: "container p-4 p-lg-5",
            children: [
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: "row",
                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: "col text-center",
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
                            className: "section-title",
                            children: title
                        })
                    })
                }),
                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                    className: "row my-5",
                    children: [
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "col-lg-5 d-none d-lg-block",
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                className: "img-fluid d-block mx-auto",
                                src: "/assets/img/forschool/testimony.png",
                                alt: "Testimony"
                            })
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "col-lg-7 my-auto",
                            id: "SwiperForSchool",
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(swiper_react__WEBPACK_IMPORTED_MODULE_1__.Swiper, {
                                autoplay: true,
                                loop: true,
                                centeredSlides: true,
                                navigation: true,
                                pagination: true,
                                slidesPerView: 1,
                                spaceBetween: 50,
                                style: {
                                    padding: '0 40px 40px 40px'
                                },
                                children: testimonials.map((testimony, index)=>{
                                    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(swiper_react__WEBPACK_IMPORTED_MODULE_1__.SwiperSlide, {
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_forschool_CardTestimony_CardTestimony__WEBPACK_IMPORTED_MODULE_2__/* ["default"] */ .Z, {
                                            thumbnail: testimony.thumbnail,
                                            name: testimony.name,
                                            role: testimony.role,
                                            organization: testimony.organization,
                                            content: testimony.content
                                        })
                                    }, index));
                                })
                            })
                        })
                    ]
                })
            ]
        })
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (SectionTestimony);

});

/***/ }),

/***/ 4971:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.a(module, async (__webpack_handle_async_dependencies__) => {
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "getServerSideProps": () => (/* binding */ getServerSideProps),
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2167);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(968);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _components_global_Layout_Layout__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(4528);
/* harmony import */ var _components_forschool_Hero_Hero__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(7212);
/* harmony import */ var _components_global_Sections_SectionCounter_SectionCounter__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(5919);
/* harmony import */ var _components_global_Sections_SectionPartners_SectionPartners__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(3858);
/* harmony import */ var _components_forschool_Sections_SectionFormatBelajar_SectionFormatBelajar__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(8799);
/* harmony import */ var _components_global_Sections_SectionRoadmapList_SectionRoadmapList__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(834);
/* harmony import */ var _components_forschool_Sections_SectionTechnology_SectionTechnology__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(9332);
/* harmony import */ var _components_global_Sections_BenefitSection_BenefitSection__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(9725);
/* harmony import */ var _components_global_Sections_SectionTestimonyLanding_SectionTestimonyLanding__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(2630);
/* harmony import */ var _components_forschool_Sections_SectionPartner_SectionPartner__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(2029);
/* harmony import */ var _components_global_Sections_SectionCTA_SectionCTA__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(122);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_components_global_Sections_SectionTestimonyLanding_SectionTestimonyLanding__WEBPACK_IMPORTED_MODULE_11__]);
_components_global_Sections_SectionTestimonyLanding_SectionTestimonyLanding__WEBPACK_IMPORTED_MODULE_11__ = (__webpack_async_dependencies__.then ? await __webpack_async_dependencies__ : __webpack_async_dependencies__)[0];














const getServerSideProps = async ()=>{
    const statsRes = await axios__WEBPACK_IMPORTED_MODULE_1___default().get(`${"https://api.codepolitan.com"}/v1/stats`);
    const stats = statsRes.data;
    return {
        props: {
            stats
        }
    };
};
const ForSchool = ({ stats  })=>{
    const testimonials = [
        {
            name: 'Soni',
            thumbnail: 'https://i.ibb.co/QPKMdjk/image2.jpg',
            role: 'Ketua Program Studi',
            organization: 'Universitas Respati Indonesia',
            content: 'Mahasiswa lebih aplikatif dalam implementasi pada hal yang berkaitan dengan matkul, Mendapatkan tambahan keahlian sesuai dengan bidang ilmu, Menambah sertifikat keahlian yang dapat digunakan dalam mencari pekerjaan'
        },
        {
            name: 'Evanita Veronica Manullang',
            thumbnail: 'https://i.ibb.co/q039Hz0/image1.jpg',
            role: 'Kepala Laboratorium FIKOM',
            organization: 'Universitas Sains dan Teknologi Jayapura',
            content: 'Kemampuan mahasiswa menjadi semakin baik dengan bantuan video-video pembelajaran yang terstruktur, terlebih disaat pandemi covid 19. Mahasiswa yang mengikuti program kampus coding sebagian besar sudah dapat membuat aplikasi web sendiri. '
        }, 
    ];
    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
        children: [
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)((next_head__WEBPACK_IMPORTED_MODULE_2___default()), {
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("title", {
                        children: "Codepolitan for School - Codepolitan"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        name: "title",
                        content: "Codepolitan for School - Codepolitan"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        name: "description",
                        content: "Persiapkan Siswa dengan Keterampilan Industri 4.0. Program yang dikembangkan khusus untuk membantu generasi muda belajar dan berlatih pemrograman agar siswa siap terjun dan bersaing di dunia kerja dalam industri 4.0"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:type",
                        content: "website"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:url",
                        content: "https://codepolitan.com/for-school"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:title",
                        content: "Codepolitan for School - Codepolitan"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:description",
                        content: "Persiapkan Siswa dengan Keterampilan Industri 4.0. Program yang dikembangkan khusus untuk membantu generasi muda belajar dan berlatih pemrograman agar siswa siap terjun dan bersaing di dunia kerja dalam industri 4.0"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:image",
                        content: "https://i.ibb.co/Jtk0cwS/OG-For-School.png"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:card",
                        content: "summary_large_image"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:url",
                        content: "https://codepolitan.com/for-school"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:title",
                        content: "Codepolitan for School - Codepolitan"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:description",
                        content: "Persiapkan Siswa dengan Keterampilan Industri 4.0. Program yang dikembangkan khusus untuk membantu generasi muda belajar dan berlatih pemrograman agar siswa siap terjun dan bersaing di dunia kerja dalam industri 4.0"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:image",
                        content: "https://i.ibb.co/Jtk0cwS/OG-For-School.png"
                    })
                ]
            }),
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_components_global_Layout_Layout__WEBPACK_IMPORTED_MODULE_3__/* ["default"] */ .Z, {
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_forschool_Hero_Hero__WEBPACK_IMPORTED_MODULE_4__/* ["default"] */ .Z, {}),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_global_Sections_SectionCounter_SectionCounter__WEBPACK_IMPORTED_MODULE_5__/* ["default"] */ .Z, {
                        data: stats
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_global_Sections_SectionPartners_SectionPartners__WEBPACK_IMPORTED_MODULE_6__/* ["default"] */ .Z, {
                        backgroundColor: "bg-white",
                        sectionTitleTextStyle: "text-center"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_forschool_Sections_SectionFormatBelajar_SectionFormatBelajar__WEBPACK_IMPORTED_MODULE_7__/* ["default"] */ .Z, {}),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_global_Sections_SectionRoadmapList_SectionRoadmapList__WEBPACK_IMPORTED_MODULE_8__/* ["default"] */ .Z, {
                        title: "Terapkan Jalur Belajar Profesional Untuk Siswa Anda",
                        subtitle: "Ikuti Jalur belajar menjadi proffesional di dunia kerja seperti"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_forschool_Sections_SectionTechnology_SectionTechnology__WEBPACK_IMPORTED_MODULE_9__/* ["default"] */ .Z, {}),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_global_Sections_BenefitSection_BenefitSection__WEBPACK_IMPORTED_MODULE_10__/* ["default"] */ .Z, {
                        title: "Kenapa CodePolitan For School?",
                        subtitle: "CodePolitan memiliki fasilitas yang lengkap sebagai sebuah platform untuk mendukung efektivitas belajar pemrograman secara online"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_global_Sections_SectionTestimonyLanding_SectionTestimonyLanding__WEBPACK_IMPORTED_MODULE_11__/* ["default"] */ .Z, {
                        title: "Sekolah Yang Berinovasi Bersama CodePolitan",
                        data: testimonials
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_forschool_Sections_SectionPartner_SectionPartner__WEBPACK_IMPORTED_MODULE_12__/* ["default"] */ .Z, {}),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_global_Sections_SectionCTA_SectionCTA__WEBPACK_IMPORTED_MODULE_13__/* ["default"] */ .Z, {
                        caption: "Siap untuk berkolaborasi dalam mencetak lulusan IT handal bersama CodePolitan?",
                        btnTitle: "Hubungi Kami Sekarang",
                        link: "https://wa.me/628999488990?text=Halo, saya mau tanya tentang Kampuscoding / Codepolitan For School."
                    })
                ]
            })
        ]
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ForSchool);

});

/***/ }),

/***/ 5368:
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/free-brands-svg-icons");

/***/ }),

/***/ 197:
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/free-regular-svg-icons");

/***/ }),

/***/ 6466:
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/free-solid-svg-icons");

/***/ }),

/***/ 7197:
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/react-fontawesome");

/***/ }),

/***/ 2167:
/***/ ((module) => {

"use strict";
module.exports = require("axios");

/***/ }),

/***/ 2733:
/***/ ((module) => {

"use strict";
module.exports = require("fuse.js");

/***/ }),

/***/ 5804:
/***/ ((module) => {

"use strict";
module.exports = require("mixpanel-browser");

/***/ }),

/***/ 2245:
/***/ ((module) => {

"use strict";
module.exports = require("moment");

/***/ }),

/***/ 5518:
/***/ ((module) => {

"use strict";
module.exports = require("moment/locale/id");

/***/ }),

/***/ 562:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/server/denormalize-page-path.js");

/***/ }),

/***/ 4014:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/i18n/normalize-locale-path.js");

/***/ }),

/***/ 8524:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/is-plain-object.js");

/***/ }),

/***/ 8020:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/mitt.js");

/***/ }),

/***/ 4964:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router-context.js");

/***/ }),

/***/ 9565:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/get-asset-path-from-route.js");

/***/ }),

/***/ 4365:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/get-middleware-regex.js");

/***/ }),

/***/ 1428:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/is-dynamic.js");

/***/ }),

/***/ 1292:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/parse-relative-url.js");

/***/ }),

/***/ 979:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/querystring.js");

/***/ }),

/***/ 6052:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/resolve-rewrites.js");

/***/ }),

/***/ 4226:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/route-matcher.js");

/***/ }),

/***/ 5052:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/route-regex.js");

/***/ }),

/***/ 9232:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/utils.js");

/***/ }),

/***/ 968:
/***/ ((module) => {

"use strict";
module.exports = require("next/head");

/***/ }),

/***/ 1853:
/***/ ((module) => {

"use strict";
module.exports = require("next/router");

/***/ }),

/***/ 6689:
/***/ ((module) => {

"use strict";
module.exports = require("react");

/***/ }),

/***/ 609:
/***/ ((module) => {

"use strict";
module.exports = require("react-countup");

/***/ }),

/***/ 997:
/***/ ((module) => {

"use strict";
module.exports = require("react/jsx-runtime");

/***/ }),

/***/ 3877:
/***/ ((module) => {

"use strict";
module.exports = import("swiper");;

/***/ }),

/***/ 3015:
/***/ ((module) => {

"use strict";
module.exports = import("swiper/react");;

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [7730,9285,1664,4528,4986,8508,5919,9725,834], () => (__webpack_exec__(4971)));
module.exports = __webpack_exports__;

})();