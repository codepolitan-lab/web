(() => {
var exports = {};
exports.id = 6478;
exports.ids = [6478];
exports.modules = {

/***/ 7272:
/***/ ((module) => {

// Exports
module.exports = {
	"hero": "Hero_hero__83lwZ",
	"hero_title": "Hero_hero_title__DJqy3"
};


/***/ }),

/***/ 2336:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Mh": () => (/* binding */ onlineCourse),
/* harmony export */   "oT": () => (/* binding */ challenges),
/* harmony export */   "_Q": () => (/* binding */ advertorial),
/* harmony export */   "No": () => (/* binding */ seminar),
/* harmony export */   "ig": () => (/* binding */ skillDefinedTraining),
/* harmony export */   "Sx": () => (/* binding */ onDemandTraining),
/* harmony export */   "tk": () => (/* binding */ onlineCourseModal),
/* harmony export */   "Rj": () => (/* binding */ challengesModal),
/* harmony export */   "G8": () => (/* binding */ seminarModal),
/* harmony export */   "Ti": () => (/* binding */ advertorialModal),
/* harmony export */   "I1": () => (/* binding */ skillDefinedTrainingModal),
/* harmony export */   "Be": () => (/* binding */ onDemandTrainingModal)
/* harmony export */ });
// Data static for partners company
const onlineCourse = [
    {
        name: 'Here',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/here_THgKeV7WUz.webp'
    },
    {
        name: 'Alibaba Cloud',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/Alibaba_kBCmKi7O7.webp'
    },
    {
        name: 'Intel',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/Intel_T_cVZHo81M_t.webp'
    },
    {
        name: 'Dicoding',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/dicoding-header-logo_-yUghUNkx7.webp'
    },
    {
        name: 'IBM',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/IBM_GXVYl92inl905.webp'
    },
    {
        name: 'XL',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/xl_t5bLnDwJn.webp'
    }, 
];
const challenges = [
    {
        name: 'Here',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/here_THgKeV7WUz.webp'
    },
    {
        name: 'Alibaba Cloud',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/Alibaba_kBCmKi7O7.webp'
    },
    {
        name: 'Pixel',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/pixel_YtPl8tvzu.webp'
    },
    {
        name: 'Refactory',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/Refactory_VO89Fn5BX.webp'
    }, 
];
const advertorial = [
    {
        name: 'Lenovo',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/Lenovo_co1GDjBpxV.webp'
    },
    {
        name: 'Intel',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/Intel_T_cVZHo81M_t.webp'
    },
    {
        name: 'XL',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/xl_t5bLnDwJn.webp'
    },
    {
        name: 'Pixel',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/pixel_YtPl8tvzu.webp'
    },
    {
        name: 'Hactive8',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/hacktiv8_Y3L2HUj4K4X.webp'
    },
    {
        name: 'Ajita',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/ajita_Za3H1pSLYI.webp'
    },
    {
        name: 'DBS',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/dbs_JaOD33dHI.png'
    },
    {
        name: 'BeKraf',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/bekraf_xdWemP6CP.png'
    }, 
];
const seminar = [
    {
        name: 'Kalibrr',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/kalibrr_vUntvF04P.png'
    },
    {
        name: 'Indosat',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/indosat_hsSFKihNc.png'
    },
    {
        name: 'UMN',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/umn_wFVQI1UOZ.png'
    },
    {
        name: 'Mitra',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/mitra_y4yIE9vxn.png'
    },
    {
        name: 'Indoomtech',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/indocomtech_VqiLUYbrz.png'
    },
    {
        name: 'Video',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/mitra_y4yIE9vxn.png'
    },
    {
        name: 'Cicil',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/cicil_jtK2IVHRw.png'
    },
    {
        name: 'IBM',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/IBM_GXVYl92inl905.webp'
    },
    {
        name: 'Hactive8',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/hacktiv8_Y3L2HUj4K4X.webp'
    },
    {
        name: 'Kudo',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/Kudo_oGHxpphEw.webp'
    },
    {
        name: 'Refactory',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/Refactory_VO89Fn5BX.webp'
    }, 
];
const skillDefinedTraining = [
    {
        name: 'Digital Talent',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/digital-talent_BDZO919u0W8h.png'
    },
    {
        name: 'Kominfo',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/kominfo_quHEyJih5Z.png'
    },
    {
        name: 'Kemnaker',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/KEMNAKER_MGfr7cHn61.webp'
    },
    {
        name: 'Lintasarta',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/lintasarta_YB5bvACBHX.png'
    },
    {
        name: 'Geometri',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/geometri_bbjZ-4j3k.png'
    },
    {
        name: 'SOS',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/sos_GnYEK_bAz8.png'
    },
    {
        name: 'Kemenkeu',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/kemenkeu_p9YQYkw-AI.webp'
    }, 
];
const onDemandTraining = [
    {
        name: 'Mitra',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/mitra_y4yIE9vxn.png'
    },
    {
        name: 'Pixel',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/pixel_YtPl8tvzu.webp'
    },
    {
        name: ' ',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/'
    },
    {
        name: ' ',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/'
    }, 
];
// Data static for modal data
const onlineCourseModal = {
    title: "Online Course Development",
    image: "/assets/img/forcompany/online-course-modal.png",
    partner: "Here",
    description: "Online Course Development adalah solusi yang sangat tepat bila Anda membutuhkan strategi adopsi teknologi ke pasar pembelajar IT. Dengan memperkenalkan produk teknologi Anda dalam bentuk online course, Anda dapat mempromosikan sekaligus mengedukasi audiens CodePolitan untuk lebih mudah lagi dalam menggunakan produk Anda.",
    logoPartner: "https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/here_THgKeV7WUz.webp"
};
const challengesModal = {
    title: "Challenges",
    image: "/assets/img/forcompany/challenges.png",
    partner: "Alibaba Cloud",
    description: "Program Challenges merupakan salah satu solusi untuk memperkenalkan atau meningkatkan produk bisnis anda, dengan adanya CodePolitan yang sudah memiliki 150k+ member aktif program ini terbukti sudah berhasil dalam mengadakan tantangan seperti Re-Cloud Challenges 2021 dan 2022 yang bisa mendapatkan 2k+ pendaftar.",
    logoPartner: "https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/Alibaba_kBCmKi7O7.webp"
};
const seminarModal = {
    title: "Online/Offline Seminar",
    image: "/assets/img/forcompany/seminar.png",
    partner: "Kalibrr",
    description: "Program seminar yang diadakan khusus untuk membantu meningkatkan atau memperkenalkan produk bisnis anda melalui seminar live baik online maupun offline, dengan jangkauan relasi kami yang luas.",
    logoPartner: "https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/kalibrr_vUntvF04P.png"
};
const advertorialModal = {
    title: "Advertorial",
    image: "/assets/img/forcompany/advertorial.png",
    partner: "Lenovo",
    description: "Melalui program Advertorial anda dapat menitipakan produk teknologi, kami dapat menjadi solusi untuk menjual produk anda di website dan semusa sosial media kami, supaya produk teknologi bisnis kamu dapat dikenal lebih cepat lagi melalui CodePolitan.",
    logoPartner: "https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/Lenovo_co1GDjBpxV.webp"
};
const skillDefinedTrainingModal = {
    title: "Skill Defined Training",
    image: "/assets/img/forcompany/kominfo-modal.png",
    partner: "Kominfo",
    description: "Program Trainer Outsources ini anda dapat meningkatkan skill karyawan anda berdasarkan materi yang kami buat. Anda juga dapat memulainya di perusahaan anda atau di office CodePolitan.",
    logoPartner: "https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/kominfo_quHEyJih5Z.png"
};
const onDemandTrainingModal = {
    title: "On-Demand Training",
    image: "/assets/img/forcompany/kominfo-modal.png",
    partner: "Pixel",
    description: "Anda dapat meningkatkan karyawan sesuai dengan request yang bisa anda minta dibidang kemampuan IT yang meliputi ( Fullstack Developer, Database Engineer, Android Developer, Frontend Developer, Backend Developer, UI/UX Developer, Cloud Computing, dan Cyber Security ).",
    logoPartner: "https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/pixel_YtPl8tvzu.webp"
};


/***/ }),

/***/ 1435:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Hero_module_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(7272);
/* harmony import */ var _Hero_module_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_Hero_module_scss__WEBPACK_IMPORTED_MODULE_1__);


const Hero = ()=>{
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("section", {
        className: `${(_Hero_module_scss__WEBPACK_IMPORTED_MODULE_1___default().hero)}`,
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
            className: "container p-4 p-lg-5",
            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                className: "row",
                children: [
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: "col-lg-7 text-white text-center text-lg-start my-auto order-last order-md-first",
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h1", {
                                className: (_Hero_module_scss__WEBPACK_IMPORTED_MODULE_1___default().hero_title),
                                children: "Tingkatkan Kapasitas Bisnis Anda dengan Memanfaatkan Ekosistem IT dari CodePolitan"
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                className: "lead text-muted mt-4",
                                children: "CodePolitan memiliki platform belajar dan juga ratusan kelas online IT yang digunakan oleh lebih dari 150 ribu member. Pelajari bagaimana solusi kami dapat membantu meningkatkan performa bisnis anda."
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("a", {
                                className: "btn btn-primary btn-rounded py-3 px-4 my-3",
                                href: "#mitraKami",
                                children: "Pelajari Selengkapnya"
                            })
                        ]
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: "col-lg-5",
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                            className: "img-fluid",
                            src: "/assets/img/forcompany/hero-forcompany.png",
                            alt: "Hero"
                        })
                    })
                ]
            })
        })
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Hero);


/***/ }),

/***/ 7937:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.a(module, async (__webpack_handle_async_dependencies__) => {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var swiper_react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3015);
/* harmony import */ var swiper__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3877);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([swiper_react__WEBPACK_IMPORTED_MODULE_2__, swiper__WEBPACK_IMPORTED_MODULE_3__]);
([swiper_react__WEBPACK_IMPORTED_MODULE_2__, swiper__WEBPACK_IMPORTED_MODULE_3__] = __webpack_async_dependencies__.then ? await __webpack_async_dependencies__ : __webpack_async_dependencies__);


// Import Swiper React components

// import Swiper core and required modules

// install Swiper modules
swiper__WEBPACK_IMPORTED_MODULE_3__["default"].use([
    swiper__WEBPACK_IMPORTED_MODULE_3__.Pagination,
    swiper__WEBPACK_IMPORTED_MODULE_3__.Navigation
]);
const ModalCorporateTraining = ({ data , dataSwiper  })=>{
    const navigationPrevRef = (0,react__WEBPACK_IMPORTED_MODULE_1__.useRef)(null);
    const navigationNextRef = (0,react__WEBPACK_IMPORTED_MODULE_1__.useRef)(null);
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        className: "modal fade",
        id: "modalCorporateTraining",
        tabIndex: "-1",
        "aria-labelledby": "exampleModalLabel",
        "aria-hidden": "true",
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
            className: "modal-dialog modal-xl",
            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                className: "modal-content position-relative",
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: "modal-header ms-auto",
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                            type: "button",
                            className: "btn btn-light btn-sm",
                            "data-bs-dismiss": "modal",
                            children: "Close"
                        })
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: "modal-body px-3 px-lg-5 pb-5 pt-1",
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "row mt-4",
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: "col",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(swiper_react__WEBPACK_IMPORTED_MODULE_2__.Swiper, {
                                    spaceBetween: 20,
                                    grabCursor: true,
                                    slidesPerView: 1,
                                    navigation: {
                                        prevEl: navigationPrevRef.current,
                                        nextEl: navigationNextRef.current
                                    },
                                    onBeforeInit: (swiper)=>{
                                        swiper.params.navigation.prevEl = navigationPrevRef.current;
                                        swiper.params.navigation.nextEl = navigationNextRef.current;
                                    },
                                    breakpoints: {
                                        // when window width is >= 414px
                                        414: {
                                            slidesPerView: 1
                                        },
                                        // when window width is >= 768px
                                        768: {
                                            slidesPerView: 1
                                        },
                                        1024: {
                                            slidesPerView: 1
                                        }
                                    },
                                    style: {
                                        paddingBottom: '10px'
                                    },
                                    children: dataSwiper.sort((item)=>item.partner == data.partner ? -1 : 1
                                    ).map((item, index)=>{
                                        return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(swiper_react__WEBPACK_IMPORTED_MODULE_2__.SwiperSlide, {
                                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                className: "row text-muted",
                                                children: [
                                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                        className: "col-lg-7 order-last order-md-first",
                                                        children: [
                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h5", {
                                                                children: item.title
                                                            }),
                                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("p", {
                                                                children: [
                                                                    "Bersama : ",
                                                                    item.partner
                                                                ]
                                                            }),
                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                                className: "lh-lg mt-4",
                                                                children: item.description
                                                            })
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                        className: "col-lg-5 mb-4 mb-md-0",
                                                        children: [
                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                                src: item.image,
                                                                className: "w-100 img-fluid rounded img-modal",
                                                                alt: ""
                                                            }),
                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                                className: "partner-logo py-3 ps-4 d-none d-lg-flex",
                                                                children: item.partner == 'Kalibrr' ? /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                                    src: item.logoPartner,
                                                                    style: {
                                                                        width: '30%',
                                                                        height: '80px'
                                                                    },
                                                                    alt: item.partner
                                                                }) : /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                                    src: item.logoPartner,
                                                                    alt: item.partner
                                                                })
                                                            })
                                                        ]
                                                    })
                                                ]
                                            })
                                        }, index));
                                    })
                                })
                            })
                        })
                    }),
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: "modal-footer bg-codepolitan px-5 py-4 text-white d-block text-center text-lg-start",
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h3", {
                                className: "fw-bolder float-lg-start",
                                children: "Tertarik Program Ini?"
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("a", {
                                href: "https://wa.me/6281382123092",
                                target: "_blank",
                                className: "btn btn-pink btn-rounded float-lg-end",
                                rel: "noopener noreferrer",
                                children: "Hubungi Kami Sekarang"
                            }),
                            "                            "
                        ]
                    })
                ]
            })
        })
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ModalCorporateTraining);

});

/***/ }),

/***/ 1163:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.a(module, async (__webpack_handle_async_dependencies__) => {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var swiper_react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3015);
/* harmony import */ var swiper__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3877);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([swiper_react__WEBPACK_IMPORTED_MODULE_2__, swiper__WEBPACK_IMPORTED_MODULE_3__]);
([swiper_react__WEBPACK_IMPORTED_MODULE_2__, swiper__WEBPACK_IMPORTED_MODULE_3__] = __webpack_async_dependencies__.then ? await __webpack_async_dependencies__ : __webpack_async_dependencies__);


// Import Swiper React components

// import Swiper core and required modules

// install Swiper modules
swiper__WEBPACK_IMPORTED_MODULE_3__["default"].use([
    swiper__WEBPACK_IMPORTED_MODULE_3__.Pagination,
    swiper__WEBPACK_IMPORTED_MODULE_3__.Navigation
]);
const ModalTechAdoption = ({ data , dataSwiper  })=>{
    const navigationPrevRef = (0,react__WEBPACK_IMPORTED_MODULE_1__.useRef)(null);
    const navigationNextRef = (0,react__WEBPACK_IMPORTED_MODULE_1__.useRef)(null);
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        className: "modal fade",
        id: "modalTechAdoption",
        tabIndex: "-1",
        "aria-labelledby": "exampleModalLabel",
        "aria-hidden": "true",
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
            className: "modal-dialog modal-xl",
            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                className: "modal-content position-relative",
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: "modal-header ms-auto",
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                            type: "button",
                            className: "btn btn-light btn-sm",
                            "data-bs-dismiss": "modal",
                            children: "Close"
                        })
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: "modal-body px-3 px-lg-5 pb-5 pt-1",
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "row mt-4",
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: "col",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(swiper_react__WEBPACK_IMPORTED_MODULE_2__.Swiper, {
                                    spaceBetween: 20,
                                    grabCursor: true,
                                    slidesPerView: 1,
                                    navigation: {
                                        prevEl: navigationPrevRef.current,
                                        nextEl: navigationNextRef.current
                                    },
                                    onBeforeInit: (swiper)=>{
                                        swiper.params.navigation.prevEl = navigationPrevRef.current;
                                        swiper.params.navigation.nextEl = navigationNextRef.current;
                                    },
                                    breakpoints: {
                                        // when window width is >= 414px
                                        414: {
                                            slidesPerView: 1
                                        },
                                        // when window width is >= 768px
                                        768: {
                                            slidesPerView: 1
                                        },
                                        1024: {
                                            slidesPerView: 1
                                        }
                                    },
                                    style: {
                                        paddingBottom: '10px'
                                    },
                                    children: dataSwiper.sort((item)=>item.partner == data.partner ? -1 : 1
                                    ).map((item, index)=>{
                                        return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(swiper_react__WEBPACK_IMPORTED_MODULE_2__.SwiperSlide, {
                                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                className: "row text-muted",
                                                children: [
                                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                        className: "col-lg-7 order-last order-md-first",
                                                        children: [
                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h5", {
                                                                children: item.title
                                                            }),
                                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("p", {
                                                                children: [
                                                                    "Bersama : ",
                                                                    item.partner
                                                                ]
                                                            }),
                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                                className: "lh-lg mt-4",
                                                                children: item.description
                                                            })
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                        className: "col-lg-5 mb-4 mb-md-0",
                                                        children: [
                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                                src: item.image,
                                                                className: "w-100 img-fluid rounded img-modal",
                                                                alt: ""
                                                            }),
                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                                className: "partner-logo p-1 d-none d-lg-flex",
                                                                children: item.partner == 'Kalibrr' ? /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                                    src: item.logoPartner,
                                                                    style: {
                                                                        width: '30%',
                                                                        height: '80px'
                                                                    },
                                                                    alt: item.partner
                                                                }) : /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                                    src: item.logoPartner,
                                                                    alt: item.partner
                                                                })
                                                            })
                                                        ]
                                                    })
                                                ]
                                            })
                                        }, index));
                                    })
                                })
                            })
                        })
                    }),
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: "modal-footer bg-codepolitan px-5 py-4 text-white d-block text-center text-lg-start",
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h3", {
                                className: "fw-bolder float-lg-start",
                                children: "Tertarik Program Ini?"
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("a", {
                                href: "https://wa.me/6281382123092",
                                target: "_blank",
                                className: "btn btn-pink btn-rounded float-lg-end",
                                rel: "noopener noreferrer",
                                children: "Hubungi Kami Sekarang"
                            }),
                            "                            "
                        ]
                    })
                ]
            })
        })
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ModalTechAdoption);

});

/***/ }),

/***/ 6817:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.a(module, async (__webpack_handle_async_dependencies__) => {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6466);
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(7197);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_image__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(5675);
/* harmony import */ var swiper_react__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3015);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([swiper_react__WEBPACK_IMPORTED_MODULE_4__]);
swiper_react__WEBPACK_IMPORTED_MODULE_4__ = (__webpack_async_dependencies__.then ? await __webpack_async_dependencies__ : __webpack_async_dependencies__)[0];





const SectionContentTab = ({ partners , image , title , description , corporate  })=>{
    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
        className: "row",
        children: [
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                className: "col-lg-3",
                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                    src: image,
                    className: "w-100 img-fluid",
                    alt: ""
                })
            }),
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                className: "col-lg-8",
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h5", {
                        className: "mt-4 mt-md-0",
                        children: title
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                        children: description
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                        className: "lh-lg p-0",
                        children: "Beberapa mitra kami yang berhasil menggunakan strategi ini :"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: "row justify-content-center justify-content-md-between",
                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                            className: "col",
                            children: [
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(swiper_react__WEBPACK_IMPORTED_MODULE_4__.Swiper, {
                                    preventInteractionOnTransition: title === 'On-Demand Training' ? true : false,
                                    autoplay: true,
                                    loop: true,
                                    spaceBetween: 5,
                                    slidesPerView: 4,
                                    breakpoints: {
                                        // when window width is >= 414px
                                        375: {
                                            slidesPerView: 3
                                        },
                                        // when window width is >= 768px
                                        768: {
                                            slidesPerView: 5
                                        },
                                        1200: {
                                            slidesPerView: 4
                                        }
                                    },
                                    children: partners.map((partner, index)=>{
                                        return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(swiper_react__WEBPACK_IMPORTED_MODULE_4__.SwiperSlide, {
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                className: "card bg-transparent border-0 mt-n2",
                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                    className: "card-body",
                                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_image__WEBPACK_IMPORTED_MODULE_3__["default"], {
                                                        src: partner.thumbnail,
                                                        alt: partner.name,
                                                        layout: "responsive",
                                                        width: 100,
                                                        height: 50,
                                                        objectFit: "contain"
                                                    })
                                                })
                                            })
                                        }, index));
                                    })
                                }),
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "mt-2",
                                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("a", {
                                        type: "button",
                                        className: "fw-bold text-primary text-decoration-none",
                                        "data-bs-toggle": "modal",
                                        "data-bs-target": corporate ? "#modalCorporateTraining" : "#modalTechAdoption",
                                        children: [
                                            "Lihat Studi Kasus ",
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__.FontAwesomeIcon, {
                                                className: "ms-1",
                                                icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_1__.faArrowRight
                                            })
                                        ]
                                    })
                                })
                            ]
                        })
                    })
                ]
            })
        ]
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (SectionContentTab);

});

/***/ }),

/***/ 7910:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.a(module, async (__webpack_handle_async_dependencies__) => {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _SectionContentTab_SectionContentTab__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(6817);
/* harmony import */ var _Data_Static__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(2336);
/* harmony import */ var _Modal_ModalTechAdoption__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(1163);
/* harmony import */ var _Modal_ModalCorporateTraining__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(7937);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_Modal_ModalCorporateTraining__WEBPACK_IMPORTED_MODULE_5__, _SectionContentTab_SectionContentTab__WEBPACK_IMPORTED_MODULE_2__, _Modal_ModalTechAdoption__WEBPACK_IMPORTED_MODULE_4__]);
([_Modal_ModalCorporateTraining__WEBPACK_IMPORTED_MODULE_5__, _SectionContentTab_SectionContentTab__WEBPACK_IMPORTED_MODULE_2__, _Modal_ModalTechAdoption__WEBPACK_IMPORTED_MODULE_4__] = __webpack_async_dependencies__.then ? await __webpack_async_dependencies__ : __webpack_async_dependencies__);






const SectionCorporateTraining = ()=>{
    let { 0: modalData , 1: setModalData  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(_Data_Static__WEBPACK_IMPORTED_MODULE_3__/* .skillDefinedTrainingModal */ .I1);
    const { 0: dataSwiper  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)([
        _Data_Static__WEBPACK_IMPORTED_MODULE_3__/* .skillDefinedTrainingModal */ .I1,
        _Data_Static__WEBPACK_IMPORTED_MODULE_3__/* .onDemandTrainingModal */ .Be
    ]);
    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("section", {
        className: "bg-grey p-4 p-lg-5",
        id: "mitraKami",
        children: [
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                className: "container text-muted",
                id: "corporateTraining",
                children: [
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: "row align-items-center pb-5",
                        children: [
                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                className: "col-lg-7 order-last order-md-first",
                                children: [
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h4", {
                                        children: "Corporate Training"
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                        className: "lh-lg mt-3",
                                        children: "Codepolitan dapat menjadi solusi untuk meningkatkan kemampuan tim perusahaan anda dibidang digital, program ini dapat diselenggarakan di kantor CodePolitan, di kantor anda atau secara online secara intensif maupun ekstensif. Anda dapat menyesuaikannya tergantung ketersediaan tim dan kebijakan anda."
                                    })
                                ]
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: "col-lg-5 mb-4 mb-md-0",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                    src: "/assets/img/forcompany/Group 2423.png",
                                    className: "img-fluid w-100",
                                    alt: ""
                                })
                            })
                        ]
                    }),
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("ul", {
                        className: "nav nav-tabs",
                        id: "corporateTab",
                        role: "tablist",
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                                className: "nav-item",
                                role: "presentation",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                    onClick: ()=>setModalData(_Data_Static__WEBPACK_IMPORTED_MODULE_3__/* .skillDefinedTrainingModal */ .I1)
                                    ,
                                    className: "nav-link text-muted active",
                                    id: "skill-tab",
                                    "data-bs-toggle": "tab",
                                    "data-bs-target": "#skill",
                                    type: "button",
                                    role: "tab",
                                    "aria-controls": "skill",
                                    "aria-selected": "true",
                                    children: "Skill Defined Training"
                                })
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                                className: "nav-item",
                                role: "presentation",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                    onClick: ()=>setModalData(_Data_Static__WEBPACK_IMPORTED_MODULE_3__/* .onDemandTrainingModal */ .Be)
                                    ,
                                    className: "nav-link text-muted",
                                    id: "training-tab",
                                    "data-bs-toggle": "tab",
                                    "data-bs-target": "#training",
                                    type: "button",
                                    role: "tab",
                                    "aria-controls": "training",
                                    "aria-selected": "false",
                                    children: "On-Demand Training"
                                })
                            })
                        ]
                    }),
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: "tab-content",
                        id: "corporateTabContent",
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: "tab-pane mt-5 fade show active",
                                id: "skill",
                                role: "tabpanel",
                                "aria-labelledby": "skill-tab",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_SectionContentTab_SectionContentTab__WEBPACK_IMPORTED_MODULE_2__/* ["default"] */ .Z, {
                                    title: "Skill Defined Training",
                                    description: "Program Trainer Outsources ini anda dapat meningkatkan skill karyawan anda berdasarkan materi yang kami buat. Anda juga dapat memulainya di perusahaan anda atau di office CodePolitan.",
                                    partners: _Data_Static__WEBPACK_IMPORTED_MODULE_3__/* .skillDefinedTraining */ .ig,
                                    image: "/assets/img/forcompany/skill-defined-training.png",
                                    corporate: true
                                })
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: "tab-pane mt-5 fade",
                                id: "training",
                                role: "tabpanel",
                                "aria-labelledby": "training-tab",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_SectionContentTab_SectionContentTab__WEBPACK_IMPORTED_MODULE_2__/* ["default"] */ .Z, {
                                    title: "On-Demand Training",
                                    description: "Anda dapat meningkatkan karyawan sesuai dengan request yang bisa anda minta dibidang kemampuan IT yang meliputi ( Fullstack Developer, Database Engineer, Android Developer, Frontend Developer, Backend Developer, UI/UX Developer, Cloud Computing, dan Cyber Security ).",
                                    partners: _Data_Static__WEBPACK_IMPORTED_MODULE_3__/* .onDemandTraining */ .Sx,
                                    image: "/assets/img/forcompany/Group 2445.png",
                                    corporate: true
                                })
                            })
                        ]
                    })
                ]
            }),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_Modal_ModalCorporateTraining__WEBPACK_IMPORTED_MODULE_5__/* ["default"] */ .Z, {
                data: modalData,
                dataSwiper: dataSwiper
            })
        ]
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (SectionCorporateTraining);

});

/***/ }),

/***/ 4402:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6466);
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(7197);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(1664);




const SectionTallentScout = ()=>{
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("section", {
        className: "p-4 p-lg-5",
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
            className: "container",
            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                className: "row text-muted align-items-center",
                children: [
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: "col-lg-7 order-last order-md-first",
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h5", {
                                children: "Tallent Scout"
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                className: "lh-lg mt-3 mb-5",
                                children: "Codepolitan dapat menjadi solusi untuk meningkatkan kemampuan tim perusahaan anda dibidang digital, program ini dapat diselenggarakan di kantor CodePolitan, di kantor anda atau secara online secara intensif maupun ekstensif. Anda dapat menyesuaikannya tergantung ketersediaan tim dan kebijakan anda."
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_link__WEBPACK_IMPORTED_MODULE_3__["default"], {
                                href: "/karier",
                                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("a", {
                                    className: "text-primary fw-bold text-decoration-none",
                                    children: [
                                        "Karier Tallent Scout ",
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__.FontAwesomeIcon, {
                                            className: "ms-2",
                                            icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_1__.faArrowRight
                                        })
                                    ]
                                })
                            })
                        ]
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: "col-lg-5 mb-4 mb-md-0",
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                            src: "/assets/img/forcompany/Group 2441.png",
                            className: "img-fluid w-100",
                            alt: ""
                        })
                    })
                ]
            })
        })
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (SectionTallentScout);


/***/ }),

/***/ 2383:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.a(module, async (__webpack_handle_async_dependencies__) => {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Modal_ModalTechAdoption__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1163);
/* harmony import */ var _SectionContentTab_SectionContentTab__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(6817);
/* harmony import */ var _Data_Static__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(2336);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_4__);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_Modal_ModalTechAdoption__WEBPACK_IMPORTED_MODULE_1__, _SectionContentTab_SectionContentTab__WEBPACK_IMPORTED_MODULE_2__]);
([_Modal_ModalTechAdoption__WEBPACK_IMPORTED_MODULE_1__, _SectionContentTab_SectionContentTab__WEBPACK_IMPORTED_MODULE_2__] = __webpack_async_dependencies__.then ? await __webpack_async_dependencies__ : __webpack_async_dependencies__);





const SectionTechAdoption = ()=>{
    let { 0: modalData , 1: setModalData  } = (0,react__WEBPACK_IMPORTED_MODULE_4__.useState)(_Data_Static__WEBPACK_IMPORTED_MODULE_3__/* .onlineCourseModal */ .tk);
    const { 0: dataSwiper  } = (0,react__WEBPACK_IMPORTED_MODULE_4__.useState)([
        _Data_Static__WEBPACK_IMPORTED_MODULE_3__/* .onlineCourseModal */ .tk,
        _Data_Static__WEBPACK_IMPORTED_MODULE_3__/* .challengesModal */ .Rj,
        _Data_Static__WEBPACK_IMPORTED_MODULE_3__/* .seminarModal */ .G8,
        _Data_Static__WEBPACK_IMPORTED_MODULE_3__/* .advertorialModal */ .Ti
    ]);
    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("section", {
        className: "p-4 p-lg-5",
        id: "mitraKami",
        children: [
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                className: "container text-muted",
                children: [
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: "text-center",
                        style: {
                            marginBottom: '90px'
                        },
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h3", {
                                children: "Apa yang Kami Lakukan Bersama Mitra-mitra Kami"
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                children: "Berikut ini beberapa solusi yang kami hadirkan untuk meningkatkan performa bisnismu"
                            })
                        ]
                    }),
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: "row align-items-center pb-5",
                        children: [
                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                className: "col-lg-7 order-last order-md-first",
                                children: [
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h4", {
                                        children: "Tech Adoption"
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                        className: "lh-lg mt-3",
                                        children: "Platform CodePolitan memiliki traffic mencapai 17K kunjungan per hari atau mencapai 500K dalam sebulan yang spesifik belajar IT. Bila bisnis atau produk Anda memerlukan strategi penetrasi pasar untuk meningkatkan jumlah pengguna dan adopsi teknologi, kami memiliki beberapa solusi strategi yang dapat Anda terapkan untuk menjangkau audiens CodePolitan, diantaranya Online Course Development, Challenges, Online/Offline Seminar dan Advertorial."
                                    })
                                ]
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: "col-lg-5 mb-4 mb-md-0",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                    src: "/assets/img/forcompany/Group 2424.png",
                                    className: "img-fluid w-100",
                                    alt: ""
                                })
                            })
                        ]
                    }),
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("ul", {
                        className: "nav nav-tabs",
                        id: "myTab",
                        role: "tablist",
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                                className: "nav-item",
                                role: "presentation",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                    onClick: ()=>setModalData(_Data_Static__WEBPACK_IMPORTED_MODULE_3__/* .onlineCourseModal */ .tk)
                                    ,
                                    className: "nav-link text-muted active",
                                    id: "online-course-tab",
                                    "data-bs-toggle": "tab",
                                    "data-bs-target": "#online-course",
                                    type: "button",
                                    role: "tab",
                                    "aria-controls": "online-course",
                                    "aria-selected": "true",
                                    children: "Online Course Development"
                                })
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                                className: "nav-item",
                                role: "presentation",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                    onClick: ()=>setModalData(_Data_Static__WEBPACK_IMPORTED_MODULE_3__/* .challengesModal */ .Rj)
                                    ,
                                    className: "nav-link text-muted",
                                    id: "challenges-tab",
                                    "data-bs-toggle": "tab",
                                    "data-bs-target": "#challenges",
                                    type: "button",
                                    role: "tab",
                                    "aria-controls": "challenges",
                                    "aria-selected": "false",
                                    children: "Challenges"
                                })
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                                className: "nav-item",
                                role: "presentation",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                    onClick: ()=>setModalData(_Data_Static__WEBPACK_IMPORTED_MODULE_3__/* .seminarModal */ .G8)
                                    ,
                                    className: "nav-link text-muted",
                                    id: "online-offline-seminar-tab",
                                    "data-bs-toggle": "tab",
                                    "data-bs-target": "#online-offline-seminar",
                                    type: "button",
                                    role: "tab",
                                    "aria-controls": "online-offline-seminar",
                                    "aria-selected": "false",
                                    children: "Online/Offline Seminar"
                                })
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                                className: "nav-item",
                                role: "presentation",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                    onClick: ()=>setModalData(_Data_Static__WEBPACK_IMPORTED_MODULE_3__/* .advertorialModal */ .Ti)
                                    ,
                                    className: "nav-link text-muted",
                                    id: "advertorial-tab",
                                    "data-bs-toggle": "tab",
                                    "data-bs-target": "#advertorial",
                                    type: "button",
                                    role: "tab",
                                    "aria-controls": "advertorial",
                                    "aria-selected": "false",
                                    children: "Advertorial"
                                })
                            })
                        ]
                    }),
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: "tab-content",
                        id: "myTabContent",
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: "tab-pane mt-5 fade show active",
                                id: "online-course",
                                role: "tabpanel",
                                "aria-labelledby": "online-course-tab",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_SectionContentTab_SectionContentTab__WEBPACK_IMPORTED_MODULE_2__/* ["default"] */ .Z, {
                                    title: "Online Course Development",
                                    description: "Online Course Development adalah solusi yang sangat tepat bila Anda membutuhkan strategi adopsi teknologi ke pasar pembelajar IT. Dengan memperkenalkan produk teknologi Anda dalam bentuk online course, Anda dapat mempromosikan sekaligus mengedukasi audiens CodePolitan untuk lebih mudah lagi dalam menggunakan produk Anda.",
                                    partners: _Data_Static__WEBPACK_IMPORTED_MODULE_3__/* .onlineCourse */ .Mh,
                                    image: "/assets/img/forcompany/Group 2440.png"
                                })
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: "tab-pane mt-5 fade",
                                id: "challenges",
                                role: "tabpanel",
                                "aria-labelledby": "challenges-tab",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_SectionContentTab_SectionContentTab__WEBPACK_IMPORTED_MODULE_2__/* ["default"] */ .Z, {
                                    title: "Challenges",
                                    description: "Program Challenges merupakan salah satu solusi untuk memperkenalkan atau meningkatkan produk bisnis anda, dengan adanya CodePolitan yang sudah memiliki 150k+ member aktif program ini terbukti sudah berhasil dalam mengadakan tantangan seperti Re-Cloud Challenges 2021 dan 2022 yang bisa mendapatkan 2k+ pendaftar.",
                                    partners: _Data_Static__WEBPACK_IMPORTED_MODULE_3__/* .challenges */ .oT,
                                    image: "/assets/img/forcompany/Group 2442.png"
                                })
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: "tab-pane mt-5 fade",
                                id: "online-offline-seminar",
                                role: "tabpanel",
                                "aria-labelledby": "online-offline-seminar",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_SectionContentTab_SectionContentTab__WEBPACK_IMPORTED_MODULE_2__/* ["default"] */ .Z, {
                                    title: "Online/Offline Seminar",
                                    description: "Program seminar yang diadakan khusus untuk membantu meningkatkan atau memperkenalkan produk bisnis anda melalui seminar live baik online maupun offline, dengan jangkauan relasi kami yang luas.",
                                    partners: _Data_Static__WEBPACK_IMPORTED_MODULE_3__/* .seminar */ .No,
                                    image: "/assets/img/forcompany/Group 2443.png"
                                })
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: "tab-pane mt-5 fade",
                                id: "advertorial",
                                role: "tabpanel",
                                "aria-labelledby": "advertorial",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_SectionContentTab_SectionContentTab__WEBPACK_IMPORTED_MODULE_2__/* ["default"] */ .Z, {
                                    title: "Advertorial",
                                    description: "Melalui program Advertorial anda dapat menitipakan produk teknologi, kami dapat menjadi solusi untuk menjual produk anda di website dan semusa sosial media kami, supaya produk teknologi bisnis kamu dapat dikenal lebih cepat lagi melalui CodePolitan.",
                                    partners: _Data_Static__WEBPACK_IMPORTED_MODULE_3__/* .advertorial */ ._Q,
                                    image: "/assets/img/forcompany/Group 2444.png"
                                })
                            })
                        ]
                    })
                ]
            }),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_Modal_ModalTechAdoption__WEBPACK_IMPORTED_MODULE_1__/* ["default"] */ .Z, {
                data: modalData,
                dataSwiper: dataSwiper
            })
        ]
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (SectionTechAdoption);

});

/***/ }),

/***/ 3057:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.a(module, async (__webpack_handle_async_dependencies__) => {
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "getServerSideProps": () => (/* binding */ getServerSideProps),
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2167);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(968);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _components_global_Layout_Layout__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(4528);
/* harmony import */ var _components_forcompany_Hero_Hero__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(1435);
/* harmony import */ var _components_global_Sections_SectionCounter_SectionCounter__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(5919);
/* harmony import */ var _components_global_Sections_SectionPartners_SectionPartners__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(3858);
/* harmony import */ var _components_global_Sections_SectionCTA_SectionCTA__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(122);
/* harmony import */ var _components_forcompany_Sections_SectionTallentScout_SectionTallentScout__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(4402);
/* harmony import */ var _components_forcompany_Sections_SectionTechAdoption_SectionTechAdoption__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(2383);
/* harmony import */ var _components_forcompany_Sections_SectionCorporateTraining_SectionCorporateTraining__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(7910);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_components_forcompany_Sections_SectionCorporateTraining_SectionCorporateTraining__WEBPACK_IMPORTED_MODULE_10__, _components_forcompany_Sections_SectionTechAdoption_SectionTechAdoption__WEBPACK_IMPORTED_MODULE_9__]);
([_components_forcompany_Sections_SectionCorporateTraining_SectionCorporateTraining__WEBPACK_IMPORTED_MODULE_10__, _components_forcompany_Sections_SectionTechAdoption_SectionTechAdoption__WEBPACK_IMPORTED_MODULE_9__] = __webpack_async_dependencies__.then ? await __webpack_async_dependencies__ : __webpack_async_dependencies__);











const getServerSideProps = async ()=>{
    const statsRes = await axios__WEBPACK_IMPORTED_MODULE_1___default().get(`${"https://api.codepolitan.com"}/v1/stats`);
    const stats = statsRes.data;
    return {
        props: {
            stats
        }
    };
};
const ForCompany = ({ stats  })=>{
    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
        children: [
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)((next_head__WEBPACK_IMPORTED_MODULE_2___default()), {
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("title", {
                        children: "Codepolitan for Company - Codepolitan"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        name: "title",
                        content: "Codepolitan For Company - Codepolitan"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        name: "description",
                        content: "Jual bisnis digital, mencari talent yang berbakat hingga meningkatkan kemampuan digital tim IT perusahaan anda bersama CodePolitan adalah langkah terbaik anda saat ini."
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:type",
                        content: "website"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:url",
                        content: "https://codepolitan.com/for-company"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:title",
                        content: "Codepolitan For Company - Codepolitan"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:description",
                        content: "Jual bisnis digital, mencari talent yang berbakat hingga meningkatkan kemampuan digital tim IT perusahaan anda bersama CodePolitan adalah langkah terbaik anda saat ini."
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:image",
                        content: "https://i.ibb.co/gTFrzNL/OG-For-Company.png"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:card",
                        content: "summary_large_image"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:url",
                        content: "https://codepolitan.com/for-company"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:title",
                        content: "Codepolitan For Company - Codepolitan"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:description",
                        content: "Jual bisnis digital, mencari talent yang berbakat hingga meningkatkan kemampuan digital tim IT perusahaan anda bersama CodePolitan adalah langkah terbaik anda saat ini."
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:image",
                        content: "https://i.ibb.co/gTFrzNL/OG-For-Company.png"
                    })
                ]
            }),
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_components_global_Layout_Layout__WEBPACK_IMPORTED_MODULE_3__/* ["default"] */ .Z, {
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_forcompany_Hero_Hero__WEBPACK_IMPORTED_MODULE_4__/* ["default"] */ .Z, {}),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_global_Sections_SectionCounter_SectionCounter__WEBPACK_IMPORTED_MODULE_5__/* ["default"] */ .Z, {
                        data: stats
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_global_Sections_SectionPartners_SectionPartners__WEBPACK_IMPORTED_MODULE_6__/* ["default"] */ .Z, {
                        backgroundColor: "bg-light",
                        sectionTitleTextStyle: "text-center"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_forcompany_Sections_SectionTechAdoption_SectionTechAdoption__WEBPACK_IMPORTED_MODULE_9__/* ["default"] */ .Z, {}),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_forcompany_Sections_SectionCorporateTraining_SectionCorporateTraining__WEBPACK_IMPORTED_MODULE_10__/* ["default"] */ .Z, {}),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_forcompany_Sections_SectionTallentScout_SectionTallentScout__WEBPACK_IMPORTED_MODULE_8__/* ["default"] */ .Z, {}),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: "bg-grey",
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_global_Sections_SectionCTA_SectionCTA__WEBPACK_IMPORTED_MODULE_7__/* ["default"] */ .Z, {
                            caption: "Mulai Menjadi Partner Bisnis Kami Sekarang? Klik Disini",
                            btnTitle: "Hubungi Kami Sekarang",
                            link: "https://wa.me/628999488990"
                        })
                    })
                ]
            })
        ]
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ForCompany);

});

/***/ }),

/***/ 5368:
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/free-brands-svg-icons");

/***/ }),

/***/ 197:
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/free-regular-svg-icons");

/***/ }),

/***/ 6466:
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/free-solid-svg-icons");

/***/ }),

/***/ 7197:
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/react-fontawesome");

/***/ }),

/***/ 2167:
/***/ ((module) => {

"use strict";
module.exports = require("axios");

/***/ }),

/***/ 2733:
/***/ ((module) => {

"use strict";
module.exports = require("fuse.js");

/***/ }),

/***/ 5804:
/***/ ((module) => {

"use strict";
module.exports = require("mixpanel-browser");

/***/ }),

/***/ 2245:
/***/ ((module) => {

"use strict";
module.exports = require("moment");

/***/ }),

/***/ 5518:
/***/ ((module) => {

"use strict";
module.exports = require("moment/locale/id");

/***/ }),

/***/ 562:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/server/denormalize-page-path.js");

/***/ }),

/***/ 8028:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/server/image-config.js");

/***/ }),

/***/ 4957:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/head.js");

/***/ }),

/***/ 4014:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/i18n/normalize-locale-path.js");

/***/ }),

/***/ 8524:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/is-plain-object.js");

/***/ }),

/***/ 8020:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/mitt.js");

/***/ }),

/***/ 4964:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router-context.js");

/***/ }),

/***/ 9565:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/get-asset-path-from-route.js");

/***/ }),

/***/ 4365:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/get-middleware-regex.js");

/***/ }),

/***/ 1428:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/is-dynamic.js");

/***/ }),

/***/ 1292:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/parse-relative-url.js");

/***/ }),

/***/ 979:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/querystring.js");

/***/ }),

/***/ 6052:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/resolve-rewrites.js");

/***/ }),

/***/ 4226:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/route-matcher.js");

/***/ }),

/***/ 5052:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/route-regex.js");

/***/ }),

/***/ 9232:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/utils.js");

/***/ }),

/***/ 968:
/***/ ((module) => {

"use strict";
module.exports = require("next/head");

/***/ }),

/***/ 1853:
/***/ ((module) => {

"use strict";
module.exports = require("next/router");

/***/ }),

/***/ 6689:
/***/ ((module) => {

"use strict";
module.exports = require("react");

/***/ }),

/***/ 609:
/***/ ((module) => {

"use strict";
module.exports = require("react-countup");

/***/ }),

/***/ 997:
/***/ ((module) => {

"use strict";
module.exports = require("react/jsx-runtime");

/***/ }),

/***/ 3877:
/***/ ((module) => {

"use strict";
module.exports = import("swiper");;

/***/ }),

/***/ 3015:
/***/ ((module) => {

"use strict";
module.exports = import("swiper/react");;

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [7730,9285,1664,5675,4528,4986,8508,5919], () => (__webpack_exec__(3057)));
module.exports = __webpack_exports__;

})();