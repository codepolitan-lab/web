(() => {
var exports = {};
exports.id = 2521;
exports.ids = [2521];
exports.modules = {

/***/ 7720:
/***/ ((module) => {

// Exports
module.exports = {
	"background": "SectionCounter_background__PNqrB"
};


/***/ }),

/***/ 5955:
/***/ ((module) => {

// Exports
module.exports = {
	"wrapper": "SectionIntro_wrapper__Itkf7",
	"card_image": "SectionIntro_card_image__RHT8E",
	"card": "SectionIntro_card__KAOzT",
	"text_wrapper": "SectionIntro_text_wrapper__varsY"
};


/***/ }),

/***/ 7813:
/***/ ((module) => {

// Exports
module.exports = {
	"dotted_line": "SectionTimeline_dotted_line__9KYxZ",
	"circle": "SectionTimeline_circle__kqJsF",
	"year": "SectionTimeline_year__ANPqa"
};


/***/ }),

/***/ 2269:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _SectionCounter_module_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(7720);
/* harmony import */ var _SectionCounter_module_scss__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_SectionCounter_module_scss__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_countup__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(609);
/* harmony import */ var react_countup__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_countup__WEBPACK_IMPORTED_MODULE_1__);



const SectionCounter = ({ data  })=>{
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("section", {
        className: `section ${(_SectionCounter_module_scss__WEBPACK_IMPORTED_MODULE_2___default().background)}`,
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
            className: "container p-4 p-lg-5",
            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                className: "row justify-content-around text-white text-center mt-5 mt-lg-3",
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: "col-4 col-lg-2",
                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("strong", {
                            children: [
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((react_countup__WEBPACK_IMPORTED_MODULE_1___default()), {
                                    className: "h1",
                                    delay: 3,
                                    end: data.total_course
                                }),
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                    children: "Kelas Online"
                                })
                            ]
                        })
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: "col-4 col-lg-2",
                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("strong", {
                            children: [
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((react_countup__WEBPACK_IMPORTED_MODULE_1___default()), {
                                    className: "h1",
                                    delay: 3,
                                    end: data.total_study
                                }),
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                    children: "Materi Belajar"
                                })
                            ]
                        })
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: "col-4 col-lg-2",
                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("strong", {
                            children: [
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((react_countup__WEBPACK_IMPORTED_MODULE_1___default()), {
                                    className: "h1",
                                    delay: 3,
                                    end: data.total_hours
                                }),
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                    children: "Jam Pelajaran"
                                })
                            ]
                        })
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: "col-4 col-lg-2",
                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("strong", {
                            children: [
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((react_countup__WEBPACK_IMPORTED_MODULE_1___default()), {
                                    className: "h1",
                                    delay: 3,
                                    end: data.total_path
                                }),
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                    children: "Roadmap"
                                })
                            ]
                        })
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: "col-4 col-lg-2",
                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("strong", {
                            children: [
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((react_countup__WEBPACK_IMPORTED_MODULE_1___default()), {
                                    className: "h1",
                                    delay: 3,
                                    end: data.total_tutorial
                                }),
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                    children: "Tutorial"
                                })
                            ]
                        })
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: "col-4 col-lg-2",
                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("strong", {
                            children: [
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((react_countup__WEBPACK_IMPORTED_MODULE_1___default()), {
                                    className: "h1",
                                    delay: 3,
                                    end: data.total_webinar
                                }),
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                    children: "Webinar"
                                })
                            ]
                        })
                    })
                ]
            })
        })
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (SectionCounter);


/***/ }),

/***/ 9892:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _SectionIntro_module_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5955);
/* harmony import */ var _SectionIntro_module_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_SectionIntro_module_scss__WEBPACK_IMPORTED_MODULE_1__);


const SectionIntro = ()=>{
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("section", {
        className: "section bg-light",
        style: {
            paddingTop: '80px'
        },
        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
            className: "container p-4 p-lg-5",
            children: [
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: "row d-none d-lg-block",
                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: `col ${(_SectionIntro_module_scss__WEBPACK_IMPORTED_MODULE_1___default().wrapper)}`,
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                className: (_SectionIntro_module_scss__WEBPACK_IMPORTED_MODULE_1___default().card_image),
                                src: "/assets/img/about/img-about-2.png",
                                alt: "Tentang Codepolitan"
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: `${(_SectionIntro_module_scss__WEBPACK_IMPORTED_MODULE_1___default().card)} card bg-codepolitan border-0 float-end`,
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "card-body",
                                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        className: (_SectionIntro_module_scss__WEBPACK_IMPORTED_MODULE_1___default().text_wrapper),
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                className: "text-white",
                                                children: "Codepolitan adalah platform belajar pemrograman secara online dengan berbahasa Indonesia yang dikembangkan khusus untuk membantu generasi muda belajar dan berlatih pemrograman agar kompetitif di era Industri 4.0."
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                className: "text-white",
                                                children: "Dalam 5 tahun, Codepolitan berkembang menjadi platform belajar pemrograman berbahasa Indonesia dan menjadi sumber terpercaya bagi generasi muda Indonesia."
                                            })
                                        ]
                                    })
                                })
                            })
                        ]
                    })
                }),
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: "row d-lg-none",
                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: "col mt-5",
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                className: "text-muted",
                                children: "Codepolitan adalah platform belajar pemrograman secara online dengan berbahasa Indonesia yang dikembangkan khusus untuk membantu generasi muda belajar dan berlatih pemrograman agar kompetitif di era Industri 4.0."
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                className: "text-muted",
                                children: "Dalam 5 tahun, Codepolitan berkembang menjadi platform belajar pemrograman berbahasa Indonesia dan menjadi sumber terpercaya bagi generasi muda Indonesia."
                            })
                        ]
                    })
                })
            ]
        })
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (SectionIntro);


/***/ }),

/***/ 4382:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ SectionTeam_SectionTeam)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: external "@fortawesome/free-brands-svg-icons"
var free_brands_svg_icons_ = __webpack_require__(5368);
// EXTERNAL MODULE: external "@fortawesome/free-solid-svg-icons"
var free_solid_svg_icons_ = __webpack_require__(6466);
// EXTERNAL MODULE: external "@fortawesome/react-fontawesome"
var react_fontawesome_ = __webpack_require__(7197);
;// CONCATENATED MODULE: ./src/components/about/Cards/CardTeam/CardTeam.js




const CardTeam = ({ thumbnail , name , role , instagramLink , facebookLink , linkedinLink  })=>{
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
        className: "col-md-6 col-lg-4 col-xl-3 mb-4",
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx("img", {
                className: "img-fluid w-100",
                loading: "lazy",
                src: thumbnail || "/assets/img/placeholder.jpg",
                alt: name
            }),
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                className: "my-3 text-center",
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx("h5", {
                        className: "section-title",
                        children: name
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("p", {
                        className: "text-muted mb-2",
                        children: role
                    }),
                    facebookLink && /*#__PURE__*/ jsx_runtime_.jsx("a", {
                        className: "link text-primary mx-1",
                        href: facebookLink,
                        target: "_blank",
                        rel: "noopener noreferrer",
                        title: "Facebook",
                        children: /*#__PURE__*/ jsx_runtime_.jsx(react_fontawesome_.FontAwesomeIcon, {
                            size: "lg",
                            icon: free_brands_svg_icons_.faFacebook
                        })
                    }),
                    instagramLink && /*#__PURE__*/ jsx_runtime_.jsx("a", {
                        className: "link text-primary mx-1",
                        href: instagramLink,
                        target: "_blank",
                        rel: "noopener noreferrer",
                        title: "Instagram",
                        children: /*#__PURE__*/ jsx_runtime_.jsx(react_fontawesome_.FontAwesomeIcon, {
                            size: "lg",
                            icon: free_brands_svg_icons_.faInstagram
                        })
                    }),
                    linkedinLink && /*#__PURE__*/ jsx_runtime_.jsx("a", {
                        className: "link text-primary mx-1",
                        href: linkedinLink,
                        target: "_blank",
                        rel: "noopener noreferrer",
                        title: "LinkedIn",
                        children: /*#__PURE__*/ jsx_runtime_.jsx(react_fontawesome_.FontAwesomeIcon, {
                            size: "lg",
                            icon: free_brands_svg_icons_.faLinkedin
                        })
                    })
                ]
            })
        ]
    }));
};
/* harmony default export */ const CardTeam_CardTeam = (CardTeam);

;// CONCATENATED MODULE: ./src/components/about/Sections/SectionTeam/SectionTeam.js



const SectionTeam = ()=>{
    const { 0: teams  } = (0,external_react_.useState)([
        {
            thumbnail: '/assets/img/about/kresna.png',
            name: 'Kresna Galuh',
            role: 'CEO & Founder',
            instagram_link: 'https://instagram.com/kresnagaluh.id',
            facebook_link: 'https://facebook.com/kresnagaluh',
            linkedin_link: 'https://linkedin.com/in/kresnagaluh'
        },
        {
            thumbnail: '/assets/img/about/singgih.png',
            name: 'M Singgih Z.A.',
            role: 'CMO & Co-Founder',
            instagram_link: 'https://instagram.com/hirokakaoshi/',
            facebook_link: 'https://facebook.com/hirokakaoshi/',
            linkedin_link: 'https://linkedin.com/in/msinggihza/'
        },
        {
            thumbnail: '/assets/img/about/oriza.png',
            name: 'Ahmad Oriza',
            role: 'CTO & Co-Founder',
            instagram_link: 'https://instagram.com/oriza.ahmad/',
            facebook_link: 'https://facebook.com/ahmad.oriza/',
            linkedin_link: 'https://linkedin.com/in/ahmadoriza/'
        },
        {
            thumbnail: '/assets/img/about/palupi.png',
            name: 'Hadyan Palupi',
            role: 'COO & Co-Founder',
            instagram_link: 'https://instagram.com/hadyan.palupi/',
            facebook_link: 'https://facebook.com/hadyan.palupi/',
            linkedin_link: 'https://linkedin.com/in/hadyan-palupi-974445163'
        },
        {
            thumbnail: '/assets/img/about/tony.png',
            name: 'Toni Haryanto',
            role: 'CPO & Co-Founder',
            instagram_link: 'https://www.instagram.com/yllumi/',
            facebook_link: 'https://www.facebook.com/toharyan/',
            linkedin_link: 'https://www.linkedin.com/in/toniharyanto/'
        },
        {
            thumbnail: '/assets/img/about/yudha.png',
            name: 'Yudha Pangesti D. Syailendra',
            role: 'Graphic Designer',
            instagram_link: 'https://www.instagram.com/yudhapangesti/',
            facebook_link: 'https://www.facebook.com/yudhapangesti/',
            linkedin_link: ''
        },
        {
            thumbnail: '/assets/img/about/Finlandia Wibisana.png',
            name: 'Finlandia Wibisana',
            role: 'Video Editor',
            instagram_link: 'https://www.instagram.com/finlandia_w',
            facebook_link: '',
            linkedin_link: 'https://www.linkedin.com/in/finlandia-wibisana'
        },
        {
            thumbnail: '/assets/img/about/novan.png',
            name: 'Novan Junaedi',
            role: 'Frontend Developer',
            instagram_link: 'https://www.instagram.com/novanjunaedi',
            facebook_link: 'https://www.facebook.com/novanjunaedi98',
            linkedin_link: 'https://www.linkedin.com/in/novanjunaedi'
        },
        {
            thumbnail: '/assets/img/about/Aldiansyah Ibrahim.png',
            name: 'Aldiansyah Ibrahim',
            role: 'Frontend Developer',
            instagram_link: 'https://www.instagram.com/aldianbaim',
            facebook_link: 'https://www.facebook.com/aldiansyahibr',
            linkedin_link: ''
        },
        {
            thumbnail: '/assets/img/about/Badar Abdi Mulya.png',
            name: 'Badar Abdi Mulya',
            role: 'UI/UX Designer',
            instagram_link: 'https://instagram.com/badartjan',
            facebook_link: '',
            linkedin_link: 'https://linkedin.com/in/badar-abdi-mulya-168866124'
        },
        {
            thumbnail: '',
            name: 'Fauzan Abdul Hakim',
            role: 'Social Media Officer',
            instagram_link: 'https://instagram.com/fauzanahak',
            facebook_link: 'https://facebook.com/fauzanaha',
            linkedin_link: 'https://linkedin.com/in/fauzanaha'
        },
        {
            thumbnail: '/assets/img/about/Chandra Trio Pamungkas.png',
            name: 'Chandra Trio Pamungkas',
            role: 'Digital Marketer',
            instagram_link: 'https://instagram.com/chandratrio',
            facebook_link: '',
            linkedin_link: 'https://linkedin.com/in/chandra-trio-pamungkas-089153157'
        },
        {
            thumbnail: '/assets/img/about/Jajat Sudrajat.png',
            name: 'Jajat Sudrajat',
            role: 'Office Support',
            instagram_link: 'https://instagram.com/',
            facebook_link: 'https://facebook.com',
            linkedin_link: 'https://linkedin.com/in/'
        }, 
    ]);
    return(/*#__PURE__*/ jsx_runtime_.jsx("section", {
        className: "section",
        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
            className: "container p-4 p-lg-5",
            children: [
                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                    className: "row mb-5",
                    children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        className: "col text-center",
                        children: /*#__PURE__*/ jsx_runtime_.jsx("h3", {
                            className: "section-title",
                            children: "Team CodePolitan"
                        })
                    })
                }),
                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                    className: "row justify-content-center",
                    children: teams.map((team, index)=>{
                        return(/*#__PURE__*/ jsx_runtime_.jsx(CardTeam_CardTeam, {
                            thumbnail: team.thumbnail,
                            name: team.name,
                            role: team.role,
                            instagramLink: team.instagram_link,
                            facebookLink: team.facebook_link,
                            linkedinLink: team.linkedin_link
                        }, index));
                    })
                })
            ]
        })
    }));
};
/* harmony default export */ const SectionTeam_SectionTeam = (SectionTeam);


/***/ }),

/***/ 3350:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _SectionTimeline_module_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(7813);
/* harmony import */ var _SectionTimeline_module_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_SectionTimeline_module_scss__WEBPACK_IMPORTED_MODULE_1__);


const SectionTimeline = ()=>{
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("section", {
        className: "section bg-light",
        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
            className: "container p-4 p-lg-5",
            children: [
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: "row mb-5",
                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: "col",
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h1", {
                            className: "section-title h3",
                            children: "Sejarah Singkat CodePolitan"
                        })
                    })
                }),
                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                    className: "d-flex align-items-start border-start border-bottom border-5 py-3",
                    children: [
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                            className: (_SectionTimeline_module_scss__WEBPACK_IMPORTED_MODULE_1___default().circle) + ' me-2 my-auto'
                        }),
                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                            className: "row w-100",
                            children: [
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "col-3 my-auto",
                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "card bg-codepolitan border-0 shadow-sm",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: "card-body text-center text-white",
                                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                className: (_SectionTimeline_module_scss__WEBPACK_IMPORTED_MODULE_1___default().year),
                                                children: [
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                        className: "d-none d-lg-block",
                                                        children: "2016"
                                                    }),
                                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                                                        className: "d-block d-lg-none",
                                                        children: [
                                                            "20 ",
                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                                            " 16"
                                                        ]
                                                    })
                                                ]
                                            })
                                        })
                                    })
                                }),
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "col-9",
                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "card border-0 shadow-sm h-100",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: "card-body text-muted",
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                children: "Kami memulai segala sesuatunya dengan Portal Media Online yang membahas tentang pemrograman dan teknologi"
                                            })
                                        })
                                    })
                                })
                            ]
                        })
                    ]
                }),
                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                    className: "d-flex align-items-start border-end border-bottom border-5 py-3",
                    children: [
                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                            className: "row",
                            children: [
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "col-3 my-auto",
                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "card bg-codepolitan border-0 shadow-sm",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: "card-body text-center text-white",
                                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                className: (_SectionTimeline_module_scss__WEBPACK_IMPORTED_MODULE_1___default().year),
                                                children: [
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                        className: "d-none d-lg-block",
                                                        children: "2017"
                                                    }),
                                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                                                        className: "d-block d-lg-none",
                                                        children: [
                                                            "20 ",
                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                                            " 17"
                                                        ]
                                                    })
                                                ]
                                            })
                                        })
                                    })
                                }),
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "col-9 order-first",
                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "card border-0 shadow-sm h-100",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: "card-body text-muted",
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                children: "Lahirnya Program Coding Bootcamp, Codepolitan Developer School. Menyelenggarakan salah satu event terbesar programmer di Indonesia, Indonesia Developer Summit."
                                            })
                                        })
                                    })
                                })
                            ]
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                            className: (_SectionTimeline_module_scss__WEBPACK_IMPORTED_MODULE_1___default().circle) + ' ms-2 my-auto'
                        })
                    ]
                }),
                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                    className: "d-flex align-items-start border-start border-bottom border-5 py-3",
                    children: [
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                            className: (_SectionTimeline_module_scss__WEBPACK_IMPORTED_MODULE_1___default().circle) + ' me-2 my-auto'
                        }),
                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                            className: "row w-100",
                            children: [
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "col-3 my-auto",
                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "card bg-codepolitan border-0 shadow-sm",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: "card-body text-center text-white",
                                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                className: (_SectionTimeline_module_scss__WEBPACK_IMPORTED_MODULE_1___default().year),
                                                children: [
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                        className: "d-none d-lg-block",
                                                        children: "2018"
                                                    }),
                                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                                                        className: "d-block d-lg-none",
                                                        children: [
                                                            "20 ",
                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                                            " 18"
                                                        ]
                                                    })
                                                ]
                                            })
                                        })
                                    })
                                }),
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "col-9",
                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "card border-0 shadow-sm h-100",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: "card-body text-muted",
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                children: "Diluncurkan program Kelas Online Premium Membership, dengan 1000 purchase di bulan pertama peluncuran."
                                            })
                                        })
                                    })
                                })
                            ]
                        })
                    ]
                }),
                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                    className: "d-flex align-items-start border-end border-bottom border-5 py-3",
                    children: [
                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                            className: "row",
                            children: [
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "col-3 my-auto",
                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "card bg-codepolitan border-0 shadow-sm",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: "card-body text-center text-white",
                                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                className: (_SectionTimeline_module_scss__WEBPACK_IMPORTED_MODULE_1___default().year),
                                                children: [
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                        className: "d-none d-lg-block",
                                                        children: "2019"
                                                    }),
                                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                                                        className: "d-block d-lg-none",
                                                        children: [
                                                            "20 ",
                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                                            " 19"
                                                        ]
                                                    })
                                                ]
                                            })
                                        })
                                    })
                                }),
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "col-9 order-first",
                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "card border-0 shadow-sm h-100",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: "card-body text-muted",
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                children: "Peluncuran program Kelas Online Lifetime dan menjadi salah satu perusahaan terdepan dan konsisten yang memberikan pembelajaran dalam bidang programming di Indonesia."
                                            })
                                        })
                                    })
                                })
                            ]
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                            className: (_SectionTimeline_module_scss__WEBPACK_IMPORTED_MODULE_1___default().circle) + ' ms-2 my-auto'
                        })
                    ]
                }),
                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                    className: "d-flex align-items-start border-start border-bottom border-5 py-3",
                    children: [
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                            className: (_SectionTimeline_module_scss__WEBPACK_IMPORTED_MODULE_1___default().circle) + ' me-2 my-auto'
                        }),
                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                            className: "row w-100",
                            children: [
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "col-3 my-auto",
                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "card bg-codepolitan border-0 shadow-sm",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: "card-body text-center text-white",
                                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                className: (_SectionTimeline_module_scss__WEBPACK_IMPORTED_MODULE_1___default().year),
                                                children: [
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                        className: "d-none d-lg-block",
                                                        children: "2020"
                                                    }),
                                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                                                        className: "d-block d-lg-none",
                                                        children: [
                                                            "20 ",
                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                                            " 20"
                                                        ]
                                                    })
                                                ]
                                            })
                                        })
                                    })
                                }),
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "col-9",
                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "card border-0 shadow-sm h-100",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: "card-body text-muted",
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                children: "Menjadi salah satu perusahaan yang bertahan di masa Pandemi dengan inovasi dalam program Kelas Online. Peluncuran program Kampus Coding dan Geekmentor."
                                            })
                                        })
                                    })
                                })
                            ]
                        })
                    ]
                }),
                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                    className: "d-flex align-items-start border-end border-5 py-3",
                    children: [
                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                            className: "row",
                            children: [
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "col-3 my-auto",
                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "card bg-codepolitan border-0 shadow-sm",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: "card-body text-center text-white",
                                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                className: (_SectionTimeline_module_scss__WEBPACK_IMPORTED_MODULE_1___default().year),
                                                children: [
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                        className: "d-none d-lg-block",
                                                        children: "2021"
                                                    }),
                                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                                                        className: "d-block d-lg-none",
                                                        children: [
                                                            "20 ",
                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}),
                                                            " 21"
                                                        ]
                                                    })
                                                ]
                                            })
                                        })
                                    })
                                }),
                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "col-9 order-first",
                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "card border-0 shadow-sm h-100",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: "card-body text-muted",
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                children: "Menyelenggarakan event challenge nasional bersama perusahaan Internasional Alibaba Cloud dan Here Maps, kampanye IT ke berbagai desa dalam program Desa Digital, inisiasi partnership dengan mentor eksternal."
                                            })
                                        })
                                    })
                                })
                            ]
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                            className: (_SectionTimeline_module_scss__WEBPACK_IMPORTED_MODULE_1___default().circle) + ' ms-2 my-auto'
                        })
                    ]
                }),
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: "d-flex align-items-end",
                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("hr", {
                        style: {
                            width: '60%',
                            marginTop: 0,
                            marginBottom: 0
                        },
                        className: (_SectionTimeline_module_scss__WEBPACK_IMPORTED_MODULE_1___default().dotted_line) + ' ms-auto'
                    })
                }),
                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                    className: "d-flex align-items-start",
                    children: [
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("hr", {
                            style: {
                                width: '49%',
                                marginTop: 0,
                                borderColor: 'transparent'
                            },
                            className: (_SectionTimeline_module_scss__WEBPACK_IMPORTED_MODULE_1___default().dotted_line)
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                            className: (_SectionTimeline_module_scss__WEBPACK_IMPORTED_MODULE_1___default().circle) + ' my-1'
                        })
                    ]
                }),
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: "postion-relative",
                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: "position-absolute start-50 translate-middle-x",
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "card bg-codepolitan text-center border-white border-3",
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: "card-body px-5 text-white",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                    className: "h3",
                                    children: "Today"
                                })
                            })
                        })
                    })
                })
            ]
        })
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (SectionTimeline);


/***/ }),

/***/ 8500:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.a(module, async (__webpack_handle_async_dependencies__) => {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var swiper_react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3015);
/* harmony import */ var swiper__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3877);
/* harmony import */ var next_image__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(5675);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([swiper__WEBPACK_IMPORTED_MODULE_3__, swiper_react__WEBPACK_IMPORTED_MODULE_2__]);
([swiper__WEBPACK_IMPORTED_MODULE_3__, swiper_react__WEBPACK_IMPORTED_MODULE_2__] = __webpack_async_dependencies__.then ? await __webpack_async_dependencies__ : __webpack_async_dependencies__);





swiper__WEBPACK_IMPORTED_MODULE_3__["default"].use([
    swiper__WEBPACK_IMPORTED_MODULE_3__.Autoplay
]);
const SectionPartnerCampus = ()=>{
    const { 0: campusPartners  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)([
        {
            name: 'AMIK Bumi Nusantara Cirebon',
            thumbnail: 'https://image.web.id/images/2022/06/30/99a332ec1d2b33139df6d49043f86df4.png'
        },
        {
            name: 'ITB',
            thumbnail: 'https://image.web.id/images/2022/06/30/cd919f34ee20d27c16a9e3fc437c3ba1.png'
        },
        {
            name: 'PNJ',
            thumbnail: 'https://image.web.id/images/2022/06/30/8aa8b63d4679f96d1b8b0e58a82f60cf.png'
        },
        {
            name: 'PNP',
            thumbnail: 'https://image.web.id/images/2022/06/30/c8d7f5e96ab1e17df1ff9901425ad949.png'
        },
        {
            name: 'UII',
            thumbnail: 'https://image.web.id/images/2022/06/30/0b1dcaee983cf698c71f4511f5c02e07.png'
        },
        {
            name: 'Universitas Jember',
            thumbnail: 'https://image.web.id/images/2022/06/30/1ed6bc00d0a70682b46f03467fb6cfcf.png'
        },
        {
            name: 'UNP',
            thumbnail: 'https://image.web.id/images/2022/06/30/aa758139224939e2b91b93fafa86d167.png'
        },
        {
            name: 'UNPAD',
            thumbnail: 'https://image.web.id/images/2022/06/30/8a5eb01267d8ea23d51bdb3ac6cb5bd2.png'
        },
        {
            name: 'UNS',
            thumbnail: 'https://image.web.id/images/2022/06/30/40f757ba9a1782d1aeeab26d7c3177bf.png'
        },
        {
            name: 'UPI',
            thumbnail: 'https://image.web.id/images/2022/06/30/0ec158d933a1c3ff2391f792ecbeb9bf.png'
        },
        {
            name: 'UPJ',
            thumbnail: 'https://image.web.id/images/2022/06/30/499b6168350f8dec7c3b78488fa11b98.png'
        },
        {
            name: 'URINDO',
            thumbnail: 'https://image.web.id/images/2022/06/30/508eb7a0aa607c24ae6f3b3e4c30e03c.png'
        },
        {
            name: 'USTJ',
            thumbnail: 'https://image.web.id/images/2022/06/30/6a9b32f259ce603a7ca4b02cf7040c4b.png'
        }, 
    ]);
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("section", {
        className: "bg-light",
        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
            className: "container p-4 p-lg-5",
            children: [
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: "row my-4",
                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: "col",
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h3", {
                            className: "text-muted text-center",
                            children: "Mitra Kampus Kami"
                        })
                    })
                }),
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: "row justify-content-center",
                    children: campusPartners.map((partner, index)=>{
                        return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "col-4 col-lg-2",
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: "card bg-transparent border-0",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "card-body py-3",
                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_image__WEBPACK_IMPORTED_MODULE_4__["default"], {
                                        src: partner.thumbnail,
                                        placeholder: "blur",
                                        blurDataURL: "/assets/img/placeholder.jpg",
                                        alt: partner.name,
                                        layout: "responsive",
                                        width: 50,
                                        height: 30,
                                        objectFit: "contain"
                                    })
                                })
                            })
                        }, index));
                    }).slice(0, 5)
                }),
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: "row justify-content-center",
                    children: campusPartners.map((partner, index)=>{
                        return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "col-4 col-lg-2",
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: "card bg-transparent border-0",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "card-body py-3",
                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_image__WEBPACK_IMPORTED_MODULE_4__["default"], {
                                        src: partner.thumbnail,
                                        placeholder: "blur",
                                        blurDataURL: "/assets/img/placeholder.jpg",
                                        alt: partner.name,
                                        layout: "responsive",
                                        width: 50,
                                        height: 30,
                                        objectFit: "contain"
                                    })
                                })
                            })
                        }, index));
                    }).slice(5, 10)
                }),
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: "row justify-content-center",
                    children: campusPartners.map((partner, index)=>{
                        return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "col-4 col-lg-2",
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: "card bg-transparent border-0",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "card-body py-3",
                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_image__WEBPACK_IMPORTED_MODULE_4__["default"], {
                                        src: partner.thumbnail,
                                        placeholder: "blur",
                                        blurDataURL: "/assets/img/placeholder.jpg",
                                        alt: partner.name,
                                        layout: "responsive",
                                        width: 50,
                                        height: 30,
                                        objectFit: "contain"
                                    })
                                })
                            })
                        }, index));
                    }).slice(10, 13)
                })
            ]
        })
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (SectionPartnerCampus);

});

/***/ }),

/***/ 9604:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.a(module, async (__webpack_handle_async_dependencies__) => {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var swiper_react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3015);
/* harmony import */ var swiper__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3877);
/* harmony import */ var next_image__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(5675);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([swiper__WEBPACK_IMPORTED_MODULE_3__, swiper_react__WEBPACK_IMPORTED_MODULE_2__]);
([swiper__WEBPACK_IMPORTED_MODULE_3__, swiper_react__WEBPACK_IMPORTED_MODULE_2__] = __webpack_async_dependencies__.then ? await __webpack_async_dependencies__ : __webpack_async_dependencies__);





swiper__WEBPACK_IMPORTED_MODULE_3__["default"].use([
    swiper__WEBPACK_IMPORTED_MODULE_3__.Autoplay
]);
const SectionPartner = ()=>{
    const { 0: partners  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)([
        {
            name: 'Kemnaker',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/KEMNAKER_MGfr7cHn61.webp'
        },
        {
            name: 'Kemenkeu',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/kemenkeu_p9YQYkw-AI.webp'
        },
        {
            name: 'Here',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/here_THgKeV7WUz.webp'
        },
        {
            name: 'Udemy',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/udemy_DVYNj94zCK14x.webp'
        },
        {
            name: 'Samsung',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/samsung_Insf8kassvK.webp'
        },
        {
            name: 'Lenovo',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/Lenovo_co1GDjBpxV.webp'
        },
        {
            name: 'Alibaba Cloud',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/Alibaba_kBCmKi7O7.webp'
        },
        {
            name: 'Intel',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/Intel_T_cVZHo81M_t.webp'
        },
        {
            name: 'Dicoding',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/dicoding-header-logo_-yUghUNkx7.webp'
        },
        {
            name: 'IBM',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/IBM_GXVYl92inl905.webp'
        },
        {
            name: 'Pixel',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/pixel_YtPl8tvzu.webp'
        },
        {
            name: 'XL',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/xl_t5bLnDwJn.webp'
        },
        {
            name: 'Hactive8',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/hacktiv8_Y3L2HUj4K4X.webp'
        },
        {
            name: 'Kudo',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/Kudo_oGHxpphEw.webp'
        },
        {
            name: 'Refactory',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/Refactory_VO89Fn5BX.webp'
        },
        {
            name: 'Ajita',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/ajita_Za3H1pSLYI.webp'
        }, 
    ]);
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("section", {
        className: "bg-light",
        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
            className: "container p-4 px-lg-5 py-lg-2",
            children: [
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: "row my-4",
                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: "col",
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h3", {
                            className: "text-muted text-center",
                            children: "Telah Dipercaya Oleh"
                        })
                    })
                }),
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: "row justify-content-center",
                    children: partners.map((partner, index)=>{
                        return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "col-4 col-lg-2",
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: "card bg-transparent border-0",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                    className: "card-body py-1",
                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_image__WEBPACK_IMPORTED_MODULE_4__["default"], {
                                        src: partner.thumbnail,
                                        placeholder: "blur",
                                        blurDataURL: "/assets/img/placeholder.jpg",
                                        alt: partner.name,
                                        layout: "responsive",
                                        width: 50,
                                        height: 30,
                                        objectFit: "contain"
                                    })
                                })
                            })
                        }, index));
                    })
                })
            ]
        })
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (SectionPartner);

});

/***/ }),

/***/ 4400:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.a(module, async (__webpack_handle_async_dependencies__) => {
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "getStaticProps": () => (/* binding */ getStaticProps),
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(968);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_about_Sections_SectionIntro_SectionIntro__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(9892);
/* harmony import */ var _components_about_Sections_SectionTimeline_SectionTimeline__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3350);
/* harmony import */ var _components_about_Sections_SectionCounter_SectionCounter__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(2269);
/* harmony import */ var _components_about_Sections_SectionTeam_SectionTeam__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(4382);
/* harmony import */ var _components_global_Layout_Layout__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(4528);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(2167);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(1664);
/* harmony import */ var _components_home_Sections_SectionPartner_SectionPartner__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(9604);
/* harmony import */ var _components_home_Sections_SectionPartnerCampus_SectionPartnerCampus__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(8500);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_components_home_Sections_SectionPartnerCampus_SectionPartnerCampus__WEBPACK_IMPORTED_MODULE_10__, _components_home_Sections_SectionPartner_SectionPartner__WEBPACK_IMPORTED_MODULE_9__]);
([_components_home_Sections_SectionPartnerCampus_SectionPartnerCampus__WEBPACK_IMPORTED_MODULE_10__, _components_home_Sections_SectionPartner_SectionPartner__WEBPACK_IMPORTED_MODULE_9__] = __webpack_async_dependencies__.then ? await __webpack_async_dependencies__ : __webpack_async_dependencies__);











const getStaticProps = async ()=>{
    const { data  } = await axios__WEBPACK_IMPORTED_MODULE_7___default().get(`${"https://api.codepolitan.com"}/v1/stats`);
    const response = {
        data
    };
    const stats = response.data;
    return {
        props: {
            stats
        }
    };
};
const About = ({ stats  })=>{
    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
        children: [
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)((next_head__WEBPACK_IMPORTED_MODULE_1___default()), {
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("title", {
                        children: "Tentang Kami - Codepolitan"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        name: "title",
                        content: "Website Belajar Coding Bahasa Indonesia - Codepolitan"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        name: "description",
                        content: "Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:type",
                        content: "website"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:url",
                        content: "https://codepolitan.com"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:title",
                        content: "Website Belajar Coding Bahasa Indonesia - Codepolitan"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:description",
                        content: "Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:image",
                        content: "https://i.ibb.co/SXP8fGc/OG-image.jpg"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:card",
                        content: "summary_large_image"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:url",
                        content: "https://codepolitan.com"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:title",
                        content: "Website Belajar Coding Bahasa Indonesia - Codepolitan"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:description",
                        content: "Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:image",
                        content: "https://i.ibb.co/SXP8fGc/OG-image.jpg"
                    })
                ]
            }),
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_components_global_Layout_Layout__WEBPACK_IMPORTED_MODULE_6__/* ["default"] */ .Z, {
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_about_Sections_SectionIntro_SectionIntro__WEBPACK_IMPORTED_MODULE_2__/* ["default"] */ .Z, {}),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_about_Sections_SectionTimeline_SectionTimeline__WEBPACK_IMPORTED_MODULE_3__/* ["default"] */ .Z, {}),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_about_Sections_SectionCounter_SectionCounter__WEBPACK_IMPORTED_MODULE_4__/* ["default"] */ .Z, {
                        data: stats
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_about_Sections_SectionTeam_SectionTeam__WEBPACK_IMPORTED_MODULE_5__/* ["default"] */ .Z, {}),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_home_Sections_SectionPartner_SectionPartner__WEBPACK_IMPORTED_MODULE_9__/* ["default"] */ .Z, {}),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_home_Sections_SectionPartnerCampus_SectionPartnerCampus__WEBPACK_IMPORTED_MODULE_10__/* ["default"] */ .Z, {}),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("section", {
                        className: "section bg-codepolitan",
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "container p-4 p-lg-5",
                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                className: "row",
                                children: [
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "col-lg-5 mb-3",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                            className: "img-fluid d-block mx-auto mx-lg-0",
                                            src: "/assets/img/about/img-about.png",
                                            alt: "About Image"
                                        })
                                    }),
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        className: "col my-auto text-white mb-5 mb-lg-auto",
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h3", {
                                                className: "section-title text-white",
                                                children: "Kami Dapat Menjadi Solusi Anda"
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                className: "my-3",
                                                children: "Di Codepolitan, Tujuan kami khusus untuk membantu generasi muda belajar dan berlatih pemrograman agar kompetitif di era Industri 4.0. melalui platform belajar berbahasa Indonesia dan menjadi sumber terpercaya bagi generasi muda , perusahaan  hingga sekolah yang ada di Indonesia"
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_link__WEBPACK_IMPORTED_MODULE_8__["default"], {
                                                href: "/for-mentor",
                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("a", {
                                                    className: "btn btn-outline-light me-2 my-1",
                                                    children: "For Mentor"
                                                })
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_link__WEBPACK_IMPORTED_MODULE_8__["default"], {
                                                href: "/for-company",
                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("a", {
                                                    className: "btn btn-outline-light me-2 my-1",
                                                    children: "For Company"
                                                })
                                            })
                                        ]
                                    })
                                ]
                            })
                        })
                    })
                ]
            })
        ]
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (About);

});

/***/ }),

/***/ 5368:
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/free-brands-svg-icons");

/***/ }),

/***/ 197:
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/free-regular-svg-icons");

/***/ }),

/***/ 6466:
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/free-solid-svg-icons");

/***/ }),

/***/ 7197:
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/react-fontawesome");

/***/ }),

/***/ 2167:
/***/ ((module) => {

"use strict";
module.exports = require("axios");

/***/ }),

/***/ 2733:
/***/ ((module) => {

"use strict";
module.exports = require("fuse.js");

/***/ }),

/***/ 562:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/server/denormalize-page-path.js");

/***/ }),

/***/ 8028:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/server/image-config.js");

/***/ }),

/***/ 4957:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/head.js");

/***/ }),

/***/ 4014:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/i18n/normalize-locale-path.js");

/***/ }),

/***/ 8524:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/is-plain-object.js");

/***/ }),

/***/ 8020:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/mitt.js");

/***/ }),

/***/ 4964:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router-context.js");

/***/ }),

/***/ 9565:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/get-asset-path-from-route.js");

/***/ }),

/***/ 4365:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/get-middleware-regex.js");

/***/ }),

/***/ 1428:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/is-dynamic.js");

/***/ }),

/***/ 1292:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/parse-relative-url.js");

/***/ }),

/***/ 979:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/querystring.js");

/***/ }),

/***/ 6052:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/resolve-rewrites.js");

/***/ }),

/***/ 4226:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/route-matcher.js");

/***/ }),

/***/ 5052:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/route-regex.js");

/***/ }),

/***/ 9232:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/utils.js");

/***/ }),

/***/ 968:
/***/ ((module) => {

"use strict";
module.exports = require("next/head");

/***/ }),

/***/ 1853:
/***/ ((module) => {

"use strict";
module.exports = require("next/router");

/***/ }),

/***/ 6689:
/***/ ((module) => {

"use strict";
module.exports = require("react");

/***/ }),

/***/ 609:
/***/ ((module) => {

"use strict";
module.exports = require("react-countup");

/***/ }),

/***/ 997:
/***/ ((module) => {

"use strict";
module.exports = require("react/jsx-runtime");

/***/ }),

/***/ 3877:
/***/ ((module) => {

"use strict";
module.exports = import("swiper");;

/***/ }),

/***/ 3015:
/***/ ((module) => {

"use strict";
module.exports = import("swiper/react");;

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [7730,9285,1664,5675,4528], () => (__webpack_exec__(4400)));
module.exports = __webpack_exports__;

})();