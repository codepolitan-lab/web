(() => {
var exports = {};
exports.id = 8188;
exports.ids = [8188];
exports.modules = {

/***/ 5920:
/***/ ((module) => {

// Exports
module.exports = {
	"card": "SectionTestimonyComment_card__ipiRq",
	"img-responsive": "SectionTestimonyComment_img-responsive__9fb_n",
	"username": "SectionTestimonyComment_username__9aXpy"
};


/***/ }),

/***/ 4341:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);

const CardMentor = ({ author , thumbnail , shortDescription  })=>{
    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
        className: "card shadow-sm mx-1",
        children: [
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                className: "card-body text-center",
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                        className: "rounded-circle p-3",
                        src: thumbnail || "/assets/img/placeholder.jpg",
                        style: {
                            width: '180px',
                            height: '180px',
                            objectFit: 'cover'
                        },
                        alt: author
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h5", {
                        className: "text-secondary",
                        children: author || 'Belum ada nama'
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                        className: "text-muted",
                        children: shortDescription || /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("i", {
                            children: "Belum ada keterangan"
                        })
                    })
                ]
            }),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                className: "d-block bg-codepolitan",
                style: {
                    height: '3px',
                    width: '60%',
                    alignSelf: 'center'
                }
            })
        ]
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (CardMentor);


/***/ }),

/***/ 6761:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ SectionTestimonyComment_SectionTestimonyComment)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: external "react-simple-star-rating"
var external_react_simple_star_rating_ = __webpack_require__(7386);
// EXTERNAL MODULE: external "@fortawesome/react-fontawesome"
var react_fontawesome_ = __webpack_require__(7197);
// EXTERNAL MODULE: external "@fortawesome/free-solid-svg-icons"
var free_solid_svg_icons_ = __webpack_require__(6466);
// EXTERNAL MODULE: ./src/components/global/Sections/SectionTestimonyComment/SectionTestimonyComment.module.scss
var SectionTestimonyComment_module = __webpack_require__(5920);
var SectionTestimonyComment_module_default = /*#__PURE__*/__webpack_require__.n(SectionTestimonyComment_module);
;// CONCATENATED MODULE: external "md5-hash"
const external_md5_hash_namespaceObject = require("md5-hash");
var external_md5_hash_default = /*#__PURE__*/__webpack_require__.n(external_md5_hash_namespaceObject);
;// CONCATENATED MODULE: ./src/components/global/Sections/SectionTestimonyComment/SectionTestimonyComment.js







function SectionTestimonyComment({ rateFilter , testimony , allRate  }) {
    const { 0: limit , 1: setLimit  } = (0,external_react_.useState)(10);
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
        children: [
            allRate ? testimony.map((detail, index)=>{
                return(/*#__PURE__*/ jsx_runtime_.jsx("div", {
                    className: `card mb-3 ${(SectionTestimonyComment_module_default()).card}`,
                    children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                        className: "row align-items-center justify-content-center",
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                className: "col-3 col-lg-3 text-center",
                                children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                    src: `https://secure.gravatar.com/avatar/${external_md5_hash_default()(detail.email)}?size=200&default=mm&rating=g`,
                                    className: `rounded-circle ${(SectionTestimonyComment_module_default())["img-responsive"]}`,
                                    alt: detail.name
                                })
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                className: "col-8 col-lg-9",
                                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx("h6", {
                                            className: `pt-4 fw-bold ${(SectionTestimonyComment_module_default()).username}`,
                                            children: detail.name
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("small", {
                                            children: detail.comment == " " ? /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                children: "Tidak ada komentar"
                                            }) : detail.comment == "" ? /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                children: "Tidak ada komentar"
                                            }) : detail.comment
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                            className: "text-end me-4 my-2",
                                            children: /*#__PURE__*/ jsx_runtime_.jsx(external_react_simple_star_rating_.RatingView, {
                                                ratingValue: detail.rate,
                                                size: 20
                                            })
                                        })
                                    ]
                                })
                            })
                        ]
                    })
                }, index));
            }).slice(0, limit) : testimony.filter((item)=>item.rate == rateFilter
            ).map((detail, index)=>{
                return(/*#__PURE__*/ jsx_runtime_.jsx("div", {
                    className: "card mb-3",
                    children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                        className: "row align-items-center justify-content-center",
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                className: "col-3 text-center",
                                children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                    src: `https://secure.gravatar.com/avatar/${external_md5_hash_default()(detail.email)}?size=200&default=mm&rating=g`,
                                    className: `rounded-circle ${(SectionTestimonyComment_module_default())["img-responsive"]}`,
                                    alt: detail.name
                                })
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                className: "col-9",
                                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                    className: "ps-5 ps-lg-0",
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx("h5", {
                                            className: "pt-4",
                                            children: detail.name
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                            children: detail.comment == " " ? /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                children: "Tidak ada komentar"
                                            }) : detail.comment == "" ? /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                children: "Tidak ada komentar"
                                            }) : detail.comment
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                            className: "text-end me-4 my-2",
                                            children: /*#__PURE__*/ jsx_runtime_.jsx(external_react_simple_star_rating_.RatingView, {
                                                ratingValue: detail.rate
                                            })
                                        })
                                    ]
                                })
                            })
                        ]
                    })
                }, index));
            }).slice(0, limit),
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                className: "text-center",
                children: [
                    testimony.filter((item)=>item.rate == rateFilter
                    ).length < limit && rateFilter !== true && /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        children: "Komentar tidak ditemukan"
                    }),
                    testimony.filter((item)=>item.rate == rateFilter
                    ).length > limit && /*#__PURE__*/ (0,jsx_runtime_.jsxs)("button", {
                        className: "btn text-muted",
                        onClick: ()=>setLimit(limit + 5)
                        ,
                        children: [
                            " ",
                            /*#__PURE__*/ jsx_runtime_.jsx(react_fontawesome_.FontAwesomeIcon, {
                                icon: free_solid_svg_icons_.faRedo,
                                className: "me-1"
                            }),
                            " Muat Lainnya "
                        ]
                    })
                ]
            })
        ]
    }));
}
/* harmony default export */ const SectionTestimonyComment_SectionTestimonyComment = (SectionTestimonyComment);


/***/ }),

/***/ 1645:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6466);
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(7197);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_simple_star_rating__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(7386);
/* harmony import */ var react_simple_star_rating__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_simple_star_rating__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _utils_helper__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(4986);





function SectionTestimonyRating({ testimony , totalReview , totalRate , filterRate  }) {
    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
        children: [
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                className: "row",
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: "col-auto",
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h4", {
                            className: "section-title",
                            children: "Testimoni Oleh Siswa"
                        })
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: "col-auto text-end ms-auto pe-lg-0",
                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                            className: "btn-group",
                            role: "group",
                            children: [
                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("button", {
                                    id: "btnGroupDrop1",
                                    type: "button",
                                    className: "btn btn-outline-secondary btn-sm btn-rounded",
                                    "data-bs-toggle": "dropdown",
                                    "aria-expanded": "false",
                                    children: [
                                        "Filter ",
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__.FontAwesomeIcon, {
                                            icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_1__.faSortAlphaDown
                                        })
                                    ]
                                }),
                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("ul", {
                                    className: "dropdown-menu",
                                    "aria-labelledby": "btnGroupDrop1",
                                    children: [
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                                            className: "dropdown-item",
                                            role: "button",
                                            onClick: ()=>filterRate(true)
                                            ,
                                            children: "Semua"
                                        }),
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                                            className: "dropdown-item",
                                            role: "button",
                                            onClick: ()=>filterRate(5)
                                            ,
                                            children: "Bintang 5"
                                        }),
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                                            className: "dropdown-item",
                                            role: "button",
                                            onClick: ()=>filterRate(4)
                                            ,
                                            children: "Bintang 4"
                                        }),
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                                            className: "dropdown-item",
                                            role: "button",
                                            onClick: ()=>filterRate(3)
                                            ,
                                            children: "Bintang 3"
                                        }),
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                                            className: "dropdown-item",
                                            role: "button",
                                            onClick: ()=>filterRate(2)
                                            ,
                                            children: "Bintang 2"
                                        }),
                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                                            className: "dropdown-item",
                                            role: "button",
                                            onClick: ()=>filterRate(1)
                                            ,
                                            children: "Bintang 1"
                                        })
                                    ]
                                })
                            ]
                        })
                    })
                ]
            }),
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                className: "row my-3",
                children: [
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: "col-lg-3 text-center",
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: "display-1",
                                children: totalRate > 0 ? totalRate.toFixed(1) : 0
                            }),
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: "my-2",
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_simple_star_rating__WEBPACK_IMPORTED_MODULE_3__.RatingView, {
                                    ratingValue: Math.floor(totalRate) || 0
                                })
                            }),
                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("p", {
                                className: "fs-6",
                                children: [
                                    "(",
                                    totalReview || 0,
                                    " reviews)"
                                ]
                            })
                        ]
                    }),
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: "col-lg-9",
                        children: [
                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                className: "row pt-3",
                                children: [
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "col-3 col-lg-3 px-0 pb-3 ps-lg-3 text-end",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                            className: "me-2",
                                            children: "5 Bintang"
                                        })
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "col-7 col-lg-8 p-0 pt-2",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: "progress",
                                            style: {
                                                height: '10px'
                                            },
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                style: {
                                                    width: `${(0,_utils_helper__WEBPACK_IMPORTED_MODULE_4__/* .getPercentageTestimony */ .Nz)(testimony, 5) || 0}%`
                                                },
                                                className: "progress-bar bg-warning rounded-pill",
                                                role: "progressbar",
                                                "aria-valuenow": "75",
                                                "aria-valuemin": "0",
                                                "aria-valuemax": "100"
                                            })
                                        })
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "col-2 col-lg-1",
                                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                                            children: [
                                                (0,_utils_helper__WEBPACK_IMPORTED_MODULE_4__/* .getPercentageTestimony */ .Nz)(testimony, 5) || 0,
                                                "%"
                                            ]
                                        })
                                    })
                                ]
                            }),
                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                className: "row",
                                children: [
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "col-3 col-lg-3 px-0 pb-3 text-end",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                            className: "me-2",
                                            children: "4 Bintang"
                                        })
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "col-7 col-lg-8 p-0 pt-2",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: "progress",
                                            style: {
                                                height: '10px'
                                            },
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                style: {
                                                    width: `${(0,_utils_helper__WEBPACK_IMPORTED_MODULE_4__/* .getPercentageTestimony */ .Nz)(testimony, 4) || 0}%`
                                                },
                                                className: "progress-bar bg-warning rounded-pill",
                                                role: "progressbar",
                                                "aria-valuenow": "75",
                                                "aria-valuemin": "0",
                                                "aria-valuemax": "100"
                                            })
                                        })
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "col-2 col-lg-1",
                                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                                            children: [
                                                (0,_utils_helper__WEBPACK_IMPORTED_MODULE_4__/* .getPercentageTestimony */ .Nz)(testimony, 4) || 0,
                                                "%"
                                            ]
                                        })
                                    })
                                ]
                            }),
                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                className: "row",
                                children: [
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "col-3 col-lg-3 px-0 pb-3 text-end",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                            className: "me-2",
                                            children: "3 Bintang"
                                        })
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "col-7 col-lg-8 p-0 pt-2",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: "progress",
                                            style: {
                                                height: '10px'
                                            },
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                style: {
                                                    width: `${(0,_utils_helper__WEBPACK_IMPORTED_MODULE_4__/* .getPercentageTestimony */ .Nz)(testimony, 3) || 0}%`
                                                },
                                                className: "progress-bar bg-warning rounded-pill",
                                                role: "progressbar",
                                                "aria-valuenow": "75",
                                                "aria-valuemin": "0",
                                                "aria-valuemax": "100"
                                            })
                                        })
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "col-2 col-lg-1",
                                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                                            children: [
                                                (0,_utils_helper__WEBPACK_IMPORTED_MODULE_4__/* .getPercentageTestimony */ .Nz)(testimony, 3) || 0,
                                                "%"
                                            ]
                                        })
                                    })
                                ]
                            }),
                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                className: "row",
                                children: [
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "col-3 col-lg-3 px-0 pb-3 text-end",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                            className: "me-2",
                                            children: "2 Bintang"
                                        })
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "col-7 col-lg-8 p-0 pt-2",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: "progress",
                                            style: {
                                                height: '10px'
                                            },
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                style: {
                                                    width: `${(0,_utils_helper__WEBPACK_IMPORTED_MODULE_4__/* .getPercentageTestimony */ .Nz)(testimony, 2) || 0}%`
                                                },
                                                className: "progress-bar bg-warning rounded-pill",
                                                role: "progressbar",
                                                "aria-valuenow": "75",
                                                "aria-valuemin": "0",
                                                "aria-valuemax": "100"
                                            })
                                        })
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "col-2 col-lg-1",
                                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                                            children: [
                                                (0,_utils_helper__WEBPACK_IMPORTED_MODULE_4__/* .getPercentageTestimony */ .Nz)(testimony, 2) || 0,
                                                "%"
                                            ]
                                        })
                                    })
                                ]
                            }),
                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                className: "row",
                                children: [
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "col-3 col-lg-3 px-0 pb-3 text-end",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                            className: "me-2",
                                            children: "1 Bintang"
                                        })
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "col-7 col-lg-8 p-0 pt-2",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: "progress",
                                            style: {
                                                height: '10px'
                                            },
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                style: {
                                                    width: `${(0,_utils_helper__WEBPACK_IMPORTED_MODULE_4__/* .getPercentageTestimony */ .Nz)(testimony, 1) || 0}%`
                                                },
                                                className: "progress-bar bg-warning rounded-pill",
                                                role: "progressbar",
                                                "aria-valuenow": "75",
                                                "aria-valuemin": "0",
                                                "aria-valuemax": "100"
                                            })
                                        })
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "col-2 col-lg-1",
                                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                                            children: [
                                                (0,_utils_helper__WEBPACK_IMPORTED_MODULE_4__/* .getPercentageTestimony */ .Nz)(testimony, 1) || 0,
                                                "%"
                                            ]
                                        })
                                    })
                                ]
                            })
                        ]
                    })
                ]
            })
        ]
    }));
}
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (SectionTestimonyRating);


/***/ }),

/***/ 4879:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.a(module, async (__webpack_handle_async_dependencies__) => {
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "getServerSideProps": () => (/* binding */ getServerSideProps),
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6466);
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(7197);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_simple_star_rating__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(7386);
/* harmony import */ var react_simple_star_rating__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_simple_star_rating__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _components_global_Sections_SectionTestimonyRating_SectionTestimonyRating__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(1645);
/* harmony import */ var _components_global_Sections_SectionTestimonyComment_SectionTestimonyComment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(6761);
/* harmony import */ var _components_global_Sections_SectionRelatedCourse_SectionRelatedCourse__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(4190);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(2167);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _components_global_Layout_Layout__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(4528);
/* harmony import */ var _utils_helper__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(4986);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(968);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _components_global_Modal_Modal__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(2541);
/* harmony import */ var _components_global_Cards_CardDetail_CardDetail__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(1180);
/* harmony import */ var _components_global_Sections_SectionSlider_SectionSlider__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(636);
/* harmony import */ var swiper_react__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(3015);
/* harmony import */ var _components_global_Cards_CardMentor_CardMentor__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(4341);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_16__);
/* harmony import */ var _components_global_Buttons_ButtonShareSocmed_ButtonShareSocmed__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(1943);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(1664);
/* harmony import */ var _components_global_Cards_CardCourse_CardCourse__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(9722);
/* harmony import */ var _components_skeletons_SkeletonCardCourse_SkeletonCardCourse__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(2927);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([swiper_react__WEBPACK_IMPORTED_MODULE_14__, _components_global_Sections_SectionRelatedCourse_SectionRelatedCourse__WEBPACK_IMPORTED_MODULE_6__, _components_global_Sections_SectionSlider_SectionSlider__WEBPACK_IMPORTED_MODULE_13__]);
([swiper_react__WEBPACK_IMPORTED_MODULE_14__, _components_global_Sections_SectionRelatedCourse_SectionRelatedCourse__WEBPACK_IMPORTED_MODULE_6__, _components_global_Sections_SectionSlider_SectionSlider__WEBPACK_IMPORTED_MODULE_13__] = __webpack_async_dependencies__.then ? await __webpack_async_dependencies__ : __webpack_async_dependencies__);





















const getServerSideProps = async (context)=>{
    const { slug  } = context.query;
    const detailRes = await axios__WEBPACK_IMPORTED_MODULE_7___default().get(`${"https://api.codepolitan.com"}/course/detailComprehensif/${slug}`);
    const testimonyRes = await axios__WEBPACK_IMPORTED_MODULE_7___default().get(`${"https://apps.codepolitan.com"}/api/feedback/course/${slug}`);
    const [detail, testimony] = await Promise.all([
        detailRes.data,
        testimonyRes.data
    ]);
    return {
        props: {
            detail,
            testimony
        }
    };
};
const CourseDetail = ({ detail , testimony  })=>{
    const video = detail.course.preview_video;
    const { 0: videoId , 1: setVideoId  } = (0,react__WEBPACK_IMPORTED_MODULE_16__.useState)(video);
    const { 0: relatedCourse , 1: setRelatedCourse  } = (0,react__WEBPACK_IMPORTED_MODULE_16__.useState)([]);
    const { 0: show , 1: setShow  } = (0,react__WEBPACK_IMPORTED_MODULE_16__.useState)(false);
    const { 0: filterRate , 1: setFilterRate  } = (0,react__WEBPACK_IMPORTED_MODULE_16__.useState)(5);
    const { 0: allRate , 1: setAllRate  } = (0,react__WEBPACK_IMPORTED_MODULE_16__.useState)(true);
    const lessons = detail.lessons;
    const ratingValue = (0,_utils_helper__WEBPACK_IMPORTED_MODULE_9__/* .countRates */ .k7)(testimony);
    let detailParams = detail.products[0].retail_price <= 0 ? detail.course : detail.products;
    const filter = (rate)=>{
        if (rate === true) {
            setAllRate(rate);
            setFilterRate(true);
        } else {
            setFilterRate(rate);
            setAllRate(false);
        }
    };
    (0,react__WEBPACK_IMPORTED_MODULE_16__.useEffect)(()=>{
        const getRelatedCourse = async ()=>{
            try {
                let response = await axios__WEBPACK_IMPORTED_MODULE_7___default().get(`${"https://api.codepolitan.com"}/course/popular`);
                if (response.data.error === "No Content") {
                    setRelatedCourse(null);
                } else {
                    setRelatedCourse(response.data);
                }
            } catch (err) {
                return err.message;
            }
        };
        getRelatedCourse();
    }, [
        detail.author_name
    ]);
    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
        children: [
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)((next_head__WEBPACK_IMPORTED_MODULE_10___default()), {
                children: [
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("title", {
                        children: [
                            detail.course.title,
                            " - Codepolitan"
                        ]
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        name: "title",
                        content: detail.course.title
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        name: "description",
                        content: detail.course.description
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:type",
                        content: "website"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:url",
                        content: `https://codepolitan.com/course/intro/${detail.course.slug}`
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:title",
                        content: detail.course.title
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:description",
                        content: detail.course.description
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "og:image",
                        content: detail.course.cover
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:card",
                        content: "summary_large_image"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:url",
                        content: `https://codepolitan.com/course/intro/${detail.course.slug}`
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:title",
                        content: detail.course.title
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:description",
                        content: detail.course.description
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        property: "twitter:image",
                        content: detail.course.cover
                    })
                ]
            }),
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_components_global_Layout_Layout__WEBPACK_IMPORTED_MODULE_8__/* ["default"] */ .Z, {
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("section", {
                        className: "section mt-5",
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "container p-3 p-lg-5",
                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                className: "row mt-4",
                                children: [
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        className: "col-lg-7 text-muted",
                                        children: [
                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("section", {
                                                className: "section",
                                                children: [
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "ratio ratio-16x9 mb-4 d-lg-none",
                                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("iframe", {
                                                            style: {
                                                                borderRadius: '25px'
                                                            },
                                                            src: `https://www.youtube.com/embed/${detail.course.preview_video}`,
                                                            title: "Course Preview",
                                                            allowFullScreen: true
                                                        })
                                                    }),
                                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                        className: "d-flex align-items-center",
                                                        children: [
                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                                                                height: "65",
                                                                className: "rounded d-none d-lg-block",
                                                                src: detail.course.thumbnail,
                                                                alt: detail.course.title
                                                            }),
                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h1", {
                                                                className: "section-title ms-lg-3 my-auto h2",
                                                                children: detail.course.title
                                                            })
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                                        className: "text-muted my-3",
                                                        children: detail.course.description
                                                    }),
                                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                        className: "d-md-flex d-grid text-center text-lg-start",
                                                        children: [
                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                                className: "badge border text-primary py-md-2",
                                                                children: detail.course.level.toUpperCase()
                                                            }),
                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_simple_star_rating__WEBPACK_IMPORTED_MODULE_3__.RatingView, {
                                                                className: "ms-1 ms-md-3 mt-md-1 mt-2 mb-1 d-none d-md-block",
                                                                size: 17,
                                                                ratingValue: Math.floor(ratingValue)
                                                            }),
                                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                                                                className: "mt-md-1 text-muted ms-lg-auto ms-md-3 my-1 my-md-0",
                                                                children: [
                                                                    testimony.length || 0,
                                                                    " penilaian"
                                                                ]
                                                            }),
                                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                                                                className: "mt-md-1 text-muted ms-lg-auto ms-md-3 my-1 my-md-0",
                                                                children: [
                                                                    detail.total_student,
                                                                    " peserta"
                                                                ]
                                                            }),
                                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("button", {
                                                                onClick: ()=>setShow(!show)
                                                                ,
                                                                className: "btn text-muted btn-sm btn-transparent ms-md-auto",
                                                                children: [
                                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__.FontAwesomeIcon, {
                                                                        icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_1__.faShareAlt
                                                                    }),
                                                                    " Share"
                                                                ]
                                                            })
                                                        ]
                                                    }),
                                                    show && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        className: "row my-3",
                                                        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                            className: "col text-center",
                                                            children: [
                                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_global_Buttons_ButtonShareSocmed_ButtonShareSocmed__WEBPACK_IMPORTED_MODULE_17__/* ["default"] */ .Z, {
                                                                    type: "facebook",
                                                                    link: `https://codepolitan.com/course/intro/${detail.course.slug}`
                                                                }),
                                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_global_Buttons_ButtonShareSocmed_ButtonShareSocmed__WEBPACK_IMPORTED_MODULE_17__/* ["default"] */ .Z, {
                                                                    type: "twitter",
                                                                    link: `https://codepolitan.com/course/intro/${detail.course.slug}`
                                                                }),
                                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_global_Buttons_ButtonShareSocmed_ButtonShareSocmed__WEBPACK_IMPORTED_MODULE_17__/* ["default"] */ .Z, {
                                                                    type: "linkedin",
                                                                    link: `https://codepolitan.com/course/intro/${detail.course.slug}`
                                                                })
                                                            ]
                                                        })
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                className: "row mt-5",
                                                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                    className: "col",
                                                    children: [
                                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                            children: [
                                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h4", {
                                                                    className: "section-title",
                                                                    children: "Tentang Kelas"
                                                                }),
                                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                                    className: "text-muted",
                                                                    dangerouslySetInnerHTML: {
                                                                        __html: detail.course.long_description
                                                                    }
                                                                })
                                                            ]
                                                        }),
                                                        /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                            className: "accordion accordion-flush my-5",
                                                            id: "accordionFlushExample",
                                                            children: [
                                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h4", {
                                                                    className: "section-title",
                                                                    children: "Daftar Materi"
                                                                }),
                                                                lessons.map((lesson, index1)=>{
                                                                    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                                        className: "accordion-item",
                                                                        children: [
                                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
                                                                                className: "accordion-header",
                                                                                id: `flush-heading-${lesson.id}`,
                                                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
                                                                                    className: "accordion-button",
                                                                                    type: "button",
                                                                                    "data-bs-toggle": "collapse",
                                                                                    "data-bs-target": `#flush-collapse-${lesson.id}`,
                                                                                    "aria-expanded": "false",
                                                                                    "aria-controls": `flush-collapse-${lesson.id}`,
                                                                                    children: lesson.topic_title
                                                                                })
                                                                            }),
                                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                                                id: `flush-collapse-${lesson.id}`,
                                                                                className: "accordion-collapse collapse show",
                                                                                "aria-labelledby": `flush-heading-${lesson.id}`,
                                                                                "data-bs-parent": "#accordionFlushExample",
                                                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                                                    className: "accordion-body px-0 py-1",
                                                                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                                                        className: "card border-0",
                                                                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                                                            className: "card-body p-0",
                                                                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("table", {
                                                                                                className: "table table-striped m-0",
                                                                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("tbody", {
                                                                                                    children: lesson.contents.map((content, index)=>{
                                                                                                        return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("tr", {
                                                                                                            children: [
                                                                                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("td", {
                                                                                                                    style: {
                                                                                                                        width: '50px'
                                                                                                                    },
                                                                                                                    children: content.lesson_title?.includes('Kuis') || content.lesson_title?.includes('Quiz') ? /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__.FontAwesomeIcon, {
                                                                                                                        className: "text-primary",
                                                                                                                        size: "2x",
                                                                                                                        icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_1__.faCircleQuestion
                                                                                                                    }) : /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__.FontAwesomeIcon, {
                                                                                                                        className: "text-primary",
                                                                                                                        size: "2x",
                                                                                                                        icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_1__.faCirclePlay
                                                                                                                    })
                                                                                                                }),
                                                                                                                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("td", {
                                                                                                                    className: "text-muted",
                                                                                                                    children: content.lesson_title || 'Untitled'
                                                                                                                }),
                                                                                                                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("td", {
                                                                                                                    className: "text-end text-muted",
                                                                                                                    children: [
                                                                                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__.FontAwesomeIcon, {
                                                                                                                            icon: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_1__.faLock,
                                                                                                                            className: "me-2"
                                                                                                                        }),
                                                                                                                        content.duration || '00.00'
                                                                                                                    ]
                                                                                                                })
                                                                                                            ]
                                                                                                        }, index));
                                                                                                    })
                                                                                                })
                                                                                            })
                                                                                        })
                                                                                    })
                                                                                })
                                                                            })
                                                                        ]
                                                                    }, index1));
                                                                })
                                                            ]
                                                        }),
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_global_Sections_SectionSlider_SectionSlider__WEBPACK_IMPORTED_MODULE_13__/* ["default"] */ .Z, {
                                                            title: "Penyusun Materi",
                                                            titleFontSize: "h4",
                                                            slidesPerView: 2.1,
                                                            children: detail.authors?.map((author, index)=>{
                                                                return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(swiper_react__WEBPACK_IMPORTED_MODULE_14__.SwiperSlide, {
                                                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_global_Cards_CardMentor_CardMentor__WEBPACK_IMPORTED_MODULE_15__/* ["default"] */ .Z, {
                                                                        author: author.name,
                                                                        shortDescription: author.short_description,
                                                                        thumbnail: author.avatar
                                                                    })
                                                                }, index));
                                                            }).slice(0, 10)
                                                        }),
                                                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_global_Sections_SectionTestimonyRating_SectionTestimonyRating__WEBPACK_IMPORTED_MODULE_4__/* ["default"] */ .Z, {
                                                            className: "mt-5",
                                                            filterRate: filter,
                                                            testimony: testimony,
                                                            totalRate: ratingValue,
                                                            totalReview: testimony.length
                                                        }),
                                                        testimony.status != 'failed' && /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_global_Sections_SectionTestimonyComment_SectionTestimonyComment__WEBPACK_IMPORTED_MODULE_5__/* ["default"] */ .Z, {
                                                            allRate: allRate,
                                                            rateFilter: filterRate,
                                                            testimony: testimony
                                                        })
                                                    ]
                                                })
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                        className: "col-lg-4 offset-lg-1 sticky-top",
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                            className: "sticky-top",
                                            style: {
                                                top: '90px'
                                            },
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_global_Cards_CardDetail_CardDetail__WEBPACK_IMPORTED_MODULE_12__/* ["default"] */ .Z, {
                                                setVideo: ()=>setVideoId(video)
                                                ,
                                                image: detail.course.cover,
                                                detailProduct: detailParams,
                                                previewVideo: video,
                                                totalModule: detail.course.total_module,
                                                totalTime: detail.course.total_time,
                                                retailPrice: detail.products[0].retail_price
                                            })
                                        })
                                    })
                                ]
                            })
                        })
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_global_Modal_Modal__WEBPACK_IMPORTED_MODULE_11__/* ["default"] */ .Z, {
                        title: "Preview Course Video",
                        setVideo: ()=>setVideoId('')
                        ,
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                            className: "ratio ratio-16x9",
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("iframe", {
                                src: `https://www.youtube.com/embed/${videoId}`,
                                title: "YouTube video",
                                allowFullScreen: true
                            })
                        })
                    }),
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_components_global_Sections_SectionRelatedCourse_SectionRelatedCourse__WEBPACK_IMPORTED_MODULE_6__/* ["default"] */ .Z, {
                        titleSection: `Kelas Populer Lainnya`,
                        children: [
                            !relatedCourse && [
                                1,
                                2,
                                3,
                                4,
                                5
                            ].map((index)=>{
                                return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(swiper_react__WEBPACK_IMPORTED_MODULE_14__.SwiperSlide, {
                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_skeletons_SkeletonCardCourse_SkeletonCardCourse__WEBPACK_IMPORTED_MODULE_20__/* ["default"] */ .Z, {})
                                }, index));
                            }),
                            relatedCourse != null && (0,_utils_helper__WEBPACK_IMPORTED_MODULE_9__/* .shuffleArray */ .Sy)(relatedCourse).map((course, index)=>{
                                return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(swiper_react__WEBPACK_IMPORTED_MODULE_14__.SwiperSlide, {
                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_link__WEBPACK_IMPORTED_MODULE_18__["default"], {
                                        href: `/course/intro/${course.slug}`,
                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("a", {
                                            className: "link",
                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_components_global_Cards_CardCourse_CardCourse__WEBPACK_IMPORTED_MODULE_19__/* ["default"] */ .Z, {
                                                thumbnail: course.thumbnail,
                                                author: course.author,
                                                title: course.title,
                                                level: course.level,
                                                totalStudents: course.total_student,
                                                totalModules: course.total_module,
                                                totalTimes: course.total_time,
                                                totalRating: course.total_rating,
                                                totalFeedback: course.total_feedback,
                                                normalBuyPrice: course.buy?.normal_price || course.normal_price,
                                                retailBuyPrice: course.buy?.retail_price || course.retail_price,
                                                normalRentPrice: course.rent?.normal_price,
                                                retailRentPrice: course.rent?.retail_price
                                            })
                                        })
                                    })
                                }, index));
                            }).slice(0, 10)
                        ]
                    })
                ]
            })
        ]
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (CourseDetail);

});

/***/ }),

/***/ 5368:
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/free-brands-svg-icons");

/***/ }),

/***/ 197:
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/free-regular-svg-icons");

/***/ }),

/***/ 6466:
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/free-solid-svg-icons");

/***/ }),

/***/ 7197:
/***/ ((module) => {

"use strict";
module.exports = require("@fortawesome/react-fontawesome");

/***/ }),

/***/ 2167:
/***/ ((module) => {

"use strict";
module.exports = require("axios");

/***/ }),

/***/ 2733:
/***/ ((module) => {

"use strict";
module.exports = require("fuse.js");

/***/ }),

/***/ 2245:
/***/ ((module) => {

"use strict";
module.exports = require("moment");

/***/ }),

/***/ 5518:
/***/ ((module) => {

"use strict";
module.exports = require("moment/locale/id");

/***/ }),

/***/ 562:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/server/denormalize-page-path.js");

/***/ }),

/***/ 8028:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/server/image-config.js");

/***/ }),

/***/ 4957:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/head.js");

/***/ }),

/***/ 4014:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/i18n/normalize-locale-path.js");

/***/ }),

/***/ 8524:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/is-plain-object.js");

/***/ }),

/***/ 8020:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/mitt.js");

/***/ }),

/***/ 4964:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router-context.js");

/***/ }),

/***/ 9565:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/get-asset-path-from-route.js");

/***/ }),

/***/ 4365:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/get-middleware-regex.js");

/***/ }),

/***/ 1428:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/is-dynamic.js");

/***/ }),

/***/ 1292:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/parse-relative-url.js");

/***/ }),

/***/ 979:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/querystring.js");

/***/ }),

/***/ 6052:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/resolve-rewrites.js");

/***/ }),

/***/ 4226:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/route-matcher.js");

/***/ }),

/***/ 5052:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/route-regex.js");

/***/ }),

/***/ 9232:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/utils.js");

/***/ }),

/***/ 968:
/***/ ((module) => {

"use strict";
module.exports = require("next/head");

/***/ }),

/***/ 1853:
/***/ ((module) => {

"use strict";
module.exports = require("next/router");

/***/ }),

/***/ 6689:
/***/ ((module) => {

"use strict";
module.exports = require("react");

/***/ }),

/***/ 7386:
/***/ ((module) => {

"use strict";
module.exports = require("react-simple-star-rating");

/***/ }),

/***/ 997:
/***/ ((module) => {

"use strict";
module.exports = require("react/jsx-runtime");

/***/ }),

/***/ 3877:
/***/ ((module) => {

"use strict";
module.exports = import("swiper");;

/***/ }),

/***/ 3015:
/***/ ((module) => {

"use strict";
module.exports = import("swiper/react");;

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [7730,9285,1664,5675,4528,4986,9722,2927,2541,4722,636], () => (__webpack_exec__(4879)));
module.exports = __webpack_exports__;

})();