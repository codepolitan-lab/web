const withPlugins = require('next-compose-plugins');
const withPWA = require('next-pwa');
const securityHeaders = [
  {
    key: 'Strict-Transport-Security',
    value: 'max-age=63072000; includeSubDomains; preload'
  }
];

module.exports = withPlugins(
  [
    [withPWA, {
      pwa: {
        dest: "public",
        register: true,
        skipWaiting: true,
        runtimeCaching: [
          {
            urlPattern: /^https:\/\/fonts\.(?:googleapis|gstatic)\.com\/.*/i,
            handler: "CacheFirst",
            options: {
              cacheName: "google-fonts",
              expiration: {
                maxEntries: 4,
                maxAgeSeconds: 365 * 24 * 60 * 60, // 365 days
              },
            },
          },
          {
            urlPattern: /^https:\/\/use\.fontawesome\.com\/releases\/.*/i,
            handler: "CacheFirst",
            options: {
              cacheName: "font-awesome",
              expiration: {
                maxEntries: 1,
                maxAgeSeconds: 365 * 24 * 60 * 60, // 365 days
              },
            },
          },
          {
            urlPattern: /\.(?:eot|otf|ttc|ttf|woff|woff2|font.css)$/i,
            handler: "StaleWhileRevalidate",
            options: {
              cacheName: "static-font-assets",
              expiration: {
                maxEntries: 4,
                maxAgeSeconds: 7 * 24 * 60 * 60, // 7 days
              },
            },
          },
          {
            urlPattern: /\.(?:jpg|jpeg|gif|png|svg|ico|webp)$/i,
            handler: "StaleWhileRevalidate",
            options: {
              cacheName: "static-image-assets",
              expiration: {
                maxEntries: 64,
                maxAgeSeconds: 24 * 60 * 60, // 24 hours
              },
            },
          },
          {
            urlPattern: /\.(?:js)$/i,
            handler: "NetworkFirst",
            options: {
              cacheName: "static-js-assets",
              expiration: {
                maxEntries: 16,
                maxAgeSeconds: 24 * 60 * 60, // 24 hours
              },
            },
          },
          {
            urlPattern: /\.(?:css|less)$/i,
            handler: "StaleWhileRevalidate",
            options: {
              cacheName: "static-style-assets",
              expiration: {
                maxEntries: 16,
                maxAgeSeconds: 24 * 60 * 60, // 24 hours
              },
            },
          },
          {
            urlPattern: /\.(?:json|xml|csv)$/i,
            handler: "NetworkFirst",
            options: {
              cacheName: "static-data-assets",
              expiration: {
                maxEntries: 16,
                maxAgeSeconds: 24 * 60 * 60, // 24 hours
              },
            },
          },
          {
            urlPattern: /.*/i,
            handler: "NetworkFirst",
            options: {
              cacheName: "others",
              expiration: {
                maxEntries: 16,
                maxAgeSeconds: 24 * 60 * 60, // 24 hours
              },
            },
          },
        ],
      },
    }]
  ],

  /* Global Config */
  {
    swcMinify: false,
    reactStrictMode: false,
    trailingSlash: true,
    async headers() {
      return [
        {
          // Apply these headers to all routes in your application.
          source: '/(.*)',
          headers: securityHeaders,
        },
      ]
    },
    images: {
      // path: ['https://ik.imagekit.io', 'static.cdn-cdpl.com'],
      domains: [
        'ik.imagekit.io',
        'i.ibb.co',
        'apps.codepolitan.com',
        'id.imgbb.com',
        'static.cdn-cdpl.com',
        'cdn-cdpl.sgp1.digitaloceanspaces.com',
        'cdn-cdpl.sgp1.cdn.digitaloceanspaces.com',
        'www.gravatar.com',
        'image.web.id'
      ],
    },
  });