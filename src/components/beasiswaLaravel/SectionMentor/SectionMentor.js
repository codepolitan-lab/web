import { faGithub, faLinkedin } from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const SectionMentor = () => {
    return (
        <section className="section">
            <div className="container p-4 p-lg-5">
                <div className="row">
                    <div className="col-lg-6 my-auto">
                        <img className="img-fluid d-block mx-auto" src="https://image.web.id/images/img-mentor-5-1.png" loading="lazy" alt="Laravel" />
                    </div>
                    <div className="col-lg-6 my-auto order-lg-first">
                        <h2 className="section-title">Belajar Bersama Mentor <span className="text-primary">Aldi Zainafif</span></h2>
                        <p className="text-muted lh-lg"><span className="fw-bold">Software Engineer di Flip.</span> Former Senior Software Engineer di Quipper. Former Backend Software Engineer di Bukalapak. Former Information Technology Expert di Pemerintahan Kota Cimahi untuk pengembangan portal for region planning BAPPEDA (Badan pengembangan dan Penelitian Daerah). Former Website Developer di BAS-IT Studio. Berpengalaman sebagai programmer profesional sejak 2015.</p>
                        <a className="link text-primary me-2" href="https://www.linkedin.com/in/aldizainafif" target="_blank" rel="noopener noreferrer">
                            <FontAwesomeIcon size="2x" icon={faLinkedin} />
                        </a>
                        <a className="link text-primary" href="https://github.com/zainafifaldi" target="_blank" rel="noopener noreferrer">
                            <FontAwesomeIcon size="2x" icon={faGithub} />
                        </a>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default SectionMentor;
