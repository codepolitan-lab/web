const SectionFAQ = () => {
    const items = [
        {
            question: 'Apa itu Beasiswa Laravel?',
            answer: 'Beasiswa Laravel adalah program beasiswa belajar coding secara gratis di kelas Laravel dari Codepolitan'
        }
    ];

    return (
        <section className="section bg-light">
            <div className="container p-4 p-lg-5">
                <div className="text-center my-5">
                    <h2 className="section-title">Pertanyaan Yang Sering Ditanyakan</h2>
                    <p className="text-muted">Simak beberapa pertanyaan-pertanyaan berikut, siapa tahu salah satunya adalah pertanyaan yang ingin kamu tanyakan.</p>
                </div>
                <div className="row justify-content-center">
                    <div className="col-lg-8">
                        <div className="accordion" id="accordionExample">
                            {items.map((item, index) => {
                                return (
                                    <div className="accordion-item" key={index}>
                                        <h2 className="accordion-header" id={`heading${index}`}>
                                            <button className={`accordion-button shadow-none ${index !== 0 && 'collapsed'}`} type="button" data-bs-toggle="collapse" data-bs-target={`#collapse${index}`} aria-expanded={index === 0 ? "true" : "false"} aria-controls={`collapse${index}`}>
                                                {item.question}
                                            </button>
                                        </h2>
                                        <div id={`collapse${index}`} className={`accordion-collapse collapse ${index === 0 && 'show'}`} aria-labelledby={`heading${index}`} data-bs-parent="#accordionExample">
                                            <div className="accordion-body bg-light text-muted">
                                                {item.answer}
                                            </div>
                                        </div>
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default SectionFAQ;
