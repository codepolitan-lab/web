const CardMaterial = ({ title, description, image }) => {
    return (
        <div className="card text-white h-100 border-0" style={{ borderRadius: '15px' }}>
            <img src={image} className="card-img" alt={title} style={{ borderRadius: '15px' }} />
            <div className="card-img-overlay d-flex flex-column justify-content-end" style={{ background: `linear-gradient(to bottom, rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5))`, borderRadius: '15px' }}>
                <h5 className="card-title">{title}</h5>
                <hr className="my-2" />
                <p className="card-text fs-6">{description}</p>
            </div>
        </div>

    );
};

export default CardMaterial;
