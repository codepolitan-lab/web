import { useRef } from 'react';
import { Swiper, SwiperSlide } from 'swiper/react';
import SwiperCore, { Pagination, Navigation } from 'swiper';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowAltCircleLeft, faArrowAltCircleRight } from '@fortawesome/free-solid-svg-icons';

import styles from './SectionMaterials.module.scss';
import CardMaterial from '../CardMaterial/CardMaterial';

// install Swiper modules
SwiperCore.use([Pagination, Navigation]);

const SectionMaterials = () => {
    const navigationPrevRef = useRef(null);
    const navigationNextRef = useRef(null);

    const materials = [
        {
            title: "Pendahuluan",
            description: "Pada tahap ini kamu akan mulai belajar mengenal apa itu laravel, bagaimana cara instalasi laravel dan composer juga penjelasan singkat tentang Command Line",
            image: "https://image.web.id/images/Group-3448.png"
        },
        {
            title: "Alur Kerja & Permulaan",
            description: "Pada tahap ini kamu akan mulai belajar mengenal apa itu laravel, bagaimana cara instalasi laravel dan composer juga penjelasan singkat tentang Command Line",
            image: "https://image.web.id/images/Group-3447.png"
        },
        {
            title: "Mendalami Laravel",
            description: "Pada tahap ini kamu akan mulai belajar mengenal apa itu laravel, bagaimana cara instalasi laravel dan composer juga penjelasan singkat tentang Command Line",
            image: "https://image.web.id/images/Group-3449.png"
        },
        {
            title: "Database Migration & Query Builder",
            description: "Pada tahap ini kamu akan mulai belajar mengenal apa itu laravel, bagaimana cara instalasi laravel dan composer juga penjelasan singkat tentang Command Line",
            image: "https://image.web.id/images/Group-3456.png"
        },
        {
            title: "Eloquent",
            description: "Pada tahap ini kamu akan mulai belajar mengenal apa itu laravel, bagaimana cara instalasi laravel dan composer juga penjelasan singkat tentang Command Line",
            image: "https://image.web.id/images/Group-3458.png"
        },
        {
            title: "Templating Blade",
            description: "Pada tahap ini kamu akan mulai belajar mengenal apa itu laravel, bagaimana cara instalasi laravel dan composer juga penjelasan singkat tentang Command Line",
            image: "https://image.web.id/images/Group-3457.png"
        },
        {
            title: "Otentikasi Input",
            description: "Pada tahap ini kamu akan mulai belajar mengenal apa itu laravel, bagaimana cara instalasi laravel dan composer juga penjelasan singkat tentang Command Line",
            image: "https://image.web.id/images/Group-3500.png"
        }
    ];

    return (
        <section className={`section ${styles.background}`}>
            <div className="container p-4 p-lg-5">
                <div className="row">
                    <div className="col-lg-3">
                        <h2 className="section-title text-white mb-4">Apa Aja Yang Bisa Kamu Pelajari ?</h2>
                        <span style={{ display: 'block', height: '10px', width: '8em', background: '#fff' }} />
                    </div>
                    <div className="col-lg-9">
                        <div className="d-none d-lg-flex gap-2 justify-content-end">
                            <button type="button" className="btn border-0 text-primary shadow-none p-1" ref={navigationPrevRef} title="Prev">
                                <FontAwesomeIcon size="2x" icon={faArrowAltCircleLeft} />
                            </button>
                            <button type="button" className="btn border-0 text-primary shadow-none p-1" ref={navigationNextRef} title="Next">
                                <FontAwesomeIcon size="2x" icon={faArrowAltCircleRight} />
                            </button>
                        </div>

                        <Swiper
                            id="swiper"
                            spaceBetween={20}
                            pagination={{ clickable: true }}
                            navigation={{
                                prevEl: navigationPrevRef.current,
                                nextEl: navigationNextRef.current,
                            }}
                            onBeforeInit={(swiper) => {
                                swiper.params.navigation.prevEl = navigationPrevRef.current;
                                swiper.params.navigation.nextEl = navigationNextRef.current;
                            }}
                            breakpoints={{
                                // when window width is >= 414px
                                375: {
                                    slidesPerView: 1.1,
                                },
                                // when window width is >= 768px
                                768: {
                                    slidesPerView: 2.2,
                                },
                                1200: {
                                    slidesPerView: 3.2,
                                },
                            }}
                            style={{ padding: '20px 0 60px 0' }}
                        >
                            {materials?.map((material, index) => {
                                return (
                                    <SwiperSlide
                                        key={index}
                                        style={{ height: 'auto' }}
                                    >
                                        <CardMaterial
                                            title={material.title}
                                            description={material.description}
                                            image={material.image}
                                        />
                                    </SwiperSlide>
                                );
                            })}
                        </Swiper>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default SectionMaterials;
