import { faWhatsapp } from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Link from "next/link";

const SectionCTA = () => {
    return (
        <section
            className="section py-5"
            style={{
                background: `url('https://image.web.id/images/bg-cta.jpg')`,
                backgroundPosition: '15%',
                backgroundSize: 'cover'
            }}>
            <div className="container p-4 p-lg-5">
                <h2 className="section-title text-center mb-5 w-50 mx-auto">Tunggu Apa lagi ? Jangan Lewatkan Kesempatan ini Dan Klaim Sekarang juga!</h2>
                <div className="d-grid gap-3 d-lg-flex justify-content-center">
                    <a className="btn btn-primary btn-lg px-4 rounded-3" href="http://dashboard.codepolitan.com/login?redirect=scholarship" target="_blank" rel="noopener noreferrer">Klaim Sekarang</a>
                    <a className="btn btn-outline-primary btn-lg px-4 rounded-3" href="http://dashboard.codepolitan.com/login?redirect=scholarship" target="_blank" rel="noopener noreferrer"><FontAwesomeIcon icon={faWhatsapp} /> Tanya Program</a>
                </div>
            </div>
        </section>
    );
};

export default SectionCTA;
