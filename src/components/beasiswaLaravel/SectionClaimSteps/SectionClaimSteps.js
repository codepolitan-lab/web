import { faArrowRight } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const SectionClaimSteps = () => {
    return (
        <section className="section">
            <div className="container-fluid px-auto">
                <h2 className="section-title text-center mb-4">Tahapan Klaim Beasiswa</h2>

                <div className="row">
                    <div className="col-3 d-none d-lg-block" style={{ backgroundColor: '#45CCC6' }}>
                        <img className="img-fluid d-block mx-auto p-5" src="https://image.web.id/images/Untitled-2a8b8fd695f7b60cf.png" alt="Step 1" />
                    </div>
                    <div className="col" style={{ backgroundColor: '#14A7A0' }}>
                        <div className="d-flex p-5 text-white">
                            <h3 className="h1">01</h3>
                            <div className="ms-3">
                                <p className="h5">Lengkapi Persyaratan:</p>
                                <ol>
                                    <li>Lengkapi dan Screenshot Profile akun Codepolitan</li>
                                    <li>Follow dan Screenshot Instagram Codepolitan</li>
                                    <li>Join dan Screenshot Discord Codepolitan</li>
                                    <li>Subscribe dan Screenshot Youtube Codepolitan</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-3 d-none d-lg-block order-last" style={{ backgroundColor: '#77DCB8' }}>
                        <img className="img-fluid d-block mx-auto p-5" src="https://image.web.id/images/OBJECTS.png" alt="Step 1" />
                    </div>
                    <div className="col" style={{ backgroundColor: '#00C880' }}>
                        <div className="d-flex justify-content-end p-5 text-white text-end">
                            <h3 className="h1 order-last">02</h3>
                            <div className="me-3">
                                <p>Jangan lupa untuk submit ya coders! supaya dapet <br className="d-none d-lg-block" /> beasiswa belajar Laravel gratis</p>
                                <a className="link text-white mt-4" href="http://" target="_blank" rel="noopener noreferrer">Submit Sekarang <FontAwesomeIcon icon={faArrowRight} /></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-3 d-none d-lg-block" style={{ backgroundColor: '#B0C2FF' }}>
                        <img className="img-fluid d-block mx-auto p-5" src="https://image.web.id/images/Group-3480.png" alt="Step 1" />
                    </div>
                    <div className="col" style={{ backgroundColor: '#829DFC' }}>
                        <div className="d-flex p-5 text-white">
                            <h3 className="h1">03</h3>
                            <div className="ms-3">
                                <p className="h5">Yeay! Kamu Dapat Voucher Beasiswa</p>
                                <p>Kamu akan mendapatkan voucher beasiswa belajar Framework <br className="d-none d-lg-block" /> Laravel Dasar 9 yang dapat di redeem!</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-3 d-none d-lg-block order-last" style={{ backgroundColor: '#FFB9CA' }}>
                        <img className="img-fluid d-block mx-auto p-5" src="https://image.web.id/images/Group-3496.png" alt="Step 1" />
                    </div>
                    <div className="col" style={{ backgroundColor: '#FF86A3' }}>
                        <div className="d-flex justify-content-end p-5 text-white text-end">
                            <h3 className="h1 order-last">04</h3>
                            <div className="me-3">
                                <p className="h5">Redeem Voucher Beasiswa</p>
                                <p>Saatnya Redeem Voucher yang kamu miliki pada apps <br className="d-none d-lg-block" /> codepolitan</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-3 d-none d-lg-block" style={{ backgroundColor: '#FFD1B0' }}>
                        <img className="img-fluid d-block mx-auto p-5" src="https://image.web.id/images/Group-3495.png" alt="Step 1" />
                    </div>
                    <div className="col" style={{ backgroundColor: '#FC9F82' }}>
                        <div className="d-flex p-5 text-white">
                            <h3 className="h1">05</h3>
                            <div className="ms-3">
                                <p className="h5">Akses kelas untuk mulai belajar</p>
                                <p>Ini adalah saatnya kamu belajar laravel dasar 9 dengan lengkap <br className="d-none d-lg-block" /> dan terarah secara gratis! kamu bakal dapet sertifikat juga loh!</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div >
        </section >
    );
};

export default SectionClaimSteps;
