const SectionIntro = () => {
    return (
        <section className="section">
            <div className="container p-4 p-lg-5">
                <div className="row">
                    <div className="col-lg-6 my-auto">
                        <img className="img-fluid d-block mx-auto" src="https://image.web.id/images/Group-3439.png" loading="lazy" alt="Laravel" />
                    </div>
                    <div className="col-lg-6 my-auto">
                        <h2 className="section-title">Kenapa Laravel ?</h2>
                        <p className="text-muted lh-lg">Laravel merupakan framework php popular yang sampai dengan sekarang masih banyak peminat dan penggunanya. Hampir setiap tahunnya merilis versi baru dengan fitur yang semakin memudahkan para pengembang web. Baru belajar Laravel atau mau tau seputar updatenya? yuk gabung kelas ini dengan gratis melalui beasiswa!</p>
                        <a className="btn btn-primary btn-lg px-4 rounded-3" href="http://dashboard.codepolitan.com/login?redirect=scholarship" target="_blank" rel="noopener noreferrer">Klaim Sekarang</a>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default SectionIntro;
