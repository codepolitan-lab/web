const Hero = () => {
    return (
        <section className="section bg-light py-5 py-lg-auto mt-5">
            <div className="container p-4 p-lg-5">
                <div className="row">
                    <div className="col-lg-6 my-auto">
                        <h1 className="section-title">Beasiswa Belajar Laravel</h1>
                        <p className="text-muted lh-lg">Dapatkan beasiswa belajar gratis laravel dasar terlengkap ini untuk kamu yang ingin mulai berkarir menjadi laravel developer dengan gratis!</p>
                        <p className="fw-bold text-muted">Batas waktu Klaim Beasiswa hingga:</p>
                        <p className="text-muted">
                            <span className="h1 fw-bold text-danger">31 Mei 2023</span>
                        </p>
                        <a className="btn btn-primary btn-lg px-4 rounded-3" href="http://dashboard.codepolitan.com/login?redirect=scholarship" target="_blank" rel="noopener noreferrer">Klaim Sekarang</a>
                        <div className="my-3">
                            <a className="text-muted" href="!#" target="_blank" rel="noopener noreferrer">Syarat dan ketentuan berlaku</a>
                        </div>
                    </div>
                    <div className="col-lg-6 my-auto p-0">
                        <img className="img-fluid d-block mx-auto p-0" src="https://image.web.id/images/Mask-group.png" loading="lazy" alt="Beasiswa Laravel" />
                    </div>
                </div>
            </div>
        </section>
    );
};

export default Hero;
