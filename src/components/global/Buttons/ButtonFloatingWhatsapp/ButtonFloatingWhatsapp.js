import { useState } from 'react';
import { faWhatsapp } from '@fortawesome/free-brands-svg-icons';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styles from './ButtonFloatingWhatsapp.module.scss';
import mixpanel from 'mixpanel-browser'

const ButtonFloatingWhatsapp = () => {
    const [show, setShow] = useState(false);
    mixpanel.init('608746391f30c018d759b8c2c1ecb097', { debug: false })

    return (
        <>
            {!show && (
                <div onClick={() => setShow(!show)} role="button" className={`${styles.floating_alert} d-none d-md-block`}>
                    Butuh bantuan? <strong>Chat kami sekarang!</strong>
                </div>
            )}
            <button onClick={() => setShow(!show)} type="button" className={styles.floating_btn}>
                <FontAwesomeIcon icon={!show ? faWhatsapp : faTimes} />
            </button>

            {show && (
                <div className={`${styles.floating_card} card`}>
                    <div className="card-header bg-codepolitan text-white" style={{ borderRadius: '10px 10px 0 0' }}>
                        <h5 className="card-title m-0">Hubungi Kami via WhatsApp</h5>
                        <p className="m-0">Mau tanya tentang Codepolitan? Silahkan hubungi CS Kami!</p>
                    </div>
                    <div className="card-body">
                        <div className="mb-3">
                            <small className="text-muted">
                                Admin biasanya membalas dalam beberapa menit
                            </small>
                        </div>

                        <a onClick={() => mixpanel.track('Tanya Support', { 'source': 'Landing' })} href="http://wa.me/628999488990" className="link" target="_blank" rel="noopener noreferrer">
                            <div className="card bg-light border-0 mb-3 border border-start border-success border-3">
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-auto my-auto">
                                            <FontAwesomeIcon size="2x" fixedWidth icon={faWhatsapp} />
                                        </div>
                                        <div className="col">
                                            <strong>Support</strong>
                                            <p className="text-muted m-0">
                                                <small>Admin Codepolitan</small>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>

                        <a onClick={() => mixpanel.track('Tanya Marketing', { 'source': 'Landing' })} className="link" href="http://wa.me/628999488990" target="_blank" rel="noopener noreferrer">
                            <div className="card bg-light border-0 mb-3 border border-start border-success border-3">
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-auto my-auto">
                                            <FontAwesomeIcon size="2x" fixedWidth icon={faWhatsapp} />
                                        </div>
                                        <div className="col">
                                            <strong>Marketing</strong>
                                            <p className="text-muted m-0">
                                                <small>Informasi seputar layanan Codepolitan</small>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            )
            }
        </>
    );
};

export default ButtonFloatingWhatsapp;