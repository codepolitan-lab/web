import { formatDateFromNow } from "../../../utils/helper";

const ToastMessage = ({ avatar, name, message, timestamp }) => {
    return (
        <div className="d-flex">
            <div className="my-auto">
                <img width={50} height={50} className="rounded-pill objectfit-cover" src={avatar || "/assets/img/placeholder.jpg"} alt={name || "Avatar"} />
            </div>
            <div className="my-auto ms-3">
                <h6 className="fw-bold text-primary mb-1">{name || "Unknown"}</h6>
                <small>
                    <p className="text-muted mb-2">{message || 'no message'}</p>
                    <span className="text-muted">{timestamp ? formatDateFromNow(timestamp) : 'unknown'}</span>
                </small>
            </div>
        </div>
    );
};

export default ToastMessage;
