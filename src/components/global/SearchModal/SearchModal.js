import axios from 'axios';
import Link from 'next/link';
import { useEffect, useState } from 'react';
import Fuse from "fuse.js";

const SearchModal = () => {
    const [lists, setLists] = useState([])
    const [search, setSearch] = useState('')

    useEffect(() => {
        const getLists = async () => {
            try {
                let response = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/v1/search`)
                if (response.data.error === "No Content") {
                    setLists(null);
                } else {
                    setLists(response.data);
                }
            } catch (err) {
                return err.message;
            }
        };
        getLists();
    }, []);

    const fuse = new Fuse(lists, {
        keys: [
            'title',
        ]
    });


    const results = fuse.search(search)
    const listResults = results.map((result) => result.item)

    const handleSearch = ({ currentTarget = {} }) => {
        const { value } = currentTarget
        setSearch(value)

    }
    // console.log(search);

    return (
        <div className="modal fade" id="searchModal" tabIndex={-1} aria-labelledby="searchModalLabel" aria-hidden="true">
            <div className="modal-dialog modal-xl">
                <div className="modal-content">
                    <div className="modal-header d-flex align-content-start">
                        <input type="text" className="form-control" value={search} onChange={handleSearch} placeholder="Cari sesuatu disini..." />
                        <button onClick={() => setSearch('')} type="button" className="btn btn-light text-muted btn-sm ms-2 border" data-bs-dismiss="modal">
                            <strong>
                                Close
                            </strong>
                        </button>
                    </div>
                    {
                        search != '' && (
                            <div className="modal-body">
                                <h6>Hasil pencarian :</h6>
                                <div className="list-group">
                                    {
                                        search != '' && listResults.length < 1 && (
                                            <>
                                                <p>Hasil tidak ditemukan. Silahkan klik link dibawah ini untuk request materi</p>
                                                <div>
                                                    <a href="https://docs.google.com/forms/d/e/1FAIpQLSd3jkYL123X5LYWlKo7PNNRvYiAu28y9KqJzLA09d4jiVqFig/viewform" target="_blank" rel="noopener noreferrer" className="btn btn-primary btn-sm">Request Materi</a>
                                                </div>
                                            </>
                                        )
                                    }
                                    {
                                        listResults &&
                                        listResults.filter(item => !item.title.includes('KelasFullstack.id')).map((item, index) => {
                                            let prefix = '';

                                            if (item?.title?.includes('Course')) {
                                                prefix = '/course/intro/';
                                            } else if (item?.title?.includes('Roadmap')) {
                                                prefix = '/roadmap/'
                                            } else if (item?.title?.includes('Article')) {
                                                prefix = '/blog/'
                                            };

                                            return (
                                                <div onClick={() => wa.track({ name: 'Search Klik' })} data-bs-dismiss="modal" key={index}>
                                                    <Link href={prefix + item.slug}>
                                                        <a key={index} className="list-group-item list-group-item-action border-0 rounded-pill text-muted" aria-current="true">
                                                            {item.title}
                                                        </a>
                                                    </Link>
                                                </div>
                                            )
                                        }).slice(0, 10)
                                    }
                                </div>
                            </div>
                        )
                    }
                </div>
            </div>
        </div>
    );
};

export default SearchModal;
