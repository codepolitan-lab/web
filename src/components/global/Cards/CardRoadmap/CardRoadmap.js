import { faBook, faChartColumn, faChartLine, faSignal, faStar, faUsers } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Image from 'next/image';
import { useState } from 'react';
import { capitalizeFirstLetter } from '../../../../utils/helper';
import styles from './CardRoadmap.module.scss';

const CardRoadmap = ({ title, thumbnail, rate, level, totalCourse, totalStudent, icon, author }) => {
    const [hover, setHover] = useState(false);

    return (
        <div className={`${styles.card} card border-0 text-white`} onMouseEnter={() => setHover(true)} onMouseLeave={() => setHover(false)}>
            <Image
                className={styles.card_img}
                src={thumbnail || "/assets/img/placeholder.jpg"}
                placeholder="blur"
                blurDataURL="/assets/img/placeholder.jpg"
                alt={title}
                width={100}
                height={200}
                layout="responsive"
            />
            <div className={`${styles.overlay_hover} card-img-overlay d-flex flex-column justify-content-end`}>
                <img className={styles.roadmap_icon} loading="lazy" src={icon} alt="Icon" />
                <span className={styles.roadmap_title}>{author}</span>
                <h5 className={styles.title}>{title}</h5>
                <div className={`${styles.roadmap_info} d-flex align-items-start`}>
                    <small>
                        <FontAwesomeIcon className="my-auto" icon={faBook} />
                        <span className="ms-1">{totalCourse} Kelas</span>
                        <FontAwesomeIcon className="my-auto ms-2" icon={faUsers} />
                        <span className="ms-1">{totalStudent} Siswa</span>
                        <FontAwesomeIcon className="my-auto ms-2" icon={faSignal} />
                        <span className="ms-1">{capitalizeFirstLetter(level)}</span>
                    </small>
                </div>
                {rate && (
                    <div className={`${styles.rate} text-end text-warning`}>
                        <FontAwesomeIcon className="my-auto" icon={faStar} />
                        <span className="ms-1">{rate}</span>
                    </div>
                )}
            </div>
        </div>
    );
};

export default CardRoadmap;
