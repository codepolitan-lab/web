import styles from './CardBeasiswa.module.scss';
import { faCalendar, faUsers } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";


const CardBeasiswa = ({ thumbnail, organizer, title, description, closedAt, participats, link }) => {
    return (
        <div className={`${styles.card_beasiswa} card h-100`}>
            <img src={thumbnail || "/assets/img/placeholder.jpg"} className={`${styles.card_img_top} card-img-top`} alt={title} />
            <div className="card-body">
                <span className="text-primary">
                    <small>
                        <strong>{organizer}</strong>
                    </small>
                </span>
                <h5 className="card-title">{title}</h5>
                <p className="card-text text-muted"><small>{description.slice(0, 100) + '...'}</small></p>
                <small className="text-muted" style={{ fontSize: 'smaller' }}>
                    <p className="mb-1">
                        <FontAwesomeIcon className="text-primary me-1" fixedWidth icon={faCalendar} />
                        {closedAt}
                    </p>
                    <p className="m-0">
                        <FontAwesomeIcon className="text-primary me-1" fixedWidth icon={faUsers} />
                        {participats}
                    </p>
                </small>
                {link && (
                    <>
                        <hr style={{ height: '1px', color: '#ccc' }} />
                        <a style={{ borderRadius: '10px' }} href={link} className="btn btn-primary float-end">Daftar Sekarang</a>
                    </>
                )}
            </div>
        </div>

    );
};

export default CardBeasiswa;
