import styles from './CardSlider.module.scss';

const CardSlider = ({ thumbnail, title, description }) => {
    return (
        <div className={`card ${styles.card_slider}`}>
            <img src={thumbnail || '/assets/img/placeholder.jpg'} className="card-img" alt={title} />
            <div className={`${styles.overlay} card-img-overlay d-flex flex-column justify-content-center`}>
                <div className="p-auto p-lg-5">
                    <h5 className={styles.title}>{title}</h5>
                    <p className={styles.description}>{description}</p>
                </div>
            </div>
        </div>
    );
};

export default CardSlider;
