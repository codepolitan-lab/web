import { faUser } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React from 'react'
import { formatOnlyDate } from '../../../../utils/helper'

const CardFeaturedArticle = ({ title, writer, createdAt, type, index }) => {
    return (
        <div className="card border-0 p-1 h-100">
            <div className="card-body">
                <div className="d-flex gap-3">
                    <div className="d-flex flex-column">

                        {type === 'post' && (<span className="badge bg-primary mb-2">Article</span>)}
                        {type === 'tutorial' && (<span className="badge bg-danger mb-2">Tutorial</span>)}
                        {type === 'event' && (<span className="badge bg-info mb-2">Event</span>)}

                        <img src={`/assets/img/article/0${index}.png`} className="mt-2" alt="Article" />
                    </div>
                    <div className="d-flex flex-column">
                        <p className="lead mb-0" style={{ fontSize: '1.1em' }}>{title}</p>
                        <small className="mt-2">
                            <FontAwesomeIcon icon={faUser} className="me-2" />
                            <span>{writer}</span>
                            <span className="mx-2">-</span>
                            <span>{formatOnlyDate(createdAt)}</span>
                        </small>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default CardFeaturedArticle