import { faCheckCircle, faCircleQuestion, faClock, faMedal, faPlayCircle, faSwatchbook } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Link from 'next/link';
import { formatPrice, getPercentage } from '../../../../utils/helper';

function CardDetail({ retailPrice, detailProduct, roadmap, image, setVideo, previewVideo, slug, totalModule, totalTime }) {
    let detail = detailProduct

    // Check jika props dari roadmap
    if (roadmap) {
        detail[0].retail_price == 0 && (detail = detailProduct[0])
    }

    let free = false;
    let buy, rent, productBuy, productRent = [];

    let normalBuyPrice = -100; // Default jika productBuy tidak ada
    let retailBuyPrice = -100; // Default jika productBuy tidak ada
    let productBuySlug = null; // Default jika productBuy tidak ada

    let normalRentPrice = -100; // Default jika productBuy tidak ada
    let retailRentPrice = -100; // Default jika productBuy tidak ada
    let productRentSlug = null; // Default jika productBuy tidak ada


    if (retailPrice <= 0 || detail[0].retail_price <= 0) {
        free = true;
        !roadmap && (detail = detail[0])
    } else {
        buy = detail.filter(item => item.tags === 'buy');
        rent = detail.filter(item => item.tags === 'rent');
        productBuy = buy[0];
        productRent = rent[0];

        if (buy[0] !== undefined) {
            normalBuyPrice = productBuy.normal_price;
            retailBuyPrice = productBuy.retail_price;
            productBuySlug = productBuy.product_slug;
        };

        if (rent[0] !== undefined) {
            normalRentPrice = productRent.normal_price;
            retailRentPrice = productRent.retail_price;
            productRentSlug = productRent.product_slug;
        };
    };

    if (productBuy) {
        normalBuyPrice = productBuy.normal_price;
        retailBuyPrice = productBuy.retail_price;
        productBuySlug = productBuy.product_slug;
    };

    if (productRent) {
        normalRentPrice = productRent.normal_price;
        retailRentPrice = productRent.retail_price;
        productRentSlug = productRent.product_slug;
    };


    return (
        <div className="card card-rounded p-0 mt-4 mt-lg-0" id="sidebarCourseDetail">
            <div className="wrap d-none d-lg-block">
                <img src={image} className="image" style={{ borderRadius: '15px 15px 0 0', width: '100%', height: '220px', objectFit: 'cover' }} alt="..." />
                {!roadmap && previewVideo != "" && previewVideo != null && (
                    <>
                        <div className="position-absolute top-0 text-white w-100 pt-3 pb-2 px-4 display">
                            <FontAwesomeIcon className="fs-5 me-1" icon={faPlayCircle}></FontAwesomeIcon>
                            <span className="fw-bolder">Lihat Rekaman</span>
                        </div>
                        <div className="overlay">
                            <div className="text">
                                <button onClick={() => setVideo()} className="bg-transparent border-0" data-bs-toggle="modal" data-bs-target="#modal">
                                    <FontAwesomeIcon className="fs-1 text-white" icon={faPlayCircle}></FontAwesomeIcon>
                                </button>
                            </div>
                        </div>
                    </>
                )}
            </div>
            {free && (
                <div className="card-body p-0">
                    {
                        roadmap && (
                            <>
                                <div className="px-4 my-2 text-muted">
                                    <span className="fs-3 fw-bold me-2 text-primary">Gratis!</span>
                                </div>
                                <div className="p-2 px-4 text-muted" style={{ fontSize: 'smaller' }}>
                                    <div className="fs-6 mb-3">Yang akan kamu dapatkan :</div>
                                    <div className="d-flex align-items-start">
                                        <FontAwesomeIcon className="my-auto me-2 text-primary" icon={faSwatchbook} />
                                        <span>{totalModule} Modul</span>
                                    </div>
                                    <div className="d-flex align-items-start">
                                        <FontAwesomeIcon className="my-auto me-2 text-primary" icon={faClock} />
                                        <span>{totalTime} jam durasi</span>
                                    </div>
                                    <div className="d-flex align-items-start">
                                        <FontAwesomeIcon className="my-auto me-2 text-primary" icon={faCircleQuestion} />
                                        <span>Forum Diskusi Tanya Jawab</span>
                                    </div>
                                    <div className="d-flex align-items-start">
                                        <FontAwesomeIcon className="my-auto me-2 text-primary" icon={faMedal} />
                                        <span>Klaim Sertifikat Digital</span>
                                    </div>
                                </div>

                                <div className="d-grid gap-2 p-2 px-4 my-2">
                                    <Link href={`https://dashboard.codepolitan.com/learn/courses/detail/${slug}`}>
                                        <a target={'_blank'} className="btn btn-primary btn-rounded">Belajar Sekarang</a>
                                    </Link>
                                </div>
                            </>
                        )
                    }
                    {
                        !roadmap && (
                            <>
                                <div className="px-4 my-2 text-muted">
                                    {retailPrice === 0 && (<span className="fs-3 fw-bold me-2 text-primary">Gratis!</span>)}
                                    {retailPrice < 0 && (<span className="fs-3 fw-bold me-2">Beli lewat Roadmap</span>)}
                                </div>
                                <div className="p-2 px-4 text-muted" style={{ fontSize: 'smaller' }}>
                                    <div className="fs-6 mb-3">Yang akan kamu dapatkan :</div>
                                    <div className="d-flex align-items-start">
                                        <FontAwesomeIcon className="my-auto me-2 text-primary" icon={faSwatchbook} />
                                        <span>{totalModule} Modul</span>
                                    </div>
                                    <div className="d-flex align-items-start">
                                        <FontAwesomeIcon className="my-auto me-2 text-primary" icon={faClock} />
                                        <span>{totalTime} jam durasi</span>
                                    </div>
                                    <div className="d-flex align-items-start">
                                        <FontAwesomeIcon className="my-auto me-2 text-primary" icon={faCircleQuestion} />
                                        <span>Forum Diskusi Tanya Jawab</span>
                                    </div>
                                    <div className="d-flex align-items-start">
                                        <FontAwesomeIcon className="my-auto me-2 text-primary" icon={faMedal} />
                                        <span>Klaim Sertifikat Digital</span>
                                    </div>
                                </div>
                                {
                                    retailPrice == 0 && (
                                        <div className="d-grid gap-2 p-2 px-4 my-2">
                                            <Link href={`https://dashboard.codepolitan.com/learn/courses/detail/${detailProduct.slug}`}>
                                                <a target={'_blank'} className="btn btn-primary btn-rounded">Belajar Sekarang</a>
                                            </Link>
                                        </div>
                                    )
                                }
                                {retailPrice < 0 && (
                                    <div className="d-grid gap-2 p-2 px-4 my-2">
                                        <Link href="/roadmap">
                                            <a className="btn btn-primary btn-rounded">Beli Roadmap</a>
                                        </Link>
                                    </div>
                                )}
                            </>
                        )
                    }
                </div>
            )}
            {!free && (
                <div className="card-body rounded-0 p-0">
                    <div className="tab-content p-0" id="pills-tabContent">
                        <div className="tab-pane show active" id="" role="tabpanel" aria-labelledby="pills-bulanan-tab">
                            <div id="tabsidebar">
                                <ul className="nav nav-pills nav-justified" id="pills-tab" role="tablist">
                                    <li className="nav-item" role="presentation">
                                        <button className="nav-link rounded-0 active" id="pills-buy-tab" data-bs-toggle="pill" data-bs-target="#pills-buy" type="button" role="tab" aria-controls="pills-buy" aria-selected="true">
                                            <div className="d-flex flex-column">
                                                <span className="fw-bold ">Beli</span>
                                                {retailBuyPrice > 0 && (
                                                    <span>Rp. {formatPrice(retailBuyPrice)}</span>
                                                )}
                                            </div>
                                        </button>
                                    </li>
                                    <li className="nav-item" role="presentation">
                                        <button className={retailRentPrice > 0 ? "nav-link rounded-0" : "nav-link rounded-0 disabled"} id="pills-rent-tab" data-bs-toggle="pill" data-bs-target={retailRentPrice > 0 ? "#pills-rent" : "#"} type="button" role="tab" aria-controls="pills-rent" aria-selected="false">
                                            <div className="d-flex flex-column">
                                                <span className="fw-bold ">Sewa</span>
                                                {retailRentPrice > 0 ? (
                                                    <span>Rp. {formatPrice(retailRentPrice)}</span>
                                                ) : (<span>Tidak Tersedia</span>)}
                                            </div>
                                        </button>
                                    </li>
                                </ul>
                                <div className="tab-content" id="pills-tabContent">
                                    <div className="tab-pane show active" id="pills-buy" role="tabpanel" aria-labelledby="pills-buy-tab">
                                        <div className="px-4 my-2 text-muted">
                                            {normalBuyPrice > 0 && normalBuyPrice != retailBuyPrice && (<span className="text-decoration-line-through">Rp. {formatPrice(normalBuyPrice)}</span>)}
                                            <div>
                                                {retailBuyPrice === -100 && (<span className="fs-3 fw-bold me-2">Tidak dijual satuan</span>)}
                                                {retailBuyPrice === -1 && (<span className="fs-3 fw-bold me-2">Beli lewat Roadmap</span>)}
                                                {retailBuyPrice == 0 && (<span className="fs-3 fw-bold me-2">Gratis</span>)}
                                                {retailBuyPrice > 0 && (<span className="fs-3 fw-bold me-2">Rp. {formatPrice(retailBuyPrice)}</span>)}
                                                {normalBuyPrice > 0 && normalBuyPrice != retailBuyPrice && (<small className="fst-italic">{getPercentage(retailBuyPrice, normalBuyPrice)}% off</small>)}
                                            </div>
                                        </div>
                                        {normalBuyPrice > 0 && normalBuyPrice != retailBuyPrice && (
                                            <>
                                                <span className="badge bg-pink py-1 me-2" style={{ borderRadius: '0 20px 20px 0' }}>Hemat Rp. {formatPrice(normalBuyPrice - retailBuyPrice)}</span>
                                                <span style={{ fontSize: '0.6rem' }} className="text-danger">
                                                    <FontAwesomeIcon icon={faClock} />
                                                    <span className="ms-1">Diskon sampai hari ini</span>
                                                </span>
                                            </>
                                        )}
                                        {retailBuyPrice !== -100 && (
                                            <div className="text-center text-muted pt-2">
                                                <span>Beli sekali akses selamanya</span>
                                            </div>
                                        )}
                                        <div className="d-grid gap-2 p-2 px-4 my-2">
                                            {productBuySlug !== null ? (
                                                <Link href={`https://pay.codepolitan.com/?slug=${productBuySlug}`}>
                                                    <a target={'_blank'} className="btn btn-primary btn-rounded py-3 text-uppercase fw-bold">Beli Sekarang</a>
                                                </Link>
                                            ) : (
                                                <a className="btn btn-secondary text-light btn-rounded py-3 text-uppercase fw-bold">Beli Sekarang</a>
                                            )}
                                        </div>
                                    </div>
                                    <div className="tab-pane" id="pills-rent" role="tabpanel" aria-labelledby="pills-rent-tab">
                                        <div className="px-4 my-2 text-muted">
                                            {normalRentPrice > 0 && normalRentPrice != retailRentPrice && (<span className="text-decoration-line-through">Rp. {formatPrice(normalRentPrice)}</span>)}
                                            <div>
                                                {retailRentPrice < 0 && (<span className="fs-3 fw-bold me-2">Beli lewat Roadmap</span>)}
                                                {retailRentPrice == 0 && (<span className="fs-3 fw-bold me-2">Gratis</span>)}
                                                {retailRentPrice > 0 && (<span className="fs-3 fw-bold me-2">Rp. {formatPrice(retailRentPrice)}</span>)}
                                                {normalRentPrice > 0 && normalRentPrice != retailRentPrice && (<small className="fst-italic">{getPercentage(retailRentPrice, normalRentPrice)}% off</small>)}
                                            </div>
                                        </div>
                                        {normalRentPrice > 0 && normalRentPrice != retailRentPrice && (
                                            <>
                                                <span className="badge bg-pink py-1 me-2" style={{ borderRadius: '0 20px 20px 0' }}>Hemat Rp. {formatPrice(normalRentPrice - retailRentPrice)}</span>
                                                <span style={{ fontSize: '0.6rem' }} className="text-danger">
                                                    <FontAwesomeIcon icon={faClock} />
                                                    <span className="ms-1">1 Hari tersisa Diharga ini!</span>
                                                </span>
                                            </>
                                        )}
                                        {retailRentPrice !== -100 && (
                                            <div className="text-center text-muted pt-2">
                                                <span>Sewa kelas dan akses selama 1 bulan</span>
                                            </div>
                                        )}
                                        <div className="d-grid gap-2 p-2 px-4 my-2">
                                            {productRentSlug !== null ? (
                                                <Link href={`https://pay.codepolitan.com/?slug=${productRentSlug}`}>
                                                    <a target={'_blank'} className="btn btn-primary btn-rounded py-3 text-uppercase fw-bold">Sewa Sekarang</a>
                                                </Link>
                                            ) : (
                                                <a className="btn btn-secondary text-light btn-rounded py-3 text-uppercase fw-bold">Sewa Sekarang</a>
                                            )}
                                        </div>
                                    </div>
                                </div>
                                <div className="card-body card-rounded-bottom p-2 pb-3 px-4 text-muted" style={{ fontSize: 'smaller', backgroundColor: '#f8f9fa' }}>
                                    <div className="fs-6 mb-1 fw-bold">Yang akan kamu dapatkan :</div>
                                    <div className="d-flex align-items-start">
                                        <FontAwesomeIcon className="my-auto me-2 text-primary" icon={faSwatchbook} />
                                        <span>{totalModule} Modul</span>
                                    </div>
                                    <div className="d-flex align-items-start">
                                        <FontAwesomeIcon className="my-auto me-2 text-primary" icon={faClock} />
                                        <span>{totalTime} jam durasi</span>
                                    </div>
                                    <div className="d-flex align-items-start">
                                        <FontAwesomeIcon className="my-auto me-2 text-primary" icon={faCircleQuestion} />
                                        <span>Forum Diskusi Tanya Jawab</span>
                                    </div>
                                    <div className="d-flex align-items-start">
                                        <FontAwesomeIcon className="my-auto me-2 text-primary" icon={faMedal} />
                                        <span>Klaim Sertifikat Digital</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )}
        </div>
    );
};

export default CardDetail;