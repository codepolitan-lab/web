import styles from "./CardSliderTestimony.module.scss"

const CardSliderTestimony = ({ image, testimony, title, subtitle }) => {
    return (
        <div className="row align-items-center justify-content-center pb-5 pb-md-0">
            <div className="col-lg-4 py-2">
                <img className={`d-block img-fluid mx-auto  ${styles.image}`} src={image} alt="First slide" />
            </div>
            <div className="col-lg-5 py-2 px-3 px-md-0 text-muted">
                <p className={`lead mb-4 ${styles.lead}`}>
                    &#8220;{testimony}&#8220;
                </p>
                <h6 className="fw-bold">{title}</h6>
                <small>{subtitle}</small>
            </div>
        </div>
    )
}

export default CardSliderTestimony