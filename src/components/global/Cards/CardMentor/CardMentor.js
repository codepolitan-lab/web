const CardMentor = ({ author, thumbnail, shortDescription }) => {
    return (
        <div className="card shadow-sm mx-1">
            <div className="card-body text-center">
                <img className="rounded-circle p-3" src={thumbnail || "/assets/img/placeholder.jpg"} style={{ width: '180px', height: '180px', objectFit: 'cover' }} alt={author} />
                <h5 className="text-secondary">{author || 'Belum ada nama'}</h5>
                <p className="text-muted">{shortDescription || <i>Belum ada keterangan</i>}</p>
            </div>
            <span className="d-block bg-codepolitan" style={{ height: '3px', width: '60%', alignSelf: 'center' }} />
        </div>
    );
};

export default CardMentor;
