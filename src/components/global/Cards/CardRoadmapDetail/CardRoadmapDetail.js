import { faClock, faSwatchbook } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styles from "./CardRoadmapDetail.module.scss"
import Link from "next/link";

const CardRoadmapDetail = ({ index, cover, courseSlug, courseTitle, totalModule, totalTime }) => {
    return (
        <div key={index} className="col-lg-4 p-2">
            <Link href={`/course/intro/${courseSlug}`}>
                <a className="text-decoration-none text-muted" id="cardRoadmapDetail">
                    <div className={`card ${styles.card}`}>
                        <div className="position-relative">
                            <div className={styles['card-roadmap']} style={{ backgroundImage: `url(${cover || '/assets/img/placeholder.jpg'})` }}></div>
                            {/* <img src="/assets/img/placeholder.jpg" className="card-img-top" style={{ height: '150px' }} alt="..." /> */}
                            <div className="position-absolute" style={{ bottom: "-9%", right: '10%' }}>
                                <div className="fw-bold bg-white rounded-circle text-center shadow" style={{ width: '50px', height: '50px', lineHeight: '50px', fontSize: '1.9rem' }}>{index + 1}</div>
                            </div>
                        </div>
                        <div className="card-body">
                            <div className="card-title fw-bold mb-2">
                                {courseTitle || 'Tidak ada judul'}
                            </div>
                            <div className="card-text d-flex gap-2 fs-6 text-muted">
                                <div className="d-flex gap-1">
                                    <FontAwesomeIcon icon={faSwatchbook} />
                                    <small>{totalModule || 0} Module</small>
                                </div>
                                <div className="d-flex gap-1">
                                    <FontAwesomeIcon icon={faClock} />
                                    <small>{totalTime || 0} Jam</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </Link>
        </div>
    )
}

export default CardRoadmapDetail;
