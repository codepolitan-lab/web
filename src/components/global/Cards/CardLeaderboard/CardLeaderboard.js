import styles from './CardLeaderboard.module.scss';

const CardLeaderboard = ({ index, thumbnail, name, rank, rankPicture, point }) => {
    return (
        <div className={`${styles.card_leaderboard} ${index === 0 && styles.animated_background} card border-0 shadow-sm my-3`}>
            <div className={`card-body ${styles.card_body}`}>
                <div className="row">
                    <div className="col-2 col-lg-1 text-center my-auto">
                        <span>
                            <strong>{index + 1}</strong>
                        </span>
                    </div>
                    <div className="col-1 text-center my-auto d-none d-lg-block">
                        {index === 0 && (
                            <img className="img-fluid" src="https://i.ibb.co/P9q9Lbs/crown.png" alt="1st place" />
                        )}
                        {index === 1 && (
                            <img className="img-fluid" src="https://i.ibb.co/LptzNwd/crown-silver.png" alt="2nd place" />
                        )}
                        {index === 2 && (
                            <img className="img-fluid" src="https://i.ibb.co/HxK1hhV/crown-gold.png" alt="3rd place" />
                        )}
                    </div>
                    <div className="col-6">
                        <div className="d-flex align-items-start">
                            <img className={`${styles.card_img} d-none d-lg-block`} src={thumbnail || '/assets/img/icons/icon-avatar.png'} alt={name} />
                            <div className="my-auto">
                                <h5 className={styles.name}>{name}</h5>
                                <p className="text-muted">{rank}</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-1 text-center ms-auto d-none d-lg-block">
                        <img className={styles.card_img + ' p-2'} src={rankPicture || '/assets/img/placeholder.jpg'} alt={name} />
                    </div>
                    <div className="col-3 col-lg-2 text-center my-auto ms-auto">
                        <span className={`${styles.point} ${index === 0 ? 'text-white' : 'text-primary'}`}>{point}</span>
                        <p className={styles.point_caption}>POINTS</p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default CardLeaderboard;
