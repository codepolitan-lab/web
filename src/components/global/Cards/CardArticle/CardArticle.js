import styles from './CardArticle.module.scss';
import moment from "moment";
import 'moment/locale/id';

const CardArticle = ({ thumbnail, title, date }) => {
    return (
        <div className={`card ${styles.card_article} h-100`}>
            <img src={thumbnail || '/assets/img/placeholder.jpg'} className="card-img" loading="lazy" alt={title} />
            <div className="card-body px-2 px-md-3 py-3">
                <h5 className={styles.title} title={title}>{title || 'Undefined'}</h5>
                <div className="text-end">
                    <small>{moment(date).fromNow()}</small>
                </div>
            </div>
        </div>
    );
};

export default CardArticle;
