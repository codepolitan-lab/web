import moment from "moment";
import styles from './CardForum.module.scss';
import { ReactMarkdown } from "react-markdown/lib/react-markdown";
import rehypeRaw from 'rehype-raw';
import remarkGfm from 'remark-gfm';
import { CodeBlock } from "../../Codeblock";

const CardForum = ({ avatar, name, date, totalAnswer, lessonTitle, subject, mark, content }) => {
    return (
        <div className="card shadow-sm rounded my-3">
            <div className="card-header bg-white border-0">
                <div className="row justify-content-between">
                    <div className="col-9 col-lg-auto">
                        <d className="d-flex align-items-start mt-2">
                            <img className={`${styles.avatar_img} rounded-circle`} src={avatar || "/assets/img/icons/icon-avatar.png"} alt={name} />
                            <div className={`${styles.user_info} ms-3 ms-lg-4 mt-1`}>
                                <h5 className="text-secondary m-0">{name}</h5>
                                <p className="text-muted m-0"><small>{moment(date).fromNow()}</small></p>
                            </div>
                        </d>
                    </div>
                    {content || !totalAnswer ? '' : (
                        <div className="col-3 col-lg-auto mt-2">
                            {totalAnswer === '0' && (
                                <span className={`float-end badge ${mark === 'solved' ? 'bg-codepolitan' : 'bg-danger'}`}>
                                    <p className="h4 mb-0">{totalAnswer}</p>
                                    <p className="mb-0">Jawaban</p>
                                </span>
                            )}
                            {totalAnswer !== '0' && (
                                <span className={`float-end badge ${mark === 'solved' ? 'bg-codepolitan' : 'bg-info'}`}>
                                    <p className="h4 mb-0">{totalAnswer}</p>
                                    <p className="mb-0">Jawaban</p>
                                </span>
                            )}
                        </div>
                    )}
                </div>
            </div>
            <div className="card-body">
                {lessonTitle && (
                    <span className="text-muted"><small>Ditanyakan pada: {lessonTitle}</small></span>
                )}
                {subject && (
                    <p className="text-muted h5">{subject}</p>
                )}
                <ReactMarkdown
                    remarkPlugins={[remarkGfm]}
                    rehypePlugins={[rehypeRaw]}
                    components={CodeBlock}
                >
                    {content}
                </ReactMarkdown>
            </div>
        </div>
    );
};

export default CardForum;
