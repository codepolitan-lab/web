import { faStar, faSortAlphaDown } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { RatingView } from "react-simple-star-rating";
import { getPercentageTestimony } from "../../../../utils/helper";

function SectionTestimonyRating({ testimony, totalReview, totalRate, filterRate }) {
    return (
        <>
            <div className="row">
                <div className="col-auto">
                    <h4 className="section-title">Testimoni Oleh Siswa</h4>
                </div>
                <div className="col-auto text-end ms-auto pe-lg-0">
                    <div className="btn-group" role="group">
                        <button id="btnGroupDrop1" type="button" className="btn btn-outline-secondary btn-sm btn-rounded" data-bs-toggle="dropdown" aria-expanded="false">
                            Filter <FontAwesomeIcon icon={faSortAlphaDown} />
                        </button>
                        <ul className="dropdown-menu" aria-labelledby="btnGroupDrop1">
                            <li className="dropdown-item" role="button" onClick={() => filterRate(true)}>Semua</li>
                            <li className="dropdown-item" role="button" onClick={() => filterRate(5)}>Bintang 5</li>
                            <li className="dropdown-item" role="button" onClick={() => filterRate(4)}>Bintang 4</li>
                            <li className="dropdown-item" role="button" onClick={() => filterRate(3)}>Bintang 3</li>
                            <li className="dropdown-item" role="button" onClick={() => filterRate(2)}>Bintang 2</li>
                            <li className="dropdown-item" role="button" onClick={() => filterRate(1)}>Bintang 1</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div className="row my-3">
                <div className="col-lg-3 text-center">
                    <div className="display-1">{totalRate > 0 ? totalRate.toFixed(1) : 0}</div>
                    <div className="my-2">
                        <RatingView ratingValue={Math.floor(totalRate) || 0} />
                    </div>
                    <p className="fs-6">({totalReview || 0} reviews)</p>
                </div>
                <div className="col-lg-9">
                    <div className="row pt-3">
                        <div className="col-3 col-lg-3 px-0 pb-3 ps-lg-3 text-end">
                            <span className="me-2">5 Bintang</span>
                        </div>
                        <div className="col-7 col-lg-8 p-0 pt-2">
                            <div className="progress" style={{ height: '10px' }}>
                                <div style={{ width: `${getPercentageTestimony(testimony, 5) || 0}%` }} className="progress-bar bg-warning rounded-pill" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                        <div className="col-2 col-lg-1">
                            <span>{getPercentageTestimony(testimony, 5) || 0}%</span>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-3 col-lg-3 px-0 pb-3 text-end">
                            <span className="me-2">4 Bintang</span>
                        </div>
                        <div className="col-7 col-lg-8 p-0 pt-2">
                            <div className="progress" style={{ height: '10px' }}>
                                <div style={{ width: `${getPercentageTestimony(testimony, 4) || 0}%` }} className="progress-bar bg-warning rounded-pill" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                        <div className="col-2 col-lg-1">
                            <span>{getPercentageTestimony(testimony, 4) || 0}%</span>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-3 col-lg-3 px-0 pb-3 text-end">
                            <span className="me-2">3 Bintang</span>
                        </div>
                        <div className="col-7 col-lg-8 p-0 pt-2">
                            <div className="progress" style={{ height: '10px' }}>
                                <div style={{ width: `${getPercentageTestimony(testimony, 3) || 0}%` }} className="progress-bar bg-warning rounded-pill" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                        <div className="col-2 col-lg-1">
                            <span>{getPercentageTestimony(testimony, 3) || 0}%</span>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-3 col-lg-3 px-0 pb-3 text-end">
                            <span className="me-2">2 Bintang</span>
                        </div>
                        <div className="col-7 col-lg-8 p-0 pt-2">
                            <div className="progress" style={{ height: '10px' }}>
                                <div style={{ width: `${getPercentageTestimony(testimony, 2) || 0}%` }} className="progress-bar bg-warning rounded-pill" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                        <div className="col-2 col-lg-1">
                            <span>{getPercentageTestimony(testimony, 2) || 0}%</span>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-3 col-lg-3 px-0 pb-3 text-end">
                            <span className="me-2">1 Bintang</span>
                        </div>
                        <div className="col-7 col-lg-8 p-0 pt-2">
                            <div className="progress" style={{ height: '10px' }}>
                                <div style={{ width: `${getPercentageTestimony(testimony, 1) || 0}%` }} className="progress-bar bg-warning rounded-pill" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                        <div className="col-2 col-lg-1">
                            <span>{getPercentageTestimony(testimony, 1) || 0}%</span>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export default SectionTestimonyRating;