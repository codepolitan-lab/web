import Link from "next/link";
import { faCalendarAlt } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styles from "../SectionMentorDetail/SectionMentorDetail.module.scss"


const SectionMentorDetail = ({ brandName, username, shortDescription, avatar, schedule }) => {
    return (
        <section className="section mb-5">
            <h3 className="mb-4 text-muted">Penyusun Materi</h3>
            <div className="row">
                <div className="col-3">
                    <img className="img-fluid" src={avatar || '/assets/img/placeholder.jpg)'} alt={brandName || 'Mentor'} />
                </div>
                <div className="col my-auto">
                    <Link href={`/mentor/${username}`}>
                        <a className="link">
                            <h4 className="fw-bolder">{brandName || 'Belum ada penyusun'}</h4>
                        </a>
                    </Link>
                    <p className={`fw-normal ${styles.text}`}>
                        {shortDescription || 'Belum ada Deskripsi'}
                    </p>
                    <p className={`fw-normal mb-0 ${styles.text}`}>
                        <FontAwesomeIcon fixedWidth className="text-muted me-2" icon={faCalendarAlt} />
                        {schedule || 'Belum ada jadwal'}
                    </p>
                </div>
            </div>
        </section>
    );
}

export default SectionMentorDetail;