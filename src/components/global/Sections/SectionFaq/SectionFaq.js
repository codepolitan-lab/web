import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faWhatsapp } from "@fortawesome/free-brands-svg-icons";

const SectionFaq = ({ data }) => {
    return (
        <section className="section" id="FaqProgramAccordion">
            <div className="container p-4 p-lg-5">
                <div className="row flex-lg-row-reverse">
                    <div className="col-lg-8 mb-5">
                        <h2
                            className="section-title d-lg-none mb-5"
                            style={{ color: "#14a7a0", fontSize: "2rem" }}
                        >
                            Frequently <br /> Asked Questions
                        </h2>
                        <div
                            className="accordion accordion-flush"
                            id="accordionFlushProgram"
                        >
                            {data.map((item, index) => {
                                return (
                                    <div className="accordion-item" key={index}>
                                        <h3 className="accordion-header" id={item.id}>
                                            <button
                                                className="accordion-button collapsed"
                                                type="button"
                                                data-bs-toggle="collapse"
                                                data-bs-target={`#${item.target}`}
                                                aria-expanded="false"
                                                aria-controls={item.target}
                                            >
                                                <strong className="text-muted">{item.title}</strong>
                                            </button>
                                        </h3>
                                        <div
                                            id={item.target}
                                            className="accordion-collapse collapse"
                                            aria-labelledby={item.id}
                                            data-bs-parent="#accordionFlushProgram"
                                        >
                                            <div className="accordion-body text-muted">
                                                <div dangerouslySetInnerHTML={{ __html: item.content }} />
                                            </div>
                                        </div>
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                    <div className="col-lg-4 d-none d-lg-block border-end border-2 text-center text-lg-start">
                        <h2 className="section-title">
                            Masih Memiliki Pertanyaan?
                            <br />
                            <span style={{ color: "#14a7a0" }}>Lihat disini</span>
                        </h2>
                        <a className="btn btn-primary px-4 py-2 mt-3" href="https://wa.me/628999488990" target="_blank" rel="noopener noreferrer">
                            <FontAwesomeIcon className="me-2" icon={faWhatsapp} />
                            Tanyakan disini
                        </a>
                    </div>
                    <div className="col-lg-4 d-lg-none text-center text-lg-start">
                        <h2 className="section-title">
                            Masih Memiliki Pertanyaan?
                            <br />
                            <span style={{ color: "#14a7a0" }}>Lihat disini</span>
                        </h2>
                        <a className="btn btn-primary px-4 py-2 mt-3" href="https://wa.me/628999488990" target="_blank" rel="noopener noreferrer">
                            <FontAwesomeIcon className="me-2" icon={faWhatsapp} />
                            Tanyakan disini
                        </a>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default SectionFaq;
