import React from 'react'

const SectionBannerArticle = () => {
    return (
        <section className="section" style={{ marginTop: '70px' }} id="hero">
            <div className="container p-4 p-lg-5 text-muted">
                <div className="row justify-content-center align-items-center">
                    <div className="col-lg-7 px-lg-5 order-last order-lg-first">
                        <h1 className="mb-4 display-5">Tutorial dan Artikel</h1>
                        <p className="lead mb-5">Temukan tutorial pemrograman praktis dan tepat guna, serta informasi terbaru seputar pemrograman disini.</p>
                        <a href="#featuredArticle" className="btn btn-primary btn-rounded px-3 py-2">Mulai Membaca</a>
                    </div>
                    <div className="col-lg-4 text-center">
                        <img src="/assets/img/article/banner-2.png" className="w-100" alt="" />
                    </div>
                </div>
            </div>
        </section>
    )
}

export default SectionBannerArticle