import { faWhatsapp } from "@fortawesome/free-brands-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import Link from "next/link"

const SectionQuestion = () => {
    return (
        <section className="section bg-light">
            <div className="container p-3 p-xl-5">
                <div className="d-flex flex-column flex-lg-row text-center p-4 text-muted bg-white shadow align-items-center">
                    <img src="/assets/img/icon-codepolitan.png" width="50" alt="" />
                    <h5 className="my-2 ms-0 ms-lg-4">Siap untuk menjadi Programmer profesional?</h5>
                    <div className="ms-0 ms-lg-auto">
                        <Link href="/library?type=free">
                            <a className="btn me-2 me-md-3 btn-rounded btn-primary my-2">Mulai Belajar Gratis</a>
                        </Link>
                        <a href="https://chat.whatsapp.com/CZFbvCQX5RU86nIGuDiMC9" target="_blank noreferrer" className="btn btn-rounded btn-outline-primary"><FontAwesomeIcon icon={faWhatsapp} /> Nanya Dulu</a>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default SectionQuestion