import Link from 'next/link'
import { useState } from 'react'
import CardFeaturedArticle from '../../Cards/CardFeaturedArticle/CardFeaturedArticle'

const SectionFeaturedArticle = ({ data }) => {
    const [limit] = useState(6);
    return (
        <section className="bg-light" id="featuredArticle">
            <div className="container p-4 p-lg-5 text-muted">
                <h4 className='section-title mb-4'>Artikel Pilihan</h4>
                <div className="row">
                    {data.error && (
                        <div className="col">
                            <p>Belum ada artikel pilihan</p>
                        </div>
                    )}
                    {!data.error && data?.map((item, index) => {
                        return (
                            <div key={index} className="col-lg-4 mb-3">
                                <Link href={`https://codepolitan.com/blog/${item.slug}`}>
                                    <a className="link">
                                        <CardFeaturedArticle
                                            index={index + 1}
                                            title={item.title}
                                            writer={item.writer}
                                            createdAt={item.created_at}
                                            type={item.type}
                                        />
                                    </a>
                                </Link>
                            </div>
                        )
                    }).slice(0, limit)}
                </div>
            </div>
        </section>
    )
}

export default SectionFeaturedArticle