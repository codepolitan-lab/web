import Link from "next/link";

const SectionVoteCourse = ({ sectionTitle, buttonTitle, link }) => {
    return (
        <section className="section bg-codepolitan">
            <div className="container p-4 p-lg-5">
                <div className="row">
                    <div className="col text-center">
                        <h3 className="section-title text-white">{sectionTitle}</h3>
                        <Link href={link}>
                            <a className="btn btn-pink mt-3">{buttonTitle}</a>
                        </Link>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default SectionVoteCourse;
