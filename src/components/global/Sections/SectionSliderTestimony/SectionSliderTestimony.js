import { Swiper, SwiperSlide } from "swiper/react"
import SwiperCore, { Pagination, Navigation, Autoplay } from "swiper";
import CardSlider from "../../Cards/CardSliderTestimony/CardSliderTestimony";

SwiperCore.use([Pagination, Navigation, Autoplay]);

const SectionSliderTestimony = ({ image, testimony, title, subtitle }) => {
    const contents = [
        {
            name: ''
        },
        {
            name: ''
        },
        {
            name: ''
        },
    ];

    return (
        <section style={{ backgroundColor: '#f8f8f8' }} id="Swiper">
            <div className="container p-4 p-lg-5" id="forMentor">
                <Swiper
                    pagination
                    autoplay
                    navigation
                    spaceBetween={10}
                    slidesPerView={1}
                    loop
                >
                    {
                        contents.map((content, index) => {
                            return (
                                <SwiperSlide key={index}>
                                    <CardSlider
                                        image={image}
                                        testimony={testimony}
                                        title={title}
                                        subtitle={subtitle}
                                    />
                                </SwiperSlide>
                            )
                        })
                    }
                </Swiper>
            </div>
        </section>
    )
}

export default SectionSliderTestimony