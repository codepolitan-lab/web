import { useState } from 'react';

const SectionPartners = ({ backgroundColor, sectionTitleTextStyle, customTItle }) => {
    const [partners] = useState([
        {
            name: 'Kemnaker',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/KEMNAKER_MGfr7cHn61.webp'
        },
        {
            name: 'Kemenkeu',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/kemenkeu_p9YQYkw-AI.webp'
        },
        {
            name: 'Here',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/here_THgKeV7WUz.webp'
        },
        {
            name: 'Udemy',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/udemy_DVYNj94zCK14x.webp'
        },
        {
            name: 'Samsung',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/samsung_Insf8kassvK.webp'
        },
        {
            name: 'Lenovo',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/Lenovo_co1GDjBpxV.webp'
        },
        {
            name: 'Alibaba Cloud',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/Alibaba_kBCmKi7O7.webp'
        },
        {
            name: 'Intel',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/Intel_T_cVZHo81M_t.webp'
        },
        {
            name: 'Dicoding',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/dicoding-header-logo_-yUghUNkx7.webp'
        },
        {
            name: 'IBM',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/IBM_GXVYl92inl905.webp'
        },
        {
            name: 'Pixel',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/pixel_YtPl8tvzu.webp'
        },
        {
            name: 'XL',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/xl_t5bLnDwJn.webp'
        },
        {
            name: 'Hactive8',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/hacktiv8_Y3L2HUj4K4X.webp'
        },
        {
            name: 'Kudo',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/Kudo_oGHxpphEw.webp'
        },
        {
            name: 'Refactory',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/Refactory_VO89Fn5BX.webp'
        },
        {
            name: 'Ajita',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/ajita_Za3H1pSLYI.webp'
        },
        {
            name: 'Geometri',
            thumbnail: 'https://i.ibb.co/86yCrCP/Geometri.png'
        },
    ]);

    return (
        <section className={`section ${backgroundColor}`} id="partners">
            <div className="container p-4 p-lg-5">
                <div className="row mb-4">
                    <div className={`col ${sectionTitleTextStyle}`}>
                        <h2 className="section-title h3">{customTItle || 'Telah Dipercaya oleh Perusahaan Besar'}</h2>
                    </div>
                </div>
                <div className="row justify-content-center">
                    {partners.map((item, index) => {
                        return (
                            <div className="col-4 col-md-2 my-2" key={index}>
                                <div className="card border-0">
                                    <div className={`card-body p-2 p-lg-3 ${backgroundColor}`}>
                                        <img className="img-fluid d-block mx-auto" src={item.thumbnail} alt={item.name} />
                                    </div>
                                </div>
                            </div>
                        );
                    })}
                </div>
            </div>
        </section>
    );
};

export default SectionPartners;
