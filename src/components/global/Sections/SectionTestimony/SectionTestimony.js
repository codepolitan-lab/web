// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

// import Swiper core and required modules
import SwiperCore, { Pagination, Navigation } from "swiper";
import CardTestimony from "../../Cards/CardTestimony/CardTestimony";

// install Swiper modules
SwiperCore.use([Pagination, Navigation]);


const SectionTestimony = ({ data }) => {
    return (
        <section className="section bg-white" id="Swiper">
            <div className="container p-4 p-lg-5">
                <div className="row">
                    <div className="col">
                        <h2 className="section-title">Testimoni</h2>
                        <p className="text-muted">Review dari siswa yang telah mengikuti kelas</p>
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        <Swiper
                            spaceBetween={25}
                            grabCursor={false}
                            slidesPerView={1}
                            loop
                            pagination={true}
                            navigation={true}
                            breakpoints={{
                                // when window width is >= 414px
                                414: {
                                    slidesPerView: 1,
                                },
                                // when window width is >= 768px
                                768: {
                                    slidesPerView: 2,
                                },
                                1024: {
                                    slidesPerView: 3,
                                },
                            }}
                            style={{ paddingLeft: '20px', paddingRight: '20px', paddingBottom: '60px' }}
                        >
                            {data?.map((item, index) => {
                                return (
                                    <SwiperSlide key={index}>
                                        <CardTestimony
                                            name={item.name.length > 20 ? item.name.slice(0, 20) + '...' : item.name}
                                            avatar={item.avatar}
                                            comment={item.comment}
                                            rating={item.rate}
                                        />
                                    </SwiperSlide>
                                );
                            }).slice(0, 6)}
                        </Swiper>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default SectionTestimony;
