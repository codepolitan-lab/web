const SectionHowItHelps = ({ titleSection, contents }) => {
    return (
        <section className="section bg-light" id="caraBelajar">
            <div className="container p-4 p-lg-5">
                <div className="row mb-5">
                    <div className="col text-center">
                        <h2 className="section-title text-muted">{titleSection}</h2>
                    </div>
                </div>
                <div className="row justify-content-center text-center">
                    <div className="col-md-6 col-lg-4 p-4">
                        <div className="card rounded-3 border-0 shadow h-100">
                            <div className="card-body rounded-3">
                                <img className="img-fluid m-3" src={contents[0].image} alt="Lengkapi Portfolio" />
                                <h5 className="card-title p-4">{contents[0].title}</h5>
                                <p className="text-muted">{contents[0].description}</p>
                            </div>
                        </div>
                    </div>

                    <div className="col-md-6 col-lg-4 p-4">
                        <div className="card rounded-3 border-0 shadow h-100">
                            <div className="card-body rounded-3">
                                <img className="img-fluid m-3" src={contents[1].image} alt="Review" />
                                <h5 className="card-title p-4">{contents[1].title}</h5>
                                <p className="text-muted">{contents[1].description}</p>
                            </div>
                        </div>
                    </div>

                    <div className="col-md-6 col-lg-4 p-4">
                        <div className="card rounded-3 border-0 shadow h-100">
                            <div className="card-body rounded-3">
                                <img className="img-fluid m-3" src={contents[2].image} alt="Certified" />
                                <h5 className="card-title p-4">{contents[2].title}</h5>
                                <p className="text-muted">{contents[2].description}</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    );
};

export default SectionHowItHelps;
