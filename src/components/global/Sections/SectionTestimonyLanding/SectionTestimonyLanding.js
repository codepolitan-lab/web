import { Swiper, SwiperSlide } from "swiper/react";
import CardTestimony from "../../../forschool/CardTestimony/CardTestimony";

// import Swiper core and required modules
import SwiperCore, { Pagination, Navigation, Autoplay } from "swiper";

// install Swiper modules
SwiperCore.use([Pagination, Navigation, Autoplay]);

const SectionTestimony = ({ title, data }) => {
    const testimonials = data;

    return (
        <section className="section bg-light">
            <div className="container p-4 p-lg-5">
                <div className="row">
                    <div className="col text-center">
                        <h2 className="section-title">{title}</h2>
                    </div>
                </div>
                <div className="row my-5">
                    <div className="col-lg-5 d-none d-lg-block">
                        <img className="img-fluid d-block mx-auto" src="/assets/img/forschool/testimony.png" alt="Testimony" />
                    </div>
                    <div className="col-lg-7 my-auto" id="SwiperForSchool">
                        <Swiper
                            autoplay
                            loop
                            centeredSlides
                            navigation
                            pagination
                            slidesPerView={1}
                            spaceBetween={50}
                            style={{ padding: '0 40px 40px 40px' }}
                        >
                            {testimonials.map((testimony, index) => {
                                return (
                                    <SwiperSlide key={index}>
                                        <CardTestimony
                                            thumbnail={testimony.thumbnail}
                                            name={testimony.name}
                                            role={testimony.role}
                                            organization={testimony.organization}
                                            content={testimony.content}
                                        />
                                    </SwiperSlide>
                                );
                            })}
                        </Swiper>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default SectionTestimony;
