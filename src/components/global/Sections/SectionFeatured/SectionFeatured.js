import { useRef } from "react";
import { Swiper } from "swiper/react";

// import Swiper core and required modules
import SwiperCore, { Navigation } from "swiper";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronCircleLeft, faChevronCircleRight } from "@fortawesome/free-solid-svg-icons";

// install Swiper modules
SwiperCore.use([Navigation]);

const SectionFeatured = ({ title, children }) => {
    const navigationPrevRef = useRef(null);
    const navigationNextRef = useRef(null);

    return (
        <section className="section">
            <div className="container p-4 p-lg-5">
                <div className="row mb-3">
                    <div className="col-auto">
                        <h2 className="section-title">{title}</h2>
                    </div>
                    <div className="col-auto ms-auto d-none d-md-flex">
                        <div ref={navigationPrevRef}>
                            <FontAwesomeIcon className="text-primary me-2" role="button" size="2x" icon={faChevronCircleLeft} />
                        </div>
                        <div ref={navigationNextRef}>
                            <FontAwesomeIcon className="text-primary" role="button" size="2x" icon={faChevronCircleRight} />
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        <Swiper
                            spaceBetween={10}
                            grabCursor={true}
                            navigation={{
                                prevEl: navigationPrevRef.current,
                                nextEl: navigationNextRef.current,
                            }}
                            onBeforeInit={(swiper) => {
                                swiper.params.navigation.prevEl = navigationPrevRef.current;
                                swiper.params.navigation.nextEl = navigationNextRef.current;
                            }}
                            slidesPerView={2.1}
                            breakpoints={{
                                // when window width is >= 414px
                                375: {
                                    slidesPerView: 2.1,
                                },
                                // when window width is >= 768px
                                768: {
                                    slidesPerView: 2.1,
                                },
                                1200: {
                                    slidesPerView: 3.1,
                                },
                            }}
                        >
                            {children}
                        </Swiper>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default SectionFeatured;
