import CountUp from 'react-countup';
import styles from './SectionCounter.module.scss';
import { formatNumber } from '../../../../utils/helper';

const SectionCounter = ({ data }) => {
    const stats = data;
    return (
        <section className="bg-codepolitan" id="counter">
            <div className="container p-4 p-lg-5">
                <div className="row text-center">
                    <div className="col-6 col-lg-3">
                        <span className={styles.count}>
                            <CountUp end={stats.total_user} duration={5} formattingFn={val => formatNumber(val)} />
                        </span>
                        <p className="text-white">Member Aktif</p>
                    </div>
                    <div className="col-6 col-lg-3">
                        <span className={styles.count}>
                            <CountUp end={stats.total_course} duration={5} formattingFn={val => formatNumber(val)} />
                        </span>
                        <p className="text-white">Kelas Online</p>
                    </div>
                    <div className="col-6 col-lg-3">
                        <span className={styles.count}>
                            <CountUp end={stats.total_study} duration={5} formattingFn={val => formatNumber(val)} />
                        </span>
                        <p className="text-white">Materi Belajar</p>
                    </div>
                    <div className="col-6 col-lg-3">
                        <span className={styles.count}>
                            <CountUp end={stats.total_hours} duration={5} formattingFn={val => formatNumber(val)} />
                        </span>
                        <p className="text-white">Jam Pelajaran</p>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default SectionCounter;
