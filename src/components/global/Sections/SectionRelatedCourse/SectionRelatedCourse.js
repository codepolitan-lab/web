import { useRef } from 'react';
import CardCourse from '../../Cards/CardCourse/CardCourse';
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

// import Swiper core and required modules
import SwiperCore, { Pagination, Navigation } from "swiper";
import Link from 'next/link';

// install Swiper modules
SwiperCore.use([Pagination, Navigation]);

const SectionRelatedCourse = ({ titleSection, children, authorName, authorUsername }) => {
    const navigationPrevRef = useRef(null);
    const navigationNextRef = useRef(null);

    return (
        <section className="section bg-light">
            <div className="container p-4 p-lg-5">
                <div className="row">
                    <div className="col-auto">
                        <h3 className="section-title">
                            {titleSection}
                            <Link href={`/mentor/${authorUsername}`}>
                                <a className="text-decoration-none link text-primary"> {authorName}</a>
                            </Link>
                        </h3>
                    </div>
                    <div className="col-auto ms-auto d-none d-md-flex">
                        <div ref={navigationPrevRef} style={{ cursor: 'pointer' }}>
                            <img className="img-fluid me-2" src="/assets/img/icons/icon-prev.png" alt="Prev" />
                        </div>
                        <div ref={navigationNextRef} style={{ cursor: 'pointer' }}>
                            <img className="img-fluid" src="/assets/img/icons/icon-next.png" alt="Next" />
                        </div>
                    </div>
                </div>
                <div className="row mt-4">
                    <div className="col">
                        <Swiper
                            spaceBetween={20}
                            grabCursor={true}
                            slidesPerView={1.1}
                            navigation={{
                                prevEl: navigationPrevRef.current,
                                nextEl: navigationNextRef.current,
                            }}
                            onBeforeInit={(swiper) => {
                                swiper.params.navigation.prevEl = navigationPrevRef.current;
                                swiper.params.navigation.nextEl = navigationNextRef.current;
                            }}
                            breakpoints={{
                                // when window width is >= 414px
                                414: {
                                    slidesPerView: 1.3,
                                },
                                // when window width is >= 768px
                                768: {
                                    slidesPerView: 2.1,
                                },
                                1024: {
                                    slidesPerView: 4.1,
                                },
                            }}
                            style={{ padding: '10px 0 10px 0' }}
                        >
                            {children}
                        </Swiper>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default SectionRelatedCourse;
