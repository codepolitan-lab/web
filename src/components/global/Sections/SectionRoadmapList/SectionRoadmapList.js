const SectionRoadmapList = ({ title, subtitle }) => {
    const roadmaps = [
        {
            thumbnail: '/assets/img/forschool/icon-db.png',
            title: 'Database Engineer',
            description: 'Siswa dapat menjadi database enginer dengan mempelajari Query RDBMS MySQL, MongoDB dan lain lain.'
        },
        {
            thumbnail: '/assets/img/forschool/icon-web.png',
            title: 'Fullstack Programmer',
            description: 'Siswa dapat menjadi fullstack programmer dengan belajar bahasa pemrograman untuk Frontend, Backend dan Database.'
        },
        {
            thumbnail: '/assets/img/forschool/icon-android.png',
            title: 'Android Developer',
            description: 'Siswa dapat menjadi Android developer dengan mempelajari bahasa pemrograman Kotlin, React Native, Fluter dan lain lain.'
        },
        {
            thumbnail: '/assets/img/forschool/icon-frontend.png',
            title: 'Frontend Developer',
            description: 'Siswa dapat menjadi Frontend Developer dengan mempelajari bahasa pemrograman, HTML, CSS, dan Javascript'
        },
        {
            thumbnail: '/assets/img/forschool/icon-backend.png',
            title: 'Backend Developer',
            description: 'Siswa dapat menjadi Backend Developer dengan mempelajari PHP, Javascript dan lain lain.'
        },
        {
            thumbnail: '/assets/img/forschool/icon-uiux.png',
            title: 'UI/UX Designer',
            description: 'Siswa dapat menjadi UI/UX designer dengan mempelajari tools Figma atau Adobe XD.'
        },
        {
            thumbnail: '/assets/img/forschool/icon-cloud.png',
            title: 'Cloud Computing',
            description: 'Siswa dapat mempelajari Cloud Computing dengan mempelajari layanan hosting seperti Firebase.'
        },
        {
            thumbnail: '/assets/img/forschool/icon-security.png',
            title: 'Cyber Security',
            description: 'Siswa dapat mempelajari Cyber Security dengan mempelajari Ethical Hacking'
        },
    ];

    return (
        <section className="section">
            <div className="container p-4 p-lg-5">
                <div className="row mb-5">
                    <div className="col text-center">
                        <h2 className="section-title">{title}</h2>
                        <p className="text-muted">{subtitle}</p>
                    </div>
                </div>
                <div className="row">
                    {roadmaps.map((roadmap, index) => {
                        return (
                            <div className="col-md-6 col-lg-3 my-3 text-center" key={index}>
                                <img className="img-fluid" src={roadmap.thumbnail} alt={roadmap.title} />
                                <h5 className="section-title my-3">{roadmap.title}</h5>
                                <p className="text-muted">{roadmap.description}</p>
                            </div>
                        );
                    })}
                </div>
            </div>
        </section>
    );
};

export default SectionRoadmapList;
