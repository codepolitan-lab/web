import { faYoutube } from "@fortawesome/free-brands-svg-icons";
import { faAward, faBook, faBriefcase, faCheckCircle, faClock, faCode, faComments, faFlask, faHandHolding, faHandHoldingUsd, faLaptopCode, faPlayCircle, faSortAlphaDown, faStepForward, faTasks } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const BenefitSection = ({ title, subtitle }) => {
    return (
        <section className="section">
            <div className="container p-5">
                <div className="row mb-5">
                    <div className="col text-center">
                        <h2 className="section-title">{title}</h2>
                        <p className="text-muted">{subtitle}</p>
                    </div>
                </div>
                <div className="row text-center text-muted">
                    <div className="col-4 col-md-3 col-lg-2 mb-3">
                        <FontAwesomeIcon style={{ fontSize: '2.5rem' }} icon={faSortAlphaDown} />
                        <p className="mt-3">Alur Belajar Terarah</p>
                    </div>
                    <div className="col-4 col-md-3 col-lg-2 mb-3">
                        <FontAwesomeIcon style={{ fontSize: '2.5rem' }} icon={faClock} />
                        <p className="mt-3">Waktu Belajar Fleksibel</p>
                    </div>
                    <div className="col-4 col-md-3 col-lg-2 mb-3">
                        <FontAwesomeIcon style={{ fontSize: '2.5rem' }} icon={faTasks} />
                        <p className="mt-3">Progres Belajar Terukur</p>
                    </div>
                    <div className="col-4 col-md-3 col-lg-2 mb-3">
                        <FontAwesomeIcon style={{ fontSize: '2.5rem' }} icon={faBriefcase} />
                        <p className="mt-3">Kurikulum Standar Industri</p>
                    </div>
                    <div className="col-4 col-md-3 col-lg-2 mb-3">
                        <FontAwesomeIcon style={{ fontSize: '2.5rem' }} icon={faYoutube} />
                        <p className="mt-3">Video Tutorial</p>
                    </div>
                    <div className="col-4 col-md-3 col-lg-2 mb-3">
                        <FontAwesomeIcon style={{ fontSize: '2.5rem' }} icon={faBook} />
                        <p className="mt-3">Materi Terus Bertambah</p>
                    </div>
                    <div className="col-4 col-md-3 col-lg-2 mb-3">
                        <FontAwesomeIcon style={{ fontSize: '2.5rem' }} icon={faAward} />
                        <p className="mt-3">Sertifikat Online</p>
                    </div>
                    <div className="col-4 col-md-3 col-lg-2 mb-3">
                        <FontAwesomeIcon style={{ fontSize: '2.5rem' }} icon={faComments} />
                        <p className="mt-3">Forum Diskusi Tanya Jawab</p>
                    </div>
                    <div className="col-4 col-md-3 col-lg-2 mb-3">
                        <FontAwesomeIcon style={{ fontSize: '2.5rem' }} icon={faFlask} />
                        <p className="mt-3">Metode Best Practice</p>
                    </div>
                    <div className="col-4 col-md-3 col-lg-2 mb-3">
                        <FontAwesomeIcon style={{ fontSize: '2.5rem' }} icon={faLaptopCode} />
                        <p className="mt-3">Contoh Project</p>
                    </div>
                    <div className="col-4 col-md-3 col-lg-2 mb-3">
                        <FontAwesomeIcon style={{ fontSize: '2.5rem' }} icon={faCode} />
                        <p className="mt-3">Source Code Program</p>
                    </div>
                    <div className="col-4 col-md-3 col-lg-2 mb-3">
                        <FontAwesomeIcon style={{ fontSize: '2.5rem' }} icon={faHandHoldingUsd} />
                        <p className="mt-3">Garansi Uang Kembali</p>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default BenefitSection;
