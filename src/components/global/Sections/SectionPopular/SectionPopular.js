import { useRef } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronCircleLeft, faChevronCircleRight } from "@fortawesome/free-solid-svg-icons";
import { Swiper, SwiperSlide } from "swiper/react";

// import Swiper core and required modules
import SwiperCore, { Autoplay, Navigation } from "swiper";
import Link from "next/link";
import CardCourse from "../../Cards/CardCourse/CardCourse";
import { shuffleArray } from "../../../../utils/helper";

// install Swiper modules
SwiperCore.use([Autoplay, Navigation]);

const SectionPopular = ({ title, data }) => {
    const navigationPrevRef = useRef(null);
    const navigationNextRef = useRef(null);

    return (
        <section className="section" id="Swiper">
            <div className="container p-4 px-lg-5">
                <div className="row justify-content-between mb-4">
                    <div className="col-auto">
                        <h2 className="section-title h3">{title}</h2>
                    </div>
                    <div className="col-auto d-none d-lg-block">
                        <div className="col-auto d-none d-lg-block">
                            <span className="me-2" ref={navigationPrevRef} role="button">
                                <FontAwesomeIcon size="2x" className="text-primary" icon={faChevronCircleLeft} />
                            </span>
                            <span ref={navigationNextRef} role="button">
                                <FontAwesomeIcon size="2x" className="text-primary" icon={faChevronCircleRight} />
                            </span>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        <Swiper
                            spaceBetween={20}
                            grabCursor={true}
                            slidesPerView={1.1}
                            navigation={{
                                prevEl: navigationPrevRef.current,
                                nextEl: navigationNextRef.current,
                            }}
                            onBeforeInit={(swiper) => {
                                swiper.params.navigation.prevEl = navigationPrevRef.current;
                                swiper.params.navigation.nextEl = navigationNextRef.current;
                            }}
                            breakpoints={{
                                // when window width is >= 414px
                                375: {
                                    slidesPerView: 1.3,
                                },
                                // when window width is >= 768px
                                768: {
                                    slidesPerView: 2.5,
                                },
                                1200: {
                                    slidesPerView: 4.2,
                                },
                            }}
                            style={{ padding: '15px 5px 15px 5px' }}
                        >
                            {shuffleArray(data)?.map((course, index) => {
                                return (
                                    <SwiperSlide key={index}>
                                        <Link href={`/course/intro/${course.slug}`}>
                                            <a className="link">
                                                <CardCourse
                                                    thumbnail={course.thumbnail}
                                                    author={course.author}
                                                    title={course.title}
                                                    level={course.level}
                                                    totalStudents={course.total_student}
                                                    totalModules={course.total_module}
                                                    totalTimes={course.total_time}
                                                    totalRating={course.total_rating}
                                                    totalFeedback={course.total_feedback}
                                                    normalBuyPrice={course.buy?.normal_price || course.normal_price}
                                                    retailBuyPrice={course.buy?.retail_price || course.retail_price}
                                                    normalRentPrice={course.rent?.normal_price}
                                                    retailRentPrice={course.rent?.retail_price}
                                                />
                                            </a>
                                        </Link>
                                    </SwiperSlide>
                                );
                            }).slice(0, 15)}
                        </Swiper>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default SectionPopular;
