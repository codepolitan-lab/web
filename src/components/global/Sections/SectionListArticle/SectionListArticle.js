import { faArrowRotateRight, faSearch, faUser } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Link from 'next/link'
import { useState } from 'react'
import { formatOnlyDate } from '../../../../utils/helper'

const SectionListArticle = ({ data, labels }) => {
    const [limit, setLimit] = useState(8)
    const [searchValue, setSearchValue] = useState("")
    return (
        <section>
            <div className="container p-4 p-lg-5 text-muted">
                <div className="row justify-content-between">
                    <div className="col-lg-7 order-last order-lg-first mt-3 mt-lg-0">
                        {
                            data.filter((value) => {
                                if (value.title.toLowerCase().includes(searchValue.toLowerCase())) {
                                    return value;
                                }
                            }).length > 0
                                ? data.filter((value) => {
                                    if (value.title.toLowerCase().includes(searchValue.toLowerCase())) {
                                        return value;
                                    }
                                }).map((item, index) => {
                                    return (
                                        <Link key={index} href={`https://codepolitan.com/blog/${item.slug}`}>
                                            <a className="link">
                                                <div className="row border-bottom mb-3 pb-3 align-items-center">
                                                    <div className="col-lg-4">
                                                        <img src={item.featured_image || "/assets/img/placeholder.jpg"} className="w-100" alt="" />
                                                    </div>
                                                    <div className="col-lg-8">

                                                        {item.type === 'post' && (<span className="badge bg-primary my-3">Article</span>)}
                                                        {item.type === 'tutorial' && (<span className="badge bg-danger my-3">Tutorial</span>)}
                                                        {item.type === 'event' && (<span className="badge bg-info my-3">Event</span>)}

                                                        <p className="lead mb-0">{item.title}</p>
                                                        <small className="my-3 d-block">
                                                            <FontAwesomeIcon icon={faUser} className="me-2" />
                                                            <span>{item.writer}</span>
                                                            <span className="mx-2">-</span>
                                                            <span>{formatOnlyDate(item.created_at)}</span>
                                                        </small>
                                                        <p>{item.excerpt}</p>
                                                    </div>
                                                </div>
                                            </a>
                                        </Link>
                                    )
                                }).slice(0, limit)
                                : (<h3 className="text-center text-muted">Hasil pencarian tidak ditemukan..</h3>)

                        }
                        {
                            data.filter((value) => {
                                if (value.title.toLowerCase().includes(searchValue.toLowerCase())) {
                                    return value;
                                }
                            }).length > 0 && (
                                <div className="text-center">
                                    <button onClick={() => setLimit(limit + 8)} className="btn btn-sm btn-outline-secondary">
                                        <FontAwesomeIcon icon={faArrowRotateRight} className="me-1" />
                                        <span>Lihat Lainnya</span>
                                    </button>
                                </div>
                            )
                        }
                    </div>
                    <div className="col-lg-4">
                        <h4 className="section-title mb-4">Temukan apa yang kamu cari disini</h4>
                        <div>
                            {
                                labels.map((item, index) => {
                                    return (
                                        <span onClick={() => setSearchValue(item.term)} key={index} style={{ cursor: 'pointer' }} className="badge bg-light overflow-auto p-2 text-center text-muted text-dark me-2">{item.term}</span>
                                    )
                                }).slice(0, 12)
                            }
                        </div>
                        <div className="mt-3">
                            <div className="input-group mb-3">
                                <input onChange={(e) => setSearchValue(e.target.value)} value={searchValue} type="text" className="form-control border-0 border-bottom" placeholder="Cari disini" />
                                <span className="input-group-text bg-white border-0 border-bottom text-muted" id="basic-addon1"><FontAwesomeIcon icon={faSearch} /></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default SectionListArticle