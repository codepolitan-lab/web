import CardSlider from '../../Cards/CardSlider/CardSlider';
import { Swiper, SwiperSlide } from "swiper/react";

// import Swiper core and required modules
import SwiperCore, { Navigation, Pagination } from "swiper";

// install Swiper modules
SwiperCore.use([Navigation, Pagination]);

const SectionHighlight = ({ data }) => {
    if (!data) {
        return (<div className="mb-5 pb-5" />);
    }
    return (
        <section className="section" id="SwiperSlider" style={{ marginTop: '70px' }}>
            <div className="container p-4 p-lg-5">
                <div className="row">
                    <div className="col">
                        <Swiper
                            spaceBetween={10}
                            grabCursor={true}
                            navigation
                            pagination
                            slidesPerView={1.1}
                            style={{ paddingBottom: '55px' }}
                        >
                            {[1, 2, 3, 4, 5].map((item, index) => {
                                return (
                                    <SwiperSlide key={index}>
                                        <CardSlider
                                            thumbnail=""
                                            title="Raih Nilai Tertinggi Di Leaderboard dan Dapatkan Beasiswa Gratis"
                                            description="Program rangking codepolitan untuk coder yang belajar di flatform codepolitan"
                                        />
                                    </SwiperSlide>
                                );
                            })}
                        </Swiper>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default SectionHighlight;
