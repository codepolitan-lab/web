import { useState } from "react";
import { RatingView } from "react-simple-star-rating"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faRedo } from "@fortawesome/free-solid-svg-icons";
import styles from "./SectionTestimonyComment.module.scss"
import md5 from "md5-hash";

function SectionTestimonyComment({ rateFilter, testimony, allRate }) {
    const [limit, setLimit] = useState(10)
    return (
        <>
            {
                allRate ?
                    testimony.map((detail, index) => {
                        return (
                            <div className={`card mb-3 ${styles.card}`} key={index}>
                                <div className="row align-items-center justify-content-center">
                                    <div className="col-3 col-lg-3 text-center">
                                        <img src={`https://secure.gravatar.com/avatar/${md5(detail.email)}?size=200&default=mm&rating=g`} className={`rounded-circle ${styles['img-responsive']}`} alt={detail.name} />
                                    </div>
                                    <div className="col-8 col-lg-9">
                                        <div>
                                            <h6 className={`pt-4 fw-bold ${styles.username}`}>{detail.name}</h6>
                                            <small>{detail.comment == " " ? (<i>Tidak ada komentar</i>) : detail.comment == "" ? (<i>Tidak ada komentar</i>) : detail.comment}</small>
                                            <div className="text-end me-4 my-2">
                                                <RatingView ratingValue={detail.rate} size={20} />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )
                    }).slice(0, limit)
                    :
                    testimony.filter(item => item.rate == rateFilter).map((detail, index) => {
                        return (
                            <div className="card mb-3" key={index}>
                                <div className="row align-items-center justify-content-center">
                                    <div className="col-3 text-center">
                                        <img src={`https://secure.gravatar.com/avatar/${md5(detail.email)}?size=200&default=mm&rating=g`} className={`rounded-circle ${styles['img-responsive']}`} alt={detail.name} />
                                    </div>
                                    <div className="col-9">
                                        <div className="ps-5 ps-lg-0">
                                            <h5 className="pt-4">{detail.name}</h5>
                                            <p>{detail.comment == " " ? (<i>Tidak ada komentar</i>) : detail.comment == "" ? (<i>Tidak ada komentar</i>) : detail.comment}</p>
                                            <div className="text-end me-4 my-2">
                                                <RatingView ratingValue={detail.rate} />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )
                    }).slice(0, limit)
            }

            <div className="text-center">
                {testimony.filter(item => item.rate == rateFilter).length < limit && rateFilter !== true && (<div>Komentar tidak ditemukan</div>)}
                {testimony.filter(item => item.rate == rateFilter).length > limit && (<button className="btn text-muted" onClick={() => setLimit(limit + 5)}> <FontAwesomeIcon icon={faRedo} className="me-1" /> Muat Lainnya </button>)}
                {/* {testimony.filter(item => item.rate == rateFilter).length > limit && rateFilter != false && (<button className="btn text-muted" onClick={() => setLimit(5)}> <FontAwesomeIcon icon={faRedo} className="me-1" /> Reset </button>)} */}
            </div>
        </>
    );
}

export default SectionTestimonyComment;