import React from 'react'

const WarrantySection = ({backgroundColor}) => {
    return (
        <section className={`section ${backgroundColor}`}>
            <div className="container p-5">
                <div className="row">
                    <div className="col text-center">
                        <img className="img-fluid d-block mx-auto" src="/assets/img/programmerzamannow/garansi.png" alt="100% garansi uang kembali" />
                        <h5 className="section-title my-4">100% GARANSI UANG KEMBALI</h5>
                        <p className="text-muted w-75 mx-auto">Jika setelah kamu bergabung dalam program belajar ini dan dalam waktu 3 hari sejak tanggal pembelian kamu merasa tidak puas, 100% uang akan kami kembalikan.</p>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default WarrantySection;
