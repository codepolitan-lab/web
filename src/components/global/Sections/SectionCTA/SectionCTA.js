import mixpanel from "mixpanel-browser"

const SectionCTA = ({ caption, link, btnTitle, background }) => {
    mixpanel.init('608746391f30c018d759b8c2c1ecb097', { debug: false })
    return (
        <section className={`section ${background}`}>
            <div className="container p-4 p-lg-5">
                <div className="row">
                    <div className="col">
                        <div className="card bg-codepolitan border-0" style={{ borderRadius: '25px' }}>
                            <div className="card-body bg-codepolitan text-center" style={{ borderRadius: '25px' }}>
                                <div className="py-4">
                                    <h2 className="section-title text-white mx-auto mb-3 h1 w-75">{caption}</h2>
                                    <a onClick={() => mixpanel.track('Isi formulir karir', { 'source': 'Landing' })} style={{ borderRadius: '15px', background: '#FF8B99' }} className="btn btn-lg text-white my-3" href={link} target="_blank" rel="noreferrer"><strong>{btnTitle}</strong></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default SectionCTA;
