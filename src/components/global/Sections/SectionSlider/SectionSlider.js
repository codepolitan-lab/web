import { useRef } from "react";
import { Swiper } from "swiper/react";

// import Swiper core and required modules
import SwiperCore, { Navigation } from "swiper";

// install Swiper modules
SwiperCore.use([Navigation]);

const SectionSlider = ({ title, titleFontSize, subtitle, slidesPerView, children, mobileOnly }) => {
    const navigationPrevRef = useRef(null);
    const navigationNextRef = useRef(null);

    return (
        <section className={`section my-5 ${mobileOnly && 'd-lg-none'}`}>
            <div className="row">
                <div className="col-auto">
                    <h5 className={`section-title ${titleFontSize}`}>{title}</h5>
                    <p className="text-muted">{subtitle}</p>
                </div>
                <div className="col-auto ms-auto my-auto d-none d-md-flex">
                    <div ref={navigationPrevRef} style={{ cursor: 'pointer' }}>
                        <img className="img-fluid me-2" src="/assets/img/icons/icon-prev.png" alt="Prev" />
                    </div>
                    <div ref={navigationNextRef} style={{ cursor: 'pointer' }}>
                        <img className="img-fluid" src="/assets/img/icons/icon-next.png" alt="Next" />
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col">
                    <Swiper
                        spaceBetween={10}
                        grabCursor={true}
                        slidesPerView={1.2}
                        navigation={{
                            prevEl: navigationPrevRef.current,
                            nextEl: navigationNextRef.current,
                        }}
                        onBeforeInit={(swiper) => {
                            swiper.params.navigation.prevEl = navigationPrevRef.current;
                            swiper.params.navigation.nextEl = navigationNextRef.current;
                        }}
                        breakpoints={{
                            // when window width is >= 414px
                            375: {
                                slidesPerView: 1.2,
                            },
                            // when window width is >= 768px
                            768: {
                                slidesPerView: 2.2,
                            },
                            1200: {
                                slidesPerView: slidesPerView,
                            },
                        }}
                        style={{ padding: '20px 0 20px 0' }}
                    >
                        {children}
                    </Swiper>
                </div>
            </div>
        </section>
    );
};

export default SectionSlider;
