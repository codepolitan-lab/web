import styles from './Navbar.module.scss';
import { useState } from 'react';
import Link from 'next/link';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; // Import the FontAwesomeIcon component
import { faBars, faSearch, faTimes } from "@fortawesome/free-solid-svg-icons"; // import the icons you need
import MegamenuItem from './MegamenuItem/MegamenuItem';
import { useRouter } from 'next/router';
import SearchModal from '../SearchModal/SearchModal';
import { courseMenus, exploreMenus, partnershipMenus, programMenus } from './navmenus';

const Navbar = () => {
  const router = useRouter();
  const [icon, setIcon] = useState(false);

  return (
    <>
      <nav id="Navbar" className="navbar navbar-expand-xl navbar-light bg-white shadow fixed-top px-lg-5">
        <div className="container px-lg-5">
          <Link href="/">
            <a className="navbar-brand">
              <img height="45" width="auto" src="/assets/img/codepolitan-logo.png" alt="Codepolitan" />
            </a>
          </Link>
          <button
            onClick={() => setIcon((icon) => !icon)}
            className="navbar-toggler shadow-none border-0"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            {icon ? icon && <FontAwesomeIcon icon={faTimes} /> : <FontAwesomeIcon icon={faBars} />}
          </button>
          <div className={`collapse navbar-collapse ${styles.navbar_collapse}`} id="navbarSupportedContent">
            <ul className="navbar-nav mb-2 mb-lg-0 ms-auto">
              <li className="nav-item input-group" style={{ minWidth: '15em' }}>
                <span className="input-group-text bg-white"><FontAwesomeIcon className='text-muted' icon={faSearch} /></span>
                <span className="form-control shadow-none border-start-0 text-muted text-start" role="button" data-bs-toggle="modal" data-bs-target="#searchModal">Cari disini...</span>
              </li>
              <li className="nav-item dropdown mx-2">
                <a className={`nav-link dropdown-toggle ${styles.nav_link}`} href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Courses
                </a>
                <ul className={`dropdown-menu ${styles.dropdown_menu_courses} dropdown-content shadow`} aria-labelledby="navbarDropdown">
                  <div className="row mx-3 my-2">
                    <div className="col-lg-4">
                      <div className="row">
                        <div className="col-12 mb-3">
                          <li>
                            <div className="py-1 border-bottom small fw-bold">Kategori</div>
                          </li>
                        </div>
                        {courseMenus.categories?.map((menu, index) => (
                          <div className="col-12 p-0 mb-2" key={index}>
                            <li>
                              <MegamenuItem
                                title={menu.title}
                                link={`/library?type=${menu.queryType}`}
                                isActive={router.pathname === '/library' && router.query.type === menu.queryType}
                                courses
                              />
                            </li>
                          </div>
                        ))}
                      </div>
                    </div>
                    <div className="col-lg-4">
                      <div className="row">
                        <div className="col-12 mb-3">
                          <li>
                            <div className="py-1 border-bottom small fw-bold">Teknologi Populer</div>
                          </li>
                        </div>
                        {courseMenus.popularTechnologies?.map((menu, index) => (
                          <div className="col-12 p-0 mb-2" key={index}>
                            <li>
                              <MegamenuItem
                                title={menu.title}
                                link={`/library?type=${menu.queryType}`}
                                isActive={router.pathname === '/library' && router.query.type === menu.queryType}
                                courses
                              />
                            </li>
                          </div>
                        ))}
                      </div>
                    </div>
                    <div className="col-lg-4 border-lg-start">
                      <div className="row">
                        {courseMenus.others?.map((menu, index) => (
                          <div className="col-12 p-0" key={index}>
                            <li>
                              <MegamenuItem
                                icon={menu.icon}
                                title={menu.title}
                                subtitle={menu.subtitle}
                                link={menu.link}
                                isActive={router.pathname === menu.link}
                              />
                            </li>
                          </div>
                        ))}
                      </div>
                    </div>
                  </div>
                </ul>
              </li>
              <li className="nav-item dropdown mx-2">
                <a className={`nav-link dropdown-toggle ${styles.nav_link}`} href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Explore
                </a>
                <ul className={`dropdown-menu ${styles.dropdown_menu} dropdown-content shadow`} style={{ left: '-120%' }} aria-labelledby="navbarDropdown">
                  <div className="row m-3">
                    {exploreMenus?.map((menu, index) => (
                      <div className="col-12 col-lg-6 p-0" key={index}>
                        <li>
                          <MegamenuItem
                            icon={menu.icon}
                            title={menu.title}
                            subtitle={menu.subtitle}
                            link={menu.link}
                            isActive={router.pathname === menu.link}
                          />
                        </li>
                      </div>
                    ))}
                  </div>
                </ul>
              </li>
              <li className="nav-item dropdown mx-2 program">
                <a className={`nav-link dropdown-toggle ${styles.nav_link}`} href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Program
                </a>
                <ul className={`dropdown-menu ${styles.dropdown_menu} dropdown-content shadow`} style={{ minWidth: '300px' }} aria-labelledby="navbarDropdown">
                  <div className="row m-3">
                    {programMenus?.map((menu, index) => (
                      <div className="col-12 p-0" key={index}>
                        <li>
                          <MegamenuItem
                            icon={menu.icon}
                            title={menu.title}
                            subtitle={menu.subtitle}
                            link={menu.link}
                            isActive={router.pathname === menu.link}
                            targetBlank={menu.targetBlank}
                          />
                        </li>
                      </div>
                    ))}
                  </div>
                </ul>
              </li>
              <li className="nav-item dropdown mx-2 partnership">
                <a className={`nav-link dropdown-toggle ${styles.nav_link}`} href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Partnership
                </a>
                <ul className={`dropdown-menu ${styles.dropdown_menu} dropdown-content shadow`} style={{ minWidth: '300px' }} aria-labelledby="navbarDropdown">
                  <div className="row m-3">
                    {partnershipMenus?.map((menu, index) => (
                      <div className="col-12 p-0" key={index}>
                        <li>
                          <MegamenuItem
                            icon={menu.icon}
                            title={menu.title}
                            subtitle={menu.subtitle}
                            link={menu.link}
                            isActive={router.pathname === menu.link}
                            targetBlank={menu.targetBlank}
                          />
                        </li>
                      </div>
                    ))}
                  </div>
                </ul>
              </li>
            </ul>
            <div className="d-grid gap-2 ms-lg-2 d-lg-block">
              <a className={`${styles.nav_link} btn btn-light px-3 mx-2 m-lg-1`} href="https://dashboard.codepolitan.com/login/">Login</a>
              <a className={`${styles.nav_link} btn btn-primary px-3 mx-2 m-lg-1`} href="https://dashboard.codepolitan.com/register/">Register</a>
            </div>
          </div>
        </div>
      </nav>
      <SearchModal />
    </>
  );
};

export default Navbar;
