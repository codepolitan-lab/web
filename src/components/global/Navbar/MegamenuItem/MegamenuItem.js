import { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Link from 'next/link';

const MegamenuItem = ({ link, icon, title, subtitle, newBadge, isActive, courses, targetBlank }) => {
    const [hover, setHover] = useState(false);

    return (
        <>
            {
                targetBlank
                    ? (
                        <a href={link} target="_blank" rel="noopener noreferrer" className="dropdown-item px-0 py-2" onMouseEnter={() => setHover(!hover)} onMouseLeave={() => setHover(!hover)}>
                            <div className="d-flex align-items-start">
                                {icon && (
                                    <div className="my-auto ps-2">
                                        <div className="rounded" style={{ backgroundColor: hover ? '#14a7a0' : '#eee' }}>
                                            <FontAwesomeIcon className={hover ? "text-light p-2" : "text-primary p-2"} size="lg" fixedWidth icon={icon} />
                                        </div>
                                    </div>
                                )}
                                <div className="ms-2 my-auto">
                                    <h6 className={isActive ? "text-primary" : undefined} style={{ fontWeight: courses ? 'normal' : 'bold', fontSize: 'smaller', marginBottom: 0 }}>
                                        {title}
                                        {newBadge && (<span className="badge rounded-pill bg-danger ms-1">NEW</span>)}
                                    </h6>

                                    <small className="text-muted" style={{ fontSize: 'small' }}>{subtitle}</small>
                                </div>
                            </div>
                        </a>
                    )
                    : (
                        <Link href={link}>
                            <a className="dropdown-item px-0 py-2" onMouseEnter={() => setHover(!hover)} onMouseLeave={() => setHover(!hover)}>
                                <div className="d-flex align-items-start">
                                    {icon && (
                                        <div className="my-auto ps-2">
                                            <div className="rounded" style={{ backgroundColor: hover ? '#14a7a0' : '#eee' }}>
                                                <FontAwesomeIcon className={hover ? "text-light p-2" : "text-primary p-2"} size="lg" fixedWidth icon={icon} />
                                            </div>
                                        </div>
                                    )}
                                    <div className="ms-2 my-auto">
                                        <h6 className={isActive ? "text-primary" : undefined} style={{ fontWeight: courses ? 'normal' : 'bold', fontSize: 'smaller', marginBottom: 0 }}>
                                            {title}
                                            {newBadge && (<span className="badge rounded-pill bg-danger ms-1">NEW</span>)}
                                        </h6>

                                        <small className="text-muted" style={{ fontSize: 'small' }}>{subtitle}</small>
                                    </div>
                                </div>
                            </a>
                        </Link>
                    )
            }
        </>
    );
};

export default MegamenuItem;
