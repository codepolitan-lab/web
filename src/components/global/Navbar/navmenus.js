import { faDiscord } from "@fortawesome/free-brands-svg-icons";
import { faBookmark } from "@fortawesome/free-regular-svg-icons";
import { faBoltLightning, faBuilding, faCalendar, faCalendarAlt, faChalkboardTeacher, faCircleCheck, faComments, faGlasses, faLayerGroup, faMap, faMicrophone, faNewspaper, faRoad, faTag, faTrophy, faUsers } from "@fortawesome/free-solid-svg-icons";

const courseMenus = {
    categories: [
        {
            title: 'Web Development',
            queryType: 'web'
        },
        {
            title: 'Mobile Development',
            queryType: 'mobile'
        },
        {
            title: 'Studi Kasus',
            queryType: 'project'
        },
        {
            title: 'Fundamental',
            queryType: 'fundamental'
        },
        {
            title: 'Pemula',
            queryType: 'beginner'
        },
        {
            title: 'Menengah',
            queryType: 'intermediate'
        },
        {
            title: 'Framework',
            queryType: 'framework'
        },
        {
            title: 'Front End',
            queryType: 'frontend'
        },
        {
            title: 'Back End',
            queryType: 'backend'
        },
    ],
    popularTechnologies: [
        {
            title: 'Laravel',
            queryType: 'laravel'
        },
        {
            title: 'PHP',
            queryType: 'php'
        },
        {
            title: 'Kotlin',
            queryType: 'kotlin'
        },
        {
            title: 'Android',
            queryType: 'android'
        },
        {
            title: 'Javascript',
            queryType: 'javascript'
        },
        {
            title: 'Wordpress',
            queryType: 'wordpress'
        },
        {
            title: 'Database',
            queryType: 'database'
        },
        {
            title: 'Java',
            queryType: 'java'
        },
        {
            title: 'Go-Lang',
            queryType: 'golang'
        }
    ],
    others: [
        {
            title: 'Kelas Terbaru',
            subtitle: 'Kelas Online Terbaru',
            icon: faCircleCheck,
            link: '/library'
        },
        {
            title: 'Kelas Gratis',
            subtitle: 'Kelas Online Gratis',
            icon: faTag,
            link: '/library?type=free'
        },
        // {
        //     title: 'Flash Sale',
        //     subtitle: 'Kelas Online Promo',
        //     icon: faBoltLightning,
        //     link: '/flashsale'
        // },
        {
            title: 'Popular',
            subtitle: 'Kelas Online Popular',
            icon: faBookmark,
            link: '/library?type=popular'
        },
        {
            title: 'Mentor',
            subtitle: 'Daftar Mentor Kelas',
            icon: faChalkboardTeacher,
            link: '/mentor'
        },
        {
            title: 'Roadmap',
            subtitle: 'Daftar Roadmap Belajar',
            icon: faMap,
            link: '/roadmap'
        },
    ]
};

const exploreMenus = [
    // {
    //     title: 'Karier',
    //     subtitle: 'Temukan Kariermu',
    //     icon: faRoad,
    //     link: '/karier'
    // },
    {
        title: 'Tutorial & Artikel',
        subtitle: 'Temukan Artikel Menarik',
        icon: faNewspaper,
        link: '/tutorials'
    },
    // {
    //     title: 'Podcast',
    //     subtitle: 'Podcast seputar pemrograman',
    //     icon: faMicrophone,
    //     link: '/podcasts'
    // },
    {
        title: 'Events',
        subtitle: 'Ikuti Berbagai Event',
        icon: faCalendar,
        link: '/events'
    },
    // {
    //     title: 'Event',
    //     subtitle: 'Temukan Event Menarik',
    //     icon: faCalendarAlt,
    //     link: '/events-and-info'
    // },
    // {
    //     title: 'Beasiswa',
    //     subtitle: 'Program Beasiswa',
    //     icon: faGraduationCap,
    //     link: '/beasiswa'
    // },
    // {
    //     title: 'Discord',
    //     subtitle: 'Komunitas Discord',
    //     icon: faDiscord,
    //     link: '/program/join-discord'
    // },
    {
        title: 'Discussions',
        subtitle: 'Diskusi antar Programmer',
        icon: faComments,
        link: '/forum'
    },
    // {
    //     title: 'Exercise',
    //     subtitle: 'Asah Pengtahuanmu di Exercise',
    //     icon: faEdit,
    //     link: '/exercise'
    // },
    {
        title: 'Leaderboard',
        subtitle: 'Ranking siswa Codepolitan',
        icon: faTrophy,
        link: '/leaderboard'
    },
];

const programMenus = [
    {
        title: 'KelasFullstack.id',
        subtitle: 'Belajar FullStack Web Development from A to Z',
        icon: faLayerGroup,
        link: 'https://kelasfullstack.id/',
        targetBlank: true
    },
    {
        title: 'PZN EXPERT',
        subtitle: 'Kuasai Skill Coding ala Startup Unicorn',
        icon: faGlasses,
        link: 'https://pzn.codepolitan.com/',
        targetBlank: true
    },
];

const partnershipMenus = [
    {
        title: 'For Company',
        subtitle: 'Solusi tepat untuk perusahaan',
        icon: faBuilding,
        link: '/for-company'
    },
    {
        title: 'For Mentor',
        subtitle: 'Peluang penghasilan untuk mentor',
        icon: faChalkboardTeacher,
        link: '/for-mentor'
    },
];

export { courseMenus, exploreMenus, programMenus, partnershipMenus };