import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; // Import the FontAwesomeIcon component
import { faFacebook, faInstagram, faLinkedin, faTiktok, faTwitter, faYoutube } from "@fortawesome/free-brands-svg-icons"; // import the icons you need
import Link from "next/link";

const Footer = () => {
    return (
        <footer id="Footer" style={{ borderTop: '1px solid #ddd' }}>
            <div className="container px-5 mt-5">
                <div className="row justify-content-between">
                    <div className="col-12 col-lg-4">
                        <img height="45" width="auto" className="mb-4" src="/assets/img/codepolitan-logo.png" alt="Codepolitan" />
                        <h6 className="fw-bolder">ADDRESS</h6>
                        <p className="text-muted">Komp. Permata, Jl. Permata Raya I No.3, Tanimulya, Ngamprah, Kabupaten Bandung Barat, Jawa Barat - 40552</p>
                    </div>
                    <div className="col-12 col-lg-2">
                        <h6 className="fw-bolder">JOIN US</h6>
                        <nav className="nav flex-column my-3">
                            <Link href="/about">
                                <a className="link">About Us</a>
                            </Link>
                            <Link href="/how-to-learn">
                                <a className="link">How to Learn</a>
                            </Link>
                            <Link href="/faq">
                                <a className="link">FAQ</a>
                            </Link>
                            <Link href="/terms-and-condition">
                                <a className="link">Terms & Conditions</a>
                            </Link>
                            <Link href="/privacy-policy">
                                <a className="link">Privacy Policy</a>
                            </Link>
                        </nav>
                    </div>
                    <div className="col-12 col-lg-2">
                        <h6 className="fw-bolder">COMMUNITY</h6>
                        <nav className="nav flex-column my-3">
                            <Link href="/events">
                                <a className="link">Events</a>
                            </Link>
                            <Link href="/tutorials">
                                <a className="link">Blog</a>
                            </Link>
                            <Link href='/leaderboard'>
                                <a className="link">Leaderboard</a>
                            </Link>
                            <Link href="/forum">
                                <a className="link">Questions</a>
                            </Link>
                            <Link href="/hall-of-fame">
                                <a className="link">Hall of Fame</a>
                            </Link>
                        </nav>
                    </div>
                    <div className="col-12 col-lg-2">
                        <h6 className="fw-bolder">PARTNERSHIP</h6>
                        <nav className="nav flex-column my-3">
                            <Link href="/for-company">
                                <a className="link">For Company</a>
                            </Link>
                            <Link href="/for-mentor">
                                <a className="link">For Mentor</a>
                            </Link>
                        </nav>
                    </div>
                </div>
            </div>
            <hr style={{ height: '1px', color: '#ccc', backgroundColor: '#ccc' }} />
            <div className="container px-5">
                <div className="row">
                    <div className=" text-center text-md-start">
                        <p className="text-muted"><small>&copy; {(new Date().getFullYear())} CodePolitan. All rights reserved. Made with ❤️ in Indonesia.</small></p>
                    </div>
                    <div className=" text-center text-md-end mb-2">
                        <a className="footer-socmed-link" title="Tiktok" href="https://www.tiktok.com/@codepolitan.com">
                            <FontAwesomeIcon icon={faTiktok} />
                        </a>
                        <a className="footer-socmed-link" title="Facebook" href="https://www.facebook.com/codepolitan/">
                            <FontAwesomeIcon icon={faFacebook} />
                        </a>
                        <a className="footer-socmed-link" title="Instagram" href="https://www.instagram.com/codepolitan/">
                            <FontAwesomeIcon icon={faInstagram} />
                        </a>
                        <a className="footer-socmed-link" title="Twitter" href="https://twitter.com/codepolitan">
                            <FontAwesomeIcon icon={faTwitter} />
                        </a>
                        <a className="footer-socmed-link" title="Linkedin" href="https://www.linkedin.com/company/codepolitan">
                            <FontAwesomeIcon icon={faLinkedin} />
                        </a>
                        <a className="footer-socmed-link" title="Youtube" href="https://www.youtube.com/c/codepolitan">
                            <FontAwesomeIcon icon={faYoutube} />
                        </a>
                    </div>
                </div>
            </div>
        </footer>
    );
};

export default Footer;
