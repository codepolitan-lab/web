import styles from './Banner.module.scss';

const Banner = ({ background, title, subtitle }) => {
    return (
        <section className={styles.banner} style={{ background: `linear-gradient(0deg, rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('${background}')` }}>
            <div className="container p-4 p-lg-5">
                <div className="row my-5">
                    <div className="col-md-8 text-white">
                        <h1>{title}</h1>
                        <p>{subtitle}</p>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default Banner;
