import { faBook, faMap, faUsers } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const CardSidebar = ({ thumbnail, totalRoadmap, totalCourse, totalRoadmapStudent, totalCourseStudent }) => {
    return (
        <div className="card border-0 shadow-sm rounded-3">
            <div className="card-header border-0">
                <img height={200} width={200} className="rounded-circle border border-white border-5 d-block mx-auto m-3" style={{ objectFit: 'cover' }} src={thumbnail || "/assets/img/placeholder.jpg"} alt="" />
            </div>
            <div className="card-body p-0">
                <div className="accordion accordion-flush" id="accordionFlushCardMentorSidebar">
                    <div className="accordion-item">
                        <h6 className="accordion-header" id="flush-headingOne">
                            <button className="accordion-button text-muted" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="true" aria-controls="flush-collapseOne">
                                Total Materi dan Siswa
                            </button>
                        </h6>
                        <div id="flush-collapseOne" className="accordion-collapse collapse show" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushCardMentorSidebar">
                            <ul className="list-group list-group-flush">
                                <li className="list-group-item text-muted"><FontAwesomeIcon className="text-primary fa-fw" icon={faMap} /> {totalRoadmap || '0'} Roadmap</li>
                                <li className="list-group-item text-muted"><FontAwesomeIcon className="text-primary fa-fw" icon={faBook} /> {totalCourse || '0'} Kelas</li>
                                <li className="list-group-item text-muted"><FontAwesomeIcon className="text-primary fa-fw" icon={faUsers} /> {totalCourseStudent + totalRoadmapStudent || '0'} Siswa</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default CardSidebar;
