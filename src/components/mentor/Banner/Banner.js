
const Banner = ({ brandName, description }) => {
    return (
        <section className="section">
            <div className="row mb-lg-5 pb-lg-5">
                <div className="col">
                    <h1 className="section-title h3">{brandName || 'Belum ada Brand'}</h1>
                    <p className="text-muted">{description || 'Belum ada deskripsi'}</p>
                </div>
            </div>
        </section>
    );
};

export default Banner;
