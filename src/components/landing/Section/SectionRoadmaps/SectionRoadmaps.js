import { useState } from 'react';
import Link from 'next/link';
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import CardRoadmap from "../../../global/Cards/CardRoadmap/CardRoadmap";

const SectionRoadmaps = ({ roadmaps }) => {
    const [searchValue, setSearchValue] = useState("");
    const [limit, setLimit] = useState(4);

    return (
        <section className="section bg-light" id="roadmaps">
            <div className="container p-4 p-lg-5">
                <div className="row justify-content-between">
                    <div className="col-auto">
                        <h2 className="section-title">Pilih Roadmap</h2>
                    </div>
                    <div className="col-12 col-md-auto">
                        <div className="input-group">
                            <span className="input-group-text bg-white text-muted"><FontAwesomeIcon icon={faSearch} /></span>
                            <input onChange={(e) => setSearchValue(e.target.value)} type="search" className="form-control border-start-0 shadow-none" placeholder="Cari roadmap..." />
                        </div>
                    </div>
                </div>
                <div className="row mt-3">
                    {roadmaps.filter((value) => {
                        if (searchValue === "") {
                            return value;
                        } else if (value.name.toLowerCase().includes(searchValue.toLowerCase())) {
                            return value;
                        }
                    }).map((roadmap, index) => {
                        return (
                            <div className="col-md-6 col-xl-3 p-2" key={index}>
                                <Link href={`/roadmap/${roadmap.slug}`}>
                                    <a className="link">
                                        <CardRoadmap
                                            thumbnail={roadmap.image}
                                            icon={roadmap.small_icon}
                                            author={roadmap.author}
                                            title={roadmap.name}
                                            level={roadmap.level}
                                            totalCourse={roadmap.total_course}
                                            totalStudent={roadmap.total_student}
                                            rate={roadmap.rate}
                                        />
                                    </a>
                                </Link>
                            </div>
                        );
                    })
                        .reverse()
                        .slice(0, limit)}
                </div>
                {roadmaps.filter((value) => {
                    if (searchValue === "") {
                        return value;
                    } else if (value.name.toLowerCase().includes(searchValue.toLowerCase())) {
                        return value;
                    }
                }).length > limit && (
                        <div className="row mt-5">
                            <div className="col text-center">
                                <button onClick={() => setLimit(limit + 4)} className="btn btn-outline-secondary rounded-3">Roadmap Lainnya</button>
                            </div>
                        </div>
                    )}
            </div>
        </section>
    );
};

export default SectionRoadmaps;