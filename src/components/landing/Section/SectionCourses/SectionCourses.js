import { useState } from 'react';
import Link from 'next/link';
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import CardCourse from '../../../global/Cards/CardCourse/CardCourse';

const SectionCourses = ({ courses }) => {
    const [searchValue, setSearchValue] = useState("");
    const [limit, setLimit] = useState(4);

    return (
        <section className="section" id="courses">
            <div className="container p-4 p-lg-5">
                <div className="row justify-content-between">
                    <div className="col-auto">
                        <h2 className="section-title">Pilih Kelas</h2>
                    </div>
                    <div className="col-12 col-md-auto">
                        <div className="input-group">
                            <span className="input-group-text bg-white text-muted"><FontAwesomeIcon icon={faSearch} /></span>
                            <input onChange={(e) => setSearchValue(e.target.value)} type="search" className="form-control border-start-0 shadow-none" placeholder="Cari kelas..." />
                        </div>
                    </div>
                </div>
                <div className="row mt-3">
                    {courses.filter((value) => {
                        if (searchValue === "") {
                            return value;
                        } else if (value.title.toLowerCase().includes(searchValue.toLowerCase())) {
                            return value;
                        }
                    }).map((course, index) => {
                        return (
                            <div className="col-md-6 col-xl-3 p-2" key={index}>
                                <Link href={`/course/intro/${course.slug}`}>
                                    <a className="link">
                                        <CardCourse
                                            isFlashsale
                                            thumbnail={course.thumbnail}
                                            author={course.author}
                                            title={course.title}
                                            level={course.level}
                                            totalStudents={course.total_student}
                                            totalModules={course.total_module}
                                            totalTimes={course.total_time}
                                            totalRating={course.total_rating}
                                            totalFeedback={course.total_feedback}
                                            normalBuyPrice={course.buy?.normal_price || course.normal_price}
                                            retailBuyPrice={course.buy?.retail_price || course.retail_price}
                                            normalRentPrice={course.rent?.normal_price}
                                            retailRentPrice={course.rent?.retail_price}
                                        />
                                    </a>
                                </Link>
                            </div>
                        );
                    })
                        .reverse()
                        .slice(0, limit)
                    }
                </div>
                {courses.filter((value) => {
                    if (searchValue === "") {
                        return value;
                    } else if (value.title.toLowerCase().includes(searchValue.toLowerCase())) {
                        return value;
                    }
                }).length > limit && (
                        <div className="row mt-5">
                            <div className="col text-center">
                                <button onClick={() => setLimit(limit + 4)} className="btn btn-outline-secondary rounded-3">Kelas Lainnya</button>
                            </div>
                        </div>
                    )}
            </div>
        </section>
    );
};

export default SectionCourses;