import { useRef } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
// import Swiper core and required modules
import SwiperCore, { Navigation } from "swiper";
import CardMentor from '../../../home/Cards/CardMentor/CardMentor';
import Link from "next/link";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronCircleLeft, faChevronCircleRight } from "@fortawesome/free-solid-svg-icons";

// install Swiper modules
SwiperCore.use([Navigation]);

const SectionMentor = ({ brand, authors, slide }) => {
    const navigationPrevRef = useRef(null);
    const navigationNextRef = useRef(null);

    return (
        <section className="section" id="Swiper">
            <div className="container p-4 p-lg-5">
                <div className="row justify-content-between">
                    <div className="col-auto">
                        <h2 className="section-title">Tim Pengajar Profesional Kami</h2>
                        <p className="text-muted">Belajar cepat dan benar langsung dari para pakar dan praktisi profesional<i> expert</i> pilihan Codepolitan</p>
                    </div>
                    <div className="col-auto d-none d-lg-block my-auto">
                        <span className="me-2" ref={navigationPrevRef} role="button">
                            <FontAwesomeIcon size="2x" className="text-primary" icon={faChevronCircleLeft} />
                        </span>
                        <span ref={navigationNextRef} role="button">
                            <FontAwesomeIcon size="2x" className="text-primary" icon={faChevronCircleRight} />
                        </span>
                    </div>
                </div>
                <div className="row mt-3 justify-content-between">
                    <div className="col">
                        <Swiper
                            spaceBetween={20}
                            grabCursor={true}
                            slidesPerView={slide?.desktop ?? 5}
                            navigation={{
                                prevEl: navigationPrevRef.current,
                                nextEl: navigationNextRef.current,
                            }}
                            onBeforeInit={(swiper) => {
                                swiper.params.navigation.prevEl = navigationPrevRef.current;
                                swiper.params.navigation.nextEl = navigationNextRef.current;
                            }}
                            breakpoints={{
                                // when window width is >= 414px
                                375: {
                                    slidesPerView: 1.3,
                                },
                                // when window width is >= 768px
                                768: {
                                    slidesPerView: slide?.tab ?? 3.3,
                                },
                                1200: {
                                    slidesPerView: slide?.desktop ?? 5,
                                },
                            }}
                            style={{ padding: 0 }}
                        >
                            {authors.filter(mentor => {
                                if (brand.brand_name === 'Programmer Zaman Now') {
                                    return mentor.name !== 'Toni Haryanto' && mentor.name !== 'Sandhika Galih' && mentor.name !== 'Angga Risky'
                                }
                                return mentor.name !== 'Angga Risky';

                            }).map((mentor, index) => {
                                return (
                                    <SwiperSlide style={{ height: 'auto' }} key={index}>
                                        <CardMentor
                                            thumbnail={mentor.avatar}
                                            name={mentor.name}
                                            jobs={mentor.jobs}
                                            description={mentor.short_description}
                                            brandImage={mentor.brand_image}
                                            fromBrand={true}
                                        />
                                    </SwiperSlide>
                                );
                            })}
                        </Swiper>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default SectionMentor;

