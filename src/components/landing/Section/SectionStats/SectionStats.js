const SectionStats = ({ data }) => {
    return (
        <section style={{ backgroundColor: '#edf0f3' }}>
            <div className="container p-4 px-lg-0 py-lg-5">
                <div className="row text-muted text-center">
                    <div className="col-6 col-lg-3 mb-4 mb-lg-0">
                        <div className="display-6 text-primary d-inline me-2">{data.kelas_online || 0}</div>
                        <span>Kelas Online</span>
                    </div>
                    <div className="col-6 col-lg-3">
                        <div className="display-6 text-primary d-inline me-2">{data.materi_belajar || 0}+</div>
                        <span>Materi Belajar</span>
                    </div>
                    <div className="col-6 col-lg-3">
                        <div className="display-6 text-primary d-inline me-2">{data.jam_belajar || 0}+</div>
                        <span>Jam Belajar</span>
                    </div>
                    <div className="col-6 col-lg-3">
                        <div className="display-6 text-primary d-inline me-2">{data.jalur_belajar || 0}</div>
                        <span>Jalur Belajar</span>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default SectionStats