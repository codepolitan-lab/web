import { faAward, faClock, faCode, faComments, faFlask, faLaptopCode, faSortAlphaDown, faTasks } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styles from './SectionWhyStudy.module.scss';

const SectionWhyStudyInCodepolitan = () => {
    return (
        <section className="py-2">
            <div className="container p-4 p-lg-5">
                <h2 className="section-title text-center text-muted pb-5">Mengapa Belajar Di Codepolitan</h2>
                <div className="row mt-5 text-center text-muted">
                    <div className="col-6 col-md-3 mb-5">
                        <span className={styles.hexagon}>
                            <FontAwesomeIcon size="2x" icon={faSortAlphaDown} />
                        </span>
                        <p className="mt-5">Alur Belajar Terarah</p>
                    </div>
                    <div className="col-6 col-md-3 mb-5">
                        <span className={styles.hexagon}>
                            <FontAwesomeIcon size="2x" icon={faClock} />
                        </span>
                        <p className="mt-5">Waktu Belajar Fleksibel</p>
                    </div>
                    <div className="col-6 col-md-3 mb-5">
                        <span className={styles.hexagon}>
                            <FontAwesomeIcon size="2x" icon={faTasks} />
                        </span>
                        <p className="mt-5">Progres Belajar Terukur</p>
                    </div>
                    <div className="col-6 col-md-3 mb-5">
                        <span className={styles.hexagon}>
                            <FontAwesomeIcon size="2x" icon={faAward} />
                        </span>
                        <p className="mt-5">Sertifikat Online</p>
                    </div>
                    <div className="col-6 col-md-3 mb-5">
                        <span className={styles.hexagon}>
                            <FontAwesomeIcon size="2x" icon={faComments} />
                        </span>
                        <p className="mt-5">Forum Diskusi Tanya Jawab</p>
                    </div>
                    <div className="col-6 col-md-3 mb-5">
                        <span className={styles.hexagon}>
                            <FontAwesomeIcon size="2x" icon={faFlask} />
                        </span>
                        <p className="mt-5">Best Practice Dari Pakar</p>
                    </div>
                    <div className="col-6 col-md-3 mb-5">
                        <span className={styles.hexagon}>
                            <FontAwesomeIcon size="2x" icon={faLaptopCode} />
                        </span>
                        <p className="mt-5">Contoh Project</p>
                    </div>
                    <div className="col-6 col-md-3 mb-5">
                        <span className={styles.hexagon}>
                            <FontAwesomeIcon size="2x" icon={faCode} />
                        </span>
                        <p className="mt-5">Source Code</p>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default SectionWhyStudyInCodepolitan