const SectionCTA = () => {
    return (
        <section className="section">
            <div className="container p-4 p-lg-5">
                <div className="row">
                    <div className="col">
                        <div className="card bg-codepolitan border-0" style={{ borderRadius: '25px' }}>
                            <div className="card-body bg-codepolitan text-center" style={{ borderRadius: '25px' }}>
                                <div className="py-4">
                                    <h2 className="section-title text-white mx-auto mb-3 h1 w-75">Tunggu apa lagi? Ayo, belajar bersama kami Sekarang Juga!</h2>
                                    <div className="row justify-content-center">
                                        <div className="col-auto">
                                            <a style={{ borderRadius: '15px', background: '#FF8B99' }} className="btn btn-lg text-white px-4 my-3" href="#roadmaps"><strong>Pilih Roadmap</strong></a>
                                        </div>
                                        <div className="col-auto">
                                            <a style={{ borderRadius: '15px' }} className="btn btn-outline-light btn-lg px-5 my-3" href="#courses"><strong>Pilih Kelas</strong></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default SectionCTA;
