const SectionVIdeo = ({ author, video }) => {
    return (
        <section className="section" style={{ backgroundColor: '#f5f7fa' }}>
            <div className="container p-4 p-lg-5">
                <div className="row align-items-center">
                    <div className="col-lg-6 mt-3">
                        <h2 className="section-title mb-4">Cukup luangkan waktu 2 jam sehari. Bisa?</h2>
                        <p className="text-muted">Dengan bergabung dalam program ini, selama 1 semester saya akan membimbingmu belajar coding secara online melalui rangkaian kelas online yang telah saya susun.</p>
                        <p className="text-muted">Kamu cukup meluangkan waktu minimal 2 jam per hari, untuk mempelajari materi dan praktek. Percayalah, dalam waktu 6 bulan kamu akan merasakan manfaatnya.</p>
                        <p className="text-muted">Ayo akselerasi kecepatan belajar codingmu. Saya tunggu di dalam kelas.</p>
                        <p><strong>{author || "Belum ada author"}</strong></p>
                    </div>
                    <div className="col-lg-6 order-first order-lg-last my-5">
                        <div className="ratio ratio-4x3">
                            <iframe style={{ borderRadius: '10px' }} src={"https://www.youtube.com/embed/" + video} title="YouTube video player" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen />
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default SectionVIdeo