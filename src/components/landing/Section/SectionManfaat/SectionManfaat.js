import { faCheckCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const SectionManfaat = () => {
    return (
        <section className="section bg-light">
            <div className="container p-4 p-lg-5">
                <div className="row justify-content-between">
                    <div className="col-lg-5 mb-5 mb-lg-auto">
                        <img className="img-fluid d-block mx-auto" src="/assets/img/partner/manfaat.png" alt="Manfaat Gabung Program" />
                    </div>
                    <div className="col-lg-6 my-auto">
                        <h2 className="section-title">Manfaat Gabung Program</h2>
                        <p className="text-muted">Banyak peluang yang akan terbuka jika kamu memiliki skill coding</p>
                        <div className="d-flex align-items-start mb-3">
                            <FontAwesomeIcon fixedWidth size="lg" className="text-primary mt-2 me-3" icon={faCheckCircle} />
                            <span className="text-muted">Bekerja pada sebuah perusahaan atau startup digital menjadi seorang programmer profesional</span>
                        </div>
                        <div className="d-flex align-items-start mb-3">
                            <FontAwesomeIcon fixedWidth size="lg" className="text-primary mt-2 me-3" icon={faCheckCircle} />
                            <span className="text-muted">Menjadi seorang freelancer yang bekerja secara remote dari rumah yang bekerja untuk klien baik dalam maupun luar negeri.</span>
                        </div>
                        <div className="d-flex align-items-start mb-3">
                            <FontAwesomeIcon fixedWidth size="lg" className="text-primary mt-2 me-3" icon={faCheckCircle} />
                            <span className="text-muted">Membangun bisnismu sendiri dengan mengembangkan aplikasi atau web yang kamu ciptakan.</span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default SectionManfaat;