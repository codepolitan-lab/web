

const SectionLangkahSukses = () => {
    return (
        <section className="section">
            <div className="container p-5">
                <div className="row text-center">
                    <div className="col">
                        <h2 className="section-title">Langkah Sukses Menjadi Mahir</h2>
                        <p className="text-muted">Belajar lebih terarah dan hasil maksimal dengan 4 langkah berikut:</p>
                    </div>
                </div>
                <div className="row mt-4">
                    <div className="col px-5">
                        <div className="d-none d-md-block">
                            <img className="img-fluid d-block mx-auto" src="/assets/img/program/cara-belajar.png" alt="Cara Belajar" />
                        </div>
                        <div className="d-md-none d-block mx-auto">
                            <img className="img-fluid" src="/assets/img/program/cara-belajar-small.png" alt="Cara Belajar" />
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default SectionLangkahSukses;
