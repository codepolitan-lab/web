import styles from './Hero.module.scss';

const Hero = ({ brandLogo, brandName, headline, description, image }) => {
    return (
        <section className={`position-relative ${styles.hero}`}>
            <div className="container p-4 p-lg-5 ">
                <div className="row text-muted">
                    <div className="col-lg-7 order-last order-lg-first mt-4 mt-lg-0">
                        <div className="d-flex align-items-center">
                            <img height="50" className="rounded-circle" src={brandLogo || "/assets/img/placeholder.jpg"} alt="Brand image" />
                            <h5 className="text-primary pt-2 ms-2 text-center text-lg-start">{brandName || 'Belum ada nama'}</h5>
                        </div>
                        <h1 className="my-4">{headline || 'Belum ada judul'}</h1>
                        <p className="mb-4 lead">{description || 'Belum ada deskripsi'}</p>
                        <a href="#caraBelajar" className="btn btn-outline-secondary btn-rounded px-5 py-2">Pelajari Selengkapnya</a>
                    </div>
                    <div className="col-lg-5 d-flex align-items-center justify-content-center justify-content-lg-end">
                        <img src={image || "/assets/img/placeholder.jpg"} className="w-75 rounded-3" alt="Brand image" style={{ zIndex: '1' }} />
                        <div className={`d-none d-lg-block ${styles['background-strip']}`}></div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Hero;
