import styles from './SectionIntro.module.scss';

const SectionIntro = () => {
    return (
        <section className="section bg-light" style={{ paddingTop: '80px' }}>
            <div className="container p-4 p-lg-5">
                <div className="row d-none d-lg-block">
                    <div className={`col ${styles.wrapper}`}>
                        <img className={styles.card_image} src="/assets/img/about/img-about-2.png" alt="Tentang Codepolitan" />
                        <div className={`${styles.card} card bg-codepolitan border-0 float-end`}>
                            <div className="card-body">
                                <div className={styles.text_wrapper}>
                                    <p className="text-white">Codepolitan adalah platform belajar pemrograman secara online dengan berbahasa Indonesia yang dikembangkan khusus untuk membantu generasi muda belajar dan berlatih pemrograman agar kompetitif di era Industri 4.0.</p>
                                    <p className="text-white">Dalam 5 tahun, Codepolitan berkembang menjadi platform belajar pemrograman berbahasa Indonesia dan menjadi sumber terpercaya bagi generasi muda Indonesia.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row d-lg-none">
                    <div className="col mt-5">
                        <p className="text-muted">Codepolitan adalah platform belajar pemrograman secara online dengan berbahasa Indonesia yang dikembangkan khusus untuk membantu generasi muda belajar dan berlatih pemrograman agar kompetitif di era Industri 4.0.</p>
                        <p className="text-muted">Dalam 5 tahun, Codepolitan berkembang menjadi platform belajar pemrograman berbahasa Indonesia dan menjadi sumber terpercaya bagi generasi muda Indonesia.</p>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default SectionIntro;
