import styles from './SectionCounter.module.scss';
import CountUp from 'react-countup';

const SectionCounter = ({ data }) => {
    return (
        <section className={`section ${styles.background}`}>
            <div className="container p-4 p-lg-5">
                <div className="row justify-content-around text-white text-center mt-5 mt-lg-3">
                    <div className="col-4 col-lg-2">
                        <strong>
                            <CountUp className="h1" delay={3} end={data.total_course} />
                            <p>Kelas Online</p>
                        </strong>
                    </div>
                    <div className="col-4 col-lg-2">
                        <strong>
                            <CountUp className="h1" delay={3} end={data.total_study} />
                            <p>Materi Belajar</p>
                        </strong>
                    </div>
                    <div className="col-4 col-lg-2">
                        <strong>
                            <CountUp className="h1" delay={3} end={data.total_hours} />
                            <p>Jam Pelajaran</p>
                        </strong>
                    </div>
                    <div className="col-4 col-lg-2">
                        <strong>
                            <CountUp className="h1" delay={3} end={data.total_path} />
                            <p>Roadmap</p>
                        </strong>
                    </div>
                    <div className="col-4 col-lg-2">
                        <strong>
                            <CountUp className="h1" delay={3} end={data.total_tutorial} />
                            <p>Tutorial</p>
                        </strong>
                    </div>
                    <div className="col-4 col-lg-2">
                        <strong>
                            <CountUp className="h1" delay={3} end={data.total_webinar} />
                            <p>Webinar</p>
                        </strong>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default SectionCounter;
