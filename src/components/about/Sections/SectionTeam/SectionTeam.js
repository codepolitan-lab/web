import { useState } from 'react';
import CardTeam from '../../Cards/CardTeam/CardTeam';

const SectionTeam = () => {
    const [teams] = useState([
        {
            thumbnail: '/assets/img/about/kresna.png',
            name: 'Kresna Galuh',
            role: 'CEO & Founder',
            instagram_link: 'https://instagram.com/kresnagaluh.id',
            facebook_link: 'https://facebook.com/kresnagaluh',
            linkedin_link: 'https://linkedin.com/in/kresnagaluh'
        },
        {
            thumbnail: '/assets/img/about/singgih.png',
            name: 'M Singgih Z.A.',
            role: 'CMO & Co-Founder',
            instagram_link: 'https://instagram.com/hirokakaoshi/',
            facebook_link: 'https://facebook.com/hirokakaoshi/',
            linkedin_link: 'https://linkedin.com/in/msinggihza/'
        },
        {
            thumbnail: '/assets/img/about/oriza.png',
            name: 'Ahmad Oriza',
            role: 'CTO & Co-Founder',
            instagram_link: 'https://instagram.com/oriza.ahmad/',
            facebook_link: 'https://facebook.com/ahmad.oriza/',
            linkedin_link: 'https://linkedin.com/in/ahmadoriza/'
        },
        {
            thumbnail: '/assets/img/about/palupi.png',
            name: 'Hadyan Palupi',
            role: 'COO & Co-Founder',
            instagram_link: 'https://instagram.com/hadyan.palupi/',
            facebook_link: 'https://facebook.com/hadyan.palupi/',
            linkedin_link: 'https://linkedin.com/in/hadyan-palupi-974445163'
        },
        {
            thumbnail: '/assets/img/about/tony.png',
            name: 'Toni Haryanto',
            role: 'CPO & Co-Founder',
            instagram_link: 'https://www.instagram.com/yllumi/',
            facebook_link: 'https://www.facebook.com/toharyan/',
            linkedin_link: 'https://www.linkedin.com/in/toniharyanto/'
        },
        {
            thumbnail: '/assets/img/about/yudha.png',
            name: 'Yudha Pangesti D. Syailendra',
            role: 'Graphic Designer',
            instagram_link: 'https://www.instagram.com/yudhapangesti/',
            facebook_link: 'https://www.facebook.com/yudhapangesti/',
            linkedin_link: ''
        },
        {
            thumbnail: '/assets/img/about/Finlandia Wibisana.png',
            name: 'Finlandia Wibisana',
            role: 'Video Editor',
            instagram_link: 'https://www.instagram.com/finlandia_w',
            facebook_link: '',
            linkedin_link: 'https://www.linkedin.com/in/finlandia-wibisana'
        },
        {
            thumbnail: '/assets/img/about/novan.png',
            name: 'Novan Junaedi',
            role: 'Frontend Developer',
            instagram_link: 'https://www.instagram.com/novanjunaedi',
            facebook_link: 'https://www.facebook.com/novanjunaedi98',
            linkedin_link: 'https://www.linkedin.com/in/novanjunaedi'
        },
        {
            thumbnail: '/assets/img/about/Aldiansyah Ibrahim.png',
            name: 'Aldiansyah Ibrahim',
            role: 'Frontend Developer',
            instagram_link: 'https://www.instagram.com/aldianbaim',
            facebook_link: 'https://www.facebook.com/aldiansyahibr',
            linkedin_link: ''
        },
        {
            thumbnail: '/assets/img/about/Badar Abdi Mulya.png',
            name: 'Badar Abdi Mulya',
            role: 'UI/UX Designer',
            instagram_link: 'https://instagram.com/badartjan',
            facebook_link: '',
            linkedin_link: 'https://linkedin.com/in/badar-abdi-mulya-168866124'
        },
        {
            thumbnail: '',
            name: 'Fauzan Abdul Hakim',
            role: 'Social Media Officer',
            instagram_link: 'https://instagram.com/fauzanahak',
            facebook_link: 'https://facebook.com/fauzanaha',
            linkedin_link: 'https://linkedin.com/in/fauzanaha'
        },
        {
            thumbnail: '/assets/img/about/Chandra Trio Pamungkas.png',
            name: 'Chandra Trio Pamungkas',
            role: 'Digital Marketer',
            instagram_link: 'https://instagram.com/chandratrio',
            facebook_link: '',
            linkedin_link: 'https://linkedin.com/in/chandra-trio-pamungkas-089153157'
        },
        {
            thumbnail: '/assets/img/about/Jajat Sudrajat.png',
            name: 'Jajat Sudrajat',
            role: 'Office Support',
            instagram_link: 'https://instagram.com/',
            facebook_link: 'https://facebook.com',
            linkedin_link: 'https://linkedin.com/in/'
        },
    ]);
    return (
        <section className="section">
            <div className="container p-4 p-lg-5">
                <div className="row mb-5">
                    <div className="col text-center">
                        <h3 className="section-title">Team CodePolitan</h3>
                    </div>
                </div>
                <div className="row justify-content-center">
                    {teams.map((team, index) => {
                        return (
                            <CardTeam
                                key={index}
                                thumbnail={team.thumbnail}
                                name={team.name}
                                role={team.role}
                                instagramLink={team.instagram_link}
                                facebookLink={team.facebook_link}
                                linkedinLink={team.linkedin_link}
                            />
                        );
                    })}
                </div>
            </div>
        </section>
    );
};

export default SectionTeam;
