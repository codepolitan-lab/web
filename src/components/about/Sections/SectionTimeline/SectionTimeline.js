import styles from './SectionTimeline.module.scss';

const SectionTimeline = () => {
    return (
        <section className="section bg-light">
            <div className="container p-4 p-lg-5">
                <div className="row mb-5">
                    <div className="col">
                        <h1 className="section-title h3">Sejarah Singkat CodePolitan</h1>
                    </div>
                </div>

                <div className="d-flex align-items-start border-start border-bottom border-5 py-3">
                    <span className={styles.circle + ' me-2 my-auto'} />
                    <div className="row w-100">
                        <div className="col-3 my-auto">
                            <div className="card bg-codepolitan border-0 shadow-sm">
                                <div className="card-body text-center text-white">
                                    <div className={styles.year}>
                                        <span className="d-none d-lg-block">2016</span>
                                        <span className="d-block d-lg-none">20 <br /> 16</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-9">
                            <div className="card border-0 shadow-sm h-100">
                                <div className="card-body text-muted">
                                    <span>Kami memulai segala sesuatunya dengan Portal Media Online yang membahas tentang pemrograman dan teknologi</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div className="d-flex align-items-start border-end border-bottom border-5 py-3">
                    <div className="row">
                        <div className="col-3 my-auto">
                            <div className="card bg-codepolitan border-0 shadow-sm">
                                <div className="card-body text-center text-white">
                                    <div className={styles.year}>
                                        <span className="d-none d-lg-block">2017</span>
                                        <span className="d-block d-lg-none">20 <br /> 17</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-9 order-first">
                            <div className="card border-0 shadow-sm h-100">
                                <div className="card-body text-muted">
                                    <span>Lahirnya Program Coding Bootcamp, Codepolitan Developer School. Menyelenggarakan salah satu event terbesar programmer di Indonesia, Indonesia Developer Summit.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <span className={styles.circle + ' ms-2 my-auto'} />
                </div>


                <div className="d-flex align-items-start border-start border-bottom border-5 py-3">
                    <span className={styles.circle + ' me-2 my-auto'} />
                    <div className="row w-100">
                        <div className="col-3 my-auto">
                            <div className="card bg-codepolitan border-0 shadow-sm">
                                <div className="card-body text-center text-white">
                                    <div className={styles.year}>
                                        <span className="d-none d-lg-block">2018</span>
                                        <span className="d-block d-lg-none">20 <br /> 18</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-9">
                            <div className="card border-0 shadow-sm h-100">
                                <div className="card-body text-muted">
                                    <span>Diluncurkan program Kelas Online Premium Membership, dengan 1000 purchase di bulan pertama peluncuran.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div className="d-flex align-items-start border-end border-bottom border-5 py-3">
                    <div className="row">
                        <div className="col-3 my-auto">
                            <div className="card bg-codepolitan border-0 shadow-sm">
                                <div className="card-body text-center text-white">
                                    <div className={styles.year}>
                                        <span className="d-none d-lg-block">2019</span>
                                        <span className="d-block d-lg-none">20 <br /> 19</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-9 order-first">
                            <div className="card border-0 shadow-sm h-100">
                                <div className="card-body text-muted">
                                    <span>Peluncuran program Kelas Online Lifetime dan menjadi salah satu perusahaan terdepan dan konsisten yang memberikan pembelajaran dalam bidang programming di Indonesia.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <span className={styles.circle + ' ms-2 my-auto'} />
                </div>


                <div className="d-flex align-items-start border-start border-bottom border-5 py-3">
                    <span className={styles.circle + ' me-2 my-auto'} />
                    <div className="row w-100">
                        <div className="col-3 my-auto">
                            <div className="card bg-codepolitan border-0 shadow-sm">
                                <div className="card-body text-center text-white">
                                    <div className={styles.year}>
                                        <span className="d-none d-lg-block">2020</span>
                                        <span className="d-block d-lg-none">20 <br /> 20</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-9">
                            <div className="card border-0 shadow-sm h-100">
                                <div className="card-body text-muted">
                                    <span>Menjadi salah satu perusahaan yang bertahan di masa Pandemi dengan inovasi dalam program Kelas Online. Peluncuran program Kampus Coding dan Geekmentor.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="d-flex align-items-start border-end border-5 py-3">
                    <div className="row">
                        <div className="col-3 my-auto">
                            <div className="card bg-codepolitan border-0 shadow-sm">
                                <div className="card-body text-center text-white">
                                    <div className={styles.year}>
                                        <span className="d-none d-lg-block">2021</span>
                                        <span className="d-block d-lg-none">20 <br /> 21</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-9 order-first">
                            <div className="card border-0 shadow-sm h-100">
                                <div className="card-body text-muted">
                                    <span>Menyelenggarakan event challenge nasional bersama perusahaan Internasional Alibaba Cloud dan Here Maps, kampanye IT ke berbagai desa dalam program Desa Digital, inisiasi partnership dengan mentor eksternal.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <span className={styles.circle + ' ms-2 my-auto'} />
                </div>

                <div className="d-flex align-items-end">
                    <hr style={{ width: '60%', marginTop: 0, marginBottom: 0 }} className={styles.dotted_line + ' ms-auto'} />
                </div>
                <div className="d-flex align-items-start">
                    <hr style={{ width: '49%', marginTop: 0, borderColor: 'transparent' }} className={styles.dotted_line} />
                    <span className={styles.circle + ' my-1'} />
                </div>
                <div className="postion-relative">
                    <div className="position-absolute start-50 translate-middle-x">
                        <div className="card bg-codepolitan text-center border-white border-3">
                            <div className="card-body px-5 text-white">
                                <p className="h3">Today</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    );
};

export default SectionTimeline;
