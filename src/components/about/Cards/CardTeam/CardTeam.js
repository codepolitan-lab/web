import { faFacebook, faInstagram, faLinkedin } from '@fortawesome/free-brands-svg-icons';
import { faGlobe } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const CardTeam = ({ thumbnail, name, role, instagramLink, facebookLink, linkedinLink }) => {
    return (
        <div className="col-md-6 col-lg-4 col-xl-3 mb-4">
            <img className="img-fluid w-100" loading='lazy' src={thumbnail || "/assets/img/placeholder.jpg"} alt={name} />
            <div className="my-3 text-center">
                <h5 className="section-title">{name}</h5>
                <p className="text-muted mb-2">{role}</p>
                {facebookLink && (
                    <a className="link text-primary mx-1" href={facebookLink} target="_blank" rel="noopener noreferrer" title="Facebook">
                        <FontAwesomeIcon size="lg" icon={faFacebook} />
                    </a>
                )}
                {instagramLink && (
                    <a className="link text-primary mx-1" href={instagramLink} target="_blank" rel="noopener noreferrer" title="Instagram">
                        <FontAwesomeIcon size="lg" icon={faInstagram} />
                    </a>
                )}
                {linkedinLink && (
                    <a className="link text-primary mx-1" href={linkedinLink} target="_blank" rel="noopener noreferrer" title="LinkedIn">
                        <FontAwesomeIcon size="lg" icon={faLinkedin} />
                    </a>
                )}
            </div>
        </div>
    );
};

export default CardTeam;
