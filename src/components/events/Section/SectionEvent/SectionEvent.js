import { useState } from 'react';
import CardEvent from '../../Cards/CardEvent/CardEvent';
import CardSkeleton from '../../Cards/CardSkeleton/CardSkeleton'
import Link from 'next/link';

const SectionEvent = ({ data, loading }) => {
    const [limit, setLimit] = useState(12);

    return (
        <>
            <h3 className="section-title">Events</h3>
            <p className="text-muted mb-4">Perluas wawasan dan tingkatkan kemampuanmu dengan mengikuti event CODEPOLITAN</p>
            <div className="row">
                {loading && [...Array(3)].map((item, index) => {
                    return (
                        <div key={index} className="col-lg-3 p-3">
                            <CardSkeleton />
                        </div>
                    );
                })}
                {!loading && data?.map((event, index) => {
                    return (
                        <div key={index} className="col-lg-3 p-3">
                            <Link href={`/events/${event.slug}`} className="link">
                                <a className='link'>
                                    <CardEvent
                                        type={event.webinar_type}
                                        thumbnail={event.featured_image}
                                        title={event.title}
                                        schedule={event.scheduled_at}
                                        location={event.location}
                                    />
                                </a>
                            </Link>
                        </div>
                    );
                }).slice(0, limit)}
            </div>
            {data?.length > limit && (
                <div className="text-center mt-3">
                    <button onClick={() => setLimit(limit + 8)} type="button" className="btn btn-outline-secondary">Load more</button>
                </div>
            )}
        </>
    );
};

export default SectionEvent;
