import { faArrowRotateRight, faVideo } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Link from 'next/link'
import React, { useState } from 'react'
import { formatDate } from '../../../../utils/helper'

const SectionWebinarReplay = ({ data, loading }) => {
    const [limit, setLimit] = useState(4)

    return (
        <section className="text-muted">
            <h4>Replay Webinar</h4>
            <p>Tingkatkan kemampuanmu dengan ikut webinar</p>
            <div className="row">
                {
                    !loading && data?.map((item, index) => {
                        return (
                            <div key={index} className="col-lg-3 mb-4">
                                <Link href={`/webinar/${item.slug}`}>
                                    <a className="link">
                                        <div className="card border-0 shadow">
                                            <img src={item.featured_image || '/assets/img/placeholder.jpg'} className="card-img-top" style={{ height: '15em', objectFit: 'cover' }} alt="" />
                                            <div className="card-body">
                                                <h6 className="card-title">{item.title}</h6>
                                                <small>{formatDate(item.scheduled_at)}</small>
                                            </div>
                                        </div>
                                    </a>
                                </Link>
                            </div>
                        )
                    }).slice(0, limit)
                }
            </div>
            {
                data?.length > limit && (
                    <div className="text-center">
                        <button onClick={() => setLimit(limit + 4)} className="btn btn-sm btn-outline-secondary">
                            <FontAwesomeIcon icon={faArrowRotateRight} className="me-1" />
                            <span>Lihat Lainnya</span>
                        </button>
                    </div>
                )
            }
        </section>
    )
}

export default SectionWebinarReplay