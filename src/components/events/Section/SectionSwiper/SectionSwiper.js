import { useRef } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronCircleLeft, faChevronCircleRight } from "@fortawesome/free-solid-svg-icons";
import { Swiper } from "swiper/react";

// import Swiper core and required modules
import SwiperCore, { Autoplay, Navigation } from "swiper";

// install Swiper modules
SwiperCore.use([Autoplay, Navigation]);

const SectionSwiper = ({ title, children, description }) => {
    const navigationPrevRef = useRef(null);
    const navigationNextRef = useRef(null);

    return (
        <section className="py-3">
            <div className="row justify-content-between mb-2">
                <div className="col-auto text-muted">
                    <h4>{title}</h4>
                    {description && (<p>{description}</p>)}
                </div>
                <div className="col-auto d-none d-lg-block">
                    <span className="me-2" ref={navigationPrevRef} role="button">
                        <FontAwesomeIcon size="2x" className="text-primary" icon={faChevronCircleLeft} />
                    </span>
                    <span ref={navigationNextRef} role="button">
                        <FontAwesomeIcon size="2x" className="text-primary" icon={faChevronCircleRight} />
                    </span>
                </div>
            </div>
            <div className="row">
                <div className="col">
                    <Swiper
                        spaceBetween={20}
                        grabCursor={true}
                        slidesPerView={4.2}
                        autoplay
                        navigation={{
                            prevEl: navigationPrevRef.current,
                            nextEl: navigationNextRef.current,
                        }}
                        onBeforeInit={(swiper) => {
                            swiper.params.navigation.prevEl = navigationPrevRef.current;
                            swiper.params.navigation.nextEl = navigationNextRef.current;
                        }}
                        breakpoints={{
                            // when window width is >= 414px
                            300: {
                                slidesPerView: 1.3,
                            },
                            // when window width is >= 768px
                            768: {
                                slidesPerView: 2.5,
                            },
                            1200: {
                                slidesPerView: 4.2,
                            },
                        }}
                        style={{ padding: '15px 5px 15px 5px' }}
                    >
                        {children}
                    </Swiper>
                </div>
            </div>
        </section>
    );
};

export default SectionSwiper;
