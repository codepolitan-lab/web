
// Data static for partners company
export const onlineCourse = [
    {
        name: 'Here',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/here_THgKeV7WUz.webp'
    },
    {
        name: 'Alibaba Cloud',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/Alibaba_kBCmKi7O7.webp'
    },
    {
        name: 'Intel',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/Intel_T_cVZHo81M_t.webp'
    },
    {
        name: 'Dicoding',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/dicoding-header-logo_-yUghUNkx7.webp'
    },
    {
        name: 'IBM',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/IBM_GXVYl92inl905.webp'
    },
    {
        name: 'XL',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/xl_t5bLnDwJn.webp'
    },
]

export const challenges = [
    {
        name: 'Here',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/here_THgKeV7WUz.webp'
    },
    {
        name: 'Alibaba Cloud',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/Alibaba_kBCmKi7O7.webp'
    },
    {
        name: 'Pixel',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/pixel_YtPl8tvzu.webp'
    },
    {
        name: 'Refactory',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/Refactory_VO89Fn5BX.webp'
    },
]

export const advertorial = [
    {
        name: 'Lenovo',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/Lenovo_co1GDjBpxV.webp'
    },
    {
        name: 'Intel',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/Intel_T_cVZHo81M_t.webp'
    },
    {
        name: 'XL',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/xl_t5bLnDwJn.webp'
    },
    {
        name: 'Pixel',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/pixel_YtPl8tvzu.webp'
    },
    {
        name: 'Hactive8',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/hacktiv8_Y3L2HUj4K4X.webp'
    },
    {
        name: 'Ajita',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/ajita_Za3H1pSLYI.webp'
    },
    {
        name: 'DBS',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/dbs_JaOD33dHI.png'
    },
    {
        name: 'BeKraf',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/bekraf_xdWemP6CP.png'
    },
]

export const seminar = [
    {
        name: 'Kalibrr',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/kalibrr_vUntvF04P.png'
    },
    {
        name: 'Indosat',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/indosat_hsSFKihNc.png'
    },
    {
        name: 'UMN',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/umn_wFVQI1UOZ.png'
    },
    {
        name: 'Mitra',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/mitra_y4yIE9vxn.png'
    },
    {
        name: 'Indoomtech',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/indocomtech_VqiLUYbrz.png'
    },
    {
        name: 'Video',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/mitra_y4yIE9vxn.png'
    },
    {
        name: 'Cicil',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/cicil_jtK2IVHRw.png'
    },
    {
        name: 'IBM',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/IBM_GXVYl92inl905.webp'
    },
    {
        name: 'Hactive8',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/hacktiv8_Y3L2HUj4K4X.webp'
    },
    {
        name: 'Kudo',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/Kudo_oGHxpphEw.webp'
    },
    {
        name: 'Refactory',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/Refactory_VO89Fn5BX.webp'
    },
]
export const skillDefinedTraining = [
    {
        name: 'Digital Talent',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/digital-talent_BDZO919u0W8h.png'
    },
    {
        name: 'Kominfo',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/kominfo_quHEyJih5Z.png'
    },
    {
        name: 'Kemnaker',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/KEMNAKER_MGfr7cHn61.webp'
    },
    {
        name: 'Lintasarta',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/lintasarta_YB5bvACBHX.png'
    },
    {
        name: 'Geometri',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/geometri_bbjZ-4j3k.png'
    },
    {
        name: 'SOS',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/sos_GnYEK_bAz8.png'
    },
    {
        name: 'Kemenkeu',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/kemenkeu_p9YQYkw-AI.webp'
    },
]
export const onDemandTraining = [
    {
        name: 'Mitra',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/mitra_y4yIE9vxn.png'
    },
    {
        name: 'Pixel',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/pixel_YtPl8tvzu.webp'
    },
    {
        name: ' ',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/'
    },
    {
        name: ' ',
        thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/'
    },
]

// Data static for modal data
export const onlineCourseModal = {
    title: "Online Course Development",
    image: "/assets/img/forcompany/online-course-modal.png",
    partner: "Here",
    description: "Online Course Development adalah solusi yang sangat tepat bila Anda membutuhkan strategi adopsi teknologi ke pasar pembelajar IT. Dengan memperkenalkan produk teknologi Anda dalam bentuk online course, Anda dapat mempromosikan sekaligus mengedukasi audiens CodePolitan untuk lebih mudah lagi dalam menggunakan produk Anda.",
    logoPartner: "https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/here_THgKeV7WUz.webp"
}
export const challengesModal = {
    title: "Challenges",
    image: "/assets/img/forcompany/challenges.png",
    partner: "Alibaba Cloud",
    description: "Program Challenges merupakan salah satu solusi untuk memperkenalkan atau meningkatkan produk bisnis anda, dengan adanya CodePolitan yang sudah memiliki 150k+ member aktif program ini terbukti sudah berhasil dalam mengadakan tantangan seperti Re-Cloud Challenges 2021 dan 2022 yang bisa mendapatkan 2k+ pendaftar.",
    logoPartner: "https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/Alibaba_kBCmKi7O7.webp"
}
export const seminarModal = {
    title: "Online/Offline Seminar",
    image: "/assets/img/forcompany/seminar.png",
    partner: "Kalibrr",
    description: "Program seminar yang diadakan khusus untuk membantu meningkatkan atau memperkenalkan produk bisnis anda melalui seminar live baik online maupun offline, dengan jangkauan relasi kami yang luas.",
    logoPartner: "https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/kalibrr_vUntvF04P.png"
}
export const advertorialModal = {
    title: "Advertorial",
    image: "/assets/img/forcompany/advertorial.png",
    partner: "Lenovo",
    description: "Melalui program Advertorial anda dapat menitipakan produk teknologi, kami dapat menjadi solusi untuk menjual produk anda di website dan semusa sosial media kami, supaya produk teknologi bisnis kamu dapat dikenal lebih cepat lagi melalui CodePolitan.",
    logoPartner: "https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/Lenovo_co1GDjBpxV.webp"
}
export const skillDefinedTrainingModal = {
    title: "Skill Defined Training",
    image: "/assets/img/forcompany/kominfo-modal.png",
    partner: "Kominfo",
    description: "Program Trainer Outsources ini anda dapat meningkatkan skill karyawan anda berdasarkan materi yang kami buat. Anda juga dapat memulainya di perusahaan anda atau di office CodePolitan.",
    logoPartner: "https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/kominfo_quHEyJih5Z.png"
}
export const onDemandTrainingModal = {
    title: "On-Demand Training",
    image: "/assets/img/forcompany/kominfo-modal.png",
    partner: "Pixel",
    description: "Anda dapat meningkatkan karyawan sesuai dengan request yang bisa anda minta dibidang kemampuan IT yang meliputi ( Fullstack Developer, Database Engineer, Android Developer, Frontend Developer, Backend Developer, UI/UX Developer, Cloud Computing, dan Cyber Security ).",
    logoPartner: "https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/pixel_YtPl8tvzu.webp"
}