import React, { useRef } from 'react'
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

// import Swiper core and required modules
import SwiperCore, { Pagination, Navigation } from "swiper";

// install Swiper modules
SwiperCore.use([Pagination, Navigation]);

const ModalTechAdoption = ({ data, dataSwiper }) => {

    const navigationPrevRef = useRef(null);
    const navigationNextRef = useRef(null);

    return (
        <div className="modal fade" id="modalTechAdoption" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div className="modal-dialog modal-xl">
                <div className="modal-content position-relative">
                    <div className="modal-header ms-auto">
                        <button type="button" className="btn btn-light btn-sm" data-bs-dismiss="modal">Close</button>
                    </div>
                    <div className="modal-body px-3 px-lg-5 pb-5 pt-1">
                        {/* <div className="row">
                            <div className="col-auto ms-auto d-none d-md-flex">
                                <div ref={navigationPrevRef} style={{ cursor: 'pointer' }}>
                                    <img className="img-fluid me-4" src="/assets/img/icons/icon-prev.png" loading="lazy" alt="Prev" />
                                </div>
                                <div ref={navigationNextRef} style={{ cursor: 'pointer' }}>
                                    <img className="img-fluid" src="/assets/img/icons/icon-next.png" loading="lazy" alt="Next" />
                                </div>
                            </div>
                        </div> */}
                        <div className="row mt-4">
                            <div className="col">
                                <Swiper
                                    spaceBetween={20}
                                    grabCursor={true}
                                    slidesPerView={1}
                                    navigation={{
                                        prevEl: navigationPrevRef.current,
                                        nextEl: navigationNextRef.current,
                                    }}
                                    onBeforeInit={(swiper) => {
                                        swiper.params.navigation.prevEl = navigationPrevRef.current;
                                        swiper.params.navigation.nextEl = navigationNextRef.current;
                                    }}
                                    breakpoints={{
                                        // when window width is >= 414px
                                        414: {
                                            slidesPerView: 1,
                                        },
                                        // when window width is >= 768px
                                        768: {
                                            slidesPerView: 1,
                                        },
                                        1024: {
                                            slidesPerView: 1,
                                        },
                                    }}
                                    style={{ paddingBottom: '10px' }}
                                >
                                    {dataSwiper.sort(item => item.partner == data.partner ? -1 : 1).map((item, index) => {
                                        return (
                                            <SwiperSlide key={index}>
                                                <div className="row text-muted">
                                                    <div className="col-lg-7 order-last order-md-first">
                                                        <h5>{item.title}</h5>
                                                        <p>Bersama : {item.partner}</p>
                                                        <p className="lh-lg mt-4">{item.description}</p>
                                                    </div>
                                                    <div className="col-lg-5 mb-4 mb-md-0">
                                                        <img src={item.image} className="w-100 img-fluid rounded img-modal" alt="" />
                                                        <div className="partner-logo p-1 d-none d-lg-flex">
                                                            {
                                                                item.partner == 'Kalibrr'
                                                                    ? (<img src={item.logoPartner} style={{ width: '30%', height: '80px' }} alt={item.partner} />)
                                                                    : (<img src={item.logoPartner} alt={item.partner} />)
                                                            }
                                                        </div>
                                                    </div>
                                                </div>
                                            </SwiperSlide>
                                        );
                                    })}
                                </Swiper>
                            </div>
                        </div>
                    </div>
                    <div className="modal-footer bg-codepolitan px-5 py-4 text-white d-block text-center text-lg-start">
                        <h3 className="fw-bolder float-lg-start">Tertarik Program Ini?</h3>
                        <a href="https://wa.me/6281382123092" target="_blank" className="btn btn-pink btn-rounded float-lg-end" rel="noopener noreferrer">Hubungi Kami Sekarang</a>                            {/* <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button> */}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ModalTechAdoption