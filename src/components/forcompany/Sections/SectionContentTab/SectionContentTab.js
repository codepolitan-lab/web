import { faArrowRight } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Image from "next/image";
import { Swiper, SwiperSlide } from "swiper/react";

const SectionContentTab = ({ partners, image, title, description, corporate }) => {
    return (
        <div className="row">
            <div className="col-lg-3">
                <img src={image} className="w-100 img-fluid" alt="" />
            </div>
            <div className="col-lg-8">
                <h5 className="mt-4 mt-md-0">{title}</h5>
                <p>{description}</p>
                <p className="lh-lg p-0">Beberapa mitra kami yang berhasil menggunakan strategi ini :</p>
                <div className="row justify-content-center justify-content-md-between">
                    <div className="col">
                        <Swiper
                            preventInteractionOnTransition={title === 'On-Demand Training' ? true : false}
                            autoplay
                            loop
                            spaceBetween={5}
                            slidesPerView={4}
                            breakpoints={{
                                // when window width is >= 414px
                                375: {
                                    slidesPerView: 3,
                                },
                                // when window width is >= 768px
                                768: {
                                    slidesPerView: 5,
                                },
                                1200: {
                                    slidesPerView: 4,
                                },
                            }}
                        >
                            {partners.map((partner, index) => {
                                return (
                                    <SwiperSlide key={index}>
                                        <div className="card bg-transparent border-0 mt-n2">
                                            <div className="card-body">
                                                <Image src={partner.thumbnail} alt={partner.name} layout="responsive" width={100} height={50} objectFit="contain" />
                                            </div>
                                        </div>
                                    </SwiperSlide>
                                );
                            })}
                        </Swiper>
                        <div className="mt-2">
                            <a type="button" className="fw-bold text-primary text-decoration-none" data-bs-toggle="modal" data-bs-target={corporate ? "#modalCorporateTraining" : "#modalTechAdoption"}>Lihat Studi Kasus <FontAwesomeIcon className="ms-1" icon={faArrowRight} /></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default SectionContentTab