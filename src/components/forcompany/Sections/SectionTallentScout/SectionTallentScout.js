import { faArrowRight } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import Link from "next/link"

const SectionTallentScout = () => {
    return (
        <section className="p-4 p-lg-5">
            <div className="container">
                <div className="row text-muted align-items-center">
                    <div className="col-lg-7 order-last order-md-first">
                        <h5>Tallent Scout</h5>
                        <p className="lh-lg mt-3 mb-5">Codepolitan dapat menjadi solusi untuk meningkatkan kemampuan tim perusahaan anda dibidang digital, program ini dapat diselenggarakan di kantor CodePolitan, di kantor anda atau secara online secara intensif maupun ekstensif. Anda dapat menyesuaikannya tergantung ketersediaan tim dan kebijakan anda.</p>
                        <Link href="/karier">
                            <a className="text-primary fw-bold text-decoration-none">Karier Tallent Scout <FontAwesomeIcon className="ms-2" icon={faArrowRight} /></a>
                        </Link>
                    </div>
                    <div className="col-lg-5 mb-4 mb-md-0">
                        <img src="/assets/img/forcompany/Group 2441.png" className="img-fluid w-100" alt="" />
                    </div>
                </div>
            </div>
        </section>
    )
}

export default SectionTallentScout