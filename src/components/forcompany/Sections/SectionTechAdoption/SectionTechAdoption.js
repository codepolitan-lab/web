import ModalTechAdoption from "../../Modal/ModalTechAdoption";
import SectionContentTab from "../SectionContentTab/SectionContentTab";
import { onlineCourse, advertorial, challenges, seminar, onlineCourseModal, challengesModal, seminarModal, advertorialModal } from "../../Data/Static";
import { useState } from "react";

const SectionTechAdoption = () => {

    let [modalData, setModalData] = useState(onlineCourseModal)
    const [dataSwiper] = useState([onlineCourseModal, challengesModal, seminarModal, advertorialModal])

    return (
        <section className="p-4 p-lg-5" id="mitraKami">
            <div className="container text-muted">
                <div className="text-center" style={{ marginBottom: '90px' }}>
                    <h3>Apa yang Kami Lakukan Bersama Mitra-mitra Kami</h3>
                    <p>Berikut ini beberapa solusi yang kami hadirkan untuk meningkatkan performa bisnismu</p>
                </div>
                <div className="row align-items-center pb-5">
                    <div className="col-lg-7 order-last order-md-first">
                        <h4>Tech Adoption</h4>
                        <p className="lh-lg mt-3">Platform CodePolitan memiliki traffic mencapai 17K kunjungan per hari atau mencapai 500K dalam sebulan yang spesifik belajar IT. Bila bisnis atau produk Anda memerlukan strategi penetrasi pasar untuk meningkatkan jumlah pengguna dan adopsi teknologi, kami memiliki beberapa solusi strategi yang dapat Anda terapkan untuk menjangkau audiens CodePolitan, diantaranya Online Course Development, Challenges, Online/Offline Seminar dan Advertorial.</p>
                    </div>
                    <div className="col-lg-5 mb-4 mb-md-0">
                        <img src="/assets/img/forcompany/Group 2424.png" className="img-fluid w-100" alt="" />
                    </div>
                </div>
                <ul className="nav nav-tabs" id="myTab" role="tablist">
                    <li className="nav-item" role="presentation">
                        <button onClick={() => setModalData(onlineCourseModal)} className="nav-link text-muted active" id="online-course-tab" data-bs-toggle="tab" data-bs-target="#online-course" type="button" role="tab" aria-controls="online-course" aria-selected="true">Online Course Development</button>
                    </li>
                    <li className="nav-item" role="presentation">
                        <button onClick={() => setModalData(challengesModal)} className="nav-link text-muted" id="challenges-tab" data-bs-toggle="tab" data-bs-target="#challenges" type="button" role="tab" aria-controls="challenges" aria-selected="false">Challenges</button>
                    </li>
                    <li className="nav-item" role="presentation">
                        <button onClick={() => setModalData(seminarModal)} className="nav-link text-muted" id="online-offline-seminar-tab" data-bs-toggle="tab" data-bs-target="#online-offline-seminar" type="button" role="tab" aria-controls="online-offline-seminar" aria-selected="false">Online/Offline Seminar</button>
                    </li>
                    <li className="nav-item" role="presentation">
                        <button onClick={() => setModalData(advertorialModal)} className="nav-link text-muted" id="advertorial-tab" data-bs-toggle="tab" data-bs-target="#advertorial" type="button" role="tab" aria-controls="advertorial" aria-selected="false">Advertorial</button>
                    </li>
                </ul>
                <div className="tab-content" id="myTabContent">
                    <div className="tab-pane mt-5 fade show active" id="online-course" role="tabpanel" aria-labelledby="online-course-tab">
                        <SectionContentTab
                            title="Online Course Development"
                            description="Online Course Development adalah solusi yang sangat tepat bila Anda membutuhkan strategi adopsi teknologi ke pasar pembelajar IT. Dengan memperkenalkan produk teknologi Anda dalam bentuk online course, Anda dapat mempromosikan sekaligus mengedukasi audiens CodePolitan untuk lebih mudah lagi dalam menggunakan produk Anda."
                            partners={onlineCourse}
                            image="/assets/img/forcompany/Group 2440.png"
                        />
                    </div>
                    <div className="tab-pane mt-5 fade" id="challenges" role="tabpanel" aria-labelledby="challenges-tab">
                        <SectionContentTab
                            title="Challenges"
                            description="Program Challenges merupakan salah satu solusi untuk memperkenalkan atau meningkatkan produk bisnis anda, dengan adanya CodePolitan yang sudah memiliki 150k+ member aktif program ini terbukti sudah berhasil dalam mengadakan tantangan seperti Re-Cloud Challenges 2021 dan 2022 yang bisa mendapatkan 2k+ pendaftar."
                            partners={challenges}
                            image="/assets/img/forcompany/Group 2442.png"
                        />
                    </div>
                    <div className="tab-pane mt-5 fade" id="online-offline-seminar" role="tabpanel" aria-labelledby="online-offline-seminar">
                        <SectionContentTab
                            title="Online/Offline Seminar"
                            description="Program seminar yang diadakan khusus untuk membantu meningkatkan atau memperkenalkan produk bisnis anda melalui seminar live baik online maupun offline, dengan jangkauan relasi kami yang luas."
                            partners={seminar}
                            image="/assets/img/forcompany/Group 2443.png"
                        />
                    </div>
                    <div className="tab-pane mt-5 fade" id="advertorial" role="tabpanel" aria-labelledby="advertorial">
                        <SectionContentTab
                            title="Advertorial"
                            description="Melalui program Advertorial anda dapat menitipakan produk teknologi, kami dapat menjadi solusi untuk menjual produk anda di website dan semusa sosial media kami, supaya produk teknologi bisnis kamu dapat dikenal lebih cepat lagi melalui CodePolitan."
                            partners={advertorial}
                            image="/assets/img/forcompany/Group 2444.png"
                        />
                    </div>
                </div>
            </div>

            {/* Modal */}
            <ModalTechAdoption
                data={modalData}
                dataSwiper={dataSwiper}
            />
        </section>
    )
}

export default SectionTechAdoption