import React, { useState } from 'react'
import SectionContentTab from '../SectionContentTab/SectionContentTab'
import { skillDefinedTraining, onDemandTraining, skillDefinedTrainingModal, onDemandTrainingModal } from '../../Data/Static'
import Modal from '../../Modal/ModalTechAdoption'
import ModalCorporateTraining from '../../Modal/ModalCorporateTraining'

const SectionCorporateTraining = () => {

    let [modalData, setModalData] = useState(skillDefinedTrainingModal)
    const [dataSwiper] = useState([skillDefinedTrainingModal, onDemandTrainingModal])

    return (
        <section className="bg-grey p-4 p-lg-5" id="mitraKami">
            <div className="container text-muted" id="corporateTraining">
                <div className="row align-items-center pb-5">
                    <div className="col-lg-7 order-last order-md-first">
                        <h4>Corporate Training</h4>
                        <p className="lh-lg mt-3">Codepolitan dapat menjadi solusi untuk meningkatkan kemampuan tim perusahaan anda dibidang digital, program ini dapat diselenggarakan di kantor CodePolitan, di kantor anda atau secara online secara intensif maupun ekstensif. Anda dapat menyesuaikannya tergantung ketersediaan tim dan kebijakan anda.</p>
                    </div>
                    <div className="col-lg-5 mb-4 mb-md-0">
                        <img src="/assets/img/forcompany/Group 2423.png" className="img-fluid w-100" alt="" />
                    </div>
                </div>
                <ul className="nav nav-tabs" id="corporateTab" role="tablist">
                    <li className="nav-item" role="presentation">
                        <button onClick={() => setModalData(skillDefinedTrainingModal)} className="nav-link text-muted active" id="skill-tab" data-bs-toggle="tab" data-bs-target="#skill" type="button" role="tab" aria-controls="skill" aria-selected="true">Skill Defined Training</button>
                    </li>
                    <li className="nav-item" role="presentation">
                        <button onClick={() => setModalData(onDemandTrainingModal)} className="nav-link text-muted" id="training-tab" data-bs-toggle="tab" data-bs-target="#training" type="button" role="tab" aria-controls="training" aria-selected="false">On-Demand Training</button>
                    </li>
                </ul>
                <div className="tab-content" id="corporateTabContent">
                    <div className="tab-pane mt-5 fade show active" id="skill" role="tabpanel" aria-labelledby="skill-tab">
                        <SectionContentTab
                            title="Skill Defined Training"
                            description="Program Trainer Outsources ini anda dapat meningkatkan skill karyawan anda berdasarkan materi yang kami buat. Anda juga dapat memulainya di perusahaan anda atau di office CodePolitan."
                            partners={skillDefinedTraining}
                            image="/assets/img/forcompany/skill-defined-training.png"
                            corporate={true}
                        />
                    </div>
                    <div className="tab-pane mt-5 fade" id="training" role="tabpanel" aria-labelledby="training-tab">
                        <SectionContentTab
                            title="On-Demand Training"
                            description="Anda dapat meningkatkan karyawan sesuai dengan request yang bisa anda minta dibidang kemampuan IT  yang meliputi ( Fullstack Developer, Database Engineer, Android Developer, Frontend Developer, Backend Developer, UI/UX Developer, Cloud Computing, dan Cyber Security )."
                            partners={onDemandTraining}
                            image="/assets/img/forcompany/Group 2445.png"
                            corporate={true}
                        />
                    </div>
                </div>
            </div>
            {/* Modal */}
            <ModalCorporateTraining
                data={modalData}
                dataSwiper={dataSwiper}
            />
        </section>
    )
}

export default SectionCorporateTraining