import styles from './Hero.module.scss';

const Hero = () => {
    return (
        <section className={`${styles.hero}`}>
            <div className="container p-4 p-lg-5">
                <div className="row">
                    <div className="col-lg-7 text-white text-center text-lg-start my-auto order-last order-md-first">
                        <h1 className={styles.hero_title}>Tingkatkan Kapasitas Bisnis Anda dengan Memanfaatkan Ekosistem IT dari CodePolitan</h1>
                        <p className="lead text-muted mt-4">CodePolitan memiliki platform belajar dan juga ratusan kelas online IT yang digunakan oleh lebih dari 150 ribu member. Pelajari bagaimana solusi kami dapat membantu meningkatkan performa bisnis anda.</p>
                        <a className="btn btn-primary btn-rounded py-3 px-4 my-3" href="#mitraKami">Pelajari Selengkapnya</a>
                    </div>
                    <div className="col-lg-5">
                        <img className="img-fluid" src="/assets/img/forcompany/hero-forcompany.png" alt="Hero" />
                    </div>
                </div>
            </div>
        </section>
    );
};

export default Hero;
