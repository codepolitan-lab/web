import { faLinkedin } from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const ButtonAddToProfileLinkedIn = ({ certificateId, certificateName, certificateUrl, organizationId, issueYear, issueMonth, expirationYear, expirationMonth }) => {
    return (
        <div>
            <a className="btn btn-outline-primary" href={`https://www.linkedin.com/profile/add?startTask=CERTIFICATION_NAME&name=${certificateName}&organizationId=${organizationId}&issueYear=${issueYear}&issueMonth=${issueMonth}&expirationYear=${expirationYear}&expirationMonth=${expirationMonth}&certUrl=${certificateUrl}&certId=${certificateId}`} target="_blank" rel="noopener noreferrer">
                <FontAwesomeIcon className="me-2" fixedWidth icon={faLinkedin} />
                Add to LinkedIn Profile
            </a>
        </div>
    );
};

export default ButtonAddToProfileLinkedIn;
