import { useRef } from 'react';
import { formatOnlyDate } from '../../../utils/helper';
import styles from './SectionCertificate.module.scss';
import ReactToPdf from "react-to-pdf"
import ButtonAddToProfileLinkedIn from '../ButtonAddToProfileLinkedIn';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faDownload } from '@fortawesome/free-solid-svg-icons';

const SectionCertificate = ({ certificate }) => {
    let expired = new Date(certificate.created_at);
    expired.setDate(expired.getDate() + 1095.75);

    let issueMonth = new Date(certificate.created_at).getMonth() + 1;
    let issueYear = new Date(certificate.created_at).getFullYear();

    let expiredMonth = expired.getMonth() + 1;
    let expiredYear = expired.getFullYear();

    // Ref and options for generate certificate
    const ref = useRef();
    const options = {
        orientation: 'landscape',
        unit: 'in',
    };

    return (
        <section className="section mb-5 mt-4 py-5">
            <div className="container-fluid">
                <div className="row justify-content-center position-relative" style={{ background: 'url(/assets/img/certificate-bg.png)', backgroundSize: 'cover', backgroundRepeat: 'no-repeat', backgroundPosition: 'center bottom', height: '400px', marginBottom: '-15%' }} >
                    <div className="col-lg-7 text-center text-white pt-5 mt-3" style={{ zIndex: '100' }}>
                        <h1>Sertifikat Digital CodePolitan</h1>
                        <p className="mt-4 px-1">Sertifikat ini adalah dokumen resmi dan valid dirilis oleh CodePolitan <br /> untuk member atas nama {certificate.name}</p>
                    </div>
                </div>
            </div>
            <div className="container" ref={ref}>
                <div className="row my-5">
                    <div className="col overflow-auto">
                        <div className={`card ${styles.card} border-0 shadow-sm`}>
                            <img src="/assets/img/bg-certificate-new.jpg" className="card-img" alt="Sertifikat" />
                            <div className="card-img-overlay d-flex flex-column">
                                <div className="row" style={{ marginTop: '265px' }}>
                                    <div className="col-7 ms-5 ps-5">
                                        <h1 className="text-primary h2">{certificate.name}</h1>
                                        <p>yang telah menyelesaikan kelas {certificate.cert_title} dalam program Kelas Online CodePolitan.</p>
                                    </div>
                                </div>
                                <div className="position-absolute" style={{ left: '100px', bottom: '230px' }}>
                                    <p className="fw-bolder">{formatOnlyDate(certificate.created_at)}</p>
                                </div>
                                <div className="position-absolute text-end" style={{ right: '80px', bottom: '50px' }}>
                                    <img width={100} height={100} src={`https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=https://www.codepolitan.com/c/${certificate.code}`} alt="" />
                                    <p className="fw-bolder mb-0 mt-3">Cek Validitas Sertifikat</p>
                                    <p className="mb-0">https://codepolitan.com/c/{certificate.code}</p>
                                    <p>Berlaku sampai {formatOnlyDate(expired)}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-auto my-2 d-none d-lg-block">
                        <ReactToPdf targetRef={ref} filename="certificate.pdf" options={options} x={-1} y={.5}>
                            {({ toPdf }) => (
                                <button onClick={toPdf} className="btn btn-primary"><FontAwesomeIcon fixedWidth className="me-2" icon={faDownload} /> Download</button>
                            )}
                        </ReactToPdf>
                    </div>
                    <div className="col-auto my-2">
                        <ButtonAddToProfileLinkedIn
                            certificateId={certificate.code}
                            certificateName={`Sertifikat Kelas ${certificate.cert_title}`}
                            certificateUrl={`https://codepolitan.com/c/${certificate.code}`}
                            organizationId="6463369"
                            issueYear={issueYear}
                            issueMonth={issueMonth}
                            expirationYear={expiredYear}
                            expirationMonth={expiredMonth}
                        />
                    </div>
                </div>
            </div>
        </section>
    );
};

export default SectionCertificate;