import { faPlay } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styles from './CardPodcast.module.scss';

const CardPodcast = ({ thumbnail, title, description, link }) => {
    return (
        <div className={`card card-rounded ${styles.card_podcast}`}>
            <div className="card-body p-4">
                <div className="row g-3">
                    <div className="col-lg-4">
                        <img style={{ height: '15em', objectFit: 'cover' }} src={thumbnail || "/assets/img/placeholder.jpg"} className="card-img card-rounded" alt={title} />
                    </div>
                    <div className="col my-3 my-lg-auto">
                        <h5 className={styles.title}>{title || 'Untitled'}</h5>
                        <p className={`text-muted ${styles.description}`}>{description || 'No description'}</p>
                        <a href={link} className="btn btn-outline-primary" target="_blank" rel="noopener noreferrer"><FontAwesomeIcon fixedWidth icon={faPlay} /> Play now</a>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default CardPodcast;