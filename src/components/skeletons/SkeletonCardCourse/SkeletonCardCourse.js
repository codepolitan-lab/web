import styles from './SkeletonCardCourse.module.scss';

const SkeletonCardCourse = () => {
    return (
        <div className={`${styles.card_course} card border-0 shadow-sm`}>
            <div className={`${styles.card_img_top} card-img-top`} />
            <div className="card-body">
                <span className="mb-3" style={{ width: '50%' }} />
                <span className="my-2" style={{ width: '100%' }} />
                <span className="d-block" style={{ width: '80%' }} />
                <div className="row my-2">
                    <div className="col-6">
                        <span className="d-inline-block" style={{ width: '70%' }} />
                        <br />
                        <span className="d-inline-block" style={{ width: '70%' }} />
                    </div>
                    <div className="col-6">
                        <span className="d-inline-block" style={{ width: '70%' }} />
                        <br />
                        <span className="d-inline-block" style={{ width: '70%' }} />
                    </div>
                </div>
                <span className="d-block" style={{ width: '65%' }} />
                <hr className="text-muted" />
                <div className={styles.rate_and_price}>
                    <div className="d-flex align-items-start">
                        <span className="d-inline-block" style={{ width: '55%' }} />
                        <span className="d-inline-block ms-auto" style={{ width: '30%' }} />
                    </div>
                </div>
            </div>
        </div>
    );
};

export default SkeletonCardCourse;
