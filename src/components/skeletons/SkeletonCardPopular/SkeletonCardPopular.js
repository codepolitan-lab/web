import { faBookmark } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const SkeletonCardPopular = () => {
    return (
        <div className="card border-0 shadow-sm" style={{ borderRadius: '15px' }}>
            <div className="card-body p-2">
                <div className="row text-muted">
                    <div className="col-4 my-auto">
                        <img className="img-fluid d-block ms-2 rounded" src={'/assets/img/placeholder.jpg'} alt="Loading..." />
                    </div>
                    <div className="col-8 my-auto">
                        <h5 style={{ fontSize: 'medium', marginBottom: 0 }}>Loading...</h5>
                        <span style={{ fontSize: 'small' }}>
                            <FontAwesomeIcon size="sm" icon={faBookmark} /> Loading...
                        </span>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default SkeletonCardPopular;
