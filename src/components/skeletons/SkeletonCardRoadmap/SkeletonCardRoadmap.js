import styles from './SkeletonCardRoadmap.module.scss';

const SkeletonCardRoadmap = () => {
    return (
        <div className={`${styles.card} card border-0 text-white`}>
            <div className={styles.card_img} />
            <div className={`${styles.overlay} card-img-overlay bg-secondary d-flex flex-column justify-content-end`}>
                <span className="my-1" style={{ width: '80%' }} />
            </div>
        </div>
    );
};

export default SkeletonCardRoadmap;
