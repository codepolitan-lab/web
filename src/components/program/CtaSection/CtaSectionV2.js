import styles from './CtaSectionV2.module.scss';

const CtaSectionV2 = () => {
    return (
        <section className={`${styles.section} section bg-codepolitan`}>
            <div className="container p-5">
                <div className="row">
                    <div className="col-lg-6 text-center text-lg-start">
                        <h2 className={`${styles.cta_title} mb-4`}>Yuk Ikut Beasiswa Gratis ini Bersama Codepolitan</h2>
                        <a className={`btn btn-lg px-5 ${styles.btn_cta}`} href="https://apps.codepolitan.com/user/login?callback=paths/detail/pemrograman-untuk-pemula">Klaim Beasiswa</a>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default CtaSectionV2;
