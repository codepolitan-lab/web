import styles from './CtaSection.module.scss';

const CtaSection = ({ title, priceImg, actionLink }) => {
  return (
    <section className={`${styles.section} section bg-codepolitan`}>
      <div className="container p-4 p-lg-5">
        <div className="row text-light py-5">
          <div className="col-lg-6">
            <h2 className={`${styles.cta_title} section-title mb-4`}>
              {title}
            </h2>
            <div
              className="card shadow border-2 border-white"
              style={{ borderRadius: '20px' }}
            >
              <div className="card-body">
                <img
                  className="img-fluid"
                  src={priceImg}
                  alt={title}
                />
              </div>
            </div>
            <a
              className={`btn ${styles.btn_cta} btn-lg shadow d-block my-3`}
              href={actionLink}
              target="_blank" rel="noopener noreferrer"
              style={{ borderRadius: '15px', fontWeight: 'bold' }}
            >
              Gabung Kelas
            </a>
          </div>
        </div>
      </div>
    </section>
  );
};

export default CtaSection;
