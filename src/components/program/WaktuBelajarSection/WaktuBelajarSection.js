const WaktuBelajarSection = ({ order }) => {
    return (
        <section className="section">
            <div className="container p-4 p-lg-5">
                <div className="row justify-content-between">
                    <div className={`col-lg-5 mb-5 mb-lg-0 ${order}`}>
                        <img className="img-fluid d-block mx-auto" src="/assets/img/program/waktu-belajar.png" alt="Waktu Belajar Fleksibel" />
                    </div>
                    <div className="col-lg-6 my-auto">
                        <h2 className="section-title">Waktu Belajar Fleksibel</h2>
                        <p className="my-3 text-muted">Gausah bingung kapan belajarnya, kamu dapat nentuin kapan dan dimana aja kamu belajar</p>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default WaktuBelajarSection;
