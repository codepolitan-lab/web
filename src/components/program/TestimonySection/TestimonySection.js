// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

// import Swiper core and required modules
import SwiperCore, { Pagination, Navigation } from "swiper";
import TestimonyCard from "../TestimonyCard/TestimonyCard";

// install Swiper modules
SwiperCore.use([Pagination, Navigation]);


const TestimonySection = ({ data }) => {
    return (
        <section className="section bg-light" id="swiperCarouselTestimony">
            <div className="container p-5">
                <div className="row text-center">
                    <div className="col">
                        <h2 className="section-title">Apa yang Siswa Katakan?</h2>
                        <p className="text-muted">Review dari siswa yang telah mengikuti kelas</p>
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        <Swiper
                            spaceBetween={20}
                            grabCursor={false}
                            slidesPerView={1}
                            loop
                            pagination={true}
                            navigation={true}
                            breakpoints={{
                                // when window width is >= 414px
                                414: {
                                    slidesPerView: 1,
                                },
                                // when window width is >= 768px
                                768: {
                                    slidesPerView: 2,
                                },
                                1024: {
                                    slidesPerView: 3,
                                },
                            }}
                            style={{ paddingBottom: "60px" }}
                        >
                            {data?.map((item, index) => {
                                return (
                                    <SwiperSlide key={index}>
                                        <TestimonyCard
                                            name={item.name.length > 20 ? item.name.slice(0, 20) + '...' : item.name}
                                            avatar={item.avatar}
                                            comment={item.comment}
                                            rating={item.rate}
                                        />
                                    </SwiperSlide>
                                );
                            }).slice(0, 5)}
                        </Swiper>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default TestimonySection;
