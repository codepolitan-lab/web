import styles from './StudycaseSection.module.scss';
import { Swiper, SwiperSlide } from "swiper/react";

// import Swiper core and required modules
import SwiperCore, { Navigation } from "swiper";

// install Swiper modules
SwiperCore.use([Navigation]);

const StudycaseSection = ({ data }) => {
    return (
        <section className="section bg-light" id="swiperCarouselStudyCase">
            <div className="container p-4 p-lg-5">
                <div className="row">
                    <div className="col-lg-4 my-auto">
                        <h2 className="section-title">Belajar Dengan Studi Kasus</h2>
                        <p className="text-muted my-3">Nggak cuman teori doang, di kelas ini kamu juga bisa langsung praktek mengikuti studi kasus yang telah disediakan. kami telah menyediakan contoh studi kasus pembuatan aplikasi webstite dan kamu juga bisa mengikuti step by step sampai kamu bisa membuatnya sendiri.</p>
                    </div>
                    <div className="col-lg-7 ms-auto">
                        <Swiper
                            spaceBetween={20}
                            grabCursor={true}
                            slidesPerView={2}
                            pagination={true}
                            navigation={true}
                            breakpoints={{
                                // when window width is >= 414px
                                414: {
                                    slidesPerView: 2,
                                },
                                // when window width is >= 768px
                                768: {
                                    slidesPerView: 3,
                                },
                                1024: {
                                    slidesPerView: 3,
                                },
                            }}
                            style={{ paddingTop: "60px" }}
                        >
                            {data.map((item, index) => {
                                return (
                                    <SwiperSlide key={index}>
                                        <div className={`${styles.card} card text-white`}>
                                            <img src={item.thumbnail} className={`${styles.card_img} card-img`} alt={item.title} />
                                            <span className={styles.text_background} />
                                            <div className={`${styles.overlay} card-img-overlay d-flex align-items-end`}>
                                                <h5 className={styles.card_title}>{item.title}</h5>
                                            </div>
                                        </div>
                                    </SwiperSlide>
                                );
                            })}
                        </Swiper>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default StudycaseSection;
