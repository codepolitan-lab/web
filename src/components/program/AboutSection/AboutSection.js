

const AboutSection = ({ thumbnail, title, description }) => {
    return (
        <section className="section bg-codepolitan text-white" id="about">
            <div className="container p-4 p-lg-5">
                <div className="row justify-content-center">
                    <div className="col-8 col-md-5 mb-5 mb-md-0">
                        <img width={'80%'} className="d-block mx-auto" src={thumbnail} alt={title} />
                    </div>
                    <div className="col-md-6 offset-md-1 my-auto">
                        <span><strong>Program Kelas Online</strong></span>
                        <h2 className="h1 section-title text-white">{title}</h2>
                        <p className="my-3">{description}</p>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default AboutSection;
