import styles from './MentorSection.module.scss';

const MentorSection = ({name, title, description, img, backgroundImg}) => {
    return (
        <section className={`${styles.section} bg-light`}>
            <div className="container p-4 p-lg-5">
                <div className="row">
                    <div className="col-lg-6 my-5 text-center text-md-start" style={{zIndex: 1}}>
                        <h2 className={styles.title}>{name}</h2>
                        <h5 className="text-muted">{title}</h5>
                        <p className="my-3 text-muted">{description}</p>
                    </div>
                    <div className="col-lg-6 order-first order-md-last">
                        <img className="img-fluid d-block mx-auto px-5" src={img} alt="Waktu Belajar Fleksibel" />
                    </div>
                </div>
            </div>
        </section>
    );
};

export default MentorSection;
