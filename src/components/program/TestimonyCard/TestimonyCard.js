import styles from './TestimonyCard.module.scss';
import { RatingView } from 'react-simple-star-rating';

const TestimonyCard = ({ name, avatar, comment, rating }) => {
    return (
        <div>
            <img className={styles.card_img} src={avatar || '/assets/img/program/icons/icon-avatar.png'} alt={name} />
            <div className={`${styles.testimony_card} card border-0`}>
                <div className="card-body mt-5 pt-4">
                    <p className="text-center text-muted">{comment || <i>Tidak ada komentar</i>}</p>
                </div>
                <div className="card-footer bg-white border-0" style={{ borderRadius: '25px' }}>
                    <div className="row">
                        <div className="col">
                            <h6>{name}</h6>
                        </div>
                        <div className="col-auto text-end">
                            <RatingView size={20} ratingValue={rating} />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default TestimonyCard;
