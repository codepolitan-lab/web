import styles from './Hero.module.scss';

const Hero = ({ heroTitle, heroSubtitle, heroImg, heroBgColor, heroFontColor, heroBtnColor }) => {
    return (
        <section className={`${styles.hero} ${heroBgColor}`}>
            <div className="container pt-5 p-4 p-lg-5">
                <div className="row text-center text-md-start">
                    <div className={`col-lg-6 ${heroFontColor} my-auto`}>
                        <h1 className={styles.hero_title}>{heroTitle}</h1>
                        <p className="lead text-muted my-3">{heroSubtitle}</p>
                        <a className={`btn ${heroBtnColor} px-4 py-2 mt-3`} href="#about">Selengkapnya</a>
                    </div>
                    <div className="col-lg-6 d-none d-lg-block">
                        <img className="img-fluid ps-lg-5" src={heroImg} alt="Cover Page" />
                    </div>
                </div>
            </div>
        </section>
    );
};

export default Hero;
