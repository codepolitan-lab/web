import styles from './HeroMobirise.module.scss';

const HeroMobirise = () => {
    return (
        <section className={`${styles.hero} bg-codepolitan`}>
            <div className="container p-4 p-lg-5">
                <div className="row text-center">
                    <div className="col">
                        <h1 className="section-title text-white">3 Hari Membuat Website Sendiri,<br />Tanpa Ngoding Tanpa Budget<br />dan Langsung Online!</h1>
                        <p className="mt-4 text-white w-75 mx-auto">Cukup satu kelas online yang Kamu butuhkan untuk membuat website Company Profile lengkap dengan pemaparan teknis, studi kasus, dan bahan-bahan buat praktek. Tidak perlu skill ngoding, install server ataupun keahlian teknis lainnya.</p>
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        <img className="img-fluid d-block mx-auto" src="/assets/img/program/mobirise-placeholder.png" alt="Mobirise" />
                    </div>
                </div>
            </div>
        </section>
    );
};

export default HeroMobirise;
