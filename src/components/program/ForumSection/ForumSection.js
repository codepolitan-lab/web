import styles from './ForumSection.module.scss';
import Link from 'next/link';

const ForumSection = () => {
    return (
        <section className={`${styles.section} bg-light`}>
            <div className="container p-4 p-lg-5">
                <div className="row">
                    <div className="col-lg-6 my-auto">
                        <div className="my-lg-5">
                            <h2 className={styles.title}>Forum Tanya Jawab</h2>
                            <p className="my-3 text-muted">Jika kamu memiliki kendala saat belajar atau mengalami kesulitan dalam memahami materi belajar, kamu bisa langsung bertanya di kanal tanya jawab yang telah disediakan. Mentor dan tim Codepolitan akan senang membantumu.</p>
                            <Link href="https://apps.codepolitan.com/forum">
                                <a className="btn btn-outline-secondary mb-5">Lihat Forum</a>
                            </Link>
                        </div>
                    </div>
                    <div className="col-lg-6 ps-lg-5">
                        <img className="img-fluid d-block mx-auto" src="/assets/img/program/forum-img-2.png" alt="Forum Tanya Jawab" />
                    </div>
                </div>
            </div>
        </section>
    );
};

export default ForumSection;
