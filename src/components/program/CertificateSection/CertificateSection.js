import styles from './CertificateSection.module.scss';

const CertificateSection = () => {
    return (
        <section className={`${styles.section}`}>
            <div className="container p-4 p-lg-5">
                <div className="row mt-5 mt-lg-0 my-lg-5 justify-content-lg-end">
                    <div className="col-lg-4 my-lg-5 py-lg-5">
                        <h2 className={styles.title}>Sertifikat</h2>
                        <p className="my-3 text-muted">Kamu akan mendapatkan sertifikat digital setelah menyelesaikan kelas ini</p>
                    </div>
                </div>
            </div>
            <div className="px-auto d-lg-none" style={{overflow: 'hidden'}}>
                <div className="row">
                    <div className="col">
                        <img className="img-fluid" src="/assets/img/program/certificate-img-small.png" alt="Forum Tanya Jawab" />
                    </div>
                </div>
            </div>
        </section>
    );
};

export default CertificateSection;
