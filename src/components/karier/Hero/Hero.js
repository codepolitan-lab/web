import styles from './Hero.module.scss';

const Hero = () => {
    return (
        <section className={`${styles.hero}`}>
            <div className="container p-4 p-lg-5">
                <div className="row">
                    <div className="col-lg-7 text-white text-center text-lg-start my-auto">
                        <h1 className={styles.hero_title}>Bangun Karier Anda di Tempat yang Tepat</h1>
                        <p className="lead text-muted">Program Karier dari CodePolitan membantu Anda dalam membangun dan meningkatkan karir dengan memilihkan perusahaan yang tepat dengan skill yang Anda miliki</p>
                        <a className="btn btn-primary py-3 px-4 my-3" href="#partners">Pelajari Selengkapnya</a>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default Hero;
