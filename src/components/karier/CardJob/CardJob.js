import styles from './CardJob.module.scss';
import { faBriefcaseClock, faMapMarkerAlt } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";


const CardJob = ({ thumbnail, title }) => {
    return (
        <div className={`${styles.card_job} card border-0 shadow h-100`}>
            <img src={thumbnail || "/assets/img/placeholder.jpg"} className={`${styles.card_img_top} card-img-top`} alt={title} />
            <div className="card-body py-4">
                <span className={styles.company}>Codepolitan</span>
                <h5 className="card-title my-3">(Urgent Needed) Senior Android Developer</h5>
                <div className="row text-muted">
                    <div className="col-auto">
                        <FontAwesomeIcon className="me-1" fixedWidth icon={faMapMarkerAlt} />
                        Kota Bandung
                    </div>
                    <div className="col-auto">
                        <FontAwesomeIcon className="me-1" fixedWidth icon={faBriefcaseClock} />
                        Fulltime
                    </div>
                </div>
            </div>
        </div>

    );
};

export default CardJob;
