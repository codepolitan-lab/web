import CardJob from "../../CardJob/CardJob";

const SectionJobList = () => {
    return (
        <section className="section">
            <div className="container p-4 p-lg-5">
                <div className="row mb-4">
                    <div className="col text-center">
                        <h2 className="section-title">Perusahaan Yang Membutuhkan Skill Anda</h2>
                        <p className="text-muted">Berikut beberapa perusahaan yang membutuhkan kandidat cepat untuk tim mereka</p>
                    </div>
                </div>
                <div className="row">
                    {[1, 2, 3].map((item, index) => {
                        return (
                            <div className="col-md-6 col-lg-4 my-3" key={index}>
                                <CardJob />
                            </div>
                        );
                    })}
                </div>
            </div>
        </section>
    );
};

export default SectionJobList;