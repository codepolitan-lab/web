const SectionSteps = () => {
    return (
        <section className="section bg-light">
            <div className="container p-4 p-lg-5">
                <div className="row mb-4">
                    <div className="col text-center">
                        <h2 className="section-title">Mulai Langkah Terbaikmu Disini</h2>
                        <p className="text-muted">6 langkah untuk terhubung dengan jaringan perusahaan mitra CodePolitan</p>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-6 col-lg-4 p-4">
                        <div className="card border-0 rounded-3 shadow h-100">
                            <div className="card-header bg-white border-0 text-end">
                                <span className="badge bg-codepolitan p-3">1</span>
                            </div>
                            <div className="card-body text-center">
                                <img className="img-fluid px-4" src="/assets/img/karier/registrasi.png" alt="Registrasi" />
                                <h5 className="text-muted my-3">Registrasi dan login ke platform CodePolitan</h5>
                            </div>
                        </div>
                    </div>

                    <div className="col-md-6 col-lg-4 p-4">
                        <div className="card border-0 rounded-3 shadow h-100">
                            <div className="card-header bg-white border-0 text-end">
                                <span className="badge bg-codepolitan p-3">2</span>
                            </div>
                            <div className="card-body text-center">
                                <img className="img-fluid px-4" src="/assets/img/karier/karier-page.png" alt="Lengkapi Portfolio" />
                                <h5 className="text-muted my-3">Masuk ke halaman Karier CodePolitan dan lengkapi data portfolio Anda</h5>
                            </div>
                        </div>
                    </div>

                    <div className="col-md-6 col-lg-4 p-4">
                        <div className="card border-0 rounded-3 shadow h-100">
                            <div className="card-header bg-white border-0 text-end">
                                <span className="badge bg-codepolitan p-3">3</span>
                            </div>
                            <div className="card-body text-center">
                                <img className="img-fluid px-4" src="/assets/img/karier/choose-job.png" alt="Pilih Pekerjaan" />
                                <h5 className="text-muted my-3">Pilih bidang pekerjaan yang cocok dengan skill Anda</h5>
                            </div>
                        </div>
                    </div>

                    <div className="col-md-6 col-lg-4 p-4">
                        <div className="card border-0 rounded-3 shadow h-100">
                            <div className="card-header bg-white border-0 text-end">
                                <span className="badge bg-codepolitan p-3">4</span>
                            </div>
                            <div className="card-body text-center">
                                <img className="img-fluid px-4" src="/assets/img/karier/skillset.png" alt="Skillset" />
                                <h5 className="text-muted my-3">Sempurnakan skillset Anda sesuai dengan rekomendasi skillset dari CodePolitan</h5>
                            </div>
                        </div>
                    </div>

                    <div className="col-md-6 col-lg-4 p-4">
                        <div className="card border-0 rounded-3 shadow h-100">
                            <div className="card-header bg-white border-0 text-end">
                                <span className="badge bg-codepolitan p-3">5</span>
                            </div>
                            <div className="card-body text-center">
                                <img className="img-fluid px-4" src="/assets/img/karier/user-list.png" alt="User List" />
                                <h5 className="text-muted my-3">Perusahaan akan menemukan Anda sesuai dengan bidang pekerjaan yang mereka cari</h5>
                            </div>
                        </div>
                    </div>

                    <div className="col-md-6 col-lg-4 p-4">
                        <div className="card border-0 rounded-3 shadow h-100">
                            <div className="card-header bg-white border-0 text-end">
                                <span className="badge bg-codepolitan p-3">6</span>
                            </div>
                            <div className="card-body text-center">
                                <img className="img-fluid px-4" src="/assets/img/karier/confirmation.png" alt="Konfirmasi" />
                                <h5 className="text-muted my-3">CodePolitan menginformasikan penawaran dari perusahaan kepada Anda</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default SectionSteps;