import { Swiper, SwiperSlide } from "swiper/react";

// import Swiper core and required modules
import SwiperCore, { Autoplay, Navigation } from "swiper";
import Link from "next/link";
import CardPopular from "../../../exercise/Cards/CardPopular/CardPopular";
import { shuffleArray } from "../../../../utils/helper";

// install Swiper modules
SwiperCore.use([Autoplay, Navigation]);

const SectionExercise = ({ title, data, courseFor, link, exerciseLevel }) => {
    return (
        <section className="section bg-light" id="Swiper">
            <div className="container p-3 px-lg-4" id="course">
                <div className="row justify-content-between pt-3 pt-lg-5">
                    <div className="col-md-6">
                        <h2 className="section-title text-muted h3 ms-lg-4">{title} {courseFor && <span className="text-primary">{courseFor}</span>}</h2>
                    </div>
                    <div className="col-md-6 text-start text-md-end my-auto">
                        <Link href={link}>
                            <a className="text-primary me-lg-4">Lihat Semua</a>
                        </Link>
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        <Swiper
                            className="py-4 px-lg-4"
                            spaceBetween={30}
                            grabCursor={true}
                            slidesPerView={1.1}
                            navigation
                            breakpoints={{
                                // when window width is >= 414px
                                375: {
                                    slidesPerView: 1.3,
                                },
                                // when window width is >= 768px
                                768: {
                                    slidesPerView: 2.5,
                                },
                                1200: {
                                    slidesPerView: 4,
                                },
                            }}
                        >
                            {
                                data.map((exercise, index) => {
                                    return (
                                        <SwiperSlide key={index}>
                                            <Link href={`/course/intro/${exercise.slug}`}>
                                                <a className="link">
                                                    <CardPopular
                                                        cover={exercise.cover}
                                                        author={exercise.author}
                                                        title={exercise.title}
                                                        level={exercise.level}
                                                        totalStudents={exercise.total_student}
                                                        totalModules={exercise.total_module}
                                                        totalTimes={exercise.duration}
                                                        totalRating={exercise.total_rating}
                                                        totalFeedback={exercise.total_feedback}
                                                        exerciseLevel={exerciseLevel}
                                                    />
                                                </a>
                                            </Link>
                                        </SwiperSlide>
                                    );
                                }).slice(0, 15)
                            }
                        </Swiper>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default SectionExercise;
