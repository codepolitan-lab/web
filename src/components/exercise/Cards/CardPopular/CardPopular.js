import { faClock, faCheckCircle, faSwatchbook, faUsers, faChartColumn, faStarHalfStroke, faStar } from '@fortawesome/free-solid-svg-icons';
import { faStar as faStarRegular } from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { formatPrice } from '../../../../utils/helper';
import styles from './CardPopular.module.scss';

const CardPopular = ({ cover, author, title, level, totalStudents, totalModules, totalTimes, exerciseLevel }) => {
    return (
        <div className={`${styles.card_course} card border-0 shadow-sm my-2`}>
            <div className="position-relative card-img-top">
                <img src={cover || "/assets/img/placeholder.jpg"} className={`${styles.card_img_top} card-img-top w-100`} loading="lazy" alt={title} />
                {exerciseLevel == 'Basic' && (<div className="position-absolute bottom-0 bg-pink px-3 small text-white" style={{ zIndex: '100!important' }}>{exerciseLevel}</div>)}
                {exerciseLevel == 'Intermediate' && (<div className="position-absolute bottom-0 bg-primary px-3 small text-white" style={{ zIndex: '100!important' }}>{exerciseLevel}</div>)}
                {exerciseLevel == 'Master' && (<div className="position-absolute bottom-0 bg-purple px-3 small text-white" style={{ zIndex: '100!important' }}>{exerciseLevel}</div>)}

            </div>

            <div className="card-body">
                {/* <span>
                    <small>By {author || 'Unknown'} {author && (<FontAwesomeIcon fixedWidth className="text-primary" icon={faCheckCircle} />)}</small>
                </span> */}
                <h5 className={styles.course_title} title={title}>{title}</h5>
                <div className={styles.course_info}>

                    <div className="row">
                        <div className="col-auto">
                            <p className="my-2"><FontAwesomeIcon fixedWidth icon={faChartColumn} /> {level && level.charAt(0).toUpperCase() + level.slice(1)}</p>
                            <p className="my-2"><FontAwesomeIcon fixedWidth icon={faUsers} /> {totalStudents} Siswa</p>
                        </div>
                        <div className="col-auto">
                            <p className="my-2"><FontAwesomeIcon fixedWidth icon={faClock} /> {totalTimes} Menit</p>
                            <p className="my-2"><FontAwesomeIcon fixedWidth icon={faSwatchbook} /> {totalModules} Modul</p>
                        </div>
                    </div>


                </div>
            </div>
            <div className={`card-footer bg-white ${styles.card_footer}`}>
                <div className={styles.rate_and_price}>
                    <div className="row justify-content-between">
                        <div className="col-auto">
                            <strong>Free</strong>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    );
};

export default CardPopular;
