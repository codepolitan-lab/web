import Link from 'next/link';
import styles from './Hero.module.scss';

const Hero = () => {
    return (
        <section className={`${styles.hero}`}>
            <div className="container p-4 p-lg-5">
                <div className="row justify-content-center">
                    <div className="col-lg-6 text-white text-center my-auto">
                        <h1 className={styles.hero_title}>Praktek Coding Yang Mudah Terukur Dan Terarah</h1>
                        <p className="lead">Codepolitan Exercises adalah sebuah fitur gratis untuk  menguji seberapa jauh skill pemrograman yang kamu miliki melalui pertanyaan exlusif codepolitan</p>
                        <div className="d-flex justify-content-evenly flex-lg-row flex-column">
                            <Link href="/exercise/library">
                                <a className="btn btn-primary py-2 px-3 my-3">Mulai Exercise</a>
                            </Link>
                            <a className="btn btn-outline-light py-2 px-3 my-3" href="#partners">Pelajari Lebih Lanjut</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default Hero;
