import Link from 'next/link';
import styles from './Hero.module.scss';

const HeroItem = ({ backgroundImage, headline, subtext, buttonLabel, buttonLink, buttonNewtab }) => {
    return (
        <div className={`card shadow-sm ${styles.hero_item} h-100`} style={{ backgroundImage: `url(${backgroundImage || '/assets/img/placeholder.jpg'})` }}>
            <div className="card-body p-5">
                <div className="row h-100">
                    <div className="col-md-7 text-white text-center text-md-start my-auto">
                        <h1 className={styles.hero_title}>{headline || "No Headline"}</h1>
                        <p className="lead">{subtext || "No subtext"}</p>
                        {buttonNewtab === "Yes" ? (
                            <a className="btn btn-light btn-rounded text-primary" href={buttonLink || '/'} target="_blank" rel="noopener noreferrer">{buttonLabel || "Button"}</a>
                        ) : (
                            <Link href={buttonLink || '/'}>
                                <a className="btn btn-light btn-rounded text-primary">{buttonLabel || "Button"}</a>
                            </Link>
                        )}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default HeroItem;