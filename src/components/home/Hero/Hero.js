import styles from './Hero.module.scss';
import { Swiper, SwiperSlide } from "swiper/react";

// import Swiper core and required modules
import SwiperCore, { Pagination, Autoplay, Navigation } from "swiper";
import HeroItem from './HeroItem';

// install Swiper modules
SwiperCore.use([Autoplay, Pagination, Navigation]);

const Hero = ({ banners }) => {
    return (
        <section className={`${styles.hero} bg-light`} id="swiperHomepage">
            <div className="overflow-hidden">
                <Swiper
                    id="Swiper"
                    className="py-3 px-lg-5"
                    slidesPerView={1.1}
                    spaceBetween={20}
                    loop
                    autoplay
                    centeredSlides
                    navigation
                >
                    {banners?.sort((a, b) => (a.sort > b.sort ? 1 : -1)).map((banner, index) => {
                        return (
                            <SwiperSlide key={index}>
                                <HeroItem
                                    backgroundImage={banner.background_image}
                                    headline={banner.headline}
                                    subtext={banner.subtext}
                                    buttonLabel={banner.button_label}
                                    buttonLink={banner.button_link}
                                    buttonNewtab={banner.button_newtab}
                                />
                            </SwiperSlide>
                        );
                    })}
                </Swiper>
            </div>
        </section >
    );
};

export default Hero;
