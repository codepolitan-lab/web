import styles from './CardFeaturedArticle.module.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCalendar, faUser } from '@fortawesome/free-solid-svg-icons';
import { formatOnlyDate } from '../../../../utils/helper';

const CardFeaturedArticle = ({ thumbnail, title, date, writer, shortDesc }) => {
    return (
        <div className={`card ${styles.card_article} h-100 shadow`}>
            <img src={thumbnail || '/assets/img/placeholder.jpg'} className="card-img" loading="lazy" alt={title} />
            <div className="card-body px-2 px-md-3 py-3">
                <h5 className={styles.title} title={title}>{title || 'Undefined'}</h5>
                <small className="my-2">
                    <p className="mb-1">
                        <FontAwesomeIcon icon={faUser} />
                        <span className="ms-1">{writer}</span>
                    </p>
                    <p className="mb-1">
                        <FontAwesomeIcon icon={faCalendar} />
                        <span className="ms-1">{formatOnlyDate(date)}</span>
                    </p>
                </small>
                {/* <p>{shortDesc}</p> */}
            </div>
        </div>
    );
};

export default CardFeaturedArticle;
