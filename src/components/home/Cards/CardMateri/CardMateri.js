import { faBook } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Image from 'next/image';

const CardMateri = ({ title, thumbnail, totalCourse }) => {
    return (
        <div className="card border-0 shadow mb-4" style={{ borderRadius: '12px' }}>
            <div className="card-body px-3 py-2">
                <div className="row text-muted">
                    <div className="col-4 my-auto">
                        <Image
                            className="rounded"
                            src={thumbnail || '/assets/img/placeholder.jpg'}
                            alt={title}
                            width={100}
                            height={100}
                            layout="responsive"
                            placeholder="blur"
                            blurDataURL="/assets/img/placeholder.jpg"
                        />
                        {/* <img className="img-fluid d-block ms-2 rounded" src={thumbnail || '/assets/img/placeholder.jpg'} loading="lazy" alt={title} /> */}
                    </div>
                    <div className="col-8 my-auto">
                        <h5 style={{ fontSize: 'medium', marginBottom: 0 }} title={title}>{title.length > 15 ? title.slice(0, 15) + '...' : title}</h5>
                        {totalCourse == 0
                            ? (<span style={{ fontSize: 'small' }}>Belum Ada Kelas</span>)
                            : (<span style={{ fontSize: 'small' }}><FontAwesomeIcon size="sm" icon={faBook} /> {totalCourse} Kelas</span>)
                        }
                    </div>
                </div>
            </div>
        </div>
    );
};

export default CardMateri;
