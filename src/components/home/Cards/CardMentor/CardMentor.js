import styles from './CardMentor.module.scss';

const CardMentor = ({ thumbnail, name, jobs, brandImage, fromBrand }) => {
    return (
        <div className="card bg-white border-0 h-100">
            <img className={styles.card_img} src={thumbnail || "/assets/img/placeholder.jpg"} alt={name || "Belum ada nama"} />
            <div className="card-body text-center px-0 pb-2">
                <h5 className="card-text h6 mb-3">{name || "Belum ada nama"}</h5>
                <div className="my-auto h-50">
                    <p className={`${styles.jobs} text-muted`} title={jobs}>
                        <small>{jobs || "Job belum terisi"}</small>
                    </p>
                </div>
            </div>
            <div className="card-footer bg-white border-0">
                {!fromBrand && (
                    <img className="img-fluid w-25 rounded-circle d-block mx-auto" src={brandImage || "/assets/img/placeholder.jpg"} alt="Brand" />
                )}
            </div>
        </div>
    );
};

export default CardMentor;
