import Image from "next/image";
import styles from './CardBeasiswa.module.scss';

const CardBeasiswa = ({ thumbnail, title, description, organizer }) => {
    return (
        <div className={`${styles.card} card border-0 rounded shadow-sm`}>
            <div>
                <Image
                    className={`card-img-top`}
                    src={thumbnail || "/assets/img/placeholder.jpg"}
                    placeholder="blur"
                    blurDataURL="/assets/img/placeholder.jpg"
                    alt={title}
                    width={100}
                    height={50}
                    layout="responsive"
                    objectFit="cover"
                />
                {/* <img style={{ height: '250px', objectFit: 'cover' }} src={thumbnail || '/assets/img/placeholder.jpg'} className="card-img-top" loading="lazy" alt="Test" /> */}
            </div>
            <div className="card-body">
                <strong className="text-primary">{organizer || 'Unknown'}</strong>
                <h5 className="card-title">{title || 'Untitled'}</h5>
                <p className="text-muted mb-0">{description || 'No description'}</p>
            </div>
        </div>
    );
};

export default CardBeasiswa;
