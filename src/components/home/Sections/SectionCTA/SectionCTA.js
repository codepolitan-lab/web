import Link from "next/link";

const SectionCTA = () => {
    return (
        <section className="section bg-codepolitan">
            <div className="container p-4 p-lg-5">
                <div className="row">
                    <div className="col text-center">
                        <h2 className="section-title text-white">Yuk Buktikan Sendiri, Bahwa Codepolitan Dapat<br />Membantumu Mengembangkan Skill Pemrogrammanmu</h2>
                        <Link href="/library?type=free">
                            <a className="btn btn-outline-light mt-4 mb-3 btn-lg">Mulai dari Kelas Gratis</a>
                        </Link>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default SectionCTA;
