import { Swiper, SwiperSlide } from "swiper/react";
import SwiperCore, { Pagination, Navigation } from "swiper";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowRight, faBuilding, faSchool } from "@fortawesome/free-solid-svg-icons";

// install Swiper modules
SwiperCore.use([Pagination, Navigation]);

const SectionTraining = () => {
    return (
        <section className="section bg-light">
            <div className="container p-4 p-lg-5">
                <div className="row justify-content-between">
                    <div className="col-lg-5">
                        <Swiper
                            id="Swiper"
                            spaceBetween={25}
                            grabCursor={false}
                            slidesPerView={1}
                            loop
                            pagination
                            navigation
                            style={{ paddingLeft: '20px', paddingRight: '20px', paddingBottom: '60px' }}
                        >
                            <SwiperSlide>
                                <img style={{ height: '20em', width: '100%', objectFit: 'cover', borderRadius: '20px' }} src="/assets/img/home/devschool-1.webp" alt="Devschool Pic 1" />
                            </SwiperSlide>
                            <SwiperSlide>
                                <img style={{ height: '20em', width: '100%', objectFit: 'cover', borderRadius: '20px' }} src="/assets/img/home/devschool-2.webp" alt="Devschool Pic 2" />
                            </SwiperSlide>
                            <SwiperSlide>
                                <img style={{ height: '20em', width: '100%', objectFit: 'cover', borderRadius: '20px' }} src="/assets/img/home/corporate-1.webp" alt="Corporate Training Pic 1" />
                            </SwiperSlide>
                            <SwiperSlide>
                                <img style={{ height: '20em', width: '100%', objectFit: 'cover', borderRadius: '20px' }} src="/assets/img/home/corporate-2.webp" alt="Corporate Training Pic 2" />
                            </SwiperSlide>
                        </Swiper>
                    </div>
                    <div className="col-lg-6 my-4 order-first order-lg-last">
                        <ul className="nav nav-pills mb-3" id="pills-tab-training" role="tablist">
                            <li className="nav-item" role="presentation">
                                <button className="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true"><FontAwesomeIcon icon={faSchool} /> Public Training</button>
                            </li>
                            <li className="nav-item" role="presentation">
                                <button className="nav-link" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false"><FontAwesomeIcon icon={faBuilding} /> Corporate Training</button>
                            </li>
                        </ul>
                        <div className="tab-content" id="pills-tabContent">
                            <div className="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab" tabIndex={0}>
                                <div className="my-5">
                                    <h3 className="section-title">Public Training</h3>
                                    <p className="text-muted my-3">Program training bootcamp yang di selenggarakan oleh Codepolitan tentang belajar Menjadi Fullstack Developer menggunakan Laravel. Kamu bisa mengikuti program ini sekarang juga supaya mendapatkan banyak benefit setelah lulus dari program ini.</p>
                                    <a className="text-primary link" href="https://devschool.id" target="_blank" rel="noopener noreferrer">Lihat Selengkapnya <FontAwesomeIcon icon={faArrowRight} /></a>
                                </div>
                            </div>
                            <div className="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab" tabIndex={0}>
                                <div className="my-5">
                                    <h3 className="section-title">Corporate Training</h3>
                                    <p className="text-muted my-3">Devschool For Corporate adalah program pelatihan yang bisa anda adakan secara custom mulai dari waktu belajar dan materi belajarnya, cukup percayakan pelatihan anda kepada kami sekarang juga.</p>
                                    <a className="text-primary link" href="https://devschool.id/for-company" target="_blank" rel="noopener noreferrer">Lihat Selengkapnya <FontAwesomeIcon icon={faArrowRight} /></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default SectionTraining;
