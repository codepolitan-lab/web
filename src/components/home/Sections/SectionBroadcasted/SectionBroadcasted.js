import Image from "next/image";

const SectionBroadcasted = () => {
    const broadcasters = [
        {
            name: 'iNews',
            logo: 'https://image.web.id/images/iNewsLogo-tag.png',
            link: 'https://www.inews.id/techno/internet/codepolitan-website-belajar-coding-online-dengan-alur-belajar-terarah'
        },
        {
            name: 'Yahoo!',
            logo: 'https://image.web.id/images/yahoo_news_id-ID_h_p_newsv2.png',
            link: 'https://id.berita.yahoo.com/nyankod-ingin-orang-indonesia-belajar-pemrograman-dengan-bahasa-020040810.html'
        },
        {
            name: 'Tech in Asia',
            logo: 'https://image.web.id/images/tech-in-asia-logo-vector.png',
            link: 'https://id.techinasia.com/codepolitan-rumah-programmer-indonesia'
        },
        {
            name: 'Merdeka',
            logo: 'https://image.web.id/images/logo-merdeka.png',
            link: 'https://www.merdeka.com/uang/melalui-it-perusahaan-ini-sediakan-720000-lowongan-kerja.html'
        },
        {
            name: 'IndoZone',
            logo: 'https://image.web.id/images/logob72c40bc07c727f3.png',
            link: 'https://www.indozone.id/tech/3esR6J/10-situs-website-belajar-coding-online-gratis-untuk-pemula'
        },
        {
            name: 'Daily Social',
            logo: 'https://image.web.id/images/daily-social.png',
            link: 'https://dailysocial.id/post/lima-situs-lokal-yang-bisa-anda-jadikan-tempat-belajar-pemrograman'
        },
    ];

    return (
        <section className="section bg-light">
            <div className="container p-4 p-lg-5">
                <div className="row mb-4">
                    <div className="col text-center">
                        <h3 className="text-muted">Codepolitan Telah Diliput di Berbagai Media Ternama</h3>
                    </div>
                </div>
                <div className="row justify-content-center mb-3">
                    {broadcasters.map((broadcaster, index) => {
                        return (
                            <div key={index} className="col-4 col-lg-2">
                                <div className="card bg-transparent border-0">
                                    <div className="card-body py-3">
                                        <a href={broadcaster.link} target="_blank" rel="noopener noreferrer">
                                            <Image
                                                src={broadcaster.logo}
                                                placeholder="blur"
                                                blurDataURL="/assets/img/placeholder.jpg"
                                                alt={broadcaster.name}
                                                layout="responsive"
                                                width={50}
                                                height={30}
                                                objectFit="contain"
                                            />
                                        </a>
                                    </div>
                                </div>
                            </div>
                        );
                    })}
                </div>
            </div>
        </section>
    );
};

export default SectionBroadcasted;