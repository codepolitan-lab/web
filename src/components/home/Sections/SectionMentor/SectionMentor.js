import { useRef } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
// import Swiper core and required modules
import SwiperCore, { Navigation } from "swiper";
import CardMentor from "../../Cards/CardMentor/CardMentor";
import Link from "next/link";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronCircleLeft, faChevronCircleRight } from "@fortawesome/free-solid-svg-icons";
import { shuffleArray } from "../../../../utils/helper";

// install Swiper modules
SwiperCore.use([Navigation]);

const SectionMentor = ({ data }) => {
    const navigationPrevRef = useRef(null);
    const navigationNextRef = useRef(null);

    return (
        <section className="section" id="Swiper">
            <div className="container p-4 p-lg-5">
                <div className="row justify-content-between">
                    <div className="col-auto">
                        <h2 className="section-title">Belajar Langsung dari Pakar</h2>
                        <p className="text-muted">Belajar cepat dan benar dari mentor <i>expert</i> pilihan Codepolitan</p>
                    </div>
                    <div className="col-auto d-none d-lg-block my-auto">
                        <span className="me-2" ref={navigationPrevRef} role="button">
                            <FontAwesomeIcon size="2x" className="text-primary" icon={faChevronCircleLeft} />
                        </span>
                        <span ref={navigationNextRef} role="button">
                            <FontAwesomeIcon size="2x" className="text-primary" icon={faChevronCircleRight} />
                        </span>
                    </div>
                </div>
                <div className="row mt-3 justify-content-between">
                    <div className="col">
                        <Swiper
                            spaceBetween={20}
                            grabCursor={true}
                            slidesPerView={1.3}
                            navigation={{
                                prevEl: navigationPrevRef.current,
                                nextEl: navigationNextRef.current,
                            }}
                            onBeforeInit={(swiper) => {
                                swiper.params.navigation.prevEl = navigationPrevRef.current;
                                swiper.params.navigation.nextEl = navigationNextRef.current;
                            }}
                            breakpoints={{
                                // when window width is >= 414px
                                375: {
                                    slidesPerView: 1.3,
                                },
                                // when window width is >= 768px
                                768: {
                                    slidesPerView: 3.3,
                                },
                                1200: {
                                    slidesPerView: 5,
                                },
                            }}
                            style={{ padding: 0 }}
                        >
                            {shuffleArray(data)
                                // .filter(mentor => mentor.brand_name !== "Bagus Budi Coders")
                                // .sort((a, b) => {
                                //     const textA = a.brand_name.toUpperCase();
                                //     const textB = b.brand_name.toUpperCase();
                                //     return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
                                // })
                                .map((mentor, index) => {
                                    return (
                                        <SwiperSlide style={{ height: 'auto' }} key={index}>
                                            <Link href={`/mentor/${mentor.username}`}>
                                                <a className="link">
                                                    <CardMentor
                                                        thumbnail={mentor.avatar}
                                                        name={mentor.name}
                                                        jobs={mentor.jobs}
                                                        description={mentor.short_description}
                                                        brandImage={mentor.brand_image}
                                                    />
                                                </a>
                                            </Link>
                                        </SwiperSlide>
                                    );
                                }).slice(0, 6)}
                            <SwiperSlide style={{ height: 'auto' }}>
                                <Link href={`/mentor`}>
                                    <a className="link">
                                        <div className="card border-0 h-100">
                                            <div className="card-body text-primary text-center">
                                                <div style={{ marginTop: '150px' }}>
                                                    <h5 className="card-text">Lihat semua mentor</h5>
                                                    <FontAwesomeIcon size="lg" icon={faChevronCircleRight} />
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </Link>
                            </SwiperSlide>
                        </Swiper>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default SectionMentor;
