import { Swiper, SwiperSlide } from "swiper/react";
import CardMateri from '../../Cards/CardMateri/CardMateri';

// import Swiper core and required modules
import SwiperCore, { Grid, Autoplay, Navigation } from "swiper";
import { formatNumber } from "../../../../utils/helper";
import Link from "next/link";

// install Swiper modules
SwiperCore.use([Grid, Autoplay, Navigation]);

const SectionMateri = ({ statistic, data, title }) => {

    const labels = [];
    const limit = 3;
    let start = 1;
    let counter = 0;
    let labelTemp = [];

    data.forEach(() => {
        if (start <= limit) {
            if (data[counter].term !== 'Free') {
                labelTemp.push(data[counter]);
                start++;
            };
        } else {
            start = 1
            labels.push(labelTemp);
            labelTemp = [];
        };
        counter++;
    });

    return (
        <section className="section bg-light" id="Swiper">
            <div className="container p-3 p-lg-4" id="course">
                <div className="row pt-3 pt-lg-5">
                    <h3 className="section-title mb-4 text-muted ms-lg-4">{title}</h3>
                    <div className="col">
                        <Swiper
                            className="px-lg-3"
                            loop
                            autoplay
                            spaceBetween={25}
                            slidesPerView={4}
                            navigation={true}
                            grabCursor={true}
                            breakpoints={{
                                // when window width is >= 414px
                                300: {
                                    slidesPerView: 2,
                                },
                                // when window width is >= 768px
                                768: {
                                    slidesPerView: 3,
                                },
                                1200: {
                                    slidesPerView: 4,
                                },
                            }}
                        >
                            {labels.map((label, index) => {
                                return (
                                    <SwiperSlide key={index}>
                                        <div className="px-2 my-3">
                                            <Link href={`/library?type=${label[0].term_slug}`}>
                                                <a className="link">
                                                    <CardMateri
                                                        title={label[0].term}
                                                        thumbnail={label[0].thumbnail}
                                                        totalCourse={label[0].total_course}
                                                    />
                                                </a>
                                            </Link>
                                        </div>
                                        <div className="px-2 my-3">
                                            <Link href={`/library?type=${label[1].term_slug}`}>
                                                <a className="link">
                                                    <CardMateri
                                                        title={label[1].term}
                                                        thumbnail={label[1].thumbnail}
                                                        totalCourse={label[1].total_course}
                                                    />
                                                </a>
                                            </Link>
                                        </div>
                                        <div className="px-2 my-3">
                                            <Link href={`/library?type=${label[2].term_slug}`}>
                                                <a className="link">
                                                    <CardMateri
                                                        title={label[2].term}
                                                        thumbnail={label[2].thumbnail}
                                                        totalCourse={label[2].total_course}
                                                    />
                                                </a>
                                            </Link>
                                        </div>
                                    </SwiperSlide>
                                );
                            })}
                        </Swiper>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default SectionMateri;
