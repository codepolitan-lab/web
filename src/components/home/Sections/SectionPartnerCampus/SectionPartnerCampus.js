import { useState } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import SwiperCore, { Autoplay } from "swiper";
import Image from "next/image";

SwiperCore.use([Autoplay]);

const SectionPartnerCampus = () => {
    const [campusPartners] = useState([
        {
            name: 'AMIK Bumi Nusantara Cirebon',
            thumbnail: 'https://image.web.id/images/2022/06/30/99a332ec1d2b33139df6d49043f86df4.png'
        },
        {
            name: 'ITB',
            thumbnail: 'https://image.web.id/images/2022/06/30/cd919f34ee20d27c16a9e3fc437c3ba1.png'
        },
        {
            name: 'PNJ',
            thumbnail: 'https://image.web.id/images/2022/06/30/8aa8b63d4679f96d1b8b0e58a82f60cf.png'
        },
        {
            name: 'PNP',
            thumbnail: 'https://image.web.id/images/2022/06/30/c8d7f5e96ab1e17df1ff9901425ad949.png'
        },
        {
            name: 'UII',
            thumbnail: 'https://image.web.id/images/2022/06/30/0b1dcaee983cf698c71f4511f5c02e07.png'
        },
        {
            name: 'Universitas Jember',
            thumbnail: 'https://image.web.id/images/2022/06/30/1ed6bc00d0a70682b46f03467fb6cfcf.png'
        },
        {
            name: 'UNP',
            thumbnail: 'https://image.web.id/images/2022/06/30/aa758139224939e2b91b93fafa86d167.png'
        },
        {
            name: 'UNPAD',
            thumbnail: 'https://image.web.id/images/2022/06/30/8a5eb01267d8ea23d51bdb3ac6cb5bd2.png'
        },
        {
            name: 'UNS',
            thumbnail: 'https://image.web.id/images/2022/06/30/40f757ba9a1782d1aeeab26d7c3177bf.png'
        },
        {
            name: 'UPI',
            thumbnail: 'https://image.web.id/images/2022/06/30/0ec158d933a1c3ff2391f792ecbeb9bf.png'
        },
        {
            name: 'UPJ',
            thumbnail: 'https://image.web.id/images/2022/06/30/499b6168350f8dec7c3b78488fa11b98.png'
        },
        {
            name: 'URINDO',
            thumbnail: 'https://image.web.id/images/2022/06/30/508eb7a0aa607c24ae6f3b3e4c30e03c.png'
        },
        {
            name: 'USTJ',
            thumbnail: 'https://image.web.id/images/2022/06/30/6a9b32f259ce603a7ca4b02cf7040c4b.png'
        },
    ]);

    return (
        <section className="bg-light">
            <div className="container p-4 p-lg-5">
                <div className="row my-4">
                    <div className="col">
                        <h3 className="text-muted text-center">Mitra Kampus Kami</h3>
                    </div>
                </div>
                <div className="row justify-content-center">
                    {
                        campusPartners.map((partner, index) => {
                            return (
                                <div key={index} className="col-4 col-lg-2">
                                    <div className="card bg-transparent border-0">
                                        <div className="card-body py-3">
                                            <Image src={partner.thumbnail} placeholder="blur" blurDataURL="/assets/img/placeholder.jpg" alt={partner.name} layout="responsive" width={50} height={30} objectFit="contain" />
                                        </div>
                                    </div>
                                </div>
                            )
                        }).slice(0, 5)
                    }
                </div>
                <div className="row justify-content-center">
                    {
                        campusPartners.map((partner, index) => {
                            return (
                                <div key={index} className="col-4 col-lg-2">
                                    <div className="card bg-transparent border-0">
                                        <div className="card-body py-3">
                                            <Image src={partner.thumbnail} placeholder="blur" blurDataURL="/assets/img/placeholder.jpg" alt={partner.name} layout="responsive" width={50} height={30} objectFit="contain" />
                                        </div>
                                    </div>
                                </div>
                            )
                        }).slice(5, 10)
                    }
                </div>
                <div className="row justify-content-center">
                    {
                        campusPartners.map((partner, index) => {
                            return (
                                <div key={index} className="col-4 col-lg-2">
                                    <div className="card bg-transparent border-0">
                                        <div className="card-body py-3">
                                            <Image src={partner.thumbnail} placeholder="blur" blurDataURL="/assets/img/placeholder.jpg" alt={partner.name} layout="responsive" width={50} height={30} objectFit="contain" />
                                        </div>
                                    </div>
                                </div>
                            )
                        }).slice(10, 13)
                    }
                </div>
            </div>
        </section>
    );
};

export default SectionPartnerCampus;
