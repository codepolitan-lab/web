import { useState } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import SwiperCore, { Autoplay } from "swiper";
import Image from "next/image";

SwiperCore.use([Autoplay]);

const SectionPartner = () => {
    const [partners] = useState([
        {
            name: 'Kemnaker',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/KEMNAKER_MGfr7cHn61.webp'
        },
        {
            name: 'Kemenkeu',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/kemenkeu_p9YQYkw-AI.webp'
        },
        {
            name: 'Here',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/here_THgKeV7WUz.webp'
        },
        {
            name: 'Udemy',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/udemy_DVYNj94zCK14x.webp'
        },
        {
            name: 'Samsung',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/samsung_Insf8kassvK.webp'
        },
        {
            name: 'Lenovo',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/Lenovo_co1GDjBpxV.webp'
        },
        {
            name: 'Alibaba Cloud',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/Alibaba_kBCmKi7O7.webp'
        },
        {
            name: 'Intel',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/Intel_T_cVZHo81M_t.webp'
        },
        {
            name: 'Dicoding',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/dicoding-header-logo_-yUghUNkx7.webp'
        },
        {
            name: 'IBM',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/IBM_GXVYl92inl905.webp'
        },
        {
            name: 'Pixel',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/pixel_YtPl8tvzu.webp'
        },
        {
            name: 'XL',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/xl_t5bLnDwJn.webp'
        },
        {
            name: 'Hactive8',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/hacktiv8_Y3L2HUj4K4X.webp'
        },
        {
            name: 'Kudo',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/Kudo_oGHxpphEw.webp'
        },
        {
            name: 'Refactory',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/Refactory_VO89Fn5BX.webp'
        },
        {
            name: 'Ajita',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/LogoPartner_WEBP/ajita_Za3H1pSLYI.webp'
        },
    ]);

    return (
        <section className="bg-light">
            <div className="container p-4 px-lg-5 py-lg-2">
                <div className="row my-4">
                    <div className="col">
                        <h3 className="text-muted text-center">Telah Dipercaya Oleh</h3>
                    </div>
                </div>
                <div className="row justify-content-center">
                    {
                        partners.map((partner, index) => {
                            return (
                                <div key={index} className="col-4 col-lg-2">
                                    <div className="card bg-transparent border-0">
                                        <div className="card-body py-1">
                                            <Image src={partner.thumbnail} placeholder="blur" blurDataURL="/assets/img/placeholder.jpg" alt={partner.name} layout="responsive" width={50} height={30} objectFit="contain" />
                                        </div>
                                    </div>
                                </div>
                            )
                        })
                    }
                </div>
            </div>
        </section>
    );
};

export default SectionPartner;
