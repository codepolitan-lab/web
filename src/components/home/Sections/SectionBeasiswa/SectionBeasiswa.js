import { useRef } from 'react';
import styles from './SectionBeasiswa.module.scss';
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

// import Swiper core and required modules
import SwiperCore, { Pagination, Navigation } from "swiper";
import CardBeasiswa from '../../Cards/CardBeasiswa/CardBeasiswa';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronCircleLeft, faChevronCircleRight } from '@fortawesome/free-solid-svg-icons';

// install Swiper modules
SwiperCore.use([Pagination, Navigation]);

const SectionBeasiswa = ({ data }) => {
    const navigationPrevRef = useRef(null);
    const navigationNextRef = useRef(null);

    return (
        <section className={`${styles.section_beasiswa} section`}>
            <div className="container p-4 p-lg-5">
                <div className="row">
                    <div className="col-md-6">
                        <h2 className="section-title">Program<br />Khusus Codepolitan</h2>
                    </div>
                    <div className="col-md-6">
                        <p className="text-muted">Ikuti setiap program kerjasama Codepolitan dengan parter teknologi untuk membantu meningkatkan kemampuanmu sebagai developer profesional</p>
                        <div className="my-2 text-end d-none d-lg-block">
                            <span className="me-2" ref={navigationPrevRef} role="button">
                                <FontAwesomeIcon size="2x" className="text-primary" icon={faChevronCircleLeft} />
                            </span>
                            <span ref={navigationNextRef} role="button">
                                <FontAwesomeIcon size="2x" className="text-primary" icon={faChevronCircleRight} />
                            </span>
                        </div>
                    </div>
                </div>
                <div className="row mt-3">
                    <div className="col">
                        <Swiper
                            spaceBetween={20}
                            grabCursor={true}
                            slidesPerView={1.1}
                            navigation={{
                                prevEl: navigationPrevRef.current,
                                nextEl: navigationNextRef.current,
                            }}
                            onBeforeInit={(swiper) => {
                                swiper.params.navigation.prevEl = navigationPrevRef.current;
                                swiper.params.navigation.nextEl = navigationNextRef.current;
                            }}
                            breakpoints={{
                                // when window width is >= 414px
                                414: {
                                    slidesPerView: 1.1,
                                },
                                // when window width is >= 768px
                                768: {
                                    slidesPerView: 2.1,
                                },
                                1024: {
                                    slidesPerView: 2.1,
                                },
                            }}
                            style={{ paddingBottom: '10px' }}
                        >
                            {data.filter(item => item.status === 'open').map((item, index) => {
                                return (
                                    <SwiperSlide key={index}>
                                        <a className="link" href={item.link}>
                                            <CardBeasiswa
                                                thumbnail={item.image}
                                                title={item.title}
                                                description={item.description}
                                                organizer={item.provider}
                                            />
                                        </a>
                                    </SwiperSlide>
                                );
                            }).reverse()}
                        </Swiper>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default SectionBeasiswa;
