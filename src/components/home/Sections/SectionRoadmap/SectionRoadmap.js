import Link from "next/link";
import { Swiper, SwiperSlide } from "swiper/react";
import CardRoadmap from "../../../global/Cards/CardRoadmap/CardRoadmap";

// import Swiper core and required modules
import SwiperCore, { Navigation } from "swiper";

// install Swiper modules
SwiperCore.use([Navigation]);

const SectionRoadmap = ({ data }) => {
    const roadmaps = data;

    return (
        <section className="section bg-light" id="Swiper">
            <div className="container p-3 p-lg-4" id="course">
                <div className="row">
                    <div className="col-md-6">
                        <h2 className="section-title text-muted ms-lg-4">Beragam Roadmap Belajar</h2>
                    </div>
                    <div className="col-md-6 text-start text-md-end my-auto">
                        <Link href="/roadmap">
                            <a className="text-primary me-lg-4">Lihat Semua</a>
                        </Link>
                    </div>
                </div>
                <div className="row mt-4">
                    <div className="col">
                        <Swiper
                            className="px-lg-4"
                            spaceBetween={25}
                            grabCursor={true}
                            slidesPerView={1.2}
                            navigation
                            breakpoints={{
                                // when window width is >= 414px
                                375: {
                                    slidesPerView: 1.5,
                                },
                                // when window width is >= 768px
                                768: {
                                    slidesPerView: 2.5,
                                },
                                1200: {
                                    slidesPerView: 4,
                                },
                            }}
                        // style={{ paddingLeft: '20px', paddingRight: '20px' }}
                        >
                            {roadmaps.map((roadmap, index) => {
                                return (
                                    <SwiperSlide key={index}>
                                        <Link href={`/roadmap/${roadmap.slug}`}>
                                            <a className="link">
                                                <CardRoadmap
                                                    thumbnail={roadmap.image}
                                                    icon={roadmap.small_icon}
                                                    author={roadmap.author}
                                                    title={roadmap.name}
                                                    level={roadmap.level}
                                                    totalCourse={roadmap.total_course}
                                                    totalStudent={roadmap.total_student}
                                                    rate={roadmap.rate}
                                                />
                                            </a>
                                        </Link>
                                    </SwiperSlide>
                                );
                            }).slice(0, 10)}
                        </Swiper>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default SectionRoadmap;
