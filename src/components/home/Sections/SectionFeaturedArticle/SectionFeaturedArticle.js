import Link from "next/link";
import { Swiper, SwiperSlide } from "swiper/react";
import CardArticle from "../../../home/Cards/CardFeaturedArticle/CardFeaturedArticle";

// import Swiper core and required modules
import SwiperCore, { Navigation } from "swiper";

// install Swiper modules
SwiperCore.use([Navigation]);

const sectionFeaturedArticle = ({ data }) => {
    return (
        <section className="section bg-light" id="Swiper">
            <div className="container p-3 p-lg-4" id="course">
                <div className="row pt-3 pt-lg-5">
                    <div className="col-md-6">
                        <h2 className="section-title text-muted ms-lg-4">Berbagai artikel untuk-mu</h2>
                    </div>
                    <div className="col-md-6 text-start text-md-end my-auto">
                        <Link href="/tutorials">
                            <a className="link text-primary">Lihat Semua</a>
                        </Link>
                    </div>
                </div>
                <div className="row mt-4">
                    <div className="col">
                        <Swiper
                            className="px-lg-4 py-3"
                            spaceBetween={35}
                            grabCursor={true}
                            slidesPerView={1.2}
                            navigation
                            breakpoints={{
                                // when window width is >= 414px
                                375: {
                                    slidesPerView: 1.5,
                                },
                                // when window width is >= 768px
                                768: {
                                    slidesPerView: 2.5,
                                },
                                1200: {
                                    slidesPerView: 4,
                                },
                            }}
                        // style={{ paddingLeft: '20px', paddingRight: '20px' }}
                        >
                            {
                                data?.map((article, index) => {
                                    return (
                                        <SwiperSlide key={index}>
                                            <Link href={`https://codepolitan.com/blog/${article.slug}`}>
                                                <a className="link">
                                                    <CardArticle
                                                        title={article.title}
                                                        thumbnail={article.featured_image}
                                                        date={article.created_at}
                                                        writer={article.writer}
                                                        shortDesc={article.excerpt}
                                                    />
                                                </a>
                                            </Link>
                                        </SwiperSlide>
                                    )
                                })
                            }
                        </Swiper>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default sectionFeaturedArticle;

