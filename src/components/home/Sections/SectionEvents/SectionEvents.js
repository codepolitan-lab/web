import Link from "next/link";
import { Swiper, SwiperSlide } from "swiper/react";

// import Swiper core and required modules
import SwiperCore, { Navigation } from "swiper";
import CardEvent from "../../../events/Cards/CardEvent/CardEvent";

// install Swiper modules
SwiperCore.use([Navigation]);

const SectionEvents = ({ data }) => {
    return (
        <section className="section" id="Swiper" style={{ background: `url('https://image.web.id/images/event-bg.png') center center no-repeat`, backgroundSize: 'cover' }}>
            <div className="container p-3 p-lg-5" id="events">
                <div className="row">
                    <div className="col-md-6">
                        <h2 className="section-title text-white ms-lg-4">Event Terbaru CODEPOLITAN</h2>
                    </div>
                    <div className="col-md-6 text-start text-md-end my-auto">
                        <Link href="/events">
                            <a className="link text-white">Lihat Semua</a>
                        </Link>
                    </div>
                </div>
                <div className="row mt-4">
                    <div className="col">
                        <Swiper
                            className="px-lg-4 py-3"
                            spaceBetween={35}
                            grabCursor={true}
                            slidesPerView={1.2}
                            navigation
                            breakpoints={{
                                // when window width is >= 414px
                                375: {
                                    slidesPerView: 1.5,
                                },
                                // when window width is >= 768px
                                768: {
                                    slidesPerView: 2.5,
                                },
                                1200: {
                                    slidesPerView: 4,
                                },
                            }}
                        >
                            {
                                data?.map((event, index) => {
                                    return (
                                        <SwiperSlide key={index}>
                                            <Link href={`/events/${event.slug}`}>
                                                <a className="link">
                                                    <CardEvent
                                                        type={event.webinar_type}
                                                        thumbnail={event.featured_image}
                                                        title={event.title}
                                                        schedule={event.scheduled_at}
                                                        location={event.location}
                                                    />
                                                </a>
                                            </Link>
                                        </SwiperSlide>
                                    )
                                })
                            }
                        </Swiper>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default SectionEvents;

