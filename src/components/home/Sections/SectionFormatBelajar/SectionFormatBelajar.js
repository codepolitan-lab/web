import { useState } from 'react';

const SectionFormatBelajar = () => {
    const [image, setImage] = useState('/assets/img/home/img-course.webp');

    return (
        <section className="section bg-light">
            <div className="container p-4 p-lg-5">
                <div className="row">
                    <div className="col-lg-6 order-last order-lg-first mt-3 mt-lg-0">
                        <h2 className="section-title">Beragam Format Belajar</h2>
                        <p className="text-muted">Belajar menyenangkan bersama Codepolitan</p>
                        <div className="accordion accordion-flush" id="FormatBelajarAccordion">
                            <div className="accordion-item bg-light">
                                <h2 className="accordion-header" id="flush-headingOne">
                                    <button onClick={() => setImage('/assets/img/home/img-course.webp')} className="accordion-button px-0" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="true" aria-controls="flush-collapseOne">
                                        <strong>Online Course</strong>
                                    </button>
                                </h2>
                                <div id="flush-collapseOne" className="accordion-collapse collapse show" aria-labelledby="flush-headingOne" data-bs-parent="#FormatBelajarAccordion">
                                    <div className="accordion-body px-0 text-muted">Materi belajar tersaji dalam bentuk video tutorial yang ringkas dan runut. Tersedia juga kuis untuk memvalidasi pemahaman belajarmu</div>
                                    <img className="img-fluid d-block d-lg-none mx-auto" src="/assets/img/home/img-course.webp" loading="lazy" alt="Codepolitan" />
                                </div>
                            </div>
                            <div className="accordion-item bg-light">
                                <h2 className="accordion-header" id="flush-headingTwo">
                                    <button onClick={() => setImage('/assets/img/home/img-webinar.webp')} className="accordion-button px-0 collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                                        <strong>Webinar</strong>
                                    </button>
                                </h2>
                                <div id="flush-collapseTwo" className="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#FormatBelajarAccordion">
                                    <div className="accordion-body px-0 text-muted">Belajar langsung secara realtime bersama mentor di jadwal yang telah ditentukan. Kamu bisa langsung bertanya soal permasalahanmu selama belajar</div>
                                    <img className="img-fluid d-block d-lg-none mx-auto" src="/assets/img/home/img-webinar.webp" loading="lazy" alt="Codepolitan" />
                                </div>
                            </div>
                            <div className="accordion-item bg-light">
                                <h2 className="accordion-header" id="flush-headingThree">
                                    <button onClick={() => setImage('/assets/img/home/img-tutorial.webp')} className="accordion-button px-0 collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
                                        <strong>Tutorial</strong>
                                    </button>
                                </h2>
                                <div id="flush-collapseThree" className="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#FormatBelajarAccordion">
                                    <div className="accordion-body px-0 text-muted">Cocok buat kamu yang ingin mempercepat proses belajar dengan mengikuti instruksional dalam bentuk teks yang ringkas dan praktis</div>
                                    <img className="img-fluid d-block d-lg-none mx-auto" src="/assets/img/home/img-tutorial.webp" loading="lazy" alt="Codepolitan" />
                                </div>
                            </div>
                            <div className="accordion-item bg-light">
                                <h2 className="accordion-header" id="flush-headingFour">
                                    <button onClick={() => setImage('/assets/img/home/img-podcast.webp')} className="accordion-button px-0 collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFour" aria-expanded="false" aria-controls="flush-collapseFour">
                                        <strong>Podcast</strong>
                                    </button>
                                </h2>
                                <div id="flush-collapseFour" className="accordion-collapse collapse" aria-labelledby="flush-headingFour" data-bs-parent="#FormatBelajarAccordion">
                                    <div className="accordion-body px-0 text-muted">Sempurnakan pola pikir dan pemahamanmu dengan penjelasan audio yang dapat didengarkan sambil menyantap makan siang</div>
                                    <img className="img-fluid d-block d-lg-none mx-auto" src="/assets/img/home/img-podcast.webp" loading="lazy" alt="Codepolitan" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-5 offset-lg-1 my-auto">
                        <img className="img-fluid d-none d-lg-block" src={image} loading="lazy" alt="Codepolitan" />
                    </div>
                </div>
            </div>
        </section>
    );
};

export default SectionFormatBelajar;
