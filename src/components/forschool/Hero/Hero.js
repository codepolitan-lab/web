import styles from './Hero.module.scss';

const Hero = () => {
    return (
        <section className={`${styles.hero}`}>
            <div className="container p-4 p-lg-5">
                <div className="row">
                    <div className="col-lg-7 text-white text-center text-lg-start my-auto">
                        <h1 className={styles.hero_title}>Persiapkan Siswa dengan Keterampilan Industri 4.0</h1>
                        <p className="lead text-muted">Program yang dikembangkan khusus untuk membantu generasi muda belajar dan berlatih pemrograman agar siswa siap terjun dan bersaing di dunia kerja dalam industri 4.0</p>
                        <a className="btn btn-primary p-3 my-3" href="#counter">Pelajari Selengkapnya</a>
                    </div>
                    <div className="col-lg-5">
                        <img className="img-fluid" src="/assets/img/forschool/hero-forschool.png" alt="Hero" />
                    </div>
                </div>
            </div>
        </section>
    );
};

export default Hero;
