import { useState } from 'react';

const SectionFormatBelajar = () => {
    const [image, setImage] = useState('/assets/img/forschool/img-forschool-1.png');

    return (
        <section className="section bg-light">
            <div className="container p-4 p-lg-5">
                <div className="row mb-5">
                    <div className="col text-center">
                        <h2 className="section-title">Tingkatkan Kualitas Lulusan IT Anda</h2>
                        <p className="text-muted">Lengkapi skill dan pengetahuan lulusan sekolah/kampus Anda melalui program komprehensif dari kami</p>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-6 order-last order-lg-first mt-3 mt-lg-0">
                        <div className="accordion accordion-flush" id="FormatBelajarAccordion">
                            <div className="accordion-item bg-light">
                                <h2 className="accordion-header" id="flush-headingOne">
                                    <button onClick={() => setImage('/assets/img/forschool/img-forschool-1.png')} className="accordion-button px-0" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="true" aria-controls="flush-collapseOne">
                                        <strong>Kurikulum Berstandar SKKNI</strong>
                                    </button>
                                </h2>
                                <div id="flush-collapseOne" className="accordion-collapse collapse show" aria-labelledby="flush-headingOne" data-bs-parent="#FormatBelajarAccordion">
                                    <div className="accordion-body px-0 text-muted">Siswa akan belajar secara online dengan kurikulum yang mengacu pada Standar Kompetensi Kerja Nasional Indonesia (SKKNI) berdasarkan jalur profesi Junior Web Developer.</div>
                                    <img className="img-fluid d-block d-lg-none mx-auto" src="/assets/img/forschool/img-forschool-1.png" alt="Codepolitan" />
                                </div>
                            </div>
                            <div className="accordion-item bg-light">
                                <h2 className="accordion-header" id="flush-headingTwo">
                                    <button onClick={() => setImage('/assets/img/forschool/img-forschool-2.png')} className="accordion-button px-0 collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                                        <strong>Webinar Session Bersama Pakar</strong>
                                    </button>
                                </h2>
                                <div id="flush-collapseTwo" className="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#FormatBelajarAccordion">
                                    <div className="accordion-body px-0 text-muted">Siswa bisa mengikuti program webinar yang diselenggarakan setiap bulan untuk mendapatkan informasi terkait dunia teknologi dan industri dalam bidang pemrograman dan teknologi</div>
                                    <img className="img-fluid d-block d-lg-none mx-auto" src="/assets/img/forschool/img-forschool-2.png" alt="Codepolitan" />
                                </div>
                            </div>
                            <div className="accordion-item bg-light">
                                <h2 className="accordion-header" id="flush-headingThree">
                                    <button onClick={() => setImage('/assets/img/forschool/img-forschool-3.png')} className="accordion-button px-0 collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
                                        <strong>Tingkatkan Kapasitas Siswa Dengan Studi Kasus</strong>
                                    </button>
                                </h2>
                                <div id="flush-collapseThree" className="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#FormatBelajarAccordion">
                                    <div className="accordion-body px-0 text-muted">Tidak hanya membahas teori, disini siswa juga akan belajar bagaimana caranya menggunakan teknologi yang sedang dipelajarinya dalam sebuah proyek nyata.</div>
                                    <img className="img-fluid d-block d-lg-none mx-auto" src="/assets/img/forschool/img-forschool-3.png" alt="Codepolitan" />
                                </div>
                            </div>
                            <div className="accordion-item bg-light">
                                <h2 className="accordion-header" id="flush-headingFour">
                                    <button onClick={() => setImage('/assets/img/forschool/img-forschool-4.png')} className="accordion-button px-0 collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFour" aria-expanded="false" aria-controls="flush-collapseFour">
                                        <strong>Perkaya Pengalaman Siswa dengan Magang</strong>
                                    </button>
                                </h2>
                                <div id="flush-collapseFour" className="accordion-collapse collapse" aria-labelledby="flush-headingFour" data-bs-parent="#FormatBelajarAccordion">
                                    <div className="accordion-body px-0 text-muted">Bagi siswa yang mampu menyelesaikan program dengan baik dan berhasil lulus seleksi akan berkesempaan mengikuti program magang Super Bootcamp dan bimbingan kari di CodePolitan selama 2 bulan. GRATIS</div>
                                    <img className="img-fluid d-block d-lg-none mx-auto" src="/assets/img/forschool/img-forschool-4.png" alt="Codepolitan" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-5 offset-lg-1 my-auto">
                        <img className="img-fluid d-none d-lg-block" src={image} alt="Codepolitan" />
                    </div>
                </div>
            </div>
        </section>
    );
};

export default SectionFormatBelajar;
