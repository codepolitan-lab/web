const SectionTechnology = () => {
    const technologies = [
        {
            id: 1,
            thumbnail: '/assets/img/program/icons/box-icons/icon-html5.png',
            title: 'HTML & CSS',
            description: 'Belajar dari dasar hingga mahir',
        },
        {
            id: 2,
            thumbnail: '/assets/img/program/icons/box-icons/icon-bootstrap.png',
            title: 'Bootstrap',
            description: 'Belajar dari dasar hingga mahir',
        },
        {
            id: 3,
            thumbnail: '/assets/img/program/icons/box-icons/icon-mysql.png',
            title: 'MySQL',
            description: 'Belajar dari dasar hingga mahir',
        },
        {
            id: 4,
            thumbnail: '/assets/img/program/icons/box-icons/icon-php.png',
            title: 'PHP',
            description: 'Belajar dari dasar hingga mahir',
        },
        {
            id: 5,
            thumbnail: '/assets/img/program/icons/box-icons/icon-codeigniter.png',
            title: 'Codeigniter',
            description: 'Belajar dari dasar hingga mahir',
        },
        {
            id: 6,
            thumbnail: '/assets/img/program/icons/box-icons/icon-laravel.png',
            title: 'Laravel',
            description: 'Belajar dari dasar hingga mahir',
        },
        {
            id: 9,
            thumbnail: '/assets/img/program/icons/box-icons/icon-jquery.png',
            title: 'JQuery',
            description: 'Belajar dari dasar hingga mahir',
        },
        {
            id: 10,
            thumbnail: '/assets/img/program/icons/box-icons/icon-js.png',
            title: 'JavaScript',
            description: 'Belajar dari dasar hingga mahir',
        },
        {
            id: 12,
            thumbnail: '/assets/img/program/icons/box-icons/icon-vue.png',
            title: 'Vue',
            description: 'Belajar dari dasar hingga mahir',
        },
    ];

    return (
        <section className="section bg-light">
            <div className="container p-4 p-lg-5">
                <div className="row my-3">
                    <div className="col text-center">
                        <h2 className="section-title">Teknologi Yang Dapat Dipelajari</h2>
                        <p className="text-muted">Ikuti Jalur belajar menjadi proffesional di dunia kerja seperti</p>
                    </div>
                </div>
                <div className="row">
                    {technologies.map((tech) => {
                        return (
                            <div className="col-md-4 mb-3" key={tech.id}>
                                <div className="card border-0 shadow-sm" style={{ borderRadius: '15px' }}>
                                    <div className="card-body p-3">
                                        <div className="row text-muted">
                                            <div className="col-4 my-auto">
                                                <img className="img-fluid rounded" src={tech.thumbnail} alt={tech.title} />
                                            </div>
                                            <div className="col-8 mt-2">
                                                <h5 style={{ fontSize: 'medium' }}>{tech.title}</h5>
                                                <p style={{ fontSize: 'small' }}>{tech.description}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        );
                    })}
                </div>
            </div>
        </section>
    );
};

export default SectionTechnology;