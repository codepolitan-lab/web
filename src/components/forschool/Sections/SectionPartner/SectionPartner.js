const SectionPartner = () => {
    const partners = [
        {
            name: 'SMKN 4 Kendal',
            thumbnail: 'https://i.ibb.co/VTkGSGv/smkn4.webp'
        },
        {
            name: 'Wikrama',
            thumbnail: 'https://i.ibb.co/2qdszLv/wikrama.webp'
        },
        {
            name: 'AMIK Bumi Nusantara Cirebon',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/MitraKampus_WEBP/logo-amik-bumi-nusantara_NcvdNtyCx.webp'
        },
        {
            name: 'UPI',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/MitraKampus_WEBP/logo-upi_Za_sjehLv.webp'
        },
        {
            name: 'USTJ',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/MitraKampus_WEBP/logo-ustj_nxp-XxsM-.webp'
        },
        {
            name: 'UII',
            thumbnail: 'https://ik.imagekit.io/6ix6n7mhslj/MitraKampus_WEBP/Logo-UII-Asli_PhHNmKFQx_GBW.webp'
        }
    ];

    return (
        <section className="section">
            <div className="container p-4 p-lg-5">
                <div className="row mb-5">
                    <div className="col text-center">
                        <h2 className="section-title">Telah Dipercaya Oleh</h2>
                    </div>
                </div>
                <div className="row justify-content-center my-3">
                    {partners.map((partner, index) => {
                        return (
                            <div className="col-4 col-md-2 my-2" key={index}>
                                <div className="card bg-transparent border-0">
                                    <div className="card-body p-0">
                                        <img className="img-fluid d-block mx-auto" src={partner.thumbnail} alt={partner.name} />
                                    </div>
                                </div>
                            </div>
                        );
                    })}
                </div>
            </div>
        </section>
    );
};

export default SectionPartner;