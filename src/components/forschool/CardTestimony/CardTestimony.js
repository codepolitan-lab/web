const CardTestimony = ({ name, role, thumbnail, organization, content }) => {
    return (
        <div className="card border-0 shadow-sm h-100">
            <div className="card-body p-4 p-lg-5">
                <h5>{organization}</h5>
                <p className="text-muted">{content}</p>
            </div>
            <div className="card-footer bg-white px-4 px-lg-5">
                <div className="row">
                    <div className="col-3">
                        <img className="img-fluid rounded-circle" src={thumbnail || '/assets/img/placeholder.jpg'} alt={role} />
                    </div>
                    <div className="col-auto my-auto">
                        <strong>{name || 'Belum ada nama'}</strong>
                        <p className="text-muted">{role || 'Belum ada role'}</p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default CardTestimony;
