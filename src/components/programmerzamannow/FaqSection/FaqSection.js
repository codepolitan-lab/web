import { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faWhatsapp } from "@fortawesome/free-brands-svg-icons";

const FaqSection = () => {
  const [data] = useState([
    {
      id: 'flush-heading1',
      target: 'flush-collapse1',
      title: 'Bagaimana cara belajarnya?',
      content: 'Cara belajar di program ini bersifat online. Kamu akan mendapatkan materi belajar berupa video pembelajaran yang bisa kamu pelajari dan praktekan. Materi tersebut bisa kamu pelajari kapan pun, sesuai dengan waktu yang kamu miliki. Kamu cukup meluangkan waktu 2 jam sehari untuk bisa mendapatkan hasil maksimal dari program ini.'
    },
    {
      id: 'flush-heading2',
      target: 'flush-collapse2',
      title: 'Bagaimana cara pembayarannya?',
      content: '<p>Ada 2 metode pembayaran yang bisa kamu pilih yaitu:</p> <ol><li>Manual, pada metode ini kamu akan dikirimkan nomor rekening dan juga biaya yang harus kamu transfer pada nomor rekening tersebut. Setelah itu kamu harus mengirimkan bukti transfer pada link yang tertera. Kamu akan menerima notifikasi bila pembayaran kamu sudah diterima.</li><li>Otomatis, pada metode ini kamu akan dikirimkan nomor virtual account (VA), lalu kamu harus mentransfer pada nomor VA tersebut. Kamu akan menerima notifikasi bila pembayaran kamu sudah diterima</li></ol>'
    },
    {
      id: 'flush-heading3',
      target: 'flush-collapse3',
      title: 'Kalau saya ada pertanyaan, kemana saya harus bertanya?',
      content: 'Bila kamu memiliki pertanyaan terkait materi yang kamu pelajari, kamu bisa menanyakannya pada forum CodePolitan yang tertera di bawah materi yang sedang kamu pelajari'
    },
    {
      id: 'flush-heading4',
      target: 'flush-collapse4',
      title: 'Apa yang dimaksud dengan garansi?',
      content: 'Bila pada 3 hari pertama setelah pembayaran kamu diterima dan mencoba kelas yang ada di CodePolitan kamu merasa tidak cocok dengan materi yang dimiliki, kamu bisa klaim pengembalian uang pendaftaran'
    },
    {
      id: 'flush-heading5',
      target: 'flush-collapse5',
      title: 'Bagaimana cara klaim garansinya?',
      content: 'Kamu bisa menghubungi nomor CS CodePolitan, lalu laporkan kalau kamu ingin mengajukan garansi'
    },
    {
      id: 'flush-heading6',
      target: 'flush-collapse6',
      title: 'Apakah video bisa ditonton ulang setelah habis masa subscribe',
      content: 'Tidak bisa. Setelah masa membership kamu habis kelas akan otomatis terkunci kembali.'
    },
    {
      id: 'flush-heading7',
      target: 'flush-collapse7',
      title: 'Apakah akan ada pembaharuan kelas?',
      content: 'Iya, kelas akan terus di update. Harga membership juga akan disesuaikan setiap ada pembaharuan materi'
    },
    {
      id: 'flush-heading8',
      target: 'flush-collapse8',
      title: 'Bagaimana jika kelas belum selesai tapi masa subscribe sudah berakhir?',
      content: 'Kamu tidak bisa mengakses kelas lagi. Tetapi, jangan khawatir, learning progress kamu akan tetap tersimpan'
    },
    {
      id: 'flush-heading9',
      target: 'flush-collapse9',
      title: 'Apakah akan disalurkan kerja',
      content: 'Untuk saat ini belum, tapi melalui program Talent Hub Codepolitan kamu akan berpotensi terhubung dengan partner Codepolitan yang membutuhkan talent. Jika profile dan skill kamu sesuai dengan kebutuhan mereka, maka kamu akan punya kesempatan direkrut. Untuk itu, pastikan kamu bergabung dalam program Talent Hub Codepolitan segera setelah bergabung dalam program ini.'
    },
    {
      id: 'flush-heading10',
      target: 'flush-collapse10',
      title: 'Saya masih terdaftar sebagai member premium di paket reguler, apakah saya bisa akses ke kelas PZN juga?',
      content: 'Tidak bisa, karena membership reguler dan membership kelas PZN berbeda'
    },
    {
      id: 'flush-heading11',
      target: 'flush-collapse11',
      title: 'Apa perbedaan membership pada kelas PZN dan reguler?',
      content: 'Pada membership reguler , kamu bisa akses semua kelas yang disediakan oleh CodePolitan kecuali yang dibuat oleh Eko Khannedy. Sedangkan pada membership PZN, kamu bisa akses seluruh kelas PZN buatan Eko Khannedy, tetapi tidak bisa akses kelas CodePolitan yang lain.'
    },
    {
      id: 'flush-heading12',
      target: 'flush-collapse12',
      title: 'Apakah saya bisa terdaftar pada dua membership tersebut secara bersamaan?',
      content: 'Bisa, kamu bisa terdaftar pada membership reguler sekaligus membership PZN dalam waktu yang sama :)'
    },
    {
      id: 'flush-heading13',
      target: 'flush-collapse13',
      title: 'Materi apa saja yang akan saya pelajari pada program ini?',
      content: 'Untuk saat ini terdapat terdapat 6 roadmap yang didalamnya 41 kelas yang bisa kamu pelajari, 6 roadmap tersebut adalah: Roadmap Java, JavaScript, Go-Lang, PHP, MySQL, dan Kotlin'
    },
    {
      id: 'flush-heading14',
      target: 'flush-collapse14',
      title: 'Berapa biaya yang harus saya bayar untuk mengikuti program ini?',
      content: 'Harganya beragam, sesuai dengan lama membership yang kamu butuhkan. Harganya bisa kamu lihat di bagian harga pada halaman ini.'
    },
    {
      id: 'flush-heading15',
      target: 'flush-collapse15',
      title: 'Apakah saya akan mendapatkan sertifikat setelah menyelesaikan materi belajar?',
      content: 'Iya, setelah beres mempelajari setiap kelasnya kamu akan mendapatkan sertifikat'
    },
    {
      id: 'flush-heading16',
      target: 'flush-collapse16',
      title: 'Apakah video tutorial bisa di download?',
      content: 'Seluruh video di CodePolitan tidak bisa di download, hanya bisa ditonton secara streaming :)'
    },
  ]);

  return (
    <section className="section">
      <div className="container p-4 p-lg-5 my-5">
        <div className="row flex-lg-row-reverse">
          <div className="col-lg-8 mb-5">
            <h2
              className="section-title d-lg-none mb-5"
              style={{ color: "#14a7a0", fontSize: "2rem" }}
            >
              Frequently <br /> Asked Questions
            </h2>
            <div
              className="accordion accordion-flush"
              id="accordionFlushPZN"
            >
              {data.map((item, index) => {
                return (
                  <div className="accordion-item" key={index}>
                    <h3 className="accordion-header" id={item.id}>
                      <button
                        className="accordion-button collapsed"
                        type="button"
                        data-bs-toggle="collapse"
                        data-bs-target={`#${item.target}`}
                        aria-expanded="false"
                        aria-controls={item.target}
                      >
                        <strong className="text-muted">{item.title}</strong>
                      </button>
                    </h3>
                    <div
                      id={item.target}
                      className="accordion-collapse collapse"
                      aria-labelledby={item.id}
                      data-bs-parent="#accordionFlushPZN"
                    >
                      <div className="accordion-body text-muted">
                        <div dangerouslySetInnerHTML={{ __html: item.content }} />
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
          <div className="col-lg-4 d-none d-lg-block border-end border-2 text-center text-lg-start">
            <h2 className="section-title">
              Masih Memiliki Pertanyaan?
              <br />
              <span style={{ color: "#14a7a0" }}>Lihat disini</span>
            </h2>
            <a className="btn btn-primary px-4 py-2 mt-3" href="https://wa.me/628999488990" target="_blank" rel="noopener noreferrer">
              <FontAwesomeIcon className="me-2" icon={faWhatsapp} />
              Tanyakan disini
            </a>
          </div>
          <div className="col-lg-4 d-lg-none text-center text-lg-start">
            <h2 className="section-title">
              Masih Memiliki Pertanyaan?
              <br />
              <span style={{ color: "#14a7a0" }}>Lihat disini</span>
            </h2>
            <a className="btn btn-primary px-4 py-2 mt-3" href="https://wa.me/628999488990" target="_blank" rel="noopener noreferrer">
              <FontAwesomeIcon className="me-2" icon={faWhatsapp} />
              Tanyakan disini
            </a>
          </div>
        </div>
      </div>
    </section>
  );
};

export default FaqSection;
