import Link from 'next/link';
import RoadmapCard from '../RoadmapCard/RoadmapCard';

const RoadmapSection = () => {
	return (
		<section className="section bg-light">
			<div className="container p-5">
				<div className="row">
					<div className="col-md-6 text-center text-md-start">
						<h2 className="section-title">Roadmap Belajar</h2>
					</div>
					<div className="col-md-6 text-center text-md-end">
						<Link href="/programmerzamannow/roadmap">
							<a className="btn text-primary rounded-pill mt-3 mt-md-0" style={{ backgroundColor: '#eee', fontWeight: 'bold' }}>Lihat Selengkapnya</a>
						</Link>
					</div>
				</div>
				<div className="row mt-5">
					<div className="col-md-6 col-lg-3 mb-3">
						<RoadmapCard
							thumbnail="/assets/img/programmerzamannow/logos/logo-golang.png"
							title="Jagoan Golang"
							description="Pemrograman Golang dari pemula sampai mahir"
							link="/programmerzamannow/roadmap/12"
						/>
					</div>
					<div className="col-md-6 col-lg-3 mb-3">
						<RoadmapCard
							thumbnail="/assets/img/programmerzamannow/logos/logo-js.png"
							title="Jagoan JavaScript"
							description="Pemrograman JavaScript dari pemula sampai mahir"
							link="/programmerzamannow/roadmap/13"
						/>
					</div>
					<div className="col-md-6 col-lg-3 mb-3">
						<RoadmapCard
							thumbnail="/assets/img/programmerzamannow/logos/logo-php.png"
							title="Jagoan PHP"
							description="Pemrograman PHP dari pemulai sampai mahir"
							link="/programmerzamannow/roadmap/15"
						/>
					</div>
					<div className="col-md-6 col-lg-3 mb-3">
						<RoadmapCard
							thumbnail="/assets/img/programmerzamannow/logos/logo-kotlin.png"
							title="Jagoan Kotlin"
							description="Pemrograman Kotlin dari pemula sampai mahir"
							link="/programmerzamannow/roadmap/14"
						/>
					</div>
				</div>
			</div>
		</section>
	);
};

export default RoadmapSection;
