import { formatPrice } from '../../../utils/helper';
import styles from './PriceCard.module.scss';

const PriceCard = ({ checked, title, subtitle, badgeText, delPrice, price }) => {
    return (
        <div className={`card mb-3 ${styles.card} ${checked === true && styles.active}`}>
            <div className="card-body">
                <div className="row">
                    <div className="col-1 m-auto p-0 p-md-2">
                        {checked ? <img className={styles.card_icon} src="/assets/img/icons/icon-checked.png" alt="Checked" /> : <img className={styles.card_icon} src="/assets/img/icons/icon-uncheck.png" alt="Unchecked" />}
                    </div>
                    <div className="col-6 col-md-7 m-auto">
                        <h5 className={styles.card_title}>{title}</h5>
                        <p className="mb-1">{subtitle}</p>
                        <span className={`${styles.badge} badge rounded-pill`}>{badgeText}</span>
                    </div>
                    <div className="col-4 m-auto p-0 p-md-2 text-end">
                        <del>Rp. {formatPrice(delPrice)}</del>
                        <p className={styles.price}><span className={styles.rp}>Rp.</span>{formatPrice(price)}</p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default PriceCard;
