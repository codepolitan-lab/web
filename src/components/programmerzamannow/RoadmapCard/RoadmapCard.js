const RoadmapCard = ({ thumbnail, title, description, link }) => {
    return (
        <div className="card border-0 shadow-sm" style={{ borderRadius: '15px' }}>
            <div className="card-body">
                <img style={{ borderRadius: '15px' }} className="card-img img-fluid" src={thumbnail || '/assets/img/placeholder.jpg'} alt={title} />
                <div className="mt-3">
                    <h5>{title || 'Belum ada judul'}</h5>
                    <p className="mt-2 text-muted">{description || 'Belum ada deskripsi'}</p>
                    <a className="btn btn-primary d-block" href={link}>Buka Roadmap</a>
                </div>
            </div>
        </div>
    );
};

export default RoadmapCard;
