import TabSidebarContent from './TabSidebarContent';

const SidebarCard = () => {
    return (
        <div className="card border-0 sticky-top shadow-sm" style={{ top: '100px', borderRadius: '10px' }}>
            <div className="card-body p-0">
                <div id="sidebarPzn">
                    <ul className="nav nav-tabs nav-justified bg-light mb-1" id="pills-tab" role="tablist">
                        <li className="nav-item" role="presentation">
                            <button className="nav-link active" id="pills-bulanan-tab" data-bs-toggle="pill" data-bs-target="#pills-bulanan" type="button" role="tab" aria-controls="pills-bulanan" aria-selected="true">Pilih Paket</button>
                        </li>
                    </ul>
                    <div className="tab-content" id="pills-tabContent">
                        <div className="tab-pane  show active" id="pills-bulanan" role="tabpanel" aria-labelledby="pills-bulanan-tab">
                            <div id="tabSidebarPzn" className="py-2">
                                <ul className="nav nav-pills nav-justified mx-2" id="pills-tab" role="tablist">
                                    <li className="nav-item" role="presentation">
                                        <button className="nav-link rounded-pill active" id="pills-satubulan-tab" data-bs-toggle="pill" data-bs-target="#pills-satubulan" type="button" role="tab" aria-controls="pills-satubulan" aria-selected="true">1 Bulan</button>
                                    </li>
                                    <li className="nav-item" role="presentation">
                                        <button className="nav-link rounded-pill" id="pills-tigabulan-tab" data-bs-toggle="pill" data-bs-target="#pills-tigabulan" type="button" role="tab" aria-controls="pills-tigabulan" aria-selected="false">4 Bulan</button>
                                    </li>
                                    <li className="nav-item" role="presentation">
                                        <button className="nav-link rounded-pill" id="pills-enambulan-tab" data-bs-toggle="pill" data-bs-target="#pills-enambulan" type="button" role="tab" aria-controls="pills-enambulan" aria-selected="false">6 Bulan</button>
                                    </li>
                                </ul>
                                <div className="tab-content" id="pills-tabContent">
                                    <div className="tab-pane show active" id="pills-satubulan" role="tabpanel" aria-labelledby="pills-satubulan-tab">
                                        <TabSidebarContent
                                            retailPrice="129.000"
                                            link="programmer-zaman-now-cicilan"
                                        />
                                    </div>
                                    <div className="tab-pane" id="pills-tigabulan" role="tabpanel" aria-labelledby="pills-tigabulan-tab">
                                        <TabSidebarContent
                                            normalPrice="516.000"
                                            retailPrice="349.000"
                                            discountPrice="167.000"
                                            link="programmer-zaman-now-ngebut"
                                        />
                                    </div>
                                    <div className="tab-pane" id="pills-enambulan" role="tabpanel" aria-labelledby="pills-enambulan-tab">
                                        <TabSidebarContent
                                            normalPrice="774.000"
                                            retailPrice="499.000"
                                            discountPrice="275.000"
                                            link="programmer-zaman-now-reguler"
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    );
};

export default SidebarCard;
