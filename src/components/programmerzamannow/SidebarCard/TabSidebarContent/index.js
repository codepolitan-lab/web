import { faCheckCircle } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styles from '../SidebarCard.module.scss';

const TabSidebarContent = ({ normalPrice, retailPrice, discountPrice, link }) => {
	return (
		<div>
			<div className="p-2 text-muted text-center">
				{normalPrice && <del className={styles.del}>Rp. {normalPrice}</del>}
				<span className={styles.price}>
					<span className={styles.rp}>Rp.</span>{retailPrice}
				</span>
			</div>
			{discountPrice && <span className="badge bg-pink" style={{ borderTopLeftRadius: 0, borderBottomLeftRadius: 0 }}>Hemat Rp. {discountPrice}</span>}
			<div className="p-2 text-muted" style={{ fontSize: 'smaller' }}>
				<div className="d-flex align-items-start">
					<FontAwesomeIcon className="my-auto me-2 text-primary" icon={faCheckCircle} />
					<span>Klaim Sertifikat Digital</span>
				</div>
				<div className="d-flex align-items-start">
					<FontAwesomeIcon className="my-auto me-2 text-primary" icon={faCheckCircle} />
					<span>Forum Diskusi Tanya Jawab</span>
				</div>
				<div className="d-flex align-items-start">
					<FontAwesomeIcon className="my-auto me-2 text-primary" icon={faCheckCircle} />
					<span>Fitur Lowongan Kerja Talent Hub</span>
				</div>
			</div>
			<div className="d-grid gap-2 p-2">
				<a className="btn btn-primary" href={`https://pay.codepolitan.com/?slug=${link}`}>Beli Sekarang</a>
			</div>
		</div>
	);
};

export default TabSidebarContent;
