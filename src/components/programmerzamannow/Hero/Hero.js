import styles from './Hero.module.scss';

const Hero = () => {
    return (
        <section className={`${styles.hero} bg-light`}>
            <div className="container p-5">
                <div className="row mt-5 text-center text-md-start">
                    <div className="col-lg-7 mt-5 text-muted" style={{ zIndex: 1 }}>
                        <h1 className={styles.hero_title}>Kursus Coding Online Programmer Zaman Now Selama 1 Semester</h1>
                        <p className="lead my-3">Saya Eko Kurniawan Khannedy, Technical Architect di Blibli.com. Dengan pengalaman saya lebih dari 15 tahun berkarya dan 11 tahun berkarir di bidang programming akan membantu kamu belajar coding lebih cepat dan terarah untuk menjadi Programmer Zaman Now</p>
                        <a className="btn btn-outline-secondary btn-lg mt-3" href="#about">Lihat Selengkapnya</a>
                    </div>
                    <div className="col-lg-5 mt-5 mt-md-0">
                        <img className="img-fluid" src="/assets/img/programmerzamannow/eko.png" alt="Kursus Coding Online Programmer Zaman Now Selama 1 Semester" />
                    </div>
                </div>
            </div>
        </section>
    );
};

export default Hero;
