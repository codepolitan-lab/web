import styles from './HeroRoadmap.module.scss';

const HeroRoadmap = ({ title, subtitle, thumbnail }) => {
    return (
        <section className={`${styles.hero} bg-light-blue`}>
            <div className="container p-5">
                <div className="row">
                    <div className="col-lg-8">

                        <div className="row">
                            <div className="col-lg-4">
                                <img style={{ borderRadius: '15px' }} className="img-fluid" src={thumbnail || '/assets/img/placeholder.jpg'} alt={title} />
                            </div>
                            <div className="col mt-5 mt-lg-0">
                                <h1>{title}</h1>
                                <p className="text-muted">{subtitle}</p>
                                <a className="btn btn-outline-primary" href="#buy">Beli Roadmap</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    );
};

export default HeroRoadmap;
