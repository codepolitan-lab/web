import styles from './CtaSection.module.scss';

const CtaSection = ({children}) => {
    return (
        <section className={`${styles.section} section`}>
		    <div className="container p-5">
			    <div className="row justify-content-center">
					<div className="col-md-8">
                        <h3 className={`${styles.title}`}>Ingin Yang Lebih Irit ? Cukup Gabung Membership Kamu Dapat Mengakses Semua Roadmap Belajar Programmer Zaman Now!</h3>
					</div>
				</div>
                <div className="row">
                    <div className="col text-center">
                        {children}
                    </div>
                </div>
			</div>
		</section>
    );
};

export default CtaSection;
