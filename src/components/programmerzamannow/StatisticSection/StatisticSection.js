import CountUp from 'react-countup';
import styles from './StatisticSection.module.scss';

const StatisticSection = () => {
    return (
        <section className="bg-light">
            <div className="container px-5 pb-3">
                <div className="row justify-content-around text-center">
                    <div className="col-6 col-lg-3">
                        <p className={`${styles.count} text-muted`}>
                            <span className="text-primary me-1">
                                <CountUp end={136} duration={5} />
                            </span>
                            Kelas Online
                        </p>
                    </div>
                    <div className="col-6 col-lg-3">
                        <p className={`${styles.count} text-muted`}>
                            <span className="text-primary me-1">
                                <CountUp end={1000} duration={5} />+
                            </span>
                            Materi Belajar
                        </p>
                    </div>
                    <div className="col-6 col-lg-3">
                        <p className={`${styles.count} text-muted`}>
                            <span className="text-primary me-1">
                                <CountUp end={100} duration={5} />+
                            </span>
                            Jam Belajar
                        </p>
                    </div>
                    <div className="col-6 col-lg-3">
                        <p className={`${styles.count} text-muted`}>
                            <span className="text-primary me-1">
                                <CountUp end={6} duration={5} />
                            </span>
                            Jalur Belajar
                        </p>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default StatisticSection;
