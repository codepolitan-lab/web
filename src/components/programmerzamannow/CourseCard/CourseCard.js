import Link from 'next/link';
import styles from './CourseCard.module.scss';

const CourseCard = ({index, thumbnail, title, totalModule, link}) => {
    return (
        <Link href={link}>
            <a className="link">
                <div className={`${styles.card} card border-0 shadow-sm`}>
                    {index && (
                        <div className={`${styles.pill_index} d-flex align-items-center`}>
                            <span className="text-white mx-auto mb-0">{index}</span>
                        </div>
                     )}
                    <div className="card-body">
                        <img className="card-img rounded-3 img-fluid" loading="lazy" src={thumbnail || '/assets/img/placeholder.jpg'} alt={title} />
                        <div className="my-3">
                            <h5 className={styles.title}>{title || 'Unamed'}</h5>
                            <p className={`${styles.total_module} text-muted`}>{totalModule} modul</p>
                        </div>
                    </div>
                </div>
            </a>
        </Link>
    );
};

export default CourseCard;
