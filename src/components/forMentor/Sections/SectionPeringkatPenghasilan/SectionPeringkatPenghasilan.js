import styles from "../SectionPeringkatPenghasilan/SectionPeringkatPenghasilan.module.scss"

const SectionPeringkatPenghasilan = () => {
    return (
        <section id="profitTotal">
            <div className="container p-4 p-lg-5">
                <div className="row">
                    <div className="col-lg-6">
                        <table className="table table-striped table-bordered shadow">
                            <thead className="bg-codepolitan">
                                <tr className="text-center text-white">
                                    <th>Mentor Partner</th>
                                    <th>Total Purchase</th>
                                </tr>
                            </thead>
                            <tbody className={`${styles.text}`}>
                                <tr className="position-relative">
                                    <td><span>Eko Kurniawan khanedy</span></td>
                                    <td align="center"><span>1.500 Purchase</span></td>
                                </tr>
                                <tr>
                                    <td><span>Angga Risky Setiawan</span></td>
                                    <td align="right"><span>750 Purchase</span></td>
                                </tr>
                                <tr>
                                    <td><span>Muhammad Amirul Ihsan</span></td>
                                    <td align="right"><span>699 Purchase</span></td>
                                </tr>
                                <tr>
                                    <td><span>Bagus Budi Cahyono</span></td>
                                    <td align="right"><span>645 Purchase</span></td>
                                </tr>
                                <tr>
                                    <td><span>Nuris Akbar</span></td>
                                    <td align="right"><span>638 Purchase</span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div className="col-lg-6 d-flex align-items-center justify-content-center">
                        <div className="container mt-4 mt-md-0">
                            <div className="h4 lh-base text-muted">Peringkat Penghasilan Partner Mentor Codepolitan</div>
                            <p className="fs-6 mt-4 lead lh-lg text-muted">Hingga hari ini, pencairan dana yang telah dicarikan oleh partner mentor Codepolitan sebesar <span className="fw-bold">Rp. 202.680.194</span>. Mereka telah memulainya, sekarang giliran kamu !</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default SectionPeringkatPenghasilan