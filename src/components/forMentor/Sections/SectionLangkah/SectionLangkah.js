
const SectionLangkah = () => {
    return (
        <section style={{ backgroundColor: '#f8f8f8;' }}>
            <div className="container p-4 p-lg-5 text-muted">
                <div className="text-center">
                    <h3 className="mt-5 mb-3">Mulai Dengan 3 Langkah Ini</h3>
                    <p>Dengan memulai langkah berikut kamu akan menjadi partner mentor kami</p>
                </div>
                <div className="row mt-5 align-items-center">
                    <div className="col-lg-6">
                        <img src="/assets/img/formentor/image-1.png" className="w-100" alt="Image Tentukan Materi" />
                    </div>
                    <div className="col-lg-6">
                        <h5 className="mb-4">Tentukan materi yang akan dibuat</h5>
                        <p>Pilih salah satu skill yang kamu kuasai, lalu tentukan topik pembahasan yang tepat sesuai dengan kemampuan yang kamu miliki.</p>
                    </div>
                </div>
                <div className="row align-items-center">
                    <div className="col-lg-6 order-first order-md-last">
                        <img src="/assets/img/formentor/image-2.png" className="w-100" alt="Image Buat Video Materi" />
                    </div>
                    <div className="col-lg-6">
                        <h5 className="mb-4">Buat Video Materi</h5>
                        <p>Buat video materi kamu menggunakan alat yang kamu miliki seperti kamera DSLR atau handphone. Buatlah dalam materi yang padat, mudah dipahami dan sesuai dengan silabus yang sudah kamu rancang/</p>
                    </div>
                </div>
                <div className="row align-items-center">
                    <div className="col-lg-6">
                        <img src="/assets/img/formentor/image-3.png" className="w-100" alt="Image Publish Video" />
                    </div>
                    <div className="col-lg-6">
                        <h5 className="mb-4">Publish Video</h5>
                        <p>Kami akan mempersiapkan video yang telah kamu buat diplatform Codepolitan dan kami akan membantu mempromosikan kelas yang kamu kirimkan ke Codepolitan dengan sistem bagi hasil.</p>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default SectionLangkah