import styles from '../SectionKeuntungan/SectionKeuntungan.module.scss'

const SectionKeuntungan = () => {
    return (
        <section className={`${styles.background}`} id="keuntunganBergabung">
            <div className="container p-4 p-lg-5 text-center">
                <h4 className="text-muted mb-5">Keuntungan Bergabung Program Partner Mentor</h4>
                <div className="row">
                    <div className="col-lg-3 my-2 my-md-0">
                        <img src="/assets/img/formentor/vector/video.svg" width={90} alt="Image Secure Video" />
                        <div className="text-muted text-center">
                            <h6 className="my-2 fw-bold">Secure Video</h6>
                            <small className="text-muted">Video kamu tidak bisa didownload sehingga dapat melindungi hak kekayaan intelektual mentor dari pembajakan.</small>
                        </div>
                    </div>
                    <div className="col-lg-3 my-2 my-md-0">
                        <img src="/assets/img/formentor/vector/50.svg" width={90} alt="Image Profit Sharing" />
                        <div className="text-muted text-center">
                            <h6 className="my-2 fw-bold">Profit Sharing</h6>
                            <small className="text-muted">Bagi hasil akan ditransfer langsung ke rekening mentor setiap awal bulan paling lambat tanggal 5 pada hari kerja.</small>
                        </div>
                    </div>
                    <div className="col-lg-3 my-2 my-md-0">
                        <img src="/assets/img/formentor/vector/traffic.svg" width={90} alt="Image Traffic" />
                        <div className="text-muted text-center">
                            <h6 className="my-2 fw-bold">Traffic</h6>
                            <small className="text-muted">Rata-rata traffic harian kami dikunjungi oleh pemula/praktisi teknologi mencapai 17k perhari atau sekitar 500k perbulannya.</small>
                        </div>
                    </div>
                    <div className="col-lg-3 my-2 my-md-0">
                        <img src="/assets/img/formentor/vector/paper.svg" width={90} alt="Image Transparansi" />
                        <div className="text-muted text-center">
                            <h6 className="my-2 fw-bold">Transparansi</h6>
                            <small className="text-muted">Catatan keuntungan kamu akan sangat transparan, disini kamu juga dapat memantaunya melalui platform CodePolitan.</small>
                        </div>
                    </div>
                </div>
                <div className="row my-2 my-md-5">
                    <div className="col-lg-3 my-2 my-md-0">
                        <img src="/assets/img/formentor/vector/target.svg" width={90} alt="Image Brand Terjaga" />
                        <div className="text-muted text-center">
                            <h6 className="my-2 fw-bold">Brand Terjaga</h6>
                            <small className="text-muted">Brand konten kamu akan terjaga karena team CodePolitan akan menampilkan brand kamu.</small>
                        </div>
                    </div>
                    <div className="col-lg-3 my-2 my-md-0">
                        <img src="/assets/img/formentor/vector/www.svg" width={90} alt="Image Custom Domain" />
                        <div className="text-muted text-center">
                            <h6 className="my-2 fw-bold">Custom Domain</h6>
                            <small className="text-muted">Custom domain akan diberikan jika mentor memiliki lebih dari 1 kelas terpublikasi.</small>
                        </div>
                    </div>
                    <div className="col-lg-3 my-2 my-md-0">
                        <img src="/assets/img/formentor/vector/list.svg" width={90} alt="Image Platform Teruji" />
                        <div className="text-muted text-center">
                            <h6 className="my-2 fw-bold">Platform Teruji</h6>
                            <small className="text-muted">Kami telah berdiri sejak tahun 2017 hingga saat ini sehingga mendapatkan banyak kepercayaan.</small>
                        </div>
                    </div>
                    <div className="col-lg-3 my-2 my-md-0">
                        <img src="/assets/img/formentor/vector/monitoring.svg" width={90} alt="Image Monitoring Siswa" />
                        <div className="text-muted text-center">
                            <h6 className="my-2 fw-bold">Monitoring Siswa</h6>
                            <small className="text-muted">Kamu dapat memantau siswa yang belajar bersama kamu melalui platform CodePolitan.</small>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default SectionKeuntungan