const SectionCodepolitanSelf = () => {
    return (
        <section id="codepolitanSelf">
            <div className="container p-4 p-lg-5">
                <h4 className="text-muted my-4">Codepolitan  VS Self-Publishing Course</h4>
                <table className="table shadow overflow-hidden">
                    <thead className="bg-codepolitan">
                        <tr className="text-white">
                            <th className="ps-md-4 py-3">Codepolitan</th>
                            <th className="ps-md-4 py-md-3">Self-Publishing Course</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Profit sharing dari penjualan kelas antara Codepolitan dengan Mentor</td>
                            <td>Seluruh hasil penjualan kelas untuk sendiri</td>
                        </tr>
                        <tr>
                            <td>Bersama CodePolitan meriset tema terbaik</td>
                            <td>Riset Tema Kelas Sendiri</td>
                        </tr>
                        <tr>
                            <td>Tim Kurikulum CodePolitan akan membantu mentor dalam menyusun dan mereview silabus</td>
                            <td>Silabus disusun sendiri</td>
                        </tr>
                        <tr>
                            <td>Tim Development CodePolitan akan memastikan platform siap untuk kebutuhan kelas mentor</td>
                            <td>Membangun dan memelihara platform sendiri</td>
                        </tr>
                        <tr>
                            <td>Tim Produk CodePolitan akan membantu menyiapkan landing page untuk penjualan kelas</td>
                            <td>Membuat landing page penjualan sendiri</td>
                        </tr>
                        <tr>
                            <td>Tim Content Codepolitan akan memberikan rekomendasi agar mentor bisa menghasilkan kelas yang berkualitas sesuai standar Codepolitan</td>
                            <td>Mencari-cari standar sendiri untuk pembuatan materi kelas</td>
                        </tr>
                        <tr>
                            <td>Tim Marketing Codepolitan akan membantu mengalirkan traffic dan calon pembeli ke kelas mentor</td>
                            <td>Promosi kelas online sendiri</td>
                        </tr>
                        <tr>
                            <td>Video editing dibantu oleh tim editor Codepolitan, studio rekaman disediakan (bila perlu)</td>
                            <td>Video rekam sendiri dan edit sendiri</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </section>
    )
}

export default SectionCodepolitanSelf