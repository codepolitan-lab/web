const SectionQuotes = () => {
    return (
        <section className="bg-codepolitan">
            <div className="container p-4 p-lg-5">
                <div className="row justify-content-center">
                    <div className="col-lg-10">
                        <div className="text-center text-white my-3">
                            <h2>&#8220;Mari menjadi bagian dalam membangun ekosistem yang nyaman untuk belajar pemrograman dan teknologi di Indonesia&#8220;</h2>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default SectionQuotes