import styles from "../SectionMentorBekerja/SectionMentorBekerja.module.scss"

const SectionMentorBekerja = () => {
    return (
        <section id="mentorBekerja">
            <div className="container p-4 p-lg-5 text-muted">
                <div className="text-center">
                    <h3 className="mt-5 mb-3">Bagaimana Mentor Bekerja ?</h3>
                    <p>Tentukan konten yang akan kamu publish berdasarkan dibawah ini</p>
                </div>
                <div className="row mt-5 d-none d-md-flex">
                    <div className="col-12 p-0 col-lg-3 d-flex align-items-start">
                        <div className="nav flex-column nav-pills shadow w-100 sticky-top" style={{ top: '130px' }} id="v-pills-tab" role="tablist" aria-orientation="vertical">
                            <button className="nav-link text-start active" id="v-pills-alur-kerjasama-tab" data-bs-toggle="pill" data-bs-target="#v-pills-alur-kerjasama" type="button" role="tab" aria-controls="v-pills-alur-kerjasama" aria-selected="false">Alur Kerjasama</button>
                            <button className="nav-link text-start" id="v-pills-kategori-konten-tab" data-bs-toggle="pill" data-bs-target="#v-pills-kategori-konten" type="button" role="tab" aria-controls="v-pills-kategori-konten" aria-selected="false">Kategori Konten</button>
                            <button className="nav-link text-start" id="v-pills-model-bisnis-tab" data-bs-toggle="pill" data-bs-target="#v-pills-model-bisnis" type="button" role="tab" aria-controls="v-pills-model-bisnis" aria-selected="false">Model Bisnis</button>
                            <button className="nav-link text-start" id="v-pills-kewajiban-partner-tab" data-bs-toggle="pill" data-bs-target="#v-pills-kewajiban-partner" type="button" role="tab" aria-controls="v-pills-kewajiban-partner" aria-selected="true">Kewajiban Partner</button>
                            <button className="nav-link text-start" id="v-pills-kewajiban-codepolitan-tab" data-bs-toggle="pill" data-bs-target="#v-pills-kewajiban-codepolitan" type="button" role="tab" aria-controls="v-pills-kewajiban-codepolitan" aria-selected="false">Kewajiban Codepolitan</button>
                            <button className="nav-link text-start" id="v-pills-profit-sharing-tab" data-bs-toggle="pill" data-bs-target="#v-pills-profit-sharing" type="button" role="tab" aria-controls="v-pills-profit-sharing" aria-selected="false">Profit Sharing</button>
                        </div>
                    </div>
                    <div className="col-12 col-lg-8 px-0">
                        <div className="tab-content ps-3 mt-5 mt-md-0" id="v-pills-tabContent">
                            <div className="tab-pane fade" id="v-pills-kewajiban-partner" role="tabpanel" aria-labelledby="v-pills-kewajiban-partner-tab">
                                <div className="row align-items-center mb-4">
                                    <div className="col-lg-2 text-center">
                                        <img src="/assets/img/formentor/vector/Group 2256.svg" width={120} alt="Image Menyusun Silabus" />
                                    </div>
                                    <div className="col-lg-10">
                                        <div className="fw-bold mb-2 text-center text-md-start">Menyusun Silabus</div>
                                        <p>Kamu dapat memulainya dari menyusun silabus materi yang akan kamu buat, supaya calon siswa memiliki gambaran terkait apa yang akan dipelajari di video konten yang kamu buat. </p>
                                    </div>
                                </div>
                                <div className="row align-items-center mb-4 ">
                                    <div className="col-lg-2 text-center">
                                        <img src="/assets/img/formentor/vector/Group 2257.svg" width={120} alt="Image Membuat Materi Belajar" />
                                    </div>
                                    <div className="col-lg-10">
                                        <div className="fw-bold text-center text-md-start mb-2">Membuat Materi Belajar</div>
                                        <p>Pilih materi belajar sesuai dengan apa yang kamu kuasai, buatlah sesuai semenarik mungkin supaya calon pembeli memberikan feedback positif. Karena feedback siswa dapat mempengaruhi penghasilan kamu nantinya.</p>
                                    </div>
                                </div>
                                <div className="row align-items-center mb-4 ">
                                    <div className="col-lg-2 text-center">
                                        <img src="/assets/img/formentor/vector/Group 2258.svg" width={120} alt="Image Memperbarui Materi Belajar" />
                                    </div>
                                    <div className="col-lg-10">
                                        <div className="fw-bold text-center text-md-start mb-2">Memperbarui Materi Belajar</div>
                                        <p>Perbarui materi belajar yang sudah kamu buat, supaya siswa lebih tertarik untuk membeli kelas yang kamu buat karena mendapatkan banyak benefit dari teknologi terbaru.</p>
                                    </div>
                                </div>
                                <div className="row align-items-center">
                                    <div className="col-lg-2 text-center">
                                        <img src="/assets/img/formentor/vector/Group 2259.svg" width={120} alt="Image Maintance Forum Tanya Jawab" />
                                    </div>
                                    <div className="col-lg-10">
                                        <div className="fw-bold text-center text-md-start mb-2">Maintenance Forum Tanya Jawab</div>
                                        <p>Disini kamu dapat aktif memantau perkembangan siswa kelas yang kamu buat, semakin ramai kelas yang kamu buat semakin besar juga peluang penjualan kelas yang kamu buat.</p>
                                    </div>
                                </div>
                            </div>
                            <div className="tab-pane fade mt-5 mt-md-0" id="v-pills-kategori-konten" role="tabpanel" aria-labelledby="v-pills-kategori-konten-tab">
                                <div className="row align-items-center mb-4">
                                    <div className="col-lg-2 text-center">
                                        <img src="/assets/img/formentor/vector/Group 2267.svg" width={120} alt="Image Programming" />
                                    </div>
                                    <div className="col-lg-10">
                                        <div className="fw-bold text-center text-md-start mb-2">Programming</div>
                                        <p>Kategori ini kamu dapat membuat konten tentang frontend, backend, android developer hingga fullstack developer. Pilih bahasa pemrograman yang sesuai dengan keahlian kamu.</p>
                                    </div>
                                </div>
                                <div className="row align-items-center mb-4 ">
                                    <div className="col-lg-2 text-center">
                                        <img src="/assets/img/formentor/vector/Group 2361.svg" width={120} alt="Image Product Management" />
                                    </div>
                                    <div className="col-lg-10">
                                        <div className="fw-bold text-center text-md-start mb-2">Product Management</div>
                                        <p>Kamu dapat membuat konten tentang UI/UX, baik hanya UI Design, UI Interaction, UI Animation, UX Research, UX Design, UI Writter hingga bagaimana cara memanagement produk baru yang akan dibuat.</p>
                                    </div>
                                </div>
                                <div className="row align-items-center mb-4 ">
                                    <div className="col-lg-2 text-center">
                                        <img src="/assets/img/formentor/vector/Group 2362.svg" width={120} alt="Image Cloud Computing" />
                                    </div>
                                    <div className="col-lg-10">
                                        <div className="fw-bold text-center text-md-start mb-2">Cloud Computing</div>
                                        <p>Pilih materi belajar sesuai dengan apa yang kamu kuasai, buatlah sesuai semenarik mungkin supaya calon pembeli memberikan feedback positif. karena feedback siswa dapat mempengaruhi penghasilan kamu nantinya</p>
                                    </div>
                                </div>
                                <div className="row align-items-center">
                                    <div className="col-lg-2 text-center">
                                        <img src="/assets/img/formentor/vector/Group 2270.svg" width={120} alt="Image Cyber Security" />
                                    </div>
                                    <div className="col-lg-10">
                                        <div className="fw-bold text-center text-md-start mb-2">Cyber Security</div>
                                        <p>Pilih materi belajar sesuai dengan apa yang kamu kuasai, buatlah sesuai semenarik mungkin supaya calon pembeli memberikan feedback positif. karena feedback siswa dapat mempengaruhi penghasilan kamu nantinya.</p>
                                    </div>
                                </div>
                            </div>
                            <div className="tab-pane fade" id="v-pills-model-bisnis" role="tabpanel" aria-labelledby="v-pills-model-bisnis-tab">
                                <div className={`ms-2 ${styles.wrap} mt-5`}>
                                    <div className="text-center">Sewa Kelas Satuan</div>
                                    <div className="position-relative d-flex justify-content-center mb-n3">
                                        <div className={`position-absolute d-flex align-items-center ${styles['vector-position']}`}>
                                            <div className="d-flex flex-column ms-3">
                                                <span>Sewa Semua</span>
                                                <span>Kelas Mentor</span>
                                            </div>
                                            <img src="/assets/img/formentor/vector/Group 2347.svg" className={`${styles['vector-responsive']}`} alt="Image Horizontal" />
                                            <div className="d-flex flex-column">
                                                <span>Sewa Roadmap</span>
                                                <span>Belajar</span>
                                            </div>
                                        </div>
                                        <div className="position-relative">
                                            <img src="/assets/img/formentor/vector/Group 2348.svg" className={`${styles['vector-responsive']}`} alt="Image Vertical" />
                                        </div>
                                    </div>
                                    <div className="text-center">Beli Kelas</div>
                                </div>
                            </div>
                            <div className="tab-pane fade mt-5 mt-md-0 px-0 px-md-4" id="v-pills-profit-sharing" role="tabpanel" aria-labelledby="v-pills-profit-sharing-tab">
                                <h4>Simulasi Profit Sharing</h4>
                                <div className="row mt-5 text-center">
                                    <div className="col-lg-6 mb-4 mb-md-0">
                                        <img src="/assets/img/formentor/vector/Group 2364.svg" className={`img-fluid ${styles['vector-profit']}`} alt="Image Regular Partner" />
                                        <p className="lead mt-n3">Regular Partner</p>
                                    </div>
                                    <div className="col-lg-6">
                                        <img src="/assets/img/formentor/vector/Group 2349.svg" className={`img-fluid ${styles['vector-profit']}`} alt="Image Exclusive Partner" />
                                        <p className="lead mt-n3">Exclusive Partner</p>
                                    </div>
                                </div>
                                <hr />
                                <p>Regular Partner adalah partner CodePolitan yang memiliki platform penjualan sendiri atau menjual konten kelas di platform lain di luar CodePolitan. <br /> Exclusive Partner adalah partner CodePolitan yang membuat konten kelas dan hanya mensubmit kelasnya di platform CodePolitan.</p>
                                <div className="mt-4">
                                    <p className="fw-bold">Harga Jual Kelas : Rp. 300.000</p>
                                    <p className="fw-bold mt-4 mb-0">Penjualan 3 kelas per hari dalam 1 bulan :</p>
                                    <p>@ Rp. 300.000 x 3 kelas x 30 hari = <strong>Rp. 27.000.000</strong></p>
                                    <p className="mt-4 fw-bold mb-n1">Bagi Hasil Skema 50 : 50</p>
                                    <p>Rp. 27.000.000 / 2 = <strong>Rp. 13.500.000</strong></p>
                                    <p className="mt-4 fw-bold text-primary fst-italic">Partner dapat membuat lebih dari 1 kelas untuk melipatgandakan keuntungan</p>
                                </div>
                            </div>
                            <div className="tab-pane fade mt-5 mt-md-0 container-lg" id="v-pills-kewajiban-codepolitan" role="tabpanel" aria-labelledby="v-pills-kewajiban-codepolitan-tab">
                                <div className="row align-items-center mb-4">
                                    <div className="col-lg-2 text-center">
                                        <img src="/assets/img/formentor/vector/Group 2292.svg" width={100} alt="Image Mempersiapkan Platform Belajar" />
                                    </div>
                                    <div className="col-lg-10">
                                        <div className="fw-bold text-center text-md-start mb-2">Mempersiapkan Platform Belajar</div>
                                        <p>Tim Development CodePolitan akan memastikan platform siap untuk kebutuhan kelas mentor</p>
                                    </div>
                                </div>
                                <div className="row align-items-center mb-4">
                                    <div className="col-lg-2 text-center order-first order-md-last">
                                        <img src="/assets/img/formentor/vector/Group 2297.svg" width={100} alt="Image Mengatur Publikasi Kelas" />
                                    </div>
                                    <div className="col-lg-10">
                                        <div className="fw-bold text-center text-md-start mb-2">Mengatur Publikasi Kelas</div>
                                        <p>Tim Kurikulum CodePolitan akan membantu mentor dalam menyusun dan mereview silabus</p>
                                    </div>
                                </div>
                                <div className="row align-items-center mb-4">
                                    <div className="col-lg-2 text-center">
                                        <img src="/assets/img/formentor/vector/Group 2298.svg" width={100} alt="Image Mempromosikan Kelas" />
                                    </div>
                                    <div className="col-lg-10">
                                        <div className="fw-bold text-center text-md-start mb-2">Mempromosikan Kelas</div>
                                        <p>Tim Marketing CodePolitan akan membantu mengalirkan traffic dan calon pembeli ke kelas mentor</p>
                                    </div>
                                </div>
                                <div className="row align-items-center mb-4">
                                    <div className="col-lg-2 text-center order-first order-md-last">
                                        <img src="/assets/img/formentor/vector/Group 2299.svg" width={100} alt="Image Laporan Keuangan" />
                                    </div>
                                    <div className="col-lg-10">
                                        <div className="fw-bold text-center text-md-start mb-2">Memberikan Laporan Keuangan Penjualan Kelas</div>
                                        <p>Profit Sharing dari penjualan kelas antara CodePolitan dengan mentor akan diberikan oleh tim CodePolitan</p>
                                    </div>
                                </div>
                                <div className="row align-items-center mb-4">
                                    <div className="col-lg-2 text-center">
                                        <img src="/assets/img/formentor/vector/Group 2300.svg" width={100} alt="Image Transfer Bagi Hasil" />
                                    </div>
                                    <div className="col-lg-10">
                                        <div className="fw-bold text-center text-md-start mb-2">Transfer Bagi Hasil Kelas</div>
                                        <p>Bagi hasil akan ditransfer langsung ke rekening mentor setiap awal bulan, paling lambat setiap tanggal 5 pada hari kerja.</p>
                                    </div>
                                </div>
                            </div>
                            <div className="tab-pane fade show active" id="v-pills-alur-kerjasama" role="tabpanel" aria-labelledby="v-pills-alur-kerjasama-tab">
                                <div className="row justify-content-center">
                                    <div className="col-lg-10">
                                        <img src="/assets/img/formentor/alur-kerja-sama.png" className="w-100 ms-0 ms-md-5" alt="Image Alur Kerjasama" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {/* Handle for mobile view */}
                <div className="mt-5 d-block d-md-none">
                    <div className="accordion accordion-flush" id="accordion-mentor">
                        <div className="accordion-item">
                            <h2 className="accordion-header" id="flush-alur-kerjasama">
                                <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse-alur-kerjasama" aria-expanded="false" aria-controls="flush-collapse-alur-kerjasama">
                                    Alur Kerjasama
                                </button>
                            </h2>
                            <div id="flush-collapse-alur-kerjasama" className="accordion-collapse collapse show" aria-labelledby="flush-alur-kerjasama" data-bs-parent="#accordion-mentor">
                                <div className="accordion-body">
                                    <div className="row justify-content-center">
                                        <div className="col-lg-10">
                                            <img src="/assets/img/formentor/vector/Group 2365.svg" className="w-100 ms-3" alt="Image Alur Kerjasama" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="accordion-item">
                            <h2 className="accordion-header" id="flush-kategori-konten">
                                <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse-kategori-konten" aria-expanded="false" aria-controls="flush-collapse-kategori-konten">
                                    Kategori Konten
                                </button>
                            </h2>
                            <div id="flush-collapse-kategori-konten" className="accordion-collapse collapse" aria-labelledby="flush-kategori-konten" data-bs-parent="#accordion-mentor">
                                <div className="accordion-body">
                                    <div className="row align-items-center mb-4">
                                        <div className="col-lg-2 text-center">
                                            <img src="/assets/img/formentor/vector/Group 2267.svg" width={120} alt="Image Programming" />
                                        </div>
                                        <div className="col-lg-10">
                                            <div className="fw-bold text-center text-md-start mb-2">Programming</div>
                                            <p>Kategori ini kamu dapat membuat konten tentang frontend, backend, android developer hingga fullstack developer. Pilih bahasa pemrograman yang sesuai dengan keahlian kamu.</p>
                                        </div>
                                    </div>
                                    <div className="row align-items-center mb-4 ">
                                        <div className="col-lg-2 text-center">
                                            <img src="/assets/img/formentor/vector/Group 2361.svg" width={120} alt="Image Product Management" />
                                        </div>
                                        <div className="col-lg-10">
                                            <div className="fw-bold text-center text-md-start mb-2">Product Management</div>
                                            <p>Kamu dapat membuat konten tentang UI/UX, baik hanya UI Design, UI Interaction, UI Animation, UX Research, UX Design, UI Writter hingga bagaimana cara memanagement produk baru yang akan dibuat.</p>
                                        </div>
                                    </div>
                                    <div className="row align-items-center mb-4 ">
                                        <div className="col-lg-2 text-center">
                                            <img src="/assets/img/formentor/vector/Group 2362.svg" width={120} alt="Image Cloud Computing" />
                                        </div>
                                        <div className="col-lg-10">
                                            <div className="fw-bold text-center text-md-start mb-2">Cloud Computing</div>
                                            <p>Pilih materi belajar sesuai dengan apa yang kamu kuasai, buatlah sesuai semenarik mungkin supaya calon pembeli memberikan feedback positif. karena feedback siswa dapat mempengaruhi penghasilan kamu nantinya</p>
                                        </div>
                                    </div>
                                    <div className="row align-items-center">
                                        <div className="col-lg-2 text-center">
                                            <img src="/assets/img/formentor/vector/Group 2270.svg" width={120} alt="Image Cyber Security" />
                                        </div>
                                        <div className="col-lg-10">
                                            <div className="fw-bold text-center text-md-start mb-2">Cyber Security</div>
                                            <p>Pilih materi belajar sesuai dengan apa yang kamu kuasai, buatlah sesuai semenarik mungkin supaya calon pembeli memberikan feedback positif. karena feedback siswa dapat mempengaruhi penghasilan kamu nantinya.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="accordion-item">
                            <h2 className="accordion-header" id="flush-model-bisnis">
                                <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse-model-bisnis" aria-expanded="false" aria-controls="flush-collapse-model-bisnis">
                                    Model Bisnis
                                </button>
                            </h2>
                            <div id="flush-collapse-model-bisnis" className="accordion-collapse collapse" aria-labelledby="flush-model-bisnis" data-bs-parent="#accordion-mentor">
                                <div className="accordion-body ps-5 pb-0">
                                    <div className={`${styles.wrap}`}>
                                        <div className="text-center">Sewa Kelas Satuan</div>
                                        <div className="position-relative d-flex justify-content-center mb-n3">
                                            <div className={`position-absolute d-flex align-items-center ${styles['vector-position']}`}>
                                                <div className="d-flex flex-column ms-3">
                                                    <span>Sewa Semua</span>
                                                    <span>Kelas Mentor</span>
                                                </div>
                                                <img src="/assets/img/formentor/vector/Group 2347.svg" className={`${styles['vector-responsive']}`} alt="Image Horizontal" />
                                                <div className="d-flex flex-column">
                                                    <span>Sewa Roadmap</span>
                                                    <span>Belajar</span>
                                                </div>
                                            </div>
                                            <div className="position-relative">
                                                <img src="/assets/img/formentor/vector/Group 2348.svg" className={`${styles['vector-responsive']}`} alt="Image Vertical" />
                                            </div>
                                        </div>
                                        <div className="text-center">Beli Kelas</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="accordion-item">
                            <h2 className="accordion-header" id="flush-kewajiban-partner">
                                <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse-kewajiban-partner" aria-expanded="false" aria-controls="flush-collapse-kewajiban-partner">
                                    Kewajiban Partner
                                </button>
                            </h2>
                            <div id="flush-collapse-kewajiban-partner" className="accordion-collapse collapse" aria-labelledby="flush-kewajiban-partner" data-bs-parent="#accordion-mentor">
                                <div className="accordion-body">
                                    <div className="row align-items-center mb-4">
                                        <div className="col-lg-2 text-center">
                                            <img src="/assets/img/formentor/vector/Group 2256.svg" width={120} alt="Image Menyusun Silabus" />
                                        </div>
                                        <div className="col-lg-10">
                                            <div className="fw-bold mb-2 text-center text-md-start">Menyusun Silabus</div>
                                            <p>Kamu dapat memulainya dari menyusun silabus materi yang akan kamu buat, supaya calon siswa memiliki gambaran terkait apa yang akan dipelajari di video konten yang kamu buat. </p>
                                        </div>
                                    </div>
                                    <div className="row align-items-center mb-4 ">
                                        <div className="col-lg-2 text-center">
                                            <img src="/assets/img/formentor/vector/Group 2257.svg" width={120} alt="Image Membuat Materi Belajar" />
                                        </div>
                                        <div className="col-lg-10">
                                            <div className="fw-bold text-center text-md-start mb-2">Membuat Materi Belajar</div>
                                            <p>Pilih materi belajar sesuai dengan apa yang kamu kuasai, buatlah sesuai semenarik mungkin supaya calon pembeli memberikan feedback positif. Karena feedback siswa dapat mempengaruhi penghasilan kamu nantinya.</p>
                                        </div>
                                    </div>
                                    <div className="row align-items-center mb-4 ">
                                        <div className="col-lg-2 text-center">
                                            <img src="/assets/img/formentor/vector/Group 2258.svg" width={120} alt="Image Memperbarui Materi Belajar" />
                                        </div>
                                        <div className="col-lg-10">
                                            <div className="fw-bold text-center text-md-start mb-2">Memperbarui Materi Belajar</div>
                                            <p>Perbarui materi belajar yang sudah kamu buat, supaya siswa lebih tertarik untuk membeli kelas yang kamu buat karena mendapatkan banyak benefit dari teknologi terbaru.</p>
                                        </div>
                                    </div>
                                    <div className="row align-items-center">
                                        <div className="col-lg-2 text-center">
                                            <img src="/assets/img/formentor/vector/Group 2259.svg" width={120} alt="Image Maintance Forum Tanya Jawab" />
                                        </div>
                                        <div className="col-lg-10">
                                            <div className="fw-bold text-center text-md-start mb-2">Maintenance Forum Tanya Jawab</div>
                                            <p>Disini kamu dapat aktif memantau perkembangan siswa kelas yang kamu buat, semakin ramai kelas yang kamu buat semakin besar juga peluang penjualan kelas yang kamu buat.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="accordion-item">
                            <h2 className="accordion-header" id="flush-kewajiban-codepolitan">
                                <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse-kewajiban-codepolitan" aria-expanded="false" aria-controls="flush-collapse-kewajiban-codepolitan">
                                    Kewajiban CodePolitan
                                </button>
                            </h2>
                            <div id="flush-collapse-kewajiban-codepolitan" className="accordion-collapse collapse" aria-labelledby="flush-kewajiban-codepolitan" data-bs-parent="#accordion-mentor">
                                <div className="accordion-body">
                                    <div className="row align-items-center mb-4">
                                        <div className="col-lg-2 text-center">
                                            <img src="/assets/img/formentor/vector/Group 2292.svg" width={100} alt="Image Mempersiapkan Platform Belajar" />
                                        </div>
                                        <div className="col-lg-10">
                                            <div className="fw-bold text-center text-md-start mb-2">Mempersiapkan Platform Belajar</div>
                                            <p>Tim Development CodePolitan akan memastikan platform siap untuk kebutuhan kelas mentor</p>
                                        </div>
                                    </div>
                                    <div className="row align-items-center mb-4">
                                        <div className="col-lg-2 text-center order-first order-md-last">
                                            <img src="/assets/img/formentor/vector/Group 2297.svg" width={100} alt="Image Mengatur Publikasi Kelas" />
                                        </div>
                                        <div className="col-lg-10">
                                            <div className="fw-bold text-center text-md-start mb-2">Mengatur Publikasi Kelas</div>
                                            <p>Tim Kurikulum CodePolitan akan membantu mentor dalam menyusun dan mereview silabus</p>
                                        </div>
                                    </div>
                                    <div className="row align-items-center mb-4">
                                        <div className="col-lg-2 text-center">
                                            <img src="/assets/img/formentor/vector/Group 2298.svg" width={100} alt="Image Mempromosikan Kelas" />
                                        </div>
                                        <div className="col-lg-10">
                                            <div className="fw-bold text-center text-md-start mb-2">Mempromosikan Kelas</div>
                                            <p>Tim Marketing CodePolitan akan membantu mengalirkan traffic dan calon pembeli ke kelas mentor</p>
                                        </div>
                                    </div>
                                    <div className="row align-items-center mb-4">
                                        <div className="col-lg-2 text-center order-first order-md-last">
                                            <img src="/assets/img/formentor/vector/Group 2299.svg" width={100} alt="Image Laporan Keuangan" />
                                        </div>
                                        <div className="col-lg-10">
                                            <div className="fw-bold text-center text-md-start mb-2">Memberikan Laporan Keuangan Penjualan Kelas</div>
                                            <p>Profit Sharing dari penjualan kelas antara CodePolitan dengan mentor akan diberikan oleh tim CodePolitan</p>
                                        </div>
                                    </div>
                                    <div className="row align-items-center mb-4">
                                        <div className="col-lg-2 text-center">
                                            <img src="/assets/img/formentor/vector/Group 2300.svg" width={100} alt="Image Transfer Bagi Hasil" />
                                        </div>
                                        <div className="col-lg-10">
                                            <div className="fw-bold text-center text-md-start mb-2">Transfer Bagi Hasil Kelas</div>
                                            <p>Bagi hasil akan ditransfer langsung ke rekening mentor setiap awal bulan, paling lambat setiap tanggal 5 pada hari kerja.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="accordion-item">
                            <h2 className="accordion-header" id="flush-profit-sharing">
                                <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse-profit-sharing" aria-expanded="false" aria-controls="flush-collapse-profit-sharing">
                                    Profit Sharing
                                </button>
                            </h2>
                            <div id="flush-collapse-profit-sharing" className="accordion-collapse collapse" aria-labelledby="flush-profit-sharing" data-bs-parent="#accordion-mentor">
                                <div className="accordion-body">
                                    <h4 className="text-center">Simulasi Profit Sharing</h4>
                                    <div className="row mt-5 text-center">
                                        <div className="col-lg-6 mb-4 mb-md-0">
                                            <img src="/assets/img/formentor/vector/Group 2364.svg" className={`img-fluid ${styles['vector-profit']}`} alt="Image Regular Partner" />
                                            <p className="lead mt-n3">Regular Partner</p>
                                        </div>
                                        <div className="col-lg-6">
                                            <img src="/assets/img/formentor/vector/Group 2349.svg" className={`img-fluid ${styles['vector-profit']}`} alt="Image Exclusive Partner" />
                                            <p className="lead mt-n3">Exclusive Partner</p>
                                        </div>
                                    </div>
                                    <hr />
                                    <p>Regular Partner adalah partner CodePolitan yang memiliki platform penjualan sendiri atau menjual konten kelas di platform lain di luar CodePolitan. <br /> Exclusive Partner adalah partner CodePolitan yang membuat konten kelas dan hanya mensubmit kelasnya di platform CodePolitan.</p>
                                    <div className="mt-4">
                                        <p className="fw-bold">Harga Jual Kelas : Rp. 300.000</p>
                                        <p className="fw-bold mt-4 mb-0">Penjualan 3 kelas per hari dalam 1 bulan :</p>
                                        <p>@ Rp. 300.000 x 3 kelas x 30 hari = <strong>Rp. 27.000.000</strong></p>
                                        <p className="mt-4 fw-bold mb-n1">Bagi Hasil Skema 50 : 50</p>
                                        <p>Rp. 27.000.000 / 2 = <strong>Rp. 13.500.000</strong></p>
                                        <p className="mt-4 fw-bold text-primary fst-italic">Partner bisa memilih lebih dari 1 kelas untuk melipatgandakan keuntungan</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default SectionMentorBekerja