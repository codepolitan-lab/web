import mixpanel from "mixpanel-browser"

const SectionInvitation = () => {
    mixpanel.init('608746391f30c018d759b8c2c1ecb097', { debug: false })
    return (
        <section className="bg-codepolitan">
            <div className="container p-4 p-lg-5">
                <div className="text-center text-white">
                    <h2 className="mb-4">Mari menjadi bagian dalam membangun ekosistem yang nyaman untuk belajar pemrograman dan teknologi di Indonesia</h2>
                    {/*<button className="btn btn-outline-light btn-rounded me-none me-md-4 mb-2 mb-md-0 d-block d-md-inline mx-auto">Syarat & Ketentuan</button>*/}
                    <a onClick={() => mixpanel.track('Daftar mentor', { 'source': 'Landing' })} href="https://airtable.com/shr7AyfCEMNuY8Hzc" target="_blank" className="btn btn-pink btn-rounded btn-lg" rel="noreferrer">Daftar Sebagai Mentor</a>
                </div>
            </div>
        </section>
    );
};

export default SectionInvitation;