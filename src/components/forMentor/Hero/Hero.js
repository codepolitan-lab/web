import styles from "../../forMentor/Hero/Hero.module.scss"

const Hero = () => {
    return (
        <>
            <section className={`${styles.hero} mt-md-5 py-5`}>
                <div className="container p-4 p-lg-5">
                    <div className="row">
                        <div className="col-lg-7 text-white text-center text-lg-start my-auto order-last order-md-first">
                            <h1 className={styles.hero_title}>Jadilah Partner Pengajar Bersama Codepolitan</h1>
                            <p className={`lead mt-4 ${styles.lead}`}>Melalui program ini Codepolitan ingin menciptakan sebuah peluang dimana para developer bisa mendapatkan penghasilan dengan cara berbagi pengetahuan yang dimilikinya dalam bidang teknologi.</p>
                            <div className="text-center text-md-start mt-4">
                                <a href="#keuntunganBergabung" className="btn btn-primary btn-lg btn-rounded text-white px-5 py-2">Mulai Sekarang</a>
                            </div>
                        </div>
                        <div className="col-lg-5">
                            <img className="img-fluid mt-4 mt-md-0" src="/assets/img/formentor/hero.png" alt="Hero" />
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}

export default Hero