import moment from 'moment';
import 'moment/locale/id';

// Remove Tag
export const removeTag = (tag) => {
  return tag.replace(/(<([^>]+)>)/ig, "");
}

// Get Lesson
export const getLessons = (lessons) => {
  const lessonTopic = [];
  const lessonContent = [];

  /* Loop for tab lessons */
  for (const lessonKey in lessons) {
    lessonTopic.push({
      topic_slug: lessonKey,
      topic_title: lessons[lessonKey]['topic_title'],
    });
  }

  lessonTopic.map((lessonTopicItem) => {
    let tempEachContent = {};
    let topicSlug = lessonTopicItem.topic_slug;

    tempEachContent.topic_slug = topicSlug;
    tempEachContent.contents = [];

    let lessonContentKey = Object.keys(lessons[topicSlug].lessons);

    for (const contentKey in lessonContentKey) {
      let tempContent = {};
      tempContent.lesson_slug = lessons[topicSlug].lessons[lessonContentKey[contentKey]].lesson_slug;
      tempContent.lesson_title = lessons[topicSlug].lessons[lessonContentKey[contentKey]].lesson_title;
      tempContent.duration = lessons[topicSlug].lessons[lessonContentKey[contentKey]].duration;
      tempEachContent.contents.push(tempContent);
    }

    lessonContent.push(tempEachContent);
  });

  return {
    lessonTopic,
    lessonContent
  }
}

// Hitung rata-rata rating
export const countRates = (rate) => {
  const rates = [];

  if (rate.status === 'failed') {
    console.log('there is no testimony');
  } else {
    rate?.map((item) => {
      rates.push(parseInt(item.rate));
    });
  }

  const sumRate = rates.reduce((a, b) => a + b, 0);
  const totalRate = rates.length;
  const ratingValue = sumRate / totalRate;

  return ratingValue;
}

// Format Price
export const formatPrice = (price) => {
  return price?.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
};

// Format Number
export const formatNumber = (value) => {
  return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

// Format Only Date
export const formatOnlyDate = (value) => {
  return moment(value).format('LL');
}

// Format Date
export const formatDate = (value) => {
  return moment(value).format('LLLL');
}

// Format From Now
export const formatDateFromNow = (value) => {
  return moment(value).fromNow();
}

// Get Percentage From Price
export const getPercentage = (retailPrice, normalPrice) => {
  const decreaseValue = normalPrice - retailPrice;
  const result = (decreaseValue / normalPrice) * 100
  return Math.round(result)
}

// Get Percentage From Testimony
export const getPercentageTestimony = (testimony, count) => {
  const total = testimony.length > 1 && testimony.map(item => item.rate).filter(item => item == count)
  let result = (total.length / testimony.length) * 100
  return Math.round(result)
}

// Suffle Array
export const shuffleArray = (array) => {
  array?.sort(() => Math.random() - 0.5);
  return array;
};

// Get total module
export const getTotalModule = (courses) => {
  let totalModule = 0;
  courses.map(course => totalModule += course.total_module);
  return totalModule
}

// Get total time
export const getTotalTime = (courses) => {
  let totalTime = 0;
  courses.map(course => totalTime += course.total_time);
  return totalTime;
}

// Capitalize first leter
export const capitalizeFirstLetter = (text) => {
  return text.charAt(0).toUpperCase() + text.slice(1);
};
