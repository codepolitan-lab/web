import Link from "next/link";
import Head from "next/head";

const NotFound = () => {
  return (
    <>
      <Head>
        <title>404 Not Found - Codepolitan</title>
        <meta name="title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
        <meta name="description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
        <meta property="og:type" content="website" />
        <meta property="og:url" content="https://codepolitan.com" />
        <meta property="og:title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
        <meta property="og:description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
        <meta property="og:image" content="https://i.ibb.co/SXP8fGc/OG-image.jpg" />
        <meta property="twitter:card" content="summary_large_image" />
        <meta property="twitter:url" content="https://codepolitan.com" />
        <meta property="twitter:title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
        <meta property="twitter:description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
        <meta property="twitter:image" content="https://i.ibb.co/SXP8fGc/OG-image.jpg" />
      </Head>
      <div className="container my-4 py-4">
        <div className="row">
          <div className="col text-center">
            <img className="img-fluid" src="/assets/img/oops.png" alt="Oops! Halaman Tidak ditemukan" />
            <h1 style={{ fontWeight: 'bold', fontSize: '2rem' }}>404 - Page Not Found</h1>
            <p className="lead text-muted mb-5">
              Sepertinya halaman yang kamu cari tidak ditemukan.
            </p>
            <Link href="https://codepolitan.com">
              <a className="btn btn-primary px-5 py-3" style={{ backgroundColor: '#14a7a0', color: 'white', border: 'none' }}>Kembali</a>
            </Link>
          </div>
        </div>
      </div>
    </>
  );
};

export default NotFound;
