import { useEffect } from 'react';
import dynamic from 'next/dynamic';
import axios from 'axios';
import Head from 'next/head';
import { ToastContainer, toast } from 'react-toastify';
import Layout from '../components/global/Layout/Layout';
import Hero from '../components/home/Hero/Hero';
import SectionRoadmap from '../components/home/Sections/SectionRoadmap/SectionRoadmap';
import SectionFeaturedArticle from '../components/home/Sections/SectionFeaturedArticle/SectionFeaturedArticle';
import SectionBroadcasted from '../components/home/Sections/SectionBroadcasted/SectionBroadcasted';
import ToastMessage from '../components/global/ToastMessage/ToastMessage';
import { shuffleArray } from '../utils/helper';
import SectionQuestion from '../components/global/Sections/SectionQuestion/SectionQuestion';
import SectionEvents from '../components/home/Sections/SectionEvents/SectionEvents';

// Dynamic Content
const SectionPartner = dynamic(() => import('../components/home/Sections/SectionPartner/SectionPartner'),
  { ssr: false }
);
const SectionMateri = dynamic(() => import('../components/home/Sections/SectionMateri/SectionMateri'),
  { ssr: false }
);
const SectionCourses = dynamic(() => import('../components/global/Sections/SectionCourses/SectionCourses'),
  { ssr: false }
);

const SectionPartnerCampus = dynamic(() => import('../components/home/Sections/SectionPartnerCampus/SectionPartnerCampus'),
  { ssr: false }
);

export const getServerSideProps = async () => {
  const bannersRes = await axios.get(`${process.env.NEXT_PUBLIC_APPS_URL}/api/entry/index/slider_homepage`);
  const roadmapsRes = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/v1/roadmap/featured?limit=200&page=1`);
  const statsRes = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/v1/stats`);
  const popularLabelsRes = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/course/popular-labels`);
  const popularCoursesRes = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/course/popular`); // Ganti sama popular-weekly kalau APInya udah fix
  const eventsRes = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/v1/webinar/latest?page=1&limit=10`);
  const featuredArticleRes = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/v1/posts/latest/post?page=1&limit=10`);
  const latestCoursesRes = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/course?limit=10&sort=created_at[desc]&page=1`);
  const activitiesRes = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/v1/activity`);

  const [banners, roadmaps, stats, popularCourses, popularLabels, events, featuredArticle, latestCourses, activities] = await Promise.all([
    bannersRes.data.results,
    roadmapsRes.data,
    statsRes.data,
    popularCoursesRes.data,
    popularLabelsRes.data,
    eventsRes.data,
    featuredArticleRes.data,
    latestCoursesRes.data,
    activitiesRes.data
  ]);

  return {
    props: {
      banners,
      roadmaps,
      stats,
      popularCourses,
      popularLabels,
      events,
      featuredArticle,
      latestCourses,
      activities
    },
    // revalidate: 1000
  }
}

const Home = ({ banners, roadmaps, stats, popularCourses, popularLabels, events, featuredArticle, latestCourses, activities }) => {
  useEffect(() => {
    const showActivities = () => {
      shuffleArray(activities).forEach((activity, index) => {
        toast(
          <a
            className="link"
            href={activity.type === "course" ? `/course/intro/${activity.slug}` : `/roadmap/${activity.slug}`}
            target="_blank"
            rel="noopener noreferrer"
          >
            <ToastMessage
              key={index}
              avatar=""
              name={activity.name}
              message={`Telah membeli ${activity.product_name}`}
              timestamp={activity.created_at}
            />
          </a>, {
          toastId: 'notif' + index
        });
      });
    };
    showActivities();
  }, [activities]);

  return (
    <>
      <Head>
        <title>Website Belajar Coding Bahasa Indonesia - Codepolitan</title>
        <meta name="title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
        <meta name="description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
        <meta property="og:type" content="website" />
        <meta property="og:url" content="https://codepolitan.com" />
        <meta property="og:title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
        <meta property="og:description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
        <meta property="og:image" content="https://i.ibb.co/SXP8fGc/OG-image.jpg" />
        <meta property="twitter:card" content="summary_large_image" />
        <meta property="twitter:url" content="https://codepolitan.com" />
        <meta property="twitter:title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
        <meta property="twitter:description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
        <meta property="twitter:image" content="https://i.ibb.co/SXP8fGc/OG-image.jpg" />
      </Head>
      <Layout>
        <Hero banners={banners} />
        <SectionQuestion />
        <SectionRoadmap data={roadmaps} />
        <SectionMateri
          statistic={stats}
          data={popularLabels.filter(label => label.display == 'public')}
          title="Eksplorasi Materi Codepolitan"
        />
        <SectionCourses
          title="Kelas Popular Minggu Ini"
          data={popularCourses}
          link="/library/?type=popular"
        />
        <SectionCourses
          title="Kelas"
          courseFor="Terbaru"
          data={latestCourses}
          link="/library"
          latestCourse
        />
        <SectionEvents data={events} />
        <SectionFeaturedArticle data={featuredArticle} />
        <SectionPartner />
        <SectionPartnerCampus />
        <SectionBroadcasted />
        <ToastContainer
          className="d-none d-lg-block"
          role="alert"
          position="bottom-left"
          limit={1}
          autoClose={5000}
          hideProgressBar={true}
          newestOnTop={false}
          closeOnClick={false}
          rtl={false}
          pauseOnFocusLoss={false}
          draggable={false}
          pauseOnHover={false}
        />
      </Layout>
    </>
  );
};

export default Home;
