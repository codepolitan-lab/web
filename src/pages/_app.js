import Head from 'next/head';
import Script from 'next/script';
import NextNProgress from 'nextjs-progressbar';
import { useEffect } from "react";
import * as fbq from '../utils/lib/fbpixel';
// import * as ga from '../utils/lib/ga';
import "@fortawesome/fontawesome-svg-core/styles.css";
import { config } from "@fortawesome/fontawesome-svg-core";
config.autoAddCss = false;
import "../styles/globals.scss";
import { useRouter } from 'next/dist/client/router';
import OneSignal from 'react-onesignal';
import { hotjar } from 'react-hotjar';

// Import Swiper styles
import 'swiper/scss';
import 'swiper/scss/grid';
import 'swiper/scss/navigation';
import 'swiper/scss/pagination';

// Import React Toastify
import 'react-toastify/dist/ReactToastify.css';

const MyApp = ({ Component, pageProps }) => {
  const router = useRouter();

  useEffect(() => {
    // Bootstrap Js
    import("bootstrap/dist/js/bootstrap");

    // Hotjar
    hotjar.initialize(2886806, 6);

    // Facebook Pixel
    fbq.pageview();

    const handleRouteChange = () => {
      console.log('route change');
      fbq.pageview();
      // ga.pageview(url);
    };

    router.events.on('routeChangeComplete', handleRouteChange)
    return () => {
      router.events.off('routeChangeComplete', handleRouteChange)
    }
  }, [router.events]);

  // One Signal
  // useEffect(() => {
  //   OneSignal.init({
  //     appId: "d6b15547-af5a-4f06-85d8-f04610ca9e04"
  //   });
  // }, []);

  return (
    <>
      <Head>
        <meta lang='id' />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      </Head>
      <NextNProgress color="#FF869A" height={3} />
      <Component {...pageProps} />
      <Script
        id="fbpixel"
        strategy="afterInteractive"
        dangerouslySetInnerHTML={{
          __html: `
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', ${fbq.FB_PIXEL_ID});
          `,
        }} />
    </>
  );
};

export default MyApp;
