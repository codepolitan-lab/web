import Head from 'next/head';
import SectionIntro from '../../components/about/Sections/SectionIntro/SectionIntro';
import SectionTimeline from '../../components/about/Sections/SectionTimeline/SectionTimeline';
import SectionCounter from '../../components/about/Sections/SectionCounter/SectionCounter';
import SectionTeam from '../../components/about/Sections/SectionTeam/SectionTeam';
import Layout from '../../components/global/Layout/Layout';
import axios from 'axios';
import Link from 'next/link';
import SectionPartner from '../../components/home/Sections/SectionPartner/SectionPartner';
import SectionPartnerCampus from '../../components/home/Sections/SectionPartnerCampus/SectionPartnerCampus';

export const getStaticProps = async () => {
    const { data } = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/v1/stats`);
    const response = { data };

    const stats = response.data;

    return {
        props: {
            stats
        },
    }
}

const About = ({ stats }) => {
    return (
        <>
            <Head>
                <title>Tentang Kami - Codepolitan</title>
                <meta name="title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta name="description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="og:type" content="website" />
                <meta property="og:url" content="https://codepolitan.com" />
                <meta property="og:title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta property="og:description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="og:image" content="https://i.ibb.co/SXP8fGc/OG-image.jpg" />
                <meta property="twitter:card" content="summary_large_image" />
                <meta property="twitter:url" content="https://codepolitan.com" />
                <meta property="twitter:title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta property="twitter:description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="twitter:image" content="https://i.ibb.co/SXP8fGc/OG-image.jpg" />
            </Head>
            <Layout>
                <SectionIntro />
                <SectionTimeline />
                <SectionCounter data={stats} />
                <SectionTeam />
                <SectionPartner />
                <SectionPartnerCampus />
                <section className="section bg-codepolitan">
                    <div className="container p-4 p-lg-5">
                        <div className="row">
                            <div className="col-lg-5 mb-3">
                                <img className="img-fluid d-block mx-auto mx-lg-0" src="/assets/img/about/img-about.png" alt="About Image" />
                            </div>
                            <div className="col my-auto text-white mb-5 mb-lg-auto">
                                <h3 className="section-title text-white">Kami Dapat Menjadi Solusi Anda</h3>
                                <p className="my-3">Di Codepolitan, Tujuan kami khusus untuk membantu generasi muda belajar dan berlatih pemrograman agar kompetitif di era Industri 4.0. melalui platform belajar berbahasa Indonesia dan menjadi sumber terpercaya bagi generasi muda , perusahaan  hingga sekolah yang ada di Indonesia</p>
                                <Link href="/for-mentor">
                                    <a className="btn btn-outline-light me-2 my-1">For Mentor</a>
                                </Link>
                                <Link href="/for-company">
                                    <a className="btn btn-outline-light me-2 my-1">For Company</a>
                                </Link>
                                {/* <Link href="/for-school">
                                    <a className="btn btn-outline-light my-1">For School</a>
                                </Link> */}
                            </div>
                        </div>
                    </div>
                </section>
            </Layout>
        </>
    );
};

export default About;
