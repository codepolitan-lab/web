import { useState, useEffect } from 'react';
import Head from 'next/head';
import Layout from '../../components/global/Layout/Layout';
import axios from 'axios';
import SectionCertificate from '../../components/certificate/SectionCertificate/SectionCertificate';
import NextNProgress from 'nextjs-progressbar';

const CertificateDetail = () => {
    const [certificate, setCertificate] = useState([]);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        const url = window.location.href;
        let slugParam = url => new URL(url).pathname.split('/').filter(Boolean);
        const slug = slugParam(url);

        const getCertificate = async () => {
            setLoading(true);
            try {
                let response = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/v1/certificate/detail/${slug[1]}`);
                if (response.data.error === "No Content") {
                    setCertificate(null);
                    setLoading(false);
                } else {
                    setCertificate(response.data.certificate);
                    setLoading(false);
                }
            } catch (err) {
                setLoading(true);
                return err.message;
            }
        };
        getCertificate();
    }, []);

    if (!certificate) {
        return (
            <Layout>
                <section className="section">
                    <div className="container p-4 p-lg-5">
                        <div className="row my-5 py-5">
                            <div className="col my-5 py-5">
                                <h1 className="section-title">Ups, Sertifikat Tidak Tersedia</h1>
                                <p className="text-muted">Silahkan hubungi admin untuk informasi lebih lanjut</p>
                            </div>
                        </div>
                    </div>
                </section>
            </Layout>
        );
    };

    return (
        <>
            <Head>
                <title>Certificate Detail - Codepolitan</title>
                <meta name="title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta name="description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="og:type" content="website" />
                <meta property="og:url" content="https://codepolitan.com" />
                <meta property="og:title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta property="og:description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="og:image" content="https://i.ibb.co/SXP8fGc/OG-image.jpg" />
                <meta property="twitter:card" content="summary_large_image" />
                <meta property="twitter:url" content="https://codepolitan.com" />
                <meta property="twitter:title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta property="twitter:description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="twitter:image" content="https://i.ibb.co/SXP8fGc/OG-image.jpg" />
            </Head>
            <Layout>
                {loading ?
                    (<NextNProgress />) : (
                        <SectionCertificate certificate={certificate} />
                    )}
            </Layout>
        </>
    );
};

export default CertificateDetail;