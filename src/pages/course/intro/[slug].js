import { faShareAlt, faLock, faCircleQuestion, faCirclePlay } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { RatingView } from "react-simple-star-rating"
import SectionTestimonyRating from "../../../components/global/Sections/SectionTestimonyRating/SectionTestimonyRating";
import SectionTestimonyComment from "../../../components/global/Sections/SectionTestimonyComment/SectionTestimonyComment";
import SectionRelatedCourse from "../../../components/global/Sections/SectionRelatedCourse/SectionRelatedCourse";
import axios from "axios";
import Layout from "../../../components/global/Layout/Layout";
import { countRates, shuffleArray } from "../../../utils/helper";
import Head from "next/head";
import Modal from "../../../components/global/Modal/Modal"
import CardDetail from "../../../components/global/Cards/CardDetail/CardDetail";
import SectionSlider from "../../../components/global/Sections/SectionSlider/SectionSlider"
import { SwiperSlide } from "swiper/react";
import CardMentor from "../../../components/global/Cards/CardMentor/CardMentor"
import { useEffect, useState } from "react";
import ButtonShareSocmed from "../../../components/global/Buttons/ButtonShareSocmed/ButtonShareSocmed";
import Link from "next/link"
import CardCourse from "../../../components/global/Cards/CardCourse/CardCourse";
import SkeletonCardCourse from "../../../components/skeletons/SkeletonCardCourse/SkeletonCardCourse";

export const getServerSideProps = async (context) => {
    const { slug } = context.query;
    const detailRes = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/course/detailComprehensif/${slug}`);
    const testimonyRes = await axios.get(`${process.env.NEXT_PUBLIC_APPS_URL}/api/feedback/course/${slug}`);

    const [detail, testimony] = await Promise.all([
        detailRes.data,
        testimonyRes.data
    ])

    return {
        props: {
            detail,
            testimony
        },
    }
};

const CourseDetail = ({ detail, testimony }) => {

    const video = detail.course.preview_video
    const [videoId, setVideoId] = useState(video)
    const [relatedCourse, setRelatedCourse] = useState([])
    const [show, setShow] = useState(false)
    const [filterRate, setFilterRate] = useState(5)
    const [allRate, setAllRate] = useState(true)
    const lessons = detail.lessons
    const ratingValue = countRates(testimony);

    let detailParams = detail.products[0].retail_price <= 0 ? detail.course : detail.products

    const filter = (rate) => {
        if (rate === true) {
            setAllRate(rate)
            setFilterRate(true)
        } else {
            setFilterRate(rate)
            setAllRate(false)
        }
    }

    useEffect(() => {
        const getRelatedCourse = async () => {
            try {
                let response = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/course/popular`);
                if (response.data.error === "No Content") {
                    setRelatedCourse(null);
                } else {
                    setRelatedCourse(response.data);
                }
            } catch (err) {
                return err.message;
            }
        };
        getRelatedCourse();
    }, [detail.author_name]);

    return (
        <>
            <Head>
                <title>{detail.course.title} - Codepolitan</title>
                <meta name="title" content={detail.course.title} />
                <meta name="description" content={detail.course.description} />
                <meta property="og:type" content="website" />
                <meta property="og:url" content={`https://codepolitan.com/course/intro/${detail.course.slug}`} />
                <meta property="og:title" content={detail.course.title} />
                <meta property="og:description" content={detail.course.description} />
                <meta property="og:image" content={detail.course.cover} />
                <meta property="twitter:card" content="summary_large_image" />
                <meta property="twitter:url" content={`https://codepolitan.com/course/intro/${detail.course.slug}`} />
                <meta property="twitter:title" content={detail.course.title} />
                <meta property="twitter:description" content={detail.course.description} />
                <meta property="twitter:image" content={detail.course.cover} />
            </Head>
            <Layout>
                <section className="section mt-5">
                    <div className="container p-3 p-lg-5">
                        <div className="row mt-4">
                            {/* Main */}
                            <div className="col-lg-7 text-muted">
                                <section className="section">
                                    <div className="ratio ratio-16x9 mb-4 d-lg-none">
                                        <iframe
                                            style={{ borderRadius: '25px' }}
                                            src={`https://www.youtube.com/embed/${detail.course.preview_video}`}
                                            title="Course Preview"
                                            allowFullScreen
                                        />
                                    </div>
                                    <div className="d-flex align-items-center">
                                        <img height="65" className="rounded d-none d-lg-block" src={detail.course.thumbnail} alt={detail.course.title} />
                                        <h1 className="section-title ms-lg-3 my-auto h2">{detail.course.title}</h1>
                                    </div>
                                    <p className="text-muted my-3">{detail.course.description}</p>
                                    <div className="d-md-flex d-grid text-center text-lg-start">
                                        <span className="badge border text-primary py-md-2">{detail.course.level.toUpperCase()}</span>
                                        <RatingView className="ms-1 ms-md-3 mt-md-1 mt-2 mb-1 d-none d-md-block" size={17} ratingValue={Math.floor(ratingValue)} />
                                        <span className="mt-md-1 text-muted ms-lg-auto ms-md-3 my-1 my-md-0">{testimony.length || 0} penilaian</span>
                                        <span className="mt-md-1 text-muted ms-lg-auto ms-md-3 my-1 my-md-0">{detail.total_student} peserta</span>
                                        <button onClick={() => setShow(!show)} className="btn text-muted btn-sm btn-transparent ms-md-auto"><FontAwesomeIcon icon={faShareAlt} /> Share</button>
                                    </div>
                                    {
                                        show && (
                                            <div className="row my-3">
                                                <div className="col text-center">
                                                    <ButtonShareSocmed type="facebook" link={`https://codepolitan.com/course/intro/${detail.course.slug}`} />
                                                    <ButtonShareSocmed type="twitter" link={`https://codepolitan.com/course/intro/${detail.course.slug}`} />
                                                    <ButtonShareSocmed type="linkedin" link={`https://codepolitan.com/course/intro/${detail.course.slug}`} />
                                                </div>
                                            </div>
                                        )
                                    }
                                </section>

                                <div className="row mt-5">
                                    <div className="col">
                                        <div>
                                            <h4 className="section-title">Tentang Kelas</h4>
                                            <div className="text-muted" dangerouslySetInnerHTML={{ __html: detail.course.long_description }} />
                                        </div>
                                        <div className="accordion accordion-flush my-5" id="accordionFlushExample">
                                            <h4 className="section-title">Daftar Materi</h4>
                                            {
                                                lessons.map((lesson, index) => {
                                                    return (
                                                        <div key={index} className="accordion-item">
                                                            <h2 className="accordion-header" id={`flush-heading-${lesson.id}`}>
                                                                <button className="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target={`#flush-collapse-${lesson.id}`} aria-expanded="false" aria-controls={`flush-collapse-${lesson.id}`}>
                                                                    {lesson.topic_title}
                                                                </button>
                                                            </h2>
                                                            <div id={`flush-collapse-${lesson.id}`} className="accordion-collapse collapse show" aria-labelledby={`flush-heading-${lesson.id}`} data-bs-parent="#accordionFlushExample">
                                                                <div className="accordion-body px-0 py-1">
                                                                    <div className="card border-0">
                                                                        <div className="card-body p-0">
                                                                            <table className="table table-striped m-0">
                                                                                <tbody>
                                                                                    {
                                                                                        lesson.contents.map((content, index) => {
                                                                                            return (
                                                                                                <tr key={index}>
                                                                                                    <td style={{ width: '50px' }}>
                                                                                                        {content.lesson_title?.includes('Kuis') || content.lesson_title?.includes('Quiz') ? (
                                                                                                            <FontAwesomeIcon className="text-primary" size="2x" icon={faCircleQuestion} />
                                                                                                        ) : (
                                                                                                            <FontAwesomeIcon className="text-primary" size="2x" icon={faCirclePlay} />
                                                                                                        )}
                                                                                                    </td>
                                                                                                    <td className="text-muted">{content.lesson_title || 'Untitled'}</td>
                                                                                                    <td className="text-end text-muted">
                                                                                                        <FontAwesomeIcon icon={faLock} className="me-2" />
                                                                                                        {content.duration || '00.00'}
                                                                                                    </td>
                                                                                                </tr>
                                                                                            )
                                                                                        })
                                                                                    }
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    )
                                                })
                                            }
                                        </div>

                                        {/* Mentor detail */}
                                        <SectionSlider
                                            title="Penyusun Materi"
                                            titleFontSize="h4"
                                            slidesPerView={2.1}
                                        >
                                            {detail.authors?.map((author, index) => {
                                                return (
                                                    <SwiperSlide key={index}>
                                                        <CardMentor author={author.name} shortDescription={author.short_description} thumbnail={author.avatar} />
                                                    </SwiperSlide>
                                                );
                                            }).slice(0, 10)}
                                        </SectionSlider>
                                        {/* End of Mentor detail */}


                                        {/* Testimony Rating */}
                                        <SectionTestimonyRating className="mt-5" filterRate={filter} testimony={testimony} totalRate={ratingValue} totalReview={testimony.length} />
                                        {/* End of Testimony Rating */}


                                        {/* Testimony Comment */}
                                        {testimony.status != 'failed' && (<SectionTestimonyComment allRate={allRate} rateFilter={filterRate} testimony={testimony} />)}
                                        {/* End of Testimony Comment */}

                                    </div>
                                </div>
                            </div>
                            {/* End of Main */}

                            {/* Sidebar */}
                            <div className="col-lg-4 offset-lg-1 sticky-top">
                                <div className="sticky-top" style={{ top: '90px' }}>
                                    {/* Card detail sidebar */}
                                    <CardDetail
                                        setVideo={() => setVideoId(video)}
                                        image={detail.course.cover}
                                        detailProduct={detailParams}
                                        previewVideo={video}
                                        totalModule={detail.course.total_module}
                                        totalTime={detail.course.total_time}
                                        retailPrice={detail.products[0].retail_price}
                                    />
                                    {/* End of Card detail sidebar */}
                                </div>
                            </div>
                            {/* End of Sidebar */}
                        </div>
                    </div>
                </section>
                <Modal title="Preview Course Video" setVideo={() => setVideoId('')}>
                    <div className="ratio ratio-16x9">
                        <iframe src={`https://www.youtube.com/embed/${videoId}`} title="YouTube video" allowFullScreen />
                    </div>
                </Modal>

                <SectionRelatedCourse titleSection={`Kelas Populer Lainnya`}>
                    {!relatedCourse && [1, 2, 3, 4, 5].map(index => {
                        return (
                            <SwiperSlide key={index}>
                                <SkeletonCardCourse />
                            </SwiperSlide>
                        );
                    })}
                    {relatedCourse != null && shuffleArray(relatedCourse).map((course, index) => {
                        return (
                            <SwiperSlide key={index}>
                                <Link href={`/course/intro/${course.slug}`}>
                                    <a className="link">
                                        <CardCourse
                                            thumbnail={course.thumbnail}
                                            author={course.author}
                                            title={course.title}
                                            level={course.level}
                                            totalStudents={course.total_student}
                                            totalModules={course.total_module}
                                            totalTimes={course.total_time}
                                            totalRating={course.total_rating}
                                            totalFeedback={course.total_feedback}
                                            normalBuyPrice={course.buy?.normal_price || course.normal_price}
                                            retailBuyPrice={course.buy?.retail_price || course.retail_price}
                                            normalRentPrice={course.rent?.normal_price}
                                            retailRentPrice={course.rent?.retail_price}
                                        />
                                    </a>
                                </Link>
                            </SwiperSlide>
                        );
                    }).slice(0, 10)}
                </SectionRelatedCourse>
            </Layout>
        </>
    );
};

export default CourseDetail;
