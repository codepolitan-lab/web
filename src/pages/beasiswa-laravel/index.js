import Head from 'next/head';
import Hero from '../../components/beasiswaLaravel/Hero/Hero';
import SectionClaimSteps from '../../components/beasiswaLaravel/SectionClaimSteps/SectionClaimSteps';
import SectionCTA from '../../components/beasiswaLaravel/SectionCTA/SectionCTA';
import SectionFAQ from '../../components/beasiswaLaravel/SectionFAQ/SectionFAQ';
import SectionIntro from '../../components/beasiswaLaravel/SectionIntro/SectionIntro';
import SectionMaterials from '../../components/beasiswaLaravel/SectionMaterials/SectionMaterials';
import SectionMentor from '../../components/beasiswaLaravel/SectionMentor/SectionMentor';
import Layout from '../../components/global/Layout/Layout';

const BeasiswaLaravel = () => {
    return (
        <>
            <Head>
                <title>Beasiswa Laravel - Codepolitan</title>
                <meta name="title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta name="description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="og:type" content="website" />
                <meta property="og:url" content="https://codepolitan.com" />
                <meta property="og:title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta property="og:description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="og:image" content="https://i.ibb.co/SXP8fGc/OG-image.jpg" />
                <meta property="twitter:card" content="summary_large_image" />
                <meta property="twitter:url" content="https://codepolitan.com" />
                <meta property="twitter:title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta property="twitter:description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="twitter:image" content="https://i.ibb.co/SXP8fGc/OG-image.jpg" />
            </Head>
            <Layout>
                <Hero />
                <SectionIntro />
                <SectionMaterials />
                <SectionMentor />
                <SectionClaimSteps />
                <SectionCTA />
                <SectionFAQ />
            </Layout>
        </>
    );
};

export default BeasiswaLaravel;
