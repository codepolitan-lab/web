import { faRedo } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import axios from 'axios';
import Head from 'next/head';
import { useState } from 'react';
import Banner from '../../components/global/Banner/Banner';
import Layout from '../../components/global/Layout/Layout';
import { formatOnlyDate, formatPrice } from '../../utils/helper';

const CheckCoupon = () => {
    const [coupons, setCoupons] = useState([]);
    const [couponName, setCouponName] = useState("");
    const [start, setStart] = useState("");
    const [end, setEnd] = useState("");
    const [showNotFound, setShowNotFound] = useState(false);
    const [loading, setLoading] = useState(false);
    const [limit, setLimit] = useState(10);

    const onSubmit = async (e) => {
        e.preventDefault();
        setLoading(true);

        try {
            const response = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/v1/coupon/track?from=${start}&to=${end}&coupon=${couponName}`);
            if (response.data) {
                setLoading(false);
                setCoupons(response.data);

                if (response.data.length === 0) setShowNotFound(true);
            };
        } catch (error) {
            setLoading(false);
            throw new Error(error.message);
        };
    };

    return (
        <>
            <Head>
                <title>Cek Kupon - Codepolitan</title>
                <meta name="title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta name="description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="og:type" content="website" />
                <meta property="og:url" content="https://codepolitan.com" />
                <meta property="og:title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta property="og:description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="og:image" content="https://i.ibb.co/SXP8fGc/OG-image.jpg" />
                <meta property="twitter:card" content="summary_large_image" />
                <meta property="twitter:url" content="https://codepolitan.com" />
                <meta property="twitter:title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta property="twitter:description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="twitter:image" content="https://i.ibb.co/SXP8fGc/OG-image.jpg" />
            </Head>
            <Layout>
                <Banner
                    background='/assets/img/backgrounds/banner-check-coupon.jpg'
                    title="Cek Kupon Codepolitan"
                    subtitle="Verifikasi kupon Codepolitan yang kamu miliki dengan mudah"
                />
                <section className="section my-5">
                    <div className="container p-4 p-lg-5">
                        <div className="row justify-content-center mb-5">
                            <div className="col-lg-5">
                                <h1 className="section-title text-center mb-5">Cek Kupon</h1>
                                <form onSubmit={onSubmit}>
                                    <div className="mb-3">
                                        <label className="form-label">Kode kupon</label>
                                        <input onChange={(e) => setCouponName(e.target.value.toUpperCase())} value={couponName} type="text" className="form-control" placeholder="Masukkan Kode Kupon..." required />
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6 mb-3">
                                            <label className="form-label">Dari tanggal</label>
                                            <input onChange={(e) => setStart(e.target.value)} value={start} type="date" className="form-control" required />
                                        </div>
                                        <div className="col-md-6 mb-3">
                                            <label className="form-label">Sampai tanggal</label>
                                            <input onChange={(e) => setEnd(e.target.value)} value={end} type="date" className="form-control" required />
                                        </div>
                                    </div>
                                    <div className="d-grid">
                                        <button type="submit" className="btn btn-primary" disabled={!couponName}>Cari Kupon</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        {loading && (
                            <div className="text-center">
                                <div className="spinner-border" role="status">
                                    <span className="visually-hidden">Loading...</span>
                                </div>
                            </div>
                        )}
                        {!loading && coupons?.length < 1 && showNotFound && (
                            <div className="text-center text-muted">Kupon tidak ditemukan</div>
                        )}
                        {!loading && coupons?.length > 0 && (
                            <div className="row justify-content-center">
                                <div className="col-lg-10">
                                    <div className="overflow-auto">
                                        <p>Total : {coupons.length}</p>
                                        <table className="table table-hover table-striped">
                                            <thead className="table-primary">
                                                <tr>
                                                    <th scope="col">#</th>
                                                    <th scope="col">Nama Kupon</th>
                                                    <th scope="col">Produk</th>
                                                    <th scope="col">Tanggal Terjual</th>
                                                    <th scope="col">Price</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    coupons.map((coupon, index) => {
                                                        return (
                                                            <tr key={index} className="text-muted">
                                                                <td>{index + 1}</td>
                                                                <td>{coupon.coupon_code}</td>
                                                                <td>{coupon.product_name}</td>
                                                                <td>{formatOnlyDate(coupon.created_at)}</td>
                                                                <td>Rp {formatPrice(coupon.price)}</td>
                                                            </tr>
                                                        )
                                                    }).slice(0, limit)
                                                }
                                            </tbody>
                                        </table>
                                        {coupons?.length > limit && (
                                            <div className="text-center my-3">
                                                <button onClick={() => setLimit(limit + 10)} className="btn btn-light">
                                                    Load more
                                                    <FontAwesomeIcon className="ms-1" size="sm" icon={faRedo} />
                                                </button>
                                            </div>
                                        )}
                                    </div>
                                </div>
                            </div>
                        )}
                    </div>
                </section>
            </Layout>
        </>
    );
};

export default CheckCoupon;
