import { faComment, faThumbsUp } from "@fortawesome/free-regular-svg-icons";
import { faCheckCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import axios from "axios";
import moment from "moment";
import Head from "next/head";
import Link from "next/link";
import CardForum from "../../../components/global/Cards/CardForum/CardForum";
import Layout from "../../../components/global/Layout/Layout";
import ReactMarkdown from 'react-markdown';
import rehypeRaw from 'rehype-raw';
import remarkGfm from 'remark-gfm';
import { CodeBlock } from '../../../components/global/Codeblock';

export const getServerSideProps = async (context) => {
    const { slug } = context.query;
    const { data } = await axios.get(`${process.env.NEXT_PUBLIC_APPS_URL}/api/forum/thread/detail/${slug}`);
    const response = { data };

    const thread = response.data;

    return {
        props: {
            thread
        },
    }
};

const ForumDetail = ({ thread }) => {
    return (
        <>
            <Head>
                <title>{thread.detail.subject || 'Forum'} - Codepolitan</title>
                <meta name="title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta name="description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="og:type" content="website" />
                <meta property="og:url" content="https://codepolitan.com" />
                <meta property="og:title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta property="og:description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="og:image" content="https://i.ibb.co/SXP8fGc/OG-image.jpg" />
                <meta property="twitter:card" content="summary_large_image" />
                <meta property="twitter:url" content="https://codepolitan.com" />
                <meta property="twitter:title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta property="twitter:description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="twitter:image" content="https://i.ibb.co/SXP8fGc/OG-image.jpg" />
            </Head>
            <Layout>
                <section className="section" style={{ marginTop: '90px' }}>
                    <div className="container p4 p-lg-5">
                        <div className="row">
                            <div className="col">
                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb">
                                        <li className="breadcrumb-item">
                                            <Link href="/">
                                                <a className="link">Home</a>
                                            </Link>
                                        </li>
                                        <li className="breadcrumb-item">
                                            <Link href="/forum">
                                                <a className="link">Forum</a>
                                            </Link>
                                        </li>
                                        <li className="breadcrumb-item active" aria-current="page">{thread.detail.subject || 'Forum'}</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6">
                                <h1 className="section-title h3">Diskusi {thread.detail.name}</h1>
                            </div>
                            <div className="col-md-6 text-md-end">
                                {thread.detail.mark === 'solved' && (
                                    <h5 className="text-secondary"><FontAwesomeIcon className="text-primary" icon={faCheckCircle} /> Selesai</h5>
                                )}
                            </div>
                        </div>
                        <div className="row mb-5">
                            <div className="col">
                                <CardForum
                                    avatar={thread.detail.avatar}
                                    name={thread.detail.name}
                                    date={thread.detail.date}
                                    lessonTitle={thread.detail.lesson_title}
                                    subject={thread.detail.subject}
                                    content={thread.detail.thread_content}
                                />
                            </div>
                        </div>
                        <div className="row mb-3">
                            <div className="col">
                                <h3>{thread.replies.length} Jawaban</h3>
                            </div>
                            {thread.detail.mark !== 'solved' && (
                                <div className="col">
                                    <a href={`https://dashboard.codepolitan.com/login?redirect=learn/discussions/thread/${thread.detail.slug}`} className="btn btn-primary btn-sm float-end">
                                        <FontAwesomeIcon className="me-1" icon={faComment} />
                                        Bantu Jawab
                                    </a>
                                </div>
                            )}
                        </div>
                        <div className="row">
                            <div className="col">
                                {thread.replies.length < 1 && (
                                    <div className="text-center my-5">
                                        <img style={{ opacity: 0.3 }} height="80" width="auto" src="/assets/img/home/cp-logo/cp-1.png" alt="Belum ada jawaban" />
                                        <p className="text-muted mt-3">Belum ada jawaban</p>
                                    </div>
                                )}
                                {thread.replies.map((item, index) => {
                                    return (
                                        <div className="card mb-3" key={index}>
                                            <div className="card-body">
                                                <div className="d-flex align-items-start">
                                                    <div className="mt-1">
                                                        <img style={{ height: '75px', width: '75px', objectFit: 'cover' }} className="rounded-circle" src={item.avatar || "/assets/img/icons/icon-avatar.png"} alt="Avatar" />
                                                    </div>
                                                    <div className="ms-4 mt-2">
                                                        <h5 className="h5 text-secondary">{item.name}</h5>
                                                        <p className="text-muted m-0">{moment(item.date).fromNow()}</p>
                                                    </div>
                                                    {item.mark === 'choosen' && (
                                                        <div className="ms-auto">
                                                            <div className="d-none d-lg-block">
                                                                <span className="badge rounded-pill p-3 bg-light text-secondary"><FontAwesomeIcon className="text-primary" icon={faThumbsUp} /> Jawaban Terpilih</span>
                                                            </div>
                                                            <div className="d-lg-none">
                                                                <span className="badge rounded-pill p-3 bg-light text-secondary"><FontAwesomeIcon className="text-primary" size="lg" icon={faThumbsUp} /></span>
                                                            </div>
                                                        </div>
                                                    )}
                                                </div>
                                                <div className="mt-3">
                                                    <ReactMarkdown
                                                        remarkPlugins={[remarkGfm]}
                                                        rehypePlugins={[rehypeRaw]}
                                                        components={CodeBlock}
                                                    >
                                                        {item.content}
                                                    </ReactMarkdown>
                                                </div>
                                            </div>
                                            {item.comments.length !== 0 && (
                                                <div className="card-footer">
                                                    {item.comments.map((comment, index) => {
                                                        return (
                                                            <div className="ms-lg-5" key={index}>
                                                                <CardForum
                                                                    avatar={comment.avatar}
                                                                    name={comment.name}
                                                                    date={comment.date}
                                                                    content={comment.comment_content}
                                                                />
                                                            </div>
                                                        );
                                                    })}
                                                </div>
                                            )}
                                        </div>
                                    );
                                })}
                            </div>
                        </div>
                    </div>
                </section>
            </Layout>
        </>
    );
};

export default ForumDetail;
