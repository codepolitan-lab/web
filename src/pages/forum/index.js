import { useState } from "react";
import axios from "axios";
import Head from "next/head";
import CardForum from "../../components/global/Cards/CardForum/CardForum";
import Layout from "../../components/global/Layout/Layout";
import Link from "next/link";

export const getServerSideProps = async () => {
    const { data } = await axios.get(`${process.env.NEXT_PUBLIC_APPS_URL}/api/forum/thread`);
    const response = { data };

    const threads = response.data;

    return {
        props: {
            threads
        },
        // revalidate: 10
    }
};

const Forum = ({ threads }) => {
    const [limit, setLimit] = useState(10);

    const all = threads;
    const answered = threads.filter(thread => thread.total_answer !== '0' && thread.mark !== 'solved');
    const unanswered = threads.filter(thread => thread.total_answer === '0' && thread.mark !== 'solved');
    const solved = threads.filter(thread => thread.mark === 'solved');

    return (
        <>
            <Head>
                <title>Forum - Codepolitan</title>
                <meta name="title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta name="description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="og:type" content="website" />
                <meta property="og:url" content="https://codepolitan.com" />
                <meta property="og:title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta property="og:description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="og:image" content="https://i.ibb.co/SXP8fGc/OG-image.jpg" />
                <meta property="twitter:card" content="summary_large_image" />
                <meta property="twitter:url" content="https://codepolitan.com" />
                <meta property="twitter:title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta property="twitter:description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="twitter:image" content="https://i.ibb.co/SXP8fGc/OG-image.jpg" />
            </Head>
            <Layout>
                <section className="section" style={{ marginTop: '100px' }}>
                    <div className="container p-4 p-lg-5">
                        <div className="row">
                            <div className="col text-center">
                                <h1 className="section-title mb-4">Tak Perlu Khawatir Saat Mengalami <br /> <span className="text-primary">Kendala</span> dalam Belajar</h1>
                                <p className="lead text-muted">Tanyakan kendala belajarmu di Forum, tim Codepolitan <br /> dan seluruh member akan membantumu</p>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="section">
                    <div className="container p-4 p-lg-5">
                        <div className="row mb-4">
                            <div className="col">
                                <h2 className="section-title h3">Forum Diskusi</h2>
                            </div>
                        </div>
                        <div className="row">
                            {/* Main */}
                            <div className="col-lg-9">
                                <div>
                                    <ul className="nav nav-tabs nav-justified d-grid d-lg-flex" id="ForumTab" role="tablist">
                                        <li className="nav-item" role="presentation">
                                            <button onClick={() => setLimit(10)} className="nav-link active" id="all-tab" data-bs-toggle="tab" data-bs-target="#all" type="button" role="tab" aria-controls="all" aria-selected="true">Terbaru ({all.length})</button>
                                        </li>
                                        <li className="nav-item" role="presentation">
                                            <button onClick={() => setLimit(10)} className="nav-link" id="answered-tab" data-bs-toggle="tab" data-bs-target="#answered" type="button" role="tab" aria-controls="answered" aria-selected="false">Terjawab ({answered.length})</button>
                                        </li>
                                        <li className="nav-item" role="presentation">
                                            <button onClick={() => setLimit(10)} className="nav-link" id="unanswered-tab" data-bs-toggle="tab" data-bs-target="#unanswered" type="button" role="tab" aria-controls="unanswered" aria-selected="false">Belum Terjawab ({unanswered.length})</button>
                                        </li>
                                        <li className="nav-item" role="presentation">
                                            <button onClick={() => setLimit(10)} className="nav-link" id="solved-tab" data-bs-toggle="tab" data-bs-target="#solved" type="button" role="tab" aria-controls="solved" aria-selected="false">Selesai ({solved.length})</button>
                                        </li>
                                    </ul>
                                    <div className="tab-content" id="ForumTabContent">
                                        <div className="tab-pane fade show active" id="all" role="tabpanel" aria-labelledby="all-tab">
                                            {all.map((thread, index) => {
                                                return (
                                                    <Link href={`/forum/thread/${thread.slug}`} key={index}>
                                                        <a className="link">
                                                            <CardForum
                                                                mark={thread.mark}
                                                                avatar={thread.avatar}
                                                                name={thread.name}
                                                                date={thread.date}
                                                                totalAnswer={thread.total_answer}
                                                                lessonTitle={thread.lesson_title}
                                                                subject={thread.subject}
                                                            />
                                                        </a>
                                                    </Link>
                                                );
                                            }).slice(0, limit)}
                                            {
                                                all.length <= limit ? ('') : (
                                                    <div className="mt-5 text-center">
                                                        <button onClick={() => setLimit(limit + 10)} className="btn btn-primary">Show More</button>
                                                    </div>
                                                )
                                            }
                                        </div>
                                        <div className="tab-pane fade" id="answered" role="tabpanel" aria-labelledby="answered-tab">
                                            {answered.map((thread, index) => {
                                                return (
                                                    <Link href={`/forum/thread/${thread.slug}`} key={index}>
                                                        <a className="link">
                                                            <CardForum
                                                                mark={thread.mark}
                                                                avatar={thread.avatar}
                                                                name={thread.name}
                                                                date={thread.date}
                                                                totalAnswer={thread.total_answer}
                                                                lessonTitle={thread.lesson_title}
                                                                subject={thread.subject}
                                                            />
                                                        </a>
                                                    </Link>
                                                );
                                            }).slice(0, limit)}
                                            {
                                                answered.length <= limit ? ('') : (
                                                    <div className="mt-5 text-center">
                                                        <button onClick={() => setLimit(limit + 10)} className="btn btn-primary">Show More</button>
                                                    </div>
                                                )
                                            }
                                        </div>
                                        <div className="tab-pane fade" id="unanswered" role="tabpanel" aria-labelledby="unanswered-tab">
                                            {unanswered.length < 1 && (
                                                <div className="my-5 text-center">
                                                    <img style={{ opacity: 0.2, width: '100px' }} className="mb-3" src="/assets/img/home/cp-logo/cp-1.png" alt="Tidak ada konten" />
                                                    <p className="text-muted">Thread tidak tersedia</p>
                                                </div>
                                            )}
                                            {unanswered.map((thread, index) => {
                                                return (
                                                    <Link href={`/forum/thread/${thread.slug}`} key={index}>
                                                        <a className="link">
                                                            <CardForum
                                                                mark={thread.mark}
                                                                avatar={thread.avatar}
                                                                name={thread.name}
                                                                date={thread.date}
                                                                totalAnswer={thread.total_answer}
                                                                lessonTitle={thread.lesson_title}
                                                                subject={thread.subject}
                                                            />
                                                        </a>
                                                    </Link>
                                                );
                                            }).slice(0, limit)}
                                            {
                                                unanswered.length <= limit ? ('') : (
                                                    <div className="mt-5 text-center">
                                                        <button onClick={() => setLimit(limit + 10)} className="btn btn-primary">Show More</button>
                                                    </div>
                                                )
                                            }
                                        </div>
                                        <div className="tab-pane fade" id="solved" role="tabpanel" aria-labelledby="solved-tab">
                                            {solved.map((thread, index) => {
                                                return (
                                                    <Link href={`/forum/thread/${thread.slug}`} key={index}>
                                                        <a className="link">
                                                            <CardForum
                                                                mark={thread.mark}
                                                                avatar={thread.avatar}
                                                                name={thread.name}
                                                                date={thread.date}
                                                                totalAnswer={thread.total_answer}
                                                                lessonTitle={thread.lesson_title}
                                                                subject={thread.subject}
                                                            />
                                                        </a>
                                                    </Link>
                                                );
                                            }).slice(0, limit)}
                                            {
                                                solved.length <= limit ? ('') : (
                                                    <div className="mt-5 text-center">
                                                        <button onClick={() => setLimit(limit + 10)} className="btn btn-primary">Show More</button>
                                                    </div>
                                                )
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/* End of Main */}

                            {/* Sidebar */}
                            <div className="col-lg-3 order-first order-lg-last mb-5">
                                <div className="sticky-top" style={{ top: '100px' }}>
                                    <div className="d-grid gap-2">
                                        <a className="btn btn-primary" href="https://apps.codepolitan.com/user/login?callback=discussion">Tanyakan disini</a>
                                    </div>
                                    <div className="card mt-4 border-0 shadow-sm rounded">
                                        <div className="card-body">
                                            <h5 className="card-title">Trending Topic</h5>
                                            <ol>
                                                <li>Android</li>
                                                <li>JavaScript</li>
                                                <li>Vue</li>
                                                <li>Laravel</li>
                                                <li>React</li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/* End of Sidebar */}
                        </div>
                    </div>
                </section>
            </Layout>
        </>
    );
};

export default Forum;
