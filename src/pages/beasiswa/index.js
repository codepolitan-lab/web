import axios from 'axios';
import Head from 'next/head';
import Banner from '../../components/global/Banner/Banner';
import CardBeasiswa from '../../components/global/Cards/CardBeasiswa/CardBeasiswa';
import Layout from '../../components/global/Layout/Layout';

export const getServerSideProps = async () => {
    const { data } = await axios.get(`${process.env.NEXT_PUBLIC_APPS_URL}/api/entry/index/beasiswa?perpage=100`);
    const response = { data };

    const scholarships = response.data.results;

    return {
        props: {
            scholarships
        },
    }
}

const Beasiswa = ({ scholarships }) => {
    return (
        <>
            <Head>
                <title>Beasiswa - Codepolitan</title>
                <meta name="title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta name="description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="og:type" content="website" />
                <meta property="og:url" content="https://codepolitan.com" />
                <meta property="og:title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta property="og:description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="og:image" content="https://i.ibb.co/SXP8fGc/OG-image.jpg" />
                <meta property="twitter:card" content="summary_large_image" />
                <meta property="twitter:url" content="https://codepolitan.com" />
                <meta property="twitter:title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta property="twitter:description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="twitter:image" content="https://i.ibb.co/SXP8fGc/OG-image.jpg" />
            </Head>
            <Layout>
                <Banner
                    background='/assets/img/backgrounds/banner-beasiswa.png'
                    title="Beasiswa Belajar Coding"
                    subtitle="Dapatkan Kesempatan Belajar Coding Gratis Melalui Program Beasiswa CODEPOLITAN"
                />
                <section className="section" id="beasiswa">
                    <div className="container p-4 p-lg-5">
                        <div className="row mb-4">
                            <div className="col">
                                <h2 className="section-title">Program Beasiswa</h2>
                                <p className="text-muted">Program kerjasama codepolitan dan partner bisnis yang tersedia</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col">
                                <div>
                                    {/* Mobile */}
                                    <ul className="nav nav-tabs nav-justified d-grid d-md-flex d-lg-none" id="BeasiswaTab" role="tablist">
                                        <li className="nav-item" role="presentation">
                                            <button className="nav-link active" id="all-tab" data-bs-toggle="tab" data-bs-target="#all" type="button" role="tab" aria-controls="all" aria-selected="true">Masih Berlangsung</button>
                                        </li>
                                        <li className="nav-item" role="presentation">
                                            <button className="nav-link" id="solved-tab" data-bs-toggle="tab" data-bs-target="#solved" type="button" role="tab" aria-controls="solved" aria-selected="false">Sudah Berakhir</button>
                                        </li>
                                    </ul>
                                    {/* End of Mobile */}
                                    <ul className="nav nav-tabs d-none d-lg-flex" id="BeasiswaTab" role="tablist">
                                        <li className="nav-item" role="presentation">
                                            <button className="nav-link active" id="all-tab" data-bs-toggle="tab" data-bs-target="#all" type="button" role="tab" aria-controls="all" aria-selected="true">Masih Berlangsung</button>
                                        </li>
                                        <li className="nav-item" role="presentation">
                                            <button className="nav-link" id="solved-tab" data-bs-toggle="tab" data-bs-target="#solved" type="button" role="tab" aria-controls="solved" aria-selected="false">Sudah Berakhir</button>
                                        </li>
                                    </ul>
                                    <div className="tab-content" id="ForumTabContent">
                                        <div className="tab-pane fade show active" id="all" role="tabpanel" aria-labelledby="all-tab">
                                            <div className="row my-3">
                                                {scholarships.filter((scholarship) => scholarship.status !== 'close').map((scholarship, index) => {
                                                    return (
                                                        <div className="col-md-6 col-lg-4 mb-3" key={index}>
                                                            <CardBeasiswa
                                                                thumbnail={scholarship.image}
                                                                organizer={scholarship.provider}
                                                                title={scholarship.title}
                                                                description={scholarship.description}
                                                                closedAt={scholarship.date}
                                                                participats={scholarship.participant}
                                                                link={scholarship.status !== 'close' && scholarship.link}
                                                            />
                                                        </div>
                                                    );
                                                }).reverse()}
                                            </div>
                                        </div>
                                        <div className="tab-pane fade" id="solved" role="tabpanel" aria-labelledby="solved-tab">
                                            <div className="row my-3">
                                                {scholarships.filter((scholarship) => scholarship.status === 'close').map((scholarship, index) => {
                                                    return (
                                                        <div className="col-md-6 col-lg-4 mb-3" key={index}>
                                                            <CardBeasiswa
                                                                thumbnail={scholarship.image}
                                                                organizer={scholarship.provider}
                                                                title={scholarship.title}
                                                                description={scholarship.description}
                                                                closedAt={scholarship.date}
                                                                participats={scholarship.participant}
                                                                link={scholarship.status !== 'close' && scholarship.link}
                                                            />
                                                        </div>
                                                    );
                                                }).reverse()}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </Layout>
        </>
    );
};

export default Beasiswa;
