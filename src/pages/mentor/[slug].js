import axios from 'axios';
import Head from 'next/head';
import Link from 'next/link';
import { SwiperSlide } from 'swiper/react';
import CardRoadmap from '../../components/global/Cards/CardRoadmap/CardRoadmap';
import CardCourse from '../../components/global/Cards/CardCourse/CardCourse';
import Layout from '../../components/global/Layout/Layout';
import Banner from '../../components/mentor/Banner/Banner';
import CardSidebar from '../../components/mentor/Cards/CardSidebar/CardSidebar';
import SectionSlider from '../../components/global/Sections/SectionSlider/SectionSlider';
import { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faRedo } from '@fortawesome/free-solid-svg-icons';

export const getServerSideProps = async (context) => {
    const { slug } = context.query;
    const mentorRes = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/v1/mentor/${slug}`);
    const roadmapsRes = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/v1/roadmap?page=1&limit=200&filter=mentor_username[eq]${slug}`);
    const coursesRes = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/course?page=1&limit=200&filter=mentor_username[eq]${slug}`);

    const [mentor, roadmaps, courses] = await Promise.all([
        mentorRes.data,
        roadmapsRes.data,
        coursesRes.data,
    ]);
    return {
        props: {
            mentor,
            roadmaps,
            courses
        },
    }
}

const MentorDetail = ({ mentor, roadmaps, courses }) => {

    const [roadmapLimit, setRoadmapLimit] = useState(6);
    const [courseLimit, setCourseLimit] = useState(6);
    const [slide] = useState({
        desktop: 2.3,
        tab: 2,
    })

    return (
        <>
            <Head>
                <title>{mentor.brand_name} - Codepolitan</title>
                <meta name="title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta name="description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="og:type" content="website" />
                <meta property="og:url" content="https://codepolitan.com" />
                <meta property="og:title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta property="og:description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="og:image" content="https://i.ibb.co/SXP8fGc/OG-image.jpg" />
                <meta property="twitter:card" content="summary_large_image" />
                <meta property="twitter:url" content="https://codepolitan.com" />
                <meta property="twitter:title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta property="twitter:description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="twitter:image" content="https://i.ibb.co/SXP8fGc/OG-image.jpg" />
            </Head>
            <Layout>
                <section className="section" style={{ marginTop: '50px', paddingTop: '30px', background: `linear-gradient(180deg, #F8F8F8 300px, #fff 100px)` }}>
                    <div className="container p-4 p-lg-5">
                        <div className="row">
                            {/* Sidebar */}
                            <div className="col-lg-3">
                                <div className="sticky-top" style={{ top: '100px' }}>
                                    <CardSidebar
                                        thumbnail={mentor.brand_image || mentor.avatar}
                                        totalCourse={mentor.total_course}
                                        totalRoadmap={mentor.total_roadmap}
                                        totalCourseStudent={mentor.total_course_student}
                                        totalRoadmapStudent={mentor.total_roadmap_student}
                                    />
                                </div>
                            </div>
                            {/* End of Sidebar */}

                            {/* Main */}
                            <div className="col-md-8 offset-lg-1 py-5">
                                <Banner
                                    brandName={mentor.brand_name || mentor.name}
                                    description={mentor.brand_description || mentor.short_description}
                                />
                                {!roadmaps.error && (
                                    <>
                                        {/* Mobile */}
                                        <SectionSlider
                                            title="Beragam Roadmap Belajar"
                                            subtitle="Belajar terarah dengan roadmap belajar"
                                            slidesPerView={3.1}
                                            mobileOnly
                                        >
                                            {!roadmaps.error && roadmaps.map((roadmap, index) => {
                                                return (
                                                    <SwiperSlide key={index}>
                                                        <Link href={`/roadmap/${roadmap.slug}`}>
                                                            <a className="link">
                                                                <CardRoadmap
                                                                    thumbnail={roadmap.image}
                                                                    icon={roadmap.small_icon}
                                                                    author={roadmap.author}
                                                                    title={roadmap.name}
                                                                    level={roadmap.level}
                                                                    totalCourse={roadmap.total_course}
                                                                    totalStudent={roadmap.total_student}
                                                                    rate={roadmap.rate}
                                                                />
                                                            </a>
                                                        </Link>
                                                    </SwiperSlide>
                                                );
                                            })}
                                        </SectionSlider>
                                        {/* End of Mobile */}

                                        {/* Large Screen */}
                                        <section className="section my-5 pt-5 d-none d-lg-block">
                                            <div className="row">
                                                <div className="col-auto">
                                                    <h5 className="section-title">Beragam Roadmap Belajar</h5>
                                                    <p className="text-muted">Belajar terarah dengan roadmap belajar</p>
                                                </div>
                                            </div>
                                            <div className="row">
                                                {!roadmaps.error && roadmaps.map((roadmap, index) => {
                                                    return (
                                                        <div className="col-lg-4 p-2" key={index}>
                                                            <Link href={`/roadmap/${roadmap.slug}`}>
                                                                <a className="link">
                                                                    <CardRoadmap
                                                                        thumbnail={roadmap.image}
                                                                        icon={roadmap.small_icon}
                                                                        author={roadmap.author}
                                                                        title={roadmap.name}
                                                                        level={roadmap.level}
                                                                        totalCourse={roadmap.total_course}
                                                                        totalStudent={roadmap.total_student}
                                                                        rate={roadmap.rate}
                                                                    />
                                                                </a>
                                                            </Link>
                                                        </div>
                                                    );
                                                }).slice(0, roadmapLimit)}
                                            </div>
                                            {roadmaps.length >= roadmapLimit && (
                                                <div className="row my-3">
                                                    <div className="col text-center">
                                                        <button onClick={() => setRoadmapLimit(roadmapLimit + 6)} className="btn btn-light">
                                                            Show more
                                                            <FontAwesomeIcon size="sm" className="ms-2" icon={faRedo} />
                                                        </button>
                                                    </div>
                                                </div>
                                            )}
                                        </section>
                                        {/* End of Large Screen */}
                                    </>
                                )}

                                {!courses.error && (
                                    <>
                                        {/* Mobile */}
                                        <SectionSlider
                                            title="Beragam Kelas Belajar"
                                            subtitle="Pilih kelas sesuai dengan kebutuhanmu"
                                            slidesPerView={3.1}
                                            mobileOnly
                                        >
                                            {!courses.error && courses.map((course, index) => {
                                                return (
                                                    <SwiperSlide key={index}>
                                                        <Link href={`/course/intro/${course.slug}`}>
                                                            <a className="link">
                                                                <CardCourse
                                                                    thumbnail={course.thumbnail}
                                                                    author={course.author}
                                                                    title={course.title}
                                                                    level={course.level}
                                                                    totalStudents={course.total_student}
                                                                    totalModules={course.total_module}
                                                                    totalTimes={course.total_time}
                                                                    totalRating={course.total_rating}
                                                                    totalFeedback={course.total_feedback}
                                                                    normalBuyPrice={course.buy?.normal_price || course.normal_price}
                                                                    retailBuyPrice={course.buy?.retail_price || course.retail_price}
                                                                    normalRentPrice={course.rent?.normal_price}
                                                                    retailRentPrice={course.rent?.retail_price}
                                                                />
                                                            </a>
                                                        </Link>
                                                    </SwiperSlide>
                                                );
                                            })}
                                        </SectionSlider>
                                        {/* End of Mobile */}

                                        {/* Large Screen */}
                                        <section className="section my-5 pt-5 d-none d-lg-block">
                                            <div className="row">
                                                <div className="col-auto">
                                                    <h5 className="section-title">Beragam Kelas Belajar</h5>
                                                    <p className="text-muted">Pilih kelas sesuai dengan kebutuhanmu</p>
                                                </div>
                                            </div>
                                            <div className="row">
                                                {!courses.error && courses.map((course, index) => {
                                                    return (
                                                        <div className="col-lg-4 px-2 py-3" key={index}>
                                                            <Link href={`/course/intro/${course.slug}`}>
                                                                <a className="link">
                                                                    <CardCourse
                                                                        thumbnail={course.thumbnail}
                                                                        author={course.author}
                                                                        title={course.title}
                                                                        level={course.level}
                                                                        totalStudents={course.total_student}
                                                                        totalModules={course.total_module}
                                                                        totalTimes={course.total_time}
                                                                        totalRating={course.total_rating}
                                                                        totalFeedback={course.total_feedback}
                                                                        normalBuyPrice={course.buy?.normal_price || course.normal_price}
                                                                        retailBuyPrice={course.buy?.retail_price || course.retail_price}
                                                                        normalRentPrice={course.rent?.normal_price}
                                                                        retailRentPrice={course.rent?.retail_price}
                                                                    />
                                                                </a>
                                                            </Link>
                                                        </div>
                                                    );
                                                }).slice(0, courseLimit)}
                                            </div>
                                            {courses.length >= courseLimit && (
                                                <div className="row my-3">
                                                    <div className="col text-center">
                                                        <button onClick={() => setCourseLimit(courseLimit + 6)} className="btn btn-light">
                                                            Show more
                                                            <FontAwesomeIcon size="sm" className="ms-2" icon={faRedo} />
                                                        </button>
                                                    </div>
                                                </div>
                                            )}
                                        </section>
                                        {/* End of Large Screen */}
                                    </>
                                )}

                                {/* Show author more than 1 */}
                                {/* {
                                    mentor.authors.length > 1 && <SectionMentor brand={mentor.brand_name} authors={mentor.authors} slide={slide} />
                                } */}
                                {/* Show author more than 1 */}

                            </div>
                            {/* Main */}
                        </div>
                    </div>
                </section>
            </Layout>
        </>
    );
};

export default MentorDetail;
