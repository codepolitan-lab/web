import { useState } from 'react';
import axios from 'axios';
import Head from 'next/head';
import Link from 'next/link';
import Layout from '../../components/global/Layout/Layout';
import CardMentor from '../../components/home/Cards/CardMentor/CardMentor';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';

export const getServerSideProps = async () => {
    const response = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/v1/mentor?page=1&limit=100`);

    const mentors = response.data;

    return {
        props: { mentors },
    }
}

const MentorList = ({ mentors }) => {
    const [limit, setLimit] = useState(8);
    const [searchValue, setSearchValue] = useState("");

    return (
        <>
            <Head>
                <title>Mentor - Codepolitan</title>
                <meta name="title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta name="description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="og:type" content="website" />
                <meta property="og:url" content="https://codepolitan.com" />
                <meta property="og:title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta property="og:description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="og:image" content="https://i.ibb.co/SXP8fGc/OG-image.jpg" />
                <meta property="twitter:card" content="summary_large_image" />
                <meta property="twitter:url" content="https://codepolitan.com" />
                <meta property="twitter:title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta property="twitter:description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="twitter:image" content="https://i.ibb.co/SXP8fGc/OG-image.jpg" />
            </Head>
            <Layout>
                <section className="section" style={{ marginTop: '100px' }}>
                    <div className="container p-4 p-lg-5">
                        <div className="row justify-content-between">
                            <div className="col-auto">
                                <h1 className="section-title h2">Mentor-mentor di Codepolitan</h1>
                            </div>
                            <div className="col-12 col-lg-auto">
                                <div className="input-group">
                                    <input onChange={(e) => setSearchValue(e.target.value)} type="search" className="form-control shadow-none border-end-0" placeholder="Cari mentor..." />
                                    <button className="input-group-text bg-white text-muted" role="button" type="submit" title="Cari">
                                        <FontAwesomeIcon icon={faSearch} />
                                    </button>
                                </div>
                            </div>
                        </div>
                        <hr />
                        <div className="row">
                            {
                                mentors.filter((value) => {
                                    if (value.name.toLowerCase().includes(searchValue.toLowerCase())) {
                                        return value;
                                    }
                                }).length > 0
                                    ? mentors.filter((value) => {
                                        if (value.name.toLowerCase().includes(searchValue.toLowerCase())) {
                                            return value;
                                        }
                                    }).map((mentor, index) => {
                                        return (
                                            <div className="col-md-6 col-lg-3 p-4" key={index}>
                                                <Link href={`/mentor/${mentor.username}`}>
                                                    <a className="link">
                                                        <CardMentor
                                                            thumbnail={mentor.avatar}
                                                            name={mentor.name}
                                                            jobs={mentor.jobs}
                                                            description={mentor.short_description}
                                                            brandImage={mentor.brand_image}
                                                        />
                                                    </a>
                                                </Link>
                                            </div>
                                        );
                                    }).slice(0, limit)
                                    : (<h3 className="text-center text-muted">Hasil pencarian tidak ditemukan..</h3>)
                            }
                        </div>
                        {mentors.filter((value) => {
                            if (searchValue === "") {
                                return value;
                            } else if (value.name.toLowerCase().includes(searchValue.toLowerCase())) {
                                return value;
                            }
                        }).length > limit && (
                                <div className="row my-4">
                                    <div className="col text-center">
                                        <button onClick={() => setLimit(limit + 8)} className="btn btn-outline-primary">Mentor Lainnya</button>
                                    </div>
                                </div>
                            )}
                    </div>
                </section>
                <section className="section bg-codepolitan text-white">
                    <div className="container p-4 p-lg-5">
                        <div className="row p-5 p-lg-auto">
                            <div className="col-lg-6 mb-3 mb-lg-auto">
                                <img className="img-fluid d-block mx-auto" src="/assets/img/cta-mentor-loop.png" alt="Tertarik Menjadi Mentor" />
                            </div>
                            <div className="col-lg-6 my-auto text-center text-lg-start">
                                <h2 className="section-title text-white mb-4">Tertarik Menjadi Mentor?</h2>
                                <Link href={'/for-mentor'}>
                                    <a className="btn btn-outline-light btn-lg">Pelajari Cara Menjadi Mentor</a>
                                </Link>
                            </div>
                        </div>
                    </div>
                </section>
            </Layout>
        </>
    );
};

export default MentorList;
