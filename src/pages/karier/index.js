import Head from "next/head";
import Layout from "../../components/global/Layout/Layout";
import SectionCTA from "../../components/global/Sections/SectionCTA/SectionCTA";
import SectionPartners from "../../components/global/Sections/SectionPartners/SectionPartners";
import SectionRoadmapList from "../../components/global/Sections/SectionRoadmapList/SectionRoadmapList";
import Hero from "../../components/karier/Hero/Hero";
import SectionHowItHelps from "../../components/global/Sections/SectionHowItHelps/SectionHowItHelps";
// import SectionJobList from "../../components/karier/Sections/SectionJobList/SectionJobList";
import SectionSteps from "../../components/karier/Sections/SectionSteps/SectionSteps";
// import SectionTestimonyLanding from "../../components/global/Sections/SectionTestimonyLanding/SectionTestimonyLanding";

const Karier = () => {
    // const testimonials = [
    //     {
    //         name: 'Soni',
    //         thumbnail: 'https://i.ibb.co/QPKMdjk/image2.jpg',
    //         role: 'Ketua Program Studi',
    //         organization: 'Universitas Respati Indonesia',
    //         content: 'Mahasiswa lebih aplikatif dalam implementasi pada hal yang berkaitan dengan matkul, Mendapatkan tambahan keahlian sesuai dengan bidang ilmu, Menambah sertifikat keahlian yang dapat digunakan dalam mencari pekerjaan'
    //     },
    //     {
    //         name: 'Evanita Veronica Manullang',
    //         thumbnail: 'https://i.ibb.co/q039Hz0/image1.jpg',
    //         role: 'Kepala Laboratorium FIKOM',
    //         organization: 'Universitas Sains dan Teknologi Jayapura',
    //         content: 'Kemampuan mahasiswa menjadi semakin baik dengan bantuan video-video pembelajaran yang terstruktur, terlebih disaat pandemi covid 19. Mahasiswa yang mengikuti program kampus coding sebagian besar sudah dapat membuat aplikasi web sendiri. '
    //     },
    // ];
    const contents = [
        {
            image: '/assets/img/icons/icon-article.png',
            title: 'Talent Melengkapi Portfolio',
            description: 'Lengkapi portfolio Anda melalui halaman Karier CodePolitan agar kami dapat merekomendasikanmu ke perusahaan yang tepat'
        },
        {
            image: '/assets/img/icons/icon-review.png',
            title: 'Perusahaan Request Talent',
            description: 'Perusahaan akan mencari kandidat terbaik di database talent CodePolitan sesuai dengan bidang pekerjaan yang dipilih'
        },
        {
            image: '/assets/img/icons/icon-certificate.png',
            title: 'CodePolitan Mencocokan Perusahaan Dengan Talent',
            description: 'CodePolitan akan mengirimkan notifikasi penawaran kepada Anda bila ada perusahaan yang tertarik untuk terhubung dengan Anda'
        },
    ]

    return (
        <>
            <Head>
                <title>Codepolitan Karier - Bangun Karier Anda di Tempat yang Tepat</title>
                <meta name="title" content="Codepolitan Karier - Bangun Karier Anda di Tempat yang Tepat" />
                <meta name="description" content="Program Karier dari CodePolitan membantu Anda dalam membangun dan meningkatkan karier dengan memilihkan perusahaan yang tepat dengan skill yang Anda miliki " />
                <meta property="og:type" content="website" />
                <meta property="og:url" content="https://codepolitan.com/karier" />
                <meta property="og:title" content="Codepolitan Karier - Bangun Karier Anda di Tempat yang Tepat" />
                <meta property="og:description" content="Program Karier dari CodePolitan membantu Anda dalam membangun dan meningkatkan karier dengan memilihkan perusahaan yang tepat dengan skill yang Anda miliki " />
                <meta property="og:image" content="https://i.ibb.co/W5dGnt8/OG-Karier.png" />
                <meta property="twitter:card" content="summary_large_image" />
                <meta property="twitter:url" content="https://codepolitan.com/karier" />
                <meta property="twitter:title" content="Codepolitan Karier - Bangun Karier Anda di Tempat yang Tepat" />
                <meta property="twitter:description" content="Program Karier dari CodePolitan membantu Anda dalam membangun dan meningkatkan karier dengan memilihkan perusahaan yang tepat dengan skill yang Anda miliki " />
                <meta property="twitter:image" content="https://i.ibb.co/W5dGnt8/OG-Karier.png" />
            </Head>
            <Layout>
                <Hero />
                <SectionPartners
                    backgroundColor="bg-white"
                    sectionTitleTextStyle="text-center"
                    customTItle="Telah Dipercaya Oleh"
                />
                <SectionHowItHelps
                    titleSection="Bagaimana Program Karier CodePolitan dapat Membantu Anda"
                    contents={contents}
                />
                <SectionRoadmapList
                    title="Bidang Pekerjaan yang Dapat Kami Hubungkan"
                    subtitle="Berikut beberapa bidang pekerjaan yang paling banyak dicari mitra kami"
                />
                <SectionSteps />
                {/*<SectionJobList />*/}
                {/* <SectionTestimonyLanding
                    title="Apa yang Mereka Katakan?"
                    data={testimonials}
                />*/}
                <SectionCTA
                    background="bg-light"
                    caption="Mulai dengan melengkapi profil dan portfolio Anda disini!"
                    btnTitle="Isi Formulir Sekarang"
                    link="https://apps.codepolitan.com/user/login?callback=karier"
                />
            </Layout>
        </>
    );
};

export default Karier;
