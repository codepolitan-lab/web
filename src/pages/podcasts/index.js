
import Head from 'next/head';
import Banner from '../../components/global/Banner/Banner';
import Layout from '../../components/global/Layout/Layout';
import CardPodcast from '../../components/podcasts/CardPodcast/CardPodcast';

const Podcasts = () => {
    const podcasts = [
        {
            videoId: 'G892z8Toyg8',
            title: 'Cerita Sandhika Galih: Awalnya buat mahasiswa, eh tapi kok malah - Podcast Codepolitan',
            description: 'Podcast CodePolitan kali ini kedatangan tamu spesial. Berbincang dengan Sandhika Galih, Pemilik Channel Programmer Unpas.',
        },
        {
            videoId: 'vxwvyHJdQgU',
            title: 'Cerita Ahmad Syarif - CEO Cyber Labs: Lulusan SMK bisa bikin perusahaan? Yaa Gitu Ceritanya',
            description: 'Podcast CodePolitan kali ini kedatangan tamu spesial. Berbincang dengan Ahmad Syarif, Pemilik Perusahaan Cyber Labs. Dimulai dari Awal mula ia lulus dari SMK dan cerita awal membuat perusahaan teknologi.',
        },
        {
            videoId: 'Ou7Nl0twAU4',
            title: 'Cerita Apookat - Fadhil Dzikri M: Bikin Startup Sampai Dicariin Dosen - Podcast Codepolitan',
            description: 'Podcast CodePolitan kali ini kedatangan tamu spesial. Berbincang dengan Fadhil Dzikri Muhammad, Founder dan CEO Apookat. Dimulai dari ia membuat perusahaan startup sampai dicari dosen karena tidak masuk sampai cerita awal membuat startup',
        },
    ];

    return (
        <>
            <Head>
                <title>Podcasts - Codepolitan</title>
                <meta name="title" content="Podcast Developer Indonesia - Codepolitan" />
                <meta name="description" content="Dengarkan podcast bertema Teknologi Informasi dan Pemrograman untuk meng-update wawasanmu. Dipersembahkan oleh Codepolitan" />
                <meta property="og:type" content="website" />
                <meta property="og:url" content="https://codepolitan.com/podcasts" />
                <meta property="og:title" content="Podcast Developer Indonesia - Codepolitan" />
                <meta property="og:description" content="Dengarkan podcast bertema Teknologi Informasi dan Pemrograman untuk meng-update wawasanmu. Dipersembahkan oleh Codepolitan" />
                <meta property="og:image" content="https://image.web.id/images/2022/07/11/157e746602c2a7f3db1e7ed6c192f4f4.jpg" />
                <meta property="twitter:card" content="summary_large_image" />
                <meta property="twitter:url" content="https://codepolitan.com/podcasts" />
                <meta property="twitter:title" content="Podcast Developer Indonesia - Codepolitan" />
                <meta property="twitter:description" content="Dengarkan podcast bertema Teknologi Informasi dan Pemrograman untuk meng-update wawasanmu. Dipersembahkan oleh Codepolitan" />
                <meta property="twitter:image" content="https://image.web.id/images/2022/07/11/157e746602c2a7f3db1e7ed6c192f4f4.jpg" />
            </Head>
            <Layout>
                <Banner
                    background='/assets/img/backgrounds/banner-podcasts.jpg'
                    title="Podcasts"
                    subtitle="Tetap update dengan mendengarkan Podcast menarik seputar pemrograman!"
                />
                <section className="section">
                    <div className="container p-4 p-lg-5">
                        <h2 className="section-title my-5">Podcast terbaru</h2>
                        <div className="row mb-5">
                            {podcasts?.map((podcast) => {
                                return (
                                    <div className="col-12 mb-4" key={podcast.videoId}>
                                        <CardPodcast
                                            thumbnail={`https://img.youtube.com/vi/${podcast.videoId}/hqdefault.jpg`}
                                            title={podcast.title}
                                            description={podcast.description}
                                            link={`https://youtu.be/${podcast.videoId}`}
                                        />
                                    </div>
                                );
                            }).reverse()}
                        </div>
                    </div>
                </section>
            </Layout>
        </>
    );
};

export default Podcasts;
