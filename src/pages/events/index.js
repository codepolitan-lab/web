import axios from 'axios';
import Head from 'next/head';
import Layout from '../../components/global/Layout/Layout';
import SectionEvent from '../../components/events/Section/SectionEvent/SectionEvent';

export const getServerSideProps = async () => {
    const eventsRes = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/v1/webinar/latest?page=1&limit=100`);
    const events = eventsRes.data;

    return {
        props: {
            events,
        },
    }
}

const Events = ({ events }) => {
    return (
        <>
            <Head>
                <title>Events - Codepolitan</title>
                <meta name="title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta name="description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="og:type" content="website" />
                <meta property="og:url" content="https://codepolitan.com" />
                <meta property="og:title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta property="og:description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="og:image" content="https://i.ibb.co/SXP8fGc/OG-image.jpg" />
                <meta property="twitter:card" content="summary_large_image" />
                <meta property="twitter:url" content="https://codepolitan.com" />
                <meta property="twitter:title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta property="twitter:description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="twitter:image" content="https://i.ibb.co/SXP8fGc/OG-image.jpg" />
            </Head>
            <Layout>
                <section className="section bg-light" style={{ paddingTop: '100px' }}>
                    <div className="container p-4 p-lg-5">
                        <SectionEvent
                            loading={false}
                            data={events}
                        />
                    </div>
                </section>
            </Layout>
        </>
    );
};

export default Events;
