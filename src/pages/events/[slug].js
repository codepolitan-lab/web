import axios from 'axios';
import Head from 'next/head';
import Layout from '../../components/global/Layout/Layout';
import parse from "html-react-parser"
import { removeTag } from '../../utils/helper';

export const getServerSideProps = async (context) => {
    const { slug } = context.query;
    const eventDetailRes = await axios.get(`${process.env.NEXT_PUBLIC_APPS_URL}/api/post/${slug}?field=slug`);
    const eventDetail = eventDetailRes.data;

    return {
        props: {
            eventDetail,
        },
    }
}

const EventDetail = ({ eventDetail }) => {
    return (
        <>
            <Head>
                <title>{eventDetail.title} - Codepolitan</title>
                <meta name="title" content={eventDetail.title} />
                <meta name="description" content={removeTag(eventDetail.content)} />
                <meta property="og:type" content="website" />
                <meta property="og:url" content={`https://codepolitan.com/events/${eventDetail.slug}`} />
                <meta property="og:title" content={eventDetail.title} />
                <meta property="og:description" content={removeTag(eventDetail.content)} />
                <meta property="og:image" content={eventDetail.featured_image} />
                <meta property="twitter:card" content="summary_large_image" />
                <meta property="twitter:url" content={`https://codepolitan.com/events/${eventDetail.slug}`} />
                <meta property="twitter:title" content={eventDetail.title} />
                <meta property="twitter:description" content={removeTag(eventDetail.content)} />
                <meta property="twitter:image" content={eventDetail.featured_image} />
            </Head>
            <Layout>
                <section className="section bg-grey text-muted" style={{ marginTop: '50px', paddingTop: '30px' }}>
                    <div className="container p-4 p-lg-5">
                        <div className="row">
                            <div className="col-lg-7">
                                {
                                    eventDetail.embed_video === ""
                                        ? (<img src={eventDetail.featured_image} className="w-100" alt={eventDetail.title} />)
                                        : (
                                            <div className="ratio ratio-4x3 shadow">
                                                <iframe className="rounded-0" style={{ borderRadius: '10px' }} src={"https://www.youtube.com/embed/" + eventDetail.embed_video} title="YouTube video player" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen />
                                            </div>

                                        )
                                }
                            </div>
                            <div className="col-lg-5 my-3 my-lg-0">
                                <h2>{eventDetail.title}</h2>
                                <p>{parse(eventDetail.content)}</p>
                            </div>
                        </div>
                    </div>
                </section>
            </Layout>
        </>
    );
};

export default EventDetail;
