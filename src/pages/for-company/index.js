import axios from "axios";
import Head from "next/head";
import Layout from "../../components/global/Layout/Layout";
import Hero from "../../components/forcompany/Hero/Hero";
import SectionCounter from "../../components/global/Sections/SectionCounter/SectionCounter";
import SectionPartners from "../../components/global/Sections/SectionPartners/SectionPartners";
import SectionCTA from "../../components/global/Sections/SectionCTA/SectionCTA";
import SectionTallentScout from "../../components/forcompany/Sections/SectionTallentScout/SectionTallentScout";
import SectionTechAdoption from "../../components/forcompany/Sections/SectionTechAdoption/SectionTechAdoption";
import SectionCorporateTraining from "../../components/forcompany/Sections/SectionCorporateTraining/SectionCorporateTraining";

export const getServerSideProps = async () => {
    const statsRes = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/v1/stats`);
    const stats = statsRes.data;

    return {
        props: {
            stats
        }
    };
};

const ForCompany = ({ stats }) => {
    return (
        <>
            <Head>
                <title>Codepolitan for Company - Codepolitan</title>
                <meta name="title" content="Codepolitan For Company - Codepolitan" />
                <meta name="description" content="Jual bisnis digital, mencari talent yang berbakat hingga meningkatkan kemampuan digital tim IT perusahaan anda bersama CodePolitan adalah langkah terbaik anda saat ini." />
                <meta property="og:type" content="website" />
                <meta property="og:url" content="https://codepolitan.com/for-company" />
                <meta property="og:title" content="Codepolitan For Company - Codepolitan" />
                <meta property="og:description" content="Jual bisnis digital, mencari talent yang berbakat hingga meningkatkan kemampuan digital tim IT perusahaan anda bersama CodePolitan adalah langkah terbaik anda saat ini." />
                <meta property="og:image" content="https://i.ibb.co/gTFrzNL/OG-For-Company.png" />
                <meta property="twitter:card" content="summary_large_image" />
                <meta property="twitter:url" content="https://codepolitan.com/for-company" />
                <meta property="twitter:title" content="Codepolitan For Company - Codepolitan" />
                <meta property="twitter:description" content="Jual bisnis digital, mencari talent yang berbakat hingga meningkatkan kemampuan digital tim IT perusahaan anda bersama CodePolitan adalah langkah terbaik anda saat ini." />
                <meta property="twitter:image" content="https://i.ibb.co/gTFrzNL/OG-For-Company.png" />
            </Head>
            <Layout>
                <Hero />
                <SectionCounter data={stats} />
                <SectionPartners
                    backgroundColor="bg-light"
                    sectionTitleTextStyle="text-center"
                />
                <SectionTechAdoption />
                <SectionCorporateTraining />
                <SectionTallentScout />
                {/* <SectionSliderTestimony
                    image="/assets/img/forcompany/Group 2447.png"
                    testimony="Codepolitan dapat menjadi solusi untuk meningkatkan kemampuan tim perusahaan anda dibidang digital, program ini dapat diselenggarakan di kantor Codepolitan, di kantor anda atau secara online intensif maupun ekstensif."
                    title="Nama Testimonial"
                    subtitle="Jabatan"
                /> */}
                <div className="bg-grey">
                    <SectionCTA
                        caption="Mulai Menjadi Partner Bisnis Kami Sekarang? Klik Disini"
                        btnTitle="Hubungi Kami Sekarang"
                        link="https://wa.me/628999488990"
                    />
                </div>
            </Layout>
        </>
    );
};

export default ForCompany;
