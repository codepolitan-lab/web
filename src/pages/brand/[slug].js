import { useEffect, useState } from 'react';
import Head from 'next/head';
import axios from 'axios';
import Layout from '../../components/global/Layout/Layout'
import Hero from '../../components/landing/Hero/Hero';
import SectionStats from '../../components/landing/Section/SectionStats/SectionStats';
import SectionHowItHelps from '../../components/global/Sections/SectionHowItHelps/SectionHowItHelps'
import SectionWhyStudyInCodepolitan from '../../components/landing/Section/SectionWhyStudyInCodepolitan/SectionWhyStudyInCodepolitan';
import SectionVideo from '../../components/landing/Section/SectionVideo/SectionVIdeo';
import SectionLangkahSukses from '../../components/landing/Section/SectionLangkahSukses/SectionLangkahSukses'
import SectionRoadmaps from '../../components/landing/Section/SectionRoadmaps/SectionRoadmaps'
import SectionCourses from '../../components/landing/Section/SectionCourses/SectionCourses'
import SectionManfaat from '../../components/landing/Section/SectionManfaat/SectionManfaat'
import SectionCTA from '../../components/landing/Section/SectionCTA/SectionCTA'
import SectionFaq from '../../components/global/Sections/SectionFaq/SectionFaq'
import SectionMentor from '../../components/landing/Section/SectionMentor/SectionMentor';

export const getStaticPaths = async () => {
    const response = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/v1/mentor?page=1&limit=10`);
    const mentors = response.data;

    const paths = await mentors.map(mentor => {
        return {
            params: {
                slug: mentor.brand_slug,
            }
        };
    });

    return {
        paths,
        fallback: "blocking"
    };
};

export const getStaticProps = async (context) => {
    const slug = context.params.slug;

    const brandRes = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/v1/brand/${slug}`);

    const brand = brandRes.data.result;

    return {
        props: {
            brand
        },
        revalidate: 10000
    };
};

const BrandLanding = ({ brand }) => {
    const [courses, setCourses] = useState([]);
    const [roadmaps, setRoadmaps] = useState([]);
    const [loadingCourses, setLoadingCourses] = useState(false);
    const [loadingRoadmaps, setLoadingRoadmaps] = useState(false);

    useEffect(() => {
        const getCourses = async () => {
            setLoadingCourses(true);
            try {
                const response = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/course?page=1&limit=200&filter=mentor_username[eq]${brand.username}`);
                setLoadingCourses(false);
                setCourses(response.data);
            } catch (error) {
                setLoadingCourses(true);
                throw new Error(error.message);
            };
        };
        getCourses();

        const getRoadmaps = async () => {
            setLoadingRoadmaps(true);
            try {
                const response = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/v1/roadmap?page=1&limit=200&filter=mentor_username[eq]${brand.username}`);
                setLoadingRoadmaps(false);
                setRoadmaps(response.data);
            } catch (error) {
                setLoadingRoadmaps(true);
                throw new Error(error.message);
            };
        };
        getRoadmaps();
    }, [brand]);

    const faq = [
        {
            id: 'flush-heading1',
            target: 'flush-collapse1',
            title: 'Bagaimana cara belajarnya?',
            content: 'Roadmap adalah kumpulan course yang disusun berdasarkan profesi keahlian IT. Misalnya frontend developer, maka isi dari kursus isinya materi frontend seperti HTML, CSS, Javascript, dan framework terkait. Dengan kata lain roadmap adalah sebuah jalur belajar(path).'
        },
        {
            id: 'flush-heading2',
            target: 'flush-collapse2',
            title: 'Bagaimana cara pembayarannya?',
            content: 'Bisa disesuaikan dengan kebutuhan. Jika kamu mempunyai target khusus profesi tertentu baiknya pilih roadmap dibanding membeli kursus satu per satu.'
        },
        {
            id: 'flush-heading3',
            target: 'flush-collapse3',
            title: 'Kalau saya ada pertanyaan, kemana saya harus bertanya?',
            content: 'Tentunya, untuk setiap roadmap yang dibeli akan ada update materi berkelanjutan.'
        },
        {
            id: 'flush-heading4',
            target: 'flush-collapse4',
            title: 'Apa yang dimaksud dengan garansi?',
            content: 'No content'
        },
        {
            id: 'flush-heading5',
            target: 'flush-collapse5',
            title: 'Bagaimana cara klaim garansi?',
            content: 'No content'
        },
        {
            id: 'flush-heading6',
            target: 'flush-collapse6',
            title: 'Apakah video bisa ditonton ulang setelah habis masa subscribe?',
            content: 'No content'
        },
        {
            id: 'flush-heading7',
            target: 'flush-collapse7',
            title: 'Apakah akan ada pembaharuan kelas?',
            content: 'No content'
        },
    ];

    const contents = [
        {
            image: '/assets/img/brand/icons/icon-step.png',
            title: 'Langkah Demi Langkah Instruksional',
            description: 'Terdapat puluha materi belajar dalam format teks dan video yang telah kami susun secara komprehensif untuk membantumu belajar lebih mudah.'
        },
        {
            image: '/assets/img/brand/icons/icon-forum.png',
            title: 'Forum Tanya Jawab Khusus Anggota',
            description: 'Kamu bisa bertanya seputar permasalahan belajarmu di forum tanya jawab yang telah disediakan, dan kami yang akan langsung menjawab'
        },
        {
            image: '/assets/img/brand/icons/icon-mentor.png',
            title: 'Pelajari Kelas Studi Kasus',
            description: 'Dengan mengikuti kelas studi kasus, kamu akan mendapatkan gambaran umum implementasi materi ke dalam sebuah proyek sebagai bekal untuk terjun di dunia kerja'
        },
    ]

    return (
        <>
            <Head>
                <title>{brand.brand_name} - Codepolitan</title>
                <meta name="title" content={`${brand.brand_headline} - Codepolitan`} />
                <meta name="description" content={brand.brand_description} />
                <meta property="og:type" content="website" />
                <meta property="og:url" content={`https://codepolitan.com`} />
                <meta property="og:title" content={brand.brand_headline} />
                <meta property="og:description" content={brand.brand_description} />
                <meta property="og:image" content="https://i.ibb.co/SXP8fGc/OG-image.jpg" />
                <meta property="twitter:card" content="summary_large_image" />
                <meta property="twitter:url" content={`https://codepolitan.com`} />
                <meta property="twitter:title" content={brand.brand_headline} />
                <meta property="twitter:description" content={brand.brand_description} />
                <meta property="twitter:image" content="https://i.ibb.co/SXP8fGc/OG-image.jpg" />
            </Head>
            <Layout>
                <Hero
                    brandLogo={brand.brand_logo}
                    brandName={brand.brand_name}
                    headline={brand.brand_headline}
                    description={brand.brand_description}
                    image={brand.brand_image}
                />
                <SectionStats data={brand.brand_statistic} />
                <SectionHowItHelps
                    titleSection="Bagaimana Cara Belajarnya?"
                    contents={contents}
                />
                <SectionWhyStudyInCodepolitan />
                <SectionVideo
                    author={brand.brand_author}
                    video={brand.brand_video}
                />
                <SectionLangkahSukses />
                {!roadmaps.error && !loadingRoadmaps && (
                    <SectionRoadmaps roadmaps={roadmaps} />
                )}
                {!courses.error && !loadingCourses && (
                    <SectionCourses courses={courses} />
                )}
                <SectionManfaat />
                {brand.authors.length > 0 && (
                    <SectionMentor brand={brand} authors={brand.authors} />
                )}
                <SectionFaq data={faq} />
                <SectionCTA />
            </Layout>
        </>
    );
};

export default BrandLanding;
