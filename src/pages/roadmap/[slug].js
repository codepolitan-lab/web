import { faShareAlt, faBuilding, faUserSecret } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import SectionRelatedCourse from "../../components/global/Sections/SectionRelatedCourse/SectionRelatedCourse";
import axios from "axios";
import Layout from "../../components/global/Layout/Layout";
import Head from "next/head";
import CardDetail from "../../components/global/Cards/CardDetail/CardDetail";
import Link from "next/link";
import ButtonShareSocmed from "../../components/global/Buttons/ButtonShareSocmed/ButtonShareSocmed";
import { useState, useEffect } from "react";
import { SwiperSlide } from "swiper/react";
import CardRoadmapDetail from "../../components/global/Cards/CardRoadmapDetail/CardRoadmapDetail";
import CardCourse from "../../components/global/Cards/CardCourse/CardCourse";
import CardRoadmap from "../../components/global/Cards/CardRoadmap/CardRoadmap";
import SkeletonCardRoadmap from "../../components/skeletons/SkeletonCardRoadmap/SkeletonCardRoadmap";
import SkeletonCardCourse from "../../components/skeletons/SkeletonCardCourse/SkeletonCardCourse";
import { shuffleArray, getTotalModule, getTotalTime } from "../../utils/helper";
// React Markdown
import ReactMarkdown from "react-markdown";
import remarkGfm from 'remark-gfm';
import rehypeRaw from "rehype-raw";

export const getServerSideProps = async (context) => {
    const { slug } = context.query;
    const detailRes = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/v1/roadmap/${slug}`);

    const [detail] = await Promise.all([
        detailRes.data,
    ])

    return {
        props: {
            detail,
            // popularCourse
        },
    }
};

const RoadmapDetail = ({ detail }) => {
    const [show, setShow] = useState(false);
    const [relatedRoadmaps, setRelatedRoadmaps] = useState([]);
    const [popularCourses, setPopularCourses] = useState([]);
    const totalModule = getTotalModule(detail.courses)
    const totalTime = getTotalTime(detail.courses)

    useEffect(() => {
        const getRelatedRoadmaps = async () => {
            try {
                let response = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/v1/roadmap?page=1&limit=10&filter=mentor_username[eq]${detail.username}`);
                if (response.data.error === "No Content") {
                    setRelatedRoadmaps(null);
                } else {
                    setRelatedRoadmaps(response.data);
                }
            } catch (err) {
                return err.message;
            }
        };
        getRelatedRoadmaps();

        const getPopularCourses = async () => {
            try {
                let response = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/course/popular`);
                if (response.data.error === "No Content") {
                    setPopularCourses(null);
                } else {
                    setPopularCourses(response.data);
                }
            } catch (err) {
                return err.message;
            }
        };
        getPopularCourses();

    }, [detail.username]);

    return (
        <>
            <Head>
                <title>{detail.name} - Codepolitan</title>
                <meta name="title" content={detail.name} />
                <meta name="description" content={detail.description} />
                <meta property="og:type" content="website" />
                <meta property="og:url" content={`https://codepolitan.com/roadmap/${detail.slug}`} />
                <meta property="og:title" content={detail.name} />
                <meta property="og:description" content={detail.description} />
                <meta property="og:image" content={detail.image} />
                <meta property="twitter:card" content="summary_large_image" />
                <meta property="twitter:url" content={`https://codepolitan.com/roadmap/${detail.slug}`} />
                <meta property="twitter:title" content={detail.name} />
                <meta property="twitter:description" content={detail.description} />
                <meta property="twitter:image" content={detail.image} />
            </Head>
            <Layout>
                <section className="section mt-5">
                    <div className="container p-3 p-lg-5">
                        <div className="row mt-4">

                            {/* Main */}
                            <div className="col-lg-8 text-muted">
                                <section className="section">
                                    <div className="d-flex align-items-center">
                                        <img height="65" className="rounded" src={detail.image || '/assets/img/placeholder.jpg'} alt={detail.name} />
                                        <div className="d-flex flex-column">
                                            <h1 style={{ fontSize: '1.5rem' }} className="section-title ms-3 my-auto">{detail.name || 'Belum ada judul'}</h1>
                                            <div className="d-flex flex-column flex-md-row gap-1 gap-md-4 ms-3">
                                                <div className="d-flex gap-2 align-items-center">
                                                    <FontAwesomeIcon icon={faBuilding} />
                                                    <span>{detail.brand_name || 'Belum ada brand'}</span>
                                                </div>
                                                <div className="d-flex gap-2 align-items-center">
                                                    <FontAwesomeIcon icon={faUserSecret} />
                                                    <span>{detail.brand_author || 'Belum ada author'}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <p className="text-muted my-3">{detail.description}</p>
                                    <div className="d-flex align-items-start">
                                        <span className="badge border text-primary py-md-2">{detail.level.toUpperCase()}</span>
                                        {/* <RatingView className="ms-1 ms-md-3 mt-md-1" size={17} ratingValue={ratingValue} /> */}
                                        {/* <span className="mt-md-1 text-muted ms-auto ms-md-3">{testimony.length || 0} penilaian</span> */}
                                        <button onClick={() => setShow(!show)} className="btn text-muted btn-sm btn-transparent ms-auto"><FontAwesomeIcon icon={faShareAlt} /> Share</button>
                                    </div>
                                    {
                                        show && (
                                            <div className="row my-3">
                                                <div className="col text-center">
                                                    <ButtonShareSocmed type="facebook" link={`https://codepolitan.com/roadmap/${detail.slug}`} />
                                                    <ButtonShareSocmed type="twitter" link={`https://codepolitan.com/roadmap/${detail.slug}`} />
                                                    <ButtonShareSocmed type="linkedin" link={`https://codepolitan.com/roadmap/${detail.slug}`} />
                                                </div>
                                            </div>
                                        )
                                    }
                                </section>

                                {/* List of card roadmap */}
                                <section className="section mt-5">
                                    <div className="row">
                                        <h3 className="mb-4">Materi Yang Akan Dipelajari</h3>                                    {
                                            detail.courses.map((course, index) => {
                                                return (
                                                    <CardRoadmapDetail
                                                        key={index}
                                                        index={index}
                                                        cover={course.cover}
                                                        courseSlug={course.slug}
                                                        courseTitle={course.course_title}
                                                        totalModule={course.total_module}
                                                        totalTime={course.total_time}
                                                    />
                                                )
                                            })}
                                    </div>
                                </section>
                                {/* End of list card roadmap */}

                                <div className="row mt-5">
                                    <div className="col">
                                        {
                                            detail.long_description != "" && (
                                                <div className="my-5">
                                                    <h3>Tentang Kelas</h3>
                                                    <ReactMarkdown
                                                        className="markdown"
                                                        remarkPlugins={[remarkGfm]}
                                                        rehypePlugins={[rehypeRaw]}
                                                        components={{ img: ({ node, ...props }) => <div className="text-center py-5"> <img alt="Logo image" style={{ width: '35%' }}{...props} /> </div> }}
                                                    >
                                                        {detail.long_description}
                                                    </ReactMarkdown>
                                                </div>
                                            )
                                        }

                                        {/* Mentor detail */}
                                        {/* <SectionMentorDetail
                                            brandName={detail.brand_name}
                                            username={detail.username}
                                            avatar={detail.brand_image}
                                            shortDescription={detail.brand_description}
                                            schedule={detail.brand_schedule}
                                        /> */}
                                        {/* End of Mentor detail */}

                                    </div>
                                </div>
                            </div>
                            {/* End of Main */}

                            {/* Sidebar */}
                            <div className="col-lg-4">
                                {/* Card detail sidebar */}
                                <div className="sticky-top" style={{ top: '80px' }}>
                                    <CardDetail
                                        image={detail.image}
                                        roadmap={true}
                                        detailProduct={detail.products}
                                        slug={detail.slug}
                                        totalModule={totalModule}
                                        totalTime={totalTime}
                                    />
                                </div>
                                {/* End of Card detail sidebar */}
                            </div>
                            {/* End of Sidebar */}

                        </div>
                    </div>
                </section>
                <SectionRelatedCourse titleSection={`Roadmap Lainnya dari ${detail.brand_name}`}>
                    {!relatedRoadmaps && [1, 2, 3, 4, 5].map((index) => {
                        return (
                            <SwiperSlide key={index}>
                                <SkeletonCardRoadmap />
                            </SwiperSlide>
                        );
                    })}
                    {relatedRoadmaps !== null && relatedRoadmaps.map((roadmap, index) => {
                        return (
                            <SwiperSlide key={index}>
                                <Link href={`/roadmap/${roadmap.slug}`}>
                                    <a className="link">
                                        <CardRoadmap
                                            thumbnail={roadmap.image}
                                            icon={roadmap.small_icon}
                                            author={roadmap.author}
                                            title={roadmap.name}
                                            level={roadmap.level}
                                            totalCourse={roadmap.total_course}
                                            totalStudent={roadmap.total_student}
                                            rate={roadmap.rate}
                                        />
                                    </a>
                                </Link>
                            </SwiperSlide>
                        );
                    }).slice(0, 6)}
                </SectionRelatedCourse>
                <SectionRelatedCourse titleSection={`Kelas Populer Lainnya`}>
                    {!popularCourses && [1, 2, 3, 4, 5].map((index) => {
                        return (
                            <SwiperSlide key={index}>
                                <SkeletonCardCourse />
                            </SwiperSlide>
                        );
                    })}
                    {popularCourses !== null && shuffleArray(popularCourses).map((course, index) => {
                        return (
                            <SwiperSlide key={index}>
                                <Link href={`/course/intro/${course.slug}`}>
                                    <a className="link">
                                        <CardCourse
                                            thumbnail={course.thumbnail}
                                            author={course.author}
                                            title={course.title}
                                            level={course.level}
                                            totalStudents={course.total_student}
                                            totalModules={course.total_module}
                                            totalTimes={course.total_time}
                                            totalRating={course.total_rating}
                                            totalFeedback={course.total_feedback}
                                            normalBuyPrice={course.buy?.normal_price || course.normal_price}
                                            retailBuyPrice={course.buy?.retail_price || course.retail_price}
                                            normalRentPrice={course.rent?.normal_price}
                                            retailRentPrice={course.rent?.retail_price}
                                        />
                                    </a>
                                </Link>
                            </SwiperSlide>
                        );
                    }).slice(0, 10)}
                </SectionRelatedCourse>
            </Layout>
        </>
    );
};

export default RoadmapDetail;
