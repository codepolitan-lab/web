import { useEffect, useState } from 'react';
import { faCheckCircle, faDatabase, faInfinity, faLaptopCode, faLayerGroup, faList, faPlayCircle, faRedo, faSearch, faServer, faShield, faTerminal } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Head from "next/head";
import Layout from "../../components/global/Layout/Layout";
import SectionVoteCourse from '../../components/global/Sections/SectionVoteCourse/SectionVoteCourse';
import SectionFaq from '../../components/global/Sections/SectionFaq/SectionFaq';
import Link from 'next/link';
import Modal from '../../components/global/Modal/Modal';
import axios from 'axios';
import SkeletonCardRoadmap from '../../components/skeletons/SkeletonCardRoadmap/SkeletonCardRoadmap';
import CardRoadmap from '../../components/global/Cards/CardRoadmap/CardRoadmap';
import { faAndroid } from '@fortawesome/free-brands-svg-icons';

const Roadmap = () => {
    const [faq] = useState([
        {
            id: 'flush-heading1',
            target: 'flush-collapse1',
            title: 'Apa yang dimaksud dengan Roadmap?',
            content: 'Roadmap adalah kumpulan course yang disusun berdasarkan profesi keahlian IT. Misalnya frontend developer, maka isi dari kursus isinya materi frontend seperti HTML, CSS, Javascript, dan framework terkait. Dengan kata lain roadmap adalah sebuah jalur belajar(path).'
        },
        {
            id: 'flush-heading2',
            target: 'flush-collapse2',
            title: 'Lebih baik saya membeli kursus atau roadmap?',
            content: 'Bisa disesuaikan dengan kebutuhan. Jika kamu mempunyai target khusus profesi tertentu baiknya pilih roadmap dibanding membeli kursus satu per satu.'
        },
        {
            id: 'flush-heading3',
            target: 'flush-collapse3',
            title: 'Apakah ada update materi pada Roadmap yang sudah saya beli?',
            content: 'Tentunya, untuk setiap roadmap yang dibeli akan ada update materi berkelanjutan.'
        },
    ]);
    const [roadmaps, setRoadmaps] = useState([]);
    const [filter, setFilter] = useState('');
    const [tag, setTag] = useState('');
    const [sort, setSort] = useState('created_at[desc]');
    const [loading, setLoading] = useState(false);
    const [search, setSearch] = useState('');
    const [searchValue, setSearchValue] = useState('');
    const [limit, setLimit] = useState(12);
    const video = "lVzBOGons6U?rel=0"
    const [videoId, setVideoId] = useState(video)

    useEffect(() => {
        const getRoadmaps = async () => {
            setLoading(true);
            try {
                let response = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/v1/roadmap?page=1&limit=200&search=${search}&filter=${filter}${tag}&sort=${sort}`);
                if (response.data.error === "No Content") {
                    setRoadmaps(null);
                    setLoading(false);
                } else {
                    setRoadmaps(response.data);
                    setLoading(false);

                    // Push first course id to local storage
                    if (sort === 'created_at[desc]' && filter === '' && tag === '' && search === '') {
                        localStorage.setItem('latestRoadmapId', response.data[0].path_id);
                    }
                }
            } catch (err) {
                setLoading(true);
                return err.message;
            }
        };
        getRoadmaps();

    }, [search, filter, tag, sort]);

    const submitSearch = (e) => {
        e.preventDefault();
        setSearch('~' + searchValue + '~');
        setTag('');
        setFilter('');
        setSort('created_at[desc]');
    };

    // Filter Menu
    const roadmapFilter = [
        {
            term: 'Semua Roadmap',
            label: '',
            icon: faList
        },
        {
            term: 'Backend Developer',
            label: 'tags[eq]Backend Developer',
            icon: faServer
        },
        {
            term: 'Database Engineer',
            label: 'tags[eq]Database Engineer',
            icon: faDatabase
        },
        {
            term: 'DevOps',
            label: 'tags[eq]DevOps',
            icon: faInfinity
        },
        {
            term: 'Frontend Developer',
            label: 'tags[eq]Frontend Developer',
            icon: faLaptopCode
        },
        {
            term: 'Fullstack Developer',
            label: 'tags[eq]Fullstack Developer',
            icon: faLayerGroup
        },
        {
            term: 'Mobile Engineer',
            label: 'tags[eq]Mobile Engineer',
            icon: faAndroid
        },
        // {
        //     term: 'Security',
        //     label: 'tags[eq]Security',
        //     icon: faShield
        // },
        {
            term: 'System Analyst',
            label: 'tags[eq]System Analyst',
            icon: faTerminal
        },
    ];

    const roadmapLevel = [
        {
            term: 'Semua Level',
            label: '',
        },
        {
            term: 'Pemula',
            label: 'level[eq]beginner',
        },
        {
            term: 'Menengah',
            label: 'level[eq]intermediate',
        },
        {
            term: 'Mahir',
            label: 'level[eq]advance',
        },
    ];

    const roadmapSort = [
        {
            term: 'Roadmap Terbaru',
            label: 'created_at[desc]',
        },
        {
            term: 'Roadmap Terpopuler',
            label: 'popular[desc]',
        }
    ];

    return (
        <>
            <Head>
                <title>Roadmap - Codepolitan</title>
                <meta name="title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta name="description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="og:type" content="website" />
                <meta property="og:url" content="https://codepolitan.com" />
                <meta property="og:title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta property="og:description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="og:image" content="https://i.ibb.co/SXP8fGc/OG-image.jpg" />
                <meta property="twitter:card" content="summary_large_image" />
                <meta property="twitter:url" content="https://codepolitan.com" />
                <meta property="twitter:title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta property="twitter:description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="twitter:image" content="https://i.ibb.co/SXP8fGc/OG-image.jpg" />
            </Head>
            <Layout>
                <section className="section" style={{ marginTop: '100px' }}>
                    <div className="container p-4 p-lg-5">
                        <div className="row">
                            <div className="col text-center">
                                <h1 className="section-title mb-4">Pilih Skill yang Kamu Ingin kuasai <br /> dan Pelajari Sampai Mahir</h1>
                                <p className="lead text-muted mb-4">Tempat Belajar Coding Online Bersama 140k+ Member</p>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="section">
                    <div className="container p-4 p-lg-5">
                        <div className="row mb-4">
                            <div className="col">
                                <h2 className="section-title h3">Roadmap Pilihan</h2>
                                <p className="text-muted">Pilih dan jadilah professional!</p>
                            </div>
                        </div>
                        <div className="row">
                            {/* Sidebar */}
                            <div className="col-lg-3 d-none d-lg-block">
                                <div className="sticky-top" style={{ top: '100px' }}>
                                    <h3 className="section-title h5 mb-3">Filter Roadmap</h3>
                                    <ul className="list-group">
                                        {roadmapFilter.map((item, index) => {
                                            const handleClick = () => {
                                                setTag(item.label);
                                                setFilter('');
                                                setSort('created_at[desc]');
                                                setSearch('');
                                            }
                                            return (
                                                <li
                                                    key={index}
                                                    className={tag === item.label ? "list-group-item border-0 text-primary px-0" : "list-group-item border-0 text-muted px-0"}
                                                    role="button"
                                                    onClick={handleClick}
                                                    style={{ fontWeight: tag === item.label && 'bold' }}
                                                >
                                                    <FontAwesomeIcon className='me-2' fixedWidth icon={item.icon} />
                                                    {item.term}
                                                </li>
                                            );
                                        })}
                                    </ul>
                                </div>
                            </div>
                            {/* End of Sidebar */}

                            {/* Main */}
                            <div className="col col-lg-9">
                                {/* Menu */}
                                <div className="sticky-top" style={{ top: '70px' }}>
                                    <div className="row mb-3 bg-white py-3">
                                        <div className="col-md-6 mb-3 mb-lg-0 my-auto">
                                            <div className="d-flex align-items-lg-start">
                                                <div className="dropdown d-lg-none">
                                                    <button className="btn btn-outline-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                        Filter
                                                    </button>
                                                    <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                        {roadmapFilter.map((item, index) => {
                                                            const handleClick = () => {
                                                                setTag(item.label);
                                                                setFilter('');
                                                                setSort('created_at[desc]');
                                                                setSearch('');
                                                            }
                                                            return (
                                                                <li
                                                                    key={index}
                                                                    onClick={handleClick}
                                                                    className="dropdown-item"
                                                                    role="button"
                                                                >
                                                                    {item.term}
                                                                    {item.label === tag && (
                                                                        <FontAwesomeIcon className="text-primary ms-1" icon={faCheckCircle} />
                                                                    )}
                                                                </li>
                                                            );
                                                        })}
                                                    </ul>
                                                </div>
                                                <div className="dropdown ms-2 ms-lg-0">
                                                    <button className="btn btn-outline-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                        Level
                                                    </button>
                                                    <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                        {roadmapLevel.map((item, index) => {
                                                            return (
                                                                <li
                                                                    key={index}
                                                                    onClick={() => setFilter(item.label)}
                                                                    className="dropdown-item"
                                                                    role="button"
                                                                >
                                                                    {item.term}
                                                                    {item.label === filter && (
                                                                        <FontAwesomeIcon className="text-primary ms-1" icon={faCheckCircle} />
                                                                    )}
                                                                </li>
                                                            );
                                                        })}
                                                    </ul>
                                                </div>
                                                <div className="dropdown ms-2">
                                                    <button className="btn btn-outline-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                        Urutkan
                                                    </button>
                                                    <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                        {roadmapSort.map((item, index) => {
                                                            return (
                                                                <li
                                                                    key={index}
                                                                    onClick={() => setSort(item.label)}
                                                                    className="dropdown-item"
                                                                    role="button"
                                                                >
                                                                    {item.term}
                                                                    {item.label === sort && (
                                                                        <FontAwesomeIcon className="text-primary ms-1" icon={faCheckCircle} />
                                                                    )}
                                                                </li>
                                                            );
                                                        })}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-5 col-lg-4 ms-auto">
                                            <form onSubmit={submitSearch}>
                                                <div className="input-group">
                                                    <input onChange={(e) => setSearchValue(e.target.value)} type="search" className="form-control shadow-none" placeholder="cari roadmap..." />
                                                    <button className="input-group-text bg-white text-muted" role="button" type="submit" title="Cari">
                                                        <FontAwesomeIcon icon={faSearch} />
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                {/* Course Loop */}
                                <div className="row">
                                    {!loading && roadmaps === null && (
                                        <div className="col text-center my-5">
                                            <object width={'35%'} type="image/svg+xml" data="/assets/img/icon_404.svg">Ups</object>
                                            <p className="text-muted">Ups, roadmap tidak ditemukan, coba cari dengan kata kunci lain</p>
                                        </div>
                                    )}
                                    {loading && [1, 2, 3, 4, 5, 6].map(index => (
                                        <div className="col-md-6 col-xl-4 p-2" key={index}>
                                            <SkeletonCardRoadmap />
                                        </div>
                                    ))}
                                    {!loading && roadmaps !== null && roadmaps?.filter(roadmap => !roadmap.name.includes("Kampus Coding")).map((roadmap, index) => {
                                        return (
                                            <div className="col-md-6 col-xl-4 p-2" key={index}>
                                                <Link href={`/roadmap/${roadmap.slug}`}>
                                                    <a className="link">
                                                        <CardRoadmap
                                                            thumbnail={roadmap.image}
                                                            icon={roadmap.small_icon}
                                                            author={roadmap.author}
                                                            title={roadmap.name}
                                                            level={roadmap.level}
                                                            totalCourse={roadmap.total_course}
                                                            totalStudent={roadmap.total_student}
                                                            rate={roadmap.rate}
                                                        />
                                                    </a>
                                                </Link>
                                            </div>
                                        );
                                    }).slice(0, limit)}
                                </div>
                                {!loading && roadmaps?.length > limit && (
                                    <div className="row my-3">
                                        <div className="col text-center">
                                            <button onClick={() => setLimit(limit + 9)} className="btn btn-light">
                                                Show more
                                                <FontAwesomeIcon size="sm" className="ms-2" icon={faRedo} />
                                            </button>
                                        </div>
                                    </div>
                                )}
                                {/* End of Course Loop */}
                            </div>
                            {/* End of Main */}
                        </div>
                    </div>
                </section>
                <SectionVoteCourse
                    sectionTitle="Atau kamu sudah tau mau belajar apa yang spesifik?"
                    buttonTitle="Jelajahi Kelas"
                    link="/library"
                />
                <SectionFaq data={faq} />
                <Modal title="Cara Belajar" setVideo={() => setVideoId('')}>
                    <div className="ratio ratio-16x9">
                        <iframe src={`https://www.youtube.com/embed/${videoId}`} title="YouTube video" allowFullScreen />
                    </div>
                </Modal>
            </Layout>
        </>
    );
};

export default Roadmap;
