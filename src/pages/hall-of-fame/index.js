import { useState } from 'react';
import Head from 'next/head';
import Banner from '../../components/global/Banner/Banner';
import Layout from '../../components/global/Layout/Layout';

const HallOfFame = () => {
    const [data] = useState([
        {
            name: '0x3rz4f',
            link: 'https://erzaf.com'
        },
        {
            name: 'Rafi Andhika Galuh',
            link: 'https://www.linkedin.com/in/rafi-andhika-galuh'
        },
        {
            name: 'Rifqi Hilmy Zhafrant',
            link: 'https://www.linkedin.com/in/rifqihz'
        },
        {
            name: 'KONSLET',
            link: ''
        },
        {
            name: 'B3T0 (Roberto Urbanus)',
            link: ''
        },
        {
            name: 'Nuevo Quesrto',
            link: ''
        },
        {
            name: 'APAPEDULIMU (Nosa Shandy)',
            link: ''
        },
        {
            name: 'Fika Ridaul Maulayya',
            link: ''
        },
        {
            name: 'enz0 (Yosua Kristanto)',
            link: ''
        },
        {
            name: 'EKA SYAHWAN',
            link: ''
        },
        {
            name: 'Arif Mukhlis',
            link: ''
        },
        {
            name: 'Bilal Abdussalam',
            link: ''
        },
        {
            name: 'Mohammad Wahyudi',
            link: ''
        },
        {
            name: 'noobSecurity',
            link: ''
        },
        {
            name: 'anasleet (Anas Setiawan)',
            link: ''
        },
        {
            name: 'Mr Frank',
            link: 'https://www.facebook.com/MR.Frank1337'
        },
        {
            name: 'Rian Arfiandi (@ri_arf)',
            link: 'https://twitter.com/ri_arf'
        },
        {
            name: 'Choirul Rizal',
            link: ''
        },
        {
            name: '/Mr.Security_system',
            link: ''
        },
        {
            name: 'Syachrul Akbar R',
            link: ''
        },
        {
            name: 'RootBakar',
            link: ''
        },
        {
            name: 'Mohammad Abdullah',
            link: 'https://fb.com/Abdul1ah'
        },
        {
            name: 'Zerboa',
            link: ''
        },
        {
            name: 'Anggi Gunawan',
            link: ''
        },
        {
            name: 'Aldhy Prakoso (Sulawesi I.T Security)',
            link: ''
        },
        {
            name: 'Mayank - Birla Institue of Technology, Mesra',
            link: 'https://www.linkedin.com/in/mayank1007'
        },
        {
            name: 'Chirag Gupta',
            link: 'https://www.linkedin.com/in/chiraggupta8769'
        },
        {
            name: 'Fox Ko',
            link: ''
        },
        {
            name: 'Mr.Luciferz (Hermansyah)',
            link: ''
        },
        {
            name: 'ardzz (Naufal Reky Ardhana)',
            link: 'https://www.instagram.com/ar_dhann/'
        },
        {
            name: 'exzettabyte (Paska Parahita)',
            link: ''
        },
        {
            name: 'Agus IndoXploit',
            link: ''
        },
        {
            name: 'riordens17 (Riordan Pramana)',
            link: 'https://facebook.com/riordens17'
        },
        {
            name: 'Mukhammad Akbar',
            link: 'https://abaykan.com/'
        },
        {
            name: 'Adeputra Armadani (Sulawesi I.T Security)',
            link: ''
        },
        {
            name: 'Negative Wibes',
            link: ''
        },
        {
            name: 'Randy Arios',
            link: ''
        },
        {
            name: 'Reyvand Pratama',
            link: ''
        },
        {
            name: 'Muhamad Nur Arifin S',
            link: 'https://linkedin.com/in/muhamadarifin'
        },
        {
            name: 'Geek Freak (B.Dhiyaneshwaran)',
            link: ''
        },
        {
            name: 'Vismit Rakhecha',
            link: ''
        },
        {
            name: 'Vishal Abasaheb Dhapte',
            link: 'https://www.linkedin.com/in/vishal-dhapte-66167b164'
        },
        {
            name: 'AnonCyberTeam (Winardi Adji Prasetyo)',
            link: ''
        },
        {
            name: 'Hidayah Bachtar',
            link: ''
        },
        {
            name: 'Riski Ramadhan',
            link: ''
        },
    ]);

    return (
        <>
            <Head>
                <title>Hall of Fame - Codepolitan</title>
                <meta name="title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta name="description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="og:type" content="website" />
                <meta property="og:url" content="https://codepolitan.com" />
                <meta property="og:title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta property="og:description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="og:image" content="https://i.ibb.co/SXP8fGc/OG-image.jpg" />
                <meta property="twitter:card" content="summary_large_image" />
                <meta property="twitter:url" content="https://codepolitan.com" />
                <meta property="twitter:title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta property="twitter:description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="twitter:image" content="https://i.ibb.co/SXP8fGc/OG-image.jpg" />
            </Head>
            <Layout>
                <Banner
                    background='/assets/img/backgrounds/banner-hacker.jpg'
                    title="Learn Coding Now, Develop Your Future"
                    subtitle="Kami apreasiasi sebesar-besarnya kepada teman-teman yang turut serta membantu melaporkan adanya malfungsi / bug / celah keamanan pada aplikasi Codepolitan. Dibawah ini beberapa nama penting yang masuk dalam bug reporter Codepolitan"
                />
                <section className="section">
                    <div className="container p-4 p-lg-5">
                        <div className="row">
                            {data.map((item, index) => {
                                return (
                                    <div className="col-6 col-md-4" key={index}>
                                        <a className={item.link ? 'link text-primary' : 'link text-muted'} href={item.link ? item.link : undefined} target="_blank" rel="noopener noreferrer">{item.name}</a>
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                </section>
            </Layout>
        </>
    );
};

export default HallOfFame;
