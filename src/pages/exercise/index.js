import Head from 'next/head';
import Layout from '../../components/global/Layout/Layout';
import Hero from "../../components/exercise/Hero/Hero";
import SectionMateri from '../../components/home/Sections/SectionMateri/SectionMateri';
import axios from 'axios';
import Link from 'next/link';

export const getServerSideProps = async () => {
    const popularLabelsRes = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/course/popular-labels`);

    const [popularLabels] = await Promise.all([
        popularLabelsRes.data,
    ]);

    return {
        props: {
            popularLabels,
        },
    }
}

const Homepage = ({ popularLabels }) => {
    return (
        <>
            <Head>
                <title>Yuk Latihan Exercise Codepolitan - Codepolitan</title>
                <meta name="title" content="Yuk Gabung Komunitas Discord Codepolitan" />
                <meta name="description" content="Belajar coding jadi lebih seru! cukup gabung Discord komunitas coding Codepolitan kamu ga akan belajar sendirian, semua anggota Discord Codepolitan akan selalu aktif berdiskusi." />
                <meta property="og:type" content="website" />
                <meta property="og:url" content="https://codepolitan.com/program/join-discord" />
                <meta property="og:title" content="Yuk Gabung Komunitas Discord Codepolitan" />
                <meta property="og:description" content="Belajar coding jadi lebih seru! cukup gabung Discord komunitas coding Codepolitan kamu ga akan belajar sendirian, semua anggota Discord Codepolitan akan selalu aktif berdiskusi." />
                <meta property="og:image" content="/assets/img/program/thumbnail/thumbnail-discord.png" />
                <meta property="twitter:card" content="summary_large_image" />
                <meta property="twitter:url" content="https://codepolitan.com/program/join-discord" />
                <meta property="twitter:title" content="Yuk Gabung Komunitas Discord Codepolitan" />
                <meta property="twitter:description" content="Belajar coding jadi lebih seru! cukup gabung Discord komunitas coding Codepolitan kamu ga akan belajar sendirian, semua anggota Discord Codepolitan akan selalu aktif berdiskusi." />
                <meta property="twitter:image" content="/assets/img/program/thumbnail/thumbnail-discord.png" />
            </Head>
            <Layout>
                <Hero />
                <section className="section" id="about">
                    <div className="container p-4 p-lg-5">
                        <div className="row justify-content-between">
                            <div className="col-lg-6">
                                <img className="img-fluid" src="/assets/img/exercise/vector-2.png" alt="Discord" />
                            </div>
                            <div className="col-lg-5 my-auto">
                                <h2 className="section-title">Yakin Skill Coding Kamu Udah Pro ?</h2>
                                <p className="lead text-muted my-3">Coba ikut exercise ini deh buat buktiin kalo skill kamu udah pro, tenang aja exercise ini gratis untuk para member setia Codepolitan.</p>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="section" style={{ background: '#f3f3f3' }}>
                    <div className="container p-4 p-lg-5">
                        <div className="row justify-content-between align-items-center">
                            <div className="col-md-6 order-lg-last">
                                <img className="img-fluid" src="/assets/img/exercise/vector-2.png" alt="Discord" />
                            </div>
                            <div className="col-lg-6 my-auto">
                                <h2 className="section-title">Exercises Seru, Mudah Dan Terarah</h2>
                                <p className="lead text-muted my-3">Kamu akan mendapatkan pertanyaan terkait topik yang kamu pilih lalu tulis jawaban kode fungsionalnya secara langsung dan ikuti langkah demi langkah dari exercises yang tim Codepolitan buat.</p>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="section">
                    <div className="container p-4 p-lg-5">
                        <div className="row justify-content-between align-items-center">
                            <div className="col-md-6">
                                <img className="img-fluid" src="/assets/img/exercise/vector-2.png" alt="Discord" />
                            </div>
                            <div className="col-lg-6 my-auto">
                                <h2 className="section-title">Pembuktian Skill Coding</h2>
                                <p className="lead text-muted my-3">Kamu akan mengetahui seberapa jauh skill coding kamu setelah menyelesaikan semua exercises yang tim codepolitan buat.</p>
                            </div>
                        </div>
                    </div>
                </section>
                <SectionMateri title="Exercise by Topik" data={popularLabels} />
                <section className="section py-5" style={{ background: "url('/assets/img/exercise/Mask group.png')", backgroundSize: 'cover', backgroundRepeat: 'no-repeat' }}>
                    <div className="container p-4 p-lg-5">
                        <div className="row justify-content-center text-center">
                            <div className="col-lg-5">
                                <h2>Buktikan Skill Coding Kamu Sekarang Juga!</h2>
                                <Link href="/exercise/library">
                                    <a className="btn btn-primary mt-3">Mulai Exercise</a>
                                </Link>
                            </div>
                        </div>
                    </div>
                </section>
            </Layout>
        </>
    )
}

export default Homepage;
