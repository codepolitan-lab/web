import Head from 'next/head';
import Layout from '../../components/global/Layout/Layout';
import SectionMateri from '../../components/home/Sections/SectionMateri/SectionMateri';
import axios from 'axios';
import SectionExercise from '../../components/exercise/Sections/SectionExercise/SectionExcercise'

export const getServerSideProps = async () => {
    const popularLabelsRes = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/course/popular-labels`);
    const exercisesRes = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/quiz?category=Exam`);

    const [popularLabels, exercises] = await Promise.all([
        popularLabelsRes.data,
        exercisesRes.data,
    ]);

    return {
        props: {
            popularLabels,
            exercises
        },
    }
}

const Homepage = ({ popularLabels, exercises }) => {
    const exerciseBeginner = exercises.filter(exercise => exercise.level == "Beginner")
    const exerciseIntermediate = exercises.filter(exercise => exercise.level == "Intermediate")
    const exerciseMaster = exercises.filter(exercise => exercise.level == "Master")
    return (
        <>
            <Head>
                <title>Yuk Latihan Exercise Codepolitan - Codepolitan</title>
                <meta name="title" content="Yuk Gabung Komunitas Discord Codepolitan" />
                <meta name="description" content="Belajar coding jadi lebih seru! cukup gabung Discord komunitas coding Codepolitan kamu ga akan belajar sendirian, semua anggota Discord Codepolitan akan selalu aktif berdiskusi." />
                <meta property="og:type" content="website" />
                <meta property="og:url" content="https://codepolitan.com/program/join-discord" />
                <meta property="og:title" content="Yuk Gabung Komunitas Discord Codepolitan" />
                <meta property="og:description" content="Belajar coding jadi lebih seru! cukup gabung Discord komunitas coding Codepolitan kamu ga akan belajar sendirian, semua anggota Discord Codepolitan akan selalu aktif berdiskusi." />
                <meta property="og:image" content="/assets/img/program/thumbnail/thumbnail-discord.png" />
                <meta property="twitter:card" content="summary_large_image" />
                <meta property="twitter:url" content="https://codepolitan.com/program/join-discord" />
                <meta property="twitter:title" content="Yuk Gabung Komunitas Discord Codepolitan" />
                <meta property="twitter:description" content="Belajar coding jadi lebih seru! cukup gabung Discord komunitas coding Codepolitan kamu ga akan belajar sendirian, semua anggota Discord Codepolitan akan selalu aktif berdiskusi." />
                <meta property="twitter:image" content="/assets/img/program/thumbnail/thumbnail-discord.png" />
            </Head>
            <Layout>
                <section className="section pt-5">
                    <div className="container p-4 p-lg-5">
                        <div className="row p-5 text-white" style={{ background: "url('/assets/img/exercise/banner-library.png')", backgroundSize: 'cover' }}>
                            <div className="col-lg-6">
                                <h2>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Fugit quia labore, nobis ex laboriosam non!</h2>
                            </div>
                        </div>
                    </div>
                </section>
                <SectionMateri title="Exercise by Topik" data={popularLabels} />
                <SectionExercise
                    title="Basic Exercise"
                    data={exerciseBeginner}
                    link="/flashsale"
                    exerciseLevel="Basic"
                />
                <SectionExercise
                    title="Intermediate Exercise"
                    data={exerciseIntermediate}
                    link="/flashsale"
                    exerciseLevel="Intermediate"
                />
                <SectionExercise
                    title="Master Exercise"
                    data={exerciseMaster}
                    link="/flashsale"
                    exerciseLevel="Master"
                />
            </Layout>
        </>
    )
}

export default Homepage;
