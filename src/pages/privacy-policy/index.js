import Head from 'next/head';
import Link from 'next/link';
import Layout from '../../components/global/Layout/Layout';

const PrivacyPolicy = () => {
    return (
        <>
            <Head>
                <title>Kebijakan Privasi - Codepolitan</title>
                <meta name="title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta name="description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="og:type" content="website" />
                <meta property="og:url" content="https://codepolitan.com" />
                <meta property="og:title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta property="og:description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="og:image" content="https://i.ibb.co/SXP8fGc/OG-image.jpg" />
                <meta property="twitter:card" content="summary_large_image" />
                <meta property="twitter:url" content="https://codepolitan.com" />
                <meta property="twitter:title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta property="twitter:description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="twitter:image" content="https://i.ibb.co/SXP8fGc/OG-image.jpg" />
            </Head>
            <Layout>
                <section className="section" style={{ marginTop: '100px' }}>
                    <div className="container p-4 p-lg-5">
                        <div className="row mb-4">
                            <div className="col">
                                <h1 className="section-title mb-3">Kebijakan Privasi</h1>
                                <p className="text-muted">Dengan menggunakan layanan CodePolitan, Anda memercayakan informasi Anda kepada kami. Kebijakan Privasi ini bertujuan untuk membantu Anda memahami data yang kami kumpulkan, alasan kami mengumpulkannya, dan yang kami lakukan dengan data tersebut. Kami harap Anda meluangkan waktu untuk membacanya dengan saksama. Dengan menggunakan Aplikasi kami, maka Anda telah setuju dengan dokumen Kebijakan Privasi ini dan dokumen <Link href="/terms-and-condition"><a className="link text-primary">Term of Service</a></Link>.</p>
                            </div>
                        </div>
                        <div className="row mb-3">
                            <div className="col">
                                <h2 className="section-title h3">Informasi Apa Saja Yang Kami Simpan</h2>
                                <p className="text-muted">Kami mengumpulkan informasi berikut terkait aktifitas Anda di Aplikasi kami:</p>

                                <div className="my-3">
                                    <h5 className="section-title">Informasi Personal yang Anda Berikan</h5>
                                    <p className="text-muted">Kami dapat memperoleh dan menyimpan informasi yang Anda berikan ketika menggunakan Aplikasi kami. Informasi ini misalnya adalah data profile Anda seperti nama, email, nomor telepon, dan informasi profile Anda lainnya. Kami juga menyimpan informasi yang telah Anda berikan dari akun Facebook atau akun Google Anda ketika Anda menggunakan akun tersebut untuk masuk ke Aplikasi.</p>
                                </div>

                                <div className="my-3">
                                    <h5 className="section-title">Informasi Personal yang Diperoleh Secara Otomatis</h5>
                                    <p className="text-muted">Kami juga menyimpan beberapa informasi yang berhubungan dengan aktifitas Anda selama menggunakan Aplikasi. Informasi ini diperoleh secara otomatis untuk membantu kami meningkatkan kualitas dari layanan yang diberikan oleh CodePolitan kepada Anda. Informasi ini misalnya halaman apa saja yang Anda buka, waktu aktifitas, Alamat IP dari media yang Anda gunakan.</p>
                                </div>

                                <div className="my-3">
                                    <h5 className="section-title">Cookies</h5>
                                    <p className="text-muted">Aplikasi kami menggunakan Cookies. Cookies adalah data yang dikirimkan dari website kami ke browser Anda dan disimpan di hard-drive komputer Anda. Kami menggunakan cookies untuk menyimpan beberapa data identifikasi dengan tujuan untuk meningkatkan kualitas pelayanan yang diberikan oleh Aplikasi terhadap kebutuhan Anda.</p>
                                    <p className="text-muted">Anda dapat memerintahkan browser untuk menolak penggunaan cookies dari Aplikasi kami. Namun hal tersebut dapat berakibat pada tidak berfungsinya beberapa fitur dari Aplikasi.</p>
                                </div>
                            </div>
                        </div>
                        <div className="row mb-3">
                            <div className="col">
                                <h2 className="section-title h3">Bagaimana Kami Menjaga Keamanan dari Informasi</h2>
                                <p className="text-muted">Kami mengupayakan dengan sungguh-sungguh dalam menjaga keamanan dari informasi yang kami peroleh. Kami mengupayakan agar informasi yang dikirim melalui internet dan yang disimpan oleh Aplikasi tetap aman. Kami terus mengupayakan untuk menggunakan pendekatan-pendekatan terbaik agar informasi Anda tidak dicuri.</p>
                                <p className="text-muted">Misalnya, kami menyimpan password Anda dalam bentuk acak (menggunakan mekanisme Hashing), sehingga password Anda tidak disimpan dalam bentuk aslinya. Kami juga membatasi akses ke sumber data Anda.</p>
                                <p className="text-muted">Meskipun kami mengusahakan dengan sungguh-sungguh keamanan dari data yang kami peroleh, kami tidak dapat menjamin bahwa pendekatan di atas memberikan keamanan secara absolut.</p>
                            </div>
                        </div>
                        <div className="row mb-3">
                            <div className="col">
                                <h2 className="section-title h3">Bagaimana Kami Menggunakan Informasi Di Atas</h2>
                                <p className="text-muted">Kami menggunakan informasi yang dikumpulkan dari semua layanan kami untuk memasok, memelihara, melindungi, dan menyempurnakan, untuk mengembangkan layanan yang baru, serta untuk melindungi CodePolitan dan pengguna kami. Kami juga menggunakan informasi ini untuk menawarkan layanan kami yang sesuai dan relevan dengan Anda.</p>
                                <p className="text-muted">Kami dapat menggunakan alamat email Anda untuk menginformasikan layanan kami, misalnya memberi tahu Anda tentang perubahan atau perbaikan yang akan datang. Kami menggunakan informasi yang dikumpulkan dari cookie dan teknologi lainnya, untuk meningkatkan pengalaman pengguna dan kualitas layanan secara keseluruhan. Kami akan meminta persetujuan Anda sebelum menggunakan informasi untuk tujuan selain dari yang ditentukan di Kebijakan Privasi ini.</p>
                            </div>
                        </div>
                        <div className="row mb-3">
                            <div className="col">
                                <h2 className="section-title h3">Apakah CodePolitan Akan Membagikan Informasi yang Diperolehnya?</h2>
                                <p className="text-muted">Kami tidak membagikan informasi pribadi dengan perusahaan, organisasi, dan individu di luar CodePolitan kecuali salah satu keadaan berikut berlaku:</p>
                                <ul className="text-muted">
                                    <li>Dengan persetujuan Anda. Kami akan membagi informasi pribadi dengan perusahaan, organisasi, dan individu di luar CodePolitan jika kami mendapatkan persetujuan dari Anda untuk melakukannya. Kami memerlukan persetujuan keikutsertaan untuk berbagi informasi pribadi yang sensitif.</li>
                                    <li>Untuk tujuan hukum. Kami akan membagikan informasi pribadi dengan perusahaan, organisasi, dan individu di luar CodePolitan jika kami berkeyakinan dengan itikad baik bahwa akses, penggunaan, penyimpanan, atau pengungkapan informasi tersebut perlu untuk:</li>
                                    <ul>
                                        <li>memenuhi hukum, peraturan, dan proses hukum yang berlaku atau permintaan pemerintah yang wajib dipenuhi.</li>
                                        <li>mendeteksi, mencegah, atau menangani pelanggaran potensial, penipuan, keamanan, atau masalah teknis.</li>
                                        <li>melindungi dari ancaman terhadap hak, properti atau keamanan CodePolitan, pengguna kami atau publik.</li>
                                    </ul>
                                </ul>
                                <p className="text-muted">Kami dapat membagikan informasi non-pribadi yang dapat teridentifikasi kepada publik dan mitra kami. Misalnya, kami dapat membagikan informasi secara publik untuk menunjukkan tren mengenai penggunaan umum atas layanan kami.</p>
                                <p className="text-muted">Apabila CodePolitan terlibat dalam sebuah penggabungan, pengambilalihan, atau penjualan aset, kami akan tetap memastikan kerahasiaan informasi pribadi dan memberitahu pengguna yang terpengaruh sebelum informasi pribadi tersebut dialihkan atau menjadi subjek kebijakan privasi yang berbeda.</p>
                            </div>
                        </div>
                    </div>
                </section>
            </Layout>
        </>
    );
};

export default PrivacyPolicy;
