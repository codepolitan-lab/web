import axios from 'axios';
import Head from 'next/head';
import CardLeaderboard from '../../components/global/Cards/CardLeaderboard/CardLeaderboard';
import Layout from '../../components/global/Layout/Layout';

export const getStaticProps = async () => {
    const weeklyRes = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/v1/points/leaderboard?page=1&period=week`);
    const monthlyRes = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/v1/points/leaderboard?page=1&period=month`);
    const overallRes = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/v1/points/leaderboard?page=1`);

    const [weekly, monthly, overall] = await Promise.all([
        weeklyRes.data,
        monthlyRes.data,
        overallRes.data
    ]);
    return {
        props: {
            weekly,
            monthly,
            overall
        },
        revalidate: 1000
    }
}

const Leaderboard = ({ weekly, monthly, overall }) => {
    return (
        <>
            <Head>
                <title>Leaderboard - Codepolitan</title>
                <meta name="title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta name="description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="og:type" content="website" />
                <meta property="og:url" content="https://codepolitan.com" />
                <meta property="og:title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta property="og:description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="og:image" content="https://i.ibb.co/SXP8fGc/OG-image.jpg" />
                <meta property="twitter:card" content="summary_large_image" />
                <meta property="twitter:url" content="https://codepolitan.com" />
                <meta property="twitter:title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta property="twitter:description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="twitter:image" content="https://i.ibb.co/SXP8fGc/OG-image.jpg" />
            </Head>
            <Layout>
                <section className="section" style={{ margin: '65px 0' }}>
                    <div className="container p-4 p-lg-5">
                        <div className="row mb-4">
                            <div className="col text-center">
                                <h2 className="section-title">Top Learner Leaderboard</h2>
                                <p className="text-muted">Tingkatkan rankingmu dengan belajar di Kelas Online dan berkontribusi di Forum Tanya Jawab</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col">
                                <div>
                                    <ul className="nav nav-tabs nav-justified d-grid d-lg-flex" id="BeasiswaTab" role="tablist">
                                        <li className="nav-item" role="presentation">
                                            <button className="nav-link active" id="weekly-tab" data-bs-toggle="tab" data-bs-target="#weekly" type="button" role="tab" aria-controls="weekly" aria-selected="true">Weekly Rank</button>
                                        </li>
                                        <li className="nav-item" role="presentation">
                                            <button className="nav-link" id="monthly-tab" data-bs-toggle="tab" data-bs-target="#monthly" type="button" role="tab" aria-controls="monthly" aria-selected="false">Monthly Rank</button>
                                        </li>
                                        <li className="nav-item" role="presentation">
                                            <button className="nav-link" id="overall-tab" data-bs-toggle="tab" data-bs-target="#overall" type="button" role="tab" aria-controls="overall" aria-selected="false">Overall Rank</button>
                                        </li>
                                    </ul>
                                    <div className="tab-content" id="ForumTabContent">
                                        <div className="tab-pane fade show active" id="weekly" role="tabpanel" aria-labelledby="weekly-tab">
                                            {weekly?.map((item, index) => {
                                                return (
                                                    <CardLeaderboard
                                                        key={index}
                                                        index={index}
                                                        thumbnail={item.avatar}
                                                        name={item.name}
                                                        rank={item.rank}
                                                        rankPicture={item.rank_picture}
                                                        point={item.total_point}
                                                    />
                                                );
                                            })}
                                        </div>
                                        <div className="tab-pane fade" id="monthly" role="tabpanel" aria-labelledby="monthly-tab">
                                            {monthly?.map((item, index) => {
                                                return (
                                                    <CardLeaderboard
                                                        key={index}
                                                        index={index}
                                                        thumbnail={item.avatar}
                                                        name={item.name}
                                                        rank={item.rank}
                                                        rankPicture={item.rank_picture}
                                                        point={item.total_point}
                                                    />
                                                );
                                            })}
                                        </div>
                                        <div className="tab-pane fade" id="overall" role="tabpanel" aria-labelledby="overall-tab">
                                            {overall?.map((item, index) => {
                                                return (
                                                    <CardLeaderboard
                                                        key={index}
                                                        index={index}
                                                        thumbnail={item.avatar}
                                                        name={item.name}
                                                        rank={item.rank}
                                                        rankPicture={item.rank_picture}
                                                        point={item.total_point}
                                                    />
                                                );
                                            })}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </Layout>
        </>
    );
};

export default Leaderboard;
