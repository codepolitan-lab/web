import axios from 'axios';
import Head from 'next/head';
import Layout from '../../components/global/Layout/Layout';
import SectionFeaturedArticle from '../../components/global/Sections/SectionFeaturedArticle/SectionFeaturedArticle';
import SectionBannerArticle from '../../components/global/Sections/SectionBannerArticle/SectionBannerArticle';
import SectionListArticle from '../../components/global/Sections/SectionListArticle/SectionListArticle';
import { useEffect } from 'react';

export const getStaticProps = async () => {
    const tutorialsFeaturedRes = await axios.get(`${process.env.NEXT_PUBLIC_STAGING_URL}/v1/posts/popularWeekly?limit=10`);
    const tutorialsAllRes = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/v1/posts/latest/post?page=1&limit=1000`);
    const labelsRes = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/course/popular-labels`);

    const [tutorialsFeatured, tutorialsAll, labels] = await Promise.all([
        tutorialsFeaturedRes.data,
        tutorialsAllRes.data,
        labelsRes.data
    ]);
    return {
        props: {
            tutorialsFeatured,
            tutorialsAll,
            labels
        },
        revalidate: 10
    }
}


const Tutorials = ({ tutorialsFeatured, tutorialsAll, labels }) => {
    useEffect(() => {
        document.getElementById('hero').scrollIntoView()
    }, [])
    return (
        <>
            <Head>
                <title>Tutorial - Codepolitan</title>
                <meta name="title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta name="description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="og:type" content="website" />
                <meta property="og:url" content="https://codepolitan.com" />
                <meta property="og:title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta property="og:description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="og:image" content="https://i.ibb.co/SXP8fGc/OG-image.jpg" />
                <meta property="twitter:card" content="summary_large_image" />
                <meta property="twitter:url" content="https://codepolitan.com" />
                <meta property="twitter:title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta property="twitter:description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="twitter:image" content="https://i.ibb.co/SXP8fGc/OG-image.jpg" />
            </Head>
            <Layout>
                <SectionBannerArticle />
                <SectionFeaturedArticle data={tutorialsFeatured} />
                <SectionListArticle data={tutorialsAll} labels={labels} />
            </Layout>
        </>
    );
};

export default Tutorials;
