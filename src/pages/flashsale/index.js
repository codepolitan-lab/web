import { useState } from 'react';
import Head from 'next/head';
import Layout from '../../components/global/Layout/Layout';
import axios from 'axios';
import Link from 'next/link';
import CardCourse from '../../components/global/Cards/CardCourse/CardCourse';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faRedo, faSearch } from '@fortawesome/free-solid-svg-icons';

export const getServerSideProps = async () => {
    const resCourses = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/course/flashsale?page=1&limit=200&sort=created_at[desc]`);
    const resRoadmaps = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/v1/roadmap/flashsale?limit=200&sort=created_at[desc]&page=1`);
    const dataCourses = resCourses.data;
    const dataRoadmaps = resRoadmaps.data;

    const courses = dataCourses !== 'error' ? dataCourses : null;
    const roadmaps = dataRoadmaps !== 'error' ? dataRoadmaps : null;

    return {
        props: {
            courses,
            roadmaps
        },
    }
}

const Flashsale = ({ courses, roadmaps }) => {

    const [searchValue, setSearchValue] = useState("");
    const [searchValueRoadmap, setSearchValueRoadmap] = useState("");
    const [limit, setLimit] = useState(12);

    return (
        <>
            <Head>
                <title>Flashsale - Codepolitan</title>
                <meta name="title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta name="description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="og:type" content="website" />
                <meta property="og:url" content="https://codepolitan.com" />
                <meta property="og:title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta property="og:description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="og:image" content="https://i.ibb.co/SXP8fGc/OG-image.jpg" />
                <meta property="twitter:card" content="summary_large_image" />
                <meta property="twitter:url" content="https://codepolitan.com" />
                <meta property="twitter:title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta property="twitter:description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="twitter:image" content="https://i.ibb.co/SXP8fGc/OG-image.jpg" />
            </Head>
            <Layout>
                <section className="section mt-5 pt-5">
                    <div className="container p-4 p-lg-5">
                        <div className="row">
                            <div className="col text-center">
                                <h1 className="section-title">Flashsale Codepolitan</h1>
                                <p className="text-muted">Temukan kelas pilihanmu dengan discount terbatas, jangan sampai lewatkan kesempatan ini!</p>
                            </div>
                        </div>
                        <div className="row justify-content-end my-5">
                            <div className="col-5 col-md-7 text-muted">
                                <h4>Roadmap Flashsale</h4>
                            </div>
                            <div className="col-7 col-md-5">
                                <div className="input-group">
                                    <input onChange={(e) => setSearchValueRoadmap(e.target.value)} type="search" className="form-control shadow-none border-end-0" placeholder="cari roadmap yang sedang promo disini..." />
                                    <button className="input-group-text bg-white text-muted" role="button" type="submit" title="Cari">
                                        <FontAwesomeIcon icon={faSearch} />
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div className="row justify-content-start my-5">
                            {roadmaps === null && (
                                <div className="text-center text-muted">
                                    <object width={'20%'} type="image/svg+xml" data="/assets/img/icon_404.svg">Ups</object>
                                    <h5>Oops, saat ini sedang tidak ada promo</h5>
                                    <p>Nantikan promo flashsale selanjutnya!</p>
                                </div>
                            )}
                            {
                                roadmaps !== null && roadmaps.filter((value) => {
                                    if (value.name.toLowerCase().includes(searchValueRoadmap.toLowerCase())) {
                                        return value;
                                    }
                                }).length > 0
                                    ? roadmaps.filter(value => {
                                        if (value.name.toLowerCase().includes(searchValueRoadmap.toLowerCase())) {
                                            return value;
                                        }
                                    }).map((roadmap, index) => {
                                        return (
                                            <div className="col-md-6 col-lg-4 col-xl-3 px-2 py-3" key={index}>
                                                <Link href={`/roadmap/${roadmap.slug}`}>
                                                    <a className="link">
                                                        <CardCourse
                                                            icon={roadmap.small_icon}
                                                            roadmap={true}
                                                            isFlashsale
                                                            thumbnail={roadmap.image}
                                                            author={roadmap.mentor_username}
                                                            title={roadmap.name}
                                                            level={roadmap.level}
                                                            totalStudents={roadmap.total_student}
                                                            totalModules={roadmap.total_course}
                                                            normalBuyPrice={roadmap.buy?.normal_price || roadmap.normal_price}
                                                            retailBuyPrice={roadmap.buy?.retail_price || roadmap.retail_price}
                                                            normalRentPrice={roadmap.rent?.normal_price}
                                                            retailRentPrice={roadmap.rent?.retail_price}
                                                        />
                                                    </a>
                                                </Link>
                                            </div>
                                        );
                                    }).slice(0, limit)
                                    : (<h4 className="text-center text-muted">Hasil pencarian tidak ditemukan..</h4>)
                            }
                        </div>
                        {courses !== null && courses.filter((value) => {
                            if (searchValue === "") {
                                return value;
                            } else if (value.title.toLowerCase().includes(searchValue.toLowerCase())) {
                                return value;
                            }
                        }).length >= limit && (
                                <div className="row my-3">
                                    <div className="col text-center">
                                        <button onClick={() => setLimit(limit + 8)} className="btn btn-light">
                                            Show more
                                            <FontAwesomeIcon size="sm" className="ms-2" icon={faRedo} />
                                        </button>
                                    </div>
                                </div>
                            )}
                        <hr />
                        <div className="row justify-content-end my-5">
                            <div className="col-5 col-md-7 text-muted">
                                <h4>Course Flashsale</h4>
                            </div>
                            <div className="col-7 col-md-5">
                                <div className="input-group">
                                    <input onChange={(e) => setSearchValue(e.target.value)} type="search" className="form-control shadow-none border-end-0" placeholder="cari kelas yang sedang promo disini..." />
                                    <button className="input-group-text bg-white text-muted" role="button" type="submit" title="Cari">
                                        <FontAwesomeIcon icon={faSearch} />
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div className="row justify-content-start my-5">
                            {courses === null && (
                                <div className="text-center text-muted">
                                    <object width={'20%'} type="image/svg+xml" data="/assets/img/icon_404.svg">Ups</object>
                                    <h5>Oops, saat ini sedang tidak ada promo</h5>
                                    <p>Nantikan promo flashale selanjutnya!</p>
                                </div>
                            )}
                            {
                                courses !== null && courses.filter((value) => {
                                    if (value.title.toLowerCase().includes(searchValue.toLowerCase())) {
                                        return value;
                                    }
                                }).length > 0
                                    ? courses.filter(value => {
                                        if (value.title.toLowerCase().includes(searchValue.toLowerCase())) {
                                            return value;
                                        }
                                    }).map((course, index) => {
                                        return (
                                            <div className="col-md-6 col-lg-4 col-xl-3 px-2 py-3" key={index}>
                                                <Link href={`/course/intro/${course.slug}`}>
                                                    <a className="link">
                                                        <CardCourse
                                                            isFlashsale
                                                            thumbnail={course.thumbnail}
                                                            author={course.author}
                                                            title={course.title}
                                                            level={course.level}
                                                            totalStudents={course.total_student}
                                                            totalModules={course.total_module}
                                                            totalTimes={course.total_time}
                                                            totalRating={course.total_rating}
                                                            totalFeedback={course.total_feedback}
                                                            normalBuyPrice={course.buy?.normal_price || course.normal_price}
                                                            retailBuyPrice={course.buy?.retail_price || course.retail_price}
                                                            normalRentPrice={course.rent?.normal_price}
                                                            retailRentPrice={course.rent?.retail_price}
                                                        />
                                                    </a>
                                                </Link>
                                            </div>
                                        );
                                    }).slice(0, limit)
                                    : (<h4 className="text-center text-muted">Hasil pencarian tidak ditemukan..</h4>)
                            }
                        </div>
                        {courses !== null && courses.filter((value) => {
                            if (searchValue === "") {
                                return value;
                            } else if (value.title.toLowerCase().includes(searchValue.toLowerCase())) {
                                return value;
                            }
                        }).length >= limit && (
                                <div className="row my-3">
                                    <div className="col text-center">
                                        <button onClick={() => setLimit(limit + 8)} className="btn btn-light">
                                            Show more
                                            <FontAwesomeIcon size="sm" className="ms-2" icon={faRedo} />
                                        </button>
                                    </div>
                                </div>
                            )}
                    </div>
                </section>
            </Layout>
        </>
    );
};

export default Flashsale;
