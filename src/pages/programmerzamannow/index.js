import { useState } from 'react';
import { faCheckCircle, faCheckSquare, faComments, faFileVideo, faUserCheck } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import axios from 'axios';
import Head from 'next/head';
import Link from 'next/link';
import BenefitSection from '../../components/global/Sections/BenefitSection/BenefitSection';
import Layout from '../../components/global/Layout/Layout';
import CourseCard from '../../components/programmerzamannow/CourseCard/CourseCard';
import Hero from '../../components/programmerzamannow/Hero/Hero';
import RoadmapSection from '../../components/programmerzamannow/RoadmapSection/RoadmapSection';
import CtaSection from '../../components/programmerzamannow/CtaSection/CtaSection';
import WarrantySection from '../../components/global/Sections/WarrantySection/WarrantySection';
import FaqSection from '../../components/programmerzamannow/FaqSection/FaqSection';
import StatisticSection from '../../components/programmerzamannow/StatisticSection/StatisticSection';

export const getServerSideProps = async () => {
	const { data } = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/course/tag/pzn`);
	const response = { data };

	const course = response.data;

	return {
		props: {
			course
		}
	}
}

const PznLanding = ({ course }) => {
	const [limit, setLimit] = useState(8);

	return (
		<>
			<Head>
				<title>Programmer Zaman Now - Codepolitan</title>
				<meta name="title" content="Kursus Coding Online Programmer Zaman Now Selama 1 Semester" />
				<meta name="description" content="Saya Eko Kurniawan Khannedy, Technical Architect di Blibli.com. Dengan pengalaman saya lebih dari 15 tahun berkarya dan 11 tahun berkarir di bidang programming akan membantu kamu belajar coding lebih cepat dan terarah untuk menjadi Programmer Zaman Now" />

				<meta property="og:type" content="website" />
				<meta property="og:url" content="https://codepolitan.com/program/mobirise" />
				<meta property="og:title" content="Kursus Coding Online Programmer Zaman Now Selama 1 Semester" />
				<meta property="og:description" content="Saya Eko Kurniawan Khannedy, Technical Architect di Blibli.com. Dengan pengalaman saya lebih dari 15 tahun berkarya dan 11 tahun berkarir di bidang programming akan membantu kamu belajar coding lebih cepat dan terarah untuk menjadi Programmer Zaman Now" />
				<meta property="og:image" content="/assets/img/programmerzamannow/og-image-pzn.jpg" />

				<meta property="twitter:card" content="summary_large_image" />
				<meta property="twitter:url" content="https://codepolitan.com/program/mobirise" />
				<meta property="twitter:title" content="Kursus Coding Online Programmer Zaman Now Selama 1 Semester" />
				<meta property="twitter:description" content="Saya Eko Kurniawan Khannedy, Technical Architect di Blibli.com. Dengan pengalaman saya lebih dari 15 tahun berkarya dan 11 tahun berkarir di bidang programming akan membantu kamu belajar coding lebih cepat dan terarah untuk menjadi Programmer Zaman Now" />
				<meta property="twitter:image" content="/assets/img/programmerzamannow/og-image-pzn.jpg" />
			</Head>
			<Layout>
				<Hero />
				<StatisticSection />
				<section className="section" id="about">
					<div className="container p-5">
						<div className="row mb-5">
							<div className="col text-center">
								<h2 className="section-title">Bagaimana Cara Belajarnya?</h2>
							</div>
						</div>
						<div className="row justify-content-center">
							<div className="col-md-6 col-lg-4 mb-3">
								<div className="card rounded bg-light-orange border-0" style={{ height: '25rem' }}>
									<div className="card-body p-5 text-muted text-center">
										<FontAwesomeIcon style={{ fontSize: '3em' }} icon={faFileVideo} />
										<h5 className="mt-4">Step by Step Video Tutorial Premium</h5>
										<p>Terdapat ratusan materi belajar dalam format video yang telah saya susun secara komprehensif untuk membantumu belajar lebih mudah</p>
									</div>
								</div>
							</div>
							<div className="col-md-6 col-lg-4 mb-3">
								<div className="card rounded bg-light-green border-0">
									<div className="card-body p-5 text-muted text-center" style={{ height: '25rem' }}>
										<FontAwesomeIcon style={{ fontSize: '3em' }} icon={faComments} />
										<h5 className="mt-4">Forum Tanya Jawab Khusus Anggota</h5>
										<p>Kamu bisa bertanya seputar permasalahan belajarmu di forum tanya jawab yang telah disediakan, saya sendiri yang akan langsung menjawabnya</p>
									</div>
								</div>
							</div>
							<div className="col-md-6 col-lg-4 mb-3">
								<div className="card rounded bg-light-blue border-0">
									<div className="card-body p-5 text-muted text-center" style={{ height: '25rem' }}>
										<FontAwesomeIcon style={{ fontSize: '3em' }} icon={faUserCheck} />
										<h5 className="mt-4">Mentoring Tatap Muka Secara Daring</h5>
										<p>Saya akan mengadakan mentoring tatap muka secara daring 1 kali setiap bulan, kamu bisa berkonsultasi langsung pada sesi tersebut</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<BenefitSection title="Mengapa Belajar di Codepolitan?" />
				<section className="section bg-light">
					<div className="container p-5">
						<div className="row justify-content-around">
							<div className="col-lg-6 mt-3">
								<h2 className="section-title mb-4">Cukup luangkan waktu 2 jam sehari. Bisa?</h2>
								<p className="text-muted">Dengan bergabung dalam program ini, selama 1 semester saya akan membimbingmu belajar coding secara online melalui rangkaian kelas online yang telah saya susun.</p>
								<p className="text-muted">Kamu cukup meluangkan waktu minimal 2 jam per hari, untuk mempelajari materi dan praktek. Percayalah, dalam waktu 6 bulan kamu akan merasakan manfaatnya.</p>
								<p className="text-muted">Ayo akselerasi kecepatan belajar codingmu. Saya tunggu di dalam kelas.</p>
								<p><strong>Eko Kurniawan Khannedy</strong></p>
							</div>
							<div className="col-lg-6 order-first order-lg-last my-5">
								<div className="ratio ratio-16x9">
									<iframe style={{ borderRadius: '25px' }} src="https://www.youtube.com/embed/1h50IfH2zdU" title="YouTube video player" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen />
								</div>
							</div>
						</div>
					</div>
				</section>
				<section className="section bg-light-blue">
					<div className="container p-5">
						<div className="row">
							<div className="col-lg-6 mt-5 mb-lg-5">
								<img className="img-fluid" src="/assets/img/programmerzamannow/logo-talent-hub.png" alt="Talent Hub" />
							</div>
							<div className="col-lg-6 my-5">
								<div className="py-lg-5">
									<h2 className="section-title">Terhubung dengan Industri Melalui Talent Hub Codepolitan</h2>
									<p className="my-3 text-muted">Belajar dan tingkatkan kemampuan codingmu, kemudian sempurnakan profile dan portfoliomu di Talent Hub Codepolitan untuk membuka berbagai peluang terhubung dengan industri. Mulai bangun karirmu sebagai programmer zaman now.</p>
								</div>
							</div>
						</div>
					</div>
				</section>
				<RoadmapSection />
				<section className="section">
					<div className="container p-5">
						<div className="row">
							<div className="col text-center">
								<h2 className="section-title mb-3">Kelas-kelas Terbaru</h2>
								<p className="text-muted w-75 mx-auto">Materi belajar akan terus bertambah seiring waktu, dan kamu tidak perlu membelinya satu per satu. Cukup gabung program semuanya bisa kamu akses.</p>
							</div>
						</div>
						<div className="row my-4">
							{course?.map((item, index) => {
								return (
									<div className="col-md-6 col-lg-3 mb-3" key={index}>
										<CourseCard
											thumbnail={item.thumbnail}
											title={item.title}
											totalModule={item.total_module}
											link={`/programmerzamannow/course/${item.slug}`}
										/>
									</div>
								);
							}).reverse().slice(0, limit)}
						</div>
						{course.length <= limit ? '' : (
							<div className="row">
								<div className="col text-center">
									<button onClick={() => setLimit(limit + 4)} className="btn btn-primary px-4 py-2">Lihat Lainnya</button>
								</div>
							</div>
						)}
					</div>
				</section>
				<section className="section bg-light">
					<div className="container p-5">
						<div className="row text-center">
							<div className="col">
								<h2 className="section-title">Langkah Sukses Menjadi Mahir</h2>
								<p className="text-muted">Belajar lebih terarah dan hasil maksimal dengan 4 langkah berikut:</p>
							</div>
						</div>
						<div className="row mt-4">
							<div className="col px-5">
								<div className="d-none d-md-block">
									<img className="img-fluid" src="/assets/img/program/cara-belajar.png" alt="Cara Belajar" />
								</div>
								<div className="d-md-none">
									<img className="img-fluid" src="/assets/img/program/cara-belajar-small.png" alt="Cara Belajar" />
								</div>
							</div>
						</div>
					</div>
				</section>
				<section className="section" style={{ backgroundColor: '#eee' }}>
					<div className="container px-5 pt-5 pb-5 pb-lg-0">
						<div className="row">
							<div className="col-lg-6 mt-lg-5 text-muted">
								<h2 className="section-title">Manfaat Gabung Program</h2>
								<p className="mt-3">Banyak peluang yang akan terbuka jika kamu memiliki skill coding.</p>

								<div className="d-flex align-items-start">
									<FontAwesomeIcon style={{ fontSize: '2em', opacity: 0.5 }} className="me-3 mt-1" icon={faCheckCircle} />
									<p>Bekerja pada sebuah perusahaan atau startup digital menjadi seorang programmer profesional</p>
								</div>

								<div className="d-flex align-items-start">
									<FontAwesomeIcon style={{ fontSize: '2em', opacity: 0.5 }} className="me-3 mt-1" icon={faCheckCircle} />
									<p>Menjadi seorang freelancer yang bekerja secara remote dari rumah yang bekerja untuk klien baik dalam maupun luar negeri.</p>
								</div>

								<div className="d-flex align-items-start">
									<FontAwesomeIcon style={{ fontSize: '2em', opacity: 0.5 }} className="me-3 mt-1" icon={faCheckCircle} />
									<p>Membangun bisnismu sendiri dengan mengembangkan aplikasi atau web yang kamu ciptakan.</p>
								</div>

							</div>
							<div className="col d-none d-lg-block">
								<img className="img-fluid" src="/assets/img/programmerzamannow/gabung-program.png" alt="Manfaat Gabung Program" />
							</div>
						</div>
					</div>
				</section>
				<CtaSection>
					<div className="d-grid d-md-inline-block gap-2 mt-5">
						<a className="btn btn-primary px-3 py-2 mx-2" href="#membership">Gabung Member</a>
						<Link href="/programmerzamannow/roadmap">
							<a className="btn btn-outline-light px-3 py-2 mx-2">Lihat Roadmap</a>
						</Link>
					</div>
				</CtaSection>
				<section className="section bg-light-blue" id="membership">
					<div className="container p-5">
						<div className="row">
							<div className="col">
								<h2 className="section-title">Siap untuk Memulai?</h2>
								<p className="text-muted my-3">Pilih salah satu dari paket belajar berikut untuk bergabung dalam program</p>
							</div>
						</div>
						<div className="row justify-content-center my-5">
							<div className="col-md-6 col-xl-4 mb-3">
								<div className="card border-0" style={{ borderRadius: '10px' }}>
									<div className="card-body text-center text-md-start">
										<h5>Member 6 Bulan</h5>
										<span className="text-muted"><del> Rp. 774.000</del> </span>
										<p className="my-3 text-center"><span style={{ position: 'relative', top: '-1.5em', fontWeight: 'bold' }}>Rp</span> <span style={{ fontSize: '2.5em', fontWeight: 'bold' }}>499.000</span></p>
										<div className="text-center mb-2">
											<span className="badge bg-pink">Hemat Rp. 275.000</span>
										</div>
										<p className="text-center text-muted"><small>Akses belajar 56 kelas selama 180 hari</small></p>
									</div>
									<Link href="https://pay.codepolitan.com/?slug=programmer-zaman-now-reguler">
										<a className="link">
											<div className="card-footer bg-codepolitan" style={{ borderBottomRightRadius: '10px', borderBottomLeftRadius: '10px' }}>
												<p className="text-center text-white my-auto">BELI ROADMAP</p>
											</div>
										</a>
									</Link>
								</div>
							</div>

							<div className="col-md-6 col-xl-4 mb-3">
								<div className="card border-0" style={{ borderRadius: '10px' }}>
									<div className="card-body text-center text-md-start">
										<div className="row">
											<div className="col-auto">
												<h5>Member 4 Bulan</h5>
											</div>
											<div className="col text-end">
												<span className="badge bg-secondary">Best seller</span>
											</div>
										</div>
										<span className="text-muted"><del> Rp. 516.000</del> </span>
										<p className="my-3 text-center"><span style={{ position: 'relative', top: '-1.5em', fontWeight: 'bold' }}>Rp</span> <span style={{ fontSize: '2.5em', fontWeight: 'bold' }}>349.000</span></p>
										<div className="text-center mb-2">
											<span className="badge bg-pink">Hemat Rp. 167.000</span>
										</div>
										<p className="text-center text-muted"><small>Akses belajar 56 kelas selama 120 hari</small></p>
									</div>
									<Link href="https://pay.codepolitan.com/?slug=programmer-zaman-now-ngebut">
										<a className="link">
											<div className="card-footer bg-codepolitan" style={{ borderBottomRightRadius: '10px', borderBottomLeftRadius: '10px' }}>
												<p className="text-center text-white my-auto">BELI ROADMAP</p>
											</div>
										</a>
									</Link>
								</div>
							</div>

							<div className="col-md-6 col-xl-4 mb-3">
								<div className="card border-0" style={{ borderRadius: '10px', height: '100%' }}>
									<div className="card-body text-center text-md-start">
										<h5>Member 1 Bulan</h5>
										<br />
										<p className="my-3 text-center"><span style={{ position: 'relative', top: '-1.5em', fontWeight: 'bold' }}>Rp</span> <span style={{ fontSize: '2.5em', fontWeight: 'bold' }}>129.000</span></p>
										<p className="text-center text-muted"><small>Akses belajar 56 kelas selama 30 hari</small></p>
									</div>
									<Link href="https://pay.codepolitan.com/?slug=programmer-zaman-now-cicilan">
										<a className="link">
											<div className="card-footer bg-codepolitan" style={{ borderBottomRightRadius: '10px', borderBottomLeftRadius: '10px' }}>
												<p className="text-center text-white my-auto">BELI ROADMAP</p>
											</div>
										</a>
									</Link>
								</div>
							</div>

						</div>
						<div className="row mb-3">
							<div className="col-12 text-center">
								<p className="text-muted">Benefit yang akan kamu dapatkan</p>
							</div>
						</div>
						<div className="row d-flex justify-content-center px-lg-5">
							<div className="col-md-6">
								<div className="d-flex align-items-start">
									<FontAwesomeIcon style={{ fontSize: '1.5em' }} className="me-3 mt-1 text-primary" icon={faCheckSquare} />
									<p className="text-muted">Kelas online premium yang tidak ada di Youtube gratis Programmer Zaman Now</p>
								</div>
								<div className="d-flex align-items-start">
									<FontAwesomeIcon style={{ fontSize: '1.5em' }} className="me-3 mt-1 text-primary" icon={faCheckSquare} />
									<p className="text-muted">Peluang terhubung dengan industri melalui fitur Talent Hub Codepolitan</p>
								</div>
								<div className="d-flex align-items-start">
									<FontAwesomeIcon style={{ fontSize: '1.5em' }} className="me-3 mt-1 text-primary" icon={faCheckSquare} />
									<p className="text-muted">Semua materi belajar dan semua roadmap Belajar (tidak hanya 1 kelas)</p>
								</div>
								<div className="d-flex align-items-start">
									<FontAwesomeIcon style={{ fontSize: '1.5em' }} className="me-3 mt-1 text-primary" icon={faCheckSquare} />
									<p className="text-muted">Forum diskusi tanya jawab yang dijawab langsung oleh mentor</p>
								</div>
							</div>

							<div className="col-md-6">
								<div className="d-flex align-items-start">
									<FontAwesomeIcon style={{ fontSize: '1.5em' }} className="me-3 mt-1 text-primary" icon={faCheckSquare} />
									<p className="text-muted">Mentoring tatap muka secara daring 1 bulan sekali</p>
								</div>
								<div className="d-flex align-items-start">
									<FontAwesomeIcon style={{ fontSize: '1.5em' }} className="me-3 mt-lg-2 text-primary" icon={faCheckSquare} />
									<p className="text-muted">Akses materi belajar sesuai dengan paket waktu belajar yang kamu pilih</p>
								</div>
								<div className="d-flex align-items-start">
									<FontAwesomeIcon style={{ fontSize: '1.5em' }} className="me-3 mt-1 text-primary" icon={faCheckSquare} />
									<p className="text-muted">Garansi 100% uang kembali jika dalam 3 hari tidak puas</p>
								</div>
							</div>
						</div>
						<div className="row my-4">
							<div className="col text-center">
								<p className="text-muted">Hanya Membutuhkan Sebagian Roadmap Atau Hanya Satuan ?</p>
								<Link href="/programmerzamannow/roadmap">
									<a className="btn btn-outline-primary px-3">Lihat disini</a>
								</Link>
							</div>
						</div>
					</div>
				</section>
				<WarrantySection backgroundColor="bg-light" />
				<FaqSection />
			</Layout>
		</>
	);
};

export default PznLanding;
