import { useState } from 'react';
import { faLock } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import axios from 'axios';
import Head from 'next/head';
import Link from 'next/link';
import { RatingView } from 'react-simple-star-rating';
import Layout from '../../../components/global/Layout/Layout';
import { removeTag, countRates, getLessons } from '../../../utils/helper';
import SidebarCard from '../../../components/programmerzamannow/SidebarCard/SidebarCard';

export const getServerSideProps = async (context) => {
  try {
    const { slug } = context.query;
    const courseDetailRes = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/course/detail/${slug}`);
    const testimonyRes = await axios.get(`${process.env.NEXT_PUBLIC_APPS_URL}/api/feedback/course/${slug}`);

    const [courseDetail, testimony] = await Promise.all([
      courseDetailRes.data,
      testimonyRes.data
    ]);

    return {
      props: {
        courseDetail,
        testimony
      },
      // revalidate: 10
    }
  } catch (err) {
    console.log(err);
    return {
      notFound: true
    }
  }
}

const CourseDetail = ({ courseDetail, testimony }) => {

  const [limit, setLimit] = useState(5);

  let courses = courseDetail.course;
  let authors = courseDetail.authors;
  let lessons = courseDetail.lessons;

  const ratingValue = countRates(testimony);

  const lessonList = getLessons(lessons);
  const lessonTopic = lessonList.lessonTopic;
  const lessonContent = lessonList.lessonContent;


  return (
    <>
      <Head>
        <title>{courses.title} Programmer Zaman Now - Codepolitan</title>
        <meta name="title" content="Kursus Coding Online Programmer Zaman Now Selama 1 Semester" />
        <meta name="description" content="Saya Eko Kurniawan Khannedy, Technical Architect di Blibli.com. Dengan pengalaman saya lebih dari 15 tahun berkarya dan 11 tahun berkarir di bidang programming akan membantu kamu belajar coding lebih cepat dan terarah untuk menjadi Programmer Zaman Now" />

        <meta property="og:type" content="website" />
        <meta property="og:url" content="https://codepolitan.com/program/mobirise" />
        <meta property="og:title" content="Kursus Coding Online Programmer Zaman Now Selama 1 Semester" />
        <meta property="og:description" content="Saya Eko Kurniawan Khannedy, Technical Architect di Blibli.com. Dengan pengalaman saya lebih dari 15 tahun berkarya dan 11 tahun berkarir di bidang programming akan membantu kamu belajar coding lebih cepat dan terarah untuk menjadi Programmer Zaman Now" />
        <meta property="og:image" content="/assets/img/programmerzamannow/og-image-pzn.jpg" />

        <meta property="twitter:card" content="summary_large_image" />
        <meta property="twitter:url" content="https://codepolitan.com/program/mobirise" />
        <meta property="twitter:title" content="Kursus Coding Online Programmer Zaman Now Selama 1 Semester" />
        <meta property="twitter:description" content="Saya Eko Kurniawan Khannedy, Technical Architect di Blibli.com. Dengan pengalaman saya lebih dari 15 tahun berkarya dan 11 tahun berkarir di bidang programming akan membantu kamu belajar coding lebih cepat dan terarah untuk menjadi Programmer Zaman Now" />
        <meta property="twitter:image" content="/assets/img/programmerzamannow/og-image-pzn.jpg" />
      </Head>
      <Layout>
        <div className="container pt-5 p-3 p-md-5">
          <div className="row mt-5">

            {/* Main */}
            <div className="col-lg-7">
              <section className="section">
                <div className="row mt-3 mb-4">
                  <div className="col">
                    <span className="text-muted"><Link href="/programmerzamannow"><a className="link text-primary">Home</a></Link> &gt; {courses.title} </span>
                  </div>
                </div>
                <div className="d-flex align-items-start">
                  <img height="65" className="rounded" src={courses.thumbnail} alt={courses.title} />
                  <h1 style={{ fontSize: '1.7rem' }} className="section-title ms-3 my-auto">{courses.title}</h1>
                </div>
                <p className="text-muted my-3">{courses.description}</p>
                <div className="d-flex align-items-start">
                  <span className="badge bg-codepolitan px-md-4 py-md-2">{courses.level.toUpperCase()}</span>
                  <RatingView className="ms-2 ms-md-4 mt-md-1" size={20} ratingValue={Math.floor(ratingValue)} />
                  <span className="mt-md-1 text-muted ms-auto ms-md-3">{testimony.length || 0} penilaian</span>
                  <span className="mt-md-1 text-muted ms-auto ms-md-3">{courseDetail.total_student} peserta</span>
                </div>
              </section>
              <section className="ratio ratio-16x9 mt-4 mb-5">
                <iframe
                  style={{ borderRadius: '25px' }}
                  src={`https://www.youtube.com/embed/${courses.preview_video}`}
                  title="Course Preview"
                  allowFullScreen
                />
              </section>
              <section className="section mb-4">
                <h3 className="section-title">Tentang Kelas</h3>
                <p className="text-muted">{removeTag(courses.long_description)}</p>
              </section>
              <section className="section mb-5">
                <h3 className="section-title mb-3">Materi Belajar</h3>
                <div className="card">
                  <div className="card-header bg-white">
                    <h4 className="text-muted">{lessonTopic[0].topic_title}</h4>
                  </div>
                  <div className="card-body p-0">
                    <table className="table table-striped m-auto">
                      <tbody>
                        {lessonContent[0].contents.map((lesson, index) => {
                          return (
                            <tr key={index}>
                              <td className="col-1 text-muted text-center">{index + 1}</td>
                              <td className="col-1">
                                <img height="30" src="/assets/img/program/play-icon.png" alt="Icon" />
                              </td>
                              <td className="text-muted">{lesson.lesson_title}</td>
                              <td className="text-end text-muted">
                                <FontAwesomeIcon icon={faLock} className="me-2" />
                                {lesson.duration || '00:00'}
                              </td>
                            </tr>
                          );
                        })}
                      </tbody>
                    </table>
                  </div>
                </div>
              </section>
              <section className="section mb-5">
                <h3 className="section-title mb-4">Penyusun Materi</h3>
                {authors.map((author, index) => {
                  return (
                    <div className="row" key={index}>
                      <div className="col-3">
                        <img className="img-fluid rounded-circle" src={author.avatar} alt={author.name} />
                      </div>
                      <div className="col mt-2 mt-md-4">
                        <h5>{author.name}</h5>
                        <p className="text-muted">{author.short_description}</p>
                      </div>
                    </div>
                  );
                })}
              </section>
              <section className="section">
                <h3 className="section-title mb-4">Testimoni oleh Siswa</h3>
                {testimony.status === 'failed' ? <p className="text-muted my-5">Belum ada testimoni</p> :
                  testimony.map((item, index) => {
                    return (
                      <div className="card mb-2" key={index}>
                        <div className="card-body">
                          <div className="d-flex align-items-start">
                            <img height="70" className="rounded-circle me-3" src="/assets/img/program/icons/icon-avatar.png" alt="" />
                            <p className="text-muted mt-2">{item.comment !== "" || item.comment !== null ? item.comment : (<i>Tidak ada ulasan</i>)}</p>
                          </div>
                        </div>
                        <div className="card-footer bg-white border-0">
                          <div className="row">
                            <div className="col-auto">
                              <h6><strong>{item.name}</strong></h6>
                            </div>
                            <div className="col text-end">
                              <RatingView className="ms-2 ms-md-4 mt-md-1" size={20} ratingValue={item.rate} />
                            </div>
                          </div>
                        </div>
                      </div>
                    );
                  }).slice(0, limit)}
                {testimony.length <= limit || testimony.status === 'failed' ? '' : (
                  <div className="d-grid gap-2">
                    <button onClick={() => setLimit(limit + 5)} className="btn btn-outline-primary shadow-none">Tampilkan lainnya</button>
                  </div>
                )}
              </section>
            </div>

            {/* Sidebar */}
            <div className="col-lg-4 offset-lg-1">
              <h2 className="d-lg-none mt-5 mb-4">Mulai Berlangganan</h2>
              <SidebarCard />
            </div>
          </div>
        </div>
      </Layout>
    </>
  );
};

export default CourseDetail;
