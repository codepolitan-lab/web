import Head from 'next/head';
import Link from 'next/link';
import Layout from '../../../components/global/Layout/Layout';
import RoadmapCard from '../../../components/programmerzamannow/RoadmapCard/RoadmapCard';
import axios from 'axios';

export const getServerSideProps = async () => {

    const { data } = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/v1/path/category/programmer-zaman-now`);
    const response = { data };

    const roadmaps = response.data;

    return {
        props: {
            roadmaps
        },
    }
}

const Roadmap = ({ roadmaps }) => {
    return (
        <>
            <Head>
                <title>Roadmap Programmer Zaman Now - Codepolitan</title>
                <meta name="title" content="Kursus Coding Online Programmer Zaman Now Selama 1 Semester" />
                <meta name="description" content="Saya Eko Kurniawan Khannedy, Technical Architect di Blibli.com. Dengan pengalaman saya lebih dari 15 tahun berkarya dan 11 tahun berkarir di bidang programming akan membantu kamu belajar coding lebih cepat dan terarah untuk menjadi Programmer Zaman Now" />

                <meta property="og:type" content="website" />
                <meta property="og:url" content="https://codepolitan.com/program/mobirise" />
                <meta property="og:title" content="Kursus Coding Online Programmer Zaman Now Selama 1 Semester" />
                <meta property="og:description" content="Saya Eko Kurniawan Khannedy, Technical Architect di Blibli.com. Dengan pengalaman saya lebih dari 15 tahun berkarya dan 11 tahun berkarir di bidang programming akan membantu kamu belajar coding lebih cepat dan terarah untuk menjadi Programmer Zaman Now" />
                <meta property="og:image" content="/assets/img/programmerzamannow/og-image-pzn.jpg" />

                <meta property="twitter:card" content="summary_large_image" />
                <meta property="twitter:url" content="https://codepolitan.com/program/mobirise" />
                <meta property="twitter:title" content="Kursus Coding Online Programmer Zaman Now Selama 1 Semester" />
                <meta property="twitter:description" content="Saya Eko Kurniawan Khannedy, Technical Architect di Blibli.com. Dengan pengalaman saya lebih dari 15 tahun berkarya dan 11 tahun berkarir di bidang programming akan membantu kamu belajar coding lebih cepat dan terarah untuk menjadi Programmer Zaman Now" />
                <meta property="twitter:image" content="/assets/img/programmerzamannow/og-image-pzn.jpg" />
            </Head>
            <Layout>
                <section
                    className="section"
                    style={{
                        paddingTop: '120px',
                        paddingBottom: '100px',
                        backgroundImage: `url(/assets/img/programmerzamannow/cta-background.webp)`,
                        backgroundSize: 'cover',
                        backgroundRepeat: 'no-repeat',
                        backgroundPosition: 'center'
                    }}>
                    <div className="container p-5">
                        <div className="row">
                            <div className="col text-center">
                                <h1 className="section-title text-white">Selain Paket Member, Kamu Juga Bisa Membeli Satu Roadmap Dengan Harga Menarik Lho!</h1>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="section bg-light">
                    <div className="container p-5">
                        <div className="row mb-4">
                            <div className="col">
                                <span className="text-muted"><Link href="/programmerzamannow"><a className="link text-primary">Home</a></Link> &gt; Roadmap</span>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col">
                                <h2 className="section-title">Roadmap Belajar</h2>
                            </div>
                        </div>
                        <div className="row mt-4">
                            {roadmaps.map((roadmap) => {
                                return (
                                    <div className="col-md-6 col-lg-3 mb-4" key={roadmap.id}>
                                        <RoadmapCard
                                            thumbnail={roadmap.thumbnail}
                                            title={roadmap.category_title}
                                            description={roadmap.category_description}
                                            link={`/programmerzamannow/roadmap/${roadmap.id}`}
                                        />
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                </section>
            </Layout>
        </>
    );
};

export default Roadmap;
