import { useState } from 'react';
import axios from 'axios';
import Head from 'next/head';
import Link from 'next/dist/client/link';
import Layout from '../../../components/global/Layout/Layout';
import HeroRoadmap from '../../../components/programmerzamannow/Hero/HeroRoadmap';
import CourseCard from '../../../components/programmerzamannow/CourseCard/CourseCard';
import WarrantySection from '../../../components/global/Sections/WarrantySection/WarrantySection';
import RoadmapSection from '../../../components/programmerzamannow/RoadmapSection/RoadmapSection';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheckSquare, faSearch } from '@fortawesome/free-solid-svg-icons';
import PriceCard from '../../../components/programmerzamannow/PriceCard/PriceCard';

export const getServerSideProps = async (context) => {
	const { id } = context.query;
	const { data } = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/v1/path/category/courses/${id}`);
	const response = { data };

	const course = response.data;

	return {
		props: {
			course
		},
		// revalidate: 10
	}
}

const RoadmapPage = ({ course }) => {
	let title = course[0].category_title;
	let description = course[0].description;
	let thumbnail = course[0].category_thumbnail;
	let product_link = JSON.parse(course[0].product_link);

	const [search, setSearch] = useState("");
	const [roadmapOneMonthisChecked, setRoadmapOneMonthisChecked] = useState(false);
	const [roadmapFourMonthisChecked, setRoadmapFourMonthisChecked] = useState(false);
	const [roadmapSixMonthisChecked, setRoadmapSixMonthisChecked] = useState(false);
	const [slug, setSlug] = useState("");

	const handleClick1 = () => {
		setRoadmapOneMonthisChecked(true);
		setRoadmapFourMonthisChecked(false);
		setRoadmapSixMonthisChecked(false);
		setSlug(product_link[1].product_slug);
	}

	const handleClick2 = () => {
		setRoadmapFourMonthisChecked(true);
		setRoadmapOneMonthisChecked(false);
		setRoadmapSixMonthisChecked(false);
		setSlug(product_link[2].product_slug);
	}

	const handleClick3 = () => {
		setRoadmapSixMonthisChecked(true);
		setRoadmapOneMonthisChecked(false);
		setRoadmapFourMonthisChecked(false);
		setSlug(product_link[3].product_slug);
	}

	return (
		<>
			<Head>
				<title>{title} Programmer Zaman Now - Codepolitan</title>
				<meta name="title" content="Kursus Coding Online Programmer Zaman Now Selama 1 Semester" />
				<meta name="description" content="Saya Eko Kurniawan Khannedy, Technical Architect di Blibli.com. Dengan pengalaman saya lebih dari 15 tahun berkarya dan 11 tahun berkarir di bidang programming akan membantu kamu belajar coding lebih cepat dan terarah untuk menjadi Programmer Zaman Now" />

				<meta property="og:type" content="website" />
				<meta property="og:url" content="https://codepolitan.com/program/mobirise" />
				<meta property="og:title" content="Kursus Coding Online Programmer Zaman Now Selama 1 Semester" />
				<meta property="og:description" content="Saya Eko Kurniawan Khannedy, Technical Architect di Blibli.com. Dengan pengalaman saya lebih dari 15 tahun berkarya dan 11 tahun berkarir di bidang programming akan membantu kamu belajar coding lebih cepat dan terarah untuk menjadi Programmer Zaman Now" />
				<meta property="og:image" content="/assets/img/programmerzamannow/og-image-pzn.jpg" />

				<meta property="twitter:card" content="summary_large_image" />
				<meta property="twitter:url" content="https://codepolitan.com/program/mobirise" />
				<meta property="twitter:title" content="Kursus Coding Online Programmer Zaman Now Selama 1 Semester" />
				<meta property="twitter:description" content="Saya Eko Kurniawan Khannedy, Technical Architect di Blibli.com. Dengan pengalaman saya lebih dari 15 tahun berkarya dan 11 tahun berkarir di bidang programming akan membantu kamu belajar coding lebih cepat dan terarah untuk menjadi Programmer Zaman Now" />
				<meta property="twitter:image" content="/assets/img/programmerzamannow/og-image-pzn.jpg" />
			</Head>
			<Layout>
				<HeroRoadmap
					title={title}
					subtitle={description}
					thumbnail={thumbnail}
				/>
				<section className="section">
					<div className="container p-5">
						<div className="row mb-4">
							<div className="col">
								<span className="text-muted"><Link href="/programmerzamannow"><a className="link text-primary">Home</a></Link> &gt; <Link href="/programmerzamannow/roadmap"><a className="link text-primary">Roadmap</a></Link> &gt; Kelas {title}</span>
							</div>
						</div>
						<div className="row mb-5">
							<div className="col-12 col-md-6 col-lg-9 mb-3 mb-lg-0">
								<h2 className="section-title">Kelas {title}</h2>
							</div>
							<div className="col-12 col-md-6 col-lg-3">
								<div className="input-group">
									<input onChange={(e) => setSearch(e.target.value)} type="search" className="form-control shadow-none border-end-0" placeholder="cari kelas" />
									<span className="input-group-text bg-white border-start-0 text-muted"><FontAwesomeIcon icon={faSearch} /></span>
								</div>
							</div>
						</div>
						<div className="row my-3">
							{course.filter((value) => {
								if (search === "") {
									return value;
								} else if (value.course_title.toLowerCase().includes(search.toLowerCase())) {
									return value;
								}
							}).map((item, index) => {
								return (
									<div className="col-md-6 col-lg-4 col-xl-3 mb-3" key={index}>
										<CourseCard
											index={index + 1}
											thumbnail={item.thumbnail}
											title={item.course_title}
											totalModule={item.total_module}
											link={`/programmerzamannow/course/${item.slug}`}
										/>
									</div>
								);
							})}
						</div>
					</div>
				</section>
				<section className="section" id="buy">
					<div className="container p-5">
						<div className="row">
							<div className="col-lg-6">
								<h2 className="section-title">Siap untuk Memulai?</h2>
								<p className="text-muted my-3">Pilih salah satu dari paket belajar berikut untuk bergabung dalam program</p>
								<div className="d-flex align-items-start">
									<FontAwesomeIcon style={{ fontSize: '2em' }} className="text-primary me-3" icon={faCheckSquare} />
									<p className="text-muted">Kelas online premium yang tidak ada di Youtube gratis Programmer Zaman Now</p>
								</div>
								<div className="d-flex align-items-start">
									<FontAwesomeIcon style={{ fontSize: '2em' }} className="text-primary me-3" icon={faCheckSquare} />
									<p className="text-muted">Peluang terhubung dengan industri melalui fitur Talent Hub Codepolitan</p>
								</div>
								<div className="d-flex align-items-start">
									<FontAwesomeIcon style={{ fontSize: '2em' }} className="text-primary me-3" icon={faCheckSquare} />
									<p className="text-muted">Semua materi belajar dan semua roadmap Belajar (tidak hanya 1 kelas)</p>
								</div>
								<div className="d-flex align-items-start">
									<FontAwesomeIcon style={{ fontSize: '2em' }} className="text-primary me-3" icon={faCheckSquare} />
									<p className="text-muted">Forum diskusi tanya jawab yang dijawab langsung oleh mentor</p>
								</div>
								<div className="d-flex align-items-start">
									<FontAwesomeIcon style={{ fontSize: '2em' }} className="text-primary me-3" icon={faCheckSquare} />
									<p className="text-muted">Mentoring tatap muka secara daring 1 bulan sekali</p>
								</div>
								<div className="d-flex align-items-start">
									<FontAwesomeIcon style={{ fontSize: '2em' }} className="text-primary me-3" icon={faCheckSquare} />
									<p className="text-muted">Akses materi belajar sesuai dengan paket waktu belajar yang kamu pilih</p>
								</div>
								<div className="d-flex align-items-start">
									<FontAwesomeIcon style={{ fontSize: '2em' }} className="text-primary me-3" icon={faCheckSquare} />
									<p className="text-muted">Garansi 100% uang kembali jika dalam 3 hari tidak puas</p>
								</div>
							</div>
							<div className="col-lg-6 mt-5 mt-md-0">
								{/* Buy PZN Roadmap */}
								<div id="buyPznRoadmap">
									<ul className="nav nav-pills mb-3" id="pills-tab" role="tablist">
										<li className="nav-item" role="presentation">
											<button
												style={{
													borderTopLeftRadius: '25px',
													borderBottomLeftRadius: '25px',
													borderTopRightRadius: 0,
													borderBottomRightRadius: 0
												}}
												className="shadow-sm nav-link active"
												id="pills-bulanan-tab"
												data-bs-toggle="pill"
												data-bs-target="#pills-bulanan"
												type="button"
												role="tab"
												aria-controls="pills-bulanan"
												aria-selected="true"
											>
												Bulanan
											</button>
										</li>
										<li className="nav-item" role="presentation">
											<button
												style={{
													borderTopLeftRadius: 0,
													borderBottomLeftRadius: 0,
													borderTopRightRadius: '25px',
													borderBottomRightRadius: '25px'
												}}
												className="shadow-sm nav-link"
												id="pills-lifetime-tab"
												data-bs-toggle="pill"
												data-bs-target="#pills-lifetime"
												type="button" role="tab"
												aria-controls="pills-lifetime"
												aria-selected="false"
											>
												Lifetime
											</button>
										</li>
									</ul>
									<div className="tab-content" id="pills-tabContent">
										<div className="tab-pane fade show active" id="pills-bulanan" role="tabpanel" aria-labelledby="pills-bulanan-tab">
											{product_link === null ?
												(<p className="text-muted my-5">Paket belum tersedia</p>) :
												product_link.map((item, index) => {
													return (
														<div onClick={index === 1 && handleClick1 || index === 2 && handleClick2 || index === 3 && handleClick3} key={index}>
															<PriceCard
																checked={index === 1 && roadmapOneMonthisChecked || index === 2 && roadmapFourMonthisChecked || index === 3 && roadmapSixMonthisChecked}
																title={item.product_name}
																subtitle={item.product_description}
																badgeText={item.note}
																delPrice={item.normal_price}
																price={item.retail_price}
															/>
														</div>
													);
												}).slice(1, 4)}
											<div className="mt-4">
												<a className="btn btn-primary d-block py-3" style={{ fontSize: '1.3rem' }} href={slug !== "" ? `http://pay.codepolitan.com/?slug=${slug}` : undefined}>Beli Roadmap</a>
											</div>
										</div>
										<div className="tab-pane fade" id="pills-lifetime" role="tabpanel" aria-labelledby="pills-lifetime-tab">
											{product_link === null ?
												(<p className="text-muted my-5">Paket belum tersedia</p>) :
												product_link.map((item, index) => {
													return (
														<div key={index}>
															<PriceCard
																checked={true}
																title={item.product_name}
																subtitle={item.product_description}
																badgeText={item.note}
																delPrice={item.normal_price}
																price={item.retail_price}
															/>
															<div className="mt-4">
																<a className="btn btn-primary d-block py-3" style={{ fontSize: '1.3rem' }} href={`http://pay.codepolitan.com/?slug=${item.product_slug}`}>Beli Roadmap</a>
															</div>
														</div>
													);
												}).slice(0, 1)}
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<WarrantySection backgroundColor="bg-white" />
				<RoadmapSection />
			</Layout>
		</>
	);
};

export default RoadmapPage;
