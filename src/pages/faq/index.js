import { useState } from 'react';
import Head from 'next/head';
import Layout from '../../components/global/Layout/Layout';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faWhatsapp } from '@fortawesome/free-brands-svg-icons';

const Faq = () => {
    const [data] = useState([
        {
            id: 'flush-heading1',
            target: 'flush-collapse1',
            title: 'Apa itu CODEPOLITAN?',
            content: 'CODEPOLITAN adalah platform belajar coding online yang dikembangkan khusus untuk membantu kamu belajar coding lebih terarah.'
        },
        {
            id: 'flush-heading2',
            target: 'flush-collapse2',
            title: 'Apakah belajar di CODEPOLITAN gratis?',
            content: 'Beberapa materi belajar bisa diakses secara gratis. Namun jika kamu ingin hasil belajar maksimal dan dukungan maksimal tentu saja kamu hanya bisa dapatkan di program berbayar.'
        },
        {
            id: 'flush-heading3',
            target: 'flush-collapse3',
            title: 'Apakah dengan belajar di program ini dijamin bisa coding?',
            content: 'Dengan mengikuti program belajar di program ini, jika kamu benar-benar mengikutinya dengan baik kami jamin kamu bisa coding bahkan bisa menjadi seorang developer profesional. Namun tingkat kemahiranmu dalam coding bergantung usaha belajar yang kamu lakukan. Usaha selalu berbanding lurus dengan hasil yang didapatkan. Sebagus apapun sebuah program belajar, tentu hanya akan berguna bagi mereka yang menjalankan setiap prosesnya.'
        },
        {
            id: 'flush-heading4',
            target: 'flush-collapse4',
            title: 'Apakah dengan mengikuti program belajar premium di CODEPOLITAN bisa membantu saya lulus kuliah di jurusan IT?',
            content: 'Itu tergantung bagaimana kamu memanfaatkan semua fasilitas yang kami berikan dan tergantung usaha kamu dalam belajar.'
        },
        {
            id: 'flush-heading5',
            target: 'flush-collapse5',
            title: 'Apakah belajar di CODEPOLITAN itu online?',
            content: 'Iya. Untuk belajar offline kamu bisa mengunjungi <a className="link text-primary" href="https://devschool.id">Developer School</a>'
        },
        {
            id: 'flush-heading6',
            target: 'flush-collapse6',
            title: 'Saya newbie, apakah saya bisa belajar di CODEPOLITAN?',
            content: 'Bisa. Materi di CODEPOLITAN bisa dipelajari oleh siapa saja termasuk yang baru mulai belajar coding. Kami sudah menyusun alur belajarnya.'
        },
        {
            id: 'flush-heading7',
            target: 'flush-collapse7',
            title: 'Bagaiman cara belajar di program ini?',
            content: 'Silahkan pilih dan beli kelas yang ingin dipelajari lalu redeem voucher yang di kirimkan melalui email dan whatsapp di dashboard CODEPOLITAN.'
        },
        {
            id: 'flush-heading8',
            target: 'flush-collapse8',
            title: 'Apakah saya akan diajarkan dari nol sampai mahir?',
            content: 'Konsep belajar online adalah belajar mandiri. Kami telah mempersiapkan materi belajar yang bisa kamu pelajari. Dan kami telah menyiapkan jalur belajarnya, sehingga buat kamu yang baru ingin memulai belajar pemrograman juga bisa mengikutinya. Kamu bisa menjadi mahir, jika kamu mengusahakannya dengan mempelajari semua materinya dan mempraktikkannya.'
        },
        {
            id: 'flush-heading9',
            target: 'flush-collapse9',
            title: 'Apakah saya bisa bertanya jika mengalami kesulitan?',
            content: 'Kamu bisa bertanya sesuai dengan materi belajar yang kamu pelajari melalui kolom diskusi pada akhir setiap lesson. Bertanyalah dengan jelas dan sopan agar member lainnya atau tim developer CODEPOLITAN bisa membantu masalahmu.'
        },
        {
            id: 'flush-heading10',
            target: 'flush-collapse10',
            title: 'Apakah akan mendapatkan sertifikat?',
            content: 'Ada beberapa kelas online yang memiliki sertifikat dan ada juga yang tidak memiliki sertifikat.'
        },
        {
            id: 'flush-heading11',
            target: 'flush-collapse11',
            title: 'Bagaimana saya bisa mengikuti webinar?',
            content: 'Kamu bisa mengikuti webinar melalui channel Youtube CODEPOLITAN. Jangan lupa subscribe ya :)'
        },
        {
            id: 'flush-heading12',
            target: 'flush-collapse12',
            title: 'Kapan webinar diselenggarakan?',
            content: 'Webinar biasanya diselenggarakan setiap bulan sekali. Untuk mendapatkan informasi webinar silahkan subscribe channel Youtube CODEPOLITAN atau bergabung dalam Telegram Group Info CODEPOLITAN setelah kami bergabung dalam program.'
        },
        {
            id: 'flush-heading13',
            target: 'flush-collapse13',
            title: 'Apakah saya bisa bergabung di dalam grup Telegram Info CODEPOLITAN?',
            content: 'Telegram Group Info CODEPOLITAN hanya untuk member premium yang telah mengikuti program berbayar CODEPOLITAN. So, jika ingin bergabung dalam Telegram Group Info CODEPOLITAN silahkan gabung dalam program berbayar CODEPOLITAN.'
        },
        {
            id: 'flush-heading14',
            target: 'flush-collapse14',
            title: 'Apakah pembayarannya bisa dicicil?',
            content: 'Mohon maaf untuk saat ini kami belum bisa menerima pembayaran secara dicicil. Silahkan ditabung terlebih dahulu saja, jika sudah terkumpul baru mendaftar :)'
        },
        {
            id: 'flush-heading15',
            target: 'flush-collapse15',
            title: 'Apakah saya bisa memperpanjang waktu keanggotaan Membership saya?',
            content: 'Bisa.'
        },
        {
            id: 'flush-heading16',
            target: 'flush-collapse16',
            title: 'Apakah saya bisa melakukan perpanjangan masa belajar dengan paket belajar yang berbeda dari sebelumnya?',
            content: 'Bisa. Dan jika kamu masih memiliki masa aktif belajar di paket yang sebelumnya, maka masa aktifmu akan diakumulasi.'
        },
        {
            id: 'flush-heading17',
            target: 'flush-collapse17',
            title: 'Apakah materi belajar bisa saya download?',
            content: 'Tidak bisa, semua materi belajar yang tersedia di CODEPOLITAN tidak bisa di download.'
        },
    ]);

    return (
        <>
            <Head>
                <title>Frequently Asked Questions - Codepolitan</title>
                <meta name="title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta name="description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="og:type" content="website" />
                <meta property="og:url" content="https://codepolitan.com" />
                <meta property="og:title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta property="og:description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="og:image" content="https://i.ibb.co/SXP8fGc/OG-image.jpg" />
                <meta property="twitter:card" content="summary_large_image" />
                <meta property="twitter:url" content="https://codepolitan.com" />
                <meta property="twitter:title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta property="twitter:description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="twitter:image" content="https://i.ibb.co/SXP8fGc/OG-image.jpg" />
            </Head>
            <Layout>
                <section className="section" style={{ marginTop: '100px' }}>
                    <div className="container p-4 p-lg-5">
                        <div className="row mb-4">
                            <div className="col">
                                <h1 className="section-title mb-3">Pertanyaan Umum</h1>
                                <p className="text-muted">Berikut ini pertanyaan umum yang sering ditanyakan kepada kami, mungkin salah satunya adalah pertanyaan yang ingin kamu tanyakan.</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col">
                                <div
                                    className="accordion accordion-flush"
                                    id="FaqProgramAccordion"
                                >
                                    {data.map((item, index) => {
                                        return (
                                            <div className="accordion-item" key={index}>
                                                <h3 className="accordion-header" id={item.id}>
                                                    <button
                                                        className="accordion-button collapsed"
                                                        type="button"
                                                        data-bs-toggle="collapse"
                                                        data-bs-target={`#${item.target}`}
                                                        aria-expanded="false"
                                                        aria-controls={item.target}
                                                    >
                                                        <strong className="text-muted">{item.title}</strong>
                                                    </button>
                                                </h3>
                                                <div
                                                    id={item.target}
                                                    className="accordion-collapse collapse"
                                                    aria-labelledby={item.id}
                                                    data-bs-parent="#FaqProgramAccordion"
                                                >
                                                    <div className="accordion-body text-muted">
                                                        <div dangerouslySetInnerHTML={{ __html: item.content }} />
                                                    </div>
                                                </div>
                                            </div>
                                        );
                                    })}
                                </div>
                            </div>
                        </div>
                        <div className="row mt-5 py-5">
                            <div className="col text-center">
                                <h5 className="section-title">Masih punya pertanyaan lain?</h5>
                                <p className="text-muted mb-3">Tanyakan disini pada jam kerja, tim kami akan segera menjawabnya!</p>
                                <a className="btn btn-primary btn-lg" href="https://wa.me/628999488990">
                                    <FontAwesomeIcon className="me-2" icon={faWhatsapp} />
                                    Hubungi kami
                                </a>
                            </div>
                        </div>
                    </div>
                </section>
            </Layout>
        </>
    );
};

export default Faq;
