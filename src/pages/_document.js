import Document, { Html, Head, Main, NextScript } from 'next/document';

class MyDocument extends Document {
    render() {
        return (
            <Html>
                <Head>
                    <link rel="manifest" href="/manifest.json" />
                    <link rel="apple-touch-icon" href="/icon-192x192.png"></link>
                    <meta name="theme-color" content="#14a7a0" />
                    <meta name="keywords" content="codepolitan, developer, fullstack, frontend, backend, programmer, programming, belajar, coding, code, ngoding, android, laravel, javascript, html, css, sass, nodejs, murah, flashsale, programmerzamannow, pzn" />
                    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,400;0,500;0,600;0,700;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet" />
                    <script dangerouslySetInnerHTML={{
                        __html: `(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                      })(window,document,'script','dataLayer','GTM-5XDJJJW');`,
                    }} />
                    <script data-host="https://webanalytic.info" data-dnt="false" src="https://webanalytic.info/js/script.js" id="ZwSg9rf6GA" async defer></script>
                </Head>
                <body>
                    <noscript dangerouslySetInnerHTML={{
                        __html: `<iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5XDJJJW" height="0" width="0" style="display:none;visibility:hidden"></iframe>`
                    }} />
                    <Main />
                    <NextScript />
                </body>
            </Html>
        )
    }
}

export default MyDocument;
