import Head from 'next/head';
import Layout from '../../components/global/Layout/Layout';


const Howtolearn = () => {
    return (
        <>
            <Head>
                <title>Cara Belajar - Codepolitan</title>
                <meta name="title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta name="description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="og:type" content="website" />
                <meta property="og:url" content="https://codepolitan.com" />
                <meta property="og:title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta property="og:description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="og:image" content="https://i.ibb.co/SXP8fGc/OG-image.jpg" />
                <meta property="twitter:card" content="summary_large_image" />
                <meta property="twitter:url" content="https://codepolitan.com" />
                <meta property="twitter:title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta property="twitter:description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="twitter:image" content="https://i.ibb.co/SXP8fGc/OG-image.jpg" />
            </Head>
            <Layout>
                <section className="section" style={{ marginTop: '100px' }}>
                    <div className="container p-4 p-lg-5">
                        <div className="row mb-5">
                            <div className="col">
                                <h1 className="section-title h2">Cara Belajar</h1>
                                <p className="lead">
                                    <strong>Cara memaksimalkan manfaat dalam belajar di CODEPOLITAN:</strong>
                                </p>
                                <ol type="1" className="text-muted">
                                    <li>Login dengan akun yang terdaftar di CODEPOLITAN</li>
                                    <li>Klik &quot;Explore Course&quot; untuk melihat kelas-kelas yang ada di CODEPOLITAN.</li>
                                    <li>Pilih kelas yang ingin kamu pelajari</li>
                                    <li>Bila kamu sudah mempelajari videonya, jangan lupa untuk klik button &quot;Ok, Saya Sudah Mengerti&quot; untuk menandai kalau kamu sudah beres menyelesaikan kelas tersebut</li>
                                    <li>Pelajari semua materi belajar dalam kelas tersebut, kemudian jangan lupa untuk ATM alias amati, tiru, dan modifikasi</li>
                                    <li>Pada video terakhir, jangan lupa untuk memberi rating kelas agar kamu bisa klaim sertifikat penyelesaian kelas</li>
                                </ol>
                            </div>
                        </div>
                        <div className="row mb-5">
                            <div className="col">
                                <h2 className="section-title">Jenis Kelas Online</h2>
                                <p className="lead">
                                    <strong>Ada 3 Jenis kelas online yang ada di CODEPOLITAN, yaitu:</strong>
                                </p>
                                <ol type="1" className="text-muted">
                                    <li>Kelas Gratis, yaitu kelas online CODEPOLITAN yang bisa diakses oleh siapa saja asalkan sudah memiliki akun CODEPOLITAN.</li>
                                    <li>Kelas Berbayar, yaitu kelas online CODEPOLITAN yang hanya bisa diakses setelah user membeli kelas tersebut terlebih dahulu. Dengan membeli kelas ini, kamu bisa mengakses kelas ini selamanya! Kelas berbayar juga bisa diakses oleh user dengan status Premium Membership.</li>
                                    <li>Kelas Membership, yaitu kelas online CODEPOLITAN yang hanya bisa diakses oleh user dengan status Premium Membership.</li>
                                </ol>
                            </div>
                        </div>
                        <div className="row mb-5">
                            <div className="col">
                                <h2 className="section-title">Membership</h2>
                                <p className="text-muted">
                                    Premium Membership atau biasa disebut membership merupakan program berlangganan konten belajar CODEPOLITAN dengan batas waktu tertentu. Ketika kamu sudah berlangganan paket membership, kamu bisa memiliki akses terhadap semua konten belajar yang ada di CODEPOLITAN. User yang telah memiliki akses premium membership, bisa mengakses semua konten belajar yang ada di CODEPOLITAN tanpa terkecuali selama masa premium membershipnya masih aktif. User yang status premium membershipnya sudah expired atau berakhir, bisa melakukan perpanjangan kapan saja sesuai dengan kebutuhannya.
                                    Info lebih lengkap terkait program Premium Membership bisa diakses melalui: <a className="link" href="https://www.codepolitan.com/membership">https://www.codepolitan.com/membership</a>
                                </p>
                            </div>
                        </div>
                        <div className="row mb-5">
                            <div className="col">
                                <h2 className="section-title">Jalur Belajar</h2>
                                <p className="text-muted">
                                    Jalur belajar (learning path) merupakan fitur yang bisa digunakan oleh user CODEPOLITAN untuk membantu mengarahkan alur belajar, sehingga user tidak perlu lagi kebingungan harus memulai belajar dari mana. Jalur belajar bersifat sebagai panduan saja, bagi user yang telah mengetahui apa yang ingin dipelajarinya tidak diwajibkan mengikuti jalur belajar, user tersebut bisa langsung menuju kelas yang diinginkannya untuk dipelajari.
                                </p>
                            </div>
                        </div>
                    </div>
                </section>
            </Layout>
        </>
    );
};

export default Howtolearn;
