import Head from 'next/head';
import Layout from '../../components/global/Layout/Layout';


const TermsAndCondition = () => {
    return (
        <>
            <Head>
                <title>Aturan dan Syarat Penggunaan - Codepolitan</title>
                <meta name="title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta name="description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="og:type" content="website" />
                <meta property="og:url" content="https://codepolitan.com" />
                <meta property="og:title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta property="og:description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="og:image" content="https://i.ibb.co/SXP8fGc/OG-image.jpg" />
                <meta property="twitter:card" content="summary_large_image" />
                <meta property="twitter:url" content="https://codepolitan.com" />
                <meta property="twitter:title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta property="twitter:description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="twitter:image" content="https://i.ibb.co/SXP8fGc/OG-image.jpg" />
            </Head>
            <Layout>
                <section className="section" style={{ marginTop: '100px' }}>
                    <div className="container p-4 p-lg-5">
                        <div className="row mb-4">
                            <div className="col">
                                <h1 className="section-title mb-5">Aturan dan Syarat Penggunaan</h1>
                                <h2 className="section-title h3">Akun Member</h2>
                                <ul className="text-muted">
                                    <li>Program CODEPOLITAN ditujukan untuk Warga Negara Indonesia (WNI).</li>
                                    <li>Setiap member hanya dapat memiliki satu akun, yang diverifikasikan dengan alamat email.</li>
                                    <li>Setiap member yang terdaftar dan terverifikasi di CODEPOLITAN disebut Member CODEPOLITAN.</li>
                                    <li>Setiap member yang bergabung sebagai Member CODEPOLITAN berarti telah sepakat dan bersedia sepenuh hati untuk mentaati segala syarat dan ketentuan yang berlaku di CODEPOLITAN.</li>
                                    <li>CODEPOLITAN berhak untuk menonaktifkan sementara, menonaktifkan secara permanen, dan menghapus akun member apabila pada akun atau perilaku pengguna akun tersebut ditemukan kejanggalan, usaha untuk melakukan kecurangan, ketidakjujuran dan atau melanggar syarat ketentuan yang berlaku di CODEPOLITAN.</li>
                                </ul>
                            </div>
                        </div>
                        <div className="row mb-4">
                            <div className="col">
                                <h3 className="section-title">Program Belajar</h3>
                                <ul className="text-muted">
                                    <li>Program belajar CODEPOLITAN adalah semua skema belajar secara daring yang disediakan oleh CODEPOLITAN melalui web https://www.codepolitan.com</li>
                                    <li>Program belajar CODEPOLITAN disajikan dalam bentuk video dan teks yang tersusun dalam rangkaian kelas online CODEPOLITAN.</li>
                                    <li>Kelas Online CODEPOLITAN adalah kumpulan materi pembelajaran dalam satu pembahasan yang telah disusun secara sistematis yang bisa diakses secara daring melalui web CODEPOLITAN.</li>
                                    <li>CODEPOLITAN menyediakan program belajar gratis dan program belajar berbayar.</li>
                                    <li>Member memiliki kebebasan memilih untuk belajar melalui program belajar gratis maupun berbayar.</li>
                                    <li>Program belajar berbayar di CODEPOLITAN ditujukan agar member belajar lebih terstruktur dan mendalam mengenai pemorgraman sesuai dengan topik yang dipilih.</li>
                                    <li>Program belajar berbayar di CODEPOLITAN dibedakan menjadi 2 jenis, yaitu Program Membership (keanggotaan) dan Program Kelas Satuan.</li>
                                    <li>Program Membership adalah program belajar keanggotaan di mana user bisa mengakses semua kelas online yang ada di CODEPOLITAN dengan batasan waktu sesuai dengan paket belajar yang dipilih oleh member.</li>
                                    <li>Dalam Program Membership CODEPOLITAN telah menyediakan beragam jalur belajar yang bisa dipilih oleh member.</li>
                                    <li>Jalur belajar adalah kumpulan materi belajar yang terdiri dari beberapa kelas online yang telah disusun secara komprehensif untuk memudahkan member dalam belajar secara terarah dengan tahapan yang jelas.</li>
                                    <li>Program Kelas Satuan adalah program belajar lifetime di mana user bisa membeli kelas tertentu dan memiliki akses selamanya untuk kelas tersebut.</li>
                                    <li>CODEPOLITAN berhak mengubah/mengurangi/menghapus materi belajar pada program belajar dan lain hal di masa depan apabila terjadi ketidaksesuaian materi yang disampaikan dan masalah lainnya.</li>
                                </ul>
                            </div>
                        </div>
                        <div className="row mb-4">
                            <div className="col">
                                <h3 className="section-title">Materi Belajar</h3>
                                <ul className="text-muted">
                                    <li>Materi belajar CODEPOLITAN tersusun dalam rangkaian kelas online yang disajikan dalam format video dan teks.</li>
                                    <li>Materi belajar CODEPOLITAN bisa diakses kapan saja dan di mana saja oleh member selama member memiliki hak akses untuk melihat materi tersebut.</li>
                                    <li>Materi belajar bisa dipelajari secara bertahap sesuai dengan urutan belajar yang disediakan atau bisa juga diakses melompat sesuai dengan materi yang ingin dipelajari member.</li>
                                    <li>Materi belajar CODEPOLITAN tidak bisa diunduh oleh member.</li>
                                    <li>Materi belajar dinyatakan selesai dipelajari hanya jika member menekan tombol &quot;SAYA SUDAH PAHAM&quot; yang terletak di akhir setiap materi belajar.</li>
                                    <li>Semua materi belajar pemrograman di CODEPOLITAN mengandung hak cipta. Semua pelanggaran terkait hak cipta akan diproses secara hukum dengan denda senilai Rp 45.000.000.000.</li>
                                    <li>Termasuk pelanggaran hak cipta di antaranya adalah: mengunduh dan menyebarkan sebagian atau seluruh materi belajar melalui media apapun kepada publik atau kepada orang lain tanpa izin tertulis dari CODEPOLITAN.</li>
                                </ul>
                            </div>
                        </div>
                        <div className="row mb-4">
                            <div className="col">
                                <h3 className="section-title">Sertifikat</h3>
                                <ul className="text-muted">
                                    <li>CODEPOLITAN menerbitkan sertifikat digital pada kelas online tertentu yang bisa diklaim oleh member hanya jika member telah menyelesaikan seluruh materi belajar pada kelas online tersebut.</li>
                                    <li>CODEPOLITAN berhak menerbitkan dan atau tidak menerbitakan sertifikat pada sebuah kelas online tertentu.</li>
                                    <li>Sertifikat digital CODEPOLITAN memiliki kode verifikasi yang bisa dilihat keabsahannya melalui web CODEPOLITAN.</li>
                                    <li>Sertifikat digital CODEPOLITAN bisa diunduh dan dibagikan kepada publik.</li>
                                    <li>CODEPOLITAN berhak membatalkan dan atau menghapus sertifikat member jika ditemukan kejanggalan atau kecurangan yang dilakukan oleh member dalam mendapatkannya.</li>
                                    <li>Sertifikat yang sudah diklaim oleh member akan berlaku seumur hidup.</li>
                                </ul>
                            </div>
                        </div>
                        <div className="row mb-4">
                            <div className="col">
                                <h3 className="section-title">Forum Diskusi dan Tanya Jawab</h3>
                                <ul className="text-muted">
                                    <li>Forum Diskusi Tanya Jawab CODEPOLITAN adalah forum tanya jawab yang bisa diakses melalui web dengan alamat https://www.codepolitan.com/forum.</li>
                                    <li>Forum Diskusi Tanya Jawab CODEPOLITAN ditujukan untuk membantu member yang mengikuti program belajar berbayar di CODEPOLITAN jika mengalami kesulitan atau kendala dalam memahami materi belajar.</li>
                                    <li>Forum Diskusi Tanya Jawab CODEPOLITAN dihubungkan dengan materi belajar, sehingga member bisa bertanya tepat terkait materi belajar yang sedang dipelajari.</li>
                                    <li>Member bisa bertanya dalam Forum Diskusi Tanya Jawab CODEPOLITAN melalui kolom diskusi yang terletak pada bagian bawah materi belajar.</li>
                                    <li>Forum Diskusi Tanya Jawab CODEPOLITAN hanya tersedia untuk kelas online berbayar.</li>
                                    <li>Member hanya bisa bertanya pada Forum Diskusi Tanya Jawab CODEPOLITAN sesuai dengan kelas online yang dia memiliki hak akses padanya.</li>
                                    <li>Member hanya bisa bertanya seputar materi yang disampaikan dalam materi belajar.</li>
                                    <li>Member dilarang untuk menyebarkan berita hoax/spam/berjualan di Forum Diskusi Tanya Jawab CODEPOLITAN.</li>
                                    <li>CODEPOLITAN berhak untuk tidak mempublikasi atau bahkan menghapus pertanyaan yang tidak bersesuaian dengan topik materi belajar dan aturan Forum Diskusi Tanya Jawab CODEPOLITAN.</li>
                                    <li>Member bisa menjawab pertanyaan yang diajukan oleh member lain pada Forum Diskusi Tanya Jawab CODEPOLITAN.</li>
                                    <li>Member bisa mengakses semua pertanyaan dan menjawab pertanyaan pada Forum Diskusi Tanya Jawab CODEPOLITAN tanpa harus mengikuti program berbayar terlebih dahulu.</li>
                                    <li>Member akan mendapatkan point setiap kali berkontribusi menjawab pertanyaan yang ada pada Forum Diskusi Tanya Jawab CODEPOLITAN.</li>
                                    <li>Tim CODEPOLITAN akan membantu menjawab pembahasan yang diajukan melalui Forum Diskusi Tanya Jawab.</li>
                                    <li>Tim CODEPOLITAN tidak berkewajiban untuk menjawab setiap pertanyaan yang diajukan oleh member terutama bila pertanyaan yang diajukan tidak bersesuaian dengan topik yang dipilih.</li>
                                </ul>
                            </div>
                        </div>
                        <div className="row mb-4">
                            <div className="col">
                                <h3 className="section-title">Grup Telegram dan Discord</h3>
                                <ul className="text-muted">
                                    <li>Telegram Group CODEPOLITAN bernama Info CODEPOLITAN diperuntukan hanya untuk member yang mengikuti program belajar berbayar CODEPOLITAN.</li>
                                    <li>Member berhak untuk bergabung atau tidak bergabung dalam Telegram Group CODEPOLITAN.</li>
                                    <li>Telegram Group CODEPOLITAN bukanlah forum tanya jawab member, melainkan grup silaturahmi dan info khusus member.</li>
                                    <li>Member tidak diperkenankan bertanya seputar pertanyaan teknis pemrograman pada Telegram Group CODEPOLITAN.</li>
                                    <li>Member bisa bergabung dalam Telegram Group CODEPOLITAN setelah mengikuti salah satu program belajar berbayar CODEPOLITAN dan bisa join melalui halaman dashboard user.</li>
                                    <li>CODEPOLITAN akan memberikan informasi seputar program belajar, rilis kelas online baru, pembaharuan materi dan jalur belajar, event, webinar, lowongan kerja, artikel serta info terkait pemrograman dan teknologi melalui Telegram Group CODEPOLITAN.</li>
                                    <li>Member dilarang untuk menyebarkan berita hoax/spam/berjualan di Telegram Group CODEPOLITAN.</li>
                                    <li>Member dilarang melakukan postingan bersifat provokasi, kebencian, syara, rasisme, menimbulkan kegaduhan dan pornografi dalam Telegram Group CODEPOLITAN.</li>
                                    <li>Segala postingan member di Telegram Group CODEPOLITAN bersifat informasi dan sharing.</li>
                                    <li>CODEPOLITAN tidak mempunyai kewajiban untuk menjawab pertanyaan teknis yang diajukan di Telegram Group CODEPOLITAN.</li>
                                    <li>CODEPOLITAN berhak mengeluarkan member apabila pada akun atau perilaku pengguna akun tersebut ditemukan kejanggalan, usaha untuk melakukan kecurangan, ketidakjujuran dan atau melanggar syarat ketentuan yang berlaku di CODEPOLITAN.</li>
                                </ul>
                            </div>
                        </div>
                        <div className="row mb-4">
                            <div className="col">
                                <h3 className="section-title">Pembayaran</h3>
                                <ul className="text-muted">
                                    <li>Fitur pembayaran merupakan fitur yang diberikan kepada member CODEPOLITAN agar bisa bergabung dalam program belajar berbayar yang disediakan.</li>
                                    <li>CODEPOLITAN memberikan pilihan dalam metode pembayaran, yaitu otomatis dan manual.</li>
                                    <li>Pembayaran otomatis memungkinkan untuk paket belajar yang diikuti oleh member secara otomatis aktif setelah melakukan pembayaran selama pembayaran dilakukan sesuai dengan instruksi yang telah diberikan.</li>
                                    <li>Pembayaran manual memerlukan adanya verifikasi transaksi secara manual dan mewajibkan member untuk melakukan konfirmasi pembayaran serta mengunggah bukti transaksi saat melakukan konfirmasi pembayaran.</li>
                                    <li>Dalam pembayaran manual, member dituntut untuk melakukan pembayaran dengan nominal tepat disertai 3 angka unik sebagaimana yang diinstruksikan agar mempermudah dan amempercepat proses verifikasi pembayaran.</li>
                                    <li>Keterlambatan proses aktivasi program belajar akibat kesalahan nominal transfer member bukanlah tanggung jawab CODEPOLITAN.</li>
                                    <li>Program belajar akan aktif selambat-lambatnya 3x24 jam hari kerja setelah melakukan pembayaran (tidak termasuk hari libur/weekend).</li>
                                    <li>CODEPOLITAN berhak untuk mengubah/mengurangi/menambah besaran biaya program belajar yang ada pada CODEPOLITAN baik bersifat sementara ataupun permanen dengan atau tanpa pemberitahuan kepada member.</li>
                                </ul>
                            </div>
                        </div>
                        <div className="row mb-4">
                            <div className="col">
                                <h3 className="section-title">Garansi Belajar</h3>
                                <ul className="text-muted">
                                    <li>Member yang telah bergabung dalam program belajar berbayar CODEPOLITAN berhak untuk mengklaim uang kembali dengan batas waktu maksimal 3 hari setelah melakukan pembayaran.</li>
                                    <li>Klaim garansi setelah 3 hari tidak akan ditindaklanjuti oleh CODEPOLITAN.</li>
                                    <li>Untuk melakukan klaim garansi, member harus menghubungi admin melalui WhatsApp di nomor +62895701067659 atau melalui email info@codepolitan.com, kemudian menginformasikan perihal klaim garansi uang kembali.</li>
                                    <li>Uang garansi akan ditransfer selambat-lambatnya 3 hari setelah klaim diajukan (tidak termasuk hari libur/weekend).</li>
                                </ul>
                            </div>
                        </div>
                        <div className="row mb-4">
                            <div className="col">
                                <h3 className="section-title">Merchandise</h3>
                                <ul className="text-muted">
                                    <li>Semua merchandise yang disertakan dalam paket belajar CODEPOLITAN tidak bisa dibeli secara terpisah.</li>
                                    <li>Merchandise dalam paket belajar CODEPOLITAN bersifat bonus.</li>
                                    <li>Pengiriman bonus merchandise dilakukan oleh CODEPOLITAN hanya dalam 2 periode dalam setiap bulannya, yaitu awal bulan dan pertengahan bulan.</li>
                                    <li>Pembelian paket belajar setelah tanggal 15, akan dikirimkan bonus merchandisenya di awal bulan.</li>
                                    <li>Pembelian paket bekajar sebelum tanggal 15, akan dikirimkan bonus merchandisenya di pertengahan bulan.</li>
                                    <li>Bonus merchandise hanya berlaku untuk pembelian paket belajar pertama kali, perpanjangan paket belajar tidak mendapatkan merchandise.</li>
                                </ul>
                            </div>
                        </div>
                        <div className="row mb-4">
                            <div className="col">
                                <h3 className="section-title">Apabila ditemukan kejanggalan atau usaha-usaha kecurangan, CODEPOLITAN berhak untuk:</h3>
                                <ul className="text-muted">
                                    <li>Menonaktifkan (sementara/permanen), atau menghapus semua akun CodePolitan yang terlibat atau dicurigai terlibat.</li>
                                    <li>Menghilangkan hak pengguna yang dimilikinya.</li>
                                    <li>Melakukan pencabutan hak seseorang untuk berpartisipasi pada program ini.</li>
                                    <li>Melakukan pencatatan dan pencegahan atas alamat email dan nomor telepon untuk aktivasi akun selanjutnya.</li>
                                    <li>Melakukan pencegahan atas sebuah detil identitas untuk berpartisipasi dan aktivasi akun selanjutnya.</li>
                                </ul>
                                <p className="text-muted mt-5">Aturan dan Syarat penggunaan ini dapat berubah sewaktu-waktu secara sepihak, dengan atau tanpa pemberitahuan terlebih dahulu.</p>
                            </div>
                        </div>
                    </div>
                </section>
            </Layout>
        </>
    );
};

export default TermsAndCondition;
