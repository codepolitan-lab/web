import Head from 'next/head';
import Link from 'next/link';
import Layout from '../../components/global/Layout/Layout';

const Comingsoon = () => {
    return (
        <>
            <Head>
                <title>Coming Soon - Codepolitan</title>
                <meta name="title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta name="description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="og:type" content="website" />
                <meta property="og:url" content="https://codepolitan.com" />
                <meta property="og:title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta property="og:description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="og:image" content="https://i.ibb.co/SXP8fGc/OG-image.jpg" />
                <meta property="twitter:card" content="summary_large_image" />
                <meta property="twitter:url" content="https://codepolitan.com" />
                <meta property="twitter:title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta property="twitter:description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="twitter:image" content="https://i.ibb.co/SXP8fGc/OG-image.jpg" />
            </Head>
            <Layout>
                <section className="section">
                    <div className="container p-4 p-lg-5">
                        <div className="row mt-5 pt-5">
                            <div className="col-md-6 my-auto">
                                <h1 className="section-title">Coming Soon</h1>
                                <p className="text-muted">We are still working on it. Please come back later 😉</p>
                                <Link href="/">
                                    <a className="btn btn-outline-primary rounded-pill">Back to Homepage</a>
                                </Link>
                            </div>
                            <div className="col-md-6">
                                <img className="img-fluid" src="https://i.ibb.co/svQ2yKN/undraw-Outer-space-re-u9vd.png" alt="Rocket Launch" />
                            </div>
                        </div>
                    </div>
                </section>
            </Layout>
        </>
    );
};

export default Comingsoon;