import { useState } from "react";
import axios from "axios";
import Head from "next/head";
import Hero from "../../components/program/Hero/Hero";
import Layout from "../../components/global/Layout/Layout";
import WaktuBelajarSection from "../../components/program/WaktuBelajarSection/WaktuBelajarSection";
import VideoSection from "../../components/program/VideoSection/VideoSection";
import CourselistSection from "../../components/program/CourselistSection/CourselistSection";
import ForumSection from "../../components/program/ForumSection/ForumSection";
import CertificateSection from "../../components/program/CertificateSection/CertificateSection";
import MentorSection from "../../components/program/MentorSection/MentorSection";
import HowToLearnSection from "../../components/program/HowToLearnSection/HowToLearnSection";
import TestimonySection from "../../components/program/TestimonySection/TestimonySection";
import CtaSection from "../../components/program/CtaSection/CtaSection";
import FaqSection from "../../components/program/FaqSection/FaqSection";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLock } from "@fortawesome/free-solid-svg-icons";
import { getLessons } from "../../utils/helper";

export const getServerSideProps = async () => {
    const courseDetailRes = await axios.get(`https://api.codepolitan.com/course/detail/membuat-game-tanpa-coding-dengan-construct-3`);
    const testimonyRes = await axios.get(`https://apps.codepolitan.com/api/feedback/course/membuat-game-tanpa-coding-dengan-construct-3`);

    const [courseDetail, testimony] = await Promise.all([
        courseDetailRes.data,
        testimonyRes.data
    ]);
    return {
        props: {
            courseDetail,
            testimony
        }
    }
}

const ConstructLanding = ({ courseDetail, testimony }) => {
    const lessons = courseDetail.lessons;

    const lessonList = getLessons(lessons);
    const lessonTopic = lessonList.lessonTopic;
    const lessonContent = lessonList.lessonContent;

    const [dataFaq] = useState([
        {
            id: 'flush-heading1',
            target: 'flush-collapse1',
            title: 'Alat apa saja yang diperlukan untuk belajar?',
            content: 'Anda akan menggunakan laptop atau komputer yang terhubung ke internet. Setelah mendaftar program Anda akan langsung mendapat akses kelas untuk mulai belajar. Silakan ikuti materi dan praktekkan studi kasus yang disampaikan oleh mentor di video pembelajaran tersebut.'
        },
    ]);

    return (
        <>
            <Head>
                <title>Construct 3 - Codepolitan</title>
                <meta name="title" content="Bikin Game 1 Jam ? Mudah dan Gak Harus Ngoding" />
                <meta name="description" content="Melalui kelas ini, bukan hanya jadi gamer kamu juga bisa jadi game developer yang dapat membuat game super keren dan memiliki nilai jual tanpa harus jago coding !" />

                <meta property="og:type" content="website" />
                <meta property="og:url" content="https://codepolitan.com/program/construct-3" />
                <meta property="og:title" content="Bikin Game 1 Jam ? Mudah dan Gak Harus Ngoding" />
                <meta property="og:description" content="Melalui kelas ini, bukan hanya jadi gamer kamu juga bisa jadi game developer yang dapat membuat game super keren dan memiliki nilai jual tanpa harus jago coding !" />
                <meta property="og:image" content="/assets/img/program/thumbnail/thumbnail-construct-3.png" />

                <meta property="twitter:card" content="summary_large_image" />
                <meta property="twitter:url" content="https://codepolitan.com/program/construct-3" />
                <meta property="twitter:title" content="Bikin Game 1 Jam ? Mudah dan Gak Harus Ngoding" />
                <meta property="twitter:description" content="Melalui kelas ini, bukan hanya jadi gamer kamu juga bisa jadi game developer yang dapat membuat game super keren dan memiliki nilai jual tanpa harus jago coding !" />
                <meta property="twitter:image" content="/assets/img/program/thumbnail/thumbnail-construct-3.png" />
            </Head>
            <Layout>
                <Hero
                    heroTitle="Bikin Game 1 Jam ? Mudah dan Gak Harus Ngoding"
                    heroSubtitle="Melalui kelas ini, bukan hanya jadi gamer kamu juga bisa jadi game developer yang dapat membuat game super keren dan memiliki nilai jual tanpa harus jago coding !"
                    heroImg="/assets/img/program/hero-construct-3.webp"
                    heroBgColor="bg-light"
                    heroFontColor="text-dark"
                    heroBtnColor="btn-outline-secondary"
                />
                <section id="about" className="section">
                    <div className="container p-5">
                        <div className="row justify-content-between">
                            <div className="col-lg-6 my-5">
                                <h2 className="section-title">Tentang Kelas</h2>
                                <p className="text-muted my-3">Kelas ditujukan bagi siapa saja yang ingin menjadi game developer dengan menggunakan tools construct 3. kamu bisa mendesign dan membuat game puzzle, Tembak-Tembakan, balapan , hingga RPG dan layak untuk dimainkan oleh siapapun, sehingga game yang kamu buat dapat diupload ke apps store atau playstore dengan nilai jual yang tinggi.</p>
                                <p className="text-muted">Tools yang digunakan:</p>
                                <img height="45" src="https://cursos.dankicode.com/app/Views/public/mkt/curso-construct-3/images/logo-hero.png" alt="" />
                            </div>
                            <div className="col-lg-5 my-3">
                                <img className="img-fluid d-block mx-auto" src="/assets/img/program/mascot.png" alt="Construct Codepolitan" />
                            </div>
                        </div>
                    </div>
                </section>
                <section className="section">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-md-6 bg-dark">
                                <div className="py-5">
                                    <img className="img-fluid" src="/assets/img/program/construct.png" alt="Mockup" />
                                </div>
                            </div>
                            <div className="col-md-6 p-5 bg-light">
                                <div className="py-lg-5">
                                    <h2 className="section-title">Belajar Dengan Studi Kasus</h2>
                                    <p className="text-muted my-3">Disini kami membuat game shooter yang bisa dimainkan pada perangkat mobile dan bisa diakses secara online, dengan mengikuti kelas ini kamu juga bisa bikin game shooter seperti kami atau bahkan lebih keren lagi. kamu bisa lihat hasilnya disini.</p>
                                    <a className="btn btn-primary btn-lg mt-3" href="http://zombierun.surge.sh/" target="_blank" rel="noopener noreferrer">Lihat demo</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <WaktuBelajarSection order="order-lg-last" />
                <VideoSection
                    videoSrc="https://www.youtube.com/embed/iZTVB-xHlgU"
                />
                <CourselistSection>
                    <ul
                        className="nav nav-pills d-grid d-md-flex nav-justified bg-light mb-3"
                        id="pills-tab"
                        role="tablist"
                    >
                        {lessonTopic.map((item, index) => {
                            return (
                                <li className="nav-item" role="presentation" key={index}>
                                    <button
                                        className={`nav-link py-3 ${index < 1 && 'active'}`}
                                        id={item.topic_slug.slice(3) + '-tab'}
                                        data-bs-toggle="pill"
                                        data-bs-target={'#' + item.topic_slug.slice(3)}
                                        type="button"
                                        role="tab"
                                        aria-controls={item.topic_slug.slice(3)}
                                        aria-selected="true"
                                        title={item.topic_title}
                                    >
                                        {item.topic_title.slice(0, 20) + '...'}
                                    </button>
                                </li>
                            );
                        })}
                    </ul>

                    <div className="tab-content" id="pills-tabContent">
                        {lessonContent.map((content, index) => {
                            return (
                                <div key={index} className={`tab-pane fade show ${index < 1 && 'active'}`} id={content.topic_slug.slice(3)} role="tabpanel" aria-labelledby={content.topic_slug.slice(3) + '-tab'}>
                                    <table className="table table-striped">
                                        <tbody>
                                            {content.contents?.map((item, index) => {
                                                return (
                                                    <tr key={index}>
                                                        <td style={{ width: '50px' }}>
                                                            <img height="30" src="/assets/img/program/play-icon.png" alt="Icon" />
                                                        </td>
                                                        <td className="text-muted">{item.lesson_title}</td>
                                                        <td className="text-end text-muted">
                                                            <FontAwesomeIcon icon={faLock} className="me-2" />
                                                            {item.duration}
                                                        </td>
                                                    </tr>
                                                );
                                            })}
                                        </tbody>
                                    </table>
                                </div>
                            );
                        })}
                    </div>
                </CourselistSection>
                <ForumSection />
                <CertificateSection />
                <MentorSection
                    name="Ahmad Hakim"
                    title="Mentor kelas Construct 3"
                    description="Saya Ahmad Hakim, seorang Full Stack Developer dan Senior Programmer di Codepolitan. Dengan pengalaman saya lebih dari 6 tahun berkarya dan  berkarir di bidang programming akan membantu kamu belajar dalam kelas ini. Apapun latar belakang kamu, melalui kelas ini kamu akan belajar membuat website perusahaan sendiri yang super keren. Saya tunggu di kelas!"
                    img="/assets/img/program/hakim.png"
                />
                <HowToLearnSection />
                <TestimonySection data={testimony} />
                <CtaSection
                    title="Tunggu Apalagi ? Ayo Gabung Kelas Bikin Web Tanpa Koding Sekarang !"
                    priceImg="/assets/img/program/price-construct-3.png"
                    actionLink="https://pay.codepolitan.com/?slug=membuat-game-tanpa-coding-dengan-construct-3"
                />
                <FaqSection data={dataFaq} />
            </Layout>
        </>
    )
}

export default ConstructLanding;
