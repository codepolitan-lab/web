import axios from 'axios';
import Head from 'next/head';
import Layout from '../../components/global/Layout/Layout';
import CertificateSection from '../../components/program/CertificateSection/CertificateSection';
import FaqSection from '../../components/program/FaqSection/FaqSection';
import ForumSection from '../../components/program/ForumSection/ForumSection';
import Hero from '../../components/program/Hero/Hero';
import HowToLearnSection from '../../components/program/HowToLearnSection/HowToLearnSection';
import StudycaseSection from '../../components/program/StudycaseSection/StudycaseSection';
import TestimonySection from '../../components/program/TestimonySection/TestimonySection';
import WaktuBelajarSection from '../../components/program/WaktuBelajarSection/WaktuBelajarSection';
import WarrantySection from '../../components/global/Sections/WarrantySection/WarrantySection';
import CountUp from 'react-countup';
import styles from '../../styles/Program.module.scss';
import CtaSection from '../../components/program/CtaSection/CtaSection';
import AboutSection from '../../components/program/AboutSection/AboutSection';

export const getServerSideProps = async () => {
    const testimonyRes = await axios.get(`https://apps.codepolitan.com/api/feedback/course/membuat-aplikasi-elearning-di-android-menggunakan-realtime-firebase-kotlin`);

    const [testimony] = await Promise.all([
        testimonyRes.data,
    ]);
    return {
        props: {
            testimony
        }
    }
}

const PathAndroidLanding = ({ testimony }) => {
    // console.log(testimony);
    const data = [
        {
            case_study: [
                {
                    id: 1,
                    thumbnail: 'https://i.ibb.co/GnLdypF/fullstack-1.png',
                    title: 'Aplikasi Management Tugas'
                },
                {
                    id: 2,
                    thumbnail: 'https://i.ibb.co/6yYcsdc/android-1.png',
                    title: 'Aplikasi E-Learning'
                },
                {
                    id: 3,
                    thumbnail: 'https://i.ibb.co/gvLcgLD/android-2.png',
                    title: 'Aplikasi Kuis'
                },
                {
                    id: 4,
                    thumbnail: 'https://i.ibb.co/jkqYBFF/android-3.png',
                    title: 'Aplikasi Musik Player'
                },
            ],
            courses: [
                {
                    id: 7,
                    thumbnail: 'https://i.ibb.co/7S3qM9j/icon-android.png',
                    title: 'Kelas Konsep',
                    description: 'Android Development Starter Pack',
                    number_module: '57'
                },
                {
                    id: 1,
                    thumbnail: 'https://i.ibb.co/7S3qM9j/icon-android.png',
                    title: 'Studi Kasus Android',
                    description: 'Membuat Aplikasi Presensi Android Berbasis Local',
                    number_module: '20'
                },
                {
                    id: 2,
                    thumbnail: 'https://i.ibb.co/7S3qM9j/icon-android.png',
                    title: 'Studi Kasus Android',
                    description: 'Implementasi Firebase Auth di Android',
                    number_module: '20'
                },
                {
                    id: 3,
                    thumbnail: 'https://i.ibb.co/7S3qM9j/icon-android.png',
                    title: 'Studi Kasus Android',
                    description: 'Implementasi ViewModel Pada Aplikasi Android',
                    number_module: '20'
                },
                {
                    id: 4,
                    thumbnail: 'https://i.ibb.co/7S3qM9j/icon-android.png',
                    title: 'Studi Kasus Android',
                    description: 'Memahami Cara Kerja Tim IT Perusahaan Yang WFH',
                    number_module: '19'
                },
                {
                    id: 5,
                    thumbnail: 'https://i.ibb.co/7S3qM9j/icon-android.png',
                    title: 'Studi Kasus Android',
                    description: 'Membuat Aplikasi Presensi Online Berbasis Web Dan Android',
                    number_module: '48'
                },
                {
                    id: 6,
                    thumbnail: 'https://i.ibb.co/7S3qM9j/icon-android.png',
                    title: 'Studi Kasus Android',
                    description: 'Membuat Infinite Scroll Time Seperti Instagram Di Android',
                    number_module: '14'
                },
                {
                    id: 8,
                    thumbnail: 'https://i.ibb.co/7S3qM9j/icon-android.png',
                    title: 'Studi Kasus Android',
                    description: 'Membuat Aplikasi Android Management Tugas',
                    number_module: '21'
                },
                {
                    id: 9,
                    thumbnail: 'https://i.ibb.co/7S3qM9j/icon-android.png',
                    title: 'Studi Kasus Android',
                    description: 'Membuat Aplikasi E-Learning di Android Dengan Realtime FIrebase',
                    number_module: '38'
                },
                {
                    id: 10,
                    thumbnail: 'https://i.ibb.co/7S3qM9j/icon-android.png',
                    title: 'Studi Kasus Android',
                    description: 'Membuat Aplikasi Quiz Dengan RecyclerView',
                    number_module: '21'
                },
                {
                    id: 11,
                    thumbnail: 'https://i.ibb.co/7S3qM9j/icon-android.png',
                    title: 'Studi Kasus Android',
                    description: 'Membuat Aplikasi Music Player di Android Dengan Kotln',
                    number_module: '59'
                },
            ],
            testimony: [
                {
                    name: 'Test',
                    comment: 'Lorem ipsum',
                    rate: 4
                },
                {
                    name: 'Test',
                    comment: 'Lorem ipsum',
                    rate: 4
                },
                {
                    name: 'Test',
                    comment: 'Lorem ipsum',
                    rate: 4
                },
                {
                    name: 'Test',
                    comment: 'Lorem ipsum',
                    rate: 4
                },
                {
                    name: 'Test',
                    comment: 'Lorem ipsum',
                    rate: 4
                },
            ],
            faq: [
                {
                    id: 'flush-heading1',
                    target: 'flush-collapse1',
                    title: 'Alat apa saja yang diperlukan untuk belajar?',
                    content: 'Anda akan menggunakan laptop atau komputer yang terinstall aplikasi Android Studio dan terhubung ke internet untuk menyimak materi. Akan lebih baik bila kamu juga menyiapkan smartphone Android untuk kebutuhan testing di device.'
                },
                {
                    id: 'flush-heading2',
                    target: 'flush-collapse2',
                    title: 'Saya baru di pemrograman, apakah saya dapat belajar membuat aplikasi Android?',
                    content: 'Kamu akan perlu mempelajari dulu dasar-dasar pemrograman dan bahasa pemrograman Java. Dari situ kamu bisa mulai belajar dari kelas konsep dasar Android Development Starter Pack dan lanjut ke kelas berikutnya sesuai urutan belajar yang telah kami siapkan.'
                },
                {
                    id: 'flush-heading3',
                    target: 'flush-collapse3',
                    title: 'Kalau ada materi yang membingungkan bagaimana solusinya?',
                    content: 'Anda dapat langsung bertanya pada bagian diskusi di halaman materi yang sedang Anda pelajari. Tanyakan bagian mana yang membuat Anda kurang paham. Mentor kami akan membantu menjawab persoalan Anda melalui kanal diskusi tersebut.'
                },
                {
                    id: 'flush-heading4',
                    target: 'flush-collapse4',
                    title: 'Bisakah saya bertanya langsung via tatap muka dengan mentor?',
                    content: 'Untuk saat ini belum. In syaa Allah kedepannya kami akan menyediakan layanan bimbingan privat untuk belajar pemrograman secara tatap muka online dan realtime bersama mentor.'
                },
            ]
        }
    ];

    return (
        <>
            <Head>
                <title>Path Android Developer - Codepolitan</title>
                <meta name="title" content="Raih Karir Android Developer Melalui Kelas Android Codepolitan" />
                <meta name="description" content="Apapun latar belakang kamu, melalui kelas online ini kamu akan belajar terarah dari nol hingga menjadi mahir pemrograman android dengan standar kompetensi kerja" />

                <meta property="og:type" content="website" />
                <meta property="og:url" content="https://codepolitan.com/program/path-android" />
                <meta property="og:title" content="Raih Karir Android Developer Melalui Kelas Android Codepolitan" />
                <meta property="og:description" content="Apapun latar belakang kamu, melalui kelas online ini kamu akan belajar terarah dari nol hingga menjadi mahir pemrograman android dengan standar kompetensi kerja" />
                <meta property="og:image" content="/assets/img/program/hero-path-android.webp" />

                <meta property="twitter:card" content="summary_large_image" />
                <meta property="twitter:url" content="https://codepolitan.com/program/path-android" />
                <meta property="twitter:title" content="Raih Karir Android Developer Melalui Kelas Android Codepolitan" />
                <meta property="twitter:description" content="Apapun latar belakang kamu, melalui kelas online ini kamu akan belajar terarah dari nol hingga menjadi mahir pemrograman android dengan standar kompetensi kerja" />
                <meta property="twitter:image" content="/assets/img/program/hero-path-android.webp" />
            </Head>
            <Layout>
                <Hero
                    heroTitle={<span>Raih Karir <span className="text-primary">Android Developer</span> Melalui Kelas Android Codepolitan</span>}
                    heroSubtitle="Apapun latar belakang kamu, melalui kelas online ini kamu akan belajar terarah dari nol hingga menjadi mahir pemrograman android dengan standar kompetensi kerja"
                    heroImg="/assets/img/program/hero-path-android.webp"
                    heroBgColor="bg-light"
                    heroFontColor="text-dark"
                    heroBtnColor="btn-outline-secondary"
                />
                <AboutSection
                    thumbnail="/assets/img/program/android-img.png"
                    title="Android App Development"
                    description="Dalam kelas ini kamu akan belajar dari awal hingga menjadi mahir pemrograman Android. melalui kelas ini, kamu yang belum mahir sekalipun dalam bidang pemrograman akan bisa membuat aplikasi Android yang super keren dan dapat bersaing di dunia industri untuk mendapatkan karir Impianmu"
                />
                <StudycaseSection data={data[0].case_study} />
                <section className="section" style={{ backgroundColor: '#eee' }}>
                    <div className="container p-4 p-lg-5">
                        <div className="row">
                            <div className="col text-center">
                                <h3 className="section-title">Apa yang Akan Kamu Pelajari</h3>
                                <p className="text-muted">Yang akan kamu pelajari dalam kelas ini</p>
                            </div>
                        </div>
                        <div className="row mt-3">
                            {data[0].courses.map((item, index) => {
                                return (
                                    <div className="col-md-6 col-lg-4 col-xl-3 mb-4" key={index}>
                                        <div className="card border-0 shadow-sm" style={{ borderRadius: '15px', height: '100%' }}>
                                            <div className="card-header bg-white border-0" style={{ borderRadius: '15px' }}>
                                                <div className="row mt-2">
                                                    <div className="col-8">
                                                        <img height="50" src={item.thumbnail} alt={item.title} />
                                                    </div>
                                                    <div className="col-4 text-center">
                                                        <span className={styles.card_label_materi}>{index + 1}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="card-body text-muted pb-0">
                                                <h6 style={{ fontSize: 'smaller' }}>{item.title}</h6>
                                                <p><strong>{item.description}</strong></p>
                                            </div>
                                            <div className="row mb-3">
                                                <div className="col text-end">
                                                    <span className="badge bg-codepolitan px-3" style={{ borderRadius: 0, borderTopLeftRadius: '25px', borderBottomLeftRadius: '25px' }}>{item.number_module} modul</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                </section>
                <section className="section bg-light">
                    <div className="container p-4 p-lg-5">
                        <div className="row justify-content-center text-center">
                            <div className="col-6 col-md-4 my-2">
                                <p className="text-secondary mb-0"><span className={`${styles.count} text-secondary`}><CountUp end={11} duration={5} /></span> Kelas Android</p>
                            </div>
                            <div className="col-6 col-md-4 my-2">
                                <p className="text-secondary mb-0"><span className={`${styles.count} text-secondary`}><CountUp end={140191} duration={5} /></span> Member Aktif</p>
                            </div>
                            <div className="col-6 col-md-4 my-2">
                                <p className="text-secondary mb-0"><span className={`${styles.count} text-secondary`}><CountUp end={367} duration={5} /></span> Modul Belajar</p>
                            </div>
                        </div>
                    </div>
                </section>
                <WaktuBelajarSection />
                <ForumSection />
                <section className="section">
                    <div className="container p-4 p-lg-5">
                        <div className="row">
                            <div className="col-lg-5 offset-lg-1 my-auto">
                                <h2 className="section-title">Online Mentoring</h2>
                                <p className="text-muted my-3">Dapatkan kesempatan 12x berdiskusi dengan mentor kelas fullstack developer dalam sesi mentoring sebanyak 2x dalam 1 bulan, supaya kamu dapat belajar dengan terarah.</p>
                            </div>
                            <div className="col-lg-6 order-lg-first">
                                <img className="img-fluid" src="/assets/img/program/discord-img-3.png" alt="Online Mentoring" />
                            </div>
                        </div>
                    </div>
                </section>
                <CertificateSection />
                <section className="section bg-light">
                    <div className="container text-center p-4 p-lg-5">
                        <div className="row my-3">
                            <div className="col">
                                <h2 className="section-title">Mentor Belajar</h2>
                                <p className="text-muted">Dalam program ini kamu akan dibimbing oleh para mentor profesional yang berpengalaman</p>
                            </div>
                        </div>
                        <div className="row justify-content-center">
                            <div className="col-6 col-lg-3 mb-3">
                                <img className="img-fluid" src="/assets/img/program/mentor/aulia-rahman.png" alt="Aulia Rahman" />
                                <h5 className="mt-4">Aulia Rahman</h5>
                                <p className="text-muted">Android Developer</p>
                            </div>
                            <div className="col-6 col-lg-3 mb-3">
                                <img className="img-fluid" src="/assets/img/program/mentor/muhammad-singgih.png" alt="Muhammad Singgih Z.A" />
                                <h5 className="mt-4">Muhammad Singgih Z.A</h5>
                                <p className="text-muted">CMO at Codepolitan</p>
                            </div>
                            <div className="col-6 col-lg-3 mb-3">
                                <img className="img-fluid" src="/assets/img/program/mentor/hakim-sembiring.png" alt="Hakim Sembiring" />
                                <h5 className="mt-4">Hakim Sembiring</h5>
                                <p className="text-muted">Fullstack Developer</p>
                            </div>
                        </div>
                    </div>
                </section>
                <HowToLearnSection />
                <TestimonySection data={testimony} />
                <CtaSection
                    title="Tunggu Apalagi ? Ayo Gabung Program Belajar Android App Development Selama 6 Bulan!"
                    priceImg="/assets/img/program/price-android.png"
                    actionLink="https://pay.codepolitan.com/?slug=program-android-app-development"
                />
                <div className="bg-light">
                    <WarrantySection />
                </div>
                <FaqSection data={data[0].faq} />
            </Layout>
        </>
    );
};

export default PathAndroidLanding;
