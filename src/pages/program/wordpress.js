import { useState } from "react";
import axios from "axios";
import Head from "next/head";
import Hero from "../../components/program/Hero/Hero";
import AboutSection from '../../components/program/AboutSection/AboutSection';
import Layout from "../../components/global/Layout/Layout";
import WaktuBelajarSection from "../../components/program/WaktuBelajarSection/WaktuBelajarSection";
import VideoSection from '../../components/program/VideoSection/VideoSection';
import CourselistSection from "../../components/program/CourselistSection/CourselistSection";
import ForumSection from "../../components/program/ForumSection/ForumSection";
import CertificateSection from "../../components/program/CertificateSection/CertificateSection";
import MentorSection from "../../components/program/MentorSection/MentorSection";
import HowToLearnSection from "../../components/program/HowToLearnSection/HowToLearnSection";
import TestimonySection from "../../components/program/TestimonySection/TestimonySection";
import CtaSection from "../../components/program/CtaSection/CtaSection";
import FaqSection from "../../components/program/FaqSection/FaqSection";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLock } from "@fortawesome/free-solid-svg-icons";
import { getLessons } from "../../utils/helper";

export const getServerSideProps = async () => {
    const courseDetailRes = await axios.get(`https://api.codepolitan.com/course/detail/cara-mudah-buat-toko-online-sendiri-dengan-wordpress`);
    const testimonyRes = await axios.get(`https://apps.codepolitan.com/api/feedback/course/cara-mudah-buat-toko-online-sendiri-dengan-wordpress`);

    const [courseDetail, testimony] = await Promise.all([
        courseDetailRes.data,
        testimonyRes.data
    ]);
    return {
        props: {
            courseDetail,
            testimony
        }
    }
}

const WordpressLanding = ({ courseDetail, testimony }) => {
    const lessons = courseDetail.lessons;

    const lessonList = getLessons(lessons);
    const lessonTopic = lessonList.lessonTopic;
    const lessonContent = lessonList.lessonContent;

    const [dataFaq] = useState([
        {
            id: 'flush-heading1',
            target: 'flush-collapse1',
            title: 'Apa saya tidak perlu punya skill ngoding untuk mengikuti kelas ini?',
            content: 'Ya. Kelas ini didesain untuk dapat diikuti oleh selain programmer. Semua teknik yang dipelajari tidak memerlukan skill ngoding sama sekali.'
        },
        {
            id: 'flush-heading2',
            target: 'flush-collapse2',
            title: 'Apakah saya dapat membuat toko online yang keren setelah mengikuti kelas ini?',
            content: 'Kami telah menjelaskan teknik-teknik dasar untuk membangun website toko online menggunakan Wordpress, termasuk teknik membuat landing page yang paling mudah dengan menggunakan page builder. Sisanya kembali kepada imajinasi dan kreatifitasmu untuk meramu semua resep tersebut menjadi sebuah website yang keren.'
        },
        {
            id: 'flush-heading3',
            target: 'flush-collapse3',
            title: 'Kapan pertemuan materi disampaikan oleh mentor?',
            content: 'Mentor kami telah membuat rangkaian video yang berisi pembahasan materi. Artinya Kamu dapat mempelajari kapanpun video tersebut tanpa batas waktu.'
        },
        {
            id: 'flush-heading4',
            target: 'flush-collapse4',
            title: 'Alat apa saja yang diperlukan untuk belajar?',
            content: 'Kamu akan menggunakan laptop atau komputer yang terhubung ke internet. Setelah mendaftar program Kamu akan langsung mendapat akses kelas untuk mulai belajar. Silakan ikuti materi dan praktekkan studi kasus yang disampaikan oleh mentor di video pembelajaran tersebut. Kamu juga akan perlu menyewa server dan domain untuk mengonlinekan websitemu nantinya.'
        },
        {
            id: 'flush-heading5',
            target: 'flush-collapse5',
            title: 'Kalau ada materi yang membingungkan bagaimana solusinya?',
            content: 'Kamu dapat langsung bertanya pada bagian diskusi di halaman materi yang sedang Kamu pelajari. Tanyakan bagian mana yang membuat Kamu kurang paham. Mentor kami akan membantu menjawab persoalan Kamu melalui kanal diskusi tersebut.'
        },
        {
            id: 'flush-heading6',
            target: 'flush-collapse6',
            title: 'Bisakah saya bertanya langsung via tatap muka dengan mentor?',
            content: 'ntuk saat ini belum. In syaa Allah kedepannya kami akan menyediakan layanan bimbingan privat untuk belajar pemrograman secara tatap muka online dan realtime bersama mentor.'
        },
    ]);

    return (
        <>
            <Head>
                <title>Wordpress - Codepolitan</title>
                <meta name="title" content="Mau Bikin Toko Online Sendiri Dengan Mudah ?" />
                <meta name="description" content="Bikin web toko online itu ternyata ga susah!  cukup ikuti kelas ini apapun latar belakang kamu, kamu dapat membuat web toko online sendiri yang memiliki nilai jual" />

                <meta property="og:type" content="website" />
                <meta property="og:url" content="https://codepolitan.com/program/wordpress" />
                <meta property="og:title" content="Mau Bikin Toko Online Sendiri Dengan Mudah ?" />
                <meta property="og:description" content="Bikin web toko online itu ternyata ga susah!  cukup ikuti kelas ini apapun latar belakang kamu, kamu dapat membuat web toko online sendiri yang memiliki nilai jual" />
                <meta property="og:image" content="/assets/img/program/thumbnail/thumbnail-wordpress.png" />

                <meta property="twitter:card" content="summary_large_image" />
                <meta property="twitter:url" content="https://codepolitan.com/program/wordpress" />
                <meta property="twitter:title" content="Mau Bikin Toko Online Sendiri Dengan Mudah ?" />
                <meta property="twitter:description" content="Bikin web toko online itu ternyata ga susah!  cukup ikuti kelas ini apapun latar belakang kamu, kamu dapat membuat web toko online sendiri yang memiliki nilai jual" />
                <meta property="twitter:image" content="/assets/img/program/thumbnail/thumbnail-wordpress.png" />
            </Head>
            <Layout>
                <Hero
                    heroTitle="Mau Bikin Toko Online Sendiri Dengan Mudah ?"
                    heroSubtitle="Bikin web toko online itu ternyata ga susah!  cukup ikuti kelas ini apapun latar belakang kamu, kamu dapat membuat web toko online sendiri yang memiliki nilai jual"
                    heroImg="/assets/img/program/hero-wordpress.webp"
                    heroBgColor="bg-light"
                    heroFontColor="text-dark"
                    heroBtnColor="btn-outline-light"
                />
                <AboutSection
                    thumbnail="/assets/img/program/wordpress-img.png"
                    title="Wordpress Development"
                    description="Dalam kelas ini kamu akan belajar dari awal hingga menjadi mahir membuat toko online dengan Wordpress. Melalui kelas ini, kamu yang belum mahir sekalipun dalam bidang pemrograman akan bisa membuat toko online yang super keren dan dapat diakses langsung."
                />
                <section className="section bg-light">
                    <div className="container p-4 p-lg-5">
                        <div className="row">
                            <div className="col-lg-6 mb-5 mb-lg-0">
                                <img className="img-fluid" src="/assets/img/program/wordpress.png" alt="Mockup" />
                            </div>
                            <div className="col-lg-6 ps-lg-5 my-auto">
                                <h2 className="section-title">Belajar Dengan Studi Kasus</h2>
                                <p className="text-muted my-3">Disini kami telah membuat toko online menggunakan Wordpres dengan cepat. dengan mengikuti kelas ini kamu akan Belajar studi kasus agar dapat membuat web seperti toko online atau bahkan lebih keren lagi.</p>
                                <a className="btn btn-primary btn-lg mt-3" href="">Lihat demo</a>
                            </div>
                        </div>
                    </div>
                </section>
                <WaktuBelajarSection order="order-lg-last" />
                <VideoSection
                    videoSrc="https://www.youtube.com/embed/msjSyFvDPTA"
                    description="Kamu akan mendapatkan materi berupa vidio yang dapat diakses selamanya tanpa batas waktu. dengan begitu kamu dapat cepat mahir membuat game dengan construct."
                    link="/"
                />
                <CourselistSection>
                    <ul
                        className="nav nav-pills d-grid d-md-flex nav-justified bg-light mb-3"
                        id="pills-tab"
                        role="tablist"
                    >
                        {lessonTopic.map((item, index) => {
                            return (
                                <li className="nav-item" role="presentation" key={index}>
                                    <button
                                        className={`nav-link py-3 ${index < 1 && 'active'}`}
                                        id={item.topic_slug.slice(3) + '-tab'}
                                        data-bs-toggle="pill"
                                        data-bs-target={'#' + item.topic_slug.slice(3)}
                                        type="button"
                                        role="tab"
                                        aria-controls={item.topic_slug.slice(3)}
                                        aria-selected="true"
                                        title={item.topic_title}
                                    >
                                        {item.topic_title.length > 20 ? item.topic_title.slice(0, 20) + '...' : item.topic_title}
                                    </button>
                                </li>
                            );
                        })}
                    </ul>

                    <div className="tab-content" id="pills-tabContent">
                        {lessonContent.map((content, index) => {
                            return (
                                <div key={index} className={`tab-pane fade show ${index < 1 && 'active'}`} id={content.topic_slug.slice(3)} role="tabpanel" aria-labelledby={content.topic_slug.slice(3) + '-tab'}>
                                    <table className="table table-striped">
                                        <tbody>
                                            {content.contents?.map((item, index) => {
                                                return (
                                                    <tr key={index}>
                                                        <td style={{ width: '50px' }}>
                                                            <img height="30" src="/assets/img/program/play-icon.png" alt="Icon" />
                                                        </td>
                                                        <td className="text-muted">{item.lesson_title}</td>
                                                        <td className="text-end text-muted">
                                                            <FontAwesomeIcon icon={faLock} className="me-2" />
                                                            {item.duration || '15:00'}
                                                        </td>
                                                    </tr>
                                                );
                                            })}
                                        </tbody>
                                    </table>
                                </div>
                            );
                        })}
                    </div>
                </CourselistSection>
                <ForumSection />
                <CertificateSection />
                <MentorSection
                    name="Tony Haryanto"
                    title="Mentor kelas Wordpress"
                    description="Saya Tony Haryanto, seorang Full Stack Developer dan Senior Programmer di Codepolitan. Dengan pengalaman saya lebih dari 6 tahun berkarya dan  berkarir di bidang programming akan membantu kamu belajar dalam kelas ini. Apapun latar belakang kamu, melalui kelas ini kamu akan belajar membuat website perusahaan sendiri yang super keren. Saya tunggu di kelas!"
                    img="/assets/img/program/tony-haryanto.webp"
                />
                <HowToLearnSection />
                <TestimonySection data={testimony} />
                <CtaSection
                    title="Tunggu Apalagi ? Ayo Gabung Kelas Bikin Web Tanpa Koding Sekarang !"
                    priceImg="/assets/img/program/price-wordpress.png"
                    actionLink="https://pay.codepolitan.com/?slug=cara-mudah-buat-toko-online-sendiri-dengan-wordpress"
                />
                <FaqSection data={dataFaq} />
            </Layout>
        </>
    );
};

export default WordpressLanding;
