import axios from 'axios';
import Head from 'next/head';
import Layout from '../../components/global/Layout/Layout';
import CertificateSection from '../../components/program/CertificateSection/CertificateSection';
import CtaSection from '../../components/program/CtaSection/CtaSection';
import FaqSection from '../../components/program/FaqSection/FaqSection';
import ForumSection from '../../components/program/ForumSection/ForumSection';
import Hero from '../../components/program/Hero/Hero';
import HowToLearnSection from '../../components/program/HowToLearnSection/HowToLearnSection';
import StudycaseSection from '../../components/program/StudycaseSection/StudycaseSection';
import TestimonySection from '../../components/program/TestimonySection/TestimonySection';
import WaktuBelajarSection from '../../components/program/WaktuBelajarSection/WaktuBelajarSection';
import WarrantySection from '../../components/global/Sections/WarrantySection/WarrantySection';
import CountUp from 'react-countup';
import styles from '../../styles/Program.module.scss';
import AboutSection from '../../components/program/AboutSection/AboutSection';

export const getServerSideProps = async () => {
    const testimonyRes = await axios.get(`https://apps.codepolitan.com/api/feedback/course/laravel-8x-fundamental`);

    const [testimony] = await Promise.all([
        testimonyRes.data,
    ]);
    return {
        props: {
            testimony
        }
    }
}

const FullstackLanding = ({ testimony }) => {
    const data = [
        {
            case_study: [
                {
                    id: 1,
                    thumbnail: 'https://i.ibb.co/GnLdypF/fullstack-1.png',
                    title: 'Aplikasi Todo Management'
                },
                {
                    id: 2,
                    thumbnail: 'https://i.ibb.co/7yTMGVh/fullstack-2.png',
                    title: 'Website Blog Artikel'
                },
                {
                    id: 3,
                    thumbnail: 'https://i.ibb.co/TwPtCj4/fullstack-3.png',
                    title: 'Website Toko Online'
                },
                {
                    id: 4,
                    thumbnail: 'https://i.ibb.co/XCt04nV/fullstack-4.png',
                    title: 'Sistem Perpustakaan'
                },
            ],
            technology: [
                {
                    id: 1,
                    thumbnail: '/assets/img/program/icons/box-icons/icon-html5.png',
                    title: 'HTML & CSS',
                    description: 'Belajar dari dasar hingga mahir',
                },
                {
                    id: 2,
                    thumbnail: '/assets/img/program/icons/box-icons/icon-bootstrap.png',
                    title: 'Bootstrap',
                    description: 'Belajar dari dasar hingga mahir',
                },
                {
                    id: 3,
                    thumbnail: '/assets/img/program/icons/box-icons/icon-mysql.png',
                    title: 'MySQL',
                    description: 'Belajar dari dasar hingga mahir',
                },
                {
                    id: 4,
                    thumbnail: '/assets/img/program/icons/box-icons/icon-php.png',
                    title: 'PHP',
                    description: 'Belajar dari dasar hingga mahir',
                },
                {
                    id: 5,
                    thumbnail: '/assets/img/program/icons/box-icons/icon-codeigniter.png',
                    title: 'Codeigniter',
                    description: 'Belajar dari dasar hingga mahir',
                },
                {
                    id: 6,
                    thumbnail: '/assets/img/program/icons/box-icons/icon-laravel.png',
                    title: 'Laravel',
                    description: 'Belajar dari dasar hingga mahir',
                },
                {
                    id: 9,
                    thumbnail: '/assets/img/program/icons/box-icons/icon-jquery.png',
                    title: 'JQuery',
                    description: 'Belajar dari dasar hingga mahir',
                },
                {
                    id: 10,
                    thumbnail: '/assets/img/program/icons/box-icons/icon-js.png',
                    title: 'JavaScript',
                    description: 'Belajar dari dasar hingga mahir',
                },
                {
                    id: 12,
                    thumbnail: '/assets/img/program/icons/box-icons/icon-vue.png',
                    title: 'Vue',
                    description: 'Belajar dari dasar hingga mahir',
                },
            ],
            faq: [
                {
                    id: 'flush-heading1',
                    target: 'flush-collapse1',
                    title: 'Apa yang dimaksud dengan fullstack web development?',
                    content: 'Fullstack web development berarti pengembangan aplikasi web yang mencakup pemrograman sisi server dan sisi client. Artinya Kamu akan belajar mulai dari dasar logika pemrograman untuk dijalankan di sisi server, managemen database, sampai penampilan antarmuka aplikasi yang berjalan di browser pengguna.'
                },
                {
                    id: 'flush-heading2',
                    target: 'flush-collapse2',
                    title: 'Bahasa pemrograman apa saja yang dipelajari?',
                    content: 'Untuk pemrograman sisi server Kamu akan belajar bahasa pemrograman PHP. Untuk pemrograman di sisi client Kamu akan belajar bahasa pemrograman JavaScript, dan juga HTML CSS. Kamu juga akan belajar dasar-dasar MySQL untuk managemen database.'
                },
                {
                    id: 'flush-heading3',
                    target: 'flush-collapse3',
                    title: 'Framework PHP apa yang digunakan dalam pembelajaran?',
                    content: 'Kami membahas mulai dari PHP native dan juga framework Laravel versi 8 dan CodeIgniter versi 3. Di sisi client kami membahas dasar-dasar javascript, library jQuery, dan juga framework VueJS.'
                },
                {
                    id: 'flush-heading4',
                    target: 'flush-collapse4',
                    title: 'Apakah saya dapat menjadi fullstack programmer dalam waktu 6 bulan?',
                    content: 'Kamu harus mengikuti semua materi teori yang disajikan agar mendapatkan konsep dan fundamental pemrograman. Kamu juga harus mencoba mempraktekkan setiap kelas studi kasus untuk mendapatkan *best-practice* di dunia pemrograman. Dengan demikian in syaa Allah Kamu akan menjadi fullstack web programmer dalam waktu 4-6 bulan bahkan lebih cepat tergantung kecepatan belajarmu.'
                },
                {
                    id: 'flush-heading5',
                    target: 'flush-collapse5',
                    title: 'Kapan pertemuan materi disampaikan oleh mentor?',
                    content: 'Mentor kami telah membuat rangkaian video yang berisi pembahasan materi. Artinya Anda dapat mempelajari kapanpun video tersebut tanpa batas waktu.'
                },
                {
                    id: 'flush-heading6',
                    target: 'flush-collapse6',
                    title: 'Alat apa saja yang diperlukan untuk belajar?',
                    content: 'Anda akan menggunakan laptop atau komputer yang terhubung ke internet. Setelah mendaftar program Anda akan langsung mendapat akses kelas untuk mulai belajar. Silakan ikuti materi dan praktekkan studi kasus yang disampaikan oleh mentor di video pembelajaran tersebut.'
                },
                {
                    id: 'flush-heading7',
                    target: 'flush-collapse7',
                    title: 'Kalau ada materi yang membingungkan bagaimana solusinya?',
                    content: 'Anda dapat langsung bertanya pada bagian diskusi di halaman materi yang sedang Anda pelajari. Tanyakan bagian mana yang membuat Anda kurang paham. Mentor kami akan membantu menjawab persoalan Anda melalui kanal diskusi tersebut.'
                },
                {
                    id: 'flush-heading8',
                    target: 'flush-collapse8',
                    title: 'Bisakah saya bertanya langsung via tatap muka dengan mentor?',
                    content: 'Untuk saat ini belum. In syaa Allah kedepannya kami akan menyediakan layanan bimbingan privat untuk belajar pemrograman secara tatap muka online dan realtime bersama mentor.'
                },
            ]
        }
    ];

    return (
        <>
            <Head>
                <title>Fullstack Developer - Codepolitan</title>
                <meta name="title" content="Bingung Cari Kelas Online Fullstack Developer ?" />
                <meta name="description" content="Apapun latar belakang kamu, melalui kelas online ini kamu akan belajar terarah dari nol hingga menjadi professional fullstack developer bisa meraih Impianmu menjadi fullstack developer dalam 6 bulan" />

                <meta property="og:type" content="website" />
                <meta property="og:url" content="https://codepolitan.com/program/fullstack-developer" />
                <meta property="og:title" content="Bingung Cari Kelas Online Fullstack Developer ?" />
                <meta property="og:description" content="Apapun latar belakang kamu, melalui kelas online ini kamu akan belajar terarah dari nol hingga menjadi professional fullstack developer bisa meraih Impianmu menjadi fullstack developer dalam 6 bulan" />
                <meta property="og:image" content="/assets/img/program/hero-fullstack.png" />

                <meta property="twitter:card" content="summary_large_image" />
                <meta property="twitter:url" content="https://codepolitan.com/program/fullstack-developer" />
                <meta property="twitter:title" content="Bingung Cari Kelas Online Fullstack Developer ?" />
                <meta property="twitter:description" content="Apapun latar belakang kamu, melalui kelas online ini kamu akan belajar terarah dari nol hingga menjadi professional fullstack developer bisa meraih Impianmu menjadi fullstack developer dalam 6 bulan" />
                <meta property="twitter:image" content="/assets/img/program/hero-fullstack.png" />
            </Head>
            <Layout>
                <Hero
                    heroTitle="Bingung Cari Kelas Online Fullstack Developer ?"
                    heroSubtitle="Apapun latar belakang kamu, melalui kelas online ini kamu akan belajar terarah dari nol hingga menjadi professional fullstack developer bisa meraih Impianmu menjadi fullstack developer dalam 6 bulan"
                    heroImg="/assets/img/program/hero-fullstack.png"
                    heroBgColor="bg-light"
                    heroFontColor="text-dark"
                    heroBtnColor="btn-outline-secondary"
                />
                <AboutSection
                    thumbnail="/assets/img/program/fullstack-img.png"
                    title="Fullstack Web Development"
                    description="Dalam kelas ini kamu akan belajar dari awal hingga menjadi professional fullstack developer selama 1 semester. Melalui kelas ini, kamu yang belum mahir sekalipun dalam bidang pemrograman akan bisa membuat aplikasi berbasis web yang super keren dan memiliki nilai jual yang tinggi loh."
                />
                <StudycaseSection data={data[0].case_study} />
                <section className="section" style={{ backgroundColor: '#eee' }}>
                    <div className="container p-4 p-lg-5">
                        <div className="row">
                            <div className="col text-center">
                                <h3 className="section-title">Apa yang Akan Kamu Pelajari</h3>
                                <p className="text-muted">Yang akan kamu pelajari dalam kelas ini</p>
                            </div>
                        </div>
                        <div className="row mt-3">
                            {data[0].technology.map((item) => {
                                return (
                                    <div className="col-md-4 mb-3" key={item.id}>
                                        <div className="card border-0 shadow-sm" style={{ borderRadius: '15px' }}>
                                            <div className="card-body p-3">
                                                <div className="row text-muted">
                                                    <div className="col-4 my-auto">
                                                        <img className="img-fluid rounded" src={item.thumbnail} alt={item.title} />
                                                    </div>
                                                    <div className="col-8 mt-2">
                                                        <h5 style={{ fontSize: 'medium' }}>{item.title}</h5>
                                                        <p style={{ fontSize: 'small' }}>{item.description}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                </section>
                <section className="section bg-codepolitan text-white">
                    <div className="container p-4 p-lg-5">
                        <div className="row justify-content-center text-center">
                            <div className="col-6 col-md-4 my-2">
                                <p className="text-white mb-0"><span className={`${styles.count} text-white`}><CountUp end={64} duration={5} /></span> Kelas Online</p>
                            </div>
                            <div className="col-6 col-md-4 my-2">
                                <p className="text-white mb-0"><span className={`${styles.count} text-white`}><CountUp end={140191} duration={5} /></span> Member Aktif</p>
                            </div>
                            <div className="col-6 col-md-4 my-2">
                                <p className="text-white mb-0"><span className={`${styles.count} text-white`}><CountUp end={1820} duration={5} /></span> Materi Belajar</p>
                            </div>
                        </div>
                    </div>
                </section>
                <WaktuBelajarSection />
                <ForumSection />
                <section className="section">
                    <div className="container p-4 p-lg-5">
                        <div className="row">
                            <div className="col-lg-5 offset-lg-1 my-auto">
                                <h2 className="section-title">Online Mentoring</h2>
                                <p className="text-muted my-3">Dapatkan kesempatan 12x berdiskusi dengan mentor kelas fullstack developer dalam sesi mentoring sebanyak 2x dalam 1 bulan, supaya kamu dapat belajar dengan terarah.</p>
                            </div>
                            <div className="col-lg-6 order-lg-first">
                                <img className="img-fluid" src="/assets/img/program/discord-img-3.png" alt="Online Mentoring" />
                            </div>
                        </div>
                    </div>
                </section>
                <CertificateSection />
                <section className="section bg-light">
                    <div className="container text-center p-4 p-lg-5">
                        <div className="row my-3">
                            <div className="col">
                                <h2 className="section-title">Mentor Belajar</h2>
                                <p className="text-muted">Dalam program ini kamu akan dibimbing oleh para mentor profesional yang berpengalaman</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-6 col-lg-3 mb-3">
                                <img className="img-fluid" src="/assets/img/program/mentor/ahmad-oriza.png" alt="Ahmad Oriza" />
                                <h5 className="mt-4">Ahmad Oriza</h5>
                                <p className="text-muted">Fullstack Developer</p>
                            </div>
                            <div className="col-6 col-lg-3 mb-3">
                                <img className="img-fluid" src="/assets/img/program/mentor/galih-pratama.png" alt="Galih Pratama" />
                                <h5 className="mt-4">Galih Pratama</h5>
                                <p className="text-muted">Fullstack Developer</p>
                            </div>
                            <div className="col-6 col-lg-3 mb-3">
                                <img className="img-fluid" src="/assets/img/program/mentor/hakim-sembiring.png" alt="Hakim Sembiring" />
                                <h5 className="mt-4">Hakim Sembiring</h5>
                                <p className="text-muted">Fullstack Developer</p>
                            </div>
                            <div className="col-6 col-lg-3 mb-3">
                                <img className="img-fluid" src="/assets/img/program/mentor/tony-haryanto.png" alt="Tony Haryanto" />
                                <h5 className="mt-4">Tony Haryanto</h5>
                                <p className="text-muted">Fullstack Developer</p>
                            </div>
                        </div>
                    </div>
                </section>
                <HowToLearnSection />
                <TestimonySection data={testimony} />
                <CtaSection
                    title="Tunggu Apalagi ? Ayo Gabung Program Belajar Fullstack Developer 6 Bulan Untuk Meraih Karirmu !"
                    priceImg="/assets/img/program/price-fullstack.png"
                    actionLink="https://pay.codepolitan.com/?slug=program-fullstack-web-development"
                />
                <div className="bg-light">
                    <WarrantySection />
                </div>
                <FaqSection data={data[0].faq} />
            </Layout>
        </>
    );
};

export default FullstackLanding;
