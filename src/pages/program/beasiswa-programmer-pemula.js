import styles from '../../styles/Program.module.scss';
import Head from 'next/head';
import Layout from '../../components/global/Layout/Layout';
import CountUp from 'react-countup';
import HeroV2 from '../../components/program/Hero/HeroV2';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons';
import ForumSection from '../../components/program/ForumSection/ForumSection';
import CertificateSection from '../../components/program/CertificateSection/CertificateSection';
import WaktuBelajarSection from '../../components/program/WaktuBelajarSection/WaktuBelajarSection';
import CtaSectionV2 from '../../components/program/CtaSection/CtaSectionV2';

const ProgramBeasiswaLanding = () => {
    return (
        <>
            <Head>
                <title>Dapatkan Beasiswa Gratis Untuk Coder Pemula Sekarang! - Codepolitan</title>
                <meta name="title" content="Dapatkan Beasiswa Gratis Untuk Coder Pemula Sekarang!" />
                <meta name="description" content="Belajar coding untuk pemula ga harus susah! cukup dapatkan beasiswa ini, kamu dapat mengakses materi coding dasar yang terstruktur secara gratis" />

                <meta property="og:type" content="website" />
                <meta property="og:url" content="https://codepolitan.com/program/beasiswa-gratis" />
                <meta property="og:title" content="Dapatkan Beasiswa Gratis Untuk Coder Pemula Sekarang!" />
                <meta property="og:description" content="Belajar coding untuk pemula ga harus susah! cukup dapatkan beasiswa ini, kamu dapat mengakses materi coding dasar yang terstruktur secara gratis" />
                <meta property="og:image" content="/assets/img/program/thumbnail/thumbnail-beasiswa-gratis.png" />

                <meta property="twitter:card" content="summary_large_image" />
                <meta property="twitter:url" content="https://codepolitan.com/program/beasiswa-gratis" />
                <meta property="twitter:title" content="Dapatkan Beasiswa Gratis Untuk Coder Pemula Sekarang!" />
                <meta property="twitter:description" content="Belajar coding untuk pemula ga harus susah! cukup dapatkan beasiswa ini, kamu dapat mengakses materi coding dasar yang terstruktur secara gratis" />
                <meta property="twitter:image" content="/assets/img/program/thumbnail/thumbnail-beasiswa-gratis.png" />
            </Head>
            <Layout>
                <HeroV2
                    heroTitle="Dapatkan Beasiswa Gratis Untuk Coder Pemula Sekarang!"
                    heroSubtitle="Belajar coding untuk pemula ga harus susah! cukup dapatkan beasiswa ini, kamu dapat mengakses materi coding dasar yang terstruktur secara gratis"
                    heroBackgroundColor="bg-white"
                    heroBackgroundImg="/assets/img/program/hero-beasiswa-gratis.png"
                    heroImg="/assets/img/program/hero-beasiswa-gratis-small.png"
                />
                <section className="bg-light">
                    <div className="container px-5 pt-4">
                        <div className="row justify-content-center text-center">
                            <div className="col-6 col-md-3 my-2">
                                <p className="text-muted"><span className={styles.count}><CountUp end={729} duration={5} /></span> Partisipan</p>
                            </div>
                            <div className="col-6 col-md-3 my-2">
                                <p className="text-muted"><span className={styles.count}><CountUp end={1000} duration={5} /></span> Kuota</p>
                            </div>
                            <div className="col-6 col-md-3 my-2">
                                <p className="text-muted"><span style={{ position: 'relative', top: '-1.3em', fontSize: '100%' }}>Des</span> <span className={styles.count}><CountUp end={31} duration={5} /></span> Berakhir</p>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="section bg-light-codepolitan" id="about">
                    <div className="container p-4 p-lg-5">
                        <div className="row justify-content-around">
                            <div className="col-lg-5">
                                <img className="img-fluid" src="/assets/img/program/beasiswa-gratis-img.png" alt="Beasiswa Gratis" />
                            </div>
                            <div className="col-lg-5">
                                <h2 className="section-title mt-5 pt-lg-3">Tentang Beasiswa</h2>
                                <p className="text-muted my-3">Beasiswa ini disediakan untuk kamu yang masih pemula dan ingin belajar coding dengan materi dasar yang mudah dipahami. Melalui beasiswa ini, kamu yang yang masih pemula dapat belajar dengan mudah secara gratis.</p>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="section">
                    <div className="container p-5">
                        <div className="row text-center mb-5">
                            <div className="col">
                                <h2 className="section-title">Materi Tersedia</h2>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6 col-lg-4 col-xl-3 mb-4">
                                <div className="card border-0 shadow-sm" style={{ borderRadius: '15px' }}>
                                    <div className="card-header bg-white border-0">
                                        <div className="row mt-2">
                                            <div className="col-8">
                                                <img height="50" src="/assets/img/program/icons/icon-code.png" alt="Code" />
                                            </div>
                                            <div className="col-4 text-center">
                                                <span className={styles.card_label_materi}>1</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="card-body text-muted">
                                        <h5>Perkenalan</h5>
                                        <p>Mengenal pemrograman komputer</p>
                                    </div>
                                </div>
                            </div>

                            <div className="col-md-6 col-lg-4 col-xl-3 mb-4">
                                <div className="card border-0 shadow-sm" style={{ borderRadius: '15px' }}>
                                    <div className="card-header bg-white border-0">
                                        <div className="row mt-2">
                                            <div className="col-8">
                                                <img height="50" src="/assets/img/program/icons/icon-laptop.png" alt="Laptop" />
                                            </div>
                                            <div className="col-4 text-center">
                                                <span className={styles.card_label_materi}>2</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="card-body text-muted">
                                        <h5>Algoritma</h5>
                                        <p>Mengenal algoritma  dan pemrograman dasar</p>
                                    </div>
                                </div>
                            </div>

                            <div className="col-md-6 col-lg-4 col-xl-3 mb-4">
                                <div className="card border-0 shadow-sm" style={{ borderRadius: '15px' }}>
                                    <div className="card-header bg-white border-0">
                                        <div className="row mt-2">
                                            <div className="col-8">
                                                <img height="50" src="/assets/img/program/icons/icon-edit.png" alt="Javascript" />
                                            </div>
                                            <div className="col-4 text-center">
                                                <span className={styles.card_label_materi}>3</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="card-body text-muted">
                                        <h5>Text Editor</h5>
                                        <p>Belajar menggunakan text editor</p>
                                    </div>
                                </div>
                            </div>

                            <div className="col-md-6 col-lg-4 col-xl-3 mb-4">
                                <div className="card border-0 shadow-sm" style={{ borderRadius: '15px' }}>
                                    <div className="card-header bg-white border-0">
                                        <div className="row mt-2">
                                            <div className="col-8">
                                                <img height="50" src="/assets/img/program/icons/icon-html-css.png" alt="HTML dan CSS" />
                                            </div>
                                            <div className="col-4 text-center">
                                                <span className={styles.card_label_materi}>4</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="card-body text-muted">
                                        <h5>HTML & CSS</h5>
                                        <p>Belajar bahasa pemrograman HTML & CSS</p>
                                    </div>
                                </div>
                            </div>

                            <div className="col-md-6 col-lg-4 col-xl-3 mb-4">
                                <div className="card border-0 shadow-sm" style={{ borderRadius: '15px' }}>
                                    <div className="card-header bg-white border-0">
                                        <div className="row mt-2">
                                            <div className="col-8">
                                                <img height="50" src="/assets/img/program/icons/icon-jquery.png" alt="JQuery" />
                                            </div>
                                            <div className="col-4 text-center">
                                                <span className={styles.card_label_materi}>5</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="card-body text-muted">
                                        <h5>JQuery</h5>
                                        <p>Belajar menggunakan library JQuery</p>
                                    </div>
                                </div>
                            </div>

                            <div className="col-md-6 col-lg-4 col-xl-3 mb-4">
                                <div className="card border-0 shadow-sm" style={{ borderRadius: '15px' }}>
                                    <div className="card-header bg-white border-0">
                                        <div className="row mt-2">
                                            <div className="col-8">
                                                <img height="50" src="/assets/img/program/icons/icon-js.png" alt="JavaScript Async" />
                                            </div>
                                            <div className="col-4 text-center">
                                                <span className={styles.card_label_materi}>6</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="card-body text-muted">
                                        <h5>JavaScript</h5>
                                        <p>Belajar bahasa pemrograman JavaScript dasar</p>
                                    </div>
                                </div>
                            </div>

                            <div className="col-md-6 col-lg-4 col-xl-3 mb-4">
                                <div className="card border-0 shadow-sm" style={{ borderRadius: '15px' }}>
                                    <div className="card-header bg-white border-0">
                                        <div className="row mt-2">
                                            <div className="col-8">
                                                <img height="50" src="/assets/img/program/icons/icon-python.png" alt="Python" />
                                            </div>
                                            <div className="col-4 text-center">
                                                <span className={styles.card_label_materi}>7</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="card-body text-muted">
                                        <h5>Python</h5>
                                        <p>Belajar bahasa pemrograman Phyton dasar</p>
                                    </div>
                                </div>
                            </div>

                            <div className="col-md-6 col-lg-4 col-xl-3 mb-4">
                                <div className="card border-0 shadow-sm" style={{ borderRadius: '15px' }}>
                                    <div className="card-header bg-white border-0">
                                        <div className="row mt-2">
                                            <div className="col-8">
                                                <img height="50" src="/assets/img/program/icons/icon-git.png" alt="Git" />
                                            </div>
                                            <div className="col-4 text-center">
                                                <span className={styles.card_label_materi}>8</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="card-body text-muted">
                                        <h5>GIT</h5>
                                        <p>Belajar version control git untuk pemula</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="section bg-light">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-6 ps-5 d-none d-lg-block">
                                <img className="img-fluid" src="/assets/img/program/fasilitas-beasiswa.png" alt="Fasilitas Beasiswa" />
                            </div>
                            <div className="col-lg-6 p-4 p-lg-5 text-muted">
                                <h2 className="section-title">Fasilitas Beasiswa</h2>
                                <p className="my-3">Coder yang mendapatkan beasiswa gratis ini akan mendapatkan akses belajar materi untuk pemula secara gratis tanpa batas waktu yang ditentukan, agar kamu dapat mengulangi materinya sampai benar-benar memahaminya.</p>
                                <p>
                                    <FontAwesomeIcon icon={faCheckCircle} className="me-2" /> Akes Belajar Gratis Seumur Hidup
                                </p>
                                <p>
                                    <FontAwesomeIcon icon={faCheckCircle} className="me-2" /> 14 Materi Pemula
                                </p>
                                <p>
                                    <FontAwesomeIcon icon={faCheckCircle} className="me-2" /> 315 modul belajar
                                </p>
                                <p>
                                    <FontAwesomeIcon icon={faCheckCircle} className="me-2" /> 10 Bahasa Pemrograman
                                </p>
                            </div>
                        </div>
                    </div>
                </section>
                <WaktuBelajarSection backgroundImg="/assets/img/program/waktu-belajar-4.png" />
                <ForumSection />
                <CertificateSection />
                <section>
                    <div className="container p-5">
                        <div className="row">
                            <div className="col">
                                <p>Telah dipercaya oleh</p>
                            </div>
                        </div>
                        <div className="row mb-3">
                            <div className="col">
                                <div id="carouselPartnerControls" className="carousel slide" data-bs-ride="carousel">
                                    <div className="carousel-inner">
                                        <div className="carousel-item active">
                                            <img src="/assets/img/partnerlogos/logo-kemnaker.png" className="d-inline w-25 px-lg-5" alt="Kemnaker" />
                                            <img src="/assets/img/partnerlogos/logo-here-maps.png" className="d-inline w-25 px-lg-5" alt="Here" />
                                            <img src="/assets/img/partnerlogos/logo-udemy.png" className="d-inline w-25 px-lg-5" alt="Udemy" />
                                            <img src="/assets/img/partnerlogos/logo-samsung.png" className="d-inline w-25 px-lg-5" alt="Samsung" />
                                        </div>
                                        <div className="carousel-item">
                                            <img src="/assets/img/partnerlogos/logo-lenovo.png" className="d-inline w-25 px-lg-5" alt="Lenovo" />
                                            <img src="/assets/img/partnerlogos/logo-alibaba-cloud.png" className="d-inline w-25 px-lg-5" alt="Alibaba Cloud" />
                                            <img src="/assets/img/partnerlogos/logo-intel.png" className="d-inline w-25 px-lg-5" alt="Intel" />
                                            <img src="/assets/img/partnerlogos/logo-dicoding.png" className="d-inline w-25 px-lg-5" alt="Dicoding" />
                                        </div>
                                        <div className="carousel-item">
                                            <img src="/assets/img/partnerlogos/logo-ibm.png" className="d-inline w-25 px-lg-5" alt="IBM" />
                                            <img src="/assets/img/partnerlogos/logo-pixel.png" className="d-inline w-25 px-lg-5" alt="Pixel" />
                                            <img src="/assets/img/partnerlogos/logo-intel.png" className="d-inline w-25 px-lg-5" alt="Intel" />
                                            <img src="/assets/img/partnerlogos/logo-xl.png" className="d-inline w-25 px-lg-5" alt="XL Axiata" />
                                        </div>
                                        <div className="carousel-item">
                                            <img src="/assets/img/partnerlogos/logo-hacktiv8.png" className="d-inline w-25 px-lg-5" alt="Hactive8" />
                                            <img src="/assets/img/partnerlogos/logo-kudo.png" className="d-inline w-25 px-lg-5" alt="Kudo" />
                                            <img src="/assets/img/partnerlogos/logo-refactory.png" className="d-inline w-25 px-lg-5" alt="Refactory" />
                                            <img src="/assets/img/partnerlogos/logo-ajita.png" className="d-inline w-25 px-lg-5" alt="Ajita" />
                                        </div>
                                    </div>
                                    <button className="carousel-control-prev" type="button" data-bs-target="#carouselPartnerControls" data-bs-slide="prev">
                                        <span className="carousel-control-prev-icon" aria-hidden="true" />
                                        <span className="visually-hidden">Previous</span>
                                    </button>
                                    <button className="carousel-control-next" type="button" data-bs-target="#carouselPartnerControls" data-bs-slide="next">
                                        <span className="carousel-control-next-icon" aria-hidden="true" />
                                        <span className="visually-hidden">Next</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <CtaSectionV2 />
            </Layout>
        </>
    );
};

export default ProgramBeasiswaLanding;
