import { faDiscord } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Head from 'next/head';
import Layout from '../../components/global/Layout/Layout';
import Hero from '../../components/program/Hero/Hero';
import styles from '../../styles/Program.module.scss';

const DiscordLanding = () => {
    return (
        <>
            <Head>
                <title>Yuk Gabung Komunitas Discord Codepolitan - Codepolitan</title>
                <meta name="title" content="Yuk Gabung Komunitas Discord Codepolitan" />
                <meta name="description" content="Belajar coding jadi lebih seru! cukup gabung Discord komunitas coding Codepolitan kamu ga akan belajar sendirian, semua anggota Discord Codepolitan akan selalu aktif berdiskusi." />
                <meta property="og:type" content="website" />
                <meta property="og:url" content="https://codepolitan.com/program/join-discord" />
                <meta property="og:title" content="Yuk Gabung Komunitas Discord Codepolitan" />
                <meta property="og:description" content="Belajar coding jadi lebih seru! cukup gabung Discord komunitas coding Codepolitan kamu ga akan belajar sendirian, semua anggota Discord Codepolitan akan selalu aktif berdiskusi." />
                <meta property="og:image" content="/assets/img/program/thumbnail/thumbnail-discord.png" />
                <meta property="twitter:card" content="summary_large_image" />
                <meta property="twitter:url" content="https://codepolitan.com/program/join-discord" />
                <meta property="twitter:title" content="Yuk Gabung Komunitas Discord Codepolitan" />
                <meta property="twitter:description" content="Belajar coding jadi lebih seru! cukup gabung Discord komunitas coding Codepolitan kamu ga akan belajar sendirian, semua anggota Discord Codepolitan akan selalu aktif berdiskusi." />
                <meta property="twitter:image" content="/assets/img/program/thumbnail/thumbnail-discord.png" />
            </Head>
            <Layout>
                <Hero
                    heroTitle="Yuk Gabung Komunitas Discord Codepolitan"
                    heroSubtitle="Belajar coding jadi lebih seru! cukup gabung Discord komunitas coding Codepolitan kamu ga akan belajar sendirian, semua anggota Discord Codepolitan akan selalu aktif berdiskusi."
                    heroImg="/assets/img/program/hero-discord.png"
                    heroBgColor="bg-white"
                    heroFontColor="text-dark"
                    heroBtnColor="btn-outline-primary"
                />
                <section className="section bg-light" id="about">
                    <div className="container p-4 p-lg-5">
                        <div className="row justify-content-between">
                            <div className="col-lg-6">
                                <div className="ratio ratio-16x9 my-5">
                                    <iframe style={{ borderRadius: '25px' }} src="https://www.youtube.com/embed/bwsV2s0gJeI" title="YouTube video" allowFullScreen></iframe>
                                </div>
                            </div>
                            <div className="col-lg-5 my-auto">
                                <h2 className="section-title">Tujuan Komunitas</h2>
                                <p className="text-muted my-3">Komunitas ini disediakan untuk kamu yang suka kebingunan ketika ngoding atau memiliki masalah terkait pemrograman. Melalui komunitas Discord Codepolitan, kamu akan mendapatkan jawaban dengan cepat dari anggota Discord Codepolitan dan tentunya ga akan belajar sendirian lagi.</p>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="section" style={{ background: '#f3f3f3' }}>
                    <div className="container p-4 p-lg-0">
                        <div className="row justify-content-around">
                            <div className="col-md-6 d-none d-lg-block">
                                <img className="img-fluid mt-5" src="/assets/img/program/discord-img-2.png" alt="Discord" />
                            </div>
                            <div className="col-lg-5 my-auto">
                                <h2 className="section-title">Apa yang Kamu Dapatkan di Discord Codepolitan?</h2>
                                <p className="text-muted my-3">Dengan komunitas Discord Code Politan, banyak manfaat yang akan kamu dapatkan</p>
                                <ul className={`${styles.discord_icon} text-muted`}>
                                    <li>Berkenalan Dengan Sesama Programer</li>
                                    <li>Diskusi Teknologi dan Pemrograman</li>
                                    <li>Update Informasi Belajar di Codepolitan</li>
                                    <li>Update Informasi Event Teknologi dan Pemrograman</li>
                                    <li>Informasi Seputar Coding</li>
                                    <li>Informasi Lowongan Kerja</li>
                                    <li>Hiburan Geek Shitposting</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="section bg-codepolitan">
                    <div className="container p-5">
                        <div className="row">
                            <div className="col-md-6 my-auto text-center text-md-start">
                                <h2 className="section-title text-white">Tunggu Apalagi ? Ayo Seru-Seruan Belajar Ngoding Bareng Di Server Discord Codepolitan</h2>
                                <a className="btn btn-light btn-lg mt-3" href="https://discord.gg/HF2UpWNCWX">
                                    <FontAwesomeIcon className="me-2" icon={faDiscord} />
                                    Gabung Discord
                                </a>
                            </div>
                            <div className="col-md-6 mt-5 mt-lg-0">
                                <img className="img-fluid" style={{ borderRadius: '20px' }} src="/assets/img/program/discord-img-3.png" alt="Gabung Discord" />
                            </div>
                        </div>
                    </div>
                </section>
            </Layout>
        </>
    )
}

export default DiscordLanding;
