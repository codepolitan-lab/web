import { useState } from "react";
import axios from "axios";
import Head from "next/head";
import Layout from "../../components/global/Layout/Layout";
import CourselistSection from "../../components/program/CourselistSection/CourselistSection";
import WarrantySection from '../../components/global/Sections/WarrantySection/WarrantySection';
import CertificateSection from "../../components/program/CertificateSection/CertificateSection";
import MentorSection from "../../components/program/MentorSection/MentorSection";
import HowToLearnSection from "../../components/program/HowToLearnSection/HowToLearnSection";
import TestimonySection from "../../components/program/TestimonySection/TestimonySection";
import CtaSection from "../../components/program/CtaSection/CtaSection";
import FaqSection from "../../components/program/FaqSection/FaqSection";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheckCircle, faClock, faComments, faLaptopCode, faLock } from "@fortawesome/free-solid-svg-icons";
import { getLessons } from "../../utils/helper";
import HeroMobirise from "../../components/program/Hero/HeroMobirise/HeroMobirise";

export const getServerSideProps = async () => {
    const courseDetailRes = await axios.get(`https://api.codepolitan.com/course/detail/membuat-website-profile-perusahaan-tanpa-coding-dengan-mobirise`);
    const testimonyRes = await axios.get(`https://apps.codepolitan.com/api/feedback/course/membuat-website-profile-perusahaan-tanpa-coding-dengan-mobirise`);
    const statsRes = await axios.get(`https://api.codepolitan.com/v1/stats`);

    const [courseDetail, testimony, stats] = await Promise.all([
        courseDetailRes.data,
        testimonyRes.data,
        statsRes.data
    ]);
    return {
        props: {
            courseDetail,
            testimony,
            stats
        }
    }
}

const MobiriseLanding = ({ courseDetail, testimony, stats }) => {
    const lessons = courseDetail.lessons;

    const lessonList = getLessons(lessons);
    const lessonTopic = lessonList.lessonTopic;
    const lessonContent = lessonList.lessonContent;

    const [faqData] = useState([
        {
            id: 'flush-heading1',
            target: 'flush-collapse1',
            title: 'Mengapa menggunakan Mobirise?',
            content: `<p>CodePolitan sudah memiliki lebih dari ${stats.total_course} kelas online dan ${stats.total_study} modul belajar terkait pemrograman. Kami juga sudah bertemu dengan banyak sekali siswa yang belajar pemrograman hanya untuk sekedar membuat sebuah website untuk perusahaan atau pemasaran produk mereka. Banyak diantara mereka akhirnya kelelahan karena belajar pemrograman tidak sesederhana yang mereka bayangkan sebelumnya.</p><p>Kami akhirnya mencari solusi atas kebutuhan ini dan menemukan banyak sekali tools yang dapat digunakan untuk mendukung kebutuhan tersebut. Kami mencobanya satu per satu dan akhirnya menemukan kalau Mobirise-lah yang paling pas untuk kebutuhan ini.</p><p>Mobirise adalah website builder yang dapat diinstal di komputer dan digunakan secara offline. Mobirise menyediakan template gratis dengan block-block template yang banyak dan variatif dan dapat dimodifikasi sesuai kebutuhan. Selain itu kami juga menemukan cara yang paling efektif dan hemat untuk mempublikasikan website tanpa harus menyewa server atau menginstal CMS terlebih dahulu.</p><p>Di kelas online ini mentor kami sudah mengupas semua fitur Mobirise dan meramu sedemikian rupa sehingga Anda dapat membuat dan mengonlinekan website Anda nantinya tanpa budget sepeserpun. Meski demikian, Anda tetap dapat memindahkan website Anda ke server sendiri dan mengembangkannya lebih lanjut bila Anda memiliki keahlian ngoding atau memiliki tim programmer kedepannya.</p>`
        },
        {
            id: 'flush-heading2',
            target: 'flush-collapse2',
            title: 'Kapan pertemuan materi disampaikan oleh mentor?',
            content: 'Mentor kami telah membuat rangkaian video yang berisi pembahasan materi. Artinya Anda dapat mempelajari kapanpun video tersebut tanpa batas waktu.'
        },
        {
            id: 'flush-heading3',
            target: 'flush-collapse3',
            title: 'Alat apa saja yang diperlukan untuk belajar?',
            content: 'Anda akan menggunakan laptop atau komputer yang terhubung ke internet. Setelah mendaftar program Anda akan langsung mendapat akses kelas untuk mulai belajar. Silakan ikuti materi dan praktekkan studi kasus yang disampaikan oleh mentor di video pembelajaran tersebut.'
        },
        {
            id: 'flush-heading4',
            target: 'flush-collapse4',
            title: 'Kalau ada materi yang membingungkan bagaimana solusinya?',
            content: 'Anda dapat langsung bertanya pada bagian diskusi di halaman materi yang sedang Anda pelajari. Tanyakan bagian mana yang membuat Anda kurang paham. Mentor kami akan membantu menjawab persoalan Anda melalui kanal diskusi tersebut.'
        },
        {
            id: 'flush-heading5',
            target: 'flush-collapse5',
            title: 'Bisakah saya bertanya langsung via tatap muka dengan mentor?',
            content: 'Untuk saat ini belum. In syaa Allah kedepannya kami akan menyediakan layanan bimbingan privat untuk belajar pemrograman secara tatap muka online dan realtime bersama mentor.'
        },
    ]);

    return (
        <>
            <Head>
                <title>Mobirise - Codepolitan</title>
                <meta name="title" content="Bisa Bikin Web Perusahaan Tanpa Ngoding?" />
                <meta name="description" content="Apapun latar belakang kamu, melalui kelas online ini kamu akan bisa membuat website perusahaan yang super keren dan memiliki nilai jual tanpa harus jadi jago coding." />

                <meta property="og:type" content="website" />
                <meta property="og:url" content="https://codepolitan.com/program/mobirise" />
                <meta property="og:title" content="Bisa Bikin Web Perusahaan Tanpa Ngoding?" />
                <meta property="og:description" content="Apapun latar belakang kamu, melalui kelas online ini kamu akan bisa membuat website perusahaan yang super keren dan memiliki nilai jual tanpa harus jadi jago coding." />
                <meta property="og:image" content="/assets/img/program/thumbnail/thumbnail-mobirise.png" />

                <meta property="twitter:card" content="summary_large_image" />
                <meta property="twitter:url" content="https://codepolitan.com/program/mobirise" />
                <meta property="twitter:title" content="Bisa Bikin Web Perusahaan Tanpa Ngoding?" />
                <meta property="twitter:description" content="Apapun latar belakang kamu, melalui kelas online ini kamu akan bisa membuat website perusahaan yang super keren dan memiliki nilai jual tanpa harus jadi jago coding." />
                <meta property="twitter:image" content="/assets/img/program/thumbnail/thumbnail-mobirise.png" />
            </Head>
            <Layout>
                <HeroMobirise />
                <section className="section">
                    <div className="container p-4 p-lg-5">
                        <div className="row justify-content-between mb-5">
                            <div className="col-lg-5 order-lg-last mb-5">
                                <img className="img-fluid d-none d-lg-block mx-auto" src="/assets/img/program/img-mobirise-2.png" alt="Mobirise 1" />
                                <img className="img-fluid d-block d-lg-none mx-auto" src="/assets/img/program/img-mobirise-2-mobile.png" alt="Mobirise 1" />
                            </div>
                            <div className="col-lg-6 text-muted my-auto">
                                <h2 className="section-title mb-3">Apakah Kamu Termasuk?</h2>
                                <div className="d-flex align-items-start">
                                    <FontAwesomeIcon size="lg" className="me-2 mt-1" icon={faCheckCircle} />
                                    <p>Pemilik usaha yang pengen bikin landing page bisnis sendiri dengan budget minimum</p>
                                </div>
                                <div className="d-flex align-items-start">
                                    <FontAwesomeIcon size="lg" className="me-2 mt-1" icon={faCheckCircle} />
                                    <p>Marketer yang ingin jualan produk online menggunakan landing page</p>
                                </div>
                                <div className="d-flex align-items-start">
                                    <FontAwesomeIcon size="lg" className="me-2 mt-1" icon={faCheckCircle} />
                                    <p>Programmer yang ingin meminimalisir effort saat ngerjain project kecil</p>
                                </div>
                                <div className="d-flex align-items-start">
                                    <FontAwesomeIcon size="lg" className="me-2 mt-1" icon={faCheckCircle} />
                                    <p>Desainer yang pengen nambah penghasilan dari pengembangan web company profile</p>
                                </div>
                                <div className="d-flex align-items-start">
                                    <FontAwesomeIcon size="lg" className="me-2 mt-1" icon={faCheckCircle} />
                                    <p>Pelajar atau mahasiswa yang ingin belajar membuat website cepat dan hemat</p>
                                </div>
                                <div className="d-flex align-items-start">
                                    <FontAwesomeIcon size="lg" className="me-2 mt-1" icon={faCheckCircle} />
                                    <p>Siapapun yang ingin dapat penghasilan tambahan dari membuat website</p>
                                </div>
                            </div>
                        </div>
                        <div className="row justify-content-between mb-5">
                            <div className="col-lg-5 my-auto mb-5">
                                <img className="img-fluid d-none d-lg-block mx-auto" src="/assets/img/program/img-mobirise-3.png" alt="" />
                                <img className="img-fluid d-block d-lg-none mx-auto" src="/assets/img/program/img-mobirise-3-mobile.png" alt="" />
                            </div>
                            <div className="col-lg-6 my-auto text-muted">
                                <h2 className="section-title mb-3">Mungkin Anda pernah mengalami masalah seperti ini:</h2>
                                <div className="d-flex align-items-start">
                                    <FontAwesomeIcon size="lg" className="me-2 mt-1" icon={faCheckCircle} />
                                    <p>Mau bikin landing page perusahaan tapi punya budget terbatas</p>
                                </div>
                                <div className="d-flex align-items-start">
                                    <FontAwesomeIcon size="lg" className="me-2 mt-1" icon={faCheckCircle} />
                                    <p>Sudah belajar internet marketing tapi kebingungan di tahapan membuat landing page</p>
                                </div>
                                <div className="d-flex align-items-start">
                                    <FontAwesomeIcon size="lg" className="me-2 mt-1" icon={faCheckCircle} />
                                    <p>Mau punya landing page promosi produk dengan budget superhemat</p>
                                </div>
                                <div className="d-flex align-items-start">
                                    <FontAwesomeIcon size="lg" className="me-2 mt-1" icon={faCheckCircle} />
                                    <p>Pengen fokus jualan dan ga mau diribetin sama belajar ngoding dulu</p>
                                </div>
                            </div>
                        </div>
                        <div className="row justify-content-between mb-5">
                            <div className="col-lg-5 order-lg-last mb-5">
                                <img className="img-fluid d-block mx-auto" src="/assets/img/program/img-mobirise-4.png" alt="Mobirise 1" />
                            </div>
                            <div className="col-lg-6 text-muted my-auto">
                                <h2 className="section-title mb-3">Atau Anda Seorang Programmer Yang</h2>
                                <div className="d-flex align-items-start">
                                    <FontAwesomeIcon size="lg" className="me-2 mt-1" icon={faCheckCircle} />
                                    <p>Baru belajar ngoding tapi pengen cepet-cepet ambil proyek</p>
                                </div>
                                <div className="d-flex align-items-start">
                                    <FontAwesomeIcon size="lg" className="me-2 mt-1" icon={faCheckCircle} />
                                    <p>Sering diminta bikin landing page dengan budget cuma ratusan ribu</p>
                                </div>
                                <div className="d-flex align-items-start">
                                    <FontAwesomeIcon size="lg" className="me-2 mt-1" icon={faCheckCircle} />
                                    <p>Mau bikin website tapi kepentok di skill desain web</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section className="section bg-codepolitan">
                    <div className="container p-4 p-lg-5">
                        <div className="row">
                            <div className="col text-center">
                                <h2 className="section-title w-75 mx-auto text-white">Dengan Kelas Ini Kamu Dapat Mengatasi Masalah Tersebut Dengan Mudah Loh</h2>
                            </div>
                        </div>
                    </div>
                </section>

                <section className="section">
                    <div className="container p-4 p-lg-5">
                        <div className="row my-5">
                            <div className="col text-center">
                                <h2 className="section-title w-75 mx-auto">Membuat Website Profile Perusahaan Tanpa Coding dengan Mobirise</h2>
                            </div>
                        </div>
                        <div className="row justify-content-center">
                            <div className="col-lg-7">
                                <div className="ratio ratio-16x9">
                                    <iframe style={{ borderRadius: '25px' }} src="https://www.youtube.com/embed/HaLxOUsZHaI" title="YouTube video player" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
                                </div>
                            </div>
                        </div>
                        <div className="row my-5">
                            <div className="col text-center text-muted">
                                <p className="w-75 mx-auto">Mendesain dan mengonlinekan website tidak pernah semudah ini sebelumnya! Hanya dengan berbekal laptop/komputer dan koneksi internet, kelas online ini akan memandu Anda langkah demi langkah cara praktis membuat website perusahaan dengan tampilan yang modern dan keren. Tidak diperlukan keahlian ngoding sama sekali!</p>
                            </div>
                        </div>
                    </div>
                </section>

                <section className="section bg-light">
                    <div className="container p-4 p-lg-5">
                        <div className="row my-5">
                            <div className="col text-center text-muted">
                                <h2 className="section-title">Ngga Harus Keluar Banyak Biaya untuk Hal yang Dapat Dikerjakan dengan Sangat Mudah</h2>
                                <p className="my-3 w-75 mx-auto">Anda bahkan nyaris tidak perlu mengeluarkan uang sama sekali untuk membeli software dan juga server untuk mengonlinekan website Anda nanti.</p>
                            </div>
                        </div>
                        <div className="row justify-content-center text-center mb-5">
                            <div className="col-md-6 col-lg-4 mb-3">
                                <div className="card border-0 shadow-sm rounded">
                                    <div className="card-body">
                                        <FontAwesomeIcon className="text-primary" size="5x" icon={faLaptopCode} />
                                        <h5 className="my-3">Belajar Studi Kasus</h5>
                                        <p className="text-muted">Praktek dengan studi kasus tidak hanya memudahkan pemahaman dari sisi teori, tapi juga memberikan gambaran praktis sesuai dengan kebutuhan di dunia kerja.</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6 col-lg-4 mb-3">
                                <div className="card border-0 shadow-sm rounded" style={{ height: '100%' }}>
                                    <div className="card-body">
                                        <FontAwesomeIcon className="text-primary" size="5x" icon={faClock} />
                                        <h5 className="my-3">Waktu Belajar Fleksibel</h5>
                                        <p className="text-muted">Tidak perlu takut ketinggalan pembahasan di kelas. Anda dapat mempelajari materi sesuai dengan jadwal yang Anda atur sendiri.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6 col-lg-4 mb-3">
                                <div className="card border-0 shadow-sm rounded">
                                    <div className="card-body">
                                        <FontAwesomeIcon className="text-primary" size="5x" icon={faComments} />
                                        <h5 className="my-3">Fitur Tanya Jawab</h5>
                                        <p className="text-muted">Jika kamu memiliki kendala saat belajar, kamu bisa langsung bertanya di kanal tanya jawab yang telah disediakan. Tim Codepolitan akan senang hati membantumu.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <CertificateSection />
                <MentorSection
                    name="Yusuf Fazeri"
                    title="Mentor kelas Mobirise"
                    description="Saya Yusuf Fazeri, seorang Full Stack Developer dan Senior Programmer di Codepolitan. Dengan pengalaman saya lebih dari 6 tahun berkarya dan  berkarir di bidang programming akan membantu kamu belajar dalam kelas ini. Apapun latar belakang kamu, melalui kelas ini kamu akan belajar membuat website perusahaan sendiri yang super keren. Saya tunggu di kelas!"
                    img="/assets/img/program/yusuf.png"
                />
                <HowToLearnSection />
                <CourselistSection>
                    <h2 className="text-center text-muted my-5">Apa Saja Materi yang akan Dipelajari?</h2>
                    <ul
                        className="nav nav-pills d-grid d-md-flex nav-justified bg-light mb-3"
                        id="pills-tab"
                        role="tablist"
                    >
                        {lessonTopic.map((item, index) => {
                            return (
                                <li className="nav-item" role="presentation" key={index}>
                                    <button
                                        className={`nav-link py-3 ${index < 1 && 'active'}`}
                                        id={item.topic_slug.slice(3) + '-tab'}
                                        data-bs-toggle="pill"
                                        data-bs-target={'#' + item.topic_slug.slice(3)}
                                        type="button"
                                        role="tab"
                                        aria-controls={item.topic_slug.slice(3)}
                                        aria-selected="true"
                                        title={item.topic_title}
                                    >
                                        {item.topic_title.length > 20 ? item.topic_title.slice(0, 20) + '...' : item.topic_title}
                                    </button>
                                </li>
                            );
                        })}
                    </ul>

                    <div className="tab-content" id="pills-tabContent">
                        {lessonContent.map((content, index) => {
                            return (
                                <div key={index} className={`tab-pane fade show ${index < 1 && 'active'}`} id={content.topic_slug.slice(3)} role="tabpanel" aria-labelledby={content.topic_slug.slice(3) + '-tab'}>
                                    <table className="table table-striped">
                                        <tbody>
                                            {content.contents?.map((item, index) => {
                                                return (
                                                    <tr key={index}>
                                                        <td style={{ width: '50px' }}>
                                                            <img height="30" src="/assets/img/program/play-icon.png" alt="Icon" />
                                                        </td>
                                                        <td className="text-muted">{item.lesson_title}</td>
                                                        <td className="text-end text-muted">
                                                            <FontAwesomeIcon icon={faLock} className="me-2" />
                                                            {item.duration || '15:00'}
                                                        </td>
                                                    </tr>
                                                );
                                            })}
                                        </tbody>
                                    </table>
                                </div>
                            );
                        })}
                    </div>
                </CourselistSection>
                <TestimonySection data={testimony} />
                <section className="section d-none" style={{ backgroundColor: '#FDFDFD' }}>
                    <div className="container p-4 p-lg-5">
                        <div className="row">
                            <div className="col text-center text-muted">
                                <h2 className="section-title mb-5">Tapi, sebelum Anda memutuskan untuk bergabung, pahami problem krusial ini terlebih dahulu!</h2>
                                <p>Anda perlu memahami bahwa dengan membuat website atau landing page saja tidak lantas membuat penjualan produk Anda meningkat, atau brand bisnis Anda serta merta langsung jadi terkenal.</p>
                                <p>Membuat website ibarat membuka toko. Anda harus benar-benar menata toko Anda sedemikian rupa supaya pengunjung paham penawaran apa yang Anda sajikan di toko Anda. Membuat website tanpa konsep yang jelas tidak hanya membuat pengunjung website Anda jadi gagal paham, tapi juga pergi meninggalkan website Anda tanpa membawa kesan dan pesan apapun di benak mereka.</p>
                                <p>Ingat, di dunia marketing, penjualan terjadi sejak Anda berhasil menanamkan brand dan penawaran Anda ke dalam benak pengunjung Anda!</p>
                                <p>Oleh karena itu, kami bekali Anda dengan dua buah Ebook eksklusif ini:</p>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="section d-none">
                    <div className="container p-4 p-lg-5">
                        <div className="row text-center text-muted">
                            <div className="col-12 mb-5">
                                <h2 className="section-title">17 Kunci Rahasia Landing Page yang Berhasil Menjual dalam Sekali Baca</h2>
                                <img className="img-fluid d-block mx-auto" src="/assets/img/program/ebook-1.png" alt="17 Kunci Rahasia Landing Page yang Berhasil Menjual dalam Sekali Baca" />
                                <p>Dalam ebook ini dibahas tuntas 17 poin kunci yang wajib Anda sertakan di dalam Halaman website Anda agar pengunjung website Anda buru-buru untuk membeli produk/layanan Anda.</p>
                            </div>
                            <div className="col-12">
                                <h2 className="section-title">Branding Acceleration</h2>
                                <img className="img-fluid d-block mx-auto" src="/assets/img/program/ebook-2.png" alt="Branding Acceleration" />
                                <p>Dalam ebook ini dibahas teknik praktis dan selalu ampuh dalam membenamkan brand Anda ke benak calon konsumen dalam waktu 21 hari bahkn kurang!</p>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="section bg-codepolitan d-none">
                    <div className="container p-4 p-lg-5">
                        <div className="row">
                            <div className="col text-center">
                                <h2 className="section-title text-white">Dua ebook powerful senilai Rp 850.000 ini akan kami sertakan dalam paket pembelajaran kelas online tanpa biaya tambahan lain bila Anda bergabung dalam program terbatas ini sebelum tanggal 21 November 2021.</h2>
                            </div>
                        </div>
                    </div>
                </section>
                <div className="bg-light">
                    <h2 className="text-center pt-5">Masih Ragu?</h2>
                    <WarrantySection />
                </div>
                <CtaSection
                    title="Tunggu Apalagi ? Ayo Gabung Kelas Bikin Web Tanpa Koding Sekarang !"
                    priceImg="/assets/img/program/price-mobirise.png"
                    actionLink="https://pay.codepolitan.com/?slug=membuat-website-profile-perusahaan-tanpa-coding-dengan-mobirise"
                />
                <FaqSection data={faqData} />
            </Layout>
        </>
    );
};

export default MobiriseLanding;
