import axios from 'axios';
import Head from 'next/head';
import Layout from '../../components/global/Layout/Layout';
import CertificateSection from '../../components/program/CertificateSection/CertificateSection';
import FaqSection from '../../components/program/FaqSection/FaqSection';
import ForumSection from '../../components/program/ForumSection/ForumSection';
import Hero from '../../components/program/Hero/Hero';
import HowToLearnSection from '../../components/program/HowToLearnSection/HowToLearnSection';
import StudycaseSection from '../../components/program/StudycaseSection/StudycaseSection';
import TestimonySection from '../../components/program/TestimonySection/TestimonySection';
import WaktuBelajarSection from '../../components/program/WaktuBelajarSection/WaktuBelajarSection';
import WarrantySection from '../../components/global/Sections/WarrantySection/WarrantySection';
import CountUp from 'react-countup';
import styles from '../../styles/Program.module.scss';
import MentorSection from '../../components/program/MentorSection/MentorSection';
import AboutSection from '../../components/program/AboutSection/AboutSection';
import CtaSection from '../../components/program/CtaSection/CtaSection';

export const getServerSideProps = async () => {
    const testimonyRes = await axios.get(`https://apps.codepolitan.com/api/feedback/course/laravel-8x-fundamental`);

    const [testimony] = await Promise.all([
        testimonyRes.data,
    ]);
    return {
        props: {
            testimony
        }
    }
}

const PathLaravelLanding = ({ testimony }) => {
    const data = [
        {
            case_study: [
                {
                    id: 1,
                    thumbnail: 'https://i.ibb.co/GnLdypF/fullstack-1.png',
                    title: 'Aplikasi Todo Management'
                },
                {
                    id: 2,
                    thumbnail: 'https://i.ibb.co/7yTMGVh/fullstack-2.png',
                    title: 'Website Blog Artikel'
                },
                {
                    id: 3,
                    thumbnail: 'https://i.ibb.co/TwPtCj4/fullstack-3.png',
                    title: 'Website Toko Online'
                },
                {
                    id: 4,
                    thumbnail: 'https://i.ibb.co/XCt04nV/fullstack-4.png',
                    title: 'Sistem Perpustakaan'
                },
            ],
            courses: [
                {
                    id: 15,
                    thumbnail: 'https://i.ibb.co/ccQ0Hty/icon-laravel.png',
                    title: 'Laravel Fundamental',
                    description: 'Mendalami Fundamental Laravel 8.x',
                    number_module: '46'
                },
                {
                    id: 2,
                    thumbnail: 'https://i.ibb.co/ccQ0Hty/icon-laravel.png',
                    title: 'Laravel Intermediate',
                    description: 'Mendalami Laravel',
                    number_module: '32'
                },
                {
                    id: 4,
                    thumbnail: 'https://i.ibb.co/ccQ0Hty/icon-laravel.png',
                    title: 'Membuat RestFull API',
                    description: 'Membangun Restfull Api dengan Laravel 6',
                    number_module: '19'
                },
                {
                    id: 5,
                    thumbnail: 'https://i.ibb.co/ccQ0Hty/icon-laravel.png',
                    title: 'Sistem Otentikasi API',
                    description: 'Sistem Otentikasi OAuth Dengan Laravel Passport',
                    number_module: '21'
                },
                {
                    id: 6,
                    thumbnail: 'https://i.ibb.co/ccQ0Hty/icon-laravel.png',
                    title: 'Payment Gateway',
                    description: 'Implementasi Payment Gateway dengan Laravel',
                    number_module: '12'
                },
                {
                    id: 7,
                    thumbnail: 'https://i.ibb.co/ccQ0Hty/icon-laravel.png',
                    title: 'Studi Kasus Laravel',
                    description: 'Toko Online dengan Livewire & Payment Gateway',
                    number_module: '30'
                },
                {
                    id: 8,
                    thumbnail: 'https://i.ibb.co/SBYN97M/icon-laravue.png',
                    title: 'Studi Kasus Laravel',
                    description: 'Membangun Aplikasi SPA Autentikasi dengan Laravel & Vue',
                    number_module: '21'
                },
                {
                    id: 9,
                    thumbnail: 'https://i.ibb.co/SBYN97M/icon-laravue.png',
                    title: 'Studi Kasus Laravel',
                    description: 'Membuat Realtime Chatroom dengan Laravel & Vue',
                    number_module: '11'
                },
                {
                    id: 10,
                    thumbnail: 'https://i.ibb.co/ccQ0Hty/icon-laravel.png',
                    title: 'Studi Kasus Laravel',
                    description: 'Membuat Role Management Menggunakan Laravel',
                    number_module: '12'
                },
                {
                    id: 11,
                    thumbnail: 'https://i.ibb.co/ccQ0Hty/icon-laravel.png',
                    title: 'Studi Kasus Laravel',
                    description: 'Membangun Aplikasi Cek Ongkir Menggunakan Laravel',
                    number_module: '13'
                },
                {
                    id: 12,
                    thumbnail: 'https://i.ibb.co/WW5ftSR/icon-larahere.png',
                    title: 'Studi Kasus Laravel',
                    description: 'Membuat Sistem Geolocation dengan Laravel Dan Here',
                    number_module: '21'
                },
                {
                    id: 13,
                    thumbnail: 'https://i.ibb.co/ccQ0Hty/icon-laravel.png',
                    title: 'Optimasi Kerja',
                    description: 'Optimasi Kinerja Laravel dengan Redis',
                    number_module: '7'
                },
                {
                    id: 14,
                    thumbnail: 'https://i.ibb.co/ccQ0Hty/icon-laravel.png',
                    title: 'Fitur Baru Laravel 7.x',
                    description: 'Mendalami Fitur Terbaru Laravel 7.x',
                    number_module: '4'
                },
            ],
            testimony: [
                {
                    name: 'Test',
                    comment: 'Lorem ipsum',
                    rate: 4
                },
                {
                    name: 'Test',
                    comment: 'Lorem ipsum',
                    rate: 4
                },
                {
                    name: 'Test',
                    comment: 'Lorem ipsum',
                    rate: 4
                },
                {
                    name: 'Test',
                    comment: 'Lorem ipsum',
                    rate: 4
                },
                {
                    name: 'Test',
                    comment: 'Lorem ipsum',
                    rate: 4
                },
            ],
            faq: [
                {
                    id: 'flush-heading1',
                    target: 'flush-collapse1',
                    title: 'Laravel versi berapa yang digunakan dalam pembelajaran?',
                    content: 'Kami menggunakan Laravel versi 8 untuk materi pembelajaran dasar, dan versi sebelumnya untuk beberapa studi kasus yang masih dapat diikuti secara teori dan praktik untuk versi terbaru.'
                },
                {
                    id: 'flush-heading2',
                    target: 'flush-collapse2',
                    title: 'Kapan pertemuan materi disampaikan oleh mentor?',
                    content: 'Mentor kami telah membuat rangkaian video yang berisi pembahasan materi. Artinya Anda dapat mempelajari kapanpun video tersebut tanpa batas waktu.'
                },
                {
                    id: 'flush-heading3',
                    target: 'flush-collapse3',
                    title: 'Alat apa saja yang diperlukan untuk belajar?',
                    content: 'Anda akan menggunakan laptop atau komputer yang terhubung ke internet. Setelah mendaftar program Anda akan langsung mendapat akses kelas untuk mulai belajar. Silakan ikuti materi dan praktekkan studi kasus yang disampaikan oleh mentor di video pembelajaran tersebut.'
                },
                {
                    id: 'flush-heading4',
                    target: 'flush-collapse4',
                    title: 'Kalau ada materi yang membingungkan bagaimana solusinya?',
                    content: 'Anda dapat langsung bertanya pada bagian diskusi di halaman materi yang sedang Anda pelajari. Tanyakan bagian mana yang membuat Anda kurang paham. Mentor kami akan membantu menjawab persoalan Anda melalui kanal diskusi tersebut.'
                },
                {
                    id: 'flush-heading5',
                    target: 'flush-collapse5',
                    title: 'Bisakah saya bertanya langsung via tatap muka dengan mentor?',
                    content: 'Untuk saat ini belum. In syaa Allah kedepannya kami akan menyediakan layanan bimbingan privat untuk belajar pemrograman secara tatap muka online dan realtime bersama mentor.'
                },
            ]
        }
    ];

    return (
        <>
            <Head>
                <title>Path Laravel Developer - Codepolitan</title>
                <meta name="title" content="Ikut kelas online Laravel dan Mulai Karir Programmermu!" />
                <meta name="description" content="Apapun latar belakang kamu, melalui kelas online codepolitan kamu akan belajar terarah dari nol hingga menjadi mahir pemrograman Laravel dengan standar kompetensi kerja" />

                <meta property="og:type" content="website" />
                <meta property="og:url" content="https://codepolitan.com/program/path-laravel" />
                <meta property="og:title" content="Ikut kelas online Laravel dan Mulai Karir Programmermu!" />
                <meta property="og:description" content="Apapun latar belakang kamu, melalui kelas online codepolitan kamu akan belajar terarah dari nol hingga menjadi mahir pemrograman Laravel dengan standar kompetensi kerja" />
                <meta property="og:image" content="/assets/img/program/hero-path-laravel.webp" />

                <meta property="twitter:card" content="summary_large_image" />
                <meta property="twitter:url" content="https://codepolitan.com/program/path-laravel" />
                <meta property="twitter:title" content="Ikut kelas online Laravel dan Mulai Karir Programmermu!" />
                <meta property="twitter:description" content="Apapun latar belakang kamu, melalui kelas online codepolitan kamu akan belajar terarah dari nol hingga menjadi mahir pemrograman Laravel dengan standar kompetensi kerja" />
                <meta property="twitter:image" content="/assets/img/program/hero-path-laravel.webp" />
            </Head>
            <Layout>
                <Hero
                    heroTitle={<span>Ikut Kelas Online <span className="text-primary">Laravel</span> dan Mulai Karir Programmermu!</span>}
                    heroSubtitle="Apapun latar belakang kamu, melalui kelas online codepolitan kamu akan belajar terarah dari nol hingga menjadi mahir pemrograman Laravel dengan standar kompetensi kerja"
                    heroImg="/assets/img/program/hero-path-laravel.webp"
                    heroBgColor="bg-light"
                    heroFontColor="text-dark"
                    heroBtnColor="btn-outline-secondary"
                />
                <AboutSection
                    thumbnail="/assets/img/program/laravel-img.png"
                    title="Web Programming with Laravel Framework"
                    description="Dalam kelas ini kamu akan belajar dari awal hingga menjadi mahir framework Laravel. Melalui kelas ini, kamu akan dipandu untuk meningkatkan skill pemrograman web PHP-mu dengan belajar cara mengembangkan aplikasi web lebih cepat dan efisien menggunakan framework Laravel yang terkenal popular dan skillnya banyak dicari industri."
                />
                <StudycaseSection data={data[0].case_study} />
                <section className="section" style={{ backgroundColor: '#eee' }}>
                    <div className="container p-4 p-lg-5">
                        <div className="row">
                            <div className="col text-center">
                                <h3 className="section-title">Apa yang Akan Kamu Pelajari</h3>
                                <p className="text-muted">Yang akan kamu pelajari dalam kelas ini</p>
                            </div>
                        </div>
                        <div className="row mt-3">
                            {data[0].courses.map((item, index) => {
                                return (
                                    <div className="col-md-6 col-lg-4 col-xl-3 mb-4" key={index}>
                                        <div className="card border-0 shadow-sm" style={{ borderRadius: '15px', height: '100%' }}>
                                            <div className="card-header bg-white border-0" style={{ borderRadius: '15px' }}>
                                                <div className="row mt-2">
                                                    <div className="col-8">
                                                        <img height="50" src={item.thumbnail} alt="Javascript" />
                                                    </div>
                                                    <div className="col-4 text-center">
                                                        <span className={styles.card_label_materi}>{index + 1}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="card-body text-muted pb-0">
                                                <h5>{item.title}</h5>
                                                <p style={{ fontSize: 'smaller' }}>{item.description}</p>
                                            </div>
                                            <div className="row mb-3">
                                                <div className="col text-end">
                                                    <span
                                                        className="badge bg-codepolitan px-3"
                                                        style={{ borderRadius: 0, borderTopLeftRadius: '25px', borderBottomLeftRadius: '25px' }}
                                                    >
                                                        {item.number_module} modul
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                </section>
                <section className="section bg-light">
                    <div className="container p-4 p-lg-5">
                        <div className="row justify-content-center text-center">
                            <div className="col-6 col-md-4 my-2">
                                <p className="text-secondary mb-0"><span className={`${styles.count} text-secondary`}><CountUp end={15} duration={5} /></span> Kelas Laravel</p>
                            </div>
                            <div className="col-6 col-md-4 my-2">
                                <p className="text-secondary mb-0"><span className={`${styles.count} text-secondary`}><CountUp end={140191} duration={5} /></span> Member Aktif</p>
                            </div>
                            <div className="col-6 col-md-4 my-2">
                                <p className="text-secondary mb-0"><span className={`${styles.count} text-secondary`}><CountUp end={367} duration={5} /></span> Modul Belajar</p>
                            </div>
                        </div>
                    </div>
                </section>
                <WaktuBelajarSection />
                <ForumSection />
                <section className="section">
                    <div className="container p-4 p-lg-5">
                        <div className="row">
                            <div className="col-lg-5 offset-lg-1 my-auto">
                                <h2 className="section-title">Online Mentoring</h2>
                                <p className="text-muted my-3">Dapatkan kesempatan 12x berdiskusi dengan mentor kelas fullstack developer dalam sesi mentoring sebanyak 2x dalam 1 bulan, supaya kamu dapat belajar dengan terarah.</p>
                            </div>
                            <div className="col-lg-6 order-lg-first">
                                <img className="img-fluid" src="/assets/img/program/discord-img-3.png" alt="Online Mentoring" />
                            </div>
                        </div>
                    </div>
                </section>
                <CertificateSection />
                <MentorSection
                    name="Ahmad Hakim"
                    title="Mentor Path Laravel"
                    description="Saya Ahmad Hakim, seorang Full Stack Developer dan Senior Programmer di Codepolitan. Dengan pengalaman saya lebih dari 6 tahun berkarya dan  berkarir di bidang programming akan membantu kamu belajar dalam kelas ini. Apapun latar belakang kamu, melalui kelas ini kamu akan belajar membuat website perusahaan sendiri yang super keren. Saya tunggu di kelas!"
                    img="/assets/img/program/hakim.png"
                />
                <HowToLearnSection />
                <TestimonySection data={testimony} />
                <CtaSection
                    title="Tunggu Apalagi ? Ayo Gabung Program Belajar Pemrograman Laravel Framework Selama 6 Bulan!"
                    priceImg="/assets/img/program/price-laravel.png"
                    actionLink="https://pay.codepolitan.com/?slug=program-laravel-web-development"
                />
                <div className="bg-light">
                    <WarrantySection />
                </div>
                <FaqSection data={data[0].faq} />
            </Layout>
        </>
    );
};

export default PathLaravelLanding;
