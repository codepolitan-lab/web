import Head from 'next/head';
import Hero from '../../components/forMentor/Hero/Hero';
import SectionKeuntungan from '../../components/forMentor/Sections/SectionKeuntungan/SectionKeuntungan';
import SectionPeringkatPenghasilan from '../../components/forMentor/Sections/SectionPeringkatPenghasilan/SectionPeringkatPenghasilan';
import Layout from '../../components/global/Layout/Layout';
import SectionCodepolitanSelf from '../../components/forMentor/Sections/SectionCodepolitanSelf/SectionCodepolitanSelf';
import SectionQuotes from '../../components/forMentor/Sections/SectionQuotes/SectionQuotes';
import SectionLangkah from '../../components/forMentor/Sections/SectionLangkah/SectionLangkah';
import SectionMentorBekerja from '../../components/forMentor/Sections/SectionMentorBekerja/SectionMentorBekerja';
import SectionInvitation from '../../components/forMentor/Sections/SectionInvitation/SectionInvitation';
import axios from 'axios';
import SectionCounter from '../../components/global/Sections/SectionCounter/SectionCounter';

export const getServerSideProps = async () => {
    const statsRes = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/v1/stats`);
    const stats = statsRes.data;

    return {
        props: {
            stats
        }
    };
};

const ForMentor = ({ stats }) => {
    return (
        <>
            <Head>
                <title>Codepolitan for Mentor - Codepolitan</title>
                <meta name="title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta name="description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="og:type" content="website" />
                <meta property="og:url" content="https://codepolitan.com" />
                <meta property="og:title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta property="og:description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="og:image" content="https://i.ibb.co/8NQZp16/og-image-for-mentor.webp" />
                <meta property="twitter:card" content="summary_large_image" />
                <meta property="twitter:url" content="https://codepolitan.com" />
                <meta property="twitter:title" content="Website Belajar Coding Bahasa Indonesia - Codepolitan" />
                <meta property="twitter:description" content="Kembangkan karirmu sebagai developer profesional dengan keahlian coding yang dibutuhkan di dunia industri melalui kelas online Codepolitan" />
                <meta property="twitter:image" content="https://i.ibb.co/8NQZp16/og-image-for-mentor.webp" />
                <script
                    src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async="" />
            </Head>
            <Layout>
                <Hero />
                <SectionPeringkatPenghasilan />
                <SectionKeuntungan />
                <SectionCounter data={stats} />
                <SectionCodepolitanSelf />
                <SectionQuotes />
                <SectionLangkah />
                <SectionMentorBekerja />
                {/* <SectionSliderTestimony /> */}
                <SectionInvitation />
            </Layout>
        </>
    );
};

export default ForMentor;
