import { useEffect, useState } from 'react';
import { faCheckCircle, faRedo, faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Head from "next/head";
import Layout from "../../components/global/Layout/Layout";
import CardCourse from '../../components/global/Cards/CardCourse/CardCourse';
import Link from 'next/link';
import Modal from '../../components/global/Modal/Modal';
import axios from 'axios';
import SkeletonCardCourse from '../../components/skeletons/SkeletonCardCourse/SkeletonCardCourse';
import { useRouter } from 'next/router';

export async function getStaticProps() {
    const response = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/course/labels`);

    const allCourse = {
        term: 'Semua Kelas',
        term_slug: '',
        thumbnail: ''
    }

    const labels = [allCourse].concat(response.data)
    return {
        props: {
            labels
        },
    }
}

const Library = ({ labels }) => {
    const [courses, setCourses] = useState([]);
    const [filter, setFilter] = useState('');
    const [courseLabel, setCourseLabel] = useState('');
    const [sort, setSort] = useState('created_at[desc]');
    const [loading, setLoading] = useState(false);
    const [search, setSearch] = useState('');
    const [searchValue, setSearchValue] = useState('');
    const [limit, setLimit] = useState(12);
    const [limitMenu, setLimitMenu] = useState(10);
    const video = "lVzBOGons6U?rel=0";
    const [videoId, setVideoId] = useState(video);
    const { query } = useRouter();

    useEffect(() => {
        const getPopularCourses = async () => {
            setLoading(true);
            try {
                let response = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/course/popular`); // Ganti sama popular-weekly kalau APInya udah fix
                if (response.data.error === "No Content") {
                    setCourses(null);
                    setLoading(false);
                } else {
                    setLoading(false);
                    setCourses(response.data);
                    // if (response.data.length < 5) {
                    //     setPopularEndpoint('popular')
                    // };
                }
            } catch (err) {
                setLoading(true);
                return err.message;
            }
        };

        const getCourses = async () => {
            setLoading(true);
            try {
                let response = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/course?page=1&limit=200&search=${search}&course_label=${courseLabel}&filter=${filter}&sort=${sort}`);
                if (response.data.error === "No Content") {
                    setCourses(null);
                    setLoading(false);
                } else {
                    setCourses(response.data);
                    setLoading(false);
                    query.type && setCourseLabel(query.type);
                    query.type && document.getElementById('courses').scrollIntoView({ behavior: 'smooth' });
                    // Push first course id to local storage
                    if (sort === 'created_at[desc]' && filter === '' && courseLabel === '' && search === '') {
                        localStorage.setItem('latestCourseId', response.data[0].id);
                    }
                }
            } catch (err) {
                setLoading(true);
                return err.message;
            }
        };
        if (query.type == 'popular') {
            getPopularCourses();
            document.getElementById('courses').scrollIntoView({ behavior: 'smooth' });
            return;
        } else {
            getCourses();
            document.getElementById('courses').scrollIntoView({ behavior: 'smooth' });
        }
    }, [search, filter, sort, courseLabel, query.type]);

    const submitSearch = (e) => {
        e.preventDefault();
        setSearch('~' + searchValue + '~');
        setCourseLabel('');
        setFilter('');
        setSort('created_at[desc]');
    };

    const scrollToTopCourses = () => {
        setTimeout(() => {
            document.getElementById('filterCourses').scrollIntoView()
        }, 300);
    }

    const courseLevel = [
        {
            term: 'Semua Level',
            label: '',
        },
        {
            term: 'Pemula',
            label: 'level[eq]beginner',
        },
        {
            term: 'Menengah',
            label: 'level[eq]intermediate',
        },
        {
            term: 'Mahir',
            label: 'level[eq]advance',
        },
    ];

    const courseSort = [
        {
            term: 'Kelas Terbaru',
            label: 'created_at[desc]',
        },
        {
            term: 'Kelas Terpopuler',
            label: 'popular[desc]',
        },
        {
            term: 'Harga Tertinggi',
            label: 'retail_price[desc]',
        },
        {
            term: 'Harga Terendah',
            label: 'retail_price[asc]',
        },
    ];

    const showCourses = [
        {
            term: 'Semua Kelas',
            label: '',
        },
        {
            term: 'Kelas Gratis',
            label: 'free',
        },
    ];

    return (
        <>
            <Head>
                <title>Library - Codepolitan</title>
                <meta name="title" content="Kelas Online Skill Digital - CodePolitan" />
                <meta name="description" content="Akses lebih dari 170 kelas online di bidang pemrograman dan skill digital lainnya dari para expert" />
                <meta property="og:type" content="website" />
                <meta property="og:url" content="https://codepolitan.com" />
                <meta property="og:title" content="Kelas Online Skill Digital - CodePolitan" />
                <meta property="og:description" content="Akses lebih dari 170 kelas online di bidang pemrograman dan skill digital lainnya dari para expert" />
                <meta property="og:image" content="https://i.ibb.co/znsT2qH/OG-library.webp" />
                <meta property="twitter:card" content="summary_large_image" />
                <meta property="twitter:url" content="https://codepolitan.com" />
                <meta property="twitter:title" content="Kelas Online Skill Digital - CodePolitan" />
                <meta property="twitter:description" content="Akses lebih dari 170 kelas online di bidang pemrograman dan skill digital lainnya dari para expert" />
                <meta property="twitter:image" content="https://i.ibb.co/znsT2qH/OG-library.webp" />
            </Head>
            <Layout>
                <section className="section" id="courses" style={{ marginTop: '70px' }}>
                    <div className="container p-4 p-lg-5">
                        <div className="row mb-4">
                            <div className="col">
                                <h2 className="section-title h3">Pilihan Kelas Online Pemrograman</h2>
                                <p className="text-muted">Pilih dan jadilah professional!</p>
                            </div>
                        </div>
                        <div className="row" id="filterCourses">
                            {/* Sidebar */}
                            <div className="col-lg-3 d-none d-lg-block">
                                <div className="sticky-top" style={{ top: '100px' }}>
                                    <h3 className="section-title h5 mb-3">Filter Kelas</h3>
                                    <ul className="list-group">
                                        {labels.map((item, index) => {
                                            const handleClick = () => {
                                                setCourseLabel(item.term_slug);
                                                setFilter('');
                                                setSort('created_at[desc]');
                                                setSearch('');
                                                query.type = '';
                                                window.history.replaceState(null, '', '/library/')
                                            }
                                            return (
                                                <li
                                                    key={index}
                                                    className={courseLabel === item.term_slug ? "list-group-item border-0 text-primary px-0" : "list-group-item border-0 text-muted px-0"}
                                                    role="button"
                                                    onClick={handleClick}
                                                    style={{ fontWeight: courseLabel === item.term_slug && 'bold' }}
                                                >
                                                    <div onClick={() => scrollToTopCourses()} className="d-flex align-items-center">
                                                        <img src={item.thumbnail || '/assets/img/placeholder.jpg'} width={30} className="img-fluid me-2" alt={item.term_slug} />
                                                        {/* <FontAwesomeIcon className='me-2' fixedWidth icon={item.icon} /> */}
                                                        <span className="mt-1">{item.term}</span>
                                                    </div>
                                                </li>
                                            );
                                        }).slice(0, limitMenu)}
                                        <li className="list-group-item border-0 text-primary px-0">
                                            {labels.length > limitMenu && <div onClick={() => setLimitMenu(limitMenu + 10)} className="link" style={{ cursor: 'pointer' }}>Show more</div>}
                                            {labels.length < limitMenu && <div onClick={() => { setLimitMenu(10); scrollToTopCourses() }} className="link" style={{ cursor: 'pointer' }}>Reset</div>}
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            {/* End of Sidebar */}

                            {/* Main */}
                            <div className="col col-lg-9">
                                {/* Menu */}
                                <div className="sticky-top" style={{ top: '70px' }}>
                                    <div className="row mb-3 bg-white py-3">
                                        <div className="col-md-6 mb-3 mb-lg-0 my-auto">
                                            <div className="d-flex align-items-lg-start">
                                                <div className="dropdown d-lg-none">
                                                    <button className="btn btn-outline-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                        Filter
                                                    </button>
                                                    <ul className="dropdown-menu overflow-scroll" style={{ height: '200px!important' }} aria-labelledby="dropdownMenuButton1">
                                                        {labels.map((item, index) => {
                                                            const handleClick = () => {
                                                                setCourseLabel(item.term_slug);
                                                                setFilter('');
                                                                setSort('created_at[desc]');
                                                                setSearch('');
                                                            }
                                                            return (
                                                                <li
                                                                    key={index}
                                                                    onClick={handleClick}
                                                                    className="dropdown-item"
                                                                    role="button"
                                                                >
                                                                    {item.term}
                                                                    {item.term_slug === courseLabel && (
                                                                        <FontAwesomeIcon className="text-primary ms-1" icon={faCheckCircle} />
                                                                    )}
                                                                </li>
                                                            );
                                                        }).slice(0, limitMenu)}
                                                        <li className="list-group-item border-0 text-primary px-0 text-center">
                                                            {labels.length > limitMenu && <div onClick={() => setLimitMenu(limitMenu + 10)} className="link" style={{ cursor: 'pointer' }}>Show more</div>}
                                                            {labels.length < limitMenu && <div onClick={() => setLimitMenu(10)} className="link" style={{ cursor: 'pointer' }}>Reset</div>}
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div className="dropdown ms-2 ms-lg-0">
                                                    <button className="btn btn-outline-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                        Level
                                                    </button>
                                                    <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                        {courseLevel.map((item, index) => {
                                                            return (
                                                                <li
                                                                    key={index}
                                                                    onClick={() => {
                                                                        setFilter(item.label);
                                                                        query.type = '';
                                                                        window.history.replaceState(null, '', '/library/')
                                                                    }}
                                                                    className="dropdown-item"
                                                                    role="button"
                                                                >
                                                                    {item.term}
                                                                    {item.label === filter && (
                                                                        <FontAwesomeIcon className="text-primary ms-1" icon={faCheckCircle} />
                                                                    )}
                                                                </li>
                                                            );
                                                        })}
                                                    </ul>
                                                </div>
                                                <div className="dropdown ms-2">
                                                    <button className="btn btn-outline-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                        Urutkan
                                                    </button>
                                                    <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                        {courseSort.map((item, index) => {
                                                            return (
                                                                <li
                                                                    key={index}
                                                                    onClick={() => {
                                                                        setSort(item.label);
                                                                        query.type = '';
                                                                        window.history.replaceState(null, '', '/library/')
                                                                    }}
                                                                    className="dropdown-item"
                                                                    role="button"
                                                                >
                                                                    {item.term}
                                                                    {item.label === sort && (
                                                                        <FontAwesomeIcon className="text-primary ms-1" icon={faCheckCircle} />
                                                                    )}
                                                                </li>
                                                            );
                                                        })}
                                                    </ul>
                                                </div>
                                                <div className="dropdown ms-2">
                                                    <button className="btn btn-outline-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                        Tampilkan
                                                    </button>
                                                    <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                        {showCourses.map((item, index) => {
                                                            return (
                                                                <li
                                                                    key={index}
                                                                    onClick={() => {
                                                                        setCourseLabel(item.label);
                                                                        query.type = '';
                                                                        window.history.replaceState(null, '', '/library/')
                                                                    }}
                                                                    className="dropdown-item"
                                                                    role="button"
                                                                >
                                                                    {item.term}
                                                                    {item.label === courseLabel && (
                                                                        <FontAwesomeIcon className="text-primary ms-1" icon={faCheckCircle} />
                                                                    )}
                                                                </li>
                                                            );
                                                        })}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-5 col-lg-4 ms-auto">
                                            <form onSubmit={submitSearch}>
                                                <div className="input-group">
                                                    <input onChange={(e) => setSearchValue(e.target.value)} type="search" className="form-control shadow-none" placeholder="cari kelas dan enter disini..." />
                                                    <button className="input-group-text bg-white text-muted" role="button" type="submit" title="Cari">
                                                        <FontAwesomeIcon icon={faSearch} />
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                {/* Course Loop */}
                                <div className="row">
                                    {!loading && courses === null && (
                                        <div className="col text-center my-5">
                                            <object width={'35%'} type="image/svg+xml" data="/assets/img/icon_404.svg">Ups</object>
                                            <p className="text-muted">Ups, kelas tidak ditemukan, coba cari dengan kata kunci lain</p>
                                        </div>
                                    )}
                                    {loading && [1, 2, 3, 4, 5, 6].map(index => (
                                        <div className="col-md-6 col-xl-4 p-2" key={index}>
                                            <SkeletonCardCourse />
                                        </div>
                                    ))}
                                    {!loading && courses !== null && courses?.map((course, index) => {
                                        return (
                                            <div className="col-md-6 col-xl-4 px-2 py-3" key={index}>
                                                <Link href={`/course/intro/${course.slug}`}>
                                                    <a className="link">
                                                        <CardCourse
                                                            thumbnail={course.thumbnail}
                                                            author={course.author}
                                                            title={course.title}
                                                            level={course.level}
                                                            totalStudents={course.total_student}
                                                            totalModules={course.total_module}
                                                            totalTimes={course.total_time}
                                                            totalRating={course.total_rating}
                                                            totalFeedback={course.total_feedback}
                                                            normalBuyPrice={course.buy?.normal_price || course.normal_price}
                                                            retailBuyPrice={course.buy?.retail_price || course.retail_price}
                                                            normalRentPrice={course.rent?.normal_price}
                                                            retailRentPrice={course.rent?.retail_price}
                                                        />
                                                    </a>
                                                </Link>
                                            </div>
                                        );
                                    }).slice(0, limit)}
                                </div>
                                {!loading && courses?.length > limit && (
                                    <div className="row my-3">
                                        <div className="col text-center">
                                            <button onClick={() => setLimit(limit + 9)} className="btn btn-light">
                                                Show more
                                                <FontAwesomeIcon size="sm" className="ms-2" icon={faRedo} />
                                            </button>
                                        </div>
                                    </div>
                                )}
                                {/* End of Course Loop */}
                            </div>
                            {/* End of Main */}
                        </div>
                    </div>
                </section>
                <Modal title="Cara Belajar" setVideo={() => setVideoId('')}>
                    <div className="ratio ratio-16x9">
                        <iframe src={`https://www.youtube.com/embed/${videoId}`} title="YouTube video" allowFullScreen />
                    </div>
                </Modal>
            </Layout>
        </>
    );
};

export default Library;
