import axios from "axios";
import Head from "next/head";
import Layout from "../../components/global/Layout/Layout";
import Hero from "../../components/forschool/Hero/Hero";
import SectionCounter from "../../components/global/Sections/SectionCounter/SectionCounter";
import SectionPartners from "../../components/global/Sections/SectionPartners/SectionPartners";
import SectionFormatBelajar from "../../components/forschool/Sections/SectionFormatBelajar/SectionFormatBelajar";
import SectionRoadmapList from "../../components/global/Sections/SectionRoadmapList/SectionRoadmapList";
import SectionTechnology from "../../components/forschool/Sections/SectionTechnology/SectionTechnology";
import BenefitSection from "../../components/global/Sections/BenefitSection/BenefitSection";
import SectionTestimonyLanding from "../../components/global/Sections/SectionTestimonyLanding/SectionTestimonyLanding";
import SectionPartner from "../../components/forschool/Sections/SectionPartner/SectionPartner";
import SectionCTA from "../../components/global/Sections/SectionCTA/SectionCTA";

export const getServerSideProps = async () => {
    const statsRes = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/v1/stats`);
    const stats = statsRes.data;

    return {
        props: {
            stats
        }
    };
};

const ForSchool = ({ stats }) => {

    const testimonials = [
        {
            name: 'Soni',
            thumbnail: 'https://i.ibb.co/QPKMdjk/image2.jpg',
            role: 'Ketua Program Studi',
            organization: 'Universitas Respati Indonesia',
            content: 'Mahasiswa lebih aplikatif dalam implementasi pada hal yang berkaitan dengan matkul, Mendapatkan tambahan keahlian sesuai dengan bidang ilmu, Menambah sertifikat keahlian yang dapat digunakan dalam mencari pekerjaan'
        },
        {
            name: 'Evanita Veronica Manullang',
            thumbnail: 'https://i.ibb.co/q039Hz0/image1.jpg',
            role: 'Kepala Laboratorium FIKOM',
            organization: 'Universitas Sains dan Teknologi Jayapura',
            content: 'Kemampuan mahasiswa menjadi semakin baik dengan bantuan video-video pembelajaran yang terstruktur, terlebih disaat pandemi covid 19. Mahasiswa yang mengikuti program kampus coding sebagian besar sudah dapat membuat aplikasi web sendiri. '
        },
    ];

    return (
        <>
            <Head>
                <title>Codepolitan for School - Codepolitan</title>
                <meta name="title" content="Codepolitan for School - Codepolitan" />
                <meta name="description" content="Persiapkan Siswa dengan Keterampilan Industri 4.0. Program yang dikembangkan khusus untuk membantu generasi muda belajar dan berlatih pemrograman agar siswa siap terjun dan bersaing di dunia kerja dalam industri 4.0" />
                <meta property="og:type" content="website" />
                <meta property="og:url" content="https://codepolitan.com/for-school" />
                <meta property="og:title" content="Codepolitan for School - Codepolitan" />
                <meta property="og:description" content="Persiapkan Siswa dengan Keterampilan Industri 4.0. Program yang dikembangkan khusus untuk membantu generasi muda belajar dan berlatih pemrograman agar siswa siap terjun dan bersaing di dunia kerja dalam industri 4.0" />
                <meta property="og:image" content="https://i.ibb.co/Jtk0cwS/OG-For-School.png" />
                <meta property="twitter:card" content="summary_large_image" />
                <meta property="twitter:url" content="https://codepolitan.com/for-school" />
                <meta property="twitter:title" content="Codepolitan for School - Codepolitan" />
                <meta property="twitter:description" content="Persiapkan Siswa dengan Keterampilan Industri 4.0. Program yang dikembangkan khusus untuk membantu generasi muda belajar dan berlatih pemrograman agar siswa siap terjun dan bersaing di dunia kerja dalam industri 4.0" />
                <meta property="twitter:image" content="https://i.ibb.co/Jtk0cwS/OG-For-School.png" />
            </Head>
            <Layout>
                <Hero />
                <SectionCounter data={stats} />
                <SectionPartners
                    backgroundColor="bg-white"
                    sectionTitleTextStyle="text-center"
                />
                <SectionFormatBelajar />
                <SectionRoadmapList
                    title="Terapkan Jalur Belajar Profesional Untuk Siswa Anda"
                    subtitle="Ikuti Jalur belajar menjadi proffesional di dunia kerja seperti"
                />
                <SectionTechnology />
                <BenefitSection
                    title="Kenapa CodePolitan For School?"
                    subtitle="CodePolitan memiliki fasilitas yang lengkap sebagai sebuah platform untuk mendukung efektivitas belajar pemrograman secara online"
                />
                <SectionTestimonyLanding
                    title="Sekolah Yang Berinovasi Bersama CodePolitan"
                    data={testimonials}
                />
                <SectionPartner />
                <SectionCTA
                    caption="Siap untuk berkolaborasi dalam mencetak lulusan IT handal bersama CodePolitan?"
                    btnTitle="Hubungi Kami Sekarang"
                    link="https://wa.me/628999488990?text=Halo, saya mau tanya tentang Kampuscoding / Codepolitan For School."
                />
            </Layout>
        </>
    );
};

export default ForSchool;
